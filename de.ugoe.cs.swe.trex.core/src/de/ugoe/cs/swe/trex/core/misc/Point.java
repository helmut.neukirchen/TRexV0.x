package de.ugoe.cs.swe.trex.core.misc;
@SuppressWarnings("rawtypes")
public class Point implements Comparable {
	int point;

	@Override
	public String toString() {
		return "[(Point) " + point + "]";
	}

	public Point(final int pTopic) {
		this.point = pTopic;
	}

	public final int compareTo(Object o) {
		if (o instanceof Range) {
			Range oRange = (Range) o;
			if (oRange.from <= this.point && this.point <= oRange.to) {
				return 0;
			} else if (this.point < oRange.to) {
				return -1;
			}
			return 1;
		} else if (o instanceof Point) {
			Point oPoint = (Point) o;
			if (this.point == oPoint.point) {
				return 0;
			} else if (this.point < oPoint.point) {
				return -1;
			}
			return 1;
		} else {
			throw new IllegalArgumentException(o.getClass().getName());
		}
	}
}
