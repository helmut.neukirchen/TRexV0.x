package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

public class PredefinedFunctions {
/*
module trexPredefinedFunctions {
	// not all entries are proper representatives!

	function int2char(integer theValue) return charstring {
	}

	function int2unichar(charstring theValue) return universal charstring {
	}

	function int2bit(in integer theValue, in integer theLength) return bitstring {
	}

	function int2hex(in integer theValue, in integer theLength) return hexstring {
	}

	function int2oct(in integer theValue, in integer theLength) return octetstring {
	}

	function int2str(integer theValue) return charstring {
	}

	function int2float(integer theValue) return float {
	}

	function float2int(float theValue) return integer {
	}

	function char2int(charstring theValue) return charstring {
	}

	function char2oct(charstring invalue) return octetstring {
	}

	function unichar2int(charstring theValue) return integer {
	}

	function bit2int(charstring theValue) return integer {
	}

	function bit2hex(bitstring theValue) return hexstring {
	}

	function bit2oct(bitstring theValue) return octetstring {
	}

	function bit2str(bitstring theValue) return charstring {
	}

	function hex2int(hexstring theValue) return integer {
	}

	function hex2bit(hexstring theValue) return bitstring {
	}

	function hex2oct(hexstring theValue) return octetstring {
	}

	function hex2str(hexstring theValue) return charstring {
	}

	function oct2int(octetstring theValue) return integer {
	}

	function oct2bit(octetstring theValue) return bitstring {
	}

	function oct2hex(octetstring theValue) return hexstring {
	}

	function oct2str(octetstring invalue) return charstring {
	}

	function oct2char(octetstring invalue) return charstring {
	}

	function str2int(charstring theValue) return integer {
	}

	function str2oct(charstring invalue) return octetstring {
	}

	function str2float(charstring theValue) return float {
	}

	// new
	function enum2int(enum theValue) return itneger {
	}

	function lengthof(anytype theValue) return charstring {
	}

	function sizeof(anytype theValue) return integer {
	}

	function ispresent(anytype theValue) return boolean {
	}

	function ischosen(anytype theValue) return boolean {
	}

	// new
	function isvalue(anytype theValue) return boolean {
	}

	function regexp(anytype instr, charstring expression, integer groupno) return anytype {
	}

	function substr(anytype theValue, in integer index, in integer returncount) return anytype {
	}

	function replace(in anytype str, in integer ind, in integer len, in anytype rep1) return anytype {
	}

	// new
	function encvalue(anytype theValue) return bitstring {
	}

	// new
	function decvalue(bitstring theValue) return anytype {
	}

	function rnd(float seed) return float {
	}

	function rnd() return float {
	}

	//new
	function str2hex(in charstring invalue) return hexstring{
	}

	// deprecated
	function sizeoftype(anytype theValue) return integer {
	}
}

 */
	//TODO: need to bring this up to date!
	// use Clipboard2JavaString to convert comment above to java string!
	public final static String PREDEFINED_FUNCTIONS = "module trexPredefinedFunctions {\n" +
	"	// not all entries are proper representatives!\n" +
	"	\n" +
	"	function int2char(integer theValue) return charstring {\n" +
	"	}\n" +
	"	\n" +
	"	function int2unichar(charstring theValue) return universal charstring {\n" +
	"	}\n" +
	"	\n" +
	"	function int2bit(in integer theValue, in integer theLength) return bitstring {\n" +
	"	}\n" +
	"	\n" +
	"	function int2hex(in integer theValue, in integer theLength) return hexstring {\n" +
	"	}\n" +
	"	\n" +
	"	function int2oct(in integer theValue, in integer theLength) return octetstring {\n" +
	"	}\n" +
	"	\n" +
	"	function int2str(integer theValue) return charstring {\n" +
	"	}\n" +
	"	\n" +
	"	function int2float(integer theValue) return float {\n" +
	"	}\n" +
	"	\n" +
	"	function float2int(float theValue) return integer {\n" +
	"	}\n" +
	"	\n" +
	"	function char2int(charstring theValue) return charstring {\n" +
	"	}\n" +
	"	function char2oct(charstring invalue) return octetstring {\n" +
	"	}\n" +
	"	function unichar2int(charstring theValue) return integer {\n" +
	"	}\n" +
	"	function bit2int(charstring theValue) return integer {\n" +
	"	}\n" +
	"	function bit2hex(bitstring theValue) return hexstring {\n" +
	"	}\n" +
	"	function bit2oct(bitstring theValue) return octetstring {\n" +
	"	}\n" +
	"	function bit2str(bitstring theValue) return charstring {\n" +
	"	}\n" +
	"	function hex2int(hexstring theValue) return integer {\n" +
	"	}\n" +
	"	function hex2bit(hexstring theValue) return bitstring {\n" +
	"	}\n" +
	"	function hex2oct(hexstring theValue) return octetstring {\n" +
	"	}\n" +
	"	function hex2str(hexstring theValue) return charstring {\n" +
	"	}\n" +
	"	function oct2int(octetstring theValue) return integer {\n" +
	"	}\n" +
	"	function oct2bit(octetstring theValue) return bitstring {\n" +
	"	}\n" +
	"	function oct2hex(octetstring theValue) return hexstring {\n" +
	"	}\n" +
	"	function oct2str(octetstring invalue) return charstring {\n" +
	"	}\n" +
	"	function oct2char(octetstring invalue) return charstring {\n" +
	"	}\n" +
	"	function str2int(charstring theValue) return integer {\n" +
	"	}\n" +
	"	function str2oct(charstring invalue) return octetstring {\n" +
	"	}\n" +
	"	function str2float(charstring theValue) return float {\n" +
	"	}\n" +
	"	// new\n" +
	"	function enum2int(enum theValue) return itneger {\n" +
	"	}\n" +
	"	function lengthof(anytype theValue) return charstring {\n" +
	"	}\n" +
	"	function sizeof(anytype theValue) return integer {\n" +
	"	}\n" +
	"	\n" +
	"	function ispresent(anytype theValue) return boolean {\n" +
	"	}\n" +
	"	\n" +
	"	function ischosen(anytype theValue) return boolean {\n" +
	"	}\n" +
	"	\n" +
	"	// new\n" +
	"	function isvalue(anytype theValue) return boolean {\n" +
	"	}\n" +
	"	\n" +
	"	function isbound(in template anytype inpar) return boolean {" +
	"	}\n" +
	"	\n" +
	"	function regexp(anytype instr, charstring expression, integer groupno) return anytype {\n" +
	"	}\n" +
	"	\n" +
	"	function substr(anytype theValue, in integer index, in integer returncount) return anytype {\n" +
	"	}\n" +
	"	\n" +
	"	function replace(in anytype str, in integer ind, in integer len, in anytype rep1) return anytype {\n" +
	"	}\n" +
	"	// new\n" +
	"	function encvalue(anytype theValue) return bitstring {\n" +
	"	}\n" +
	"	\n" +
	"	// new\n" +
	"	function decvalue(bitstring theValue) return anytype {\n" +
	"	}\n" +
	"	function rnd(float seed) return float {\n" +
	"	}\n" +
	"	\n" +
	"	function rnd() return float {\n" +
	"	}\n" +
	"	//new\n"+
	"	function str2hex(in charstring invalue) return hexstring{\n"+
	"	}\n"+
	"	// deprecated	 \n" +
	"	function sizeoftype(anytype theValue) return integer {\n" +
	"	}\n" +
	"	function testcasename() return charstring {\n" +
	"	}\n" +
	"	\n" +
	"}";

}
