lexer grammar TTCN3Lexer;

tokens {
  AddExpression;
  AddressRefList;
  AliveKeyword;
  AliveOp;
  AllAltstepsWithExcept;
  AllCompsAllPortsSpec;
  AllConnectionsSpec;
  AllConstsWithExcept;
  AllFunctionsWithExcept;
  AllGroupsWithExcept;
  AllModuleParWithExcept;
  AllPortsSpec;
  AllSignaturesWithExcept;
  AllTemplsWithExcept;
  AllTestcasesWithExcept;
  AllTypesWithExcept;
  AllWithExcepts;
  AltstepInstance;
  AltstepLocalDef;
  AltstepLocalDefList;
  AltstepRef;
  AltstepRefList;
  AndExpression;
  AnyTypeKeyword;
  AllRef;
  BitAndExpression;
  BitNotExpression;
  BitOrExpression;
  BitXorExpression;
  BreakStatement;
  CaseKeyword;
  CheckPortOpsPresent;
  CompenentIdentifierOrLiteral;
  ComponentIdentifierOrLiteral;
  ComponentReferenceOrLiteral;
  ConstRefList;
  ContinueStatement;
  DefaultKeyword;
  DerivedRefWithParList;
  EqualExpression;
  ExceptAltstepSpec;
  ExceptConstSpec;
  ExceptElement;
  ExceptFunctionSpec;
  ExceptGroupRefList;
  ExceptGroupSpec;
  ExceptModuleParSpec;
  ExceptSignatureSpec;
  ExceptSpec;
  ExceptTemplateSpec;
  ExceptTestcaseSpec;
  ExceptTypeDefSpec;
  ExceptsDef;
  ExtendsKeyword;
  FloatDotNotation;
  FloatENotation;
  FromClausePresent;
  FullGroupIdentifier;
  FullGroupIdentifierWithExcept;
  FunctionRefList;
  GroupRefList;
  GroupRefListWithExcept;
  ImportAltstepSpec;
  ImportModuleParSpec;
  KillKeyword;
  KillTCStatement;
  KilledStatement;
  LogItem;
  ModuleDefinitionList;
  ModuleParRefList;
  MulExpression;
  NestedEnumDef;
  NestedRecordDef;
  NestedRecordOfDef;
  NestedSetDef;
  NestedSetOfDef;
  NestedTypeDef;
  NestedUnionDef;
  NotExpression;
  OmitValue;
  PortRedirectSymbol;
  PortRedirectWithValueAndParam;
  RedirectPresent;
  RedirectWithParamSpec;
  RedirectWithValueAndParamSpec;
  RelExpression;
  RepeatStatement;
  RestrictedTemplate;
  SelectCase;
  SelectCaseBody;
  SelectCaseConstruct;
  SelectKeyword;
  ShiftExpression;
  SignatureRefList;
  SingleConnectionSpec;
  SingleOrMultiConnectionSpec;
  SingleTimerInstance;
  TempVarList;
  TemplateRefList;
  TemplateRestriction;
  TestcaseRefList;
  TimerList;
  TypeRefList;
  UnaryExpression;
  NameForm;
  NumberForm;
  NameAndNumberForm;
  VariationKeyword;
  DefinitiveIdentifier;
  DefinitiveNumberForm;
  XorExpression;
  DefinitiveNameAndNumberForm;
  ActivateOp;
  AddOp;
  AddressKeyword;
  AddressRef;
  AddressValue;
  AllKeyword;
  AllOrSignatureList;
  AllOrTypeList;
  AllPort;
  AllowedValues;
  AltConstruct;
  AltGuardChar;
  AltGuardElement;
  AltGuardElements;
  AltGuardList;
  AltKeyword;
  AltstepDef;
  AltstepKeyword;
  And4b;
  And;
  AnyKeyword;
  AnyOrOmit;
  AnyValue;
  ArrayBounds;
  ArrayDef;
  ArrayDefRange;
  ArrayElementConstExpressionList;
  ArrayElementExpressionList;
  ArrayElementSpec;
  ArrayElementSpecList;
  ArrayExpression;
  ArrayOrBitRef;
  ArrayValueOrAttrib;
  Assignment;
  AssignmentChar;
  AssignmentList;
  AttribKeyword;
  AttribQualifier;
  AttribSpec;
  BaseTemplate;
  BasicStatements;
  BeginChar;
  BehaviourStatements;
  BinOrMatch;
  BitOp;
  BitStringKeyword;
  BitStringMatch;
  BitStringValue;
  BooleanExpression;
  BooleanKeyword;
  BooleanValue;
  CallBodyGuard;
  CallBodyOps;
  CallBodyStatement;
  CallBodyStatementList;
  CallParameters;
  CallStatement;
  CallTimerValue;
  CatchOpParameter;
  CatchStatement;
  Cell;
  CharKeyword;
  CharStringKeyword;
  CharStringMatch;
  CharStringPattern;
  CharStringValue;
  CharStringValueId;
  CheckParameter;
  CheckStatement;
  ClearStatement;
  Colon;
  Comma;
  CommunicationStatements;
  Complement;
  ComponentDef;
  ComponentDefList;
  ComponentElementDef;
  ComponentId;
  ComponentIdentifier;
  ComponentRef;
  ComponentType;
  ComponentRefAssignment;
  CompoundConstExpression;
  CompoundExpression;
  ConditionalConstruct;
  ConfigSpec;
  ConfigurationOps;
  ConfigurationStatements;
  ConnectStatement;
  ConstDef;
  ConstList;
  ConstantExpression;
  ControlStatement;
  ControlStatementOrDef;
  ControlStatementOrDefList;
  CreateOp;
  Cstring;
  Dash;
  DeactivateStatement;
  DefIndentClose;
  DefIndentOpen;
  DefKeyword;
  DefOrFieldRef;
  DefOrFieldRefList;
  DerivedDef;
  DerivedRegWithParList;
  Direction;
  DisconnectStatement;
  DisplayKeyword;
  DoWhileStatement;
  DoneStatement;
  Dot;
  DoubleColon;
  ElseClause;
  ElseIfClause;
  ElseIfClauses;
  ElseStatement;
  EncodeKeyword;
  EndChar;
  EnumDef;
  EnumeratedValue;
  Enumeration;
  EnumerationList;
  Equal;
  Error;
  ExceptionSpec;
  ExceptionTypeList;
  Expression;
  ExtConstDef;
  ExtFunctionDef;
  ExtendedFieldReference;
  ExtensionKeyword;
  ExtraMatchingAttributes;
  Factor;
  Fail;
  False;
  FieldConstExpressionList;
  FieldConstExpressionSpec;
  FieldExpressionList;
  FieldExpressionSpec;
  FieldReference;
  FieldSpec;
  FieldSpecList;
  FieldValueOrAttrib;
  Final;
  FloatKeyword;
  FloatValue;
  ForStatement;
  FormalPortPar;
  FormalTemplatePar;
  FormalTimerPar;
  FormalTypePar;
  FormalValuePar;
  FromClause;
  FunctionActualParAssignment;
  FriendKeyword;
  FriendModuleDef;
  FunctionActualPar;
  FunctionActualParList;
  FunctionBody;
  FunctionDef;
  FunctionLocalDefOrInst;
  FunctionLocalDefOrInstList;
  FunctionStatementList;
  FunctionFormalPar;
  FunctionInstance;
  FunctionLocalDef;
  FunctionLocalInst;
  FunctionRef;
  FunctionStatement;
  GetCallStatement;
  GetLocalVerdict;
  GetReplyStatement;
  GlobalModuleId;
  GotoStatement;
  Group;
  GroupDef;
  GuardOp;
  GuardStatement;
  HexOrMatch;
  HexStringKeyword;
  HexStringMatch;
  HexStringValue;
  HaltStatement;
  Identifier;
  IfPresentMatch;
  ImportAllSpec;
  ImportConstSpec;
  ImportDef;
  ImportElement;
  ImportFromSpec;
  ImportFunctionSpec;
  ImportGroupSpec;
  ImportNamedAltSpec;
  ImportSignatureSpec;
  ImportSpec;
  ImportTemplateSpec;
  ImportTestcaseSpec;
  ImportTypeDefSpec;
  InLineTemplate;
  InOutParKeyword;
  InParKeyword;
  Inconc;
  Infinity;
  Initial;
  IntegerKeyword;
  IntegerValue;
  InterleavedAction;
  InterleavedConstruct;
  InterleavedGuard;
  InterleavedGuardElement;
  InterleavedGuardList;
  KilledKeyword;
  LParen;
  LabelStatement;
  LanguageSpec;
  LengthMatch;
  Lessorequal;
  Lessthan;
  LogStatement;
  LoopConstruct;
  LowerBound;
  MTCOp;
  MapStatement;
  MatchOp;
  MatchingSymbol;
  MessageAttribs;
  MessageList;
  Minus;
  MixedAttribs;
  MixedList;
  Mod;
  ModifiesKeyword;
  ModuleControlBody;
  ModuleControlPart;
  ModuleDefinition;
  ModuleDefinitionsList;
  ModuleDefinitionsPart;
  ModuleId;
  ModulePar;
  ModuleParDef;
  ModuleParList;
  Moreorequal;
  Morethan;
  MultiWithAttrib;
  MultiplyOp;
  MultitypedModuleParList;
  NamedAltActualPar;
  NamedAltActualParList;
  NamedAltDef;
  NamedAltFormalPar;
  NamedAltFormalParList;
  NamedAltInstance;
  NamedAltList;
  NamedAltRef;
  NamedAltRefList;
  NamedKeyword;
  NamedValue;
  NamedValueList;
  NoBlockKeyword;
  None;
  Not4b;
  Not;
  NotUsedOrExpression;
  NotUsedSymbol;
  Notequals;
  NowaitKeyword;
  Null;
  Number;
  OctOrMatch;
  OctetStringKeyword;
  OctetStringMatch;
  OctetStringValue;
  OmitKeyword;
  OpCall;
  OpValue;
  OptionalKeyword;
  Or4b;
  Or;
  OutParKeyword;
  OverrideKeyword;
  ParRef;
  ParaAssignmentList;
  ParaSpec;
  Pass;
  PermutationMatch;
  Plane;
  Plus;
  Port;
  PortCallBody;
  PortCallOp;
  PortCatchOp;
  PortCheckOp;
  PortClearOp;
  PortDef;
  PortDefAttribs;
  PortDefBody;
  PortElement;
  PortGetCallOp;
  PortGetReplyOp;
  PortInstance;
  PortKeyword;
  PortOrAll;
  PortOrAny;
  PortRaiseOp;
  PortReceiveOp;
  PortRedirect;
  PortRedirectWithParam;
  PortRef;
  PortReplyOp;
  PortSendOp;
  PortSpec;
  PortStartOp;
  PortStopOp;
  PortTriggerOp;
  PortType;
  PredefinedType;
  PredefinedValue;
  Primary;
  ProcOrType;
  ProcOrTypeList;
  ProcedureAttribs;
  ProcedureList;
  Product;
  PortHaltOp;
  PrivateKeyword;
  PublicKeyword;
  Quadruple;
  QuestionMark;
  RParen;
  RaiseStatement;
  Range;
  RangeDef;
  RangeOp;
  ReadTimerOp;
  ReceiveParameter;
  ReceiveStatement;
  RecordDef;
  RecordOfDef;
  RecordRef;
  RecursiveKeyword;
  ReferencedType;
  ReferencedValue;
  RelOp;
  Rem;
  ReplyStatement;
  ReturnStatement;
  ReturnType;
  Row;
  RunningOp;
  RunningTimerOp;
  RunsOnSpec;
  SUTAction;
  SUTStatements;
  SelfOp;
  SemiColon;
  SendParameter;
  SendStatement;
  SenderSpec;
  SetDef;
  SetLocalVerdict;
  SetOfDef;
  ShiftOp;
  Signature;
  SignatureDef;
  SignatureFormalPar;
  SignatureFormalParList;
  SignatureList;
  SimpleExpression;
  SimpleExpressions;
  SingleConstDef;
  SingleExpression;
  SingleQuote;
  SingleTempVarInstance;
  SingleValueOrAttrib;
  SingleVarInstance;
  SingleValueSpec;
  SingleWithAttrib;
  Slash;
  SquareClose;
  SquareOpen;
  Star;
  StartStatement;
  StartTCStatement;
  StartTimerStatement;
  StatementBlock;
  Step;
  StopStatement;
  StopTCStatement;
  StopTimerStatement;
  StringAnyOrOmit;
  StringAnyValue;
  StringLength;
  StringMatchOp;
  StringOp;
  StructDefBody;
  StructDefFormalPar;
  StructDefFormalParList;
  StructFieldDef;
  StructFieldRef;
  StructOfDefBody;
  StructuredTypeDef;
  SubExpression;
  SubTypeDef;
  SubTypeSpec;
  SubsetMatch;
  SupersetMatch;
  SystemOp;
  SystemSpec;
  TTCN3File;
  TTCN3Module;
  TTCN3ModuleId;
  TemplateActualPar;
  TemplateActualParList;
  TemplateBody;
  TemplateDef;
  TemplateFormalPar;
  TemplateFormalParList;
  TemplateInstanceAssignment;
  TemplateInstance;
  TemplateKeyword;
  TemplateOps;
  TemplateRef;
  TemplateRefWithParList;
  Term;
  TestcaseActualParAssignment;
  TestcaseActualPar;
  TestcaseActualParList;
  TestcaseDef;
  TestcaseInstance;
  TestcaseRef;
  TimeoutKeyword;
  TimeoutStatement;
  TimerInstance;
  TimerKeyword;
  TimerOps;
  TimerRef;
  TimerRefOrAll;
  TimerRefOrAny;
  TimerStatements;
  TimerValue;
  ToClause;
  TriggerStatement;
  True;
  Type;
  TypeActualPar;
  TypeActualParList;
  TypeDef;
  TypeDefIdentifier;
  TypeList;
  TypeReference;
  UnaryOp;
  Underscore;
  UnionDef;
  UnionDefBody;
  UnionFieldDef;
  UniversalChar;
  UniversalCharString;
  UniversalKeyword;
  UnmapStatement;
  UpperBound;
  Value;
  ValueList;
  ValueMatchSpec;
  ValueOrAttribList;
  ValueOrRange;
  ValueParOrTypePar;
  ValueReference;
  ValueSpec;
  ValueofOp;
  VarInitialValue;
  VarInstance;
  VarList;
  VariableAssignment;
  VariableEntry;
  VariableList;
  VariableRef;
  VerdictOps;
  VerdictStatements;
  VerdictTypeKeyword;
  VerdictTypeValue;
  Visibility;
  WhileStatement;
  WithAttribList;
  WithStatement;
  Xor4b;
  Xor;
  BSTRING;
  BSTRINGMATCH;
  DOT;
  FLOATVALUE;
  HSTRING;
  HSTRINGMATCH;
  OSTRING;
  OSTRINGMATCH;
  RANGEOP;
}

@header {
package de.ugoe.cs.swe.trex.core.analyzer.rfparser.antlr3;
}

WS
  :
  (
    ' '
    | '\t'
    | '\n'
    | '\r'
    | '\r\n'
  ) { skip(); }
  ;

PORTREDIRECTSYMBOL
  :
  '->'
  ;

BEGINCHAR
  :
  '{'
  ;

ENDCHAR
  :
  '}'
  ;

ACTION
  :
  'action'
  ;

ACTIVATE
  :
  'activate'
  ;

ADDRESS
  :
  'address'
  ;

ALIVE
  :
  'alive'
  ;

ALL
  :
  'all'
  ;

ALT
  :
  'alt'
  ;

ALTSTEP
  :
  'altstep'
  ;

AND
  :
  'and'
  ;

AND4B
  :
  'and4b'
  ;

ANY
  :
  'any'
  ;

ANYTYPE
  :
  'anytype'
  ;

BITSTRING
  :
  'bitstring'
  ;

BOOLEAN
  :
  'boolean'
  ;

BREAK
  :
  'break'
  ;

CALL
  :
  'call'
  ;

CASE
  :
  'case'
  ;

CATCH
  :
  'catch'
  ;

CHARKEYWORD
  :
  'char'
  ;

CHARSTRING
  :
  'charstring'
  ;

CHECK
  :
  'check'
  ;

CLEAR
  :
  'clear'
  ;

COMPLEMENTKEYWORD
  :
  'complement'
  ;

COMPONENT
  :
  'component'
  ;

CONNECT
  :
  'connect'
  ;

CONST
  :
  'const'
  ;

CONTINUE
  :
  'continue'
  ;

CONTROL
  :
  'control'
  ;

CREATE
  :
  'create'
  ;

DEACTIVATE
  :
  'deactivate'
  ;

DEFAULT
  :
  'default'
  ;

DISCONNECT
  :
  'disconnect'
  ;

DISPLAY
  :
  'display'
  ;

DO
  :
  'do'
  ;

DONE
  :
  'done'
  ;

ELSE
  :
  'else'
  ;

ENCODE
  :
  'encode'
  ;

ENUMERATED
  :
  'enumerated'
  ;

ERROR
  :
  'error'
  ;

EXCEPT
  :
  'except'
  ;

EXCEPTION
  :
  'exception'
  ;

EXECUTE
  :
  'execute'
  ;

EXTENDS
  :
  'extends'
  ;

EXTENSION
  :
  'extension'
  ;

EXTERNAL
  :
  'external'
  ;

FAIL
  :
  'fail'
  ;

FALSE
  :
  'false'
  ;

FLOAT
  :
  'float'
  ;

FOR
  :
  'for'
  ;

FROM
  :
  'from'
  ;

FUNCTION
  :
  'function'
  ;

FRIEND
  :
  'friend'
  ;

GETCALL
  :
  'getcall'
  ;

GETREPLY
  :
  'getreply'
  ;

GETVERDICT
  :
  'getverdict'
  ;

GOTO
  :
  'goto'
  ;

GROUP
  :
  'group'
  ;

HALT
  :
  'halt'
  ;

HEXSTRING
  :
  'hexstring'
  ;

IF
  :
  'if'
  ;

IFPRESENT
  :
  'ifpresent'
  ;

IMPORT
  :
  'import'
  ;

IN
  :
  'in'
  ;

INCONC
  :
  'inconc'
  ;

INFINITY
  :
  'infinity'
  ;

INOUT
  :
  'inout'
  ;

INTEGER
  :
  'integer'
  ;

INTERLEAVE
  :
  'interleave'
  ;

KILL
  :
  'kill'
  ;

KILLED
  :
  'killed'
  ;

LABEL
  :
  'label'
  ;

LANGUAGE
  :
  'language'
  ;

LENGTH
  :
  'length'
  ;

LOG
  :
  'log'
  ;

MAP
  :
  'map'
  ;

MATCH
  :
  'match'
  ;

MESSAGE
  :
  'message'
  ;

MIXED
  :
  'mixed'
  ;

MOD
  :
  'mod'
  ;

MODIFIES
  :
  'modifies'
  ;

MODULE
  :
  'module'
  ;

MODULEPAR
  :
  'modulepar'
  ;

MTC
  :
  'mtc'
  ;

NOBLOCK
  :
  'noblock'
  ;

NONE
  :
  'none'
  ;

NOT
  :
  'not'
  ;

NOT4B
  :
  'not4b'
  ;

NOWAIT
  :
  'nowait'
  ;

NULL
  :
  'null'
  ;

OBJECTIDENTIFIERKEYWORD
  :
  'objid'
  ;

OCTETSTRING
  :
  'octetstring'
  ;

OF
  :
  'of'
  ;

OMIT
  :
  'omit'
  ;

ON
  :
  'on'
  ;

OPTIONAL
  :
  'optional'
  ;

OR
  :
  'or'
  ;

OR4B
  :
  'or4b'
  ;

OUT
  :
  'out'
  ;

OVERRIDEKEYWORD
  :
  'override'
  ;

PARAM
  :
  'param'
  ;

PASS
  :
  'pass'
  ;

PATTERNKEYWORD
  :
  'pattern'
  ;

PERMUTATION
  :
  'permutation'
  ;

PORT
  :
  'port'
  ;

PRESENT
  :
  'present'
  ;

PRIVATE
  :
  'private'
  ;

PROCEDURE
  :
  'procedure'
  ;

PUBLIC
  :
  'public'
  ;

RAISE
  :
  'raise'
  ;

READ
  :
  'read'
  ;

RECEIVE
  :
  'receive'
  ;

RECORD
  :
  'record'
  ;

RECURSIVE
  :
  'recursive'
  ;

REM
  :
  'rem'
  ;

REPEAT
  :
  'repeat'
  ;

REPLY
  :
  'reply'
  ;

RETURN
  :
  'return'
  ;

RUNNING
  :
  'running'
  ;

RUNS
  :
  'runs'
  ;

SELECT
  :
  'select'
  ;

SELF
  :
  'self'
  ;

SEND
  :
  'send'
  ;

SENDER
  :
  'sender'
  ;

SET
  :
  'set'
  ;

SETVERDICT
  :
  'setverdict'
  ;

SIGNATURE
  :
  'signature'
  ;

START
  :
  'start'
  ;

STOP
  :
  'stop'
  ;

SUBSET
  :
  'subset'
  ;

SUPERSET
  :
  'superset'
  ;

SYSTEM
  :
  'system'
  ;

TEMPLATE
  :
  'template'
  ;

TESTCASE
  :
  'testcase'
  ;

TIMEOUT
  :
  'timeout'
  ;

TIMER
  :
  'timer'
  ;

TO
  :
  'to'
  ;

TRIGGER
  :
  'trigger'
  ;

TRUE
  :
  'true'
  ;

TYPE
  :
  'type'
  ;

TYPEDEFKEYWORD
  :
  'typedef'
  ;

UNION
  :
  'union'
  ;

UNIVERSAL
  :
  'universal'
  ;

UNMAP
  :
  'unmap'
  ;

VALUE
  :
  'value'
  ;

VALUEOF
  :
  'valueof'
  ;

VAR
  :
  'var'
  ;

VARIANT
  :
  'variant'
  ;

VERDICTTYPE
  :
  'verdicttype'
  ;

WHILE
  :
  'while'
  ;

WITH
  :
  'with'
  ;

XOR
  :
  'xor'
  ;

XOR4B
  :
  'xor4b'
  ;

IDENTIFIER
  :       
	(('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*)
	| PreProcessorMacro
  ;

fragment PreProcessorMacro:
	'__LINE__'
	| '__FILE__'
	| '__MODULE__'
	| '__SCOPE__'
;
        