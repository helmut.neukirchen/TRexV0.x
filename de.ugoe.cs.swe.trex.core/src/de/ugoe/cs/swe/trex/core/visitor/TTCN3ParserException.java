package de.ugoe.cs.swe.trex.core.visitor;

public class TTCN3ParserException extends Exception {

	private static final long serialVersionUID = -207414866662976520L;

	public TTCN3ParserException(String message) {
		super("TTCN-3 Parser Exception: " + message); //TODO: add tree dump
	}

}
