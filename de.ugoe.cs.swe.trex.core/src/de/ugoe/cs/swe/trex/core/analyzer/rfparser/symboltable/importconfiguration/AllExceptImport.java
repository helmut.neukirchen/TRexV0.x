package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration;

import java.util.LinkedList;
import java.util.List;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration.AbstractKindImport.KindEnum;


public class AllExceptImport extends AbstractImport {
	
	List<KindAllExceptImport> exceptionList = new LinkedList<KindAllExceptImport>();
	
	@Override
	public boolean isAllowed(Symbol s, KindEnum kind) {
		boolean result = true;
		
		for (int i=0; i < exceptionList.size(); i++) {
			KindAllExceptImport k = exceptionList.get(i);
			if (k.getType() != kind)
				continue;
			
			if (k.getMode() == KindAllExceptImport.ModesEnum.all) {
				return false;
			}
			
			if (k.getMode() == KindAllExceptImport.ModesEnum.except) {
				if (k.hasName(s.getName()))
					return false;
			}
		}
				
		return result;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		
		result.append("AllExceptImport - Exceptions (" + exceptionList.size() + "): ");
		
		for (int i=0; i < exceptionList.size(); i++) {
			AbstractKindImport kindImp = exceptionList.get(i);
			if (kindImp != null)
				result.append(kindImp.toString());
		}
		
		return result.toString();
	}

	public List<KindAllExceptImport> getExceptionList() {
		return exceptionList;
	}

	public void setExceptionList(List<KindAllExceptImport> exceptionList) {
		this.exceptionList = exceptionList;
	}

}
