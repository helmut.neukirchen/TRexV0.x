package de.ugoe.cs.swe.trex.core.formatter;

import antlr.Token;

public interface ITokenFilter {
	public Token filterToken(Token t);
}
