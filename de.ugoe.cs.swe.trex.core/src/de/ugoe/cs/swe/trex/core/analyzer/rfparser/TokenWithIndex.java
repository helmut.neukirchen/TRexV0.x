package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import antlr.CommonToken;

public class TokenWithIndex extends CommonToken {
	int index;
	boolean marked = false;
	int offset = 0;
	private TokenStreamTracker tracker;

	public int getOffset() {
		return this.offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public void setIndex(int i) {
		this.index = i;
	}

	public int getIndex() {
		return this.index;
	}

	@Override
	public String toString() {
		return "[" + index + ":\"" + getText() + "\",<" + getType() + ">,line="
				+ line + ",col=" + col + ",ofs=" + getOffset() + "]\n";
	}

	public boolean isComment() {
		return (getText().matches("/\\*[^/\\*]*\\*/") || getText().matches(
				"//.*"));
	}

	public boolean isMarked() {
		return marked;
	}

	public void setMarked(boolean marked) {
		this.marked = marked;
	}

	public TokenStreamTracker getTracker() {
		return tracker;
	}

	public void setTracker(TokenStreamTracker tracker) {
		this.tracker = tracker;
	}

}
