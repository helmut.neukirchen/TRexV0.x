package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration;

import java.util.LinkedList;
import java.util.List;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;


public class GroupExceptImport extends AbstractKindImport {
	private String groupName;

	private List<KindAllExceptImport> exceptionList = new LinkedList<KindAllExceptImport>();

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void addForbiddenKind(KindAllExceptImport k) {
		exceptionList.add(k);
	}

	@Override
	public boolean isAllowed(Symbol s, KindEnum kind) {
		if (s.getGroupName() == null)
			return false;
		
		if (!(getGroupName().equals(s.getGroupName())))
			return false;
		
		for (int i=0; i < exceptionList.size(); i++) {
			KindAllExceptImport k = exceptionList.get(i);
			
			if (k.getMode() == KindAllExceptImport.ModesEnum.all) {
				if (kind == k.getType())
					return false;
			}
			
			if (k.getMode() == KindAllExceptImport.ModesEnum.except) {
				if ((k.hasName(s.getName())) && (kind == k.getType()))
					return false;
			}

		}
		
		return true;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();

		result.append("GroupExceptImport: ");
		result.append("Group Name: " + getGroupName() + " - ");
		result.append("ExceptionList: ");

		for (int i = 0; i < exceptionList.size(); i++) {
			AbstractKindImport kindImp = exceptionList.get(i);
			result.append(kindImp.toString());
		}

		return result.toString();
	}

	public List<KindAllExceptImport> getExceptionList() {
		return exceptionList;
	}

	public void setExceptionList(List<KindAllExceptImport> exceptionList) {
		this.exceptionList = exceptionList;
	}

}
