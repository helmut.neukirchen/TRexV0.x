package de.ugoe.cs.swe.trex.core.misc;

import java.util.LinkedList;
import java.util.List;

public class RangeMapValue<K> {
	Range range = null;

	List<RangeValue<K>> valueList = new LinkedList<RangeValue<K>>();
	
	public Range getRange() {
		return range;
	}

	public void setRange(Range range) {
		this.range = range;
	}

	public List<RangeValue<K>> getValueList() {
		return valueList;
	}

	public void setValueList(List<RangeValue<K>> valueList) {
		this.valueList = valueList;
	}
	
	public void putValue(RangeValue<K> value, Range r) {
		valueList.add(value);
		if (range == null) {
			range = new Range(r.getFrom(), r.getTo());
		} else {
			range.setFrom(Math.min(range.getFrom(), r.getFrom()));
			range.setTo(Math.max(range.getTo(), r.getTo()));
		}
	}
	
	@Override
	public String toString() {
		return "[(RangeMapValue) " + range + " - " + valueList + "]";
	}
	
}
