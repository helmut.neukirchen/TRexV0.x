//This class has been generated automatically. Do not modify it by hand!
package de.ugoe.cs.swe.trex.core.visitor;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
//import de.ugoe.cs.swe.t3simexec.exceptions.TTCN3BehaviorException;
import de.ugoe.cs.swe.trex.core.visitor.TTCN3BehaviorException;

public abstract class AbstractVisitor {

    public int visitedNodeCount;

    public ContinueStatus visitAddExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAddressRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAliveKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAliveOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllAltstepsWithExcept(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllCompsAllPortsSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllConnectionsSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllConstsWithExcept(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllFunctionsWithExcept(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllGroupsWithExcept(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllModuleParWithExcept(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllPortsSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllSignaturesWithExcept(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllTemplsWithExcept(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllTestcasesWithExcept(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllTypesWithExcept(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllWithExcepts(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltstepInstance(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltstepLocalDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltstepLocalDefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltstepRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltstepRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAndExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAnyTypeKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBitAndExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBitNotExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBitOrExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBitXorExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBreakStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCaseKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCheckPortOpsPresent(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCompenentIdentifierOrLiteral(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitComponentIdentifierOrLiteral(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitComponentReferenceOrLiteral(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitConstRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitContinueStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDefaultKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDerivedRefWithParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEqualExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptAltstepSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptConstSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptElement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptFunctionSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptGroupRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptGroupSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptModuleParSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptSignatureSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptTemplateSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptTestcaseSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptTypeDefSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptsDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExtendsKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFloatDotNotation(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFloatENotation(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFromClausePresent(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFullGroupIdentifier(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFullGroupIdentifierWithExcept(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGroupRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGroupRefListWithExcept(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportAltstepSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportModuleParSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitKillKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitKillTCStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitKilledStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLogItem(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitModuleDefinitionList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitModuleParRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMulExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNestedEnumDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNestedRecordDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNestedRecordOfDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNestedSetDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNestedSetOfDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNestedTypeDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNestedUnionDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNotExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOmitValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortRedirectSymbol(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortRedirectWithValueAndParam(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRedirectPresent(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRedirectWithParamSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRedirectWithValueAndParamSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRelExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRepeatStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRestrictedTemplate(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSelectCase(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSelectCaseBody(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSelectCaseConstruct(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSelectKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitShiftExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSignatureRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSingleConnectionSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSingleOrMultiConnectionSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSingleTimerInstance(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTempVarList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateRestriction(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTestcaseRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTimerList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTypeRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUnaryExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNameForm(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNumberForm(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNameAndNumberForm(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVariationKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDefinitiveIdentifier(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDefinitiveNumberForm(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitXorExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDefinitiveNameAndNumberForm(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitActivateOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAddOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAddressKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAddressDecl(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAddressRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAddressValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllOrSignatureList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllOrTypeList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllPort(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAllowedValues(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltConstruct(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltGuardChar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltGuardElement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltGuardElements(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltGuardList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltstepDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAltstepKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAnd4b(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAnd(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAnyKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAnyOrOmit(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAnyValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitArrayBounds(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitArrayConstExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitArrayDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitArrayDefRange(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitArrayElementConstExpressionList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitArrayElementExpressionList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitArrayElementSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitArrayElementSpecList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitArrayExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitArrayOrBitRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitArrayValueOrAttrib(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAssignment(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAssignmentChar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAssignmentList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAttribKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAttribQualifier(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAttribSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBaseTemplate(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBasicStatements(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBeginChar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBehaviourStatements(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBinOrMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBitOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBitStringKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBitStringMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBitStringValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBooleanExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBooleanKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBooleanValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCallBodyGuard(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCallBodyOps(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCallBodyStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCallBodyStatementList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCallParameters(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCallStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCallTimerValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCatchOpParameter(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCatchStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCell(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCharKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCharStringKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCharStringMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCharStringPattern(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCharStringValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCharStringValueId(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCheckParameter(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCheckStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCheckStateStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitClearStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitColon(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitComma(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCommunicationStatements(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitComplement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitComponentDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitComponentDefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitComponentElementDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitComponentId(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitComponentIdentifier(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitComponentRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitComponentType(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitComponentRefAssignment(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCompoundConstExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCompoundExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitConditionalConstruct(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitConfigParamDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitConfigSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitConfigurationOps(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitConfigurationStatements(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitConnectStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitConstDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitConstList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitConstantExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitControlStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitControlStatementOrDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitControlStatementOrDefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCreateOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCstring(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDash(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDeactivateStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDefIndentClose(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDefIndentOpen(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDefKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDefOrFieldRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDefOrFieldRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDerivedDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDerivedRegWithParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDirection(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDisconnectStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDisplayKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDoWhileStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDoneStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDot(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDoubleColon(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitElseClause(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitElseIfClause(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitElseIfClauses(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitElseStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEncodeKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEndChar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEnumDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEnumeratedValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEnumeration(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEnumerationList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEqual(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitError(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptionSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExceptionTypeList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExclamationMark(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExtConstDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExtFunctionDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExtendedFieldReference(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExtensionKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitExtraMatchingAttributes(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFactor(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFail(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFalse(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFieldConstExpressionList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFieldConstExpressionSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFieldExpressionList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFieldExpressionSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFieldReference(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFieldSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFieldSpecList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFieldValueOrAttrib(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFinal(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFloatKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFloatValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitForStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFormalPortPar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFormalTemplatePar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFormalTimerPar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFormalTypePar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFormalValuePar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFromClause(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionActualParAssignment(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFriendKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFriendModuleDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionActualPar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionActualParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionBody(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionLocalDefOrInst(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionLocalDefOrInstList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionStatementList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionFormalPar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionInstance(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionLocalDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionLocalInst(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFunctionStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGetCallStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGetLocalVerdict(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGetReplyStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGlobalModuleId(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGotoStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGroup(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGroupDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGuardOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGuardStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitHexOrMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitHexStringKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitHexStringMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitHexStringValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitHaltStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitIdentifier(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitIfPresentMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportAllSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportConstSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportElement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportFromSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportFunctionSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportGroupSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportNamedAltSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportSignatureSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportTemplateSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportTestcaseSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportTypeDefSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitImportImportSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitInLineTemplate(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitInOutParKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitInParKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitInconc(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitInfinity(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitInitial(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitIntegerKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitIntegerValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitInterleavedAction(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitInterleavedConstruct(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitInterleavedGuard(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitInterleavedGuardElement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitInterleavedGuardList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitKilledKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLParen(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLabelStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLanguageSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLengthMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLessorequal(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLessthan(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLogStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLoopConstruct(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLowerBound(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMTCOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMapParamDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMapStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMatchOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMatchingSymbol(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMessageAttribs(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMessageList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMinus(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMixedAttribs(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMixedList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMod(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitModifiesKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitModuleControlBody(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitModuleControlPart(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitModuleDefinition(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitModuleDefinitionsList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitModuleDefinitionsPart(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitModuleId(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitModulePar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitModuleParDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitModuleParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMoreorequal(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMorethan(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMultiWithAttrib(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMultiplyOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMultitypedModuleParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNamedAltActualPar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNamedAltActualParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNamedAltDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNamedAltFormalPar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNamedAltFormalParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNamedAltInstance(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNamedAltList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNamedAltRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNamedAltRefList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNamedKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNamedValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNamedValueList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNoBlockKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNone(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNot4b(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNot(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNotUsedOrExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNotUsedSymbol(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNotequals(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNowaitKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNull(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNumber(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNaN(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOctOrMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOctetStringKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOctetStringMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOctetStringValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOmitKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOpCall(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOpValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOptionalKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOr4b(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOr(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOutParKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOverrideKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitParRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitParaAssignmentList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitParamClause(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitParaSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPass(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPermutationMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPlane(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPlus(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPort(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortCallBody(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortCallOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortCatchOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortCheckOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortClearOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortDefAttribs(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortDefBody(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortElement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortGetCallOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortGetReplyOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortInstance(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortOrAll(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortOrAllOrAny(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortOrAny(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortRaiseOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortReceiveOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortRedirect(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortRedirectWithParam(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortReplyOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortSendOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortRefAssignment(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortStartOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortStopOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortTriggerOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortType(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPredefinedType(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPredefinedValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPrimary(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitProcOrType(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitProcOrTypeList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitProcedureAttribs(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitProcedureList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitProduct(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPortHaltOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPrivateKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPublicKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitQuadruple(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitQuestionMark(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRParen(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRaiseStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRange(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRangeDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRangeOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitReadTimerOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitReceiveParameter(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitReceiveStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRecordDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRecordOfDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRecordRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRecursiveKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitReferencedType(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitReferencedValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRelOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRem(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitReplyStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitReturnStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitReturnType(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRow(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRunningOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRunningTimerOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRunsOnSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSUTAction(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSUTStatements(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSelfOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSemiColon(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSendParameter(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSendStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSenderSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSetDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSetLocalVerdict(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSetOfDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitShiftOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSignature(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSignatureDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSignatureFormalPar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSignatureFormalParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSignatureList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSimpleExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSimpleExpressions(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSingleConstDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSingleExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSingleQuote(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSingleTempVarInstance(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSingleValueOrAttrib(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSingleVarInstance(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSingleValueSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSingleWithAttrib(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSlash(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSquareClose(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSquareOpen(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStartStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStartTCStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStartTimerStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStatementBlock(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStep(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStopStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStopTCStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStopTimerStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStringAnyOrOmit(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStringAnyValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStringLength(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStringMatchOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStringOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStructDefBody(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStructDefFormalPar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStructDefFormalParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStructFieldDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStructFieldRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStructOfDefBody(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitStructuredTypeDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSubExpression(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSubTypeDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSubTypeSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSubsetMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSupersetMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSystemOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSystemSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTTCN3File(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTTCN3Module(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTTCN3ModuleId(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateActualPar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateActualParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateBody(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateActualParAssignment(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateFormalPar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateFormalParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateInstanceAssignment(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateInstance(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateOps(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTemplateRefWithParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTerm(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTestcaseActualParAssignment(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTestcaseActualPar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTestcaseActualParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTestcaseDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTestcaseInstance(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTestcaseOperation(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTestcaseRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTimeoutKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTimeoutStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTimerInstance(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTimerKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTimerOps(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTimerRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTimerRefOrAll(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTimerRefAssignment(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTimerRefOrAny(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTimerStatements(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTimerValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitToClause(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTriggerStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTrue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitType(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTypeActualPar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTypeActualParList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTypeDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTypeDefIdentifier(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTypeList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTypeReference(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUnaryOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUnderscore(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUnionDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUnionDefBody(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUnionFieldDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUniversalChar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUniversalCharString(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUniversalKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUnmapParamDef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUnmapStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUpperBound(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitValueList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitValueMatchSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitValueOrAttribList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitValueOrRange(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitValueParOrTypePar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitValueReference(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitValueSpec(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitValueofOp(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVarInitialValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVarInstance(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVarList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVariableAssignment(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVariableEntry(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVariableList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVariableRef(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVerdictOps(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVerdictStatements(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVerdictTypeKeyword(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVerdictTypeValue(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVisibility(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitWhileStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitWildcardLengthMatch(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitWithAttribList(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitWithStatement(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitXor4b(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitXor(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBSTRING(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBSTRINGMATCH(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDOT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFLOATVALUE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitHSTRING(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitHSTRINGMATCH(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOSTRING(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOSTRINGMATCH(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRANGEOP(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitACTION(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitACTIVATE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitADDRESS(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitALIVE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitALL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitALT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitALTSTEP(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAND(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAND4B(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitANY(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitANYTYPE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBITSTRING(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBOOLEAN(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBREAK(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCALL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCASE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCATCH(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCHAR(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCHARSTRING(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCHECK(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCHECKSTATE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCLEAR(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCOMPLEMENT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCOMPONENT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCONNECT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCONST(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCONTINUE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCONTROL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCREATE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDEACTIVATE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDEFAULT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDISCONNECT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDISPLAY(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDO(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDONE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitELSE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitENCODE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitENUMERATED(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitERROR(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEXCEPT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEXCEPTION(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEXECUTE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEXTENDS(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEXTENSION(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEXTERNAL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFAIL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFALSE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFLOAT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFOR(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFROM(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFUNCTION(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitFRIEND(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGETCALL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGETREPLY(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGETVERDICT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGOTO(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGROUP(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitHALT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitHEXSTRING(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitIF(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitIFPRESENT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitIMPORT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitIN(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitINCONC(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitINFINITY(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitINOUT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitINTEGER(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitINTERLEAVE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitKILL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitKILLED(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLABEL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLANGUAGE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLENGTH(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLOG(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMAP(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMATCH(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMESSAGE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMIXED(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMOD(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMODIFIES(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMODULE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMODULEPAR(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMTC(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNAN(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNOBLOCK(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNONE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNOT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNOT4B(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNOWAIT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNULL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOBJID(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOCTETSTRING(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOF(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOMIT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitON(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOPTIONAL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOR(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOR4B(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOUT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOVERRIDE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPARAM(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPASS(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPATTERN(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPERMUTATION(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPORT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPRESENT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPRIVATE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPROCEDURE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPUBLIC(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRAISE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitREAD(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRECEIVE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRECORD(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRECURSIVE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitREM(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitREPEAT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitREPLY(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRETURN(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRUNNING(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRUNS(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSELECT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSELF(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSEND(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSENDER(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSET(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSETVERDICT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSIGNATURE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSTART(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSTOP(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSUBSET(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSUPERSET(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSYSTEM(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTEMPLATE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTESTCASE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTIMEOUT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTIMER(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTO(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTRIGGER(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTRUE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitTYPE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUNION(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUNIVERSAL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUNMAP(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVALUE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVAR(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVARIANT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitVERDICTTYPE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitWHILE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitWITH(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitXOR(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitXOR4B(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitWS(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSL_COMMENT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitML_COMMENT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitIDENTIFIER(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitASSIGNMENTCHAR(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPORTREDIRECTSYMBOL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBEGINCHAR(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCSTRING(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSTRING(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNUMBER(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEXPONENT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSEMICOLON(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCOMMA(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDOUBLECOLON(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitCOLON(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUNDERSCORE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitENDCHAR(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSQUAREOPEN(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSQUARECLOSE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLPAREN(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitRPAREN(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLESSTHAN(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMORETHAN(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNOTEQUALS(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMOREOREQUAL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLESSOREQUAL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEQUAL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPLUS(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitMINUS(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSTAR(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSLASH(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSINGLEQUOTE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDOUBLEQUOTE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitQUESTIONMARK(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitEXCLAMATIONMARK(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSHIFTLEFT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSHIFTRIGHT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitROTATELEFT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitROTATERIGHT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSTRINGANYVALUE(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSTRINGANYOROMIT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitQUOTESYMBOL(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSTRINGOP(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitZero(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNonzeronum(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNonzeronumber(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitDecimalnumber(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitNum(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitBIN(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitHEX(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOCT(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitChar(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitGeneralTextCharacter(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitAlphanumeric(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLetter(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitUppercaseLetter(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitLowercaseLetter(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSpecial(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOtherSpecial(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitOtherCharacter(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitSpace(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}
    public ContinueStatus visitPreProcessorMacro(LocationAST node) throws TTCN3BehaviorException {return ContinueStatus.getInstance(true,true);}

    public ContinueStatus visit(LocationAST node) throws TTCN3BehaviorException {
        visitedNodeCount++;
        ContinueStatus continueStatus;
        switch(node.getType()) {
            case TTCN3ParserTokenTypes.AddExpression:
                            continueStatus=visitAddExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AddressRefList:
                            continueStatus=visitAddressRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AliveKeyword:
                            continueStatus=visitAliveKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AliveOp:
                            continueStatus=visitAliveOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllAltstepsWithExcept:
                            continueStatus=visitAllAltstepsWithExcept(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllCompsAllPortsSpec:
                            continueStatus=visitAllCompsAllPortsSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllConnectionsSpec:
                            continueStatus=visitAllConnectionsSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllConstsWithExcept:
                            continueStatus=visitAllConstsWithExcept(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllFunctionsWithExcept:
                            continueStatus=visitAllFunctionsWithExcept(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllGroupsWithExcept:
                            continueStatus=visitAllGroupsWithExcept(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllModuleParWithExcept:
                            continueStatus=visitAllModuleParWithExcept(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllPortsSpec:
                            continueStatus=visitAllPortsSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllSignaturesWithExcept:
                            continueStatus=visitAllSignaturesWithExcept(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllTemplsWithExcept:
                            continueStatus=visitAllTemplsWithExcept(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllTestcasesWithExcept:
                            continueStatus=visitAllTestcasesWithExcept(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllTypesWithExcept:
                            continueStatus=visitAllTypesWithExcept(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllWithExcepts:
                            continueStatus=visitAllWithExcepts(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltstepInstance:
                            continueStatus=visitAltstepInstance(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltstepLocalDef:
                            continueStatus=visitAltstepLocalDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltstepLocalDefList:
                            continueStatus=visitAltstepLocalDefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltstepRef:
                            continueStatus=visitAltstepRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltstepRefList:
                            continueStatus=visitAltstepRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AndExpression:
                            continueStatus=visitAndExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AnyTypeKeyword:
                            continueStatus=visitAnyTypeKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllRef:
                            continueStatus=visitAllRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BitAndExpression:
                            continueStatus=visitBitAndExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BitNotExpression:
                            continueStatus=visitBitNotExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BitOrExpression:
                            continueStatus=visitBitOrExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BitXorExpression:
                            continueStatus=visitBitXorExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BreakStatement:
                            continueStatus=visitBreakStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CaseKeyword:
                            continueStatus=visitCaseKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CheckPortOpsPresent:
                            continueStatus=visitCheckPortOpsPresent(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CompenentIdentifierOrLiteral:
                            continueStatus=visitCompenentIdentifierOrLiteral(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ComponentIdentifierOrLiteral:
                            continueStatus=visitComponentIdentifierOrLiteral(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ComponentReferenceOrLiteral:
                            continueStatus=visitComponentReferenceOrLiteral(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ConstRefList:
                            continueStatus=visitConstRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ContinueStatement:
                            continueStatus=visitContinueStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DefaultKeyword:
                            continueStatus=visitDefaultKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DerivedRefWithParList:
                            continueStatus=visitDerivedRefWithParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EqualExpression:
                            continueStatus=visitEqualExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptAltstepSpec:
                            continueStatus=visitExceptAltstepSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptConstSpec:
                            continueStatus=visitExceptConstSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptElement:
                            continueStatus=visitExceptElement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptFunctionSpec:
                            continueStatus=visitExceptFunctionSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptGroupRefList:
                            continueStatus=visitExceptGroupRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptGroupSpec:
                            continueStatus=visitExceptGroupSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptModuleParSpec:
                            continueStatus=visitExceptModuleParSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptSignatureSpec:
                            continueStatus=visitExceptSignatureSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptSpec:
                            continueStatus=visitExceptSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptTemplateSpec:
                            continueStatus=visitExceptTemplateSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptTestcaseSpec:
                            continueStatus=visitExceptTestcaseSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptTypeDefSpec:
                            continueStatus=visitExceptTypeDefSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptsDef:
                            continueStatus=visitExceptsDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExtendsKeyword:
                            continueStatus=visitExtendsKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FloatDotNotation:
                            continueStatus=visitFloatDotNotation(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FloatENotation:
                            continueStatus=visitFloatENotation(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FromClausePresent:
                            continueStatus=visitFromClausePresent(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FullGroupIdentifier:
                            continueStatus=visitFullGroupIdentifier(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FullGroupIdentifierWithExcept:
                            continueStatus=visitFullGroupIdentifierWithExcept(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionRefList:
                            continueStatus=visitFunctionRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GroupRefList:
                            continueStatus=visitGroupRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GroupRefListWithExcept:
                            continueStatus=visitGroupRefListWithExcept(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportAltstepSpec:
                            continueStatus=visitImportAltstepSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportModuleParSpec:
                            continueStatus=visitImportModuleParSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.KillKeyword:
                            continueStatus=visitKillKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.KillTCStatement:
                            continueStatus=visitKillTCStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.KilledStatement:
                            continueStatus=visitKilledStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LogItem:
                            continueStatus=visitLogItem(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ModuleDefinitionList:
                            continueStatus=visitModuleDefinitionList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ModuleParRefList:
                            continueStatus=visitModuleParRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MulExpression:
                            continueStatus=visitMulExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NestedEnumDef:
                            continueStatus=visitNestedEnumDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NestedRecordDef:
                            continueStatus=visitNestedRecordDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NestedRecordOfDef:
                            continueStatus=visitNestedRecordOfDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NestedSetDef:
                            continueStatus=visitNestedSetDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NestedSetOfDef:
                            continueStatus=visitNestedSetOfDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NestedTypeDef:
                            continueStatus=visitNestedTypeDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NestedUnionDef:
                            continueStatus=visitNestedUnionDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NotExpression:
                            continueStatus=visitNotExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OmitValue:
                            continueStatus=visitOmitValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortRedirectSymbol:
                            continueStatus=visitPortRedirectSymbol(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortRedirectWithValueAndParam:
                            continueStatus=visitPortRedirectWithValueAndParam(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RedirectPresent:
                            continueStatus=visitRedirectPresent(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RedirectWithParamSpec:
                            continueStatus=visitRedirectWithParamSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RedirectWithValueAndParamSpec:
                            continueStatus=visitRedirectWithValueAndParamSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RelExpression:
                            continueStatus=visitRelExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RepeatStatement:
                            continueStatus=visitRepeatStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RestrictedTemplate:
                            continueStatus=visitRestrictedTemplate(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SelectCase:
                            continueStatus=visitSelectCase(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SelectCaseBody:
                            continueStatus=visitSelectCaseBody(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SelectCaseConstruct:
                            continueStatus=visitSelectCaseConstruct(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SelectKeyword:
                            continueStatus=visitSelectKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ShiftExpression:
                            continueStatus=visitShiftExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SignatureRefList:
                            continueStatus=visitSignatureRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SingleConnectionSpec:
                            continueStatus=visitSingleConnectionSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SingleOrMultiConnectionSpec:
                            continueStatus=visitSingleOrMultiConnectionSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SingleTimerInstance:
                            continueStatus=visitSingleTimerInstance(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TempVarList:
                            continueStatus=visitTempVarList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateRefList:
                            continueStatus=visitTemplateRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateRestriction:
                            continueStatus=visitTemplateRestriction(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TestcaseRefList:
                            continueStatus=visitTestcaseRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TimerList:
                            continueStatus=visitTimerList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TypeRefList:
                            continueStatus=visitTypeRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UnaryExpression:
                            continueStatus=visitUnaryExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NameForm:
                            continueStatus=visitNameForm(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NumberForm:
                            continueStatus=visitNumberForm(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NameAndNumberForm:
                            continueStatus=visitNameAndNumberForm(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VariationKeyword:
                            continueStatus=visitVariationKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DefinitiveIdentifier:
                            continueStatus=visitDefinitiveIdentifier(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DefinitiveNumberForm:
                            continueStatus=visitDefinitiveNumberForm(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.XorExpression:
                            continueStatus=visitXorExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DefinitiveNameAndNumberForm:
                            continueStatus=visitDefinitiveNameAndNumberForm(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ActivateOp:
                            continueStatus=visitActivateOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AddOp:
                            continueStatus=visitAddOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AddressKeyword:
                            continueStatus=visitAddressKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AddressDecl:
                            continueStatus=visitAddressDecl(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AddressRef:
                            continueStatus=visitAddressRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AddressValue:
                            continueStatus=visitAddressValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllKeyword:
                            continueStatus=visitAllKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllOrSignatureList:
                            continueStatus=visitAllOrSignatureList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllOrTypeList:
                            continueStatus=visitAllOrTypeList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllPort:
                            continueStatus=visitAllPort(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AllowedValues:
                            continueStatus=visitAllowedValues(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltConstruct:
                            continueStatus=visitAltConstruct(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltGuardChar:
                            continueStatus=visitAltGuardChar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltGuardElement:
                            continueStatus=visitAltGuardElement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltGuardElements:
                            continueStatus=visitAltGuardElements(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltGuardList:
                            continueStatus=visitAltGuardList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltKeyword:
                            continueStatus=visitAltKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltstepDef:
                            continueStatus=visitAltstepDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AltstepKeyword:
                            continueStatus=visitAltstepKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.And4b:
                            continueStatus=visitAnd4b(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.And:
                            continueStatus=visitAnd(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AnyKeyword:
                            continueStatus=visitAnyKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AnyOrOmit:
                            continueStatus=visitAnyOrOmit(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AnyValue:
                            continueStatus=visitAnyValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ArrayBounds:
                            continueStatus=visitArrayBounds(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ArrayConstExpression:
                            continueStatus=visitArrayConstExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ArrayDef:
                            continueStatus=visitArrayDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ArrayDefRange:
                            continueStatus=visitArrayDefRange(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ArrayElementConstExpressionList:
                            continueStatus=visitArrayElementConstExpressionList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ArrayElementExpressionList:
                            continueStatus=visitArrayElementExpressionList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ArrayElementSpec:
                            continueStatus=visitArrayElementSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ArrayElementSpecList:
                            continueStatus=visitArrayElementSpecList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ArrayExpression:
                            continueStatus=visitArrayExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ArrayOrBitRef:
                            continueStatus=visitArrayOrBitRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ArrayValueOrAttrib:
                            continueStatus=visitArrayValueOrAttrib(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Assignment:
                            continueStatus=visitAssignment(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AssignmentChar:
                            continueStatus=visitAssignmentChar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AssignmentList:
                            continueStatus=visitAssignmentList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AttribKeyword:
                            continueStatus=visitAttribKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AttribQualifier:
                            continueStatus=visitAttribQualifier(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AttribSpec:
                            continueStatus=visitAttribSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BaseTemplate:
                            continueStatus=visitBaseTemplate(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BasicStatements:
                            continueStatus=visitBasicStatements(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BeginChar:
                            continueStatus=visitBeginChar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BehaviourStatements:
                            continueStatus=visitBehaviourStatements(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BinOrMatch:
                            continueStatus=visitBinOrMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BitOp:
                            continueStatus=visitBitOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BitStringKeyword:
                            continueStatus=visitBitStringKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BitStringMatch:
                            continueStatus=visitBitStringMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BitStringValue:
                            continueStatus=visitBitStringValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BooleanExpression:
                            continueStatus=visitBooleanExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BooleanKeyword:
                            continueStatus=visitBooleanKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BooleanValue:
                            continueStatus=visitBooleanValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CallBodyGuard:
                            continueStatus=visitCallBodyGuard(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CallBodyOps:
                            continueStatus=visitCallBodyOps(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CallBodyStatement:
                            continueStatus=visitCallBodyStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CallBodyStatementList:
                            continueStatus=visitCallBodyStatementList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CallParameters:
                            continueStatus=visitCallParameters(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CallStatement:
                            continueStatus=visitCallStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CallTimerValue:
                            continueStatus=visitCallTimerValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CatchOpParameter:
                            continueStatus=visitCatchOpParameter(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CatchStatement:
                            continueStatus=visitCatchStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Cell:
                            continueStatus=visitCell(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CharKeyword:
                            continueStatus=visitCharKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CharStringKeyword:
                            continueStatus=visitCharStringKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CharStringMatch:
                            continueStatus=visitCharStringMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CharStringPattern:
                            continueStatus=visitCharStringPattern(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CharStringValue:
                            continueStatus=visitCharStringValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CharStringValueId:
                            continueStatus=visitCharStringValueId(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CheckParameter:
                            continueStatus=visitCheckParameter(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CheckStatement:
                            continueStatus=visitCheckStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CheckStateStatement:
                            continueStatus=visitCheckStateStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ClearStatement:
                            continueStatus=visitClearStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Colon:
                            continueStatus=visitColon(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Comma:
                            continueStatus=visitComma(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CommunicationStatements:
                            continueStatus=visitCommunicationStatements(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Complement:
                            continueStatus=visitComplement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ComponentDef:
                            continueStatus=visitComponentDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ComponentDefList:
                            continueStatus=visitComponentDefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ComponentElementDef:
                            continueStatus=visitComponentElementDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ComponentId:
                            continueStatus=visitComponentId(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ComponentIdentifier:
                            continueStatus=visitComponentIdentifier(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ComponentRef:
                            continueStatus=visitComponentRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ComponentType:
                            continueStatus=visitComponentType(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ComponentRefAssignment:
                            continueStatus=visitComponentRefAssignment(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CompoundConstExpression:
                            continueStatus=visitCompoundConstExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CompoundExpression:
                            continueStatus=visitCompoundExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ConditionalConstruct:
                            continueStatus=visitConditionalConstruct(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ConfigParamDef:
                            continueStatus=visitConfigParamDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ConfigSpec:
                            continueStatus=visitConfigSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ConfigurationOps:
                            continueStatus=visitConfigurationOps(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ConfigurationStatements:
                            continueStatus=visitConfigurationStatements(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ConnectStatement:
                            continueStatus=visitConnectStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ConstDef:
                            continueStatus=visitConstDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ConstList:
                            continueStatus=visitConstList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ConstantExpression:
                            continueStatus=visitConstantExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ControlStatement:
                            continueStatus=visitControlStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ControlStatementOrDef:
                            continueStatus=visitControlStatementOrDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ControlStatementOrDefList:
                            continueStatus=visitControlStatementOrDefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CreateOp:
                            continueStatus=visitCreateOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Cstring:
                            continueStatus=visitCstring(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Dash:
                            continueStatus=visitDash(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DeactivateStatement:
                            continueStatus=visitDeactivateStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DefIndentClose:
                            continueStatus=visitDefIndentClose(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DefIndentOpen:
                            continueStatus=visitDefIndentOpen(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DefKeyword:
                            continueStatus=visitDefKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DefOrFieldRef:
                            continueStatus=visitDefOrFieldRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DefOrFieldRefList:
                            continueStatus=visitDefOrFieldRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DerivedDef:
                            continueStatus=visitDerivedDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DerivedRegWithParList:
                            continueStatus=visitDerivedRegWithParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Direction:
                            continueStatus=visitDirection(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DisconnectStatement:
                            continueStatus=visitDisconnectStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DisplayKeyword:
                            continueStatus=visitDisplayKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DoWhileStatement:
                            continueStatus=visitDoWhileStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DoneStatement:
                            continueStatus=visitDoneStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Dot:
                            continueStatus=visitDot(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DoubleColon:
                            continueStatus=visitDoubleColon(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ElseClause:
                            continueStatus=visitElseClause(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ElseIfClause:
                            continueStatus=visitElseIfClause(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ElseIfClauses:
                            continueStatus=visitElseIfClauses(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ElseStatement:
                            continueStatus=visitElseStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EncodeKeyword:
                            continueStatus=visitEncodeKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EndChar:
                            continueStatus=visitEndChar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EnumDef:
                            continueStatus=visitEnumDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EnumeratedValue:
                            continueStatus=visitEnumeratedValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Enumeration:
                            continueStatus=visitEnumeration(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EnumerationList:
                            continueStatus=visitEnumerationList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Equal:
                            continueStatus=visitEqual(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Error:
                            continueStatus=visitError(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptionSpec:
                            continueStatus=visitExceptionSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExceptionTypeList:
                            continueStatus=visitExceptionTypeList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExclamationMark:
                            continueStatus=visitExclamationMark(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Expression:
                            continueStatus=visitExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExtConstDef:
                            continueStatus=visitExtConstDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExtFunctionDef:
                            continueStatus=visitExtFunctionDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExtendedFieldReference:
                            continueStatus=visitExtendedFieldReference(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExtensionKeyword:
                            continueStatus=visitExtensionKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ExtraMatchingAttributes:
                            continueStatus=visitExtraMatchingAttributes(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Factor:
                            continueStatus=visitFactor(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Fail:
                            continueStatus=visitFail(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.False:
                            continueStatus=visitFalse(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FieldConstExpressionList:
                            continueStatus=visitFieldConstExpressionList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FieldConstExpressionSpec:
                            continueStatus=visitFieldConstExpressionSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FieldExpressionList:
                            continueStatus=visitFieldExpressionList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FieldExpressionSpec:
                            continueStatus=visitFieldExpressionSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FieldReference:
                            continueStatus=visitFieldReference(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FieldSpec:
                            continueStatus=visitFieldSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FieldSpecList:
                            continueStatus=visitFieldSpecList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FieldValueOrAttrib:
                            continueStatus=visitFieldValueOrAttrib(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Final:
                            continueStatus=visitFinal(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FloatKeyword:
                            continueStatus=visitFloatKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FloatValue:
                            continueStatus=visitFloatValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ForStatement:
                            continueStatus=visitForStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FormalPortPar:
                            continueStatus=visitFormalPortPar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FormalTemplatePar:
                            continueStatus=visitFormalTemplatePar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FormalTimerPar:
                            continueStatus=visitFormalTimerPar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FormalTypePar:
                            continueStatus=visitFormalTypePar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FormalValuePar:
                            continueStatus=visitFormalValuePar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FromClause:
                            continueStatus=visitFromClause(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionActualParAssignment:
                            continueStatus=visitFunctionActualParAssignment(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FriendKeyword:
                            continueStatus=visitFriendKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FriendModuleDef:
                            continueStatus=visitFriendModuleDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionActualPar:
                            continueStatus=visitFunctionActualPar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionActualParList:
                            continueStatus=visitFunctionActualParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionBody:
                            continueStatus=visitFunctionBody(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionDef:
                            continueStatus=visitFunctionDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionLocalDefOrInst:
                            continueStatus=visitFunctionLocalDefOrInst(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionLocalDefOrInstList:
                            continueStatus=visitFunctionLocalDefOrInstList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionStatementList:
                            continueStatus=visitFunctionStatementList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionFormalPar:
                            continueStatus=visitFunctionFormalPar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionInstance:
                            continueStatus=visitFunctionInstance(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionLocalDef:
                            continueStatus=visitFunctionLocalDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionLocalInst:
                            continueStatus=visitFunctionLocalInst(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionRef:
                            continueStatus=visitFunctionRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FunctionStatement:
                            continueStatus=visitFunctionStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GetCallStatement:
                            continueStatus=visitGetCallStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GetLocalVerdict:
                            continueStatus=visitGetLocalVerdict(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GetReplyStatement:
                            continueStatus=visitGetReplyStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GlobalModuleId:
                            continueStatus=visitGlobalModuleId(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GotoStatement:
                            continueStatus=visitGotoStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Group:
                            continueStatus=visitGroup(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GroupDef:
                            continueStatus=visitGroupDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GuardOp:
                            continueStatus=visitGuardOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GuardStatement:
                            continueStatus=visitGuardStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.HexOrMatch:
                            continueStatus=visitHexOrMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.HexStringKeyword:
                            continueStatus=visitHexStringKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.HexStringMatch:
                            continueStatus=visitHexStringMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.HexStringValue:
                            continueStatus=visitHexStringValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.HaltStatement:
                            continueStatus=visitHaltStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Identifier:
                            continueStatus=visitIdentifier(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.IfPresentMatch:
                            continueStatus=visitIfPresentMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportAllSpec:
                            continueStatus=visitImportAllSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportConstSpec:
                            continueStatus=visitImportConstSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportDef:
                            continueStatus=visitImportDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportElement:
                            continueStatus=visitImportElement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportFromSpec:
                            continueStatus=visitImportFromSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportFunctionSpec:
                            continueStatus=visitImportFunctionSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportGroupSpec:
                            continueStatus=visitImportGroupSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportNamedAltSpec:
                            continueStatus=visitImportNamedAltSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportSignatureSpec:
                            continueStatus=visitImportSignatureSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportSpec:
                            continueStatus=visitImportSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportTemplateSpec:
                            continueStatus=visitImportTemplateSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportTestcaseSpec:
                            continueStatus=visitImportTestcaseSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportTypeDefSpec:
                            continueStatus=visitImportTypeDefSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ImportImportSpec:
                            continueStatus=visitImportImportSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.InLineTemplate:
                            continueStatus=visitInLineTemplate(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.InOutParKeyword:
                            continueStatus=visitInOutParKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.InParKeyword:
                            continueStatus=visitInParKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Inconc:
                            continueStatus=visitInconc(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Infinity:
                            continueStatus=visitInfinity(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Initial:
                            continueStatus=visitInitial(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.IntegerKeyword:
                            continueStatus=visitIntegerKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.IntegerValue:
                            continueStatus=visitIntegerValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.InterleavedAction:
                            continueStatus=visitInterleavedAction(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.InterleavedConstruct:
                            continueStatus=visitInterleavedConstruct(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.InterleavedGuard:
                            continueStatus=visitInterleavedGuard(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.InterleavedGuardElement:
                            continueStatus=visitInterleavedGuardElement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.InterleavedGuardList:
                            continueStatus=visitInterleavedGuardList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.KilledKeyword:
                            continueStatus=visitKilledKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LParen:
                            continueStatus=visitLParen(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LabelStatement:
                            continueStatus=visitLabelStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LanguageSpec:
                            continueStatus=visitLanguageSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LengthMatch:
                            continueStatus=visitLengthMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Lessorequal:
                            continueStatus=visitLessorequal(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Lessthan:
                            continueStatus=visitLessthan(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LogStatement:
                            continueStatus=visitLogStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LoopConstruct:
                            continueStatus=visitLoopConstruct(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LowerBound:
                            continueStatus=visitLowerBound(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MTCOp:
                            continueStatus=visitMTCOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MapParamDef:
                            continueStatus=visitMapParamDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MapStatement:
                            continueStatus=visitMapStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MatchOp:
                            continueStatus=visitMatchOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MatchingSymbol:
                            continueStatus=visitMatchingSymbol(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MessageAttribs:
                            continueStatus=visitMessageAttribs(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MessageList:
                            continueStatus=visitMessageList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Minus:
                            continueStatus=visitMinus(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MixedAttribs:
                            continueStatus=visitMixedAttribs(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MixedList:
                            continueStatus=visitMixedList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Mod:
                            continueStatus=visitMod(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ModifiesKeyword:
                            continueStatus=visitModifiesKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ModuleControlBody:
                            continueStatus=visitModuleControlBody(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ModuleControlPart:
                            continueStatus=visitModuleControlPart(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ModuleDefinition:
                            continueStatus=visitModuleDefinition(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ModuleDefinitionsList:
                            continueStatus=visitModuleDefinitionsList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ModuleDefinitionsPart:
                            continueStatus=visitModuleDefinitionsPart(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ModuleId:
                            continueStatus=visitModuleId(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ModulePar:
                            continueStatus=visitModulePar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ModuleParDef:
                            continueStatus=visitModuleParDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ModuleParList:
                            continueStatus=visitModuleParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Moreorequal:
                            continueStatus=visitMoreorequal(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Morethan:
                            continueStatus=visitMorethan(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MultiWithAttrib:
                            continueStatus=visitMultiWithAttrib(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MultiplyOp:
                            continueStatus=visitMultiplyOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MultitypedModuleParList:
                            continueStatus=visitMultitypedModuleParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NamedAltActualPar:
                            continueStatus=visitNamedAltActualPar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NamedAltActualParList:
                            continueStatus=visitNamedAltActualParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NamedAltDef:
                            continueStatus=visitNamedAltDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NamedAltFormalPar:
                            continueStatus=visitNamedAltFormalPar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NamedAltFormalParList:
                            continueStatus=visitNamedAltFormalParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NamedAltInstance:
                            continueStatus=visitNamedAltInstance(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NamedAltList:
                            continueStatus=visitNamedAltList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NamedAltRef:
                            continueStatus=visitNamedAltRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NamedAltRefList:
                            continueStatus=visitNamedAltRefList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NamedKeyword:
                            continueStatus=visitNamedKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NamedValue:
                            continueStatus=visitNamedValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NamedValueList:
                            continueStatus=visitNamedValueList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NoBlockKeyword:
                            continueStatus=visitNoBlockKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.None:
                            continueStatus=visitNone(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Not4b:
                            continueStatus=visitNot4b(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Not:
                            continueStatus=visitNot(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NotUsedOrExpression:
                            continueStatus=visitNotUsedOrExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NotUsedSymbol:
                            continueStatus=visitNotUsedSymbol(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Notequals:
                            continueStatus=visitNotequals(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NowaitKeyword:
                            continueStatus=visitNowaitKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Null:
                            continueStatus=visitNull(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Number:
                            continueStatus=visitNumber(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NaN:
                            continueStatus=visitNaN(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OctOrMatch:
                            continueStatus=visitOctOrMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OctetStringKeyword:
                            continueStatus=visitOctetStringKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OctetStringMatch:
                            continueStatus=visitOctetStringMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OctetStringValue:
                            continueStatus=visitOctetStringValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OmitKeyword:
                            continueStatus=visitOmitKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OpCall:
                            continueStatus=visitOpCall(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OpValue:
                            continueStatus=visitOpValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OptionalKeyword:
                            continueStatus=visitOptionalKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Or4b:
                            continueStatus=visitOr4b(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Or:
                            continueStatus=visitOr(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OutParKeyword:
                            continueStatus=visitOutParKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OverrideKeyword:
                            continueStatus=visitOverrideKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ParRef:
                            continueStatus=visitParRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ParaAssignmentList:
                            continueStatus=visitParaAssignmentList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ParamClause:
                            continueStatus=visitParamClause(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ParaSpec:
                            continueStatus=visitParaSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Pass:
                            continueStatus=visitPass(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PermutationMatch:
                            continueStatus=visitPermutationMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Plane:
                            continueStatus=visitPlane(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Plus:
                            continueStatus=visitPlus(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Port:
                            continueStatus=visitPort(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortCallBody:
                            continueStatus=visitPortCallBody(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortCallOp:
                            continueStatus=visitPortCallOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortCatchOp:
                            continueStatus=visitPortCatchOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortCheckOp:
                            continueStatus=visitPortCheckOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortClearOp:
                            continueStatus=visitPortClearOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortDef:
                            continueStatus=visitPortDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortDefAttribs:
                            continueStatus=visitPortDefAttribs(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortDefBody:
                            continueStatus=visitPortDefBody(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortElement:
                            continueStatus=visitPortElement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortGetCallOp:
                            continueStatus=visitPortGetCallOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortGetReplyOp:
                            continueStatus=visitPortGetReplyOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortInstance:
                            continueStatus=visitPortInstance(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortKeyword:
                            continueStatus=visitPortKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortOrAll:
                            continueStatus=visitPortOrAll(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortOrAllOrAny:
                            continueStatus=visitPortOrAllOrAny(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortOrAny:
                            continueStatus=visitPortOrAny(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortRaiseOp:
                            continueStatus=visitPortRaiseOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortReceiveOp:
                            continueStatus=visitPortReceiveOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortRedirect:
                            continueStatus=visitPortRedirect(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortRedirectWithParam:
                            continueStatus=visitPortRedirectWithParam(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortRef:
                            continueStatus=visitPortRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortReplyOp:
                            continueStatus=visitPortReplyOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortSendOp:
                            continueStatus=visitPortSendOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortRefAssignment:
                            continueStatus=visitPortRefAssignment(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortSpec:
                            continueStatus=visitPortSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortStartOp:
                            continueStatus=visitPortStartOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortStopOp:
                            continueStatus=visitPortStopOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortTriggerOp:
                            continueStatus=visitPortTriggerOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortType:
                            continueStatus=visitPortType(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PredefinedType:
                            continueStatus=visitPredefinedType(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PredefinedValue:
                            continueStatus=visitPredefinedValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Primary:
                            continueStatus=visitPrimary(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ProcOrType:
                            continueStatus=visitProcOrType(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ProcOrTypeList:
                            continueStatus=visitProcOrTypeList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ProcedureAttribs:
                            continueStatus=visitProcedureAttribs(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ProcedureList:
                            continueStatus=visitProcedureList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Product:
                            continueStatus=visitProduct(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PortHaltOp:
                            continueStatus=visitPortHaltOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PrivateKeyword:
                            continueStatus=visitPrivateKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PublicKeyword:
                            continueStatus=visitPublicKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Quadruple:
                            continueStatus=visitQuadruple(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.QuestionMark:
                            continueStatus=visitQuestionMark(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RParen:
                            continueStatus=visitRParen(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RaiseStatement:
                            continueStatus=visitRaiseStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Range:
                            continueStatus=visitRange(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RangeDef:
                            continueStatus=visitRangeDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RangeOp:
                            continueStatus=visitRangeOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ReadTimerOp:
                            continueStatus=visitReadTimerOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ReceiveParameter:
                            continueStatus=visitReceiveParameter(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ReceiveStatement:
                            continueStatus=visitReceiveStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RecordDef:
                            continueStatus=visitRecordDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RecordOfDef:
                            continueStatus=visitRecordOfDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RecordRef:
                            continueStatus=visitRecordRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RecursiveKeyword:
                            continueStatus=visitRecursiveKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ReferencedType:
                            continueStatus=visitReferencedType(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ReferencedValue:
                            continueStatus=visitReferencedValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RelOp:
                            continueStatus=visitRelOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Rem:
                            continueStatus=visitRem(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ReplyStatement:
                            continueStatus=visitReplyStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ReturnStatement:
                            continueStatus=visitReturnStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ReturnType:
                            continueStatus=visitReturnType(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Row:
                            continueStatus=visitRow(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RunningOp:
                            continueStatus=visitRunningOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RunningTimerOp:
                            continueStatus=visitRunningTimerOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RunsOnSpec:
                            continueStatus=visitRunsOnSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SUTAction:
                            continueStatus=visitSUTAction(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SUTStatements:
                            continueStatus=visitSUTStatements(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SelfOp:
                            continueStatus=visitSelfOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SemiColon:
                            continueStatus=visitSemiColon(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SendParameter:
                            continueStatus=visitSendParameter(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SendStatement:
                            continueStatus=visitSendStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SenderSpec:
                            continueStatus=visitSenderSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SetDef:
                            continueStatus=visitSetDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SetLocalVerdict:
                            continueStatus=visitSetLocalVerdict(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SetOfDef:
                            continueStatus=visitSetOfDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ShiftOp:
                            continueStatus=visitShiftOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Signature:
                            continueStatus=visitSignature(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SignatureDef:
                            continueStatus=visitSignatureDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SignatureFormalPar:
                            continueStatus=visitSignatureFormalPar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SignatureFormalParList:
                            continueStatus=visitSignatureFormalParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SignatureList:
                            continueStatus=visitSignatureList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SimpleExpression:
                            continueStatus=visitSimpleExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SimpleExpressions:
                            continueStatus=visitSimpleExpressions(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SingleConstDef:
                            continueStatus=visitSingleConstDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SingleExpression:
                            continueStatus=visitSingleExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SingleQuote:
                            continueStatus=visitSingleQuote(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SingleTempVarInstance:
                            continueStatus=visitSingleTempVarInstance(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SingleValueOrAttrib:
                            continueStatus=visitSingleValueOrAttrib(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SingleVarInstance:
                            continueStatus=visitSingleVarInstance(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SingleValueSpec:
                            continueStatus=visitSingleValueSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SingleWithAttrib:
                            continueStatus=visitSingleWithAttrib(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Slash:
                            continueStatus=visitSlash(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SquareClose:
                            continueStatus=visitSquareClose(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SquareOpen:
                            continueStatus=visitSquareOpen(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Star:
                            continueStatus=visitStar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StartStatement:
                            continueStatus=visitStartStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StartTCStatement:
                            continueStatus=visitStartTCStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StartTimerStatement:
                            continueStatus=visitStartTimerStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StatementBlock:
                            continueStatus=visitStatementBlock(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Step:
                            continueStatus=visitStep(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StopStatement:
                            continueStatus=visitStopStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StopTCStatement:
                            continueStatus=visitStopTCStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StopTimerStatement:
                            continueStatus=visitStopTimerStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StringAnyOrOmit:
                            continueStatus=visitStringAnyOrOmit(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StringAnyValue:
                            continueStatus=visitStringAnyValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StringLength:
                            continueStatus=visitStringLength(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StringMatchOp:
                            continueStatus=visitStringMatchOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StringOp:
                            continueStatus=visitStringOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StructDefBody:
                            continueStatus=visitStructDefBody(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StructDefFormalPar:
                            continueStatus=visitStructDefFormalPar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StructDefFormalParList:
                            continueStatus=visitStructDefFormalParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StructFieldDef:
                            continueStatus=visitStructFieldDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StructFieldRef:
                            continueStatus=visitStructFieldRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StructOfDefBody:
                            continueStatus=visitStructOfDefBody(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.StructuredTypeDef:
                            continueStatus=visitStructuredTypeDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SubExpression:
                            continueStatus=visitSubExpression(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SubTypeDef:
                            continueStatus=visitSubTypeDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SubTypeSpec:
                            continueStatus=visitSubTypeSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SubsetMatch:
                            continueStatus=visitSubsetMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SupersetMatch:
                            continueStatus=visitSupersetMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SystemOp:
                            continueStatus=visitSystemOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SystemSpec:
                            continueStatus=visitSystemSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TTCN3File:
                            continueStatus=visitTTCN3File(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TTCN3Module:
                            continueStatus=visitTTCN3Module(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TTCN3ModuleId:
                            continueStatus=visitTTCN3ModuleId(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateActualPar:
                            continueStatus=visitTemplateActualPar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateActualParList:
                            continueStatus=visitTemplateActualParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateBody:
                            continueStatus=visitTemplateBody(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateActualParAssignment:
                            continueStatus=visitTemplateActualParAssignment(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateDef:
                            continueStatus=visitTemplateDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateFormalPar:
                            continueStatus=visitTemplateFormalPar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateFormalParList:
                            continueStatus=visitTemplateFormalParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateInstanceAssignment:
                            continueStatus=visitTemplateInstanceAssignment(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateInstance:
                            continueStatus=visitTemplateInstance(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateKeyword:
                            continueStatus=visitTemplateKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateOps:
                            continueStatus=visitTemplateOps(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateRef:
                            continueStatus=visitTemplateRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TemplateRefWithParList:
                            continueStatus=visitTemplateRefWithParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Term:
                            continueStatus=visitTerm(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TestcaseActualParAssignment:
                            continueStatus=visitTestcaseActualParAssignment(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TestcaseActualPar:
                            continueStatus=visitTestcaseActualPar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TestcaseActualParList:
                            continueStatus=visitTestcaseActualParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TestcaseDef:
                            continueStatus=visitTestcaseDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TestcaseInstance:
                            continueStatus=visitTestcaseInstance(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TestcaseOperation:
                            continueStatus=visitTestcaseOperation(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TestcaseRef:
                            continueStatus=visitTestcaseRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TimeoutKeyword:
                            continueStatus=visitTimeoutKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TimeoutStatement:
                            continueStatus=visitTimeoutStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TimerInstance:
                            continueStatus=visitTimerInstance(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TimerKeyword:
                            continueStatus=visitTimerKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TimerOps:
                            continueStatus=visitTimerOps(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TimerRef:
                            continueStatus=visitTimerRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TimerRefOrAll:
                            continueStatus=visitTimerRefOrAll(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TimerRefAssignment:
                            continueStatus=visitTimerRefAssignment(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TimerRefOrAny:
                            continueStatus=visitTimerRefOrAny(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TimerStatements:
                            continueStatus=visitTimerStatements(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TimerValue:
                            continueStatus=visitTimerValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ToClause:
                            continueStatus=visitToClause(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TriggerStatement:
                            continueStatus=visitTriggerStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.True:
                            continueStatus=visitTrue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Type:
                            continueStatus=visitType(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TypeActualPar:
                            continueStatus=visitTypeActualPar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TypeActualParList:
                            continueStatus=visitTypeActualParList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TypeDef:
                            continueStatus=visitTypeDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TypeDefIdentifier:
                            continueStatus=visitTypeDefIdentifier(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TypeList:
                            continueStatus=visitTypeList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TypeReference:
                            continueStatus=visitTypeReference(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UnaryOp:
                            continueStatus=visitUnaryOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Underscore:
                            continueStatus=visitUnderscore(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UnionDef:
                            continueStatus=visitUnionDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UnionDefBody:
                            continueStatus=visitUnionDefBody(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UnionFieldDef:
                            continueStatus=visitUnionFieldDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UniversalChar:
                            continueStatus=visitUniversalChar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UniversalCharString:
                            continueStatus=visitUniversalCharString(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UniversalKeyword:
                            continueStatus=visitUniversalKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UnmapParamDef:
                            continueStatus=visitUnmapParamDef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UnmapStatement:
                            continueStatus=visitUnmapStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UpperBound:
                            continueStatus=visitUpperBound(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Value:
                            continueStatus=visitValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ValueList:
                            continueStatus=visitValueList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ValueMatchSpec:
                            continueStatus=visitValueMatchSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ValueOrAttribList:
                            continueStatus=visitValueOrAttribList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ValueOrRange:
                            continueStatus=visitValueOrRange(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ValueParOrTypePar:
                            continueStatus=visitValueParOrTypePar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ValueReference:
                            continueStatus=visitValueReference(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ValueSpec:
                            continueStatus=visitValueSpec(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ValueofOp:
                            continueStatus=visitValueofOp(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VarInitialValue:
                            continueStatus=visitVarInitialValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VarInstance:
                            continueStatus=visitVarInstance(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VarList:
                            continueStatus=visitVarList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VariableAssignment:
                            continueStatus=visitVariableAssignment(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VariableEntry:
                            continueStatus=visitVariableEntry(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VariableList:
                            continueStatus=visitVariableList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VariableRef:
                            continueStatus=visitVariableRef(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VerdictOps:
                            continueStatus=visitVerdictOps(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VerdictStatements:
                            continueStatus=visitVerdictStatements(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VerdictTypeKeyword:
                            continueStatus=visitVerdictTypeKeyword(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VerdictTypeValue:
                            continueStatus=visitVerdictTypeValue(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Visibility:
                            continueStatus=visitVisibility(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.WhileStatement:
                            continueStatus=visitWhileStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.WildcardLengthMatch:
                            continueStatus=visitWildcardLengthMatch(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.WithAttribList:
                            continueStatus=visitWithAttribList(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.WithStatement:
                            continueStatus=visitWithStatement(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Xor4b:
                            continueStatus=visitXor4b(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Xor:
                            continueStatus=visitXor(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BSTRING:
                            continueStatus=visitBSTRING(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BSTRINGMATCH:
                            continueStatus=visitBSTRINGMATCH(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DOT:
                            continueStatus=visitDOT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FLOATVALUE:
                            continueStatus=visitFLOATVALUE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.HSTRING:
                            continueStatus=visitHSTRING(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.HSTRINGMATCH:
                            continueStatus=visitHSTRINGMATCH(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OSTRING:
                            continueStatus=visitOSTRING(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OSTRINGMATCH:
                            continueStatus=visitOSTRINGMATCH(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RANGEOP:
                            continueStatus=visitRANGEOP(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ACTION:
                            continueStatus=visitACTION(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ACTIVATE:
                            continueStatus=visitACTIVATE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ADDRESS:
                            continueStatus=visitADDRESS(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ALIVE:
                            continueStatus=visitALIVE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ALL:
                            continueStatus=visitALL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ALT:
                            continueStatus=visitALT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ALTSTEP:
                            continueStatus=visitALTSTEP(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AND:
                            continueStatus=visitAND(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.AND4B:
                            continueStatus=visitAND4B(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ANY:
                            continueStatus=visitANY(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ANYTYPE:
                            continueStatus=visitANYTYPE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BITSTRING:
                            continueStatus=visitBITSTRING(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BOOLEAN:
                            continueStatus=visitBOOLEAN(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BREAK:
                            continueStatus=visitBREAK(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CALL:
                            continueStatus=visitCALL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CASE:
                            continueStatus=visitCASE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CATCH:
                            continueStatus=visitCATCH(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CHAR:
                            continueStatus=visitCHAR(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CHARSTRING:
                            continueStatus=visitCHARSTRING(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CHECK:
                            continueStatus=visitCHECK(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CHECKSTATE:
                            continueStatus=visitCHECKSTATE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CLEAR:
                            continueStatus=visitCLEAR(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.COMPLEMENT:
                            continueStatus=visitCOMPLEMENT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.COMPONENT:
                            continueStatus=visitCOMPONENT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CONNECT:
                            continueStatus=visitCONNECT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CONST:
                            continueStatus=visitCONST(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CONTINUE:
                            continueStatus=visitCONTINUE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CONTROL:
                            continueStatus=visitCONTROL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CREATE:
                            continueStatus=visitCREATE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DEACTIVATE:
                            continueStatus=visitDEACTIVATE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DEFAULT:
                            continueStatus=visitDEFAULT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DISCONNECT:
                            continueStatus=visitDISCONNECT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DISPLAY:
                            continueStatus=visitDISPLAY(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DO:
                            continueStatus=visitDO(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DONE:
                            continueStatus=visitDONE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ELSE:
                            continueStatus=visitELSE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ENCODE:
                            continueStatus=visitENCODE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ENUMERATED:
                            continueStatus=visitENUMERATED(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ERROR:
                            continueStatus=visitERROR(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EXCEPT:
                            continueStatus=visitEXCEPT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EXCEPTION:
                            continueStatus=visitEXCEPTION(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EXECUTE:
                            continueStatus=visitEXECUTE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EXTENDS:
                            continueStatus=visitEXTENDS(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EXTENSION:
                            continueStatus=visitEXTENSION(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EXTERNAL:
                            continueStatus=visitEXTERNAL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FAIL:
                            continueStatus=visitFAIL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FALSE:
                            continueStatus=visitFALSE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FLOAT:
                            continueStatus=visitFLOAT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FOR:
                            continueStatus=visitFOR(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FROM:
                            continueStatus=visitFROM(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FUNCTION:
                            continueStatus=visitFUNCTION(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.FRIEND:
                            continueStatus=visitFRIEND(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GETCALL:
                            continueStatus=visitGETCALL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GETREPLY:
                            continueStatus=visitGETREPLY(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GETVERDICT:
                            continueStatus=visitGETVERDICT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GOTO:
                            continueStatus=visitGOTO(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GROUP:
                            continueStatus=visitGROUP(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.HALT:
                            continueStatus=visitHALT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.HEXSTRING:
                            continueStatus=visitHEXSTRING(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.IF:
                            continueStatus=visitIF(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.IFPRESENT:
                            continueStatus=visitIFPRESENT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.IMPORT:
                            continueStatus=visitIMPORT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.IN:
                            continueStatus=visitIN(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.INCONC:
                            continueStatus=visitINCONC(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.INFINITY:
                            continueStatus=visitINFINITY(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.INOUT:
                            continueStatus=visitINOUT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.INTEGER:
                            continueStatus=visitINTEGER(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.INTERLEAVE:
                            continueStatus=visitINTERLEAVE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.KILL:
                            continueStatus=visitKILL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.KILLED:
                            continueStatus=visitKILLED(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LABEL:
                            continueStatus=visitLABEL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LANGUAGE:
                            continueStatus=visitLANGUAGE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LENGTH:
                            continueStatus=visitLENGTH(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LOG:
                            continueStatus=visitLOG(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MAP:
                            continueStatus=visitMAP(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MATCH:
                            continueStatus=visitMATCH(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MESSAGE:
                            continueStatus=visitMESSAGE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MIXED:
                            continueStatus=visitMIXED(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MOD:
                            continueStatus=visitMOD(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MODIFIES:
                            continueStatus=visitMODIFIES(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MODULE:
                            continueStatus=visitMODULE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MODULEPAR:
                            continueStatus=visitMODULEPAR(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MTC:
                            continueStatus=visitMTC(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NAN:
                            continueStatus=visitNAN(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NOBLOCK:
                            continueStatus=visitNOBLOCK(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NONE:
                            continueStatus=visitNONE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NOT:
                            continueStatus=visitNOT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NOT4B:
                            continueStatus=visitNOT4B(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NOWAIT:
                            continueStatus=visitNOWAIT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NULL:
                            continueStatus=visitNULL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OBJID:
                            continueStatus=visitOBJID(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OCTETSTRING:
                            continueStatus=visitOCTETSTRING(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OF:
                            continueStatus=visitOF(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OMIT:
                            continueStatus=visitOMIT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ON:
                            continueStatus=visitON(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OPTIONAL:
                            continueStatus=visitOPTIONAL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OR:
                            continueStatus=visitOR(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OR4B:
                            continueStatus=visitOR4B(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OUT:
                            continueStatus=visitOUT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OVERRIDE:
                            continueStatus=visitOVERRIDE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PARAM:
                            continueStatus=visitPARAM(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PASS:
                            continueStatus=visitPASS(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PATTERN:
                            continueStatus=visitPATTERN(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PERMUTATION:
                            continueStatus=visitPERMUTATION(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PORT:
                            continueStatus=visitPORT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PRESENT:
                            continueStatus=visitPRESENT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PRIVATE:
                            continueStatus=visitPRIVATE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PROCEDURE:
                            continueStatus=visitPROCEDURE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PUBLIC:
                            continueStatus=visitPUBLIC(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RAISE:
                            continueStatus=visitRAISE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.READ:
                            continueStatus=visitREAD(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RECEIVE:
                            continueStatus=visitRECEIVE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RECORD:
                            continueStatus=visitRECORD(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RECURSIVE:
                            continueStatus=visitRECURSIVE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.REM:
                            continueStatus=visitREM(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.REPEAT:
                            continueStatus=visitREPEAT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.REPLY:
                            continueStatus=visitREPLY(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RETURN:
                            continueStatus=visitRETURN(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RUNNING:
                            continueStatus=visitRUNNING(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RUNS:
                            continueStatus=visitRUNS(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SELECT:
                            continueStatus=visitSELECT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SELF:
                            continueStatus=visitSELF(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SEND:
                            continueStatus=visitSEND(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SENDER:
                            continueStatus=visitSENDER(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SET:
                            continueStatus=visitSET(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SETVERDICT:
                            continueStatus=visitSETVERDICT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SIGNATURE:
                            continueStatus=visitSIGNATURE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.START:
                            continueStatus=visitSTART(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.STOP:
                            continueStatus=visitSTOP(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SUBSET:
                            continueStatus=visitSUBSET(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SUPERSET:
                            continueStatus=visitSUPERSET(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SYSTEM:
                            continueStatus=visitSYSTEM(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TEMPLATE:
                            continueStatus=visitTEMPLATE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TESTCASE:
                            continueStatus=visitTESTCASE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TIMEOUT:
                            continueStatus=visitTIMEOUT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TIMER:
                            continueStatus=visitTIMER(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TO:
                            continueStatus=visitTO(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TRIGGER:
                            continueStatus=visitTRIGGER(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TRUE:
                            continueStatus=visitTRUE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.TYPE:
                            continueStatus=visitTYPE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UNION:
                            continueStatus=visitUNION(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UNIVERSAL:
                            continueStatus=visitUNIVERSAL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UNMAP:
                            continueStatus=visitUNMAP(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VALUE:
                            continueStatus=visitVALUE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VAR:
                            continueStatus=visitVAR(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VARIANT:
                            continueStatus=visitVARIANT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.VERDICTTYPE:
                            continueStatus=visitVERDICTTYPE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.WHILE:
                            continueStatus=visitWHILE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.WITH:
                            continueStatus=visitWITH(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.XOR:
                            continueStatus=visitXOR(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.XOR4B:
                            continueStatus=visitXOR4B(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.WS:
                            continueStatus=visitWS(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SL_COMMENT:
                            continueStatus=visitSL_COMMENT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ML_COMMENT:
                            continueStatus=visitML_COMMENT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.IDENTIFIER:
                            continueStatus=visitIDENTIFIER(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ASSIGNMENTCHAR:
                            continueStatus=visitASSIGNMENTCHAR(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PORTREDIRECTSYMBOL:
                            continueStatus=visitPORTREDIRECTSYMBOL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BEGINCHAR:
                            continueStatus=visitBEGINCHAR(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.CSTRING:
                            continueStatus=visitCSTRING(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.STRING:
                            continueStatus=visitSTRING(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NUMBER:
                            continueStatus=visitNUMBER(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EXPONENT:
                            continueStatus=visitEXPONENT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SEMICOLON:
                            continueStatus=visitSEMICOLON(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.COMMA:
                            continueStatus=visitCOMMA(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DOUBLECOLON:
                            continueStatus=visitDOUBLECOLON(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.COLON:
                            continueStatus=visitCOLON(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UNDERSCORE:
                            continueStatus=visitUNDERSCORE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ENDCHAR:
                            continueStatus=visitENDCHAR(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SQUAREOPEN:
                            continueStatus=visitSQUAREOPEN(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SQUARECLOSE:
                            continueStatus=visitSQUARECLOSE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LPAREN:
                            continueStatus=visitLPAREN(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.RPAREN:
                            continueStatus=visitRPAREN(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LESSTHAN:
                            continueStatus=visitLESSTHAN(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MORETHAN:
                            continueStatus=visitMORETHAN(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.NOTEQUALS:
                            continueStatus=visitNOTEQUALS(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MOREOREQUAL:
                            continueStatus=visitMOREOREQUAL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LESSOREQUAL:
                            continueStatus=visitLESSOREQUAL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EQUAL:
                            continueStatus=visitEQUAL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PLUS:
                            continueStatus=visitPLUS(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.MINUS:
                            continueStatus=visitMINUS(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.STAR:
                            continueStatus=visitSTAR(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SLASH:
                            continueStatus=visitSLASH(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SINGLEQUOTE:
                            continueStatus=visitSINGLEQUOTE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.DOUBLEQUOTE:
                            continueStatus=visitDOUBLEQUOTE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.QUESTIONMARK:
                            continueStatus=visitQUESTIONMARK(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.EXCLAMATIONMARK:
                            continueStatus=visitEXCLAMATIONMARK(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SHIFTLEFT:
                            continueStatus=visitSHIFTLEFT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.SHIFTRIGHT:
                            continueStatus=visitSHIFTRIGHT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ROTATELEFT:
                            continueStatus=visitROTATELEFT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.ROTATERIGHT:
                            continueStatus=visitROTATERIGHT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.STRINGANYVALUE:
                            continueStatus=visitSTRINGANYVALUE(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.STRINGANYOROMIT:
                            continueStatus=visitSTRINGANYOROMIT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.QUOTESYMBOL:
                            continueStatus=visitQUOTESYMBOL(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.STRINGOP:
                            continueStatus=visitSTRINGOP(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Zero:
                            continueStatus=visitZero(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Nonzeronum:
                            continueStatus=visitNonzeronum(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Nonzeronumber:
                            continueStatus=visitNonzeronumber(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Decimalnumber:
                            continueStatus=visitDecimalnumber(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Num:
                            continueStatus=visitNum(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.BIN:
                            continueStatus=visitBIN(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.HEX:
                            continueStatus=visitHEX(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OCT:
                            continueStatus=visitOCT(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Char:
                            continueStatus=visitChar(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.GeneralTextCharacter:
                            continueStatus=visitGeneralTextCharacter(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Alphanumeric:
                            continueStatus=visitAlphanumeric(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Letter:
                            continueStatus=visitLetter(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.UppercaseLetter:
                            continueStatus=visitUppercaseLetter(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.LowercaseLetter:
                            continueStatus=visitLowercaseLetter(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Special:
                            continueStatus=visitSpecial(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OtherSpecial:
                            continueStatus=visitOtherSpecial(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.OtherCharacter:
                            continueStatus=visitOtherCharacter(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.Space:
                            continueStatus=visitSpace(node);
                            return continueStatus;
            case TTCN3ParserTokenTypes.PreProcessorMacro:
                            continueStatus=visitPreProcessorMacro(node);
                            return continueStatus;
        }
        return new ContinueStatus();
    }

    public abstract void init();
    public abstract void finish();
    
    public void acceptDFS(LocationAST node) throws TTCN3BehaviorException {
        init();
        searchASTDepthFirst(node);
        finish();
    }

    public void acceptBFS(LocationAST node) throws TTCN3BehaviorException {
        init();
        searchASTBreadthFirst(node);
        finish();
    }

    protected ContinueStatus searchASTDepthFirst(LocationAST node) throws TTCN3BehaviorException {
        if (node == null)
            return ContinueStatus.getInstance(true, true);

        LocationAST next = node;
        while (next != null) {
            ContinueStatus continueStatus = visit(next);
            if (!continueStatus.continueSearch)
                return ContinueStatus.getInstance(false, false);
            if (continueStatus.goDeeper)
                searchASTDepthFirst(next.getFirstChild());
            next = next.getNextSibling();
        }
        return ContinueStatus.getInstance(true, true);
    }

    protected ContinueStatus searchASTBreadthFirst(LocationAST node) throws TTCN3BehaviorException {
        if (node == null)
            return ContinueStatus.getInstance(true, true);
        
        Queue<LocationAST> open = new LinkedList<LocationAST>();
        open.add(node);
        searchASTBreadthFirst(open);
        return ContinueStatus.getInstance(true, true);
    }

    private ContinueStatus searchASTBreadthFirst(Queue<LocationAST> nodes) throws TTCN3BehaviorException {
        Queue<LocationAST> open = new LinkedList<LocationAST>();
        Iterator<LocationAST> it = nodes.iterator();
        while (it.hasNext()) {
            LocationAST item = it.next();
            while (item != null) {
                ContinueStatus continueStatus = visit(item);
                if (!continueStatus.continueSearch)
                    return ContinueStatus.getInstance(false, false);
                if ( (item.getFirstChild() != null) && (continueStatus.goDeeper) )
                    open.add(item.getFirstChild());
                item = item.getNextSibling();
            }
        }
        if (open.size() > 0)
            searchASTBreadthFirst(open);
        return ContinueStatus.getInstance(true, true);
    }

    public static class ContinueStatus {
        public boolean continueSearch = true;
        public boolean goDeeper = true;
        private static ContinueStatus truetrue = new ContinueStatus(true, true);
        private static ContinueStatus truefalse = new ContinueStatus(true, false);
        private static ContinueStatus falsetrue = new ContinueStatus(false, true);
        private static ContinueStatus falsefalse = new ContinueStatus(false, false);

        private ContinueStatus() {
            this.continueSearch = true;
            this.goDeeper = true;
        }

        private ContinueStatus(boolean continueSearch, boolean goDeeper) {
            this.continueSearch = continueSearch;
            this.goDeeper = goDeeper;
        }

        public static ContinueStatus getInstance(boolean continueSearch, boolean goDeeper) {
            if (continueSearch && goDeeper) {
                return truetrue;
            } else if (!continueSearch && goDeeper) {
                return falsetrue;
            } else if (continueSearch && !goDeeper) {
                return truefalse;
            } else {
                return falsefalse;
            }
        }
    }
    

}