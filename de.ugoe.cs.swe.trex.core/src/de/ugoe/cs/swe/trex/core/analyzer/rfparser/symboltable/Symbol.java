package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;

public class Symbol implements ITTCN3EclipseSymbol {
	private String name;

	// TODO: nomenclature is bad! scope=scope introduced by symbol,
	// surroundingScope=the actual scope when the symbol is added
	private Scope scope = null;

	private LocationAST declarationNode = null;

	private Scope surroundingScope;

	private String groupName = null;
	
	public enum VisibilityMode {visPublic, visPrivate, visFriend};
	
	private VisibilityMode visibility =  VisibilityMode.visPublic; 

	public Symbol() {
		// empty constructor
	}

	public Symbol(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public LocationAST getDeclarationNode() {
		return declarationNode;
	}

	public void setDeclarationNode(LocationAST delcarationNode) {
		this.declarationNode = delcarationNode;
	}

	public String getSymbolDescription() {
		return "<unimplemented/>";
	}

	public String getSymbolDescriptionPlain() {
		return "<unimplemented/>";
	}

	public String getSymbolDescriptionContentAssist() {
		return "<unimplemented/>";
	}

	public String toString() {
		return "symbol name: " + getName();
	}
	
	public Scope getSurroundingScope() {
		return surroundingScope;
	}

	public void setSurroundingScope(Scope surroundingScope) {
		this.surroundingScope = surroundingScope;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void setPrivate(){
		this.visibility = VisibilityMode.visPrivate;
	}

	public void setPublic(){
		this.visibility = VisibilityMode.visPublic;
	}

	public void setFriend(){
		this.visibility = VisibilityMode.visFriend;
	}
	
	public void setVisibility(VisibilityMode visibility) {
		this.visibility = visibility;
	}

	public VisibilityMode getVisibility() {
		return visibility;
	}

	public boolean isPublic() {
		boolean isPublic = false;
		if (visibility.equals(VisibilityMode.visPublic))
			isPublic = true;
		return isPublic;
	}

	public boolean isPrivate() {
		boolean isPrivate = false;
		if (visibility.equals(VisibilityMode.visPrivate))
			isPrivate = true;
		return isPrivate;
	}

	public boolean isFriend() {
		boolean isFriend = false;
		if (visibility.equals(VisibilityMode.visFriend))
			isFriend = true;
		return isFriend;
	}

	public String getVisibilityDescription(){
		StringBuffer result= new StringBuffer();;
		HTMLPrinter.getInstance().addSmallHeader(result, "Visibility:");
		if (isFriend()){
			result.append("friend");
		} else if (isPrivate()){
			result.append("private");
		} else {
			result.append("public");
		}
		result.append("<br>");
		return result.toString();
	}
	
}
