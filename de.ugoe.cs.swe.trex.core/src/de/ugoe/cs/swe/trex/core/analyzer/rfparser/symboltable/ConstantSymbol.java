package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;

public class ConstantSymbol extends TypeSymbol {

	private boolean external = false;

	public boolean isExternal() {
		return external;
	}

	public void setExternal(boolean external) {
		this.external = external;
	}

	@Override
	public String getSymbolDescription() {
		StringBuffer result = new StringBuffer();
		HTMLPrinter.getInstance().addPageProlog(result);
		result.append(getVisibilityDescription());
		String externalStr = "";
		if (external)
			externalStr += "External ";
		HTMLPrinter.getInstance().addSmallHeader(result, externalStr + "Constant Name: ");
		HTMLPrinter.getInstance().addLine(result, getName());

		result.append(getFormattedTypeString());

		if (getGroupName() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Group:");
			HTMLPrinter.getInstance().addLine(result, getGroupName());
		}

		if (getSurroundingScope().getScopeSymbol() != null) {
			String plainDescription = getSurroundingScope().getScopeSymbol()
			.getSymbolDescriptionPlain();

			if ((plainDescription != null) && (plainDescription.length() > 0)) {
				HTMLPrinter.getInstance().addSmallHeader(result, "In Scope of:");
				HTMLPrinter.getInstance().addLine(result, plainDescription);
			}
		}

		HTMLPrinter.getInstance().addPageEpilog(result);

		return result.toString();
	}

	@Override
	public String getSymbolDescriptionContentAssist() {
		StringBuffer result = new StringBuffer(getName()).append(" - constant ");
		
		if (getPredefinedType() != null) {
			result.append(getPredefinedTypeAsString());
		} else if (getReferencedType() != null) {
			if (getReferencedType().getModule() != null)
				result.append(getReferencedType().getModule()).append(".");
			result.append(getReferencedType().getIdentifier());
		} else if (getStructuredType() != null) {
			result.append(getStructuredTypeAsString());
		}
		
		return result.toString();
	}
	
}
