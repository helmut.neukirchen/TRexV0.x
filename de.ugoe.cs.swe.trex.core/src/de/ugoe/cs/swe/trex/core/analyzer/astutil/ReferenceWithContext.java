package de.ugoe.cs.swe.trex.core.analyzer.astutil;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;

/**
 * A ReferenceWithContext object represents a reference found by
 * the ReferenceFinder class
 */
public class ReferenceWithContext {
	private LocationAST referenceNode = null;
	private LocationAST matchNode = null;
	private LocationAST contextNode = null;

	public ReferenceWithContext() {
		// empty constructor
	}

	public ReferenceWithContext(LocationAST referenceNode,
			LocationAST contextNode) {
		this.referenceNode = referenceNode;
		this.contextNode = contextNode;
	}

	public LocationAST getContextNode() {
		return contextNode;
	}

	public void setContextNode(LocationAST contextNode) {
		this.contextNode = contextNode;
	}

	public LocationAST getMatchNode() {
		return matchNode;
	}

	public void setMatchNode(LocationAST matchNode) {
		this.matchNode = matchNode;
	}
	
	public LocationAST getReferenceNode() {
		return referenceNode;
	}

	public void setReferenceNode(LocationAST referenceNode) {
		this.referenceNode = referenceNode;
	}

	@Override
	public String toString() {
		return "contextNode: " + contextNode + " - referenceNode: "
				+ referenceNode;
	}

}
