package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import javax.swing.JFrame;
import de.ugoe.cs.swe.trex.core.TTCN3CorePlugin;

public class Clipboard2JavaString extends JFrame {

	private static final long serialVersionUID = 1L;
	Clipboard clipboard = null;

	public void run() {
		if (clipboard == null)
			clipboard = getToolkit().getSystemClipboard();
		Transferable clipData = clipboard.getContents(this);
		String s;
		try {
			s = (String) (clipData.getTransferData(DataFlavor.stringFlavor));
			String[] strings = s.split("[\r\n]+");
			for (int i=0; i < strings.length; i++) {
				if (i < strings.length-1)
					TTCN3CorePlugin.debugMessage("\"" + strings[i] + "\\n\" + ");
				else
					TTCN3CorePlugin.debugMessage("\"" + strings[i] + "\";");
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		}

	}

	public static void main(String[] args) {
		new Clipboard2JavaString().run();
	}

}
