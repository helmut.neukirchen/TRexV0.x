package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import java.util.List;
import java.util.Set;

public class PairTemplateListAndFields {
	List<String> fields;
	Set<LocationAST> templates;
	
	public PairTemplateListAndFields(Set<LocationAST> templates, List<String> fields) {
		this.templates = templates;
		this.fields = fields;
	}

	/**
	 * @return the fields
	 */
	public List<String> getFields() {
		return fields;
	}

	/**
	 * @return the templates
	 */
	public Set<LocationAST> getTemplates() {
		return templates;
	}
}