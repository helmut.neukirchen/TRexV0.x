package de.ugoe.cs.swe.trex.core.metrics;

public class ValueCoupling {
	private int score, numStatements;
	
	// ---------------------------------------------------------------------------
	
	public ValueCoupling() {
		this.score = 0;
		this.numStatements = 0;
	}
	
	// ---------------------------------------------------------------------------
	
	public ValueCoupling(int score, int numStatements) {
		this.score = score;
		this.numStatements = numStatements;
	}
	
	// ---------------------------------------------------------------------------

	public int getNumStatements() {
		return numStatements;
	}
	
	// ---------------------------------------------------------------------------

	public int getScore() {
		return score;
	}
	
	// ---------------------------------------------------------------------------
	
	public void add(final ValueCoupling value) {
		if(value != null) {
			this.score += value.getScore();
			this.numStatements += value.getNumStatements();
		}
	}
	
	// ---------------------------------------------------------------------------
	
	public void set(final int score, final int numStatements) {
		this.score = score;
		this.numStatements = numStatements;
	}
	
	// ---------------------------------------------------------------------------

	public void setNumStatements(final int numStatements) {
		this.numStatements = numStatements;
	}
	
	// ---------------------------------------------------------------------------

	public void setScore(final int score) {
		this.score = score;
	}
}