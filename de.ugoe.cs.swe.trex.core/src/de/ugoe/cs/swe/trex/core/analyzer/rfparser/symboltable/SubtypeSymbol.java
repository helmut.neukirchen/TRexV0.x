package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;

public class SubtypeSymbol extends Symbol {

	private PredefinedTypeEnum predefinedType = null;

	private Reference referencedType = null;

	public enum PredefinedTypeEnum {
		ttcn3_bitstring, ttcn3_boolean, ttcn3_charstring, ttcn3_universalcharstring, ttcn3_integer, ttcn3_octetstring, ttcn3_hexstring, ttcn3_verdicttype, ttcn3_float, ttcn3_address, ttcn3_default, ttcn3_anytype, ttcn3_objid
	}

	public enum TypeEnum {
		// surprisingly, redefinition of enums in subclasses causes problems.
		// use structuredType only in the
		// TypeSymbol subclass.
		predefinedType, referencedType, structuredType
	}

	private TypeEnum type = null;

	public SubtypeSymbol() {
		super();
	}

	public SubtypeSymbol(TypeEnum type) {
		super();
		this.type = type;
	}

	public PredefinedTypeEnum getPredefinedType() {
		return predefinedType;
	}

	public void setPredefinedType(PredefinedTypeEnum predefinedType) {
		this.predefinedType = predefinedType;
	}

	public Reference getReferencedType() {
		return referencedType;
	}

	public void setReferencedType(Reference referencedType) {
		this.referencedType = referencedType;
	}

	public TypeEnum getType() {
		return type;
	}

	public void setType(TypeEnum type) {
		this.type = type;
	}

	@Override
	public String getSymbolDescription() {
		StringBuffer result = new StringBuffer();
		HTMLPrinter.getInstance().addPageProlog(result);
		result.append(getVisibilityDescription());
		HTMLPrinter.getInstance().addSmallHeader(result, "Subtype Name: ");
		HTMLPrinter.getInstance().addLine(result, getName());

		if (getGroupName() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Group:");
			HTMLPrinter.getInstance().addLine(result, getGroupName());
		}

		result.append(getFormattedTypeString());
		if (getSurroundingScope().getScopeSymbol() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Scope of:");
			result.append(getSurroundingScope().getScopeSymbol()
					.getSymbolDescriptionPlain());
		}

		HTMLPrinter.getInstance().addPageEpilog(result);

		return result.toString();
	}

	@Override
	public String getSymbolDescriptionContentAssist() {
		if (getPredefinedType() != null) {
			return getName() + " - subtype " + getPredefinedTypeAsString();
		}

		String result = getName() + " - subtype ";
		if (getReferencedType().getModule() != null)
			result += getReferencedType().getModule() + ".";
		if (getReferencedType().getIdentifier() != null)
			result += getReferencedType().getIdentifier().getName();
		return result;
	}

	protected String getFormattedTypeString() {
		StringBuffer result = new StringBuffer();
		
		if (getPredefinedType() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "Of Predefined Type: ");
			HTMLPrinter.getInstance().addLine(result, getPredefinedTypeAsString());
		}
		if (getReferencedType() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "Of Referenced Type: ");
			if (getReferencedType().getModule() != null)
				HTMLPrinter.getInstance().addLine(result, getReferencedType().getModule().getName()
						+ ".");
			if (getReferencedType().getIdentifier() != null)
				HTMLPrinter.getInstance().addLine(result, getReferencedType().getIdentifier()
						.getName());
		}
		return result.toString();
	}

	public String getPredefinedTypeAsString() {
		String result = getPredefinedType().name().replaceAll("ttcn3_", "");
		if ("universalcharstring".equals(result)) {
			result = "universal charstring";
		}
		return result;
	}
}
