package de.ugoe.cs.swe.trex.core.misc;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

public class IntRangeMap<K> {
	protected TreeMap<Range, RangeMapValue<K>> treeMap = new TreeMap<Range, RangeMapValue<K>>();

	public void put(final int from, final int to, final K value) {
		Range range = new Range(from, to);
		
		RangeMapValue<K> match = treeMap.get(range);

		if (match == null) {
			match = new RangeMapValue<K>();
			match.putValue(new RangeValue<K>(range, value), range);
			treeMap.put(match.getRange(), match);
			return;
		}
		
		treeMap.remove(match.getRange());
		match.putValue(new RangeValue<K>(range, value), range);
		putRecursion(match.getRange().from, match.getRange().to, match);
	}
	
	public void putRecursion(int from, int to, RangeMapValue<K> value) {
		Range range = new Range(from, to);
		
		RangeMapValue<K> match = treeMap.get(range);
		
		if (match == null) {
			treeMap.put(value.getRange(), value);
		} else {
			treeMap.remove(match.getRange());
			for (int i=0; i < value.getValueList().size(); i++) {
				match.putValue(value.getValueList().get(i), value.getRange());
			}
			putRecursion(match.getRange().from, match.getRange().to, match);
		}
	}
	
	public RangeMapValue<K> get(int from, int to) {
		return treeMap.get(new Range(from, to));
	}
	
	public List<K> get(final int key) {
		if (this.treeMap == null)
			return null;
		
		List<K> results = new LinkedList<K>();
		
		RangeMapValue<K> matches = treeMap.get(new Point(key));
		
		for (int i=0; i <matches.getValueList().size(); i++) {
			RangeValue<K> item = matches.getValueList().get(i);
			if ((item.getRange().from <= key) && (key <= item.getRange().to))
				results.add(item.getValue());
		}
		return results;
	}

	public K getWithSmallestRange(final int key) {
		if (this.treeMap == null)
			return null;
		
		K result = null;
		
		RangeMapValue<K> matches = treeMap.get(new Point(key));
		Integer rangeLength = null;
		
		if (matches == null)
			return null;
		
		if (matches.getValueList() == null)
			return null;
		
		for (int i=0; i <matches.getValueList().size(); i++) {
			RangeValue<K> item = matches.getValueList().get(i);
			if ((item.getRange().from <= key) && (key <= item.getRange().to)) {
				if (rangeLength == null) {
					rangeLength = Integer.valueOf(item.getRange().to - item.getRange().from);
					result = item.getValue();
				} else {
					if ((item.getRange().to - item.getRange().from) < rangeLength.intValue()) {
						rangeLength = Integer.valueOf(item.getRange().to - item.getRange().from);
						result = item.getValue();
					}
				}
				
			}
		}
		
		return result;
	}
	
	public void clear() {
		this.treeMap.clear();
	}

	public Set<Entry<Range, RangeMapValue<K>>> entrySet() {
		return this.treeMap.entrySet();
	}

	public Set<Range> keySet() {
		return this.treeMap.keySet();
	}

	public boolean isEmpty() {
		return this.treeMap.isEmpty();
	}

	public int size() {
		return this.treeMap.size();
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		
		Set<Entry<Range, RangeMapValue<K>>> entrySet = treeMap.entrySet();
		
		Iterator<Entry<Range, RangeMapValue<K>>> it = entrySet.iterator();
		
		while (it.hasNext()) {
			Entry<Range, RangeMapValue<K>> item = it.next();
			result.append("\nRange: " + item.getKey() + "\n");
			result.append("Values: \n");
			result.append(item.getValue());
		}
		
		return result.toString();
	}

	public void shift(int offset, int length) {
		Set<Entry<Range, RangeMapValue<K>>> entrySet = treeMap.entrySet();
		
		Iterator<Entry<Range, RangeMapValue<K>>> it = entrySet.iterator();
		
		boolean first = true;
		while (it.hasNext()) {
			Entry<Range, RangeMapValue<K>> item = it.next();
			if (offset >= item.getKey().from) {
				if (first) {
					treeMap.remove(item.getValue());
					List<RangeValue<K>> list = item.getValue().getValueList();
					for (int i=0; i < list.size(); i++) {
						RangeValue<K> listItem = list.get(i);
						list.remove(listItem);
						listItem.getRange().setTo(list.get(i).getRange().getTo()+length);
						list.add(listItem);
					}
					
					putRecursion(item.getKey().from, item.getKey().to+length, item.getValue());
					first = false;
				} else {
					treeMap.remove(item.getValue());
					List<RangeValue<K>> list = item.getValue().getValueList();
					for (int i=0; i < list.size(); i++) {
						RangeValue<K> listItem = list.get(i);
						list.remove(listItem);
						listItem.getRange().setFrom(list.get(i).getRange().getFrom()+length);
						listItem.getRange().setTo(list.get(i).getRange().getTo()+length);
						list.add(listItem);
					}
					putRecursion(item.getKey().from+length, item.getKey().to+length, item.getValue());
				}
			}
		}
	}
	
}
