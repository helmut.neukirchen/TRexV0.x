package de.ugoe.cs.swe.trex.core.formatter;

public class ScanResult {
	private int index;

	private StringBuffer stringBuffer = new StringBuffer();

	private int whiteSpaceCount = 0;

	private String indentationSinceNewline = null;
	
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public StringBuffer getStringBuffer() {
		return stringBuffer;
	}

	public void setStringBuffer(StringBuffer stringBuffer) {
		this.stringBuffer = stringBuffer;
	}

	public int getWhiteSpaceCount() {
		return whiteSpaceCount;
	}

	public void setWhiteSpaceCount(int whiteSpaceCount) {
		this.whiteSpaceCount = whiteSpaceCount;
	}

	public String getIndentationSinceNewline() {
		return indentationSinceNewline;
	}

	public void setIndentationSinceNewline(String indentationSinceNewline) {
		this.indentationSinceNewline = indentationSinceNewline;
	}

	@Override
	public String toString() {
		return "index: " + index + " - whitespace count: " + whiteSpaceCount + " - stringbuffer: '" + stringBuffer.toString() + "'";
	}
	
}
