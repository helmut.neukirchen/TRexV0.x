package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;

public class TypeSymbol extends SubtypeSymbol {

	public enum StructuredTypeEnum {
		ttcn3_record, ttcn3_union, ttcn3_set, ttcn3_recordof, ttcn3_setof, ttcn3_enumerated, ttcn3_port, ttcn3_component
	}

	private StructuredTypeEnum structuredType = null;

	public TypeSymbol() {
		super();
	}

	public TypeSymbol(TypeEnum type) {
		super(type);
	}

	public StructuredTypeEnum getStructuredType() {
		return structuredType;
	}

	public void setStructuredType(StructuredTypeEnum structuredType) {
		this.structuredType = structuredType;
	}

	@Override
	public String getSymbolDescription() {
		StringBuffer result = new StringBuffer();
		HTMLPrinter.getInstance().addPageProlog(result);
		result.append(getVisibilityDescription());
		HTMLPrinter.getInstance().addSmallHeader(result, "Type Name: ");
		HTMLPrinter.getInstance().addLine(result, getName());
		result.append(getFormattedTypeString());
		if (getGroupName() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Group:");
			HTMLPrinter.getInstance().addLine(result, getGroupName());
		}
		if (getSurroundingScope().getScopeSymbol() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Scope of:");
			result.append(getSurroundingScope().getScopeSymbol()
					.getSymbolDescriptionPlain());
		}

		HTMLPrinter.getInstance().addPageEpilog(result);

		return result.toString();
	}

	@Override
	protected String getFormattedTypeString() {
		String superResult = super.getFormattedTypeString();
		StringBuffer result = new StringBuffer();
		result.append(superResult);
		if (getStructuredType() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "Of Structured Type: ");
			HTMLPrinter.getInstance().addLine(result, getStructuredTypeAsString());
		}
		return result.toString();
	}

	@Override
	public String getSymbolDescriptionPlain() {
		StringBuffer result = new StringBuffer();
		if (getPredefinedType() != null) {
			result.append(getPredefinedTypeAsString());
		} else if (getReferencedType() != null) {
			if (getReferencedType().getModule() != null)
				result.append(getReferencedType().getModule()).append(".");
			result.append(getReferencedType().getIdentifier());
		} else if (getStructuredType() != null) {
			result.append(getStructuredTypeAsString());
		}

		result.append(" ").append(getName());

		return result.toString();
	}

	@Override
	public String getSymbolDescriptionContentAssist() {
		StringBuffer result = new StringBuffer(getName()).append(" - type ");

		if (getPredefinedType() != null) {
			result.append(getPredefinedTypeAsString());
		} else if (getReferencedType() != null) {
			if (getReferencedType().getModule() != null)
				result.append(getReferencedType().getModule()).append(".");
			result.append(getReferencedType().getIdentifier());
		} else if (getStructuredType() != null) {
			result.append(getStructuredTypeAsString());
		}

		return result.toString();
	}

	public String toString() {
		return getSymbolDescriptionPlain();
	}

	public String getStructuredTypeAsString() {
		String result = getStructuredType().name().replaceAll("ttcn3_", "");
		if ("recordof".equals(result))
			return "record of";
		if ("setof".equals(result))
			return "set of";
		return result;
	}
}
