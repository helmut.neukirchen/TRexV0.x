package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration;

import java.util.LinkedList;
import java.util.List;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;


public class KindImport extends AbstractKindImport {
	private List<AbstractKindImport> kinds = new LinkedList<AbstractKindImport>();

	@Override
	public boolean isAllowed(Symbol s, KindEnum kind) {
		boolean result = false;
		
		for (int i=0; i < kinds.size(); i++) {
			AbstractKindImport kindNames = kinds.get(i);
			if (kindNames == null)
				continue;
			
			if (kindNames.getType() == kind) {
				if (kindNames.isAllowed(s, kind))
					result = true;
			} else if (kindNames instanceof GroupImport) {
				result = kindNames.isAllowed(s, kind);
			}
		}
		return result;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();

		result.append("KindImport: ");
		for (int i = 0; i < kinds.size(); i++) {
			result.append(kinds.get(i));
			if (i < kinds.size() - 1)
				result.append(", ");
		}
		result.append(" ("+getVisibility().toString().substring(3)+")");
		return result.toString();
	}

	public List<AbstractKindImport> getKinds() {
		return kinds;
	}

	public void setKinds(List<AbstractKindImport> kinds) {
		this.kinds = kinds;
	}

	public void addKind(AbstractKindImport kindNames) {
		kinds.add(kindNames);
	}

}
