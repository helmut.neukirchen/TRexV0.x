package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration;

import java.util.LinkedList;
import java.util.List;

public class ImportConfiguration {
	private String moduleName;

	private List<AbstractImport> imports = new LinkedList<AbstractImport>();

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public List<AbstractImport> getImports() {
		return imports;
	}

	public void addImport(AbstractImport ip) {
		imports.add(ip);
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("Import Configuration for " + getModuleName() + ": \n");
		for (int i = 0; i < imports.size(); i++) {
			AbstractImport imp = imports.get(i);
			result.append("  imports(" + i + "): " + imp.toString()+"\n");
		}
		
		return result.toString();
	}
	
	public boolean importsFromModule(String name) {
		for (int i=0; i < imports.size(); i++) {
			AbstractImport currentImport = imports.get(i);
			if (currentImport.getModuleName().equals(name))
				return true;
		}
		
		return false;
	}
	
}
