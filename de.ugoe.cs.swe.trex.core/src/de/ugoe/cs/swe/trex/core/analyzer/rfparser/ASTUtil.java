package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import antlr.RecognitionException;
import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.TTCN3CorePlugin;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.ModuleSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.TypeSymbol;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterParameters;

public final class ASTUtil {
	private ASTUtil() {
		// empty private constructor: this class contains only static methods!
	}

	public static LocationAST deepCloneAST(LocationAST t) throws Exception {
		ObjectOutputStream oos = null;
		ObjectInputStream ois = null;
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(bos);
			// serialize and pass the object
			oos.writeObject(t);
			oos.flush();
			ByteArrayInputStream bin = new ByteArrayInputStream(bos
					.toByteArray());
			ois = new ObjectInputStream(bin);
			// return the new object
			return (LocationAST) ois.readObject();
		} catch (Exception e) {
			TTCN3CorePlugin.debugMessage("Exception in LocationASTCloner = " + e);
			throw (e);
		} finally {
			if (oos != null)
				oos.close();
			if (ois != null)
				ois.close();
		}
	}

	// -------------------------------------------------------------------------

	public static AST findChild(AST first, int type) {
		AST child = first;
		while ((child != null) && (child.getType() != type)) {
			child = child.getFirstChild();
		}

		if ((first == child) && (first.getType() != type)) {
			return null;
		}

		return child;
	}

	// -------------------------------------------------------------------------

	public static AST findSibling(AST first, int type) {
		AST sibling = first;
		while ((sibling != null) && (sibling.getType() != type)) {
			sibling = sibling.getNextSibling();
		}

		if ((first == sibling) && (first.getType() != type)) {
			return null;
		}

		return sibling;
	}

	// -------------------------------------------------------------------------

	public static AST findNextFormalParameterSibling(AST first) {
		if (first == null) {
			return null;
		}
		AST sibling = first;
		while ((sibling != null)
				&& (sibling.getType() != TTCN3LexerTokenTypes.FormalValuePar && sibling
						.getType() != TTCN3LexerTokenTypes.FormalTemplatePar)) {
			sibling = sibling.getNextSibling();
		}

		if ((first == sibling)
				&& (first.getType() != TTCN3LexerTokenTypes.FormalValuePar && first
						.getType() != TTCN3LexerTokenTypes.FormalTemplatePar)) {
			return null;
		}

		return sibling;
	}

	// -------------------------------------------------------------------------

	public static LocationAST findDerivedDef(LocationAST defNode) {
		LocationAST derived = defNode.getFirstChild();

		while ((derived != null)
				&& (derived.getType() != TTCN3LexerTokenTypes.DerivedDef)) {
			derived = derived.getNextSibling();
		}

		return derived;
	}

	// -------------------------------------------------------------------------

	public static List<LocationAST> getParameterReferenceNodes(LocationAST start) {
		List<LocationAST> parameterReferenceNodes = new LinkedList<LocationAST>();
		findParameterReferenceNodes(parameterReferenceNodes, start);
		return parameterReferenceNodes;
	}

	// -------------------------------------------------------------------------

	private static void findParameterReferenceNodes(
			List<LocationAST> parameterReferenceNodes, LocationAST startNode) {
		LocationAST node = startNode;
		while (node != null) {
			// System.err.println("checking
			// "+TTCN3Analyzer.getDescendantText(node));

			if (node.getType() == TTCN3LexerTokenTypes.IDENTIFIER) {
				// System.err.println("adding
				// "+TTCN3Analyzer.getDescendantText(node));
				parameterReferenceNodes.add(node);
			}

			if (node.getFirstChild() != null) {
				findParameterReferenceNodes(parameterReferenceNodes, node
						.getFirstChild());
			}

			node = node.getNextSibling();
		}
	}

	// -------------------------------------------------------------------------

	public static LocationAST findTemplateBodyFromTemplateDef(
			LocationAST defNode) {
		LocationAST templateBody = defNode.getFirstChild();

		while ((templateBody != null)
				&& (templateBody.getType() != TTCN3LexerTokenTypes.TemplateBody)) {
			templateBody = templateBody.getNextSibling();
		}
		return templateBody;
	}

	// -------------------------------------------------------------------------

	public static List<LocationAST> getActualParameters(LocationAST ast,
			boolean resolve) {
		LocationAST functionRefNode = ast.getParent().getParent();

		LocationAST functionActualParList = functionRefNode.getNextSibling();

		// Need to check for null pointer. This occurrs when there are
		// no actual parameters. In this case we return an empty list

		if (functionActualParList == null) {
			return new LinkedList<LocationAST>();
		}

		List<LocationAST> paramsList = getParamsFromParList(
				functionActualParList, resolve);

		return paramsList;
	}

	// -------------------------------------------------------------------------

	public static List<LocationAST> getParamsFromParList(
			LocationAST functionActualParList, boolean resolve) {
		if (functionActualParList.getType() != TTCN3LexerTokenTypes.FunctionActualParList) {
			return null;
		}

		List<LocationAST> paramsList = new LinkedList<LocationAST>();

		LocationAST actualPar = functionActualParList.getFirstChild();

		while (actualPar != null) {
			if (actualPar.getType() != TTCN3LexerTokenTypes.FunctionActualPar) {
				actualPar = actualPar.getNextSibling();
				continue;
			}

			LocationAST finalChild = getDescendantNode(actualPar);
			if (resolve
					&& finalChild.getParent().getType() == TTCN3LexerTokenTypes.Identifier) {
				Scope scope = finalChild.getScope();

				Symbol symb = scope.resolve(finalChild);

				if (symb != null) {
					final LocationAST declNode = symb.getDeclarationNode();
					final LocationAST declSibling = declNode.getParent()
							.getNextSibling();
					if (declSibling == null) {
						paramsList.add(declNode);
					} else {
						paramsList.add(declSibling);
					}
				}
			} else {
				paramsList.add(actualPar.getFirstChild());
			}
			actualPar = actualPar.getNextSibling();
		}
		return paramsList;
	}

	// -------------------------------------------------------------------------

	public static Map<String, String> getCommonFields(LocationAST template1,
			LocationAST template2) {
		Map<String, String> commonFields = new HashMap<String, String>();

		// get the fields present in each template
		List<AST> fields1 = getFields(template1);
		List<AST> fields2 = getFields(template2);

		// get the parameters (if any) present in each template
		List<String> params1 = null, params2 = null;
		if (isParameterizedTemplate(template1)) {
			params1 = getFormalParametersStr(template1);
		}
		if (isParameterizedTemplate(template2)) {
			params2 = getFormalParametersStr(template2);
		}

		if (fields1.size() == fields2.size()) {
			for (int count = 0; count < fields1.size(); count++) {
				String ident = ASTUtil.getDescendantText(fields1.get(count));
				String field1 = getFieldValueStr(fields1.get(count));
				String field2 = getFieldValueStr(fields2.get(count));

				// if the field is given equal value in both templates, or if
				// both templates give the field a value from their parameters
				// assume them to be able to be merged as common
				if (field1.equals(field2)) {
					commonFields.put(ident, field1);
				} else if (params1 != null && params2 != null
						&& params1.contains(field1) && params2.contains(field2)) {
					commonFields.put(ident, null);
				}
			}
		}

		return commonFields;
	}

	/**
	 * Keep descending the parse tree via the first child until no more children
	 * exist (i.e. a leaf node has been reached).
	 *
	 * @param a
	 *            the <code>AST</code> node to begin descending from.
	 * @return the leaf node that was reached.
	 */
	public static LocationAST getDescendantNode(AST a) {
		final AST child = a.getFirstChild();
		if (child == null) {
			return (LocationAST) a;
		}
		return getDescendantNode(child);
	}

	/**
	 * Get node declaring the enclosing module
	 *
	 * @param node
	 *            an AST node for which the enclosing module node is obtained.
	 * @return The module node (of type TTCN3Module); null if not found.
	 */
	public static LocationAST getModuleNode(AST node) {
		if (node == null)
			return null;
		LocationAST testNode = (LocationAST) node;
		if (testNode.getType() == TTCN3ParserTokenTypes.TTCN3Module) {
			return (LocationAST) node;
		} else {
			testNode = testNode.getParent();
			return getModuleNode(testNode);
		}
	}

	/**
	 * Get the start node of the module definitions part
	 *
	 * @param node
	 *            an AST node for which the enclosing module definitions part
	 *            node is obtained.
	 * @return The module definitions part node (of type ModuleDefinitionsPart);
	 *         null if not found.
	 */
	public static LocationAST getModuleDefinitionsPartNode(AST node) {
		if (node == null)
			return null;
		LocationAST testNode = (LocationAST) node;
		if (testNode.getType() == TTCN3ParserTokenTypes.ModuleDefinitionsPart) {
			return (LocationAST) node;
		} else {
			testNode = testNode.getParent();
			return getModuleDefinitionsPartNode(testNode);
		}
	}

	/**
	 * Sensibly descend the parse tree to construct a suitable
	 * <code>String</code> representation of the given <code>AST</code>node's
	 * type.
	 *
	 * @param ast
	 *            the <code>AST</code> node to construct a <code>String</code>
	 *            representation of.
	 * @return a <code>String</code> representation of the given
	 *         <code>AST</code> node.
	 */
	public static String getDescendantText(AST ast) {
		int subtype = ast.getType();
		StringBuffer result = new StringBuffer();
		AST child = ast.getFirstChild();
		AST type, ident;

		switch (subtype) {
		case TTCN3ParserTokenTypes.ConstDef:
			ident = getIdentNode(ast);
			type = getDescendantNode(child);
			result.append(ident.getText()).append(" : ").append(type.getText());
			break;
		case TTCN3ParserTokenTypes.ReceiveStatement:
		case TTCN3ParserTokenTypes.SendStatement:
			AST port = getDescendantNode(child);
			AST sibling = child.getNextSibling();
			if (port.getType() == TTCN3ParserTokenTypes.AnyKeyword) {
				result.append("any port.");
			} else {
				result.append(port.getText()).append('.');
			}

			result
					.append((subtype == TTCN3ParserTokenTypes.SendStatement) ? "send"
							: "receive");

			if (sibling.getFirstChild() != null) {
				result.append('(');

				while (sibling.getType() != TTCN3ParserTokenTypes.Type) {
					if (sibling.getFirstChild() == null) {
						result.append(sibling.getText()).append(')');
						break;
					}
					sibling = sibling.getFirstChild();
				}
				if (sibling.getFirstChild() != null) {
					type = getDescendantNode(sibling);
					ident = sibling.getNextSibling();

					result.append(type.getText()).append(" : ");

					// case for parameterised
					while (ident != null) {
						if (ident.getType() == TTCN3ParserTokenTypes.FunctionRef) {
							// work out all params and put them inside brackets
							StringBuffer buf = new StringBuffer();
							final List<LocationAST> params = ASTUtil
									.getActualParameters(
											getDescendantNode(ident), false);
							if (params.size() > 0) {
								for (AST param : params) {
									final String text = getDescendantText(param);
									if (text != null) {
										buf.append(text).append(", ");
									}
								}
								final int length = buf.length();
								// delete extra ", " at end of stringbuffer
								buf.deleteCharAt(length - 1);
								buf.deleteCharAt(length - 2);
								result.append(getDescendantText(ident)).append(
										" (").append(buf).append("))");
							} else {
								result.append(getDescendantText(ident)).append(
										"(params))");
							}
							break;
						}
						ident = ident.getFirstChild();
					}
					if (ident == null) {
						result.append(
								getDescendantText(sibling.getNextSibling()))
								.append(')');
					}
				}
			}
			break;
		case TTCN3ParserTokenTypes.TemplateDef:
			ident = getIdentNode(ast);
			type = getDescendantNode(ast);
			result.append(ident.getText()).append(" : ").append(type.getText());
			break;
		case TTCN3ParserTokenTypes.TypeDef:
			AST secondChild = child.getFirstChild();
			ident = getIdentNode(ast);
			result.append(ident.getText()).append(" : ");
			if (child.getType() == TTCN3ParserTokenTypes.StructuredTypeDef) {
				result.append(secondChild.getText());
			} else if (child.getType() == TTCN3ParserTokenTypes.SubTypeDef) {
				type = getDescendantNode(secondChild);
				result.append(type.getText());
			}
			break;
		default:
			// default fallthrough - return the getText() representation of the
			// descendant node
			final LocationAST descendantNode = getDescendantNode(ast);
			String text = descendantNode.getText();
			if (descendantNode.getParent().getType() == TTCN3LexerTokenTypes.CharStringValue) {
				text = "\"" + text + "\"";
			}
			result.append(text);
			break;
		}
		return result.toString();
	}

	/**
	 * Find the differing fields between two templates.
	 *
	 * @param template1
	 *            the first template to compare
	 * @param template2
	 *            the second template to compare
	 * @return a <code>List</code> containing the differring fields of the two
	 *         templates
	 */
	public static List<String> getDifferingFieldsStr(LocationAST template1,
			LocationAST template2) {
		List<AST> differingFields = getDifferingFields(template1, template2);
		List<String> differingFieldsStr = new LinkedList<String>();

		for (AST field : differingFields) {
			differingFieldsStr.add(field.getText());
		}

		return differingFieldsStr;
	}

	/**
	 * Find the differing fields between two templates.
	 *
	 * @param template1
	 *            the first template to compare
	 * @param template2
	 *            the second template to compare
	 * @return a <code>List</code> containing the <code>AST</code> nodes for
	 *         differring fields of the two templates
	 */
	public static List<AST> getDifferingFields(LocationAST template1,
			LocationAST template2) {
		List<AST> differingFields = new LinkedList<AST>();

		// get the fields present in each template
		List<AST> fields1 = getFields(template1);
		List<AST> fields2 = getFields(template2);

		// get the parameters (if any) present in each template
		List<String> params1 = null, params2 = null;
		if (isParameterizedTemplate(template1)) {
			params1 = getFormalParametersStr(template1);
		}
		if (isParameterizedTemplate(template2)) {
			params2 = getFormalParametersStr(template2);
		}

		if (fields1.size() == fields2.size()) {
			for (int count = 0; count < fields1.size(); count++) {
				AST ident = ASTUtil.getDescendantNode(fields1.get(count));
				String field1 = getFieldValueStr(fields1.get(count));
				String field2 = getFieldValueStr(fields2.get(count));

				// if the field is not given equal value in both templates and
				// if both templates do not give the field a value from their
				// parameters, and the fields are not given inlined values
				// assume them to be differences
				if (!field1.equals(field2)
						&& (params1 == null || params2 == null
								|| !params1.contains(field1) || !params2
								.contains(field2))
						&& !ASTUtil.isInlineField(fields1.get(count))
						&& !ASTUtil.isInlineField(fields2.get(count))) {
					differingFields.add(ident);
				}
			}
		}

		return differingFields;
	}

	// -------------------------------------------------------------------------

	private static void getFieldIdent(AST field, List<AST> idents) {
		LocationAST ast = (LocationAST) field;
		while (ast != null) {
			final LocationAST sibling = ast.getNextSibling();
			if (sibling != null) {
				getFieldIdent(sibling, idents);
			}

			final int type = ast.getType();
			if (type == TTCN3LexerTokenTypes.FunctionRef) {
				// workaround function references
				final LocationAST parList = sibling;
				LocationAST par = parList.getFirstChild();
				while (par != null) {
					ast = ASTUtil.getDescendantNode(par);
					idents.add(ast);
					par = par.getNextSibling();
				}
				break;
			} else if (type == TTCN3LexerTokenTypes.FieldSpecList) {
				// workaround nested fields
				List<AST> nestedFields = new LinkedList<AST>();
				getFieldsFromSpecList(ast, nestedFields);
				for (AST nestedField : nestedFields) {
					getFieldIdent(nestedField, idents);
				}
				break;
			} else if (type == TTCN3LexerTokenTypes.IDENTIFIER) {
				idents.add(ast);
			}
			ast = ast.getFirstChild();
		}
	}

	/**
	 * Get a <code>List</code> of the Field identifiers from a template.
	 *
	 * @param template
	 *            the <code>TemplateDef</code> node to get fields for
	 * @return a
	 *         <code>List> of the <code>AST</code> nodes representing the templates fields
	 */
	public static List<AST> getFields(AST template) {
		AST templateBody = template;
		List<AST> fields = new LinkedList<AST>();

		if (template.getType() == TTCN3ParserTokenTypes.TemplateDef) {
			templateBody = template.getFirstChild().getNextSibling();
		}

		if (templateBody.getType() == TTCN3ParserTokenTypes.TemplateBody) {
			AST child = templateBody.getFirstChild();
			switch (child.getType()) {
			case TTCN3ParserTokenTypes.FieldSpecList:
				// parse spec list
				getFieldsFromSpecList(child, fields);
				break;
			case TTCN3ParserTokenTypes.SingleValueOrAttrib:
				// add single value
				getFieldsFromSingleValueOrAttrib(child, fields);
				break;
			default:
				break;
			}
		}

		return fields;
	}

	/**
	 * @param templateBodyChild
	 * @param fields
	 */
	private static void getFieldsFromSingleValueOrAttrib(AST single,
			List<AST> fields) {
		AST child = single.getFirstChild();
		int type = child.getType();

		switch (type) {
		case TTCN3ParserTokenTypes.MatchingSymbol:
			int subtype = child.getFirstChild().getType();
			switch (subtype) {
			case TTCN3ParserTokenTypes.ValueOrAttribList:
				AST templateBody = child.getFirstChild();
				while (templateBody != null) {
					fields.addAll(getFields(templateBody));
					templateBody = templateBody.getNextSibling();
				}
				break;
			default:
				fields.add(child); // just add the MatchingSymbol
				break;
			}
			break;
		case TTCN3ParserTokenTypes.SingleExpression:
			fields.add(child);
			break;
		default:
			break;
		}
	}

	/**
	 * Find the fields and their assigned values in a template. NB IGNORES
	 * FIELDS GIVEN THEIR VALUES VIA PARAMETERS
	 *
	 * @param template
	 *            the template to examine
	 * @return a <code>Map</code> of all the fields and their respective
	 *         values in a template
	 */
	public static Map<String, String> getFieldsAndValues(final AST template) {
		Map<String, String> map = new HashMap<String, String>();

		List<AST> fields = getFields(template);
		List<String> params = getFormalParametersStr(template);

		for (AST field : fields) {
			String value = getFieldValueStr(field);

			if (!params.contains(value)) {
				map.put(getDescendantText(field), value);
			}
		}

		return map;
	}

	/**
	 * Append to a <code>List</code> all Fields within a
	 * <code>FieldSpecList</code>, taking into account nesting.
	 *
	 * @param fieldSpecList
	 *            the <code>FieldSpecList</code> node to get fields from
	 * @param list
	 *            the <code>List</code> to append all fields to
	 */
	private static void getFieldsFromSpecList(AST fieldSpecList, List<AST> list) {
		if (fieldSpecList.getType() == TTCN3ParserTokenTypes.FieldSpecList) {
			AST fieldSpec = fieldSpecList.getFirstChild();
			while (fieldSpec != null) {
				// check for nested
				AST templateBody = fieldSpec.getFirstChild().getNextSibling();
				AST child = templateBody.getFirstChild();
				if (child.getType() == TTCN3ParserTokenTypes.FieldSpecList) {
					getFieldsFromSpecList(child, list);
				}

				list.add(fieldSpec);

				fieldSpec = fieldSpec.getNextSibling();
			}
		}
	}

	/**
	 * Find all the identifiers for the values assigned the fields of a
	 * template.
	 *
	 * @param template
	 *            the template whose field's values should be returned
	 * @return a <code>List</code> of the Identifier <code>AST</code> nodes
	 *         for all the values of the fields in the template
	 */
	public static List<AST> getFieldValueIdents(AST template) {
		List<AST> fields = getFields(template);
		List<AST> idents = new LinkedList<AST>();

		for (AST field : fields) {
			getFieldIdent(field, idents);
			// remove the descendant of the field from the list as this will be
			// the actual name of the field
			idents.remove(ASTUtil.getDescendantNode(field));
		}

		return idents;
	}

	/**
	 * Find the value assigned to a particular field in a template.
	 *
	 * @param field
	 *            the field to lookup
	 * @return the <code>AST</code> node for the assigned value
	 */
	public static AST getFieldValue(AST field) {
		switch (field.getType()) {
		case TTCN3LexerTokenTypes.FieldSpec:
			AST templateBody = field.getFirstChild().getNextSibling();
			return templateBody;
		default:
			return field;
		}
	}

	/**
	 * Find the value assigned to a particular field in a template.
	 *
	 * @param field
	 *            the field to lookup
	 * @return the assigned value as a <code>String</code>
	 */
	public static String getFieldValueStr(AST field)
			throws IllegalArgumentException {
		if (field == null)
			throw new IllegalArgumentException("Field must not be null");

		final LocationAST fieldValue = (LocationAST) getFieldValue(field);

		if (fieldValue == null)
			throw new IllegalArgumentException(
					"Field value must not be null @ "
							+ ((LocationAST) field).getOffset());

		String text = "";

		TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
		formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
		formatterWalker.setFormatterParameters(new TTCN3FormatterParameters());
		try {
			if (fieldValue.getType() == TTCN3LexerTokenTypes.TemplateBody) {
				text = formatterWalker.pr_TemplateBody(fieldValue).toString();
			} else {
				LocationAST templateBody = LocationAST.resolveParentsUntilType(
						fieldValue, TTCN3LexerTokenTypes.TemplateBody);
				text = formatterWalker.pr_TemplateBody(templateBody).toString();
			}
			return text;
		} catch (RecognitionException e) {
			e.printStackTrace();
		}

		// should never get here
		throw new IllegalArgumentException("Failed to parse field "
				+ field.getText());
	}

	/**
	 * Find the formal parameters for a given template.
	 *
	 * @param template
	 *            the template to examine
	 * @return a <code>List</code> of <code>AST</code> nodes for the Formal
	 *         Param identifiers
	 */
	public static List<AST> getFormalParameters(AST template) {
		List<AST> params = new LinkedList<AST>();
		AST baseTemplate = findSibling(template.getFirstChild(),
				TTCN3LexerTokenTypes.BaseTemplate);

		if (baseTemplate == null) {
			return null;
		}
		// Check for normal parameters and template parameters also

		AST formalValueOrTemplatePar = findNextFormalParameterSibling(baseTemplate.getFirstChild());
		while (formalValueOrTemplatePar != null) {
			AST parIdentifier = getIdentNode(formalValueOrTemplatePar);

			params.add(parIdentifier);

			formalValueOrTemplatePar = findNextFormalParameterSibling(formalValueOrTemplatePar.getNextSibling());
		}

		if (params.isEmpty()) {
			return null;
		} else {
			return params;
		}
	}

	/**
	 * Find the formal parameters for a given template.
	 *
	 * @param template
	 *            the template to examine
	 * @return a <code>List</code> of <code>Strings</code> giving the Formal
	 *         Parameter identifier names
	 */
	public static List<String> getFormalParametersStr(AST ast) {
		List<AST> params = getFormalParameters(ast);
		List<String> paramsStr = new LinkedList<String>();

		if (params != null) {
			for (AST parIdentifier : params) {
				paramsStr.add(parIdentifier.getText());
			}
		}

		return paramsStr;
	}

	/**
	 * Find the node that should be used to identify a particular parent
	 *
	 * @param parent
	 *            the node to represent
	 * @return the <code>AST</code> node that should be used to identify the
	 *         given parent
	 */
	public static LocationAST getIdentNode(final AST parent) {
		//TODO: this needs an urgent revision!!!!!
		LocationAST result = null;
		final int subtype = parent.getType();
		final AST child = parent.getFirstChild();
		AST secondChild;
		switch (subtype) {
		case TTCN3ParserTokenTypes.ConstDef:
		case TTCN3ParserTokenTypes.FieldSpec:
		case TTCN3ParserTokenTypes.FormalValuePar:
			result = getDescendantNode(child.getNextSibling());
			break;
		case TTCN3ParserTokenTypes.FormalTemplatePar:
			result = getDescendantNode(findSibling(child, TTCN3ParserTokenTypes.Identifier));
			break;
		case TTCN3ParserTokenTypes.TemplateDef:
			secondChild = child.getFirstChild();
			result = getDescendantNode(secondChild.getNextSibling());
			break;
		case TTCN3ParserTokenTypes.TypeDef:
			secondChild = child.getFirstChild();
			final int type = child.getType();
			if (type == TTCN3ParserTokenTypes.SubTypeDef) {
				result = getDescendantNode(secondChild.getNextSibling());
			} else if (type == TTCN3ParserTokenTypes.StructuredTypeDef) {
				result = getDescendantNode(secondChild);
			}
			break;
		default:
			result = getDescendantNode(parent);
		}

		return result;
	}

	// -------------------------------------------------------------------------

	public static TypeSymbol getTypeSymbol(AST field) {
		final LocationAST identNode = ASTUtil.getDescendantNode(field);
		Symbol symb = identNode.getScope().resolve(identNode);
		return (TypeSymbol) symb;
	}

	// -------------------------------------------------------------------------

	public static ModuleSymbol getModuleSymbol(LocationAST ast) {
		Scope scope = ast.getScope();

		if (scope == null)
			scope = ASTUtil.getModuleDefinitionsPartNode(ast).getScope();

		if (scope == null)
			scope = ASTUtil.getDescendantNode(ast).getScope();

		ModuleSymbol moduleSymbol = (ModuleSymbol) scope.getModuleScope()
				.getScopeSymbol();

		return moduleSymbol;
	}

	/**
	 * Determine if the given <code>AST</code> node is a
	 * <code>FieldSpec</code>
	 *
	 * @param ast
	 *            the <code>AST</code> to examine
	 * @return true if the node is a <code>FieldSpec</code>
	 */
	public static boolean isFieldSpec(LocationAST ast) {
		LocationAST parentAST = LocationAST.resolveParentsUntilType(ast,
				new int[] { TTCN3LexerTokenTypes.FieldSpec,
						TTCN3LexerTokenTypes.ArrayElementSpec });

		return parentAST == null ? false : true;
	}

	// -------------------------------------------------------------------------

	public static boolean isInlineField(AST template) {
		int type = template.getType();

		if (type == TTCN3LexerTokenTypes.CharStringMatch
				|| type == TTCN3LexerTokenTypes.MatchingSymbol
				|| type == TTCN3LexerTokenTypes.SingleExpression)
			return false;

		if (type != TTCN3LexerTokenTypes.FieldSpec)
			throw new IllegalArgumentException(
					"isInlineField() can only be called on FieldSpec, MatchingSymbol or SingleExpression");

		AST templateBody = template.getFirstChild().getNextSibling();
		if (templateBody.getFirstChild().getType() == TTCN3LexerTokenTypes.FieldSpecList)
			return true;

		return false;
	}

	/**
	 * Determine if the given template is parameterised or not.
	 *
	 * @param template
	 *            the template to examine
	 * @return true if the template is parameterised false otherwise
	 */
	public static boolean isParameterizedTemplate(AST template) {
		AST child = template.getFirstChild().getFirstChild();
		while (child != null) {
			if (child.getType() == TTCN3LexerTokenTypes.FormalValuePar
					|| child.getType() == TTCN3LexerTokenTypes.FormalTemplatePar) {
				return true;
			}
			child = child.getNextSibling();
		}

		return false;
	}

	/**
	 * Determine if the given reference to a template uses parameters.
	 *
	 * @param ref
	 *            the template reference to examine
	 * @return true if the reference is parameterised false otherwise
	 */
	public static boolean isParameterizedTemplateReference(LocationAST ref) {
		if (ref.getParent().getParent().getType() == TTCN3LexerTokenTypes.FunctionRef) {
			return true;
		}

		return false;
	}

	// -------------------------------------------------------------------------

	/**
	 * Finds all nodes of given type in the AST starting with root. When
	 * traversing the AST, ignore nodes of type given by ignore and all
	 * subnodes.
	 */
	public static LinkedList<LocationAST> findTypeNodesIgnore(LocationAST root,
			int type, int[] ignore) {
		LinkedList<LocationAST> results = new LinkedList<LocationAST>();
		findTypeNodesRecursion(root, results, type, ignore);
		return results;
	}

	public static LinkedList<LocationAST> findTypeNodes(LocationAST root,
			int type) {
		return findTypeNodesIgnore(root, type, null);
	}

	private static void findTypeNodesRecursion(LocationAST root,
			LinkedList<LocationAST> results, int type, int[] ignore) {
		if (root.getType() == type) {
			results.add(root);
			return;
		}

		LocationAST current = root.getFirstChild();
		while (current != null) {
			if (ignore == null || !containsType(ignore, current.getType()))
				findTypeNodesRecursion(current, results, type, ignore);
			current = current.getNextSibling();
		}
	}

	private static boolean containsType(int[] ignore, int type) {
		if (ignore == null)
			return false;
		for (int i = 0; i < ignore.length; i++) {
			if (ignore[i] == type)
				return true;
		}
		return false;
	}

	/**
	 * Get the name of the enclosing module.
	 *
	 * @param node
	 *            An arbitrary node from inside a TTCN-3 module
	 * @return A String containing the TTCN-3 identifier of module
	 */
	public static String getEnclosingModuleName(LocationAST node) {
		LocationAST moduleNode = ASTUtil.getModuleNode(node);
		return moduleNode.getFirstChild().getFirstChild().getFirstChild()
				.getFirstChild().getFirstChild().getText();
	}

	/**
	 * Get the name of the identifier being declared by the node.
	 *
	 * @param node
	 *            An AST node defining a behaviour.
	 * @return A String containing the TTCN-3 identifier of the behaviour
	 *         ("Control Part" if node is ModuleControlBody node); null if node
	 *         is not a behaviour defining node.
	 */
	public static String getBehaviourIdentifier(LocationAST node) {
		if ((node.getType() == TTCN3LexerTokenTypes.FunctionDef)
				|| (node.getType() == TTCN3LexerTokenTypes.TestcaseDef)
				|| (node.getType() == TTCN3LexerTokenTypes.AltstepDef))
			return node.getFirstChild().getFirstChild().getText();

		if (node.getType() == TTCN3LexerTokenTypes.ModuleControlBody)
			return "Control Part";

		return null;
	}

}
