package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import de.ugoe.cs.swe.trex.core.misc.IntRangeMap;

public class IdentifierCache {
	private IntRangeMap<LocationAST> identifierMap = new IntRangeMap<LocationAST>();

	public void put(int startOffset, int endOffset, LocationAST t) {
		identifierMap.put(startOffset, endOffset, t);
	}
	
	public LocationAST get(int offset) {
		return identifierMap.getWithSmallestRange(offset);
	}

}
