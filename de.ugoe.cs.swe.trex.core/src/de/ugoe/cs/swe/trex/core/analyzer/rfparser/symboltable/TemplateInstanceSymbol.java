package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;

public class TemplateInstanceSymbol extends TypeSymbol {
	@Override
	public String getSymbolDescription() {
		StringBuffer result = new StringBuffer();
		HTMLPrinter.getInstance().addPageProlog(result);
		HTMLPrinter.getInstance().addSmallHeader(result, "Template Instance: ");
		
		HTMLPrinter.getInstance().addLine(result, getName());

		result.append(getFormattedTypeString());

		if (getSurroundingScope().getScopeSymbol() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Scope of:");
			HTMLPrinter.getInstance().addLine(result, getSurroundingScope().getScopeSymbol().getSymbolDescriptionPlain());
		}
		HTMLPrinter.getInstance().addPageEpilog(result);
		return result.toString();
	}

	@Override
	public String getSymbolDescriptionContentAssist() {
		String result = getName() + " - var template ";
		
		if (getPredefinedType() != null) {
			result += getPredefinedTypeAsString();
		} else {
			if (getReferencedType() != null) {
				if (getReferencedType().getModule() != null)
					result += getReferencedType().getModule() + ".";
				if (getReferencedType().getIdentifier() != null)
					result += getReferencedType().getIdentifier().getName();
			}
		}
		return result;
	}
	
}
