// The TTCN-3 lexer and parser are derived from the sources of the open source TTthreeParser.
// These sources are covered by the GPL, but Testing Tech IST GmbH granted the permission to redistribute 
// the derived work under the Eclipse Public License.
//

header
{
package de.ugoe.cs.swe.trex.core.analyzer.rfparser;
}

{
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
import java.util.ArrayList;

@SuppressWarnings("unused")
}

class TTCN3Parser extends Parser;

options
{
		importVocab=TTCN3Lexer;
		exportVocab=TTCN3Parser;
		buildAST = true;
		defaultErrorHandler=true;
		//  defaultErrorHandler=true;
		ASTLabelType = "LocationAST";
		k=8;
}

{
	private static boolean statementBlockCompatibilityMode = true; 
	
	private static String supportedVersion = "4.5.1"; 
	
	private IdentifierCache identifierCache = new IdentifierCache();

	private ArrayList<RecognitionException> exceptions = new ArrayList<RecognitionException>();

	public IdentifierCache getIdentifierCache() {
		return identifierCache;
	}
	
	public static String getSupportedVersion() {
		return supportedVersion;
	}

	public static void enableStatementBlockCompatibilityMode(){
		statementBlockCompatibilityMode = true;	
	}

	public static void disableStatementBlockCompatibilityMode(){
		statementBlockCompatibilityMode = false;	
	}

	public static boolean isStatementBlockCompatibilityMode(){
		return statementBlockCompatibilityMode;	
	}


	public void resetIdentifierCache() {
		identifierCache = null;
		identifierCache = new IdentifierCache();
	}

//	public void consume() throws TokenStreamException {
//	    super.consume();
//	    System.out.println("consumed token: " + TTCN3Parser._tokenNames[LA(1)]);
//	}


    public void reportError(RecognitionException ex) {
//        System.err.println(ex);
		exceptions.add(ex);
    }

	public ArrayList<RecognitionException> getExceptions() {
		return exceptions;
	}

	private boolean checkTokenBeforeScopeChange(int[] tokenTypes, Token ltTok) {
		TokenWithIndex tok = (TokenWithIndex) ltTok;
		int tokIndex = tok.getIndex();
		Token nextToken = tok.getTracker().getToken(tokIndex);
		while ((nextToken.getType() != TTCN3ParserTokenTypes.BEGINCHAR || nextToken.getType() != TTCN3ParserTokenTypes.BEGINCHAR || nextToken.getType() != TTCN3ParserTokenTypes.SEMICOLON) && (tokIndex < tok.getTracker().getTokenCount())) {
			nextToken = tok.getTracker().getToken(tokIndex);
			for (int i=0; i < tokenTypes.length; i++) {
				if (nextToken.getType() == tokenTypes[i])
					return true;
			}
			tokIndex++;
		}
		return false;
	}

	private boolean test(){
		return true;	
	}

  private boolean checkMatchingSymbolToken(int[] tokenTypes, Token ltTok) {
    TokenWithIndex tok = (TokenWithIndex) ltTok;
    int tokIndex = tok.getIndex();
    Token nextToken = tok.getTracker().getToken(tokIndex);
    int tokenLimit = 8; //for some odd reason the list of tokens is limited and can be expanded with lookahead size
    while ((nextToken.getType() != TTCN3ParserTokenTypes.SEMICOLON || nextToken.getType() != TTCN3ParserTokenTypes.ASSIGNMENTCHAR) && (tokIndex < tok.getTracker().getTokenCount())) {
      nextToken = tok.getTracker().getToken(tokIndex);
      for (int i=0; i < tokenTypes.length; i++) {
        if (nextToken.getType() == tokenTypes[i])
          return true;
      }
      tokIndex++;
      if (tokIndex == tok.getTracker().getTokenCount()){
        try {
          //ugly workaround to get token list to expand
          tokenLimit+=8;
          LA(tokenLimit);
        } catch (TokenStreamException e) {
          //the while condition should handle this
        }
      }
    }
    return false;
  }

	private boolean checkNextTokenTypeNOT(int[] tokenTypes, Token ltTok){
		TokenWithIndex tok = (TokenWithIndex) ltTok;
		int tokIndex = tok.getIndex();
		Token nextToken = tok.getTracker().getToken(tokIndex);
		for (int i=0; i<tokenTypes.length; i++){
			if (nextToken.getType() == tokenTypes[i])
				return false;
		}
		return true;
	}
	
	private long currentTime = System.currentTimeMillis();
	
	public void trackLineNodeTime(){
		long endTime = System.currentTimeMillis();
		long duration = endTime - currentTime;
		System.out.println(returnAST.getLine()+":"+returnAST.getText()+":"+duration+"ms");
		currentTime = endTime;
	}
	
}


//Proposed improvement by Gustaf Johansson
pr_TTCN3File{LocationAST col = null, endcol = null;}:
    (
        a:EOF {col = #a; endcol = #a;}
        |
        (
            b:pr_TTCN3Module ( c:pr_TTCN3Module )* {col = #b; endcol = #c;}
            EOF!
        )
    )
{
    ## = #([TTCN3File,"TTCN3File"], ##);
    if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


//-----------------------------------------------------
//                    TTCN Module  1.6.0
//-----------------------------------------------------

pr_TTCN3Module{LocationAST endCol = null;}:
	(
		col:pr_TTCN3ModuleKeyword!
		pr_TTCN3ModuleId
		pr_BeginChar!
		( pr_ModuleDefinitionsPart)?
		( pr_ModuleControlPart )?
		enda:pr_EndChar!{endCol = #enda;}
		( endc:pr_WithStatement{endCol = #endc;} )?
		( endb:pr_SemiColon{endCol = #endb;} )?
	)
{
	## = #([TTCN3Module,"TTCN3Module"], ##);
	if(## != null) ##.setLocation(#col, #endCol);
};


pr_TTCN3ModuleKeyword:
	col:MODULE
{
	if(## != null) ##.setLocation(#col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

// ASN.1 with TTCN-3 BNF Extension Start

pr_ObjectIdentifierKeyword:
	col:OBJID
{
	if (## != null) ##.setLocation(#col);
	if (## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ObjectIdentifierValue:
	OBJID^ BEGINCHAR! pr_ObjIdComponentList ENDCHAR!
;

pr_ObjIdComponentList:
	(pr_ObjIdComponent)+
;

pr_ObjIdComponent:
	(pr_ReferencedValue) => pr_ReferencedValue | pr_NameForm | pr_NumberForm | pr_NameAndNumberForm 
;

pr_NameForm:
	col:IDENTIFIER
{
	## = #([NameForm,"NameForm"], ##);
	if (## != null) ##.setLocation(#col);
	if (## != null) ##.setEndLocation(#col);
};

pr_NumberForm:
	col:NUMBER 
{
	## = #([NumberForm,"NumberForm"], ##);
	if (## != null) ##.setLocation(#col);
	if (## != null) ##.setEndLocation(#col);
};

pr_NameAndNumberForm:
	col:IDENTIFIER LPAREN! pr_NumberForm endcol:RPAREN!
{
	## = #([NameAndNumberForm,"NameAndNumberForm"], ##);
	if (## != null) ##.setLocation(#col);
	if (## != null) ##.setEndLocation(#endcol);
};



// ASN.1 with TTCN-3 BNF Extension End

pr_TTCN3ModuleId {LocationAST endcol=null;}:
	(
		col:pr_ModuleId {endcol=#col;} 
	)
{
	## = #([TTCN3ModuleId,"TTCN3ModuleId"], ##);
	if(## != null) ##.setLocation(#col, endcol);
};


pr_ModuleIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};


//------------------------------------------------------
//          Module Definitions Part      1.6.1  136
//------------------------------------------------------


pr_ModuleDefinitionsPart:
	col:pr_ModuleDefinitionsList
{
	## = #([ModuleDefinitionsPart,"ModuleDefinitionsPart"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_ModuleDefinitionsList {LocationAST endc=null;}:
	(
		col:pr_ModuleDefinition ( pr_SemiColon )?
		(
			endcol:pr_ModuleDefinition {endc=#endcol;}
			( pr_SemiColon )?
		)*
	)
{
	## = #([ModuleDefinitionList, "ModuleDefinitionList"], ##);
	if(## != null) ##.setLocation(#col, endc==null?#col:endc);
};


pr_ModuleDefinition{ LocationAST col=null; LocationAST cola=null; LocationAST endcol=null;}: //11
	(	
		(	(pr_Visibility)=>vis:pr_Visibility {cola = #vis;}
		)?
		(
			(a:pr_TypeDef { col = #a; }) 
		  | (b:pr_ConstDef { col = #b; }) 
		  | (c:pr_TemplateDef { col = #c; }) 
		  | (d:pr_FunctionDef { col = #d; }) 
		  | (e:pr_SignatureDef { col = #e; }) 
		  | (f:pr_TestcaseDef { col = #f; }) 
		  | (g:pr_AltstepDef { col = #g; }) 
		  | (h:pr_ImportDef { col = #h; }) 
		  | (i:pr_GroupDef { col = #i; }) 
		  | (j:pr_ExtFunctionDef { col = #j; }) 
		  | (k:pr_ExtConstDef { col = #k; }) 
		  | (l:pr_ModuleParDef {col = #l; })
		  | (m:pr_FriendModuleDef {col = #m; })
		)
		(
			n:pr_WithStatement {endcol = #n;}
		)?
	)
{
	## = #([ModuleDefinition,"ModuleDefinition"], ##);
	if(## != null) ##.setLocation(#cola==null?#col:#cola, endcol==null?#col:#endcol);
	//DEBUG
	//trackLineNodeTime();
};

pr_Visibility{LocationAST col=null;}:
	
	(	{checkNextTokenTypeNOT(new int[]{TTCN3ParserTokenTypes.FRIEND}, LT(2))}? a:pr_PublicKeyword {col = #a;}
	  | {checkNextTokenTypeNOT(new int[]{TTCN3ParserTokenTypes.GROUP,TTCN3ParserTokenTypes.FRIEND}, LT(2))}? b:pr_FriendKeyword {col = #b;}
	  | {checkNextTokenTypeNOT(new int[]{TTCN3ParserTokenTypes.GROUP}, LT(2))}? c:pr_PrivateKeyword {col = #c;}
	)
{
	## = #([Visibility,"Visibility"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};	

pr_PublicKeyword:
	col:PUBLIC
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_PrivateKeyword:
	col:PRIVATE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_FriendKeyword:
	col:FRIEND
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


//------------------------------------------------------
//   Typdef Definitions  1.6.1.1  136
// -----------------------------------------------------

pr_TypeDef:
	(
		col:pr_TypeDefKeyword!
		endcol:pr_TypeDefBody
	)
{
	## = #([TypeDef,"TypeDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_TypeDefBody {LocationAST col=null;}:
	(
		(
			pr_RecordKeyword | pr_UnionKeyword 
		  | pr_SetKeyword | pr_EnumKeyword  
		  | pr_PortKeyword | pr_ComponentKeyword
		) => a:pr_StructuredTypeDef { col = #a;}
	  | b:pr_SubTypeDef { col = #b;}
	)
{
	if(## != null) ##.setLocation(#col, #col);
};


pr_TypeDefKeyword:
	col:TYPE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_StructuredTypeDef{ LocationAST col=null;}://16
	(
		a:pr_RecordDef { col = #a; }
	  | b:pr_UnionDef { col = #b; }
	  | c:pr_SetDef { col = #c; }
	  | (pr_RecordKeyword (pr_StringLength)? pr_OfKeyword) => (d:pr_RecordOfDef { col = #d; })
	  | e:pr_SetOfDef { col = #e; }
	  | f:pr_EnumDef { col = #f; }
	  | g:pr_PortDef { col = #g; }
	  | h:pr_ComponentDef { col = #h; }
	)
{
	## = #([StructuredTypeDef,"StructuredTypeDef"], ##);
	if(col != null) ##.setLocation(#col, #col);
};

pr_RecordDef:
	(
		col:pr_RecordKeyword!
		endcol:pr_StructDefBody
	)
{
	## = #([RecordDef,"RecordDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_RecordKeyword:
	col:RECORD
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_StructDefBody{ LocationAST col=null;}://18
	(
		(
			(a:pr_StructTypeIdentifier {col= #a;} ( pr_StructDefFormalParList )?)
		  | b: pr_AddressKeyword  {col= #b;}
		)
		pr_BeginChar!
		(
			pr_StructFieldDef
			(
				pr_Comma!
				pr_StructFieldDef
			)*
		)?
		endcol:pr_EndChar!
	)
{
	## = #([StructDefBody,"StructDefBody"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_StructTypeIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_StructDefFormalParList:
	(
		col:pr_LParen!
		pr_StructDefFormalPar
		( pr_Comma! pr_StructDefFormalPar )*
		endcol:pr_RParen!
	)
{
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_StructDefFormalPar{ LocationAST col=null;}:
	(
		col:pr_FormalValuePar
	)
{
	if( ## != null) ##.setLocation(#col, #col);
};


pr_StructFieldDef{LocationAST col=null; LocationAST endcol=null;}:
	(
		(
			e:pr_Type {col=#e ;}
		  | f:pr_NestedTypeDef {col = #f ;}
		)
		a:pr_StructFieldIdentifier { endcol = #a;}
		( b:pr_ArrayDef { endcol = #b;} )?
		( c:pr_SubTypeSpec { endcol = #c;} )?
		( d:pr_OptionalKeyword { endcol = #d;} )?
	)
{
	## = #([StructFieldDef,"StructFieldDef"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};

pr_NestedTypeDef{LocationAST col = null;}:
	(
		(pr_RecordKeyword pr_BeginChar) => a:pr_NestedRecordDef {col=#a ;}
	  | b:pr_NestedUnionDef {col=#b ;}
	  | c:pr_NestedSetDef {col=#c ;}
	  | (pr_RecordKeyword (pr_StringLength)? pr_OfKeyword) => d:pr_NestedRecordOfDef {col=#d ;}
	  | e:pr_NestedSetOfDef {col=#e ;}
	  | f:pr_NestedEnumDef {col=#f ;}
	)
{
	## = #([NestedTypeDef,"NestedTypeDef"], ##);
	if(## != null) ##.setLocation(#col,#col);
};

pr_NestedRecordDef :
	( 
		col:pr_RecordKeyword!
		pr_BeginChar!
		(pr_StructFieldDef (pr_Comma! pr_StructFieldDef)*)?
		endcol:pr_EndChar!
	)
{
	## = #([NestedRecordDef,"NestedRecordDef"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};

pr_NestedUnionDef :
	( 
		col:pr_UnionKeyword!
		pr_BeginChar!
		(pr_UnionFieldDef (pr_Comma! pr_UnionFieldDef)*)
		endcol:pr_EndChar!
	)
{
	## = #([NestedUnionDef,"NestedUnionDef"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};

pr_NestedSetDef :
	( 
		col:pr_SetKeyword!
		pr_BeginChar!
		(pr_StructFieldDef (pr_Comma! pr_StructFieldDef)*)?
		endcol:pr_EndChar!
	)
{
	## = #([NestedSetDef,"NestedSetDef"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};

pr_NestedRecordOfDef {LocationAST endcol = null;}:
	(
		col:pr_RecordKeyword!
		(pr_StringLength)? pr_OfKeyword!
		(
			a:pr_Type {endcol = #a ;}
		  | b:pr_NestedTypeDef {endcol=#b;}
		)
	)
{
	## = #([NestedRecordOfDef ,"NestedRecordOfDef "], ##);
	if(## != null) ##.setLocation(#col,endcol);
};

pr_NestedSetOfDef {LocationAST endcol = null;}:
	(  
		col:pr_SetKeyword!
		(pr_StringLength)? pr_OfKeyword!
		(
			a:pr_Type {endcol = #a ;}
		  | b:pr_NestedTypeDef {endcol = #b;}
		)
	)
{
	## = #([NestedSetOfDef,"NestedSetOfDef"], ##);
	if(## != null) ##.setLocation(#col,endcol);
};

pr_NestedEnumDef :
	(	
		col:pr_EnumKeyword!
		pr_BeginChar!
		pr_EnumerationList
		endcol:pr_EndChar!
	)
{
	## = #([NestedEnumDef,"NestedEnumDef"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};



pr_StructFieldIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};


pr_OptionalKeyword:
	col:OPTIONAL
{
//	## = #([OptionalKeyword,"optional"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_UnionDef:
	(
		col:pr_UnionKeyword!
		endcol:pr_UnionDefBody
	)
{
	## = #([UnionDef,"UnionDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_UnionKeyword://33
	col:UNION
{
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
}
;

pr_UnionDefBody{ LocationAST col=null;}:
	(
		(
			a:pr_StructTypeIdentifier { col = #a; }
			( pr_StructDefFormalParList )?
		)
	  | b:pr_AddressKeyword { col = #b; }
	)
	(
		pr_BeginChar!
		pr_UnionFieldDef
		(
			pr_Comma!
			pr_UnionFieldDef
		)*
		endcol:pr_EndChar!
	)
{
	## = #([UnionDefBody,"UnionDefBody"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_UnionFieldDef{ LocationAST col=null;}:
	(	(	d:pr_Type {col = #d;}
	  	  | e:pr_NestedTypeDef {col = #e;}
	  	)
		cola:pr_StructFieldIdentifier
		(	colb:pr_ArrayDef 
		)?
		(	colc:pr_SubTypeSpec 
		)?
	)
{
	## = #([UnionFieldDef,"UnionFieldDef"], ##);
	if(#colc != null) {
		if(## != null) ##.setLocation(#col, #colc);
	} else if(#colb != null) {
		if(## != null) ##.setLocation(#col, #colb);
	} else {
		if(## != null) ##.setLocation(#col, #cola);
	}
};

pr_SetDef:
	(
		col:pr_SetKeyword!
		endcol:pr_StructDefBody
	)
{
	## = #([SetDef,"SetDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_SetKeyword:
	col:SET
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_RecordOfDef:
	(
		col:pr_RecordKeyword!
		( pr_StringLength )?
		pr_OfKeyword!
		endcol:pr_StructOfDefBody
	)
{
	## = #([RecordOfDef,"RecordOfDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_OfKeyword:
	col:OF
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_StructOfDefBody{LocationAST col=null;LocationAST endcol=null;}:
	(
		(d:pr_Type{col=#d;}|e:pr_NestedTypeDef{col=#e;})
		(
			a:pr_StructTypeIdentifier{ endcol = #a; }
		  | b:pr_AddressKeyword{ endcol = #b; }
		)
		( c:pr_SubTypeSpec { endcol = #c; } )?
	)
{
	## = #([StructOfDefBody,"StructOfDefBody"], ##);
	if(## != null) ##.setLocation(#col, endcol);
};

pr_SetOfDef:
	(
		col:pr_SetKeyword!
		( pr_StringLength )?
		pr_OfKeyword!
		endcol:pr_StructOfDefBody
	)
{
	## = #([SetOfDef,"SetOfDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_EnumDef://42
	(
		col:pr_EnumKeyword!
		(
			pr_EnumTypeIdentifier
			|
			pr_AddressKeyword
		)
		pr_BeginChar!
		pr_EnumerationList
		endcol:pr_EndChar!
	)
{
	## = #([EnumDef,"EnumDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
} ;


pr_EnumKeyword:
	col:ENUMERATED
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};



pr_EnumTypeIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_EnumerationList:
	(
		col:pr_Enumeration
		(
			pr_Comma!
			pr_Enumeration
		)*
	)
{
	## = #([EnumerationList,"EnumerationList"], ##);
	if(## != null) ##.setLocation(#col, #col);

};

pr_Enumeration:
	(
		col:pr_EnumerationIdentifier
		(
			pr_LParen!
			(pr_Minus)?
			pr_Number
			pr_RParen!
		)?
	)
{
	## = #([Enumeration,"Enumeration"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_EnumerationIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_SubTypeDef{ LocationAST endcol=null;}:
	(
		col:pr_Type
		(
			a:pr_SubTypeIdentifier { endcol = #a; }
		  | b:pr_AddressKeyword { endcol = #b; identifierCache.put(#b.getOffset(), #b.getEndOffset(), #b);}
		)
		( c:pr_ArrayDef { endcol = #c; }  )?
		( d:pr_SubTypeSpec { endcol = #d; }  )?
	)
{
	## = #([SubTypeDef,"SubTypeDef"], ##);
	if(## != null) ##.setLocation(#col, endcol);
};

pr_SubTypeIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_SubTypeSpec{ LocationAST col=null;}:
	(
		(pr_AllowedValues) => ( a:pr_AllowedValues (pr_StringLength)?{ col = #a; })
	  | b:pr_StringLength { col = #b; }
	)
{
	## = #([SubTypeSpec,"SubTypeSpec"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_AllowedValues:
	(
		col:pr_LParen!
		(( pr_ValueOrRange (pr_Comma! pr_ValueOrRange)* ) | pr_CharStringMatch)
		endcol:pr_RParen!
	)
{
	## = #([AllowedValues,"AllowedValues"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ValueOrRange{LocationAST col=null;}://52
	(
	 	( pr_LowerBound RANGEOP ) =>
		  a:pr_RangeDef{ col = #a;}
	  	| (pr_ConstantExpression) => b:pr_ConstantExpression{ col = #b;}
	  	| c:pr_Type {col = #c;}
	)
{
	## = #([ValueOrRange,"ValueOrRange"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_RangeDef://53
	(
		col:pr_LowerBound
		pr_RangeOp!
		endcol:pr_UpperBound
	)
{
	## = #([RangeDef,"RangeDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_StringLength:
	(
		col:pr_LengthKeyword!
		pr_LParen!
		pr_SingleConstExpression
		(
			pr_RangeOp!
			pr_UpperBound
		)?
		endcol:pr_RParen!
	)
{
	## = #([StringLength,"StringLength"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_LengthKeyword:
	col:LENGTH
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_PortType:
	(
		cola:pr_GlobalModuleId
		colb:pr_Dot!
	)?
	endcol:pr_PortTypeIdentifier
{
	## = #([PortType,"PortType"], ##);
	if(#cola != null) {
	if(## != null) ##.setLocation(#cola, #endcol);
	} else if (#colb != null) {
	if(## != null) ##.setLocation(#colb, #endcol);
	} else {
	if(## != null) ##.setLocation(#endcol, #endcol);
	}
};

pr_PortDef:
	(
		col:pr_PortKeyword!
		endcol:pr_PortDefBody
	)
{
	## = #([PortDef,"PortDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_PortDefBody:
	(
		col:pr_PortTypeIdentifier
		endcol:pr_PortDefAttribs
	)
{
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_PortKeyword:
	col:PORT!
{
	## = #([PortKeyword,"port"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};


pr_PortTypeIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_PortDefAttribs{ LocationAST col=null;}:
	(
		a:pr_MessageAttribs { col = #a; }
		|
		b:pr_ProcedureAttribs { col = #b; }
		|
		c:pr_MixedAttribs { col = #c; }
	)
{
	## = #([PortDefAttribs,"PortDefAttribs"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_MessageAttribs://62
	(	//why is this any different than ProcedureAttribs?
		col:pr_MessageKeyword!
		pr_BeginChar!
		(	( 	pr_AddressDecl
			  |	pr_MessageList
			  | pr_ConfigParamDef
			)
			( pr_SemiColon )?
		)+
		endcol:pr_EndChar!
	)
{
	## = #([MessageAttribs,"MessageAttribs"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_AddressDecl:
	(
		col:pr_AddressKeyword!
		endcol:pr_Type
	)
{
	## = #([AddressDecl,"AddressDecl"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ConfigParamDef{ LocationAST col=null;}:
	(
		a:pr_MapParamDef { col = #a; }
	  |	b:pr_UnmapParamDef { col = #b; }
	)
{
	## = #([ConfigParamDef,"ConfigParamDef"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_MapParamDef:
	(
		col:pr_MapKeyword!
		pr_ParaKeyword!
		pr_LParen!
		pr_FormalValuePar
		(	pr_Comma!
			pr_FormalValuePar
		)*
		endcol:pr_RParen!
	)

{
	## = #([MapParamDef,"MapParamDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_UnmapParamDef:
	(
		col:pr_UnmapKeyword!
		pr_ParaKeyword!
		pr_LParen!
		pr_FormalValuePar
		(	pr_Comma!
			pr_FormalValuePar
		)*
		endcol:pr_RParen!
	)

{
	## = #([UnmapParamDef,"UnmapParamDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

	

pr_MessageList:
	(
		col:pr_Direction
		endcol:pr_AllOrTypeList
	)
{
	## = #([MessageList,"MessageList"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_Direction{ LocationAST col=null;}:
	(
		a:pr_InParKeyword { col = #a; }
	  | b:pr_OutParKeyword { col = #b; }
	  | c:pr_InOutParKeyword { col = #c; }
	)
{
	## = #([Direction,"Direction"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_MessageKeyword:
	col:MESSAGE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_AllOrTypeList{ LocationAST col=null;}:
	(
		a:pr_AllKeyword { col = #a; }
	  | b:pr_TypeList { col = #b; }
	)
{
//  ## = #([AllOrTypeList,"AllOrTypeList"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_AllKeyword:
	col:ALL!
{
	## = #([AllKeyword,"all"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};


pr_TypeList:
	(
		col:pr_Type
		(
			pr_Comma!
			endcol:pr_Type
		)*
	)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_ProcedureAttribs:
	(
		col:pr_ProcedureKeyword!
		pr_BeginChar!
		(	( 	pr_AddressDecl
			  |	pr_ProcedureList
			  | pr_ConfigParamDef
			)
			( pr_SemiColon )?
		)+
		endcol:pr_EndChar!
	)
{
	## = #([ProcedureAttribs,"ProcedureAttribs"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ProcedureKeyword:
	col:PROCEDURE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ProcedureList:
	(
		col:pr_Direction
		endcol:pr_AllOrSignatureList
	)
{
	## = #([ProcedureList,"ProcedureList"], ##); if(## != null) ##.setLocation(#col, #endcol);
};

pr_AllOrSignatureList{LocationAST col=null;}://72
	(
		a:pr_AllKeyword{ col = #a;}
	  | b:pr_SignatureList{ col = #b;}
	)
{
//    ## = #([AllOrSignatureList,"AllOrSignatureList"], ##); 	if(## != null) ##.setLocation(col, col);
};

pr_SignatureList:
	(
		col:pr_Signature
		(
			pr_Comma!
			endcol:pr_Signature
		)*
	)
{
	## = #([SignatureList,"SignatureList"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_MixedAttribs:
	(
		col:pr_MixedKeyword!
		pr_BeginChar!
		(	( 	pr_AddressDecl
			  |	pr_MixedList
			  | pr_ConfigParamDef
			)
			( pr_SemiColon )?
		)+
		endcol:pr_EndChar!
	)
{
	## = #([MixedAttribs,"MixedAttribs"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_MixedKeyword:
	col:MIXED
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_MixedList:
	(
		col:pr_Direction
		endcol:pr_ProcOrTypeList
	)
{
	## = #([MixedList,"MixedList"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ProcOrTypeList{LocationAST col=null;}:
	(
		a:pr_AllKeyword{col = #a;}
	  | (
			b:pr_ProcOrType{col = #b;}
			(
				pr_Comma!
				pr_ProcOrType
			)*
		)
	)
{
   // ## = #([ProcOrTypeList,"ProcOrTypeList"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_ProcOrType{LocationAST col=null;}:
	(
		(pr_Type) =>   a:pr_Type{col = #a;}
					 | b:pr_Signature{col = #b;}
	)
{
	## = #([ProcOrType,"ProcOrType"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_ComponentDef:
	(
		col:pr_ComponentKeyword!
		pr_ComponentTypeIdentifier
		(pr_ExtendsKeyword! pr_ComponentType (pr_Comma! pr_ComponentType)*)?
		pr_BeginChar!
		( pr_ComponentDefList )?
		endcol:pr_EndChar!
	)
{
	## = #([ComponentDef,"ComponentDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ExtendsKeyword :
	col:EXTENDS
{
	## = #([ExtendsKeyword ,"extends"], ##);
	if(## != null) ##.setLocation(#col, #col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ComponentKeyword:
	col:COMPONENT
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ComponentType:
	(
		(
			( pr_GlobalModuleId pr_Dot ) => ( pr_GlobalModuleId pr_Dot! )
		  | // XXX: empty alternative?
		)
		col:pr_ComponentTypeIdentifier
	)
{
	## = #([ComponentType,"ComponentType"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_ComponentTypeIdentifier://83
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};


pr_ComponentDefList{LocationAST endcol=null;}:
	(
		col:pr_ComponentElementDef
		(	a:pr_WithStatement {endcol=#a;}
		)?
		( pr_SemiColon )?
		(
	  		b:pr_ComponentElementDef{endcol=#b;}
	  		(	c:pr_WithStatement {endcol=#c;}
			)?
	  		( pr_SemiColon )?
		)*
	)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_ComponentElementDef{ LocationAST col=null;}:
	(
		a:pr_PortInstance   { col = #a; }
	  | b:pr_VarInstance    { col = #b; }
	  | c:pr_TimerInstance  { col = #c; }
	  | d:pr_ConstDef       { col = #d; }
	)
{
	## = #([ComponentElementDef,"ComponentElementDef"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_PortInstance:
	(
		col:pr_PortKeyword!
		pr_PortType
		endcola:pr_PortElement
		(
			pr_Comma!
			endcolb:pr_PortElement
		)*
	)
{
	## = #([PortInstance,"PortInstance"], ##);
	if(## != null) ##.setLocation(#col, #endcolb==null?#endcola:#endcolb);
};

pr_PortElement:
	(
		col:pr_PortIdentifier
		( endcol:pr_ArrayDef )?
	)
{
	## = #([PortElement,"PortElement"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_PortIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};



//-----------------------------------------------------/
// Constant Definitions               1.6.1.2
//-----------------------------------------------------/

pr_ConstDef:
	(
		col:pr_ConstKeyword!
		pr_Type
		endcol:pr_ConstList
	)
{
	## = #([ConstDef,"ConstDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ConstList:
	(
		col:pr_SingleConstDef
		(
			pr_Comma!
			endcol:pr_SingleConstDef
		)*
	)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_SingleConstDef:
	(
		col:pr_ConstIdentifier
		( pr_ArrayDef )?
		pr_AssignmentChar!
		endcol:pr_ConstantExpression
	)
{
	## = #([SingleConstDef,"SingleConstDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ConstKeyword:
	col:CONST
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_ConstIdentifier://93
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

//-----------------------------------------------------/
// Template Definitions     1.6.1.3
//-----------------------------------------------------/
pr_TemplateDef:
	(
		col:pr_TemplateKeyword!
		(	pr_TemplateRestriction
		)?
		pr_BaseTemplate
		( pr_DerivedDef )?
		pr_AssignmentChar!
		endcol:pr_TemplateBody
	)
{
	## = #([TemplateDef,"TemplateDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_BaseTemplate{LocationAST col=null;}:
	(	(	(	pr_Type
			) => a:pr_Type 
				 {col=#a ;}
		  | b:pr_Signature  
		    {col=#b;}
		)
		endcola:pr_TemplateIdentifier
		(	pr_LParen!
			pr_TemplateFormalParList
			endcolb:pr_RParen!
		)?
	)
{
	## = #([BaseTemplate,"BaseTemplate"], ##);
	if(## != null) ##.setLocation(col, #endcolb==null?#endcola:#endcolb);
};


pr_TemplateKeyword:
	col:TEMPLATE!
{
	## = #([TemplateKeyword,"template"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};


pr_TemplateIdentifier:
	col:pr_Identifier
{
	if( ## != null) ##.setLocation(#col, #col);
};

pr_DerivedDef:
	(
		col:pr_ModifiesKeyword!
		endcol:pr_TemplateRef
	)
{
	## = #([DerivedDef,"DerivedDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ModifiesKeyword://99
	col:MODIFIES!
{
	## = #([ModifiesKeyword,"modifies"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};


//ANYWHERE NEAR CORRECT?
pr_TemplateRestriction:
	col:pr_LParen!
	(	pr_OmitKeyword
		//pr_OmitValue //changed for conformity
	  | pr_ValueKeyword //added as per v3.4.1
	  | pr_PresentKeyword //added as per v3.4.1
	)
	endcol:pr_RParen!
{
	## = #([TemplateRestriction,"TemplateRestriction"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
	if(## != null) ##.setEndLocation(#endcol);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##); //is this necessary?

};


pr_RestrictedTemplate{LocationAST col = null;}:
	(	(	a:pr_OmitKeyword //further expansion is necessary 
			{col = #a;} 
		)
	  | (	b:pr_TemplateKeyword 
	  		{col = #b;}
	  		pr_TemplateRestriction //further expansion is necessary as well
	  	)
	)
{
	## = #([RestrictedTemplate,"RestrictedTemplate"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##); //is this necessary
};




pr_TemplateFormalParList:
	(
		col:pr_TemplateFormalPar
		(
			pr_Comma!
			endcol:pr_TemplateFormalPar
		)*
	)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_TemplateFormalPar{LocationAST col=null;}:
/*	
	(	(	pr_FormalValuePar 
		) => a:pr_FormalValuePar
		 { col = #a;}
	  | (	pr_FormalTemplatePar 
	  	) => b:pr_FormalTemplatePar
	  		 { col = #b;}
	)
*/	
//replaced by (equivalent as in test case formal par)

	(	( 	pr_FormalTemplatePar 
		) => a:pr_FormalTemplatePar
			 { col=#a; }
	  | b:pr_FormalValuePar
	  	{ col=#b; }
	)
	
{
	if(## != null) ##.setLocation(#col, #col);
}
;


pr_TemplateBody{ LocationAST col=null;}:
// -------------------------------------------------------------------------------------------
// note: this is the correct variant of the original rule
//
//    102. TemplateBody ::= (SimpleSpec | FieldSpecList | ArrayValueOrAttrib) | [ExtraMatchingAttributes]
//
// -------------------------------------------------------------------------------------------
	(	
		(		
			a:pr_SimpleSpec { col = #a; }
		  | ( pr_BeginChar pr_FieldReference pr_AssignmentChar ) => b:pr_FieldSpecList { col = #b; }
		  | ( pr_BeginChar pr_EndChar ) => c:pr_FieldSpecList { col = #c; }
//		  | ( pr_FieldSpecList) => c:pr_FieldSpecList { col = #c; }
		  | e:pr_ArrayValueOrAttrib { col = #e; }
		)
		(	d:pr_ExtraMatchingAttributes{col=#d ;}
		)?
	)
{
	## = #([TemplateBody,"TemplateBody"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_SimpleSpec {LocationAST col=null,endc=null;}://103
	col:pr_SingleValueOrAttrib
	//alternative implementation in SingleValueOrAttrib	

	//here be performance problems! beware
	/*(
		//need a shorter or more efficient predicate here! 
		//but how?
		(pr_SingleExpression)=>
		(	a:pr_SingleExpression {col = #a;}
			(	STRINGOP!
				c:pr_SimpleTemplateSpec {endc = #c;}
			)?
		)
		|/* * /
		b:pr_SimpleTemplateSpec {endc = #b;}
		
	)/**/
{
	//if(## != null) ##.setLocation(#col==null?#endc:#col, #endc);
	if(## != null) ##.setLocation(#col, #endc==null?#col:#endc);
	
};

pr_SimpleTemplateSpec{LocationAST col=null,endc=null;}:
	(
//overlaps with other alternative, makes no sense, propose change?	
//seems like proposed change didn't go through or was incorrect
//when removing CharStringMatch from MatchingSymbol works fine,
//but is this really intended
//=> 
//TODO: clarify above, for now CharStringMatch is disabled in MatchingSymbol 
//=> does not seem to be an option
//		(	a:pr_CharStringMatch {col = #a;}
//		)
//	  | 
	  	(	b:pr_SingleTemplateExpression {col = #b;}
		)
	)
{
	if(## != null) ##.setLocation(#col, #endc==null?#col:#endc);
}
;

pr_SingleTemplateExpression{LocationAST col=null,endc=null;}:
	(	
		(	a:pr_MatchingSymbol {col = #a;}
		)
	  	|	
	  	(	//b:pr_TemplateIdentifier{col = #b;} 
	  		//	(	d:pr_TemplateActualParList{endc=#d;}
	  		//	)? 
			//the above can substitute the rule below as it may cause issues due to greediness
	  		b:pr_TemplateRefWithParList {col = #b;}
			(	c:pr_ExtendedFieldReference {endc = #c;}
			)?
	  	)
	)
	//moved from SimpleTemplateSpec (which itself could be removed now)
	(	STRINGOP!
		e:pr_SimpleSpec {endc = #e;}
	)?
	
{
	if(## != null) ##.setLocation(#col, #endc==null?#col:#endc);
}
;

/**/
pr_FieldSpecList:
	(
		col:pr_BeginChar!
		//(
			pr_FieldSpec
			(
				pr_Comma!
				pr_FieldSpec
			)*
		//)?
		endcol:pr_EndChar!
	)
{
	## = #([FieldSpecList,"FieldSpecList"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_FieldSpec{ LocationAST endcol=null;}:
	(
		col:pr_FieldReference
		pr_AssignmentChar!
		(	
		    (pr_Minus) => b:pr_Minus { endcol = #b; }
		  | a:pr_TemplateBody { endcol = #a; }
		)
	)
{
	## = #([FieldSpec,"FieldSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_FieldReference{ LocationAST col=null;}:
	(
		(pr_StructFieldRef) => a:pr_StructFieldRef { col = #a; }
	  | b:pr_ArrayOrBitRef { col = #b; }
	  | c:pr_ParRef { col = #c;}
	)
{
	## = #([FieldReference,"FieldReference"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_StructFieldRef{ LocationAST col=null;}:
   (
	   (pr_StructFieldIdentifier) => a:pr_StructFieldIdentifier { col = #a; }
	 | b:pr_PredefinedType { col = #b; }
	 | c:pr_TypeReference { col = #c; }
   )
{
	//	## = #([StructFieldRef,"StructFieldRef"], ##);
	if(## != null) ##.setLocation(#col, #col);
}
;


pr_ParRef:
	col:pr_SignatureParIdentifier
{
	if(## != null) ##.setLocation(#col, #col);
};


pr_SignatureParIdentifier:
	col:pr_ValueParIdentifier
{
	if(## != null) ##.setLocation(#col, #col);
};


pr_ArrayOrBitRef:
	(
		col:pr_SquareOpen!
//		pr_FieldOrBitNumber
		pr_SingleExpression
		endcol:pr_SquareClose!
	)
{
	## = #([ArrayOrBitRef,"ArrayOrBitRef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_FieldOrBitNumber:
	col:pr_SingleExpression
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_SingleValueOrAttrib{ LocationAST col=null; LocationAST endcol=null;}://112
	(
		//ugly hack, check if it operates correctly
		//reduces processing itme by a factor of 10 though
			//=>does not really work, e.g. causes parsing issues
			//  template charstring cr_GenericParam_NoValue := ("", omit);    // i.e. either an empty string or omit
			

		//this reordering does seem to solve an issue,
		//BUT!!! it increases processing time monumentally by a factor of 6
	  //{!checkMatchingSymbolToken(new int[]{TTCN3ParserTokenTypes.STAR,TTCN3ParserTokenTypes.HSTRINGMATCH,TTCN3ParserTokenTypes.OSTRINGMATCH,TTCN3ParserTokenTypes.BSTRINGMATCH,TTCN3ParserTokenTypes.QUESTIONMARK,TTCN3ParserTokenTypes.PATTERN,TTCN3ParserTokenTypes.COMPLEMENT,TTCN3ParserTokenTypes.SUBSET,TTCN3ParserTokenTypes.SUPERSET,TTCN3ParserTokenTypes.IFPRESENT,TTCN3ParserTokenTypes.LENGTH},LT(1))}? 

	  //( pr_SingleExpression ) =>
	  //b:pr_SingleExpression { col = #b; }
	  //|
/*				
		( pr_MatchingSymbol ) => 
		( a: pr_MatchingSymbol  { col = #a; } )
//	  | ( pr_SingleExpression ) => ( b:pr_SingleExpression { col = #b; } )
//	  | ( (IDENTIFIER DOT)? IDENTIFIER ) => ( c:pr_TemplateRefWithParList { col = #c; } )
	  | c:pr_SingleExpression { col = #c; }
*/

		//This seems to address the expressions problem while retaining the runtime 
		//and starting parentheses seem to be the key ambiguity
		
		(pr_LParen) => 
		(
		    ( pr_SingleExpression ) => ( a:pr_SingleExpression { col = #a; } )
		  |	( b: pr_MatchingSymbol  { col = #b; } )
		)
	    |
		(
			( pr_MatchingSymbol ) => ( c: pr_MatchingSymbol  { col = #c; } )
		  | ( d:pr_SingleExpression { col = #d; } )
		)



	)
	//potentially somewhat flakier alternative to the mess that is SimpleSpec
	//works so far!
	(	STRINGOP!
		e:pr_SimpleSpec {endcol = #e;}
	)? 
{
	## = #([SingleValueOrAttrib,"SingleValueOrAttrib"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_ArrayValueOrAttrib:
	(
		col:pr_BeginChar!
		(	pr_ArrayElementSpecList
		)?
		endcol:pr_EndChar!
	)
{
	## = #([ArrayValueOrAttrib,"ArrayValueOrAttrib"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ArrayElementSpecList:
	(
		col:pr_ArrayElementSpec
		(
			pr_Comma!
			endcol:pr_ArrayElementSpec
		)*
	)
{

	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_ArrayElementSpec{LocationAST col=null;}:
	(
		// XXX: shorten predicate?
		( pr_TemplateBody ) => a:pr_TemplateBody { col = #a;}
	  | b:pr_NotUsedSymbol{ col = #b;}
	  | c:pr_PermutationMatch  {col = #c ;}
	)
{
	## = #([ArrayElementSpec,"ArrayElementSpec"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_NotUsedSymbol:
	col:pr_Dash!
{
	## = #([NotUsedSymbol,"NotUsedSymbol"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_MatchingSymbol{ LocationAST col=null,endcol=null;}:
	( 
		a:pr_Complement { col = #a; }
	//| ( pr_ValueOrAttribList )=> b:pr_ValueOrAttribList { col = #b; }
	  | ( pr_ListOfTemplates )=> b:pr_ListOfTemplates { col = #b; }
	  | ( c:pr_AnyValue { col = #c; }
	  		//this could also be matched by the ExtraMatchingAttributes
	  		//setting option greedy seems to do the trick for the warning
	  		(	options { greedy = true; } : 
	  			m:pr_WildcardLengthMatch{endcol = #m;} 
	  		)?
	  	)
	  | ( d:pr_AnyOrOmit { col = #d; }
	  	 	(	options { greedy = true; } : 
	  	 		n:pr_WildcardLengthMatch{endcol = #n;} 
	  		)?
	    )
	  | e:pr_Range { col = #e; }
	  | g:pr_BitStringMatch { col = #g; }
	  | h:pr_HexStringMatch { col = #h; }
	  | i:pr_OctetStringMatch { col = #i; }
	  | j:pr_CharStringMatch { col = #j; }
	  | k:pr_SubsetMatch { col = #k; }
	  | l:pr_SupersetMatch { col = #l; }
	)
{
	## = #([MatchingSymbol,"MatchingSymbol"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_ExtraMatchingAttributes{ LocationAST col=null;LocationAST endcol=null;}://118
	(
		(pr_LengthMatch pr_IfPresentMatch) => (c:pr_LengthMatch d:pr_IfPresentMatch {col=#c;endcol=#d;})
	  | (
			a:pr_LengthMatch {col=#a;}
		  | b:pr_IfPresentMatch {col=#b;}
		)
	)
{
	## = #([ExtraMatchingAttributes,"ExtraMatchingAttributes"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_BitStringMatch{LocationAST endcol=null;}:
	(	//col:BSTRING | endcol:BSTRINGMATCH
		col:BSTRINGMATCH
	)
{
	## = #([BitStringMatch,"BitStringMatch"], ##);
	//if(## != null) ##.setLocation(#endcol==null?#col:#endcol, #endcol==null?#col:#endcol);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_SubsetMatch://130
	(
		col:pr_SubsetKeyword!
		//endcol:pr_ValueOrAttribList
		endcol:pr_ListOfTemplates
	)
{
	## = #([SubsetMatch,"SubsetMatch"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_SubsetKeyword:
	col:SUBSET
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_SupersetMatch:
	(
		col:pr_SupersetKeyword!
		//endcol:pr_ValueOrAttribList
		endcol:pr_ListOfTemplates
	)
{
	## = #([SupersetMatch,"SupersetMatch"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_SupersetKeyword:
	col:SUPERSET
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_HexStringMatch:
	(
		//col:HSTRING | endcol:HSTRINGMATCH
		col:HSTRINGMATCH
	)
{
	## = #([HexStringMatch,"HexStringMatch"], ##);
	//if(## != null) ##.setLocation(#endcol==null?#col:#endcol, #endcol==null?#col:#endcol);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_PermutationMatch:
	(
		col:pr_PermutationKeyword!
		//endcol:pr_PermutationList
		endcol:pr_ListOfTemplates
	)
{
	## = #([PermutationMatch,"PermutationMatch"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

/*
pr_PermutationList:
	(
		col:pr_LParen!
		pr_TemplateBody
		( pr_Comma! pr_TemplateBody )*
		endcol:pr_RParen!
	)
{
	if(## != null) ##.setLocation(#col, #endcol);
};
*/


pr_PermutationKeyword:
	col:PERMUTATION
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_OctetStringMatch:
	(	
		//TODO: this seems to break other cases, investigate a proper fix
		//col:OSTRING | endcol:OSTRINGMATCH
		col:OSTRINGMATCH
	)
{
	## = #([OctetStringMatch,"OctetStringMatch"], ##);
	//if(## != null) ##.setLocation(#endcol==null?#col:#endcol, #endcol==null?#col:#endcol);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_CharStringMatch{LocationAST endcol=null;}:
	(
		col: pr_PatternKeyword!
		(
			a: CSTRING {endcol = #a;}
		  | b: pr_ReferencedValue {endcol = #b;}
		)
	)
	//TODO: make sure this don't cause no further trouble
	(options { greedy = true; } : 
		STRINGOP!
		(
			c:CSTRING {endcol = #c;}
		  | d:pr_ReferencedValue {endcol = #d;} 
		)
	)*
{
	## = #([CharStringMatch,"CharStringMatch"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PatternKeyword:
	col:PATTERN
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_Complement{ LocationAST endcol = null; }:
	(	col:pr_ComplementKeyword!
//      pr_ValueList
//		(	a:pr_ValueList { endcol = #a; } //changed to ValueOrAttribList
		//(	a:pr_ValueOrAttribListComplement { endcol = #a; }
		//)
		a:pr_ListOfTemplates { endcol = #a; }
	)
{
	## = #([Complement,"Complement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ComplementKeyword:
	col: COMPLEMENT
{
	if(## != null) ##.setLocation(#col, #col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_OmitKeyword:
	col:OMIT
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_PresentKeyword:
	col:PRESENT
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};



pr_AnyValue:
	col:pr_QuestionMark!
{
	## = #([AnyValue,"AnyValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_AnyOrOmit:
	col:pr_Star!
{
	## = #([AnyOrOmit,"AnyOrOmit"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_ListOfTemplates: //
	(
		col:pr_LParen!
		pr_TemplateListItem
		(
			pr_Comma!
			pr_TemplateListItem
		)* //TODO: is this really not a +??
		endcol: pr_RParen!
	)
{
	## = #([ListOfTemplates,"ListOfTemplates"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_TemplateListItem {LocationAST col = null;}: //
	(
	  	a:pr_TemplateBody {col = #a;}
	  | b:pr_AllElementsFrom {col = #b;}
	)
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_AllElementsFrom: //
	(
		col:pr_AllKeyword! 
	  	pr_FromKeyword!
	  	endcol:pr_TemplateBody
	)
{
	## = #([AllElementsFrom,"AllElementsFrom"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ValueOrAttribList: //a.k.a. TemplateList
	(
		col:pr_LParen!
		pr_TemplateBody
		(
			pr_Comma!
			pr_TemplateBody
		)+
		endcol: pr_RParen!
	)
{
	## = #([ValueOrAttribList,"ValueOrAttribList"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ValueOrAttribListComplement:
	(
		col:pr_LParen!
		pr_TemplateBody
		(
			pr_Comma!
			pr_TemplateBody
		)*
		endcol: pr_RParen!
	)
{
	## = #([ValueOrAttribList,"ValueOrAttribList"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

//TODO: this shall be removed as per v4.2.1
pr_ValueList:
	(
		col:pr_LParen!
		pr_ConstantExpression
		(
			pr_Comma!
			pr_ConstantExpression
		)*
		endcol:pr_RParen!
	)
{
	## = #([ValueList,"ValueList"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_LengthMatch:
	col:pr_StringLength
{
//  ## = #([LengthMatch,"LengthMatch"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_IfPresentMatch://141
	col:IFPRESENT!
{
	## = #([IfPresentMatch,"IfPresentMatch"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_WildcardLengthMatch:
	col:pr_LengthKeyword!
	pr_LParen!
	pr_ConstantExpression
	endcol:pr_RParen!
{
	## = #([WildcardLengthMatch,"WildcardLengthMatch"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_Range:
	(
		col:pr_LParen!
		pr_LowerBound
		pr_RangeOp!
		pr_UpperBound
		endcol:pr_RParen!
	)
{
	## = #([Range,"Range"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

//proposed bound would allow minus infinity as an upper bound syntactically
//=> stick to old bound rules with extension
/*
pr_Bound{LocationAST col=null,endcol=null;}:
	(	(	(	a:pr_ExclamationMark{col=#a;}
			)?
			b:pr_SingleExpression{endcol=#b;}
		)
	  | (	(	c:pr_Minus{col=#c;}
	  		)?
	  		d:pr_InfinityKeyword{endcol=#d;}
	  	)
	)
{
	## = #([Bound,"Bound"], ##);
	if(## != null) ##.setLocation(#col==null?#endcol:#col, #endcol);
};
*/

pr_LowerBound{LocationAST col=null,endcol=null;}:
	(c:pr_ExclamationMark {col=#c;})?

	(
		a:pr_SingleConstExpression { endcol = #a;}
	  | (
			pr_Minus 
			b:pr_InfinityKeyword { endcol = #b;}
		)
	)
{
	## = #([LowerBound,"LowerBound"], ##);
	if(## != null) ##.setLocation(#col==null?#endcol:#col, #endcol);
};

pr_UpperBound{LocationAST col=null,endcol=null;;}:
	(c:pr_ExclamationMark {col=#c;})?
	(
		a:pr_SingleConstExpression { endcol = #a;}
	  | b:pr_InfinityKeyword { endcol = #b;}
	)
{
	## = #([UpperBound,"UpperBound"], ##);
	if(## != null) ##.setLocation(#col==null?#endcol:#col, #endcol);
};


pr_InfinityKeyword:
	col:INFINITY!
{
	## = #([Infinity,"infinity"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};


pr_TemplateInstance:
	col:pr_InLineTemplate
{
	## = #([TemplateInstance,"TemplateInstance"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_TemplateInstanceAssignment:
	col:pr_TemplateParIdentifier
	pr_AssignmentChar!
	endcol:pr_InLineTemplate
{
	## = #([TemplateInstanceAssignment,"TemplateInstanceAssignment"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_TemplateRefWithParList {LocationAST col = null;LocationAST endcol = null;}:
	(	//TODO: make sure greedy causes no further trouble here
		(options {greedy = true;} :
			a:pr_GlobalModuleId {col= #a; }
			pr_Dot!
		)?
		b:pr_TemplateIdentifier {if (#col==null) col= #b;}
		(
			( pr_LParen pr_TemplateActualPar ) => c:pr_TemplateActualParList {endcol= #c; }
		)?
	)
{
	## = #([TemplateRefWithParList,"TemplateRefWithParList"], ##);
	if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_TemplateRef{ LocationAST col=null; LocationAST endcol=null;}:     //149
	(
		(
			cola:pr_GlobalModuleId
			pr_Dot!
		)?
		(pr_TemplateIdentifier) => a:pr_TemplateIdentifier {endcol = #a; }
	  | b:pr_TemplateParIdentifier {endcol = #b; }
	)
{
	if(## != null) ##.setLocation(#cola==null?#col:#cola, endcol);
};

pr_InLineTemplate{ LocationAST col=null;}:
	(
		(
			// ReferencedType ==> Signature
			{checkTokenBeforeScopeChange(new int[]{TTCN3ParserTokenTypes.COLON},LT(1))}?
			( pr_Type pr_Colon! ) => a:pr_Type pr_Colon!
		)?
		(
			b:pr_DerivedRefWithParList
			pr_AssignmentChar!
		)?

		endcol: pr_TemplateBody
	)
{
	## = #([InLineTemplate,"InLineTemplate"], ##);

	if((#a != null)&&(#b==null)) {
		if(## != null) ##.setLocation(#a, #endcol);
	}
	if((#a == null)&&(#b !=null)) {
		if(## != null) ##.setLocation(#b, #endcol);
	}
	if((#a != null)&&(#b !=null)) {
		if(## != null) ##.setLocation(#a, #endcol);
	}
	if((#a == null) && (#b == null)) {
		if(## != null) ##.setLocation(#endcol,#endcol);
	}
	
};

pr_DerivedRefWithParList:
	(
		col:pr_ModifiesKeyword!
		endcol:pr_TemplateRefWithParList
	)
{
	## = #([DerivedRefWithParList,"DerivedRefWithParList"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_TemplateActualParList:
	(
		col:pr_LParen!
		(	(	pr_TemplateActualPar
				(
					pr_Comma!
					pr_TemplateActualPar
				)*
			)
		  | (	pr_TemplateActualParAssignment
				(
					pr_Comma!
					pr_TemplateActualParAssignment
				)*
		  	)
		)?
		endcol:pr_RParen!
	)
{
	## = #([TemplateActualParList,"TemplateActualParList"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_TemplateActualPar{LocationAST col = null;}:
	(pr_TemplateInstance) => a:pr_TemplateInstance {col=#a;}
  |	b:pr_Dash {col=#b;}
{
	## = #([TemplateActualPar,"TemplateActualPar"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_TemplateActualParAssignment{LocationAST col = null;}:
	col:pr_TemplateInstanceAssignment
{
	## = #([TemplateActualParAssignment,"TemplateActualParAssignment"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_TemplateOps{ LocationAST col=null;}:
	(
		a:pr_MatchOp{ col = #a;}
	  | b:pr_ValueofOp{ col = #b;}
	)
{
	## = #([TemplateOps, "TemplateOps"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_MatchOp:
	(
		col:pr_MatchKeyword!
		pr_LParen!
		pr_Expression
		pr_Comma!
		pr_TemplateInstance
		endcol:pr_RParen!
	)
{
	## = #([MatchOp, "MatchOp"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_MatchKeyword://156
	col:MATCH
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ValueofOp:
	(
		col:pr_ValueofKeyword!
		pr_LParen!
		pr_TemplateInstance
		endcol:pr_RParen!
	)
{
	## = #([ValueofOp, "ValueofOp"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ValueofKeyword:
	col:VALUEOF
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};



//----------------------------------------------------//
// Function Definitions    1.6.1.4                    //
//----------------------------------------------------//


pr_FunctionDef:
	(
		col:pr_FunctionKeyword!
		pr_FunctionIdentifier
		pr_LParen!
		( pr_FunctionFormalParList )?
		pr_RParen!
		( pr_RunsOnSpec )?
		( pr_ReturnType )?
		endcol:pr_StatementBlock
	)
{
	## = #([FunctionDef,"FunctionDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_FunctionKeyword:
	col:FUNCTION
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_FunctionIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};


pr_FunctionFormalParList:
	(
		col:pr_FunctionFormalPar
		(
			pr_Comma!
			endcol:pr_FunctionFormalPar
		)*
	)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_FunctionFormalPar{LocationAST col=null;}://163
	(
		// XXX: shorten predicates?
		( (pr_InOutParKeyword)? pr_TimerKeyword ) => b:pr_FormalTimerPar{ col = #b;}
		| ( pr_FormalValuePar ) => a:pr_FormalValuePar{ col = #a;}
		| ( pr_FormalTemplatePar ) => c:pr_FormalTemplatePar{ col = #c;}
		| d:pr_FormalPortPar{ col = #d;}
	)
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_ReturnType:
	(
		col:pr_ReturnKeyword!
		(	(	pr_TemplateKeyword
			) => (	pr_TemplateKeyword
				 )
		  | pr_RestrictedTemplate
		)?
		endcol:pr_Type
	)
{
	## = #([ReturnType,"ReturnType"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ReturnKeyword:
	col:RETURN
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_RunsOnSpec{ LocationAST endcol=null;}:
	(
		col:pr_RunsKeyword!
		pr_OnKeyword!
		endcol: pr_ComponentType
	)
{
	## = #([RunsOnSpec,"RunsOnSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_RunsKeyword:
	col:RUNS
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_OnKeyword:
	col:ON
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_MTCKeyword:
	col:MTC
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_FunctionLocalDefOrInst{LocationAST col = null,endcol = null;}:
	(	(	(	a:pr_FunctionLocalDef {col = #a;}
			)
		  |	(	b:pr_FunctionLocalInst {col = #b;}
		  	)
	  	)
	  	(	c:pr_WithStatement {endcol = #c;}
	  	)?
	  	(	pr_SemiColon
	  	)?
	)
{
//	## = #([FunctionLocalDefOrInst,"FunctionLocalDefOrInst"], ##);
	if(## != null) ##.setLocation(#col, endcol==null?#col:endcol);
};

/*
pr_FunctionStatementOrDefList  { LocationAST endcol = null; }:
	(
		col:pr_FunctionStatementOrDef
		( pr_SemiColon )?
		(
			endcol:pr_FunctionStatementOrDef
			( pr_SemiColon )?
		)*
	)
{
//  ## = #([FunctionStatementOrDefList,"FunctionStatementOrDefList"], ##);
	if(## != null) ##.setLocation(#col, endcol==null?#col:endcol);
};

pr_FunctionStatementOrDef{ LocationAST col=null;}:
	(
		a:pr_FunctionLocalDef { col = #a; }
	  | b:pr_FunctionLocalInst { col = #b; }
	  | c:pr_FunctionStatement { col = #c; }
	)
{
	## = #([FunctionStatementOrDef,"FunctionStatementOrDef"], ##);
	if(## != null) ##.setLocation(#col, #col);
};
*/
pr_FunctionLocalInst{ LocationAST col=null;}:
	(
		a:pr_VarInstance { col = #a; }
	  | b:pr_TimerInstance { col = #b; }
	)
{
	## = #([FunctionLocalInst,"FunctionLocalInst"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_FunctionLocalDef{ LocationAST col=null;}:
	(
	   a:pr_ConstDef {col=#a ;}
	 | b:pr_TemplateDef { col=#b ;}
	)
{
	## = #([FunctionLocalDef,"FunctionLocalDef"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_FunctionStatement{ LocationAST col=null;}:
	(	
		(	pr_ConnectKeyword 
		  | pr_MapKeyword
		  | (	( 	pr_ComponentId (pr_ExtendedFieldReference)? | pr_SelfOp | pr_MTCOp )
				pr_Dot
				(	( 	pr_StartKeyword pr_LParen pr_FunctionInstance pr_RParen 
					)
				  | pr_DoneKeyword
				  | pr_StopKeyword
				  | pr_KillKeyword 
				  | pr_KilledKeyword
				)
			)
		  | pr_StopKeyword 
		  | pr_DisconnectKeyword 
		  | pr_UnmapKeyword 
		  | pr_KillKeyword
		  | (	pr_TestcaseKeyword pr_Dot pr_StopKeyword	
		    )
		) => a:pr_ConfigurationStatements { col = #a; }
	  | ( pr_TimerRef pr_Dot ( pr_StartKeyword |  pr_StopKeyword | pr_TimeoutKeyword )
	  | (pr_AnyKeyword pr_TimerKeyword) | (pr_AllKeyword pr_TimerKeyword) ) => b:pr_TimerStatements { col = #b; }
	  | (
			(
				( pr_Port | (pr_AllKeyword pr_PortKeyword) )
				pr_Dot
				(
					pr_SendOpKeyword | pr_CallOpKeyword 
				  | pr_ReplyKeyword | pr_RaiseKeyword 
				  | pr_ReceiveOpKeyword | pr_TriggerOpKeyword 
				  | pr_GetCallOpKeyword | pr_GetReplyOpKeyword 
				  | pr_CatchOpKeyword | pr_CheckOpKeyword 
				  | pr_ClearOpKeyword | pr_StartKeyword
				  | pr_StopKeyword | pr_HaltKeyword | pr_CheckStateKeyword
				)
			)
		  | (
				( pr_AnyKeyword pr_PortKeyword ) pr_Dot 
				(
					pr_ReceiveOpKeyword | pr_TriggerOpKeyword 
				  | pr_GetCallOpKeyword | pr_GetReplyOpKeyword 
				  | pr_CatchOpKeyword | pr_CheckOpKeyword | pr_CheckStateKeyword
				)
			)
		) => c:pr_CommunicationStatements { col = #c; }
	  | (	pr_ExecuteKeyword
	      | pr_FunctionRef 
	        pr_LParen 
		  | pr_ReturnKeyword 
		  | pr_AltKeyword 
		  | pr_InterleavedKeyword 
		  | pr_LabelKeyword 
		  | pr_GotoKeyword 
		  | pr_ActivateKeyword 
		  | pr_DeactivateKeyword 
		  | pr_RepeatStatement  
		  | pr_BreakStatement
		  | pr_ContinueStatement
		) => d:pr_BehaviourStatements { col = #d; }
	  | ( pr_SetVerdictKeyword ) => e:pr_VerdictStatements { col = #e; }
	  | f:pr_SUTStatements { col = #f; }
	  | g:pr_BasicStatements { col = #g; }
	  | h:pr_TestcaseOperation { col = #h; }
	)
	
{
	## = #([FunctionStatement,"FunctionStatement"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_FunctionInstance:
	(
		col:pr_FunctionRef
		pr_LParen!
		( pr_FunctionActualParList )?
		endcol:pr_RParen!
	)
{
	## = #([FunctionInstance,"FunctionInstance"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_FunctionRef:  //177
	(
		(
			cola:pr_GlobalModuleId
			pr_Dot!
		)?
		col:pr_FunctionIdentifier
	)
{
	## = #([FunctionRef,"FunctionRef"], ##);
	if(## != null) ##.setLocation(#cola==null?#col:#cola, #col);
};

pr_pr_ExtFunctionIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);

};

pr_FunctionActualParList{LocationAST col=null,endcol=null;}:
	(	(	col:pr_FunctionActualParAssignment
			(	pr_Comma!
				endcol:pr_FunctionActualParAssignment
			)*
		)
	  |	(	a:pr_FunctionActualPar {#col=#a;} // I do not understand why, but  {col=#a;} as everywherelse generates wrong code in this case
			(	pr_Comma!
				b:pr_FunctionActualPar {#endcol=#b;}  // I do not understand why, but  {endcol=#a;} as everywherelse generates wrong code in this case
			)*
		)
	)
{
	## = #([FunctionActualParList,"FunctionActualParList"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_FunctionActualParAssignment{ LocationAST col=null;}:
	//TODO: consider switching these
	(	(pr_TemplateInstanceAssignment)=>col:pr_TemplateInstanceAssignment
	  |	(pr_ComponentRefAssignment)=>a:pr_ComponentRefAssignment {col=#a;}
	  | (pr_PortRefAssignment)=>b:pr_PortRefAssignment {col=#b;}
	  | c:pr_TimerRefAssignment {col=#c;}
	)
	
{
	## = #([FunctionActualParAssignment,"FunctionActualParAssignment"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_FunctionActualPar{ LocationAST col=null;}:
	(
	//TODO: reconsider readding removed parts
		// original variant slow!
//		( pr_TemplateInstance ) => b:pr_TemplateInstance { col = #b; }
//	  | ( pr_TimerRef ) => a:pr_TimerRef { col = #a; }
//	  | ( pr_Port ) => c:pr_Port { col = #c; }
//	  | ( pr_ComponentRef ) => d:pr_ComponentRef { col = #d; }

	  (SYSTEM | SELF | MTC ) => d:pr_ComponentRef { col = #d; }
	  | 
	  (pr_TemplateInstance) => b:pr_TemplateInstance { col = #b; }
	  |
	  e:pr_Dash {col = #e;}
	)
{
	## = #([FunctionActualPar,"FunctionActualPar"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

//------------------------------------------------------
//  Signature Definitions        1.6.1.5
//------------------------------------------------------

pr_SignatureDef{ LocationAST endcol=null;}:
	(
		col:pr_SignatureKeyword!
		pr_SignatureIdentifier
		pr_LParen!
		( pr_SignatureFormalParList )?
		a:pr_RParen! { endcol = #a; }
		( b:pr_ReturnType { endcol = #b; } | d:pr_NoBlockKeyword {endcol = #d ;})?
		( c:pr_ExceptionSpec { endcol = #c; } )?
	)
{
	## = #([SignatureDef,"SignatureDef"], ##);
	if(## != null) ##.setLocation(#col, endcol);
};

pr_SignatureKeyword:
	col:SIGNATURE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_SignatureIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_SignatureFormalParList: //Rule 184.
	(
		col:pr_SignatureFormalPar
		(
			pr_Comma!
			endcol:pr_SignatureFormalPar
		)*
	)
{
	## = #([SignatureFormalParList,"SignatureFormalParList"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_SignatureFormalPar{LocationAST col=null;}:
	(
		col:pr_FormalValuePar
	)
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_ExceptionSpec:
	(
		col:pr_ExceptionKeyword!
		pr_LParen!
		pr_ExceptionTypeList
		endcol:pr_RParen!
	)
{
	## = #([ExceptionSpec,"ExceptionSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ExceptionKeyword:
	col:EXCEPTION
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ExceptionTypeList:
	(
		col:pr_Type
		(
			pr_Comma!
			endcol:pr_Type
		)*
	)
{
//  ## = #([ExceptionTypeList,"ExceptionTypeList"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_NoBlockKeyword:
	col:NOBLOCK!
{
	## = #([NoBlockKeyword,"noblock"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};


pr_Signature://190
	(
		(
			cola:pr_GlobalModuleId
			pr_Dot!
		)?
		col:pr_SignatureIdentifier
	)
{
	## = #([Signature,"Signature"], ##);
	if(## != null) ##.setLocation(#cola==null?#col:#cola, #col);
};


//------------------------------------------------//-----
// Testcase Definitions           1.6.1.6         //
//------------------------------------------------//---

pr_TestcaseDef:
	(
		col:pr_TestcaseKeyword!
		pr_TestcaseIdentifier
		pr_LParen!
		( pr_TestcaseFormalParList )?
		pr_RParen!
		pr_ConfigSpec
		endcol:pr_StatementBlock
	)
{
	## = #([TestcaseDef,"TestcaseDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_TestcaseKeyword:
	col:TESTCASE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_TestcaseIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_TestcaseFormalParList{LocationAST col=null; LocationAST endcol=null;}:
	(
		col:pr_TestcaseFormalPar
		(
			pr_Comma!
			endcol:pr_TestcaseFormalPar
		)*
	)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_TestcaseFormalPar{LocationAST col=null;}:
	(	( 	pr_FormalTemplatePar 
		) => a:pr_FormalTemplatePar
			 { col=#a; }
	  | b:pr_FormalValuePar
	  	{ col=#b; }
	)
{
	if(## != null) ##.setLocation(#col, #col);
};


pr_ConfigSpec{LocationAST col=null; LocationAST endcol=null;}://196
	(
		col:pr_RunsOnSpec
		( endcol:pr_SystemSpec )?
	)
{
	## = #([ConfigSpec,"ConfigSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_SystemSpec{LocationAST col=null;}:
	(
		col:pr_SystemKeyword!
		endcol:pr_ComponentType
	)
{
	## = #([SystemSpec,"SystemSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};



pr_SystemKeyword:
	col:SYSTEM
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_TestcaseInstance{LocationAST col=null; LocationAST endcol=null;}:
	(
		col:pr_ExecuteKeyword!
		pr_LParen!
		pr_TestcaseRef
		pr_LParen!
		( ( pr_TestcaseActualPar ) => pr_TestcaseActualParList )?
		pr_RParen!
		(
			pr_Comma!
			(	(pr_Minus) =>
				pr_Minus
			  |	pr_TimerValue
			)
			(	pr_Comma!
				pr_SingleExpression	
			)?
		)?
		endcol:pr_RParen!
	)
{
	## = #([TestcaseInstance,"TestcaseInstance"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ExecuteKeyword:
	col:EXECUTE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_TestcaseRef{LocationAST col=null; LocationAST cola=null;}:
	(
		(
			cola:pr_GlobalModuleId
			pr_Dot!
		)?
		col:pr_TestcaseIdentifier
	)
{
	## = #([TestcaseRef,"TestcaseRef"], ##);
	if(## != null) ##.setLocation(#cola==null?#col:#cola, #col);
};

pr_TestcaseActualParList{LocationAST col=null; LocationAST endcol=null;}:
	(	(	a:pr_TestcaseActualParAssignment{col = #a;}
			(	pr_Comma!
				c:pr_TestcaseActualParAssignment {endcol = #c;}
			)*
		)
	  | (	b:pr_TestcaseActualPar{col = #b;}
			(	pr_Comma!
				d:pr_TestcaseActualPar {endcol = #d;}
			)*
		)
	)
{
	## = #([TestcaseActualParList,"TestcaseActualParList"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_TestcaseActualParAssignment{ LocationAST col=null;}:
	col:pr_TemplateInstanceAssignment
{
	## = #([TestcaseActualParAssignment,"TestcaseActualParAssignment"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_TestcaseActualPar{LocationAST col=null;}://203
 	(	(pr_TemplateInstance) => a:pr_TemplateInstance {col = #a;}
  	  | b:pr_Dash {col=#b;}
  	)
{
	## = #([TestcaseActualPar,"TestcaseActualPar"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


//--------------------------------------------//----------
// Altstep Definitions     1.6.1.7            //
//--------------------------------------------//----------
pr_AltstepDef:
	(
		col:pr_AltstepKeyword!
		pr_AltstepIdentifier
		pr_LParen!
		(
		  pr_AltstepFormalParList
		)?
		pr_RParen!
		( pr_RunsOnSpec )?
		pr_BeginChar!
		(pr_AltstepLocalDefList)?
		pr_AltGuardList
		endcol:pr_EndChar!
	)
{
	## = #([AltstepDef,"AltstepDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_AltstepKeyword:
	col:ALTSTEP!
{
	## = #([AltstepKeyword,"altstep"], ##);
	if(## != null) {
		##.setLocation(#col, #col);
		identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
	}
};


pr_AltstepIdentifier://195
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};


pr_AltstepFormalParList:
	(
		col:pr_FunctionFormalParList
	)
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_AltstepLocalDefList {LocationAST endcol=null;}:
// -------------------------------------------------------------------------------------------
// note: this is an implementable variant as the original rule
//
//    208. AltstepLocalDefList ::= {AltstepLocalDef [SemiColon]}
//
// leads to an indeterminism on  "[" SingleExpression  between  ArrayDef  and  GuardStatement;
// a solution is the obligatory semicolon after the last AltstepLocalDef;
// alternative solutions could be:
// - forbid ArrayDef within the last AltstepLocalDef,
// - obligatory semicolon after the last AltstepLocalDef if it includes ArrayDef.
// -------------------------------------------------------------------------------------------
//	(
//		col: pr_AltstepLocalDef
//		(endcol:pr_SemiColon)?
//	)*
	col: pr_AltstepLocalDef
	(	c:pr_WithStatement {endcol=#c;}
	)?
	(
		(pr_SemiColon)?
		b:pr_AltstepLocalDef {endcol=#b;}
		(	d:pr_WithStatement {endcol=#d;}
		)?
	)*
	pr_SemiColon!
{
	##=#([AltstepLocalDefList,"AltstepLocalList"],##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
}
;

pr_AltstepLocalDef{ LocationAST col=null;}:
	(
		// FIXME: may be too short predicates...
		( pr_VarKeyword ) => a:pr_VarInstance { col = #a; } |
		( pr_TimerKeyword ) => b:pr_TimerInstance { col = #b; } |
		( pr_ConstKeyword ) => c:pr_ConstDef { col = #c; } |
		( pr_TemplateKeyword ) => d:pr_TemplateDef{col = #d;}
	)
{
	## = #([AltstepLocalDef,"AltstepLocalDef"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_AltstepInstance://199
	(
		col:pr_AltstepRef
		pr_LParen!
		( ( pr_FunctionActualPar ) => pr_FunctionActualParList )?
		endcol:pr_RParen!
	)
{
	## = #([AltstepInstance,"AltstepInstance"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_AltstepRef:
	(
		(
			( pr_GlobalModuleId ) => cola:pr_GlobalModuleId
			pr_Dot!
		)?
		col:pr_AltstepIdentifier
	)
{
//  ## = #([AltstepRef,"AltstepRef"], ##);
	if(## != null) ##.setLocation(#cola==null?#col:#cola, #col);
};


//-----------------------------------------------------//
// Import Definitions    1.6.1.8         //
//----------------------------------------------------//
pr_ImportDef{LocationAST endcol = null;}:
	(
		col:pr_ImportKeyword!
		pr_ImportFromSpec
		(
			enda:pr_AllWithExcepts {endcol=#enda ;}
		  | (pr_BeginChar! pr_ImportSpec endb:pr_EndChar!) {endcol=#endb ;}
		)
	)
{
	## = #([ImportDef,"ImportDef"], ##);
	if(## != null) ##.setLocation(#col, endcol);
};

pr_ImportKeyword:
	col:IMPORT
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ExceptSpec:
	( col:pr_ExceptElement (endcol:pr_SemiColon)? )*
{
	## = #([ExceptSpec,"ExceptSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol );
}
;

pr_ExceptsDef://215
	col:pr_ExceptKeyword! pr_BeginChar! pr_ExceptSpec endcol: pr_EndChar!
{
	## = #([ExceptsDef,"ExceptsDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_AllWithExcepts:
   (
	   col:pr_AllKeyword!
	   (endcol:pr_ExceptsDef)?
   )
{
	## = #([AllWithExcepts,"AllWithExcepts"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol );
};

pr_ExceptKeyword:
	col:EXCEPT
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ExceptElement{LocationAST col=null;}:
	(
		a:pr_ExceptGroupSpec { col = #a; }
	  | b:pr_ExceptTypeDefSpec { col = #b; }
	  | c:pr_ExceptTemplateSpec { col = #c; }
	  | d:pr_ExceptConstSpec { col = #d; }
	  | e:pr_ExceptTestcaseSpec { col = #e; }
	  | f:pr_ExceptAltstepSpec { col = #f; }
	  | g:pr_ExceptFunctionSpec { col = #g; }
	  | h:pr_ExceptSignatureSpec { col = #h; }
	  | i:pr_ExceptModuleParSpec { col = #i; }
	)
{
	## = #([ExceptElement,"ExceptElement"], ##);
	if(## != null) ##.setLocation(#col,#col);
};

pr_ExceptGroupSpec{LocationAST endcol=null;}:  //addtest
   (
		col:pr_GroupKeyword!
		(
			a:pr_ExceptGroupRefList {endcol = #a ;}
		  | b:pr_AllKeyword  {endcol = #b ;}
		)
   )
{
	## = #([ExceptGroupSpec,"ExceptGroupSpec"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};


pr_ExceptTypeDefSpec{LocationAST endcol=null;}:
	(
		col:pr_TypeDefKeyword!
		(
			a:pr_TypeRefList {endcol = #a ;}
		  | b:pr_AllKeyword {endcol = #b ;}
		)
	)
{
	## = #([ExceptTypeDefSpec,"ExceptTypeDefSpec"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};


pr_ExceptTemplateSpec{LocationAST endcol=null;}://221
	(
		col:pr_TemplateKeyword!
		(
			a:pr_TemplateRefList {endcol = #a ;}
		  | b:pr_AllKeyword  {endcol = #b ;}
		)
	)
{
	## = #([ExceptTemplateSpec,"ExceptTemplateSpec"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};


pr_ExceptConstSpec {LocationAST endcol=null;}:
	(
		col:pr_ConstKeyword!
		(
			a:pr_ConstRefList {endcol = #a ;}
		  | b:pr_AllKeyword  {endcol = #b ;}
		)
	)
{
	## = #([ExceptConstSpec,"ExceptConstSpec"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};


pr_ExceptTestcaseSpec {LocationAST endcol=null;}:
	(
		col:pr_TestcaseKeyword!
		(
			a:pr_TestcaseRefList {endcol = #a ;}
		  | b:pr_AllKeyword  {endcol = #b ;}
		)
	)
{
	## = #([ExceptTestcaseSpec,"ExceptTestcaseSpec"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};

// TODO format here down

pr_ExceptAltstepSpec{LocationAST endcol=null;}:
	 (
	   col:pr_AltstepKeyword!
	   (
	   a:pr_AltstepRefList {endcol = #a ;}
	   |
	   b:pr_AllKeyword  {endcol = #b ;}
	   )
	)
{
	## = #([ExceptAltstepSpec,"ExceptAltstepSpec"], ##);
	  if(## != null) ##.setLocation(#col,#endcol);
};


pr_ExceptFunctionSpec{LocationAST endcol=null;}: //225
		(
		col:pr_FunctionKeyword!
		  (
		  a:pr_FunctionRefList {endcol = #a ;}
		  |
		  b:pr_AllKeyword {endcol = #b ;}
		  )
		)
{
	## = #([ExceptFunctionSpec,"ExceptFunctionSpec"], ##);
		  if(## != null) ##.setLocation(#col,#endcol);
};


pr_ExceptSignatureSpec{LocationAST endcol=null;}:
		 (
		 col:pr_SignatureKeyword!
			(
		   a:pr_SignatureRefList {endcol = #a ;}
			|
		   b:pr_AllKeyword {endcol = #b ;}
			)
		 )
{
	## = #([ExceptSignatureSpec,"ExceptSignatureSpec"], ##);
	  if(## != null) ##.setLocation(#col,#endcol);
};


pr_ExceptModuleParSpec{LocationAST endcol=null;}:
		  (
		   col:pr_ModuleParKeyword!
		   (
			 a:pr_ModuleParRefList {endcol = #a ;}
			 |
			 b:pr_AllKeyword  {endcol = #b ;}
		   )
		  )
{
	## = #([ExceptModuleParSpec,"ExceptModuleParSpec"], ##);
	  if(## != null) ##.setLocation(#col,#endcol);
};

pr_ImportSpec:
   (
	 col:pr_ImportElement
	(endcol:pr_SemiColon)?
   )*
{
	## = #([ImportSpec,"ImportSpec"], ##);
	 if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_ImportElement {LocationAST col=null;}:
		(
			a:pr_ImportModuleParSpec{ col = #a;}
			|
			b:pr_ImportGroupSpec{ col = #b;}
			|
			c:pr_ImportTypeDefSpec{ col = #c;}
			|
			d:pr_ImportTemplateSpec{ col = #d;}
			|
			e:pr_ImportConstSpec{ col = #e;}
			|
			f:pr_ImportTestcaseSpec{ col = #f;}
			|
			g:pr_ImportAltstepSpec{ col = #g;}
			|
			h:pr_ImportFunctionSpec{ col = #h;}
			|
			i:pr_ImportSignatureSpec{ col = #i;}
			|
			j:pr_ImportImportSpec{col = #j;}
		)
{
	## = #([ImportElement,"ImportElement"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_ImportModuleParSpec{LocationAST endcol=null;}://262
	(
		col:pr_ModuleParKeyword!
		(
			a:pr_ModuleParRefList  {endcol = #a ;}
			| b:pr_AllModuleParWithExcept {endcol = #b ;}
		)
	)
{
	## = #([ImportModuleParSpec,"ImportModuleParSpec"], ##);
	 if(## != null) ##.setLocation(#col,#endcol);
};

pr_ModuleParRefList:
	col:pr_ModuleParIdentifier (pr_Comma! endcol:pr_ModuleParIdentifier)*
{
	## = #([ModuleParRefList,"ModuleParRefList"], ##);
	 if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_AllModuleParWithExcept:
	col:pr_AllKeyword! (pr_ExceptKeyword! endcol:pr_ModuleParRefList)?
{
	## = #([AllModuleParWithExcept,"AllModuleParWithExcept"], ##);
		 if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_ImportImportSpec:
	col:pr_ImportKeyword!
	endcol:pr_AllKeyword!
{
	## = #([ImportImportSpec,"ImportImportSpec"], ##);
	 if(## != null) ##.setLocation(#col,#endcol);
};

pr_ImportFromSpec:
		(
			col:pr_FromKeyword!
				pr_ModuleId
			( endcol:pr_RecursiveKeyword)?
		)
{
	## = #([ImportFromSpec,"ImportFromSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_RecursiveKeyword:
	  col:RECURSIVE
{
	## = #([RecursiveKeyword,"recursive"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_ModuleId{LocationAST endcol=null;}://4
		(
			col:pr_GlobalModuleId
			( b:pr_LanguageSpec { endcol =#b; } )?
		)
{
	## = #([ModuleId,"ModuleId"], ##);
   if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);

};

pr_LanguageKeyword:
		col:LANGUAGE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_LanguageSpec:
		(
			col:pr_LanguageKeyword!
			(	pr_FreeText
				pr_Comma!
			)*
			endcol:pr_FreeText
		)
{
	## = #([LanguageSpec,"LanguageSpec"], ##);
	   if(## != null) ##.setLocation(#col, #endcol);
};

pr_GlobalModuleId:
	col:pr_ModuleIdentifier
	(
		(DOT)=>DOT!
		pr_ObjectIdentifierValue
	)?
{
	## = #([GlobalModuleId,"GlobalModuleId"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_ImportGroupSpec{LocationAST endcol = null; }:
	col:pr_GroupKeyword!
	(
		a:pr_GroupRefListWithExcept { endcol=#a;}
		| b:pr_AllGroupsWithExcept {endcol= #b ;}
	)
{
	## = #([ImportGroupSpec,"ImportGroupSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_GroupRefList:
	col:pr_FullGroupIdentifier (pr_Comma! endcol:pr_FullGroupIdentifier)*
{
	## = #([GroupRefList,"GroupRefList"], ##);
	   if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_GroupRefListWithExcept:
	col:pr_FullGroupIdentifierWithExcept (pr_Comma! endcol:pr_FullGroupIdentifierWithExcept)*
{
	## = #([GroupRefListWithExcept,"GroupRefListWithExcept"], ##);
	 if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_AllGroupsWithExcept:
	col:pr_AllKeyword! (pr_ExceptKeyword! endcol:pr_GroupRefList)?
{
	## = #([AllGroupsWithExcept,"AllGroupsWithExcept"], ##);
	 if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_FullGroupIdentifier:
	col:pr_GroupIdentifier (pr_Dot endcol:pr_GroupIdentifier)*
{
	## = #([FullGroupIdentifier,"FullGroupIdentifier"], ##);
	 if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_FullGroupIdentifierWithExcept {LocationAST endcol = null; }:
	col:pr_FullGroupIdentifier (b:pr_ExceptsDef {endcol=#b;})?
{
   ## = #([FullGroupIdentifierWithExcept,"FullGroupIdentifierWithExcept"], ##);
	   if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};


pr_ImportTypeDefSpec {LocationAST endcol = null; }://240
	col:pr_TypeDefKeyword!
	(
		a:pr_TypeRefList {endcol= #a ;}
		| b:pr_AllTypesWithExcept {endcol= #b ;}
	)
{
	## = #([ImportTypeDefSpec,"ImportTypeDefSpec"], ##);
	   if(## != null) ##.setLocation(#col,#endcol);
};


pr_ExceptGroupRefList:
	col:pr_ExceptFullGroupIdentifier
	(
		pr_Comma!
		endcol:pr_ExceptFullGroupIdentifier
	)*
{
	## = #([ExceptGroupRefList,"ExceptGroupRefList"], ##);
	   if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_ExceptFullGroupIdentifier:
	col:pr_FullGroupIdentifier
{
	if(## != null) ##.setLocation(#col,#col);
};

pr_TypeDefIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col,#col);
};

pr_TypeRefList:
	col:pr_TypeDefIdentifier (pr_Comma! endcol:pr_TypeDefIdentifier)*
{
	  ## = #([TypeRefList,"TypeRefList"], ##);
	  if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_AllTypesWithExcept:
	col:pr_AllKeyword! (pr_ExceptKeyword! endcol:pr_TypeRefList)?
{
	## = #([AllTypesWithExcept,"AllTypesWithExcept"], ##);
	if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};


pr_ImportTemplateSpec{ LocationAST endcol=null;}:
	col:pr_TemplateKeyword!
	(
		a:pr_TemplateRefList {endcol=#a ;}
		| b:pr_AllTemplsWithExcept {endcol= #b;}
	)
{
	## = #([ImportTemplateSpec,"ImportTemplateSpec"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};


pr_TemplateRefList:
	col:pr_TemplateIdentifier (pr_Comma! endcol:pr_TemplateIdentifier)*
{
	 ## = #([TemplateRefList,"TemplateRefList"], ##);
	 if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_AllTemplsWithExcept://246
	col:pr_AllKeyword! (pr_ExceptKeyword! endcol:pr_TemplateRefList)?
{
	## = #([AllTemplsWithExcept,"AllTemplsWithExcept"], ##);
	   if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};


pr_ImportConstSpec{LocationAST endcol=null;}:
	col:pr_ConstKeyword!
	(
		a:pr_ConstRefList {endcol = #a ;}
		| b: pr_AllConstsWithExcept {endcol = #b ;}
	)
{
	## = #([ImportConstSpec,"ImportConstSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ConstRefList:
	col:pr_ConstIdentifier (pr_Comma! endcol:pr_ConstIdentifier)*
{
	  ## = #([ConstRefList,"ConstRefList"], ##);
		 if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_AllConstsWithExcept:
	col:pr_AllKeyword! (pr_ExceptKeyword! endcol: pr_ConstRefList)?
{
	## = #([AllConstsWithExcept,"AllConstsWithExcept"], ##);
	   if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};


pr_ImportTestcaseSpec{LocationAST endcol=null;}:
	col:pr_TestcaseKeyword!
	(
		a: pr_TestcaseRefList {endcol = #a ;}
		| b:pr_AllTestcasesWithExcept{endcol = #b ;}
	)
{
	## = #([ImportTestcaseSpec,"ImportTestcaseSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_TestcaseRefList:
	col:pr_TestcaseIdentifier (pr_Comma! endcol: pr_TestcaseIdentifier)*
{
	  ## = #([TestcaseRefList,"TestcaseRefList"], ##);
	   if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_AllTestcasesWithExcept:
	col:pr_AllKeyword! (pr_ExceptKeyword! endcol:pr_TestcaseRefList)?
{
	## = #([AllTestcasesWithExcept,"AllTestcasesWithExcept"], ##);
	   if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_ImportAltstepSpec{ LocationAST endcol=null;}:
	col:pr_AltstepKeyword!
	(
		a: pr_AltstepRefList {endcol= #a ;}
		| b:pr_AllAltstepsWithExcept { endcol= #b ;}
	)
{
	## = #([ImportAltstepSpec,"ImportAltstepSpec"], ##);
	if(## != null) ##.setLocation(#col,#endcol);

};


pr_AltstepRefList:
	col:pr_AltstepIdentifier (pr_Comma! endcol: pr_AltstepIdentifier)*
{
	  ## = #([AltstepRefList,"AltstepRefList"], ##);
	  if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};


pr_AllAltstepsWithExcept:
	col:pr_AllKeyword! (pr_ExceptKeyword! endcol:pr_AltstepRefList)?
{
	## = #([AllAltstepsWithExcept,"AllAltstepsWithExcept"], ##);
	  if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};


pr_ImportFunctionSpec{LocationAST endcol = null; }:
	col:pr_FunctionKeyword!
	(
		a:pr_FunctionRefList {endcol= #a ;}
		| b:pr_AllFunctionsWithExcept {endcol= #b ;}
	)
{
	## = #([ImportFunctionSpec,"ImportFunctionSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_FunctionRefList:
	col:pr_FunctionIdentifier (pr_Comma! endcol: pr_FunctionIdentifier)*
{
	## = #([FunctionRefList,"FunctionRefList"], ##);
		if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_AllFunctionsWithExcept:
	col:pr_AllKeyword! (pr_ExceptKeyword! endcol: pr_FunctionRefList)?
{
	## = #([AllFunctionsWithExcept,"AllFunctionsWithExcept"], ##);
		if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};


pr_ImportSignatureSpec{LocationAST endcol = null; }:
	col:pr_SignatureKeyword!
	(
		a: pr_SignatureRefList {endcol= #b ;}
		| b: pr_AllSignaturesWithExcept{endcol= #b ;}
	)
{
	## = #([ImportSignatureSpec,"ImportSignatureSpec"], ##);
  if(## != null) ##.setLocation(#col, #endcol);
};

pr_SignatureRefList:
	col:pr_SignatureIdentifier (pr_Comma! endcol:pr_SignatureIdentifier)*
{
	## = #([SignatureRefList,"SignatureRefList"], ##);
	  if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_AllSignaturesWithExcept:
	col:pr_AllKeyword! (pr_ExceptKeyword! endcol: pr_SignatureRefList)?
{
	## = #([AllSignaturesWithExcept,"AllSignaturesWithExcept"], ##);
	  if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};


//------------------------------------------------------
// Group Definitions    1.6.1.9
//------------------------------------------------------
pr_GroupDef:
		(
			col:pr_GroupKeyword!
			pr_GroupIdentifier
			pr_BeginChar!
			( pr_ModuleDefinitionsPart )?
			endcol:pr_EndChar!
		)
{
	## = #([GroupDef,"GroupDef"], ##);
if(## != null) ##.setLocation(#col, #endcol);
};

pr_GroupKeyword://266
		col:GROUP
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_GroupIdentifier:
		col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};


//------------------------------------------------------
// External Function Definitions   1.6.1.10
//------------------------------------------------------

pr_ExtFunctionDef{LocationAST endCol = null;}:
		(
			col:pr_ExtKeyword!
			pr_FunctionKeyword!
			pr_ExtFunctionIdentifier
			pr_LParen!
			( pr_FunctionFormalParList )?
			enda:pr_RParen!{endCol = #enda;}
			( endb:pr_ReturnType{endCol = #endb;} )?
		)
{
	## = #([ExtFunctionDef,"ExtFunctionDef"], ##);
	if(## != null) ##.setLocation(#col, #endCol);
};

pr_ExtKeyword://269
		col:EXTERNAL
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_ExtFunctionIdentifier:
		col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};


//------------------------------------------------------
// External Constant Definitions    1.6.1.11
//------------------------------------------------------

pr_ExtConstDef:
		(
			col:pr_ExtKeyword!
			pr_ConstKeyword!
			pr_Type
			endcol:pr_ExtConstIdentifierList //Changed to list 
		)
{
	## = #([ExtConstDef,"ExtConstDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

//Introduced ExtConstIdentifierList
pr_ExtConstIdentifierList:
	(
		col:pr_ExtConstIdentifier
		(
			pr_Comma!
			endcol:pr_ExtConstIdentifier
		)*
	)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};



pr_ExtConstIdentifier:
		col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};


//------------------------------------------------------
//   Module parameter definitions    1.6.1.12
//------------------------------------------------------

pr_ModuleParDef { LocationAST endcol=null;}:
	col:pr_ModuleParKeyword!
	(
		a:pr_ModulePar {endcol = #a;}
		| (pr_BeginChar! pr_MultitypedModuleParList b:pr_EndChar!{endcol = #b;})
	)
{
	## = #([ModuleParDef,"ModuleParDef"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};

pr_MultitypedModuleParList{ LocationAST endcol=null;}:
	(col:pr_ModulePar (endcol:pr_SemiColon)? )* //empty list shall be possible as per v3.2.1 : changed from +
{
	## = #([MultitypedModuleParList,"MultitypedModuleParList"], ##);
 	if(## != null) ##.setLocation(#col,#endcol);
};


pr_ModuleParKeyword:
	col:MODULEPAR
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_ModulePar :
	( col:pr_ModuleParType endcol:pr_ModuleParList)
{
  ## = #([ModulePar,"ModulePar"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ModuleParList{ LocationAST endcol=null;}://278
	col:pr_ModuleParIdentifier
	( pr_AssignmentChar! d:pr_ConstantExpression {endcol = #d ;})?
	(
		pr_Comma!
		c: pr_ModuleParIdentifier {endcol= #c ;}
		(pr_AssignmentChar! e:pr_ConstantExpression {endcol = #e ;})?
	)*
{
	## = #([ModuleParList,"ModuleParList"], ##);
	if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
}
;

pr_ModuleParType:
	col:pr_Type
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_ModuleParIdentifier:
	col:pr_Identifier
{
  if(## != null) ##.setLocation(#col, #col);
};

pr_FriendModuleDef{LocationAST endcol=null;}:
(
	col:pr_FriendKeyword!
	pr_TTCN3ModuleKeyword!
	a:pr_ModuleIdentifier{endcol = #a;}
	(	pr_Comma!
		b:pr_ModuleIdentifier{endcol = #b;}
	)*
)
{
	## = #([FriendModuleDef,"FriendModuleDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


//------------------------------------------------------
//   Control Part    1.6.2
//------------------------------------------------------

pr_ModuleControlPart{LocationAST endcol = null; }:
	(
			col:pr_ControlKeyword!
			pr_BeginChar!
			body:pr_ModuleControlBody
			enda:pr_EndChar!{endcol = #enda;}
			( endb:pr_WithStatement{endcol = #endb;} )?
			( endc:pr_SemiColon{endcol = #endc;} )?
	)
{
	## = #([ModuleControlPart,"ModuleControlPart"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
	if (#body.getOffset() < 0) #body.setLocation(#col, #endcol);	
};


pr_ControlKeyword:
		col:CONTROL
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ModuleControlBody://282
		( col:pr_ControlStatementOrDefList )?
{
	## = #([ModuleControlBody,"ModuleControlBody"], ##);
	if(## != null ) ##.setLocation(#col, #col);
};

pr_ControlStatementOrDefList{LocationAST endcol = null; }:
	col: pr_ControlStatementOrDef
	( a: pr_SemiColon {endcol= #a;} )?
	(
	  b: pr_ControlStatementOrDef {endcol= #b;}
	  ( c: pr_SemiColon  {endcol= #c;} )?
	)*
{
  //  ## = #([ControlStatementOrDefList,"ControlStatementOrDefList"], ##);
   if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_ControlStatementOrDef{ LocationAST col=null,endcol=null;}:
		(
			(	(	a:pr_FunctionLocalInst { col = #a; }
				  |	c:pr_FunctionLocalDef { col = #c; }
				)
				(	d:pr_WithStatement {endcol=#d;}	
				)?
			)
			
			|
			b:pr_ControlStatement { col = #b; }
		)
{
	## = #([ControlStatementOrDef,"ControlStatementOrDef"], ##);
   if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_ControlStatement{ LocationAST col=null;}:
		(
				(  pr_VariableRef pr_AssignmentChar! | pr_LogKeyword |
					pr_ForKeyword | pr_WhileKeyword | pr_DoKeyword |
					pr_IfKeyword | pr_SelectKeyword ) =>
			a:pr_BasicStatements { col = #a; }
			|
			b:pr_SUTStatements { col = #b; }
			|
				( pr_TimerRef pr_Dot ( pr_StartKeyword |  pr_StopKeyword | pr_TimeoutKeyword )
				|
				pr_AnyKeyword pr_TimerKeyword | pr_AllKeyword pr_TimerKeyword  ) =>
			c:pr_TimerStatements { col = #c; }
			|
			d:pr_BehaviourStatements  { col = #d; }
			|pr_StopKeyword
		)
{
	## = #([ControlStatement,"ControlStatement"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

//------------------------------------------------------
// Variable Instantiation   1.6.2.1
//------------------------------------------------------



//old version
/*
pr_VarInstance{LocationAST endcol=null;}:
	(
		col:pr_VarKeyword!
		(
			// FIXME: shouldn't this be pr_TemplateKeyword?
			(TemplateKeyword pr_Type pr_TempVarList) => (pr_TemplateKeyword pr_Type b:pr_TempVarList {endcol = #b;} ) |
			(pr_Type a:pr_VarList {endcol = #a;})
	)
	)
{
	## = #([VarInstance,"VarInstance"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};
*/


pr_VarInstance{LocationAST endcol=null;}:
	( 	col:pr_VarKeyword!
		(	// FIXME: shouldn't this be pr_TemplateKeyword? => Fixed
			(	(	(	pr_TemplateKeyword
					) => (	pr_TemplateKeyword
						 )
				  | pr_RestrictedTemplate //this has to be extrapolated OR NOT?
				) 
//			(   pr_TemplateKeyword		
				pr_Type 
				pr_TempVarList
			
			
			) => (	(	(	pr_TemplateKeyword
						) => (	pr_TemplateKeyword
							 )
					  | pr_RestrictedTemplate

					)
//			) => (	pr_TemplateKeyword
					pr_Type 
					b:pr_TempVarList 
					{endcol = #b;} 
				 ) 
		  |	(	pr_Type 
		  		a:pr_VarList 
		  		{endcol = #a;}
		  	)
		)
	)
{
	## = #([VarInstance,"VarInstance"], ##);
	if(## != null) ##.setLocation(#col,#endcol);
};

pr_TempVarList :
	(
		col:pr_SingleTempVarInstance
		( (pr_Comma! endcol:pr_SingleTempVarInstance)*)
	)
{
 ## = #([TempVarList,"TempVarList"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};



pr_SingleTempVarInstance{LocationAST endcol=null;}:
	(
		col:pr_VarIdentifier
		((pr_ArrayDef)=>a:pr_ArrayDef {endcol = #a;})?
		(
			pr_AssignmentChar!
			b:pr_TempVarInitialValue {endcol = #b;}
		)?
	)
{
 ## = #([SingleTempVarInstance,"SingleTempVarInstance"], ##);
  if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_TempVarInitialValue:

 col:pr_TemplateBody

{
	if(## != null) ##.setLocation(#col, #col);
};

pr_VarList:
		(
			col:pr_SingleVarInstance
			(
			  (
					pr_Comma!
				endcol:pr_SingleVarInstance
				)*
			)
		)
{
## = #([VarList,"VarList"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_SingleVarInstance{LocationAST endcol=null;}://288
	(
			col:pr_VarIdentifier
			( (pr_ArrayDef)=>a:pr_ArrayDef { endcol=#a; })?
			(
				pr_AssignmentChar!
				b:pr_VarInitialValue {endcol=#b;}
			)?
		)
{
	## = #([SingleVarInstance,"SingleVarInstance"], ##);
   if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_VarInitialValue:
		col:pr_Expression
{
	if(## != null) ##.setLocation(#col, #col);
};


pr_VarKeyword:
		col:VAR
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);

};


pr_VarIdentifier:
	col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_VariableRef:
	col:pr_Identifier
	(	( pr_Dot  | pr_SquareOpen | pr_LParen ) => //TODO: why is there LParen here?
		endcol:pr_ExtendedFieldReference
		|
	)
{
	## = #([VariableRef,"VariableRef"], ##);
	if(## != null) ##.setLocation(#col, #endcol == null?#col:#endcol);

};


//------------------------------------------------------
//  Timer Instantiation    1.6.2.2
//------------------------------------------------------

pr_TimerInstance ://296
		(
		  col:pr_TimerKeyword!
		   endcol:pr_TimerList
		)
{
	## = #([TimerInstance,"TimerInstance"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_TimerList:
	 (
	 col :pr_SingleTimerInstance
	   ( pr_Comma! endcol: pr_SingleTimerInstance )*
	 )
{
	if(## != null) ##.setLocation(#col, #endcol==null? #col:#endcol);
};

pr_SingleTimerInstance {LocationAST endcol = null;}:
		   col:pr_TimerIdentifier
			( b:pr_ArrayDef { endcol = #b; } )?
			(
				pr_AssignmentChar!
				c:pr_TimerValue { endcol = #c; }
			)?
{
	## = #([SingleTimerInstance,"SingleTimerInstance"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null? #col:#endcol);
};


pr_TimerKeyword:
		col:TIMER!
{
	## = #([TimerKeyword,"timer"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};


pr_TimerIdentifier:
		col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};


pr_TimerValue:
		col:pr_Expression
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_TimerRef:
		(
			col:pr_TimerRefId
			( endcol:pr_ArrayOrBitRef )*
		)
{
	## = #([TimerRef,"TimerRef"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_TimerRefAssignment:
	(	col:pr_TimerParIdentifier
		pr_AssignmentChar!
		endcol:pr_TimerRef
	)
{
	## = #([TimerRefAssignment,"TimerRefAssignment"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_TimerRefId:
		col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

//------------------------------------------------------
// Component Operations     1.6.2.3
//------------------------------------------------------

pr_ConfigurationStatements {LocationAST col=null;}:
	(
		( pr_ConnectStatement ) => a:pr_ConnectStatement { col = #a; }
	  | ( pr_MapStatement ) => b:pr_MapStatement { col = #b; }
	  | ( pr_DisconnectStatement ) => e:pr_DisconnectStatement { col = #e; }
	  | ( pr_UnmapStatement ) => f:pr_UnmapStatement { col = #f; }
	  | ( pr_DoneStatement ) => g:pr_DoneStatement { col = #g; }
	  | ( pr_KilledStatement ) => i:pr_KilledStatement  { col = #i ;}
	  | ( pr_StartTCStatement ) => c:pr_StartTCStatement { col = #c; }
	  | ( pr_StopTCStatement ) => d:pr_StopTCStatement { col = #d; }
	  | ( pr_KillTCStatement ) => h:pr_KillTCStatement { col = #h ;}
	)
{
	## = #([ConfigurationStatements,"ConfigurationStatements"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_ConfigurationOps{ LocationAST col=null; boolean checkAlive=false;}://304
	(	
	  {checkTokenBeforeScopeChange(new int[]{TTCN3ParserTokenTypes.CREATE},LT(1))}? ( pr_ComponentType pr_Dot pr_CreateKeyword ) => a:pr_CreateOp { col = #a; } 
	  | b:pr_SelfOp { col = #b; }
	  | c:pr_SystemOp { col = #c; }
	  | d:pr_MTCOp { col = #d; }
	  | {checkTokenBeforeScopeChange(new int[]{TTCN3ParserTokenTypes.RUNNING},LT(1))}? (pr_RunningOp ) => e:pr_RunningOp { col = #e; }
	  | {checkTokenBeforeScopeChange(new int[]{TTCN3ParserTokenTypes.ALIVE},LT(1))}? f:pr_AliveOp {col = #f; }
	)

{
	## = #([ConfigurationOps,"ConfigurationOps"], ##);
	if(## != null) ##.setLocation(#col, #col);

};

pr_CreateOp{LocationAST endcol=null;}:
	(
		col:pr_ComponentType
		pr_Dot!
		a:pr_CreateKeyword!{endcol=#a;}
		(
			pr_LParen!
			((pr_SingleExpression) => pr_SingleExpression
				| pr_Minus
			)
			(	pr_Comma!
				pr_SingleExpression
			)?
			b:pr_RParen! {endcol=#b;}
		)?
		(d:pr_AliveKeyword {endcol=#d;})?
	)
{
	## = #([CreateOp,"CreateOp"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_SystemOp:
		col:SYSTEM!
{
	## = #([SystemOp,"SystemOp"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_SelfOp:
		col:SELF!
{
	## = #([SelfOp,"SelfOp"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_AliveOp :

( col:pr_ComponentId
  pr_Dot!
  endcol:pr_AliveKeyword!)
 {
	## = #([AliveOp,"AliveOp"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_MTCOp:
		col:MTC!
{
  ## = #([MTCOp,"MTCOp"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_KillTCStatement{LocationAST col=null; LocationAST endcol=null;}:
(
 (a:pr_KillKeyword  {col = #a;}
 |
 (b:pr_ComponentReferenceOrLiteral {col = #b ;}
   pr_Dot! d:pr_KillKeyword!{endcol=#d;})
 |
 (c:pr_AllKeyword {col = #c;}
   pr_ComponentKeyword!
   pr_Dot!
  e:pr_KillKeyword!{endcol = #e;})
))
{
  ## = #([KillTCStatement,"KillTCStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_KilledStatement:
(
	col:pr_ComponentId
	pr_Dot!
	endcol:pr_KilledKeyword!)
{
  ## = #([KilledStatement,"KilledStatement"], ##);
if(## != null) ##.setLocation(#col, #endcol);
};

pr_KilledKeyword :
	col:KILLED
{
	## = #([KilledKeyword ,"killed"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_AliveKeyword :
  col:ALIVE!
{
	## = #([AliveKeyword ,"alive"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_KillKeyword:
	col:KILL
{
	## = #([KillKeyword ,"kill"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};


pr_DoneStatement:
		(
			col:pr_ComponentId
			pr_Dot!
			endcol:pr_DoneKeyword!
		)
{
	## = #([DoneStatement,"DoneStatement"], ##);
if(## != null) ##.setLocation(#col, #endcol);
};

pr_ComponentId{LocationAST col=null;}:
	(
		a:pr_ComponentOrDefaultReference{ col =#a;}
		| 
		(( b:pr_AnyKeyword{ col = #b;} | c:pr_AllKeyword{ col = #c;} ) pr_ComponentKeyword! )
	)
{
	## = #([ComponentId,"ComponentId"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_ComponentOrDefaultReference {LocationAST col=null;}:
(
  (pr_VariableRef)=>a:pr_VariableRef {col = #a ;}
  |
  b:pr_FunctionInstance {col= #b;}
)
{
 	if(## != null) ##.setLocation(#col, #col);
};

pr_RunningOp:
		(
			col:pr_ComponentId
			pr_Dot!
			endcol:pr_RunningKeyword!
		)
{
	## = #([RunningOp,"RunningOp"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_RunningKeyword:
		col:RUNNING
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_DoneKeyword:
		col:DONE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_CreateKeyword:
		col:CREATE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ConnectStatement://319
		(
			col:pr_ConnectKeyword!
			endcol:pr_SingleConnectionSpec
		)
{
	## = #([ConnectStatement,"ConnectStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_SingleConnectionSpec:
	(col:pr_LParen! pr_PortRef pr_Comma! pr_PortRef endcol:pr_RParen!)
{
	## = #([SingleConnectionSpec,"SingleConnectionSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ConnectKeyword:
		col:CONNECT
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_PortRef:
	(	col:pr_ComponentRef
		pr_Colon!
		endcol:pr_Port
	)
{
	## = #([PortRef,"PortRef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

//NOTE: in the BNF it is called PortAssignment, 
//for consistency with other *RefAssignment rules this one will be named similarly 
pr_PortRefAssignment:
	(	col:pr_PortParIdentifier
		pr_AssignmentChar!
		endcol:pr_Port
	)
{
	## = #([PortRefAssignment,"PortRefAssignment"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ComponentRef{ LocationAST col=null;}:
	(
		a:pr_ComponentOrDefaultReference { col = #a; }
	  | b:pr_SystemOp { col = #b; }
	  | c:pr_SelfOp { col = #c; }
	  | d:pr_MTCOp { col = #d; }
	)
{
	## = #([ComponentRef,"ComponentRef"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_ComponentRefAssignment:
	col:pr_ValueParIdentifier
	pr_AssignmentChar!
	endcol:pr_ComponentRef
{
	## = #([ComponentRefAssignment,"ComponentRefAssignment"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_DisconnectStatement:
	(
		col:pr_DisconnectKeyword!
		(endcol:pr_SingleOrMultiConnectionSpec)?
	)
{
	## = #([DisconnectStatement,"DisconnectStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_DisconnectKeyword:
	col:DISCONNECT
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_SingleOrMultiConnectionSpec {LocationAST col=null,endcol=null;}:
	(//TODO: currently the tree is in disharmony, adjust!
		// XXX: shorten predicates?
		(pr_SingleConnectionSpec ) => 
		(	a:pr_SingleConnectionSpec {col=#a;} 
			(	e:pr_ParamClause {endcol = #e; }
			)?
		) 
		|
		(pr_AllConnectionsSpec) => 
		(	b:pr_AllConnectionsSpec {col=#b;} 
			(	f:pr_ParamClause {endcol = #f; }
			)?
		)
		|
		(pr_AllPortsSpec) => c:pr_AllPortsSpec {col=#c;} 
		|
		(pr_AllCompsAllPortsSpec) => d:pr_AllCompsAllPortsSpec{col=#d;}
	)
{
	## = #([SingleOrMultiConnectionSpec,"SingleOrMultiConnectionSpec"], ##);
	if(## != null) ##.setLocation(#col,#endcol==null?#col:#endcol);
};

pr_AllConnectionsSpec :
	(
		col:pr_LParen! pr_PortRef endcol:pr_RParen!
	)
{
	if(## != null) ##.setLocation(#col,#endcol);
};

pr_AllPortsSpec :
	(
		col:pr_LParen!
		pr_ComponentRef
		pr_Colon!
		pr_AllKeyword!
		pr_PortKeyword!
		endcol:pr_RParen!
	)
{
	if(## != null) ##.setLocation(#col,#endcol);
};

pr_AllCompsAllPortsSpec :
	(
		col:pr_LParen!
		pr_AllKeyword
		pr_ComponentKeyword
		pr_Colon!
		pr_AllKeyword
		pr_PortKeyword
		endcol:pr_RParen!
	)
{
 	if(## != null) ##.setLocation(#col,#endcol);
};

pr_MapStatement{LocationAST endcol = null;}:
		(
			col:pr_MapKeyword!
			b: pr_SingleConnectionSpec{endcol = #b;}
			(	a:pr_ParamClause {endcol = #a;}
			)?
		)
{
	## = #([MapStatement,"MapStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ParamClause: //TODO: submit CR on missing parens in BNF leading to inconsistency with clauses 21.1.1/2
	(
		col:pr_ParaKeyword!
		pr_LParen!
		pr_FunctionActualParList
		endcol:pr_RParen!
	)
{	
	## = #([ParamClause,"ParamClause"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
}
;

pr_MapKeyword:
		col:MAP
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_UnmapStatement://332
		(
			col:pr_UnmapKeyword!
			(endcol: pr_SingleOrMultiConnectionSpec  )?
		)
{
	## = #([UnmapStatement,"UnmapStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_UnmapKeyword:
		col:UNMAP
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_StartTCStatement:
		(
			col:pr_ComponentOrDefaultReference
			pr_Dot!
			pr_StartKeyword!
			pr_LParen!
			pr_FunctionInstance
			endcol:pr_RParen!
		)
{
	## = #([StartTCStatement,"StartTCStatement"], ##);
if(## != null) ##.setLocation(#col, #endcol);
};



pr_StartKeyword:
		col:START
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_StopTCStatement{LocationAST col=null; LocationAST endcol=null;}:
	(
		a:pr_StopKeyword! { col=#a;}
		| (b:pr_ComponentReferenceOrLiteral { col=#b;} pr_Dot! enda: pr_StopKeyword{endcol= #enda ;}  )
		| (c: pr_AllKeyword { col=#c;} pr_ComponentKeyword pr_Dot! endb:pr_StopKeyword {endcol= #endb;})
		| (	d:pr_TestcaseKeyword {col=#c;} 
			pr_Dot! 
			endd:pr_StopKeyword {endcol=#endd;}
			(	pr_LParen!
				(	(pr_FreeText)=>pr_FreeText
				  | pr_TemplateInstance
				)
				(	pr_Comma!
					(	(pr_FreeText)=>pr_FreeText
					  | pr_TemplateInstance
					)
				)*
				ende:pr_RParen! {endcol=#ende;}
			)?
		  ) //TODO:TEMPORARY HACK UNTIL BNF IS FIXED!!
	)
{
	## = #([StopTCStatement,"StopTCStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol );
};

pr_ComponentReferenceOrLiteral {LocationAST col=null;}://addtest
	 		(
	 		     a:pr_ComponentOrDefaultReference  {col=#a;}
	 		     |
	 		     b:pr_MTCOp {col=#b;}
	 		     |
	 		     c:pr_SelfOp{col=#c;}
	 		)
{
	## = #([ComponentReferenceOrLiteral,"ComponentReferenceOrLiteral"], ##);
		if(## != null) ##.setLocation(#col, #col);
};


//------------------------------------------------------
//               Port Operations   1.6.2.4
//------------------------------------------------------

pr_Port:
		(
			col:pr_PortIdentifier
			( endcol:pr_ArrayOrBitRef )*
		)
{
	## = #([Port,"Port"], ##);
if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_CommunicationStatements{ LocationAST col=null;}:
		(
				( pr_Port pr_Dot pr_SendOpKeyword ) =>
			a:pr_SendStatement { col = #a; }
			|
				( pr_Port pr_Dot pr_CallOpKeyword ) =>
			b:pr_CallStatement { col = #b; }
			|
				( pr_Port pr_Dot pr_ReplyKeyword ) =>
			c:pr_ReplyStatement { col = #c; }
			|
				( pr_Port pr_Dot pr_RaiseKeyword ) =>
			d:pr_RaiseStatement { col = #d; }
			|
				( pr_PortOrAny pr_Dot pr_ReceiveOpKeyword ) =>
			e:pr_ReceiveStatement { col = #e; }
			|
				( pr_PortOrAny pr_Dot pr_TriggerOpKeyword  ) =>
			f:pr_TriggerStatement { col = #f; }
			|
				( pr_PortOrAny pr_Dot pr_GetCallOpKeyword ) =>
			g:pr_GetCallStatement { col = #g; }
			|
				( pr_PortOrAny pr_Dot pr_GetReplyOpKeyword ) =>
			h:pr_GetReplyStatement { col = #h; }
			|
				( pr_PortOrAny pr_Dot pr_CatchOpKeyword ) =>
			i:pr_CatchStatement { col = #i; }
			|
				( pr_PortOrAny pr_Dot pr_CheckOpKeyword ) =>
			j:pr_CheckStatement { col = #j; }
			|
				( (pr_PortOrAny | (pr_AllKeyword pr_PortKeyword) ) pr_Dot pr_ClearOpKeyword ) =>
			k:pr_ClearStatement { col = #k; }
			|
				( (pr_PortOrAny | (pr_AllKeyword pr_PortKeyword) ) pr_Dot pr_StartKeyword ) =>
			l:pr_StartStatement { col = #l; }
			|
				( (pr_PortOrAny | (pr_AllKeyword pr_PortKeyword) ) pr_Dot pr_StopKeyword ) =>
			m:pr_StopStatement { col = #m; }
			|
				( (pr_PortOrAny | (pr_AllKeyword pr_PortKeyword) ) pr_Dot pr_HaltKeyword ) =>
			n:pr_HaltStatement { col = #n; } // see note at pr_HaltStatement
			|
				( (pr_PortOrAny | (pr_AllKeyword pr_PortKeyword) ) pr_Dot pr_CheckStateKeyword ) =>
			o:pr_CheckStateStatement { col = #o; } 
			
		)
{
	## = #([CommunicationStatements,"CommunicationStatements"], ##);
   if(## != null) ##.setLocation(#col, #col);
};

pr_SendStatement://343
		(
			col:pr_Port
			pr_Dot!
			endcol:pr_PortSendOp
		)
{
	## = #([SendStatement,"SendStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortSendOp:
		(
			pr_SendOpKeyword!
			pr_LParen!
			col:pr_SendParameter
			pr_RParen!
			// FIXME: may be too short predicate...
			( ( pr_ToKeyword ) => pr_ToClause )?
		)
{
   // ## = #([PortSendOp,"PortSendOp"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_SendOpKeyword:
		col:SEND
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_SendParameter:
		col:pr_TemplateInstance
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_ToClause{LocationAST col= null ; LocationAST endcol = null;}:
	(	a:pr_ToKeyword! 
		{col=#a;}
		(   (	pr_LParen! 
				pr_AddressRef
			) => (	c:pr_AddressRefList 
		  			{col=#c;}
		  		 )
		  |	(	pr_AddressRef
			) => (	b:pr_AddressRef 
					{endcol=#b;}
				 )
//changed order of the above to apture AddressRefLists
		  | (	d:pr_AllKeyword
		  		{col=#d;} 
		  		e:pr_ComponentKeyword{endcol=#e;} 
		  	)
		)
	)
	{
		## = #([ToClause,"ToClause"], ##);
    	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
	};

pr_AddressRefList :
	col:pr_LParen!
	pr_AddressRef
  	(	pr_Comma! 
  		pr_AddressRef
  	)*
  	endcol:pr_RParen!

{
   	## = #([AddressRefList,"AddressRefList"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ToKeyword:
		col:TO
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_AddressRef:
		(
			  col:pr_TemplateInstance
		)
{
	## = #([AddressRef,"AddressRef"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_CallStatement{LocationAST endcol = null;}:
		(
			col:pr_Port
			pr_Dot!
			a:pr_PortCallOp {endcol = #a;}
			( 	(pr_BeginChar!)=>b:pr_PortCallBody {endcol = #b;} 
			)?
		)
{
	## = #([CallStatement,"CallStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortCallOp{LocationAST endcol = null;}://352
		(
			col:pr_CallOpKeyword!
			pr_LParen!
			pr_CallParameters
			a:pr_RParen! { endcol = #a; }
			// FIXME: may be too short predicate...
			( (pr_ToKeyword ) => b:pr_ToClause { endcol = #b; } )?
		)
{
	//## = #([PortCallOp,"PortCallOp"], ##);
  if(## != null) ##.setLocation(#col, #endcol);
};

pr_CallOpKeyword:
		col:CALL
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_CallParameters:
		(
			col:pr_TemplateInstance
			(
				pr_Comma!
				endcol:pr_CallTimerValue
			)?
		)
{
	## = #([CallParameters,"CallParameters"], ##);
  if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_CallTimerValue{LocationAST col=null;}:
		(
			a:pr_TimerValue{ col = #a;}
			|
			b:pr_NowaitKeyword{ col = #b;}
		)
{
	## = #([CallTimerValue,"CallTimerValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_NowaitKeyword:
	col:NOWAIT!
{
	## = #([NowaitKeyword,"nowait"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_PortCallBody:
	(	col:pr_BeginChar!
		pr_CallBodyStatementList
		endcol:pr_EndChar!
	)
{
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_CallBodyStatementList{LocationAST endcol=null;}:
	(	col:pr_CallBodyStatement
	  	(	a:pr_SemiColon {endcol= #a; } 
	  	)?
	  	(	b:pr_CallBodyStatement {endcol= #b; }
	  		(	c:pr_SemiColon {endcol= #c; }
	  		)?
	  	)*
	)
{
   // ## = #([CallBodyStatementList,"CallBodyStatementList"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_CallBodyStatement:
	(	col:pr_CallBodyGuard
		endcol:pr_StatementBlock
	)
{
	## = #([CallBodyStatement,"CallBodyStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_CallBodyGuard://360
	(	col:pr_AltGuardChar
		endcol:pr_CallBodyOps
	)
{
	## = #([CallBodyGuard,"CallBodyGuard"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_CallBodyOps{LocationAST col=null;}:
	(	(pr_PortOrAny pr_Dot pr_GetReplyOpKeyword) =>
		a:pr_GetReplyStatement{ col = #a;}
	  |	b:pr_CatchStatement{ col = #b;}
	)
{
	## = #([CallBodyOps,"CallBodyOps"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_ReplyStatement:
		(
			col:pr_Port
			pr_Dot!
			endcol:pr_PortReplyOp
		)
{
	## = #([ReplyStatement,"ReplyStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortReplyOp{LocationAST endcol=null;}:
		(
			col:pr_ReplyKeyword!
			pr_LParen!
			pr_TemplateInstance
			( pr_ReplyValue )?
			a:pr_RParen! { endcol = #a; }
			// FIXME: may be too short predicates...
			( (pr_ToKeyword ) => b:pr_ToClause { endcol = #b; } )?
		)
{
	## = #([PortReplyOp,"PortReplyOp"], ##);
	if(## != null) ##.setLocation(#col, endcol);
};


pr_ReplyKeyword:
		col:REPLY
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ReplyValue:
		(
			col:pr_ValueKeyword!
			endcol:pr_Expression
		)
{
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_RaiseStatement:
		(
			col:pr_Port
			pr_Dot!
			endcol:pr_PortRaiseOp
		)
{
	## = #([RaiseStatement,"RaiseStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortRaiseOp{LocationAST endcol=null;}:
		(
			col:pr_RaiseKeyword!
			pr_LParen!
			pr_Signature
			pr_Comma!
			pr_TemplateInstance
			a:pr_RParen! { endcol = #a; }
			// FIXME: may be too short predicates...
			( (pr_ToKeyword ) => b:pr_ToClause { endcol = #b; } )?
		)
{
	## = #([PortRaiseOp,"PortRaiseOp"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_RaiseKeyword://368
		col:RAISE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ReceiveStatement:
		(
			col:pr_PortOrAny
			pr_Dot!
			endcol:pr_PortReceiveOp
		)
{
	## = #([ReceiveStatement,"ReceiveStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortOrAny{ LocationAST col=null; LocationAST endcol=null;}:
		(
			a:pr_Port { col = #a; }
			|
			b:pr_AnyKeyword  { col = #b; }
 		    x:pr_PortKeyword { endcol =#x; }
		)
{
	## = #([PortOrAny,"PortOrAny"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_PortReceiveOp{LocationAST endcol=null;}:
		(
			col:pr_ReceiveOpKeyword!
			(
				(
					pr_LParen!
					pr_ReceiveParameter
					a:pr_RParen! { endcol =#a; }
				)
			)?
			( b:pr_FromClause { endcol =#b; } )?
			( c:pr_PortRedirect { endcol =#c; } )?
		)
{
	## = #([PortReceiveOp,"PortReceiveOp"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_ReceiveOpKeyword:
		col:RECEIVE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_ReceiveParameter:
		col:pr_TemplateInstance
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_FromClause{LocationAST col= null ; LocationAST endcol = null;}:
	//copied largely from pr_ToClause
	(	a:pr_FromKeyword!
		{col=#a;}
		(	(	pr_LParen! 
				pr_AddressRef
			) => (	c:pr_AddressRefList 
		  			{col=#c;}
		  		 ) //MISSING endcol?
//These to needed to be reordered	
		  |	(	pr_AddressRef
			) => (	b:pr_AddressRef 
					{endcol=#b;}
				 )
		  | (	d:pr_AnyKeyword //only change from pr_AllKeyword
		  		{col=#d;} 
		  		e:pr_ComponentKeyword{endcol=#e;} 
		  	)
		)
	)
{
	## = #([FromClause,"FromClause"], ##);
	//if(## != null) ##.setLocation(#col, #endcol);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol); //this probably fixes it
};


pr_FromKeyword:
		col:FROM
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_PortRedirect{LocationAST endcol = null;}://376
		(
			col:pr_PortRedirectSymbol!
			(
				(
					a:pr_ValueSpec {endcol = #a;}
					( b:pr_SenderSpec {endcol = #b;})?
				)
				|
				c:pr_SenderSpec {endcol = #c;}
			)
		)
{
	## = #([PortRedirect,"PortRedirect"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_PortRedirectSymbol:
		col:PORTREDIRECTSYMBOL
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

//Alternative implementation based on suggestion by Anthony Baire 
//Mantis Issue: 0005365
pr_SingleValueSpec{LocationAST endcol=null;}:
	col:pr_VariableRef
	(	pr_AssignmentChar!
		pr_FieldReference
		endcol:pr_ExtendedFieldReference
	)?

/* 	(  	//(pr_VariableRef pr_AssignmentChar)=>
 		(	(pr_Identifier)=>pr_VariableRef
			//(pr_AssignmentChar!)=>
			(	pr_AssignmentChar!
				(	(pr_FieldReference) => pr_FieldReference
				  |	pr_TypeDefIdentifier
				)
				pr_ExtendedFieldReference
			)?		 	     	
    	)?
    	(	pr_Comma!	
    	)?
 	)*
*/
{
	## = #([SingleValueSpec,"SingleValueSpec"], ##);
	if(## != null) ##.setLocation(#col, endcol==null?#col:#endcol);
};


pr_ValueSpec{LocationAST col = null; LocationAST endcol=null;}:
	(	col:pr_ValueKeyword!
  		(
	  		//(pr_Identifier) =>
	  		( 	a:pr_VariableRef {endcol = #a;}
		  	)
		  |	( 	b:pr_LParen! {col = #b;}
				pr_SingleValueSpec
				(	pr_Comma!
					pr_SingleValueSpec
				)*
//		 	 	(  	//(pr_VariableRef pr_AssignmentChar)=>
//		 	 		(	(pr_Identifier)=>pr_VariableRef
//						//(pr_AssignmentChar!)=>
//						(	pr_AssignmentChar!
//							(	(pr_FieldReference) => pr_FieldReference
//							  |	pr_TypeDefIdentifier
//							)
//							pr_ExtendedFieldReference
//						)?		 	     	
//		 	    	)?
//		 	    	(	pr_Comma!	
//		 	    	)?
//		 	 	)*
		 	 	c:pr_RParen! {endcol = #c;}
			)
		)
	)
{
	## = #([ValueSpec,"ValueSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ValueKeyword:
		col:VALUE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_SenderSpec:
		(
			col:pr_SenderKeyword!
			endcol:pr_VariableRef
		)
{
	## = #([SenderSpec,"SenderSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_SenderKeyword:
		col:SENDER
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_TriggerStatement:
		(
			col:pr_PortOrAny
			pr_Dot!
			endcol:pr_PortTriggerOp
		)
{
	## = #([TriggerStatement,"TriggerStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortTriggerOp{LocationAST endcol = null;}:
		(
			col:pr_TriggerOpKeyword!
			(
				pr_LParen!
				pr_ReceiveParameter
				a:pr_RParen! {endcol = #a;}
			)?
			( b:pr_FromClause )? {endcol = #b;}
			( c:pr_PortRedirect )? {endcol = #c;}
		)
{
	## = #([PortTriggerOp,"PortTriggerOp"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_TriggerOpKeyword:
		col:TRIGGER
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_GetCallStatement://360
		(
			col:pr_PortOrAny
			pr_Dot!
			endcol:pr_PortGetCallOp
		)
{
	## = #([GetCallStatement,"GetCallStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortGetCallOp{LocationAST endcol=null;}:
		(
			col:pr_GetCallOpKeyword!
			(
				pr_LParen!
				pr_ReceiveParameter
				a:pr_RParen! { endcol =#a; }
			)?
			( b:pr_FromClause { endcol =#b; } )?
			( c:pr_PortRedirectWithParam { endcol =#c; } )?
		)
{
	## = #([PortGetCallOp,"PortGetCallOp"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_GetCallOpKeyword:
		col:GETCALL
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_PortRedirectWithParam:
		(
			col:pr_PortRedirectSymbol!
		  endcol:pr_RedirectWithParamSpec
		)
{
	if(## != null) ##.setLocation(#col, #col);
};


pr_RedirectWithParamSpec {LocationAST bcol=null; LocationAST ecol = null;}:
		(

				 colc:pr_ParaSpec {ecol=#colc;}
				( cold:pr_SenderSpec {bcol=#cold;} )?

			|
			 cole:pr_SenderSpec {bcol=#cole;}

		)
{
	## = #([RedirectWithParamSpec,"RedirectWithParamSpec"], ##);
	if(## != null) ##.setLocation(#ecol, #bcol);
};



pr_ParaSpec:
		(
			col:pr_ParaKeyword!
			endcol:pr_ParaAssignmentList
		)
{
	## = #([ParaSpec,"ParaSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ParaKeyword://391
		col:PARAM
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ParaAssignmentList:
		(
			col:pr_LParen!
			(
					( pr_VariableRef pr_AssignmentChar! ) =>
				pr_AssignmentList
				|
				pr_VariableList
			)
			endcol:pr_RParen!
		)
{
	## = #([ParaAssignmentList,"ParaAssignmentList"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_AssignmentList:
		(
			col:pr_VariableAssignment
			(
				pr_Comma!
				endcol:pr_VariableAssignment
			)*
		)
{
	## = #([AssignmentList,"AssignmentList"], ##);
	if( ## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_VariableAssignment:
		(
			col:pr_VariableRef
			pr_AssignmentChar!
			endcol:pr_ParameterIdentifier
		)
{
	## = #([VariableAssignment,"VariableAssignment"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ParameterIdentifier:
		col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};


pr_PortRedirectWithValueAndParam :

(
	col:pr_PortRedirectSymbol!
	endcol:pr_RedirectWithValueAndParamSpec
)
{
## = #([PortRedirectWithValueAndParam,"PortRedirectWithValueAndParam"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_RedirectWithValueAndParamSpec{LocationAST endcol=null;} :

 (
	col:pr_ValueSpec
	 (pr_ParaSpec)?
	 (a:pr_SenderSpec {endcol=#a;})?
	  |
	  b:pr_RedirectWithParamSpec{endcol=#b;}

)
{
## = #([RedirectWithValueAndParamSpec,"RedirectWithValueAndParamSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_VariableList:
		(
			col:pr_VariableEntry
			(
				pr_Comma!
				endcol:pr_VariableEntry
			)*
		)
{
	## = #([VariableList,"VariableList"], ##);
	if(## != null) ##.setLocation(#col, #endcol == null?#col:#endcol);
};

pr_VariableEntry{LocationAST col=null;}:
		(
			a:pr_VariableRef{ col = #a;}
			|
			b:pr_NotUsedSymbol{ col = #b;}
		)
{
	## = #([VariableEntry,"VariableEntry"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_GetReplyStatement:
		(
			col:pr_PortOrAny
			pr_Dot!
			endcol:pr_PortGetReplyOp
		)
{
	## = #([GetReplyStatement,"GetReplyStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortGetReplyOp{LocationAST endcol=null;}://399
		(
			col:pr_GetReplyOpKeyword!
			(
				pr_LParen!
				pr_ReceiveParameter
				( pr_ValueMatchSpec )?
				a:pr_RParen! { endcol =#a; }
			)?
			( b:pr_FromClause { endcol =#b; } )?
			( c:pr_PortRedirectWithValueAndParam { endcol =#c; } )?
		)
{
	## = #([PortGetReplyOp,"PortGetReplyOp"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_GetReplyOpKeyword:
		col:GETREPLY
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ValueMatchSpec:
		(
			col:pr_ValueKeyword!
			endcol:pr_TemplateInstance
		)
{
	## = #([ValueMatchSpec,"ValueMatchSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_CheckStatement:
		(
			col:pr_PortOrAny
			pr_Dot!
			endcol:pr_PortCheckOp
		)
{
	## = #([CheckStatement,"CheckStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortCheckOp://405
		(
			col:pr_CheckOpKeyword!
			(
				pr_LParen!
				pr_CheckParameter
				endcol:pr_RParen!
			)?
		)
{
	## = #([PortCheckOp,"PortCheckOp"], ##);
   	if(## != null) ##.setLocation(#col, #endcol == null?#col:#endcol);
};


pr_CheckOpKeyword:
		col:CHECK
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_CheckParameter{LocationAST col=null;}:
		(
			a:pr_CheckPortOpsPresent{ col = #a;}
			|
			b:pr_FromClausePresent{ col = #b;}
			|
			c:pr_RedirectPresent{ col = #c;}
		)
{
	## = #([CheckParameter,"CheckParameter"], ##);
	if(## != null) ##.setLocation(#col,#col);

};

pr_FromClausePresent:
				(
				 col:pr_FromClause
				 (
				 pr_PortRedirectSymbol!
				 pr_SenderSpec)?
				)
{
	## = #([FromClausePresent,"FromClausePresent"], ##);
};

pr_CheckPortOpsPresent://addtest
					(
					  pr_PortReceiveOp
					  |
					  pr_PortGetCallOp
					  |
					  pr_PortGetReplyOp
					  |
					  pr_PortCatchOp
					)
{
	## = #([CheckPortOpsPresent,"CheckPortOpsPresent"], ##);
};

pr_RedirectPresent:
				(
				col:pr_PortRedirectSymbol!
				pr_SenderSpec
				)
{
	## = #([RedirectPresent,"RedirectPresent"], ##);
};

pr_CatchStatement:
		(
			col:pr_PortOrAny
			pr_Dot!
			endcol:pr_PortCatchOp
		)
{
	## = #([CatchStatement,"CatchStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortCatchOp{LocationAST endcol=null;}:
		(
		 col:pr_CatchOpKeyword!
		 (
			pr_LParen!
			pr_CatchOpParameter
			a:pr_RParen! { endcol =#a; }
		 )?
		 ( b:pr_FromClause { endcol =#b; } )?
  		 ( c:pr_PortRedirect { endcol =#c; } )?
		)
{
	## = #([PortCatchOp,"PortCatchOp"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_CatchOpKeyword://413
		col:CATCH
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_CatchOpParameter:
	(
			(
				begcol:pr_Signature
				pr_Comma!
				endcol:pr_TemplateInstance
			)
		|
			( col:pr_TimeoutKeyword )
	)
{
	## = #([CatchOpParameter,"CatchOpParameter"], ##);
	if(#begcol != null) {
	if(## != null) ##.setLocation(#begcol, #endcol);
	} else
	if(## != null) ##.setLocation(#col, #col);
};

pr_ClearStatement:
		(
			col:pr_PortOrAll
			pr_Dot!
			endcol:pr_PortClearOp
		)
{
	## = #([ClearStatement,"ClearStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortOrAll{LocationAST col=null;}:
		(
			a:pr_Port{ col = #a;}
			|
			b:pr_AllPort{ col = #b;}
		)
{
	## = #([PortOrAll,"PortOrAll"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_AllPort:
		(
			col:pr_AllKeyword!
			endcol:pr_PortKeyword!
		)
{
	## = #([AllPort,"AllPort"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_PortClearOp:
		col:pr_ClearOpKeyword!
{
	## = #([PortClearOp,"PortClearOp"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_ClearOpKeyword:
		col:CLEAR
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_StartStatement:
		(
			col:pr_PortOrAll
			pr_Dot!
			endcol:pr_PortStartOp
		)
{
	## = #([StartStatement,"StartStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_PortStartOp:
		col:pr_StartKeyword!
{
	## = #([PortStartOp,"PortStartOp"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_StopStatement:
		(
			col:pr_PortOrAll
			pr_Dot!
			endcol:pr_PortStopOp
		)
{
	## = #([StopStatement,"StopStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortStopOp:
		col:pr_StopKeyword!
{
	## = #([PortStopOp,"PortStopOp"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_StopKeyword://423
		col:STOP
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_AnyKeyword:
		col:ANY!
{
	## = #([AnyKeyword,"any"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

// -------------------------------------------------------------------------------------------
// The halt statement has been added to TTCN-3 v.3.1.1, but no the BNF.
// We just added it to the BNF in the intended spirit.
// note: compare specification v.3.1.1 sec. 23.5.4 "The halt port operation"
// -------------------------------------------------------------------------------------------
pr_HaltStatement:
		(
			col:pr_PortOrAll
			pr_Dot!
			endcol:pr_PortHaltOp
		)
{
	## = #([HaltStatement,"HaltStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortHaltOp:
		col:pr_HaltKeyword!
{
	## = #([PortHaltOp,"PortHaltOp"], ##);
	if(## != null) ##.setLocation(#col, #col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};


pr_HaltKeyword:
		col:HALT
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_CheckStateStatement:
		(
			col:pr_PortOrAllOrAny
			pr_Dot!
			pr_CheckStateKeyword!
			pr_LParen!
			pr_SingleExpression
			endcol:pr_RParen!
		)
{
	## = #([CheckStateStatement,"CheckStateStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_PortOrAllOrAny{LocationAST col=null,endcol=null;}:
		(
			a:pr_PortOrAll{ col = #a;}
			|
			(	b:pr_AnyKeyword{ col = #b;}
				c:pr_PortKeyword!{ endcol= #c;}
			)
		)
{
	## = #([PortOrAllOrAny,"PortOrAllOrAny"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_CheckStateKeyword:
		col:CHECKSTATE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};




//------------------------------------------------------
// Timer Operations      1.6.2.5
//------------------------------------------------------

pr_TimerStatements{ LocationAST col=null;}:
	(
		(pr_StartTimerStatement{ col = #a; }) => a:pr_StartTimerStatement { col = #a; }
	  | (pr_StopTimerStatement{ col = #a; }) => b:pr_StopTimerStatement { col = #b; }
	  | ( pr_TimeoutStatement ) => c:pr_TimeoutStatement { col = #c; }
	)
{
	## = #([TimerStatements,"TimerStatements"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_TimerOps{ LocationAST col=null;}:
	(
		( pr_ReadTimerOp ) => a:pr_ReadTimerOp { col = #a; }
	  | b:pr_RunningTimerOp { col = #b; }
	)
{
	## = #([TimerOps,"TimerOps"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_StartTimerStatement:
		(
			col:pr_TimerRef
			pr_Dot!
			pr_StartKeyword!
			(
				pr_LParen!
				pr_TimerValue
				pr_RParen!
			)?
		)
{
	## = #([StartTimerStatement,"StartTimerStatement"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_StopTimerStatement:
		(
			col:pr_TimerRefOrAll
			pr_Dot!
			endcol:pr_StopKeyword!
		)
{
	## = #([StopTimerStatement,"StopTimerStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_TimerRefOrAll{ LocationAST col=null;}://429
		(
		  a: pr_TimerRef {col= #a;}
		|
		  (
	 	    b: pr_AllKeyword {col= #b;}
			endcol: pr_TimerKeyword
		  )
		)
{
	## = #([TimerRefOrAll,"TimerRefOrAll"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?col:#endcol);
};

pr_ReadTimerOp:
		(
			col:pr_TimerRef
			pr_Dot!
			endcol:pr_ReadKeyword!
		)
{
	## = #([ReadTimerOp,"ReadTimerOp"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ReadKeyword:
		col:READ
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_RunningTimerOp:
		(
			col:pr_TimerRefOrAny
			pr_Dot!
			endcol:pr_RunningKeyword!
		)
{
	## = #([RunningTimerOp,"RunningTimerOp"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_TimeoutStatement:
		(
		  col:pr_TimerRefOrAny
		  pr_Dot!
			  endcol:pr_TimeoutKeyword!
		)
{
	## = #([TimeoutStatement,"TimeoutStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol!=null?#col:#endcol);
};

pr_TimerRefOrAny{ LocationAST col=null;}:
		(
		  a: pr_TimerRef {col= #a;}
		|
		  (
	 	    b: pr_AnyKeyword {col= #b;}
			endcol: pr_TimerKeyword
		  )
		)
{
	## = #([TimerRefOrAny,"TimerRefOrAny"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?col:#endcol);
};

pr_TimeoutKeyword://435
		col:TIMEOUT!
{
	## = #([TimeoutKeyword,"timeout"], ##);
	if(## != null) ##.setLocation(#col, #col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

//orignal at least partially wrong or just plain lazy
pr_TestcaseOperation{LocationAST endcol=null;}:
	(	col:pr_TestcaseKeyword!
		pr_Dot!
		a:pr_StopKeyword!{endcol=#a;}
		(	pr_LParen!
			(	(pr_FreeText)=>pr_FreeText
			  | pr_InLineTemplate
			)
			(	pr_Comma!
				(	(pr_FreeText)=>pr_FreeText
			 	  | pr_InLineTemplate
				)
			)*
			b:pr_RParen!{endcol=#b;}
		)?
	)
{
	## = #([TestcaseOperation,"TestcaseOperation"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};
	



//------------------------------------------------------
//              Type        1.6.3
//------------------------------------------------------

pr_Type {LocationAST col=null;}:
		(
			a:pr_PredefinedType{ col = #a;}
			|
			b:pr_ReferencedType{ col = #b;}
		)
{
  ## = #([Type,"Type"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_PredefinedType { LocationAST col=null;}:
		(
			a:pr_BitStringKeyword { col = #a; }
			|
			b:pr_HexStringKeyword { col = #b; }
			|
			c:pr_OctetStringKeyword { col = #c; }
			|
			d:pr_BooleanKeyword { col = #d; }
			|
			e:pr_CharStringKeyword { col = #e; }
			|
			f:pr_UniversalCharString { col = #f; }
			|
			i:pr_IntegerKeyword { col = #i; }
			|
			k:pr_VerdictTypeKeyword { col = #k; }
			|
			l:pr_FloatKeyword { col = #l; }
			|
			m:pr_AddressKeyword { col = #m; }
			|
			o:pr_DefaultKeyword { col = #o; }
			|
			p:pr_AnyTypeKeyword {col= #p; }
			|
			r:pr_ObjectIdentifierKeyword {col=#r;}
			| // static error handling
			q:pr_CharKeyword { col=#q; throw new SemanticException("The char synonym is not permitted in TTCN-3 version 3.1.1. Define your own charstring of fixed length.", getFilename(), col.getLine(), col.getColumn()); }
		)
{
	## = #([PredefinedType,"PredefinedType"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_BitStringKeyword://438
		col:BITSTRING!
{
	## = #([BitStringKeyword,"bitstring"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_BooleanKeyword:
		col:BOOLEAN!
{
	## = #([BooleanKeyword,"boolean"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_IntegerKeyword:
		col:INTEGER!
{
	## = #([IntegerKeyword,"integer"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};


pr_OctetStringKeyword:
		col:OCTETSTRING!
{
	## = #([OctetStringKeyword,"octetstring"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};


pr_HexStringKeyword:
		col:HEXSTRING!
{
	## = #([HexStringKeyword,"hexstring"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_VerdictTypeKeyword:
		col:VERDICTTYPE!
{
	## = #([VerdictTypeKeyword,"verdicttype"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_FloatKeyword:
		col:FLOAT!
{
	## = #([FloatKeyword,"float"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_AddressKeyword:
		col:ADDRESS!
{
	## = #([AddressKeyword,"address"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_CharStringKeyword:
		col:CHARSTRING!
{
	## = #([CharStringKeyword,"charstring"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_UniversalCharString:
		(
			col:pr_UniversalKeyword
			endcol:pr_CharStringKeyword
		)
{
	## = #([UniversalCharString,"universal charstring"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_CharKeyword:
		col:CHAR!
{
	## = #([CharKeyword,"char"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_UniversalKeyword:
		col:UNIVERSAL!
{
	## = #([UniversalKeyword,"universal"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_DefaultKeyword://446
		col:DEFAULT!
{
	## = #([DefaultKeyword,"default"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_AnyTypeKeyword:
		col:ANYTYPE!
{
	## = #([AnyTypeKeyword,"anytype"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_ReferencedType:
		(
			(
					( pr_GlobalModuleId pr_Dot ) =>
				cola:pr_GlobalModuleId
				pr_Dot!
				|
			)
			col:pr_TypeReference
			(
					( pr_Dot | pr_SquareOpen | pr_LParen ) =>
				colb:pr_ExtendedFieldReference
				|
			)
		)
{
	## = #([ReferencedType,"ReferencedType"], ##);
	if(## != null) ##.setLocation(#cola==null?#col:#cola, #colb==null?#col:#colb);
};

pr_TypeReference:
		(
			col:pr_TypeReferenceId
			(
					( pr_LParen ) =>
				endcol:pr_TypeActualParList
				|
			)
		)
{
	## = #([TypeReference,"TypeReference"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_TypeReferenceId:
		col:pr_Identifier
{

	if(## != null) ##.setLocation(#col, #col);
};

pr_TypeActualParList:
	col:pr_LParen! pr_TypeActualPar (pr_Comma! pr_TypeActualPar)* endcol:pr_RParen!
{
	## = #([TypeActualParList,"TypeActualParList"], ##);
if(## != null) ##.setLocation(#col, #endcol);
};

pr_TypeActualPar{LocationAST col=null;}://454
	col:pr_ConstantExpression
{
	## = #([TypeActualPar,"TypeActualPar"], ##);
	if(## != null) ##.setLocation(#col,#col);
};

pr_ArrayDef:
	(
		(
			col:pr_SquareOpen!
			(
				( pr_ArrayBounds pr_RangeOp) => pr_ArrayDefRange | pr_ArrayBounds
			)
			endcol:pr_SquareClose!
		)+
	)
{
	## = #([ArrayDef,"ArrayDef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ArrayDefRange:
	col:pr_ArrayBounds pr_RangeOp! endcol:pr_ArrayBounds
{
	## = #([ArrayDefRange,"ArrayDefRange"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
}
;

pr_ArrayBounds:
	col:pr_SingleConstExpression
{
   // ## = #([ArrayBounds,"ArrayBounds"], ##);
   if(## != null) ##.setLocation(#col, #col);
};

//------------------------------------------------------
//                    Value   1.6.4
//------------------------------------------------------

pr_Value{LocationAST col=null;}:
	(
		( pr_PredefinedValue ) => a:pr_PredefinedValue { col = #a; }
		| b:pr_ReferencedValue { col = #b; }
	)
{
	## = #([Value,"Value"], ##);
   if(## != null) ##.setLocation(#col, #col);
};


pr_PredefinedValue{ LocationAST col=null; }:
	(
		a1:pr_HexStringValue { col = #a1; }
	  | a2:pr_BitStringValue { col = #a2; }
	  | a3:pr_OctetStringValue { col = #a3; }
	  | ( pr_BooleanValue ) => b:pr_BooleanValue { col = #b; }
	  | ( pr_CharStringValue ) => c:pr_CharStringValue { col = #c; }
	  | d:pr_IntegerValue { col = #d; }
	  | f:pr_VerdictTypeValue { col = #f; }
	  | g:pr_EnumeratedValue { col = #g; }
	  | h:pr_FloatValue { col = #h; }
	  | l:pr_NaNValue { col = #l; }
	  | i:pr_AddressValue { col = #i; }
	  | j:pr_OmitValue { col = #j; }
	  | k:pr_ObjectIdentifierValue { col = #k;}
	)
{
	## = #([PredefinedValue,"PredefinedValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_OmitValue:
	col:pr_OmitKeyword!
{
	## = #([OmitValue,"OmitValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_BitStringValue:
	col:BSTRING
{
	## = #([BitStringValue,"BitStringValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_OctetStringValue:
	col:OSTRING
{
	## = #([OctetStringValue,"OctetStringValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_HexStringValue:
	col:HSTRING
{
	## = #([HexStringValue,"HexStringValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_BooleanValue{ LocationAST col=null;}://460
	(
		a:pr_True { col = #a; }
	  | b:pr_False { col = #b; }
	)
{
	## = #([BooleanValue,"BooleanValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_True:
	col:TRUE
{
	//## = #([True,"True"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_False:
	col:FALSE
{
	//## = #([False,"False"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_IntegerValue:
	col:NUMBER
{
	## = #([IntegerValue,"IntegerValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
};

pr_Number:
	col: NUMBER
{
	## = #([Number,"Number"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_VerdictTypeValue{ LocationAST col=null;}:
	(
		a:pr_Pass { col = #a; }
	  | b:pr_Fail { col = #b; }
	  | c:pr_Inconc { col = #c; }
	  | d:pr_None { col = #d; }
	  | e:pr_Error { col = #e; }
	)
{
	## = #([VerdictTypeValue,"VerdictTypeValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
//	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_Pass:
	col:PASS!
{
	## = #([Pass,"Pass"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_Fail:
	col:FAIL!
{
	## = #([Fail,"Fail"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_Inconc:
	col:INCONC!
{
	## = #([Inconc,"Inconc"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_None:
	col:NONE!
{
	## = #([None,"None"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_Error:
	col:ERROR!
{
	## = #([Error,"Error"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_EnumeratedValue://465
	col:pr_EnumerationIdentifier
{
	## = #([EnumeratedValue,"EnumeratedValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_CharStringValue{ LocationAST col=null;}:
	(
		a:CSTRING { col = #a; }
	  | b:pr_Quadruple { col = #b; }
	)
{
	## = #([CharStringValue,"CharStringValue"], ##);
	if(## != null) ##.setLocation(col,col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_Quadruple:
	(
		col:pr_CharKeyword!
		pr_LParen!
		pr_Group
		pr_Comma!
		pr_Plane
		pr_Comma!
		pr_Row
		pr_Comma!
		pr_Cell
		endcol:pr_RParen!
	)
{
	## = #([Quadruple,"Quadruple"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_Group:
	col:NUMBER
{
	## = #([Group,"Group"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_Plane:
	col:NUMBER
{
	## = #([Plane,"Plane"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_Row:
	col:NUMBER
{
	## = #([Row,"Row"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_Cell:
	col:NUMBER
{
	## = #([Cell,"Cell"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_FloatValue:
	col:FLOATVALUE
//  ( (pr_FloatDotNotation)=>a:pr_FloatDotNotation { col = #a; }|(pr_FloatENotation)=>b:pr_FloatENotation { col = #b; })
{
	## = #([FloatValue,"FloatValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

//NOTE: a bit of a hack currently due to the unusual handling of FloatValue
pr_NaNValue:
	col:NAN
{
	## = #([NaN,"NaN"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(##.getOffset(), ##.getEndOffset(), ##);
	
}
;

pr_FloatENotation:
   (
		col:pr_Number
		(pr_Dot pr_DecimalNumber)?
		pr_Exponential
		(pr_Minus)?
		endcol:pr_Number
   )
{
	## = #([FloatENotation,"FloatENotation"], ##);
};

pr_Exponential:
	col:EXPONENT
{
	## = #([EXPONENT,"EXPONENT"], ##);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_FloatDotNotation:
	(
		col:pr_Number
		pr_Dot!
		endcol:Decimalnumber
	)
{
	## = #([FloatDotNotation,"FloatDotNotation"], ##);
};

pr_DecimalNumber:
	col:Decimalnumber
{
	## = #([Decimalnumber,"Decimalnumber"], ##);
	if(## != null) ##.setLocation(#col, #col);
}
;

pr_ReferencedValue:
	col:pr_ValueReference
	( ( pr_Dot | pr_SquareOpen ) => endcol:pr_ExtendedFieldReference )?
{
	## = #([ReferencedValue,"ReferencedValue"], ##);
	if(## != null) ##.setLocation(#col, #endcol == null?#col:#endcol);
};

pr_ValueReference{LocationAST col = null;}:
	((pr_GlobalModuleId DOT) => (a:pr_GlobalModuleId pr_Dot!))?
	endcol:pr_Identifier
{
	## = #([ValueReference,"ValueReference"], ##);
	if(## != null) ##.setLocation(#a==null?#endcol:#a,#endcol);
};

pr_AddressValue:
		col:pr_Null
{
//	## = #([AddressValue,"AddressValue"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_Null:
	col:NULL
{
	## = #([Null,"Null"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


//------------------------------------------------------
// Parameterisation     1.6.5
//------------------------------------------------------
pr_InParKeyword:
		col:IN
{
	## = #([InParKeyword,"in"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_OutParKeyword:
		col:OUT
{
	## = #([OutParKeyword,"out"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_InOutParKeyword:
	 col:INOUT
{
	## = #([InOutParKeyword,"inout"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_FormalValuePar{LocationAST bcol = null; LocationAST endcol = null;}://504
		(
			(
				cola:pr_InParKeyword {bcol = #cola;}
			|
				colb:pr_InOutParKeyword {bcol = #colb;}
			|
				colc:pr_OutParKeyword {bcol = #colc;}
			)?
			col:pr_Type
			endcol:pr_ValueParIdentifier
			(	pr_AssignmentChar!
				(	(pr_Expression)=>a:pr_Expression {endcol= #a ;}
				  | b:pr_Dash {endcol = #b;}
				)
			)?
		)
{
	## = #([FormalValuePar,"FormalValuePar"], ##);
	if(## != null) ##.setLocation(#bcol!=null?#bcol:#col, #endcol);
};


pr_ValueParIdentifier:
		col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_FormalPortPar:
		(
			( cola:pr_InOutParKeyword )?
			col:pr_PortTypeIdentifier
			endcol:pr_PortParIdentifier
		)
{
	## = #([FormalPortPar,"FormalPortPar"], ##);
	if(## != null) ##.setLocation(#cola==null?#col:#cola, #endcol);
};


pr_PortParIdentifier:
		col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_FormalTimerPar:
		(
			( cola:pr_InOutParKeyword )?
			col:pr_TimerKeyword
			endcol:pr_TimerParIdentifier
		)
{
	## = #([FormalTimerPar,"FormalTimerPar"], ##);
	if(## != null) ##.setLocation(#cola==null?#col:#cola, #endcol);
};


pr_TimerParIdentifier://509
		col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};



pr_FormalTemplatePar{LocationAST cola = null,colb = null,endcol=null; }:
	(	(	a:pr_InParKeyword {cola=#a;}
		  | b:pr_OutParKeyword {cola=#b;}
		  | c:pr_InOutParKeyword {cola=#c;}
		)?
//new

		(	(	pr_TemplateKeyword
			) => (	d:pr_TemplateKeyword 
					{colb=#d;}
				 )
		  | e:pr_RestrictedTemplate 
		  	{colb=#e;}
		)
		
//instead of 
//		col:pr_TemplateKeyword!
		pr_Type
		endcol:pr_TemplateParIdentifier
		(	pr_AssignmentChar!
			(	(pr_TemplateInstance)=>f:pr_TemplateInstance {endcol= #f ;}
			  | g:pr_Dash {endcol = #g;}
			)
		)?
	)
{
	## = #([FormalTemplatePar,"FormalTemplatePar"], ##);
//new
	if (#cola != null){
		if(## != null) ##.setLocation(#cola, #endcol);
	} else {
		if(## != null) ##.setLocation(#colb, #endcol);
	}
//instead of 
//	if(## != null) ##.setLocation(#cola==null?#col:#cola, #endcol);

};


pr_TemplateParIdentifier:
		col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

//------------------------------------------------------
//    With Statement                 1.6.6
//------------------------------------------------------

pr_WithStatement:
		(
			col:pr_WithKeyword!
			endcol:pr_WithAttribList
		)
{
	## = #([WithStatement,"WithStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_WithKeyword:
	col:WITH
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_WithAttribList:
		(
				col:pr_BeginChar!
				pr_MultiWithAttrib
				endcol:pr_EndChar!
		 )
{
//    	## = #([WithAttribList,"WithAttribList"], ##);
if(## != null) ##.setLocation(#col, #endcol);
};

pr_MultiWithAttrib{LocationAST col=null;LocationAST endcol = null;}:
		(
			(
			  b:pr_SingleWithAttrib{ col = #b; }
			  ( c:pr_SemiColon { endcol = #c; })?
			)*
		)
{
//    ## = #([MultiWithAttrib,"MultiWithAttrib"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_SingleWithAttrib:
		(
			col:pr_AttribKeyword
		( pr_OverrideKeyword )?
			( pr_AttribQualifier )?
			endcol:pr_AttribSpec
		)
{
	## = #([SingleWithAttrib,"SingleWithAttrib"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_AttribKeyword{ LocationAST col=null;}://517
		(	a:pr_EncodeKeyword { col = #a; }
			|
			b:pr_DisplayKeyword { col = #b; }
			|
			c:pr_ExtensionKeyword { col = #c; }
			|
			d:pr_VariationKeyword { col = #d; }
			|
			e:pr_OptionalKeyword { col = #e; } //optional k
			
		)
{
//    ## = #([AttribKeyword,"AttribKeyword"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_EncodeKeyword:
		col:ENCODE
{
//	## = #([EncodeKeyword,"EncodeKeyword"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_VariationKeyword:
   col:VARIANT
{
//	## = #([VariationKeyword,"VariationKeyword"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_DisplayKeyword:
		col:DISPLAY
{
//    ## = #([DisplayKeyword,"DisplayKeyword"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_ExtensionKeyword:
		col:EXTENSION
{
//	## = #([ExtensionKeyword,"ExtensionKeyword"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_OverrideKeyword:
		col:OVERRIDE
{
//	## = #([OverrideKeyword ,"OverrideKeyword "], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_AttribQualifier:
		(
			col:pr_LParen!
			pr_DefOrFieldRefList
			endcol:pr_RParen!
		)
{
	## = #([AttribQualifier,"AttribQualifier"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_DefOrFieldRefList:
		(
			col:pr_DefOrFieldRef
			(
				pr_Comma!
				endcol:pr_DefOrFieldRef
			)*
		)
{
	if(## != null) ##.setLocation(#col, #endcol == null?#col:#endcol);
};

pr_DefOrFieldRef{ LocationAST col=null; LocationAST endcol=null;}:
	(
	   (pr_FieldReference)=>
	   		(	a:pr_FieldReference { col = #a ;}
			  	( 	endcol:pr_ExtendedFieldReference
		  		)?
	   		)
	   |
	   (	d:pr_SquareOpen! {col = #d;}
	   		(	(pr_Dash!) => pr_NotUsedSymbol
	   		  |	pr_SingleExpression
	   		)
	   		e:pr_SquareClose! {endcol = #e;}
	   )
			   |
		  b:pr_DefinitionRef {col = #b ;}
			   |
		  c:pr_AllRef  {col= #c;}
	)
{
	## = #([DefOrFieldRef,"DefOrFieldRef"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_DefinitionRef://526
		col:pr_Identifier

{
	   	if(## != null) ##.setLocation(#col, #col);
};

pr_AllRef{LocationAST col=null;LocationAST endcol=null;}://527
	(
		(
			a:pr_GroupKeyword{col = #a;} enda:pr_AllKeyword!{endcol = #enda;}
			( pr_ExceptKeyword! pr_BeginChar! pr_GroupRefList endenda:pr_EndChar!{endcol = #endenda;} )?
		)
		|
		(
			b:pr_TypeDefKeyword{col = #b;} endb:pr_AllKeyword!{endcol = #endb;}
			( pr_ExceptKeyword! pr_BeginChar! pr_TypeRefList endendb:pr_EndChar!{endcol = #endendb;} )?
		)
		|
		(
			c:pr_TemplateKeyword{col = #c;} endc:pr_AllKeyword!{endcol = #endc;}
			( pr_ExceptKeyword! pr_BeginChar! pr_TemplateRefList endendc:pr_EndChar!{endcol = #endendc;} )?
		)
		|
		(
			d:pr_ConstKeyword{col = #d;} endd:pr_AllKeyword!{endcol = #endd;}
			( pr_ExceptKeyword! pr_BeginChar! pr_ConstRefList endendd:pr_EndChar!{endcol = #endendd;} )?
		)
		|
		(
			e:pr_AltstepKeyword{col = #e;} ende:pr_AllKeyword!{endcol = #ende;}
			( pr_ExceptKeyword! pr_BeginChar! pr_AltstepRefList endende:pr_EndChar!{endcol = #endende;} )?
		)
		|
		(
			f:pr_TestcaseKeyword{col = #f;} endf:pr_AllKeyword!{endcol = #endf;}
			( pr_ExceptKeyword! pr_BeginChar! pr_TestcaseRefList endendf:pr_EndChar!{endcol = #endendf;} )?
		)
		|
		(
			g:pr_FunctionKeyword{col = #g;} endg:pr_AllKeyword!{endcol = #endg;}
			( pr_ExceptKeyword! pr_BeginChar! pr_FunctionRefList endendg:pr_EndChar!{endcol = #endendg;} )?
		)
		|
		(
			h:pr_SignatureKeyword{col = #h;} endh:pr_AllKeyword!{endcol = #endh;}
			( pr_ExceptKeyword! pr_BeginChar! pr_SignatureRefList endendh:pr_EndChar!{endcol = #endendh;} )?
		)
		|
		(
			i:pr_ModuleParKeyword{col = #i;} endi:pr_AllKeyword!{endcol = #endi;}
			( pr_ExceptKeyword! pr_BeginChar! pr_ModuleParRefList endendi:pr_EndChar!{endcol = #endendi;} )?
		)
	)
{
	## = #([AllRef,"AllRef"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_AttribSpec:
	 (   col:pr_FreeText)
{
	## = #([AttribSpec, "AttribSpec"], ##);
	   	if(## != null) ##.setLocation(#col, #col);

};

//------------------------------------------------------
//                Behaviour Statements   1.6.7
//-----------------------------------------------------

pr_BehaviourStatements{ LocationAST col=null;}://529
	(	a:pr_TestcaseInstance 
		{ col = #a; }
	  | (	pr_FunctionInstance
	  	)=> b:pr_FunctionInstance 
	  		{ col = #b; }
	  |	c:pr_ReturnStatement 
	  	{ col = #c; }
	  |	d:pr_AltConstruct 
	  	{ col = #d; }
	  |	e:pr_InterleavedConstruct 
	  	{ col = #e; }
	  |	g:pr_LabelStatement 
	  	{ col = #g; }
	  |	h:pr_GotoStatement 
	  	{ col = #h; }
	  | i:pr_AltstepInstance 
	  	{ col = #i; }
	  |	j:pr_DeactivateStatement 
	  	{ col = #j; }
	  |	k:pr_ActivateOp  
	  	{ col = #k; }
	  |	l:pr_RepeatStatement 
	  	{ col = #l; }
	  |	m:pr_BreakStatement 
	  	{ col = #m; }
	  |	n:pr_ContinueStatement 
	  	{ col = #n; }

	)
{
	## = #([BehaviourStatements,"BehaviourStatements"], ##);
   	if(## != null) ##.setLocation(#col, #col);
};



pr_VerdictStatements:
		col:pr_SetLocalVerdict
{
   // ## = #([VerdictStatements,"VerdictStatements"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_VerdictOps:
		col:pr_GetLocalVerdict
{
	//## = #([VerdictOps,"VerdictOps"], ##);
		if(## != null) ##.setLocation(#col, #col);
};

pr_SetLocalVerdict://532
		(
			col:pr_SetVerdictKeyword!
			pr_LParen!
			pr_SingleExpression
			(	pr_Comma! //log item support
				pr_LogItem
			)*
			endcol:pr_RParen!
		)
{
	## = #([SetLocalVerdict,"SetLocalVerdict"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_SetVerdictKeyword:
		col:SETVERDICT
{	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_GetLocalVerdict:
		col:GETVERDICT
{	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_SUTStatements:
		(
			col:pr_ActionKeyword!
			pr_LParen!
// -------------------------------------------------------------------------------------------
// note: this is an implementable equivalent as the original rules
//
//    535. SUTStatements ::= ActionKeyword "(" [ActionText] {StringOp ActionText} ")"
//    537. ActionText ::= FreeText | Expression
//
// lead to an indeterminism on  "&" CSTRING  between  "&" ActionText  and  "&" MulExpression;
// ActionText  is equal to  Expression  because  FreeText ::= CSTRING  and  Expression ==> CSTRING;
// Expression {StringOp Expression}  is equal to  Expression  because  Expression ==> AddExpression  and  Expression ==> MulExpression;
// finally  [ActionText] {StringOp ActionText}  is equal to  [StringOp] [Expression].
// -------------------------------------------------------------------------------------------
//	        (pr_ActionText)?
//			(pr_StringOp! pr_ActionText)*
			(pr_StringOp)?  
			(pr_Expression)?	//TODO: this may need revision as per v4.2.1

			endcol:pr_RParen!
		)
{
	## = #([SUTStatements,"SUTStatements"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ActionKeyword:
		col:ACTION
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

// not used, see pr_SUTStatements
pr_ActionText { LocationAST col=null;}:
((pr_FreeText)=> a:pr_FreeText {col = #a;}
  |b:pr_Expression {col=#b;}
  )
{
 if(## != null) ##.setLocation(#col,#col);
 }
;

pr_ReturnStatement { LocationAST endcol=null;}:
		(
			col:pr_ReturnKeyword!
// -------------------------------------------------------------------------------------------
// note: this is probably the more correct variant as the original rule
//
//    538. ReturnStatement ::= ReturnKeyword [Expression]
//
// doesn't match template references and match operators. compare specification v.3.1.1 p. 86.
// -------------------------------------------------------------------------------------------
//			( (pr_Expression) => endcol: pr_Expression )?
			(
			  	//TODO: this shall be in as per v4.2.1, it is however syntactically ambiguous
			  	//TODO: check if it causes any issues due to the changed ordering 
			  	(pr_InLineTemplate) => d:pr_InLineTemplate {endcol=#d;} |
				(pr_Expression) => b:pr_Expression {endcol=#b;} |
				// XXX: shorten predicates?
				//TODO: why are these still available here ?
				(pr_TemplateRefWithParList) => a:pr_TemplateRefWithParList {endcol=#a;} |
				(pr_MatchingSymbol) => c:pr_MatchingSymbol {endcol=#c;}
			)?
		)
{
	## = #([ReturnStatement,"ReturnStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol == null?#col:#endcol);
};

pr_AltConstruct:
		(
			col:pr_AltKeyword!
			pr_BeginChar!
			pr_AltGuardList
			endcol:pr_EndChar!
		)
{
	## = #([AltConstruct,"AltConstruct"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_AltKeyword://540
		col:ALT
{
	## = #([AltKeyword,"alt"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_AltGuardList{ LocationAST col1=null,col2=null;}:
	(
		(
			(
			 a:pr_GuardStatement {if (col1==null) col1=#a; col2=#a;} | 
			 b:pr_ElseStatement {if (col1==null) col1=#b; col2=#a;}
			)
			(endcol:pr_SemiColon {col2=#endcol;})?
		)*
	)
{
	if ((#col1 != null) && (#col2 != null)) {
		## = #([AltGuardList,"AltGuardList"], ##);
		if(## != null) ##.setLocation(#col1, #col2);
	}
};

pr_GuardStatement  {LocationAST endcol=null; }:
	(
		col:pr_AltGuardChar
		(
			(pr_AltstepInstance) =>
			(
				a: pr_AltstepInstance  { endcol= #a ;}
				( c:pr_StatementBlock { endcol= #c ;} )?
			)
			|
			(
				pr_GuardOp b:pr_StatementBlock { endcol= #b ;}
			)
		)
	)
{
	## = #([GuardStatement,"GuardStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_StatementBlock:
	(	col:pr_BeginChar!
		
		(	{!isStatementBlockCompatibilityMode()}? //has to be negated to work properly, don't ask why!
			(
				(	pr_FunctionLocalDefOrInst
				)+
	//		  	(	pr_FunctionStatementList
	//			)?
				(	pr_FunctionStatement
					(	pr_SemiColon
					)?
				)+
			)
		  | (
				(	pr_FunctionLocalDefOrInst
				)
			  |	(	pr_FunctionStatement
					(	pr_SemiColon
					)?
				)
			)+
	
	//		(	(	pr_FunctionStatementOrDef 
	//				( 	pr_SemiColon 
	//				)?
	//			) => pr_FunctionStatementOrDefList
	//		)?
		)?
		endcol:pr_EndChar!
	)
{
	## = #([StatementBlock,"StatementBlock"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ElseStatement:
		(
			col:pr_SquareOpen!
			pr_ElseKeyword!
			pr_SquareClose!
			endcol:pr_StatementBlock
		)
{
	## = #([ElseStatement,"ElseStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_AltGuardChar:
	(
		col:pr_SquareOpen!
		( ( pr_BooleanExpression ) => pr_BooleanExpression )?
		endcol:pr_SquareClose!
	)
{
	## = #([AltGuardChar,"AltGuardChar"], ##);
	   if(## != null) ##.setLocation(#col, #endcol);
};

pr_GuardOp{ LocationAST col=null; }:
		(
				( pr_TimeoutStatement ) =>
			a:pr_TimeoutStatement { col = #a; }
			|
				( pr_PortOrAny pr_Dot pr_ReceiveOpKeyword ) =>
			b:pr_ReceiveStatement { col = #b; }
			|
				( pr_PortOrAny pr_Dot pr_TriggerOpKeyword  ) =>
			c:pr_TriggerStatement { col = #c; }
			|
				( pr_PortOrAny pr_Dot pr_GetCallOpKeyword ) =>
			d:pr_GetCallStatement { col = #d; }
			|
				( pr_PortOrAny pr_Dot pr_CatchOpKeyword ) =>
			e:pr_CatchStatement { col = #e; }
			|
				( pr_PortOrAny pr_Dot pr_CheckOpKeyword ) =>
			f:pr_CheckStatement { col = #f; }
			|
				( pr_PortOrAny pr_Dot pr_GetReplyOpKeyword ) =>
			g:pr_GetReplyStatement { col = #g; }
			| (pr_ComponentId pr_Dot pr_KilledKeyword) => 
			i:pr_KilledStatement { col = #i;}
			|
			h:pr_DoneStatement { col = #h; }
		)
{
	## = #([GuardOp,"GuardOp"], ##);
if(## != null) ##.setLocation(#col, #col);
};

pr_InterleavedConstruct:
		(
			col:pr_InterleavedKeyword!
			pr_BeginChar!
			pr_InterleavedGuardList
			endcol:pr_EndChar!
		)
{
	## = #([InterleavedConstruct,"InterleavedConstruct"], ##);
   	if(## != null) ##.setLocation(#col, #endcol);
};

pr_InterleavedKeyword://547
		col:INTERLEAVE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_InterleavedGuardList{ LocationAST endcol=null; }:
		(
			col:pr_InterleavedGuardElement
			( a:pr_SemiColon {endcol= #a;} )?
			(
			  b:pr_InterleavedGuardElement  {endcol= #b;}
			  ( c:pr_SemiColon  {endcol= #c;} )?
			)*
		)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_InterleavedGuardElement:
		(
			col:pr_InterleavedGuard
			endcol:pr_InterleavedAction
		)
{
	## = #([InterleavedGuardElement,"InterleavedGuardElement"], ##);
if(## != null) ##.setLocation(#col, #endcol);
};

pr_InterleavedGuard:
		(
			col:pr_SquareOpen!
			pr_SquareClose!
			endcol:pr_GuardOp
		)
{
	## = #([InterleavedGuard,"InterleavedGuard"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_InterleavedAction:
		col:pr_StatementBlock
{
	## = #([InterleavedAction,"InterleavedAction"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_LabelStatement:
		(
			col:pr_LabelKeyword!
			endcol:pr_LabelIdentifier
		)
{
	## = #([LabelStatement,"LabelStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_LabelKeyword://553
		col:LABEL
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_LabelIdentifier:
		col:pr_Identifier
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_GotoStatement:
		(
			col:pr_GotoKeyword!
		 endcol: 	pr_LabelIdentifier
		)
{
	## = #([GotoStatement,"GotoStatement"], ##);
if(## != null) ##.setLocation(#col, #endcol);
};

pr_GotoKeyword:
		col:GOTO
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_RepeatStatement:
			col:REPEAT
{
  ## = #([RepeatStatement,"RepeatStatement"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);

};


pr_ActivateOp://558
		(
			col:pr_ActivateKeyword!
				pr_LParen!
				pr_AltstepInstance
	 endcol:pr_RParen!
		)
{
	## = #([ActivateOp,"ActivateOp"], ##);
  if(## != null) ##.setLocation(#col, #endcol);
};

pr_ActivateKeyword:
		col:ACTIVATE
{
   	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_DeactivateStatement:
		(
			col:pr_DeactivateKeyword!
			(
				pr_LParen!
				pr_ComponentOrDefaultReference
				endcol:pr_RParen   !
			)?
		)
{
	## = #([DeactivateStatement,"DeactivateStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};


pr_DeactivateKeyword:
	col:DEACTIVATE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

// NEW Break and Continue statements support

pr_BreakStatement:
	col:BREAK
{
    ## = #([BreakStatement,"BreakStatement"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);

};

pr_ContinueStatement:
	col:CONTINUE
{
    ## = #([ContinueStatement,"ContinueStatement"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);

};


//------------------------------------------------------
//          Basic Statements  1.6.8
//------------------------------------------------------

pr_BasicStatements{LocationAST col=null;}:
		(
			a:pr_Assignment{ col = #a; }
			|
			b:pr_LogStatement{ col = #b; }
			|
			c:pr_LoopConstruct{ col = #c; }
			|
			d:pr_ConditionalConstruct{ col = #d; }
			|
			e:pr_SelectCaseConstruct { col = #e ;}
			|
			f:pr_StatementBlock { col = #f ;}
			
		)
{
	## = #([BasicStatements,"BasicStatements"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_Expression{ LocationAST col=null; }:
		(
			a:pr_SingleExpression { col = #a; }
			|
			b:pr_CompoundExpression { col = #b; }
		)
{
	## = #([Expression,"Expression"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_CompoundExpression{LocationAST col=null;}://564
		(
				( pr_BeginChar! pr_FieldReference pr_AssignmentChar!) =>
			a:pr_FieldExpressionList{ col = #a;}
			|
			b:pr_ArrayExpression{ col = #b;}
		)
{
	## = #([CompoundExpression,"CompoundExpression"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_FieldExpressionList:
		(
			col:pr_BeginChar!
			pr_FieldExpressionSpec
			(
				pr_Comma!
				pr_FieldExpressionSpec
			)*
			endcol:pr_EndChar!
		)
{
	## = #([FieldExpressionList,"FieldExpressionList"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_FieldExpressionSpec:
		(
			col:pr_FieldReference
			pr_AssignmentChar!
			endcol:pr_NotUsedOrExpression
		)
{
	## = #([FieldExpressionSpec,"FieldExpressionSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ArrayExpression:
		(
			col:pr_BeginChar!
			( ( pr_NotUsedOrExpression ) => pr_ArrayElementExpressionList )?
			endcol:pr_EndChar!
		)
{
	## = #([ArrayExpression,"ArrayExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ArrayElementExpressionList://568
		(
			col:pr_NotUsedOrExpression
			(
				pr_Comma!
				pr_NotUsedOrExpression
			)*
		)
{
	## = #([ArrayElementExpressionList,"ArrayElementExpressionList"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_NotUsedOrExpression{ LocationAST col=null;}:
		(
				( pr_Expression ) =>
			a:pr_Expression { col = #a; }
			|
			b:pr_NotUsedSymbol { col = #b; }
		)
{
	## = #([NotUsedOrExpression,"NotUsedOrExpression"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_ConstantExpression{ LocationAST col=null;}:
		(
			a:pr_SingleConstExpression { col = #a; }
			|
			b:pr_CompoundConstExpression { col = #b; }
		)
{
	## = #([ConstantExpression,"ConstantExpression"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_SingleConstExpression:
		col:pr_SingleExpression
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_BooleanExpression:
		col:pr_SingleExpression
{
	## = #([BooleanExpression,"BooleanExpression"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_CompoundConstExpression{LocationAST col=null;}:
		(
				( pr_BeginChar! pr_FieldReference pr_AssignmentChar!) =>
			a:pr_FieldConstExpressionList{ col = #a;}
			|
			b:pr_ArrayConstExpression{ col = #b;}
		)
{
	if(## != null) ##.setLocation(#col, #col);
};

pr_FieldConstExpressionList:
		(
			col:pr_BeginChar!
			pr_FieldConstExpressionSpec
			(
				pr_Comma!
				pr_FieldConstExpressionSpec
			)*
			endcol:pr_EndChar!
		)
{
	## = #([FieldConstExpressionList,"FieldConstExpressionList"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_FieldConstExpressionSpec:
		(
			col:pr_FieldReference
			pr_AssignmentChar!
			endcol:pr_ConstantExpression
		)
{
	## = #([FieldConstExpressionSpec,"FieldConstExpressionSpec"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ArrayConstExpression:
		(
			col:pr_BeginChar!
			( ( pr_ConstantExpression ) => pr_ArrayElementConstExpressionList )?
			endcol:pr_EndChar!
		)
{
 	## = #([ArrayConstExpression,"ArrayConstExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_ArrayElementConstExpressionList:
		(
			col:pr_ConstantExpression
			(
				pr_Comma!
				pr_ConstantExpression
			)*
		)
{
	## = #([ArrayElementConstExpressionList,"ArrayElementConstExpressionList"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_Assignment{ LocationAST endcol=null;}://578
		(options {greedy=true;}:
			col:pr_VariableRef
			pr_AssignmentChar!
			(	//ugly hack, now also for ifpresent and length, otherwise it goes down in a completely wrong direction
				{!checkMatchingSymbolToken(new int[]{TTCN3ParserTokenTypes.STAR,TTCN3ParserTokenTypes.HSTRINGMATCH,TTCN3ParserTokenTypes.OSTRINGMATCH,TTCN3ParserTokenTypes.BSTRINGMATCH,TTCN3ParserTokenTypes.QUESTIONMARK,TTCN3ParserTokenTypes.PATTERN,TTCN3ParserTokenTypes.COMPLEMENT,TTCN3ParserTokenTypes.SUBSET,TTCN3ParserTokenTypes.SUPERSET,TTCN3ParserTokenTypes.IFPRESENT,TTCN3ParserTokenTypes.LENGTH},LT(1))}?(pr_Expression)=>
				a:pr_Expression {endcol= #a ;}
			  |
			 	b:pr_TemplateBody{endcol= #b ;})// XXX: shorten predicate?
			 	//(pr_TemplateBody)=>
			 	
		)
{
	## = #([Assignment,"Assignment"], ##);
	if(## != null) ##.setLocation(#col, endcol);
};

/*
  there are two mutually exlusive solutions below:
  - strictly v3.1.1 expressions,
  - efficient expressions.
*/

// ---------------------------------------------------------------------------------------

// strictly v3.1.1 expressions - begin

/*
pr_SingleExpression{ LocationAST col=null,endcol=null;}:
			(
				col:pr_XorExpression
				(
					pr_Or!
					endcol:pr_XorExpression
				)*
			)
{
	## = #([SingleExpression,"SingleExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_XorExpression{ LocationAST col=null,endcol=null;}:
			(
				col:pr_AndExpression
				(
			  		pr_Xor!
			  		endcol:pr_AndExpression
			 	)*
			)
{
	## = #([XorExpression,"XorExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_AndExpression{ LocationAST col=null,endcol=null;}:
		 (
			 col:pr_NotExpression
			 (
			 	pr_And!
			  	endcol:pr_NotExpression
			 )*
		  )
{
	## = #([AndExpression,"AndExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_NotExpression{ LocationAST col=null,endcol=null;}:
			(
				( col:pr_Not )? endcol:pr_EqualExpression
			)
{
	## = #([NotExpression,"NotExpression"], ##);
	if(## != null) ##.setLocation(#col==null?#endcol:#col, #endcol);
};

pr_EqualExpression{ LocationAST col=null,endcol=null;}:
			(
				col:pr_RelExpression
				(
					pr_EqualOp!
					endcol:pr_RelExpression
				)*
			)
{
	## = #([EqualExpression,"EqualExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_RelExpression{ LocationAST col=null,endcol=null;}:
				(
				   col:pr_ShiftExpression
				   (
					 pr_RelOp
					 endcol:pr_ShiftExpression
					)?
				)
{
	## = #([RelExpression,"RelExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_ShiftExpression{ LocationAST col=null,endcol=null;}:
				(
				   col:pr_BitOrExpression
				   (
					 pr_ShiftOp
					 endcol:pr_BitOrExpression
				   )*
				)
{
	## = #([ShiftExpression,"ShiftExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_BitOrExpression{ LocationAST col=null,endcol=null;}:
 				(
 				    col:pr_BitXorExpression
 				    (
 				      pr_Or4b!
 				      endcol:pr_BitXorExpression
 				      )*
 				 )
{
	## = #([BitOrExpression,"BitOrExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_BitXorExpression{ LocationAST col=null,endcol=null;}:
 					(
 					   col:pr_BitAndExpression
 					   (
 					     pr_Xor4b!
 					     endcol:pr_BitAndExpression
 					    )*
 					)
{
	## = #([BitXorExpression,"BitXorExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_BitAndExpression{ LocationAST col=null,endcol=null;}:
				(
				   col:pr_BitNotExpression
				   (
				   pr_And4b!
				   endcol:pr_BitNotExpression
				   )*
				)
{
	## = #([BitAndExpression,"BitAndExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_BitNotExpression{ LocationAST col=null,endcol=null;}:
	  (
	   ( col:pr_Not4b )? endcol:pr_AddExpression
	  )
{
	## = #([BitNotExpression,"BitNotExpression"], ##);
	if(## != null) ##.setLocation(#col==null?#endcol:#col, #endcol);
};

pr_AddExpression{ LocationAST col=null,endcol=null;}:
				(
				  col:pr_MulExpression
				  (
				  pr_AddOp
				  endcol:pr_MulExpression
				  )*
				)
{
	## = #([AddExpression,"AddExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_MulExpression{ LocationAST col=null,endcol=null;}:
	  (
		 col:pr_UnaryExpression
		 (
		 pr_MultiplyOp
		 endcol:pr_UnaryExpression
		 )*
	  )
{
	## = #([MulExpression,"MulExpression"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_UnaryExpression{ LocationAST col=null,endcol=null;}:
  		(
  		  ( col:pr_UnaryOp )?
  		   endcol:pr_Primary
  		)
{
	## = #([UnaryExpression,"UnaryExpression"], ##);
	if(## != null) ##.setLocation(#col==null?#endcol:#col, #endcol);
};
*/

// strictly v3.1.1 expressions - end

// ---------------------------------------------------------------------------------------

// efficient expressions - begin

pr_SingleExpression{ LocationAST col=null;}:
	(
		col:pr_SimpleExpressions

	)
{
	## = #([SingleExpression,"SingleExpression"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_SimpleExpressions{ LocationAST col=null,endcol=null;}:
	(
		col:pr_SimpleExpression
		(
			(AND4B^ | XOR4B^ | OR4B^ | AND^ | OR^ | XOR^ | EQUAL^)
			endcol:pr_SimpleExpressions
		)?
	)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_SimpleExpression{ LocationAST col=null,endcol=null;}:
	(
		col:pr_SubExpression
		(
			(LESSTHAN^ | MORETHAN^ | NOTEQUALS^ | MOREOREQUAL^ | LESSOREQUAL^)
			endcol:pr_SimpleExpression
		)? //| pr_CompoundExpession not added due to numerous conflicts, pending...
	)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_SubExpression{ LocationAST col=null,endcol=null;}:
	(
		col:pr_Product
		(
			(SHIFTLEFT^|SHIFTRIGHT^|ROTATELEFT^|ROTATERIGHT^)
			endcol:pr_SubExpression
		)?
	)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_Product{ LocationAST col=null,endcol=null;}:
	(
		col:pr_Term
		(
			options { greedy = true; } :
			(PLUS^|MINUS^|STRINGOP^)
			endcol:pr_Term
		)*
	)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_Term{ LocationAST col=null;}:
	(
		col:pr_Factor
		(
			//options { greedy = true; } :
			(STAR^|SLASH^|MOD^|REM^)
			endcol:pr_Factor
		)* //| pr_CompoundExpession not added due to numerous conflicts, pending...
	)
{
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_Factor{ LocationAST col=null; }:
	(
		(PLUS^ | MINUS^ | NOT^ | NOT4B^)? col:pr_Primary
	)
{
	## = #([Factor,"Factor"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

// efficient expressions - end

// ---------------------------------------------------------------------------------------

pr_Primary{ LocationAST col=null; }://593
	(
		// XXX: shorten predicate?
		( pr_OpCall ) => 
		a:pr_OpCall { col = #a; }
	  | b:pr_Value { col = #b; }
	  | c:pr_LParen! pr_SingleExpression pr_RParen! { col = #c; }
	)
{
	## = #([Primary,"Primary"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_ExtendedFieldReference{ LocationAST col=null; LocationAST endcol=null;}:
	(
		options { greedy = true; }:
// -------------------------------------------------------------------------------------------
// note: this is probably the more correct variant as the original rule
//
//    594. ExtendedFieldReference ::= {(Dot ( StructFieldIdentifier | TypeDefIdentifier)) | ArrayOrBitRef }+
//
// doesn't match PredefinedType. compare specification v.3.1.1 sec. 6.4 "The anytype"
// -------------------------------------------------------------------------------------------
		(
			a:pr_Dot!  { col = #a; }
//			pr_StructFieldIdentifier
			(pr_StructFieldIdentifier | pr_PredefinedType)
		)
		| (pr_ArrayOrBitRef)=>( b:pr_ArrayOrBitRef { col = #b; } )
		| (pr_SquareOpen!)=>(		c:pr_SquareOpen!{col = #c;} 
				pr_NotUsedSymbol 
				d:pr_SquareClose!{endcol = #d;}
		  )
	)+
{
	## = #([ExtendedFieldReference,"ExtendedFieldReference"], ##);
	if(## != null) ##.setLocation(#col, endcol==null?#col:#endcol);
};

pr_OpCall{ LocationAST col=null; LocationAST endcol=null;}:
	(
	  ( pr_TimerOps ) => c:pr_TimerOps { col = #c; }
	  | b:pr_VerdictOps { col = #b; }
	  | e:pr_TestcaseInstance { col = #e; } 
	  | (	f:pr_TemplateOps { col = #f; }
	  		(	i:pr_ExtendedFieldReference {endcol = #i;}
	  		)?
	  	)
	  | g:pr_ActivateOp {col = #g ;}
	  | {checkTokenBeforeScopeChange(new int[]{TTCN3ParserTokenTypes.CREATE, TTCN3ParserTokenTypes.SELF, TTCN3ParserTokenTypes.SYSTEM, TTCN3ParserTokenTypes.MTC, TTCN3ParserTokenTypes.RUNNING, TTCN3ParserTokenTypes.ALIVE },LT(1))}? (pr_ConfigurationOps) => d:pr_ConfigurationOps { col = #d; } 
	  | (	a:pr_FunctionInstance { col = #a; }
	  		(	h:pr_ExtendedFieldReference {endcol = #h;}
	  		)?
	  
	  	)
	   
	)
{
	## = #([OpCall,"OpCall"], ##);
	if(## != null) ##.setLocation(#col, #endcol==null?#col:#endcol);
};

pr_AddOp{ LocationAST col=null;}:
	(
		a:pr_Plus { col = #a; }
	  | b:pr_Minus { col = #b; }
	  | c:pr_StringOp { col = #c; }
	)
{
	## = #([AddOp,"AddOp"], ##);
	if(## != null) ##.setLocation(col, col);
};

pr_MultiplyOp{ LocationAST col=null;}:
	(
		a:pr_Star { col = #a; }
	  | b:pr_Slash { col = #b; }
	  | c:pr_Mod { col = #c; }
	  | d:pr_Rem { col = #d; }
	)
{
	## = #([MultiplyOp,"MultiplyOp"], ##);
	if(## != null) ##.setLocation(col, col);
};

pr_UnaryOp{ LocationAST col=null;}:
	(
		a:pr_Plus { col = #a; }
	  | b:pr_Minus { col = #b; }
	)
{
	## = #([UnaryOp,"UnaryOp"], ##);
	if(## != null) ##.setLocation(col, col);
};

pr_RelOp{ LocationAST col=null;}:
	(
		b:pr_Lessthan{ col = #b; }
	  | c:pr_Morethan{ col = #c; }
	  | d:pr_Notequals{ col = #d; }
	  | e:pr_Moreorequal{ col = #e; }
	  | f:pr_Lessorequal{ col = #f; }
	)
{
	## = #([RelOp,"RelOp"], ##);
	if(## != null) ##.setLocation(col, col);
};

pr_EqualOp:
	col:EQUAL!
{
	## = #([Equal,"Equal"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_ShiftOp{LocationAST col=null;}://578
	(
		a:SHIFTLEFT{ col = #a; }
	  | b:SHIFTRIGHT{ col = #b; }
	  | c:ROTATELEFT{ col = #c; }
	  | d:ROTATERIGHT{ col = #d; }
	)
{
	## = #([ShiftOp,"ShiftOp"], ##);
	if(## != null) ##.setLocation(col, col);
};


pr_StringOp: // not used within the expression parser
	col:STRINGOP!
{
	## = #([StringOp,"StringOp"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


// ------------------------------------------------------------------------------------------

pr_Not:
	col:NOT
{
	## = #([Not,"Not"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_Not4b:
	col:NOT4B
{
	## = #([Not4b,"Not4b"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_Lessthan:
	col:LESSTHAN!
{
	## = #([Lessthan,"Lessthan"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_Morethan:
	col:MORETHAN!
{
	## = #([Morethan,"Morethan"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_Notequals:
	col:NOTEQUALS!
{
	## = #([Notequals,"Notequals"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_Moreorequal:
	col:MOREOREQUAL!
{
	## = #([Moreorequal,"Moreorequal"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_Lessorequal:
	col:LESSOREQUAL!
{
	## = #([Lessorequal,"Lessorequal"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_And4b:
	col:AND4B!
{
	## = #([And4b,"And4b"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_Xor4b:
	col:XOR4B!
{
	## = #([Xor4b,"Xor4b"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_Or4b:
	col:OR4B!
{
	## = #([Or4b,"Or4b"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_And:
	col:AND!
{
	## = #([And,"And"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_Xor:
	col:XOR!
{
	## = #([Xor,"Xor"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

// ==========================================================================================

pr_LogStatement:
		(
			col:pr_LogKeyword!
			pr_LParen!
			( pr_LogItem  (pr_Comma! pr_LogItem)* )
			endcol:pr_RParen!
		)
{
   ## = #([LogStatement,"LogStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_LogKeyword:
		col:LOG
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_LogItem{LocationAST col=null;}:

 ((pr_FreeText)=>a:pr_FreeText {col = #a;}| b:pr_TemplateInstance{col=#b ;})

{
	## = #([LogItem,"LogItem"], ##);
	if(## != null) ##.setLocation(#col, #col);
};


pr_LoopConstruct{LocationAST col=null;}:
		(
			a:pr_ForStatement{ col = #a;}
			|
			b:pr_WhileStatement{ col = #b;}
			|
			c:pr_DoWhileStatement{ col =#c;}
		)
{
	## = #([LoopConstruct,"LoopConstruct"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_ForStatement:
		(
			col:pr_ForKeyword!
			pr_LParen!
			pr_Initial
			pr_SemiColon!
			pr_Final
			pr_SemiColon!
			pr_Step
			pr_RParen!
			endcol:pr_StatementBlock
		)
{
	## = #([ForStatement,"ForStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ForKeyword:
		col:FOR
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_Initial{LocationAST col=null;}:
		(
			a:pr_VarInstance {col= #a;}
		|
			b:pr_Assignment {col= #b;}
		)
{
   // ## = #([Initial,"Initial"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_Final://610
		col:pr_BooleanExpression
{
	if( ## != null) ##.setLocation(#col, #col);
};

pr_Step:
		(
			col:pr_Assignment
		)
{
   // ## = #([Step,"Step"], ##);
	if(## != null) ##.setLocation(#col, #col);
};

pr_WhileStatement:
		(
			col:pr_WhileKeyword!
			pr_LParen!
			pr_BooleanExpression
			pr_RParen!
			endcol:pr_StatementBlock
		)
{
	## = #([WhileStatement,"WhileStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_WhileKeyword:
		col:WHILE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_DoWhileStatement:
		(
			col:pr_DoKeyword!
			pr_StatementBlock
			pr_WhileKeyword!
			pr_LParen!
			pr_BooleanExpression
			endcol:pr_RParen!
		)
{
	## = #([DoWhileStatement,"DoWhileStatement"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_DoKeyword:
		col:DO
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ConditionalConstruct{LocationAST endcol = null;}:
	(
		 	col:pr_IfKeyword!
			pr_LParen!
			pr_BooleanExpression
			pr_RParen!
			a:pr_StatementBlock
		( options { greedy = true; } :
		   b:pr_ElseIfClause )*
		// FIXME: may be too short predicate...
		( (pr_ElseKeyword ) => c:pr_ElseClause )?
	)
{
	## = #([ConditionalConstruct,"ConditionalConstruct"], ##);

	if ((#b == null)&&(#c == null)){
		if(## != null) ##.setLocation(#col, #a);
		  }
	 if ((#b == null)&&(#c != null)){
		if(## != null) ##.setLocation(#col, #c);
		  }
	  if ((#b != null)&&(#c == null)){
		if(## != null) ##.setLocation(#col, #b);
		  }
	   if ((#b != null)&&(#c != null)){
		if(## != null) ##.setLocation(#col, #c);
		  }
};


pr_IfKeyword://592
		col:IF
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};


pr_ElseIfClause:
		(
			col:pr_ElseKeyword!
			pr_IfKeyword!
			pr_LParen! pr_BooleanExpression pr_RParen!
			endcol:pr_StatementBlock
		)
{
	## = #([ElseIfClause,"ElseIfClause"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};


pr_ElseKeyword:
	col:ELSE
{
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_ElseClause:
	(
		col:pr_ElseKeyword!
		endcol:pr_StatementBlock
	)
{
	## = #([ElseClause,"ElseClause"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_SelectCaseConstruct :
	(
		col:pr_SelectKeyword!
		pr_LParen!
		pr_SingleExpression
		pr_RParen!
		endcol:pr_SelectCaseBody
	)
{
   ## = #([SelectCaseConstruct,"SelectCaseConstruct"], ##);
	if(## != null) ##.setLocation(#col, #endcol);
};

pr_SelectKeyword :
	col:SELECT
{
	## = #([SelectKeyword,"select"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_SelectCaseBody :
	(
		col:pr_BeginChar!
		( pr_SelectCase )+
		endcol:pr_EndChar!
	)
{
	## = #([SelectCaseBody,"SelectCaseBody"], ##);
	if(## != null) ##.setLocation(#col, #col);
}
;

pr_SelectCase :
	(
		col:pr_CaseKeyword!
		(
			(pr_LParen! pr_TemplateInstance  (pr_Comma! pr_TemplateInstance )* pr_RParen!)
			| pr_ElseKeyword
		)
		endcol:pr_StatementBlock
	)
{
	## = #([SelectCase,"SelectCase"], ##);
	if(## != null) ##.setLocation(#col, #endcol);

};


pr_CaseKeyword :
	col:CASE
{
	## = #([CaseKeyword,"case"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
};

pr_Identifier:
	col:IDENTIFIER
{
	## = #([Identifier,"Identifier"], ##);
	identifierCache.put(#col.getOffset(), #col.getEndOffset(), #col);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_FreeText:
	col:CSTRING
;

//------------------------------------------------------
// Miscellaneous productions  1.6.9
//------------------------------------------------------

pr_Dot:
		col:DOT
	{
	## = #([Dot,"Dot"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_Dash:
		col:MINUS!
{
	## = #([Dash,"Dash"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_Minus:
		col:MINUS!
{
	## = #([Minus,"Minus"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_SemiColon:
		col:SEMICOLON!
{
	## = #([SemiColon,"SemiColon"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_Colon:
		col:COLON!
{
	## = #([Colon,"Colon"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_Comma:
		col:COMMA!
{
	## = #([Comma,"Comma"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_Underscore:
		col:UNDERSCORE!
{
	## = #([Underscore,"Underscore"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_BeginChar:
		col:BEGINCHAR
{
	## = #([BeginChar,"BeginChar"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_EndChar:
		col:ENDCHAR
{
	## = #([EndChar,"EndChar"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_AssignmentChar://632
		col:ASSIGNMENTCHAR
{
   // ## = #([AssignmentChar,"AssignmentChar"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


// ------------------------------------------------------------------------------------------

pr_LParen:
		col:LPAREN!
{
	## = #([LParen,"LParen"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_RParen:
		col:RPAREN!
{
	## = #([RParen,"RParen"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};



pr_SquareOpen:
		col:SQUAREOPEN
{
   // ## = #([SquareOpen,"SquareOpen"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_SquareClose:
		col:SQUARECLOSE
{
 //   ## = #([SquareClose,"SquareClose"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_SingleQuote:
		col:SINGLEQUOTE
{
	## = #([SingleQuote,"SingleQuote"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_RangeOp:
		col:RANGEOP
{
	## = #([RangeOp,"RangeOp"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_Star:
		col:STAR
{
	## = #([Star,"Star"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_QuestionMark:
		col:QUESTIONMARK
{
	## = #([QuestionMark,"QuestionMark"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};

pr_ExclamationMark:
		col:EXCLAMATIONMARK
{
	## = #([ExclamationMark,"ExclamationMark"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_Plus:
		col:PLUS!
{
	## = #([Plus,"Plus"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};



pr_Or:
		col:OR!
{
	## = #([Or,"Or"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_Slash:
		col:SLASH!
{
	## = #([Slash,"Slash"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_Mod:
		col:MOD!
{
	## = #([Mod,"Mod"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);
};


pr_Rem:
		col:REM!
{
	## = #([Rem,"Rem"], ##);
	if(## != null) ##.setLocation(#col, #col);
	if(## != null) ##.setEndLocation(#col);

}
;




