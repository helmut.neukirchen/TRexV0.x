header {
package de.ugoe.cs.swe.trex.core.analyzer.rfparser;
}

{
import java.util.ArrayList;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;

@SuppressWarnings("unused")
@Deprecated
/**
 * @deprecated  This tree parser is not used anymore (and might get removed in future versions): 
 * instead, the visitor pattern is now used for semantic analysis. You can find it in class
 * de.ugoe.cs.swe.trex.core.visitor.TTCN3SemanticAnalysisVisitor
 * (Note that other tree parsers (e.g. for source code formatting) are still used.
 */

}

class TTCN3SemanticAnalysisTreeParser extends TreeParser;

options
{
  importVocab = TTCN3Parser;
  defaultErrorHandler=true; 
}

{
private TTCN3Analyzer analyzer;
private	String filename = null;
private ArrayList<TTCN3SemanticException> exceptions = new ArrayList<TTCN3SemanticException>();
private ArrayList<RecognitionException> recognitionExceptions = new ArrayList<RecognitionException>();

public void setFilename(String name) {
	filename = name;
}

public String getFilename() {
	return filename;
}

public TTCN3Analyzer getAnalyzer() {
	return analyzer;
}

public void setAnalyzer(TTCN3Analyzer a) {
	analyzer = a;
}

public ArrayList<TTCN3SemanticException> getExceptions() {
	return exceptions;
}

public void reportError(RecognitionException ex) {
   	recognitionExceptions.add(ex);
}
	
public ArrayList<RecognitionException> getRecognitionExceptions() {
	return recognitionExceptions;
}	

}

//Proposed improvement by Gustaf Johansson
pr_TTCN3File :
    #(TTCN3File
        (
            EOF
            |
            (
                (pr_TTCN3Module )*
            )
        ) 
    )
;

// =============================================================================================

pr_TTCN3Module {StringBuffer s;}:
	#(TTCN3Module 
		
		s=pr_TTCN3ModuleId 
		( pr_ModuleDefinitionsPart  )? 
		( pr_ModuleControlPart )? 
		( pr_WithStatement )? 
		
		(SemiColon)?
	)
;

pr_ModuleId returns [StringBuffer s=null]:
	#(ModuleId 
		(
			s=pr_GlobalModuleId 
			( pr_LanguageSpec )?
		) 
	)
;

pr_TTCN3ModuleId returns [StringBuffer s=null]:
	#(TTCN3ModuleId s=pr_ModuleId)
;

pr_DefinitiveObjIdComponent :
	pr_NameForm
	| pr_DefinitiveNumberForm
	| pr_DefinitiveNameAndNumberForm
;

pr_Number  :
	#(Number a:NUMBER )
;
 

pr_DefinitiveNumberForm :
	pr_Number
;


pr_DefinitiveNameAndNumberForm {StringBuffer s;}:
	#(DefinitiveNameAndNumberForm s=pr_Identifier pr_DefinitiveNumberForm )
;


pr_ModulePar :
	#(ModulePar pr_Type pr_ModuleParList ) 
;


pr_ModuleParList {StringBuffer s;}:
	#(ModuleParList 
		(
			s=pr_Identifier
			( pr_ConstantExpression  )? 
		)+ 
	)
;

/************************* 1.6.1 - Module Definitions Part *********************/


pr_ModuleDefinitionsPart :
	#( ModuleDefinitionsPart 
		#(ModuleDefinitionList 
			( 
				(pr_ModuleDefinition  (SemiColon )?)
			)+ 
		) 
	)
;

pr_ModuleDefinition  :
	#(ModuleDefinition 
		(
		pr_TypeDef 
		| pr_ConstDef 
		| pr_TemplateDef 
		| pr_FunctionDef 
		| pr_SignatureDef 
		| pr_TestcaseDef 
		| pr_ModuleParDef 
		| pr_ImportDef 
		| pr_GroupDef 
		| pr_ExtFunctionDef 
		| pr_ExtConstDef 
		| pr_AltstepDef
		)
		( pr_WithStatement )? 
	) 
;

/***** 1.6.1.1 - Typedef Definitions *****/

pr_TypeDef :
	#(TypeDef
    	( pr_StructuredTypeDef  | pr_SubTypeDef )
	) 
;

pr_StructuredTypeDef 
:
	#(StructuredTypeDef
		(
			pr_RecordDef 
			| pr_UnionDef 
			| pr_SetDef 
			| pr_RecordOfDef 
			| pr_SetOfDef 
			| pr_EnumDef 
			| pr_PortDef 
			| pr_ComponentDef 
		)
	)
	
;

pr_RecordDef  :
  #( RecordDef  pr_StructDefBody )
;

pr_StructDefBody {StringBuffer s;}:
	#(n:StructDefBody 
		(
			((s=a:pr_Identifier  ( pr_StructDefFormalParList )?  | c:AddressKeyword )  	
			( nodea:pr_StructFieldDef  )*)
		)
	)
;


pr_StructDefFormalParList  :
  //#( StructDefFormalParList ( pr_StructDefFormalPar )* ) //FIXED: no explicit StructDefFormalParList node is present
  pr_StructDefFormalPar ( pr_StructDefFormalPar )* 
;


pr_StructDefFormalPar :
    pr_FormalValuePar
;

pr_StructFieldDef {StringBuffer s;}:
	#( StructFieldDef 
    	(
    		( pr_Type | pr_NestedTypeDef ) 
    		s=pr_Identifier 
    		( pr_ArrayDef )? 
    		( pr_SubTypeSpec  )? 
    		( OPTIONAL )? 
    	)
	)
;

pr_NestedTypeDef :
	
	#(NestedTypeDef
		(
			nodea:pr_NestedRecordDef  |
			nodeb:pr_NestedUnionDef  |
			nodec:pr_NestedSetDef  |
			noded:pr_NestedRecordOfDef  |
			nodee:pr_NestedSetOfDef  |
			nodef:pr_NestedEnumDef 
		)
	)
	
;                      
                   
pr_NestedRecordDef  :
 #(NestedRecordDef  ( (pr_StructFieldDef  ( pr_StructFieldDef )* )?
  ))
;
 
pr_NestedUnionDef  :
#(NestedUnionDef ( 
 pr_UnionFieldDef  ( pr_UnionFieldDef )* ))
 ;
 
pr_NestedSetDef  :
#(NestedSetDef(
 (pr_StructFieldDef ( pr_StructFieldDef )*)?)) 
 ;
 
pr_NestedRecordOfDef  :
	#(NestedRecordOfDef(  
		(pr_StringLength )?  (pr_Type  | pr_NestedTypeDef ))) 
;

pr_NestedSetOfDef  : 
#(NestedSetOfDef(  
 (pr_StringLength )?  (pr_Type | pr_NestedTypeDef )))
;

pr_NestedEnumDef  :
 #(NestedEnumDef 
	 pr_EnumerationList 
  )              
;

pr_UnionDef {StringBuffer s;}:
	#( UnionDef 
		#(n:UnionDefBody
		(  s=a:pr_Identifier  ( pr_StructDefFormalParList  )? |  b:AddressKeyword  )  
		( nodea:pr_UnionFieldDef )*  
		)
	)
;


pr_UnionFieldDef {StringBuffer s;}:
	#( UnionFieldDef 
		(pr_Type | pr_NestedTypeDef ) 
		s=pr_Identifier  
		( pr_ArrayDef )? 
		( pr_SubTypeSpec )? 
	)
;

pr_SetDef  :
  #( SetDef pr_StructDefBody )
;


pr_RecordOfDef  :
	#( RecordOfDef 
		( pr_StringLength )? 
		pr_StructOfDefBody 
	)
;


pr_StructOfDefBody {StringBuffer s;}:
	#(StructOfDefBody
		(
			(pr_Type |pr_NestedTypeDef )
			( s=pr_Identifier | AddressKeyword )
			( pr_SubTypeSpec )?
		)
	)
;


pr_SetOfDef  :
  #( SetOfDef 
     ( pr_StringLength )? pr_StructOfDefBody )
;


pr_EnumDef {StringBuffer s;}:
	#(n:EnumDef 
		(s=a:pr_Identifier | b:AddressKeyword )
		nodea:pr_EnumerationList 
	)
;

pr_EnumerationList  :
//	 
	#(EnumerationList 
		pr_Enumeration (pr_Enumeration)* 
	)
//	
;

pr_Enumeration {StringBuffer s;}:
	#(Enumeration 
		s=pr_Identifier ( (Minus)? pr_Number )? 
	)
;

pr_SubTypeDef {StringBuffer s;}:
	#( SubTypeDef
		pr_Type
		( 
			s=pr_Identifier 
			| 
			AddressKeyword 
		) 
		( pr_ArrayDef )? 
		( pr_SubTypeSpec )?
		
	)
;


pr_SubTypeSpec  :
  #(SubTypeSpec ( (pr_AllowedValues  (pr_StringLength  )? ) | pr_StringLength  ) )
;


pr_AllowedValues  :
  #( AllowedValues (( pr_ValueOrRange )+ | (pr_CharStringMatch )  ))
;

pr_CharStringMatch  :
	#(CharStringMatch (a:CSTRING | pr_ReferencedValue)+ )
;

pr_ValueOrRange :
	#( ValueOrRange (pr_RangeDef | pr_ConstantExpression))
;


pr_RangeDef  :
  #( RangeDef pr_LowerBound pr_UpperBound ) 
;


pr_StringLength  :

  #( StringLength
     pr_SingleConstExpression  ( pr_UpperBound )? )

;


pr_PortType {StringBuffer s,a;}:
  #( PortType
     ( ( pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId )? s=pr_Identifier )
;


pr_PortDef {StringBuffer s;}:
	#(PortDef 
		s=pr_Identifier  
		nodea:pr_PortDefAttribs 
	)
;


pr_PortDefAttribs :
	#(PortDefAttribs 
		(
			pr_MessageAttribs
			| pr_ProcedureAttribs
			| pr_MixedAttribs
		)
	)
;


pr_MessageAttribs:
	#(MessageAttribs 
		( pr_MessageList (SemiColon )? )* 
	)
;


pr_MessageList:
	#(MessageList
		pr_Direction  
		( AllKeyword  | ( pr_Type )* ) 
	)
;

pr_Direction :
	#(Direction
		(
			InParKeyword 
			| OutParKeyword 
			| InOutParKeyword 
		)
	)
;


pr_ProcedureAttribs  :
	#( ProcedureAttribs 
		( pr_ProcedureList (SemiColon )? )* 
	)
;

pr_ProcedureList  :
	#(ProcedureList
		pr_Direction 
		( AllKeyword  | pr_SignatureList  ) 
	) 
;

pr_SignatureList  :
	#(SignatureList pr_Signature  (pr_Signature )* )
;

pr_MixedAttribs  :
	#(MixedAttribs 
		(
			(pr_MixedList  (SemiColon )? )* 
		)
	)
;

pr_MixedList  :
	#(MixedList
		pr_Direction  
		( AllKeyword  | ( pr_ProcOrType )* ) 
	)
;

pr_ProcOrType :
	#(ProcOrType
		(pr_Type | pr_Signature)
	)
;

pr_ComponentDef {StringBuffer s;}: 
	#(n:ComponentDef
		s=id:pr_Identifier 
		( 
			pr_ComponentType 
			(
				pr_ComponentType
			)*
		)? // extends...
		( nodea:pr_ComponentElementDef  (nodeb:SemiColon )? )* 
	) 
;

pr_ComponentType {StringBuffer s,a;}:
	#(ComponentType
		( (pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId  )? 
		s=pr_Identifier 
	)
;

pr_ComponentElementDef :
	#(ComponentElementDef
		(
			pr_PortInstance
			| pr_VarInstance
			| pr_TimerInstance
			| pr_ConstDef
		)
	)
;

pr_PortInstance  :
	#(PortInstance 
		pr_PortType  
		( pr_PortElement  )* 
	)
;

pr_PortElement {StringBuffer s;}:
	#(PortElement
		s=pr_Identifier 
		( pr_ArrayDef )? 
	)
;

/***** 1.6.1.2 - Constant Definitions  *****/

pr_ConstDef : 
	#(ConstDef
		pr_Type
		( pr_SingleConstDef )*
	)
;


pr_SingleConstDef {StringBuffer s;}: 
	#(SingleConstDef
		s=pr_Identifier  
		( pr_ArrayDef  )? 
		pr_ConstantExpression  
	)
;


/***** 1.6.1.3 - Template Definitions  *****/

pr_TemplateDef:
	#(TemplateDef 
		(	pr_TemplateRestriction
		)?
		pr_BaseTemplate  
		( 	pr_DerivedDef 
		)?  
		
		nodea:pr_TemplateBody 
		
	)
;

pr_BaseTemplate {StringBuffer s;}:
	#( BaseTemplate 
		( pr_Type | pr_Signature ) 
		s=pr_Identifier
		( pr_TemplateFormalParList )? 
	)
;



pr_DerivedDef {StringBuffer s,a;}:
	#( DerivedDef 
		(a=pr_GlobalModuleId)? 
		s=pr_Identifier 
	)
;

//New Entries

//shortcuts, valid?
//pr_TemplateRestriction  :
//	a:TemplateRestriction //"a"needed?
//;

//pr_RestrictedTemplate  :
//	a:RestrictedTemplate //"a"needed?
//;

pr_TemplateRestriction  :
	#(TemplateRestriction
		(	OMIT
		  | VALUE
		  | PRESENT
		)
	)
;

pr_RestrictedTemplate  :
	#(RestrictedTemplate
		(	OMIT
		  | (	TemplateKeyword
		  		pr_TemplateRestriction
		  	)
		)
	)
;

//---



pr_TemplateFormalParList  :
	pr_TemplateFormalPar  ( pr_TemplateFormalPar  )* 
;


pr_TemplateFormalPar :
    pr_FormalValuePar | pr_FormalTemplatePar
;


pr_TemplateBody :
	#( TemplateBody (
		pr_SimpleSpec
		| pr_FieldSpecList
		| pr_ArrayValueOrAttrib) ( pr_ExtraMatchingAttributes )?
	)
;


pr_SimpleSpec :
  pr_SingleValueOrAttrib
;


pr_FieldSpecList  :
  #( FieldSpecList 
     ( pr_FieldSpec )* 
   ) 
;

pr_FieldSpec  :
  #( FieldSpec
     pr_FieldReference pr_TemplateBody )
;


pr_FieldReference  :
 #(FieldReference
 (
    (pr_StructFieldRef) => pr_StructFieldRef
  | pr_ArrayOrBitRef
  | (pr_ParRef )=>pr_ParRef
))
;


pr_StructFieldRef {StringBuffer s;}:
  pr_PredefinedType | s=pr_Identifier
;


pr_ParRef  :
  pr_SignatureParIdentifier
;


pr_SignatureParIdentifier {StringBuffer s;}:
	s=pr_Identifier
;


pr_ArrayOrBitRef  :
  #( ArrayOrBitRef pr_SingleExpression )
;


pr_SingleValueOrAttrib  :
#(SingleValueOrAttrib
(
    ( pr_TemplateRefWithParList ) => pr_TemplateRefWithParList
  | pr_MatchingSymbol
  | pr_SingleExpression
))
;


pr_ArrayValueOrAttrib  :
	#( ArrayValueOrAttrib 
		( pr_ArrayElementSpec  )* 
	)
;


pr_ArrayElementSpec  :
#(ArrayElementSpec
	(NotUsedSymbol  | pr_TemplateBody | pr_PermutationMatch)
 )
;

pr_PermutationMatch  :
	#(PermutationMatch pr_PermutationList )
;

pr_PermutationList  :
	pr_TemplateBody  (pr_TemplateBody )* 
;

pr_MatchingSymbol  :
 #(MatchingSymbol
 ( 
    pr_Complement
  | AnyValue 
  | AnyOrOmit 
  | pr_ValueOrAttribList
  | pr_Range
  | (pr_BitStringMatch)=> pr_BitStringMatch
  | pr_HexStringMatch
  | pr_OctetStringMatch
  | pr_CharStringMatch
  | (pr_SubsetMatch)=> pr_SubsetMatch
  | (pr_SupersetMatch) => pr_SupersetMatch 

))
;

pr_ValueOrAttribList  :
	#(ValueOrAttribList
		(
			pr_TemplateBody (pr_TemplateBody )*
		)
	)
;


pr_ExtraMatchingAttributes  :
	#(ExtraMatchingAttributes 
		(
			(pr_LengthMatch  (IfPresentMatch )? ) | IfPresentMatch  
		)
	)
;

pr_SubsetMatch :
   #(SubsetMatch pr_ValueList )
;

pr_PermutaionMatch  :
   pr_ValueList 
;

pr_SupersetMatch  :
   #(SupersetMatch pr_ValueList )
;

pr_BitStringMatch  :
  #(BitStringMatch (b:BSTRINGMATCH ) )
;

pr_HexStringMatch  :
  #(HexStringMatch (b:HSTRINGMATCH ) )
;

pr_OctetStringMatch  :
  #(OctetStringMatch (b:OSTRINGMATCH ) )
;

pr_Complement  :
	#(Complement 
     	(	pr_ValueOrAttribList //changed from pr_ValueList 
     	) 
    )
;

pr_ValueList  :

  #( ValueList 
     ( pr_ConstantExpression )*  )

;


pr_LengthMatch :
	pr_StringLength 
;

pr_Range  :
  #( Range pr_LowerBound  pr_UpperBound )

;

pr_LowerBound  :
	#( LowerBound (pr_SingleConstExpression  | (Minus Infinity) ))
;

pr_UpperBound  :
	#( UpperBound (pr_SingleConstExpression  | Infinity ))
;

pr_TemplateInstance :
	#(TemplateInstance pr_InLineTemplate)
;

pr_TemplateRefWithParList {StringBuffer s,a;}:
	#(TemplateRefWithParList 
		( a=pr_GlobalModuleId )? s=pr_Identifier  (pr_TemplateActualParList )?
	)
;

pr_InLineTemplate  :
#(InLineTemplate
  ( pr_Type)? 
  ( pr_DerivedRefWithParList  )? 
    pr_TemplateBody 
)
;

pr_DerivedRefWithParList:
  #( DerivedRefWithParList pr_TemplateRefWithParList )
;

pr_TemplateActualParList  :
  #( TemplateActualParList
     ( pr_TemplateActualPar )* )
;

pr_TemplateActualPar :
  #(TemplateActualPar
  	(	pr_TemplateInstance | Dash	)
  )
;


pr_TemplateOps :
	#(TemplateOps
		(pr_MatchOp | pr_ValueofOp)
	)
;


pr_MatchOp  :
  #( MatchOp 
     pr_Expression  pr_TemplateInstance )
;


pr_ValueofOp  :

  #( ValueofOp pr_TemplateInstance  )

;

/***** 1.6.1.4 - Function Definitions  *****/

pr_FunctionDef {StringBuffer s;}: 
	#(upper:FunctionDef
		s=pr_Identifier  

		(nodea:pr_FunctionFormalParList )?  
		(
			nodeb:pr_RunsOnSpec 
		)? 
		(nodec:pr_ReturnType )?  
		noded:pr_StatementBlock 
	)
;


pr_FunctionFormalParList  :
     pr_FunctionFormalPar  ( pr_FunctionFormalPar )*
;


pr_FunctionFormalPar :
    pr_FormalValuePar
  | pr_FormalTimerPar
  | pr_FormalTemplatePar
  | pr_FormalPortPar
;


pr_ReturnType  :
  	#(ReturnType  
  		(	TemplateKeyword
  		  | pr_RestrictedTemplate 
  		)? 
  		pr_Type  
  	)
;


pr_RunsOnSpec :
  #( RunsOnSpec pr_ComponentType )
;


pr_StatementBlock  :
	#( StatementBlock
		(	(	pr_FunctionLocalDefOrInst 
			)
	  	  | (	pr_FunctionStatement
				(	SemiColon	
				)?
			)
		)*
	)
;

pr_FunctionLocalDefOrInst :
	(  	(  	b:pr_FunctionLocalDef 
	  	  | c:pr_FunctionLocalInst
	  	)	 
	)
	(	SemiColon
	)?
;



//pr_FunctionStatementOrDef  :
// #(upper:FunctionStatementOrDef(
//    (
//    	 
//    	a:pr_FunctionLocalDef 
//    	
//    )
//	| 
//	(
//		 
//		b:pr_FunctionLocalInst 
//		
//	)
//	| pr_FunctionStatement
//))
//;



pr_FunctionLocalInst :

#(FunctionLocalInst(
    pr_VarInstance
  | pr_TimerInstance
))
;

pr_FunctionLocalDef :
	#(FunctionLocalDef
		(	pr_ConstDef | pr_TemplateDef 
		)
	)
;


pr_FunctionStatement  :
#(FunctionStatement(
    pr_ConfigurationStatements
  | pr_TimerStatements
  | pr_CommunicationStatements
  | pr_BasicStatements
  | pr_BehaviourStatements
  | pr_VerdictStatements
  | pr_SUTStatements
 )  )
;


pr_FunctionInstance  :
  #( FunctionInstance 
     pr_FunctionRef   ( pr_FunctionActualParList  )? )
;


pr_FunctionRef {StringBuffer s, a;}:
#(FunctionRef(
  ( ( pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId )? s=pr_Identifier 
  ))
;


pr_FunctionActualParList  :
  #( FunctionActualParList   // -=> FunctionActualParList
     ( pr_FunctionActualPar )* )
;


pr_FunctionActualPar :
	#(FunctionActualPar (
		( pr_TemplateInstance ) => pr_TemplateInstance
		| (pr_TimerRef) => pr_TimerRef
		| (pr_Port) => pr_Port
		| pr_ComponentRef
		| Dash
		)
	)
;

/*****1.6.2.5 - Signature Definitions  *****/

pr_SignatureDef {StringBuffer s;}:
	#( SignatureDef 
		s=pr_Identifier  
		( pr_SignatureFormalParList  )? 
		( pr_ReturnType  | NoBlockKeyword )? 
		( pr_ExceptionSpec )? 
	)
;

pr_SignatureFormalParList  :
	#( SignatureFormalParList
		( pr_SignatureFormalPar )* 
	)
;


pr_SignatureFormalPar :
  pr_FormalValuePar
;

pr_ExceptionSpec:
     #(ExceptionSpec 
     	pr_Type (pr_Type )* 
     ) 
;


pr_Signature {StringBuffer s, a;}:
#(Signature(
  ( ( pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId )? s=pr_Identifier 
))
;

/***** 1.6.2.6 - Testcase Definitions  *****/

pr_TestcaseDef {StringBuffer s;}: 
	#( upper:TestcaseDef 
		s=pr_Identifier 
		( nodea:pr_TestcaseFormalParList  )? 
		nodeb:pr_ConfigSpec 
		nodec:pr_StatementBlock
			 
	)
;

pr_TestcaseFormalParList  :
	pr_TestcaseFormalPar  (pr_TestcaseFormalPar )*
;

pr_TestcaseFormalPar :
    pr_FormalValuePar
  | pr_FormalTemplatePar
;

pr_ConfigSpec  :
	#(ConfigSpec
		pr_RunsOnSpec  
		( pr_SystemSpec  )? 
	)
;

pr_SystemSpec :
	#(SystemSpec 
		pr_ComponentType 
	)
;

pr_TestcaseInstance  :
	#(TestcaseInstance
		( 
			pr_TestcaseRef  
			( pr_TestcaseActualParList )? 
			( pr_TimerValue )?
		)
	)
;

pr_TestcaseRef {StringBuffer s, a;}:
  #( TestcaseRef
     ( ( pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId )? s=pr_Identifier )
;

pr_TestcaseActualParList  :
  #( TestcaseActualParList
     ( pr_TestcaseActualPar  )* )
;

pr_TestcaseActualPar :
  #(TestcaseActualPar (pr_TemplateInstance | Dash ))
;

/***** 1.6.1.7 - Altstep Definitions  *****/

pr_AltstepDef {StringBuffer s;}: 
	#( n:AltstepDef 
		(
			s=pr_Identifier 
			( nodea:pr_AltstepFormalParList )? 
			( 
				nodeb:pr_RunsOnSpec  
				
			)?
			(nodec:pr_AltstepLocalDefList)?
			
			(noded:pr_AltGuardList)?
			
		)
	)
;

pr_AltstepFormalParList :
  pr_FunctionFormalParList
;

pr_AltstepLocalDefList :
		#(upper:AltstepLocalDefList
			a:pr_AltstepLocalDef  
			(
				SemiColon  
				b:pr_AltstepLocalDef  
			)*
		)
;

pr_AltstepLocalDef :
#(AltstepLocalDef(
     pr_VarInstance 
   | pr_TimerInstance 
   | pr_ConstDef 
   | pr_TemplateDef))
;


pr_AltstepInstance  :
  #( AltstepInstance   
     pr_AltstepRef  ( pr_FunctionActualParList  )? )
;


pr_AltstepRef {StringBuffer s, a;}:
  ( ( pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId  )? s=pr_Identifier 
;

/***** 1.6.1.8 - Import Definitions  *****/

pr_ImportDef  :
	#(ImportDef
		(
			pr_ImportFromSpec 
			( 
				pr_AllWithExcepts
				| 
				pr_ImportSpec 
			)
		)
		
	)
;

pr_AllWithExcepts  :
	#(AllWithExcepts 
		( 
			( 
				pr_ExceptsDef 
				
			)? 
		)
	)
;

pr_ExceptsDef :
	#(ExceptsDef pr_ExceptSpec)
;

pr_ExceptSpec  :
	#(ExceptSpec (pr_ExceptElement  (SemiColon )? )*)
;

pr_ExceptElement :
	#(ExceptElement
		(
			pr_ExceptGroupSpec
			| pr_ExceptTypeDefSpec 
			| pr_ExceptTemplateSpec 
			| pr_ExceptConstSpec 
			| pr_ExceptTestcaseSpec 
			| pr_ExceptAltstepSpec  
			| pr_ExceptFunctionSpec 
			| pr_ExceptSignatureSpec 
			| pr_ExceptModuleParSpec 
		)
	)
;

//////////////////////////////////////////////////////////////

pr_ExceptGroupSpec  :
	#(ExceptGroupSpec 
		
		( 
			pr_ExceptGroupRefList 
			| 
			AllKeyword 
		)
	)
;

pr_ExceptGroupRefList  :
	#(ExceptGroupRefList pr_FullGroupIdentifier  (pr_FullGroupIdentifier )*)
;

pr_ExceptTypeDefSpec  :
	#(ExceptTypeDefSpec 
		
		(
			pr_TypeRefList 
			|
			AllKeyword 
		)
	)
;

pr_TypeRefList {StringBuffer a,b;}:
	#(TypeRefList a=pr_Identifier (b=pr_Identifier )*)
;

pr_ExceptTemplateSpec  :
	#(ExceptTemplateSpec 
		
		(
			pr_TemplateRefList 
			| 
			AllKeyword 
		)
	)
; 

pr_TemplateRefList {StringBuffer a,b;}:
	#(TemplateRefList a=pr_Identifier  (b=pr_Identifier )*)
;

pr_ExceptConstSpec  :
	#(ExceptConstSpec 
		
		(
			pr_ConstRefList 
			|
			AllKeyword 
		)
	)
;


pr_ExceptTestcaseSpec  :
	#(ExceptTestcaseSpec 
		
		(
			pr_TestcaseRefList 
			|
			AllKeyword 
		)
	)
;

pr_ExceptAltstepSpec  :
	#(ExceptAltstepSpec 
		
		(
			pr_AltstepRefList 
			| 
			AllKeyword 
		)
	)
;


pr_ExceptFunctionSpec  :
	#(ExceptFunctionSpec 
		
		(
			pr_FunctionRefList 
			| 
			AllKeyword 
		)
	)
;


pr_ExceptSignatureSpec  :
	#(ExceptSignatureSpec 
		
		(
			pr_SignatureRefList 
			| 
			AllKeyword  
		)
	)
;


pr_ExceptModuleParSpec  :
	#(ExceptModuleParSpec 
		
		(
			pr_ModuleParRefList 
			| 
			AllKeyword 
		)
	)
;          

//////////////////////////////////////////////////////////////////

pr_ImportSpec  :
	#(ImportSpec 
		(
			( pr_ImportElement  (SemiColon )? )*
		)
	)
;

pr_ImportElement :
	#(ImportElement 
		(
			pr_ImportGroupSpec
			| pr_ImportTypeDefSpec
			| pr_ImportTemplateSpec
			| pr_ImportConstSpec
			| pr_ImportTestcaseSpec
			| pr_ImportAltstepSpec
			| pr_ImportFunctionSpec
			| pr_ImportSignatureSpec
			| pr_ImportModuleParSpec
		)
	)
;

pr_ImportFromSpec {StringBuffer s;} :
	#(ImportFromSpec 
		s=pr_ModuleId  
		(RecursiveKeyword)?
	)
;

pr_LanguageSpec :
	#(LanguageSpec pr_FreeText)
;


pr_GlobalModuleId returns [StringBuffer s=null]:
	#(GlobalModuleId (s=pr_Identifier) (pr_ObjectIdentifierValue)? )
;

// ---

pr_ObjectIdentifierValue:
	#(OBJID 
		(
			(pr_ReferencedValue
			| pr_NumberForm
			| pr_NameAndNumberForm 
			| pr_NameForm
			)
		)+
	)
;

pr_NumberForm:
	#(NumberForm a:NUMBER)
;

pr_NameAndNumberForm:
	#(NameAndNumberForm a:IDENTIFIER pr_NumberForm)
;

pr_NameForm:
	#(NameForm a:IDENTIFIER)
;

// -----



pr_ImportModuleParSpec    :
	#(ImportModuleParSpec 
		
		(  
			pr_ModuleParRefList 
			| 
			pr_AllModuleParWithExcept 
		)
	)
;

pr_AllModuleParWithExcept  :
	#(AllModuleParWithExcept  
		( pr_ModuleParRefList )?
	)
;

pr_ModuleParRefList {StringBuffer a,b;}:
	#(ModuleParRefList 
		a=pr_Identifier 
		(b=pr_Identifier  )*
	)
;

pr_ImportGroupSpec  :
	#(ImportGroupSpec 
		( 
			pr_GroupRefListWithExcept 
			| 
			pr_AllGroupsWithExcept 
		)
	)
	
;

pr_GroupRefListWithExcept   :
	#(GroupRefListWithExcept 
		
		pr_FullGroupIdentifierWithExcept 
		(
			(pr_FullGroupIdentifierWithExcept  )*
		)
	)
;

pr_AllGroupsWithExcept  :
	#(AllGroupsWithExcept 
		( pr_GroupRefList )?
	)
;


pr_FullGroupIdentifierWithExcept   :
    #(FullGroupIdentifierWithExcept
		pr_FullGroupIdentifier
		
		( pr_ExceptsDef  )?	
	)
;

pr_GroupRefList  :
	#(GroupRefList 
		pr_FullGroupIdentifier  
		(pr_FullGroupIdentifier  )* 
	)
;

pr_FullGroupIdentifier {StringBuffer a,b;}:
   #(FullGroupIdentifier
     a=pr_Identifier  (Dot b=pr_Identifier )*
    ) 
;

pr_ImportTypeDefSpec   :
	#(ImportTypeDefSpec 
	
		(
			pr_TypeRefList 
			| 
			pr_AllTypesWithExcept 
		)
	)
;

pr_AllTypesWithExcept :
	#(AllTypesWithExcept  
		( pr_TypeRefList )?
	)
;

pr_TypeDefIdentifier {StringBuffer s;}:
	s=pr_Identifier
;

pr_ImportTemplateSpec  :
	#(ImportTemplateSpec 
		
		(
			pr_TemplateRefList 
			| 
			pr_AllTemplsWithExcept 
		) 
	)
;

pr_AllTemplsWithExcept :
	#(AllTemplsWithExcept 
		( pr_TemplateRefList )?
	)
;

pr_ImportConstSpec  :
	#(ImportConstSpec
		(
			pr_ConstRefList 
			| 
			pr_AllConstsWithExcept 
		)
	)
;

pr_AllConstsWithExcept :
	#(AllConstsWithExcept 
		( pr_ConstRefList )?
	)
;

pr_Identifier returns [StringBuffer s] {s=new StringBuffer();}:
	#(Identifier a:IDENTIFIER 
		{
			s.append(a.getText());
			LocationAST ast = (LocationAST)a;
			Scope scope = ast.getScope();
			Symbol symb = scope.resolve(ast);
			if (symb == null) {
				exceptions.add(new TTCN3SemanticException(ast.getText() + " is not declared", getFilename(), ast.getLine(), ast.getColumn(), TTCN3SemanticException.warningEnum.ERROR));
			}
		}
	)
;

pr_ConstRefList {StringBuffer a,b;}:
	#(ConstRefList 
		a=pr_Identifier 
		(b=pr_Identifier )*
	)
;

pr_ImportAltstepSpec  :
	#(ImportAltstepSpec
		
		(  
			pr_AltstepRefList 
			| 
			pr_AllAltstepsWithExcept 
		)
	)
;

pr_AltstepRefList {StringBuffer a,b;}:
	#(AltstepRefList 
		a=pr_Identifier  (b=pr_Identifier )* 
	)
;

pr_AllAltstepsWithExcept :
	#(AllAltstepsWithExcept 
		( pr_AltstepRefList )? 
	)
;

pr_ImportTestcaseSpec  :
	#( ImportTestcaseSpec
		
		(  
			pr_TestcaseRefList 
			| 
			pr_AllTestcasesWithExcept 
		)
	)
;

pr_TestcaseRefList {StringBuffer a,b;}:
	#(TestcaseRefList 
		a=pr_Identifier  (b=pr_Identifier )* 
	)
;

pr_AllTestcasesWithExcept :
	#(AllTestcasesWithExcept 
		( pr_TestcaseRefList )? 
	)
;

pr_ImportFunctionSpec  :
	#( ImportFunctionSpec
		
		(  
			pr_FunctionRefList 
			| 
			pr_AllFunctionsWithExcept 
		)
	)
;

pr_FunctionRefList {StringBuffer a,b;}:
	#(FunctionRefList 
		a=pr_Identifier  (b=pr_Identifier )*
	)
;

pr_AllFunctionsWithExcept :
	#(AllFunctionsWithExcept 
		( pr_FunctionRefList )?
	)
;

pr_ImportSignatureSpec  :
	#(ImportSignatureSpec
		
		(  
			pr_SignatureRefList 
			| 
			pr_AllSignaturesWithExcept 
		)
	)
;

pr_SignatureRefList {StringBuffer a,b;}:
	#(SignatureRefList 
		a=pr_Identifier  (b=pr_Identifier )* 
	)
;

pr_AllSignaturesWithExcept :
	#(AllSignaturesWithExcept 
		( pr_SignatureRefList )?
	)
;


/*****  1.6.1.9 - Group Definitions  *****/

pr_GroupDef {StringBuffer s;}:
	#(upper:GroupDef  
		s=pr_Identifier  
		( pr_ModuleDefinitionsPart  )?   
	)
;


/***** 1.6.1.10 - External Function Definitions  *****/

pr_ExtFunctionDef {StringBuffer s;}:
	#(ExtFunctionDef 
  		s=pr_Identifier
  		( pr_FunctionFormalParList  )? 
  		( pr_ReturnType )? 
	)
;



/***** 1.6.1.11 - External Constant Definitions  *****/

pr_ExtConstDef {StringBuffer s;}:
	#(ExtConstDef 
		pr_Type
		(	s=pr_Identifier
		)* //to support lists  
	) 
;

/***** 1.6.1.12 - Module parameter definitions *****/

pr_ModuleParDef  :
	#(ModuleParDef  
		(	(	pr_ModulePar  
			)+
		  | pr_MultitypedModuleParList 
		) 
	)
;

pr_MultitypedModuleParList  :
	#(MultitypedModuleParList 
		(	pr_ModulePar  
			(	SemiColon
			)?  
		)* //changed from + 
	)
;

/***** 1.6.2 - Control Part  *****/

pr_ModuleControlPart  : 
	#(n:ModuleControlPart 
		 
		pr_ModuleControlBody 
		
		( pr_WithStatement )? 
		(SemiColon )?
		 
		
	)
;

pr_ModuleControlBody  :
	#(ModuleControlBody ( pr_ControlStatementOrDef  (SemiColon )? )* )
;

pr_ControlStatementOrDef :
	#(upper:ControlStatementOrDef
		(
			(
				 
				a:pr_FunctionLocalInst
		    	
	    	)
			| 
			(
				 
				b:pr_FunctionLocalDef 
				
			)
			| pr_ControlStatement
		)
	)
;

pr_ControlStatement  :
	#(ControlStatement
		(
			pr_TimerStatements
			| pr_BasicStatements
			| pr_BehaviourStatements
			| pr_SUTStatements
			| STOP 
		)
	)
;


/***** 1.6.3.1 - Variable Instantiation  *****/

pr_VarInstance  :
	#(VarInstance  
		(	(	(	TemplateKeyword
				  | pr_RestrictedTemplate
				)?
				pr_Type
				(	pr_VarList 
				  | pr_TempVarList  
				) 
			) 
		) 
	) 
;

pr_VarList :
	#(VarList (pr_SingleVarInstance )+)
;

pr_TempVarList :
	#(TempVarList (pr_SingleTempVarInstance  (pr_SingleTempVarInstance )*))
;


pr_SingleTempVarInstance {StringBuffer s;}:
	#(SingleTempVarInstance 
		(
			s=pr_Identifier  
			(pr_ArrayDef )? 
			( pr_TemplateBody )? 
		)
	)
;
                 
pr_SingleVarInstance {StringBuffer s;}: 
	#(SingleVarInstance 
		s=pr_Identifier 
		( pr_ArrayDef )? 
		(pr_Expression  )? 
	)
;

pr_VariableRef {StringBuffer s;}:
	#(VariableRef (s=pr_Identifier  ( (pr_ExtendedFieldReference)=> pr_ExtendedFieldReference )?))
;


/*****    1.6.2.2 - Timer Instantiation  *****/

pr_TimerInstance  :
	#( TimerInstance ( pr_SingleTimerInstance  )+ )
;


pr_SingleTimerInstance {StringBuffer s;}: 
	#(SingleTimerInstance 
		s=pr_Identifier  
		(pr_ArrayDef)? 
		(pr_TimerValue)? 
	) 
;


pr_TimerValue :
	pr_Expression
;


pr_TimerRef {StringBuffer s;}:
	#(TimerRef (s=pr_Identifier  ( pr_ArrayOrBitRef  )*))
;

/***** 1.6.2.3 - Component Operations  *****/

pr_ConfigurationStatements  :
	#(ConfigurationStatements 
		(
    	pr_ConnectStatement
		| pr_MapStatement
		| pr_DisconnectStatement
		| pr_UnmapStatement
		| pr_DoneStatement
		| pr_KilledStatement
		| pr_StartTCStatement
		| pr_StopTCStatement
		| pr_KillTCStatement
		)
	)
;

pr_KillTCStatement  :
	#(KillTCStatement 
		(
			KillKeyword
			| pr_ComponentReferenceOrLiteral  
			| AllKeyword 
		)
	)		   
;

pr_KilledStatement  :
	#(KilledStatement pr_ComponentId )
;

pr_ConfigurationOps :
	#(ConfigurationOps 
		(
		pr_CreateOp
		| pr_SelfOp
		| pr_SystemOp
		| pr_MTCOp
		| pr_RunningOp
		| pr_AliveOp
		)
	)
;

pr_CreateOp  :
	#(CreateOp 
		pr_ComponentType  
		(pr_SingleExpression )?
		(AliveKeyword )?
	)
;

pr_SystemOp  :
	SystemOp 
;

pr_AliveOp  :
	#(AliveOp pr_ComponentId )
;

pr_SelfOp  :
	SelfOp 
;


pr_MTCOp  :
	MTCOp 
;


pr_DoneStatement  :
	#(DoneStatement pr_ComponentId )

;

pr_ComponentId  :
	#(ComponentId ( (pr_VariableRef |pr_FunctionInstance) | AnyKeyword  | AllKeyword ))
;

pr_RunningOp  :
	#(RunningOp pr_ComponentId )
;

pr_ConnectStatement  :
	#(ConnectStatement pr_SingleConnectionSpec )
;

pr_SingleConnectionSpec  :
	#(SingleConnectionSpec (pr_PortRef pr_PortRef)) 
;

pr_PortSpec  :
	#(PortSpec pr_PortRef pr_PortRef) 
;

pr_PortRef  :
	#(PortRef pr_ComponentRef pr_Port ) 
;


pr_ComponentRef :
	#(ComponentRef (pr_ComponentOrDefaultReference | pr_SystemOp | pr_SelfOp | pr_MTCOp))
;

pr_ComponentOrDefaultReference :
	pr_VariableRef | pr_FunctionInstance
;

pr_DisconnectStatement  :
	#(DisconnectStatement  (pr_SingleOrMultiConnectionSpec )?)
;

pr_SingleOrMultiConnectionSpec :
	#(SingleOrMultiConnectionSpec
		((pr_SingleConnectionSpec | pr_AllConnectionsSpec | pr_AllPortsSpec | pr_AllCompsAllPortsSpec))
	)
;

pr_AllConnectionsSpec :
	pr_PortRef
;

pr_AllPortsSpec  :
	pr_ComponentRef 
;
 
pr_AllCompsAllPortsSpec  :
	AllKeyword 
	COMPONENT 
	AllKeyword 
	PortKeyword  
;
   
pr_MapStatement  :
	#(MapStatement  pr_SingleConnectionSpec )
;

pr_UnmapStatement  :
	#(UnmapStatement (pr_SingleOrMultiConnectionSpec )? )
;


pr_StartTCStatement  :
	#(StartTCStatement pr_ComponentOrDefaultReference pr_FunctionInstance ) 
;


pr_StopTCStatement  :
	#(StopTCStatement 
		(
			(pr_ComponentReferenceOrLiteral  STOP) |
			(AllKeyword COMPONENT STOP )
		)?
	)
;

pr_ComponentReferenceOrLiteral :
	#(ComponentReferenceOrLiteral
		(
		pr_ComponentOrDefaultReference 
		| pr_MTCOp 
		| pr_SelfOp
		)
	)
;

pr_ComponentIdentifier :
	pr_FunctionInstance | pr_VariableRef
;

/*****  1.6.3.4 - Port Operations  *****/

pr_Port {StringBuffer s;}:
	#(Port (s=pr_Identifier  ( pr_ArrayOrBitRef )*))
;

pr_CommunicationStatements :
	#(CommunicationStatements
		(
		pr_SendStatement
		| pr_CallStatement
		| pr_ReplyStatement
		| pr_RaiseStatement
		| pr_ReceiveStatement
		| pr_TriggerStatement
		| pr_GetCallStatement
		| pr_GetReplyStatement
		| pr_CatchStatement
		| pr_CheckStatement
		| pr_ClearStatement
		| pr_StartStatement
		| pr_StopStatement
		| pr_HaltStatement // see parser
		)
	)
;

pr_SendStatement  :
	#(SendStatement pr_Port  pr_SendParameter  ( pr_ToClause  )? )
;

pr_SendParameter :
	pr_TemplateInstance
;


pr_ToClause  :
	#(ToClause 
		(	pr_AddressRef 
		  | pr_AddressRefList 
		  | (	AllKeyword 
		  		COMPONENT
		  	) 
		) 
	)
;

pr_AddressRefList  :
	#(AddressRefList pr_AddressRef  (pr_AddressRef )*)
;

pr_AddressRef :
	#(AddressRef (pr_TemplateInstance))
;

pr_CallStatement  :
	#( CallStatement pr_Port pr_CallParameters  ( pr_ToClause )? ( pr_PortCallBody )? )
;

pr_CallParameters  :
	#(CallParameters pr_TemplateInstance  ( pr_CallTimerValue  | NowaitKeyword )? )
;

pr_CallTimerValue  :
	#(CallTimerValue (pr_TimerValue | NowaitKeyword ))
;

pr_PortCallBody  :
	pr_CallBodyStatement  
	( 
		pr_CallBodyStatement  
		(SemiColon )?
	)* 
;


pr_CallBodyStatement  :
	#(CallBodyStatement 
		pr_CallBodyGuard  
		pr_StatementBlock 
	)
;


pr_CallBodyGuard  :
	#(CallBodyGuard pr_AltGuardChar  pr_CallBodyOps )
;


pr_CallBodyOps :
	#(CallBodyOps (pr_GetReplyStatement | pr_CatchStatement))
;


pr_ReplyStatement  :
	#(ReplyStatement 
		pr_Port  
		pr_PortReplyOp 
	)
;

pr_PortReplyOp  :
	#(PortReplyOp
		(	
			pr_TemplateInstance  
			( pr_ReplyValue  )? 
			
			( pr_ToClause )? 
		)
	)
;

pr_ReplyValue  :
	pr_Expression 
;

pr_RaiseStatement  :
	#(RaiseStatement 
		pr_Port 
		pr_PortRaiseOp 
	)
;

pr_PortRaiseOp  :
	#(PortRaiseOp
		(	
			pr_Signature 
			pr_TemplateInstance 
			( pr_ToClause  )? 
		)
	)
;

pr_ReceiveStatement  :
	#(ReceiveStatement pr_PortOrAny pr_PortReceiveOp )
;

pr_PortOrAny  :
	#(PortOrAny (pr_Port | AnyKeyword PortKeyword ))
;

pr_PortReceiveOp  :
	#(PortReceiveOp  ( pr_ReceiveParameter  )? ( pr_FromClause )? ( pr_PortRedirect )? )
;

pr_ReceiveParameter :
  pr_TemplateInstance
;

pr_FromClause  :
	#(FromClause 
		(	pr_AddressRef 
		  | pr_AddressRefList 
		  | (	AnyKeyword 
		  		COMPONENT
		  	) 
		) 
	)
;

pr_PortRedirect  :
	#(PortRedirect  ( pr_ValueSpec  ( pr_SenderSpec )? | pr_SenderSpec ) )
;

pr_ValueSpec  :
	#(ValueSpec pr_VariableRef )
;

pr_SenderSpec  :
	#(SenderSpec pr_VariableRef  )
;

pr_TriggerStatement  :
	#(TriggerStatement 
		pr_PortOrAny  
		pr_PortTriggerOp 
	)
;

pr_PortTriggerOp  :
	#(PortTriggerOp 
		( pr_ReceiveParameter )? 
		( pr_FromClause )? 
		( pr_PortRedirect )? 
	)
;

pr_GetCallStatement  :
	#(GetCallStatement 
		pr_PortOrAny  
		pr_PortGetCallOp 
	)
;

pr_PortGetCallOp  :
	#(PortGetCallOp ( pr_ReceiveParameter )? 
	( pr_FromClause )? 
	( pr_PortRedirectWithParam )? )
;


pr_PortRedirectWithParam  :
	pr_RedirectWithParamSpec 
;

pr_PortRedirectWithValueAndParam  :
	#(PortRedirectWithValueAndParam pr_RedirectWithValueAndParamSpec )
;

pr_RedirectWithValueAndParamSpec  :
	#(RedirectWithValueAndParamSpec (pr_ValueSpec  (pr_ParaSpec )? (pr_SenderSpec )? | pr_RedirectWithParamSpec ))
;

pr_RedirectWithParamSpec  :
	#( RedirectWithParamSpec ((pr_ParaSpec  (pr_SenderSpec )? | pr_SenderSpec )))
;


pr_ParaSpec  :
	#(ParaSpec pr_ParaAssignmentList )
;

pr_ParaAssignmentList  :
  #(ParaAssignmentList ( pr_AssignmentList  | pr_VariableList ) )
;

pr_AssignmentList  :
	#( AssignmentList ( pr_VariableAssignment  )* )
;

pr_VariableAssignment  :
	#( VariableAssignment pr_VariableRef  pr_ParameterIdentifier )
;

pr_ParameterIdentifier {StringBuffer s;}:
	s=pr_Identifier
;

pr_VariableList  :
	#(VariableList ( pr_VariableEntry  )* )
;

pr_VariableEntry  :
	#(VariableEntry (pr_VariableRef | NotUsedSymbol ) )
;

pr_GetReplyStatement  :
	#( GetReplyStatement pr_PortOrAny pr_PortGetReplyOp  )
;

pr_PortGetReplyOp  :
	#(PortGetReplyOp 
		( 
			pr_ReceiveParameter  
			( 
				pr_ValueMatchSpec  
			)? 
		)? 
		( pr_FromClause )? 
		( pr_PortRedirectWithValueAndParam )? 
	)
;

pr_ValueMatchSpec  :
	#(ValueMatchSpec pr_TemplateInstance )
;

pr_CheckStatement  :
	#(CheckStatement 
		pr_PortOrAny  
		pr_PortCheckOp 
	)
;

pr_PortCheckOp  :
	#(PortCheckOp 
		( pr_CheckParameter  )? 
	)
;

pr_CheckParameter  :
	#(CheckParameter
		(
			pr_CheckPortOpsPresent 
			| pr_FromClausePresent 
			| pr_RedirectPresent 
		)
	)
;

pr_RedirectPresent  :
	#(RedirectPresent
		pr_SenderSpec 
	)
;

pr_FromClausePresent  :
	#(FromClausePresent
		pr_FromClause 
		( pr_SenderSpec )?
	)
;

pr_CheckPortOpsPresent  :
	#(CheckPortOpsPresent
		(
			pr_PortReceiveOp
			| pr_PortGetCallOp
			| pr_PortGetReplyOp
			| pr_PortCatchOp
		)
	)
;

pr_CatchStatement  :
	#( CatchStatement pr_PortOrAny pr_PortCatchOp ) 
;

pr_PortCatchOp  :
	#( PortCatchOp 
		 
		( pr_CatchOpParameter  )? 
		( pr_FromClause )? 
		( pr_PortRedirect )? 
	)
;

pr_CatchOpParameter  :
	#( CatchOpParameter ( pr_Signature pr_TemplateInstance  | TimeoutKeyword  ) )
;

pr_ClearStatement  :
	#( ClearStatement pr_PortOrAll )
;


pr_PortOrAll :
	#(PortOrAll (pr_Port | pr_AllPort))
;

pr_AllPort  :
	#(AllPort ())
;

pr_StartStatement  :
	#(StartStatement pr_PortOrAll )
;

pr_StopStatement  :
	#(StopStatement pr_PortOrAll )
;

// see parser
pr_HaltStatement  :
	#(HaltStatement pr_PortOrAll )
;

/***** 1.6.2.5 - Timer Operations  *****/

pr_TimerStatements :
	#(TimerStatements (pr_StartTimerStatement | pr_StopTimerStatement | pr_TimeoutStatement))
;

pr_TimerOps :
	#(TimerOps (pr_ReadTimerOp | pr_RunningTimerOp))
;

pr_StartTimerStatement  :
	#(StartTimerStatement pr_TimerRef  ( pr_TimerValue )? )
;

pr_StopTimerStatement  :
	#(StopTimerStatement pr_TimerRefOrAll )
;

pr_TimerRefOrAll  :
	#(TimerRefOrAll (pr_TimerRef | AllKeyword TimerKeyword  ))
;

pr_ReadTimerOp  :
	#(ReadTimerOp pr_TimerRef )
;

pr_RunningTimerOp :
	#( RunningTimerOp pr_TimerRefOrAny )
;

pr_TimeoutStatement  :
	#( TimeoutStatement pr_TimerRefOrAny  )
;

pr_TimerRefOrAny  :
	#(TimerRefOrAny (pr_TimerRef  | AnyKeyword TimerKeyword ))
;

/*****  1.6.3 - Type  *****/

pr_Type  :
	#(Type 
		(
			pr_PredefinedType 
			 
			| pr_ReferencedType
			
		)
	)
;

pr_PredefinedType :
	#(PredefinedType
		(
			a:BitStringKeyword 
			| b:BooleanKeyword 
			| c:CharStringKeyword 
			| d:UniversalCharString 
			| e:IntegerKeyword 
			| f:OctetStringKeyword 
			| g:HexStringKeyword 
			| h:VerdictTypeKeyword 
			| i:FloatKeyword 
			| j:AddressKeyword 
			| k:DefaultKeyword 
			| l:AnyTypeKeyword 
			| m:OBJID
		)
	)
;


pr_ReferencedType {StringBuffer s;} :
	#(ReferencedType
		(
			( ( pr_GlobalModuleId pr_TypeReference ) => s=pr_GlobalModuleId  )?
			pr_TypeReference  
			( (pr_ExtendedFieldReference )=>pr_ExtendedFieldReference  )?
		)
	)
;

pr_TypeReference {StringBuffer s;} :
	#( TypeReference ( s=pr_Identifier ( pr_TypeActualParList  )?) )
;


pr_TypeActualParList  :
	#( TypeActualParList ( pr_TypeActualPar )* )
;


pr_TypeActualPar :
	//pr_ConstantExpression
	#(TypeActualPar pr_ConstantExpression) //FIXED: explicit TypeActualPar node is present
	
;

pr_ArrayDef  :
	#(ArrayDef (pr_ArrayBounds |pr_ArrayDefRange )+ )
;


pr_ArrayDefRange  :
	#(ArrayDefRange pr_ArrayBounds pr_ArrayBounds )
;

pr_ArrayBounds :
	pr_SingleConstExpression
;

/*****1.6.3 - Value  *****/


pr_Value :
	#(Value (pr_ReferencedValue | pr_PredefinedValue))
;

pr_PredefinedValue  :
	#(PredefinedValue
		(
		pr_BitStringValue
		| pr_BooleanValue
		| pr_CharStringValue
		| pr_IntegerValue
		| pr_OctetStringValue
		| pr_ObjectIdentifierValue
		| pr_HexStringValue
		| pr_VerdictTypeValue
		| pr_EnumeratedValue
		| pr_FloatValue
		| pr_AddressValue
		| pr_OmitValue
		)
	)
;

pr_FloatValue  :
	#(FloatValue a:FLOATVALUE) 
;

pr_BitStringValue  :
	#(BitStringValue a:BSTRING) 
;

pr_BooleanValue  :
	#(BooleanValue (a:TRUE | b:FALSE ))
;


pr_IntegerValue  :
	#(IntegerValue b:NUMBER )
;


pr_OctetStringValue  :
	#(OctetStringValue a:OSTRING )
;

pr_ObjIdComponent :
	(pr_NameForm)=>pr_NameForm
	| pr_NumberForm
	| pr_NameAndNumberForm
;

pr_HexStringValue  :
	#(HexStringValue a:HSTRING )
;


pr_VerdictTypeValue :
	#(VerdictTypeValue
		(
		pr_Pass
		| pr_Fail
		| pr_Inconc
		| pr_None
		| pr_Error
		)
	)
;

pr_Pass  :
	a:Pass 
;

pr_Fail  :
	a:Fail 
;
       
pr_Inconc  :
	a:Inconc 
;
            
pr_None  :
	a:None 
;
            
pr_Error  :
	a:Error 
;

pr_EnumeratedValue {StringBuffer s;}:
	#(EnumeratedValue s=pr_Identifier)
;

pr_CharStringValue  :
	#(CharStringValue (a:CSTRING | pr_Quadruple))
;


pr_Quadruple  :
	#(Quadruple pr_Group  pr_Plane  pr_Row pr_Cell )
;

pr_Group  :
	#(Group a:NUMBER) 
;

pr_Plane  :
	#(Plane a:NUMBER) 
;

pr_Row  :
	#(Row a:NUMBER) 
;

pr_Cell  :
	#(Cell a:NUMBER) 
;

pr_ReferencedValue  :
	#(ReferencedValue (pr_ValueReference  ( (pr_ExtendedFieldReference)=>pr_ExtendedFieldReference )?))
;

pr_ValueReference {StringBuffer s, a;}:
	#(ValueReference  (a=pr_GlobalModuleId )? s=pr_Identifier )
;

pr_FreeText  :
	a:CSTRING 
;

pr_AddressValue  :
	Null 
;

pr_OmitValue  :
	a:OmitValue 
;

/*****  SECTION A.1.6.6 - Parameterisation  *****/

pr_FormalValuePar {StringBuffer s;}:
	#(FormalValuePar 
		( InParKeyword  | InOutParKeyword  | OutParKeyword  )? 
		pr_Type 
		s=pr_Identifier
	)
;

pr_FormalTypePar {StringBuffer s;}:
	#( FormalTypePar s=pr_Identifier )
;

pr_FormalPortPar {StringBuffer a,b;}:
	#(FormalPortPar
		(InOutParKeyword)? 
		a=pr_Identifier
		b=pr_Identifier
	) 
;

pr_FormalTimerPar {StringBuffer s;}:
	#(FormalTimerPar 
		(InOutParKeyword)? 
		TimerKeyword 
		s=pr_Identifier 
	) 
;

pr_FormalTemplatePar {StringBuffer s;}:
	#(FormalTemplatePar 
		(InParKeyword |OutParKeyword|InOutParKeyword)? 
		(TemplateKeyword 
  		  | pr_RestrictedTemplate)?
		pr_Type
		s=pr_Identifier
		//TODO: does this fix an issue?
		(	pr_TemplateInstance 	
		)?
	) 
;

/***** 1.6.6 - The With Statement  *****/

pr_WithStatement 
:
	#( WithStatement  
		( pr_SingleWithAttrib 
		(SemiColon )? )* 
	)
;

pr_SingleWithAttrib:
	#( SingleWithAttrib pr_AttribKeyword ( o:OVERRIDE )? ( pr_AttribQualifier )? pr_AttribSpec )
;

pr_AttribKeyword:
	a:ENCODE  | b:DISPLAY  
	| c:EXTENSION  | d:VARIANT | e:OPTIONAL
;

pr_AttribQualifier  :
	#( AttribQualifier ( pr_DefOrFieldRef  )* )
;


pr_DefOrFieldRef  :
	#(DefOrFieldRef
		((pr_FieldReference)=>pr_FieldReference | pr_DefinitionRef | pr_AllRef)
	)
;


pr_DefinitionRef {StringBuffer s;}:
	( (pr_Identifier pr_Identifier ) => pr_FullGroupIdentifier |  s=pr_Identifier)
;

pr_AllRef :
	#(AllRef
        (
            (
                        GROUP 
                        ( pr_GroupRefList )? 
            )
            |
            (
                        TYPE 
                        ( pr_TypeRefList )? 
            )
            |
            (
                        TemplateKeyword
                        ( pr_TemplateRefList )? 
            )
            |
            (
                        CONST
                        ( pr_ConstRefList )? 
            )
            |
            (
                        AltstepKeyword
                        ( pr_AltstepRefList )? 
            )
            |
            (
                        TESTCASE
                        ( pr_TestcaseRefList )? 
            )
            |
            (
                        FUNCTION
                        ( pr_FunctionRefList )? 
            )
            |
            (
                        SIGNATURE
                        ( pr_SignatureRefList )? 
            )
            |
            (
                        MODULEPAR
                        ( pr_ModuleParRefList )? 
            )
        )
	)
;

pr_AttribSpec :
	#(AttribSpec pr_FreeText)
;

/*****  1.6.7 - Behaviour Statements  *****/

pr_BehaviourStatements :
	#(BehaviourStatements
		(
		pr_TestcaseInstance
		| pr_FunctionInstance
		| pr_ReturnStatement
		| pr_AltConstruct
		| pr_InterleavedConstruct
		| pr_LabelStatement
		| pr_GotoStatement
		| pr_ActivateOp
		| pr_DeactivateStatement
		| pr_AltstepInstance
		| pr_RepeatStatement
		| pr_BreakStatement
		| pr_ContinueStatement
		)
	)
;

pr_VerdictStatements :
	pr_SetLocalVerdict
;

pr_VerdictOps :
	pr_GetLocalVerdict
;

pr_SetLocalVerdict  :
	#(SetLocalVerdict 
		pr_SingleExpression
		(	pr_LogItem
		)*
	)
;

pr_GetLocalVerdict  :
	GETVERDICT 
;

pr_SUTStatements  :
	#(SUTStatements 
		(
	      (StringOp)?
		  (pr_Expression)?
		)
	)
;

pr_ActionText :
	pr_FreeText 
	| pr_Expression
;

pr_ReturnStatement  :
// see parser.
	#( ReturnStatement 
	   ( pr_Expression | pr_InLineTemplate | pr_TemplateRefWithParList | pr_MatchingSymbol )? 
) 
;

pr_AltConstruct  :
	#( AltConstruct 
		(pr_AltGuardList )?
	)
;

pr_AltGuardList  :
	#(AltGuardList 
		( ( pr_GuardStatement | pr_ElseStatement ) (SemiColon)? )*
	)
;

pr_GuardStatement  :
	#(GuardStatement pr_AltGuardChar 
		(
			( 
				pr_AltstepInstance  
				(
					pr_StatementBlock 
				)?
				
			)
			| 
			(
				pr_GuardOp 
				pr_StatementBlock 
			)
		) 
	)
;

pr_ElseStatement  :
	#(ElseStatement 
		pr_StatementBlock 
	)
;

pr_AltGuardChar  :
	#(AltGuardChar ( ( pr_BooleanExpression  )? ) )
;

pr_GuardOp :
	#(GuardOp
		(
		pr_TimeoutStatement
		| pr_ReceiveStatement
		| pr_TriggerStatement
		| pr_GetCallStatement
		| pr_CatchStatement
		| pr_CheckStatement
		| pr_GetReplyStatement
		| pr_DoneStatement
		| pr_KilledStatement
		)
	)
;

pr_InterleavedConstruct  :
	#(InterleavedConstruct 
		( 
			pr_InterleavedGuardElement  
			(SemiColon )? 
		)* 
		
	)
;

pr_InterleavedGuardElement  :
	#(InterleavedGuardElement 
		pr_InterleavedGuard 
		pr_InterleavedAction 
	) 
	
;

pr_InterleavedGuard  :
	#(InterleavedGuard 
		pr_GuardOp 
	)
;

pr_InterleavedAction  :
	#(InterleavedAction  pr_StatementBlock  )
;

pr_LabelStatement {StringBuffer s;}:
	#(LabelStatement s=pr_Identifier )
;

pr_GotoStatement {StringBuffer s;}:
	#(GotoStatement s=pr_Identifier )
;

pr_RepeatStatement  :
	#(RepeatStatement 
		REPEAT 
	)
;

pr_BreakStatement  :
	#(BreakStatement 
		BREAK 
	)
;

pr_ContinueStatement  :
	#(ContinueStatement 
		CONTINUE 
	)
;



pr_ActivateOp  :
	#(ActivateOp pr_AltstepInstance )
;


pr_DeactivateStatement  :
	#(DeactivateStatement (pr_ComponentOrDefaultReference )? )

;

/***** 1.6.8 - Basic Statements  *****/

pr_BasicStatements  :
	#(BasicStatements
		(
		pr_Assignment 
		| pr_LogStatement 
		| pr_LoopConstruct 
		| pr_ConditionalConstruct 
		| pr_SelectCaseConstruct 
		)
		
	)
;

pr_SelectCaseConstruct  :
	#(SelectCaseConstruct 
		( 
			pr_SingleExpression 
			pr_SelectCaseBody
		)
	)
;

pr_SelectCaseBody  :
	#(SelectCaseBody 
		( pr_SelectCase  )+ 
	)
;  

pr_SelectCase  :
	#(SelectCase 
		(
			( (pr_TemplateInstance)* | ELSE )
			pr_StatementBlock 
		)
	)
;

pr_Expression :
	#(Expression
		(
		pr_SingleExpression
		| pr_CompoundExpression
		)
	)
;

pr_CompoundExpression :
	#(CompoundExpression (pr_FieldExpressionList | pr_ArrayExpression))
;

pr_FieldExpressionList  :
	#( FieldExpressionList  ( pr_FieldExpressionSpec  )* ) 
;

pr_FieldExpressionSpec  :
	#( FieldExpressionSpec pr_FieldReference  #(NotUsedOrExpression (pr_Expression | (NotUsedSymbol  ))))
;

pr_ArrayExpression  :
	#( ArrayExpression (pr_ArrayElementExpressionList)? )
;

pr_ArrayElementExpressionList  :
	#(ArrayElementExpressionList  (  (#(NotUsedOrExpression (pr_Expression | NotUsedSymbol )) )* )) 
;

pr_ConstantExpression  :
	#(ConstantExpression (pr_SingleConstExpression  | pr_CompoundConstExpression ))
;

pr_InnerConstantExpression  :
	#(ConstantExpression (pr_SingleConstExpression  | pr_CompoundConstExpression))
;

pr_SingleConstExpression :
	pr_SingleExpression
;

pr_BooleanExpression :
	#(BooleanExpression pr_SingleExpression)
;

pr_CompoundConstExpression :
	pr_FieldConstExpressionList | pr_ArrayConstExpression
;

pr_FieldConstExpressionList  :
	#( FieldConstExpressionList  ( pr_FieldConstExpressionSpec )* ) 
;

pr_FieldConstExpressionSpec  :
	#( FieldConstExpressionSpec pr_FieldReference  pr_InnerConstantExpression )
;

pr_ArrayConstExpression  :
    #( ArrayConstExpression ( pr_ArrayElementConstExpressionList )? )
;
 
pr_ArrayElementConstExpressionList :
	#( ArrayElementConstExpressionList  ( pr_InnerConstantExpression  )* ) 
;

pr_Assignment  :
	#(Assignment pr_VariableRef  (pr_Expression |pr_TemplateBody )) 
;

/*
  see parser.
  there are two mutually exlusive solutions below:
  - strictly v3.1.1 expressions,
  - efficient expressions.
*/

// ---------------------------------------------------------------------------------------

// strictly v3.1.1 expressions - begin
/*
pr_SingleExpression  :
	#(SingleExpression
			(
				pr_XorExpression 
				(
					pr_XorExpression 
				)*
			)
	 )
;

pr_XorExpression  : 
	#(XorExpression
    		(
    			pr_AndExpression 
    			( 
    		  		pr_AndExpression 
    		 	)*
    		)
	)
;

pr_AndExpression  :
	#(AndExpression
         (
			 pr_NotExpression 
             ( 
              	pr_NotExpression
             )*
          )
	)
;

pr_NotExpression  :
	#(NotExpression
			( 
				( Not )? pr_EqualExpression
			)
	)
;

pr_EqualExpression  :
	#(EqualExpression
			(
				pr_RelExpression 
				( 
					pr_RelExpression 
				)*
			)
	)
;

pr_RelExpression  :
	#(RelExpression
				(
				   pr_ShiftExpression 
				   (
				   #(RelOp
						(
						  Lessthan
						| Morethan
						| Notequals
						| Moreorequal
						| Lessorequal 
						)
					)
				     pr_ShiftExpression 
				    )?
				)
	)
;

pr_ShiftExpression  : 
	#(ShiftExpression
				(
				   pr_BitOrExpression 
				   (
				   #(ShiftOp				   
						(
						  SHIFTLEFT 
						| SHIFTRIGHT  
						| ROTATELEFT 
						| ROTATERIGHT  
						)
				   )
				   pr_BitOrExpression
				   )*
				)
	)
;

pr_BitOrExpression  :
	#(BitOrExpression
 				(
 				    pr_BitXorExpression 
 				    ( 
 				      pr_BitXorExpression 
 				      )*
 				 )
	)
;

pr_BitXorExpression  :
	#(BitXorExpression
 					(
 					   pr_BitAndExpression 
 					   ( 
 					     pr_BitAndExpression 
 					    )*
 					)
	)
;

pr_BitAndExpression  :
	#(BitAndExpression
				(
				   pr_BitNotExpression 
				   ( 
				   pr_BitNotExpression 
				   )*
				)
	)
;

pr_BitNotExpression  :
	#(BitNotExpression
      ( 
       ( Not4b )? pr_AddExpression 
      )
	)
;

pr_AddExpression  : 
	#(AddExpression
				(
				  pr_MulExpression 	 		  
				  ( 
				  #(AddOp
					  ( 
					    Plus
					  | Minus
			 		  | StringOp
			 		  ) 
			      )
				  pr_MulExpression 
				  )*
				)
	 )
;

pr_MulExpression  :
	#(MulExpression
      (
         pr_UnaryExpression 
         (
         #(MultiplyOp
			 ( 
			   Star
			 | Slash
			 | Mod
			 | Rem
			 ) 
	     )
         pr_UnaryExpression 
         )*
      )
	)
;

pr_UnaryExpression  :
	#(UnaryExpression
  		(
	  		#(UnaryOp
		  		  ( 
		  		    Plus 
		  		  | Minus 
		  		  )
		    )
  		)? 
  		pr_Primary
	)
;
*/

// strictly v3.1.1 expressions - end

// ---------------------------------------------------------------------------------------

// efficient expressions - begin

pr_SingleExpression  : 
    #(SingleExpression pr_SimpleExpressions)
;

pr_SimpleExpressions  : 
    (AND4B | XOR4B | OR4B | AND | XOR | OR | STRINGOP | SHIFTOP | EQUAL) => (
        #(AND4B pr_SimpleExpressions pr_SimpleExpressions)  |
        #(XOR4B pr_SimpleExpressions pr_SimpleExpressions)  |
        #(OR4B pr_SimpleExpressions pr_SimpleExpressions)  |
        #(AND pr_SimpleExpressions pr_SimpleExpressions)  |
        #(OR pr_SimpleExpressions pr_SimpleExpressions)  |
        #(XOR pr_SimpleExpressions pr_SimpleExpressions)  |
        #(STRINGOP pr_SimpleExpressions pr_SimpleExpressions)  |
        #(EQUAL pr_SimpleExpressions pr_SimpleExpressions)  
    ) 
    | pr_SimpleExpression
;

pr_SimpleExpression  : 
    pr_SubExpression |
    (
        #(LESSTHAN pr_SubExpression pr_SubExpression)  |
        #(MORETHAN pr_SubExpression pr_SubExpression)  |
        #(NOTEQUALS pr_SubExpression pr_SubExpression)  |
        #(MOREOREQUAL pr_SubExpression pr_SubExpression)  |
        #(LESSOREQUAL pr_SubExpression pr_SubExpression) 
    )
;

pr_SubExpression  : 
    pr_Product |
    (
        #(SHIFTLEFT pr_Product pr_SubExpression)  |
        #(SHIFTRIGHT pr_Product pr_SubExpression)  |
        #(ROTATELEFT pr_Product pr_SubExpression)  |
        #(ROTATERIGHT pr_Product pr_SubExpression) 
    )
;

pr_Product  : 
    pr_Term |
    (
        #(PLUS pr_Product pr_Product)  |
        #(MINUS pr_Product pr_Product)  |
        #(STRINGOP pr_Product pr_Product) 
    )
;

pr_Term  : 
    pr_Factor |
    (
        #(STAR pr_Term pr_Term)  |
        #(SLASH pr_Term pr_Term)  |
        #(MOD pr_Term pr_Term)  |
        #(REM pr_Term pr_Term) 
    )
;

pr_Factor  : 
    #(Factor 
        (
            pr_Primary |
            (
                #(PLUS pr_Primary)  |
                #(MINUS pr_Primary)  |
                #(NOT pr_Primary)  |
                #(NOT4B pr_Primary) 
            )
        )
    )
;

// efficient expressions - end


// ---------------------------------------------------------------------------------------

pr_Primary  : 
	#(Primary 
		(
			( pr_OpCall ) => pr_OpCall
			| pr_Value
			| pr_SingleExpression 
		)
	)
;

// see parser
pr_ExtendedFieldReference {StringBuffer s;} :
	#(ExtendedFieldReference (((s=pr_Identifier  |pr_PredefinedType|pr_ArrayOrBitRef) )+))
;

pr_OpCall  :
	#(OpCall
		(
		pr_ConfigurationOps
		| pr_VerdictOps
		| pr_TimerOps
		| pr_TestcaseInstance
		| pr_FunctionInstance
		| pr_TemplateOps
		| pr_ActivateOp
		)
	)
;

// ---------------------------------------------------------------------------------------

pr_LogItem :
	#(LogItem( pr_FreeText | pr_TemplateInstance ))
;

pr_LogStatement  :
	#( LogStatement 
		(
			pr_LogItem  
			(pr_LogItem )*  
		) 
	)
;


pr_LoopConstruct : 
	#(LoopConstruct (pr_ForStatement | pr_WhileStatement | pr_DoWhileStatement))
;

pr_ForStatement  :
	#(ForStatement  
		( pr_VarInstance  | pr_Assignment  ) 
		pr_BooleanExpression 
		pr_Assignment  
		pr_StatementBlock 
		
	)
;

pr_WhileStatement  :
	#(WhileStatement 
		(
			pr_BooleanExpression 
			pr_StatementBlock 
		)
	)
;

pr_DoWhileStatement  :
	#(DoWhileStatement 
		(
			pr_StatementBlock
			pr_BooleanExpression 
		)
	) 
;

pr_ConditionalConstruct  : 
	#(ConditionalConstruct
		pr_BooleanExpression 
		pr_StatementBlock 
		( pr_ElseIfClause  )* 
		( pr_ElseClause )? 
	)
;

pr_ElseIfClause :
	#(ElseIfClause 
		( 
			pr_BooleanExpression 
			pr_StatementBlock 
		) 
	)
;


pr_ElseClause  :
	#(ElseClause 
		(
			pr_StatementBlock
		)
	)
;

pr_InOutParKeyword  :
	#(InOutParKeyword a:INOUT )
;

