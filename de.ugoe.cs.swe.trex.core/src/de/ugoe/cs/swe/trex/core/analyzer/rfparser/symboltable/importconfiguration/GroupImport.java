package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration;

import java.util.LinkedList;
import java.util.List;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;


public class GroupImport extends AbstractKindImport {

	private List<String> exceptionList = new LinkedList<String>();

	private List<GroupExceptImport> refList = new LinkedList<GroupExceptImport>();

	public enum ModeEnum {
		allGroupWithExcept, groupRefListWithExcept
	}

	private ModeEnum mode;

	public List<String> getExceptionList() {
		return exceptionList;
	}

	public void setExceptionList(List<String> exceptionList) {
		this.exceptionList = exceptionList;
	}

	public List<GroupExceptImport> getRefList() {
		return refList;
	}

	public void setRefList(List<GroupExceptImport> refList) {
		this.refList = refList;
	}

	public ModeEnum getMode() {
		return mode;
	}

	public void setMode(ModeEnum mode) {
		this.mode = mode;
	}

	public void addToRefList(GroupExceptImport imp) {
		refList.add(imp);
	}
	
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		
		result.append("GroupImport: ");
		result.append(mode.name()).append(": ");
		if (mode == ModeEnum.allGroupWithExcept) {
			result.append("Exceptions: ");
			if (exceptionList != null) {
				for (int i=0; i < exceptionList.size(); i++) {
					result.append(exceptionList.get(i)).append(", ");
				}
			} else {
				result.append(" n/a ");
			}
		}
		
		if (mode == ModeEnum.groupRefListWithExcept) {
			if (refList != null) {
				for (int i=0; i < refList.size(); i++) {
					result.append(refList.get(i).toString());
				}
			} else {
				result.append(" n/a ");
			}
		}

		return result.toString();
	}

	@Override
	public boolean isAllowed(Symbol s, KindEnum kind) {
		if (s.getGroupName() == null)
			return false;
		
		if (mode == ModeEnum.allGroupWithExcept) {
			if (exceptionList == null)
				return true;
			
			for (int i=0; i < exceptionList.size(); i++) {
				if (exceptionList.get(i).equals(s.getGroupName()))
					return false;
			}
			return true;
		}
		
		if (mode == ModeEnum.groupRefListWithExcept) {
			if (refList == null)
				return false;

			for (int i=0; i < refList.size(); i++) {
				if (refList.get(i).getGroupName().equals(s.getGroupName())) {
					if (!(refList.get(i).isAllowed(s, kind)))
						return false;
					return true;
				}
			}
		}
		
		return false;
	}


}
