package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import antlr.CommonAST;
import antlr.Token;
import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.TTCN3CorePlugin;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;

public class LocationAST extends CommonAST implements Comparable<LocationAST> {

	private static final long serialVersionUID = 1L;

	protected int offset = -1;

	protected int endOffset = -1;

	private LocationAST mParent;

	private transient Token token;

	private transient TokenWithIndex lexerToken;

	private transient Scope scope = null;

	private transient Scope childScope = null;

	private int line = -1;

	private int endLine = -1;

	// ---------------------------------------------------------------------------

	public LocationAST() {
		super();
	}

	// ---------------------------------------------------------------------------

	public int getEndOffset() {
		return endOffset;
	}

	// ---------------------------------------------------------------------------

	public void setEndOffset(int endOffset) {
		this.endOffset = endOffset;
	}

	// ---------------------------------------------------------------------------

	public int getOffset() {
		return offset;
	}

	// ---------------------------------------------------------------------------

	public void setOffset(int offset) {
		this.offset = offset;
	}

	// ---------------------------------------------------------------------------

	public int getEndLine() {
		return endLine;
	}

	// ---------------------------------------------------------------------------

	public Scope getScope() {
		return scope;
	}

	// ---------------------------------------------------------------------------

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	// ---------------------------------------------------------------------------

	public LocationAST(Token tok) {
		super(tok);
		initialize(tok);
	}

	// ---------------------------------------------------------------------------

	@Override
	public void initialize(int t, String txt) {
		setType(t);
		setText(txt);
	}

	// ---------------------------------------------------------------------------

	public void initialize(AST t)
	{
		super.initialize(t);
	}

	// ---------------------------------------------------------------------------

	@Override
	public void initialize(Token tok) {
		if (tok == null) {
			return;
		}
		setText(tok.getText());
		setType(tok.getType());

		if (tok instanceof TokenWithIndex) {
			TokenWithIndex tokIndex = (TokenWithIndex) tok;
			if (tokIndex.getType() == TTCN3LexerTokenTypes.CSTRING) {
				offset = (tokIndex.getOffset() - 2 - tokIndex.getText()
						.length());
				endOffset = (tokIndex.getOffset());
				line = tokIndex.getLine();
			} else if ((tokIndex.getType() == TTCN3LexerTokenTypes.BSTRING)
					|| (tokIndex.getType() == TTCN3LexerTokenTypes.HSTRING)
					|| (tokIndex.getType() == TTCN3LexerTokenTypes.OSTRING)
					|| (tokIndex.getType() == TTCN3LexerTokenTypes.BSTRINGMATCH)
					|| (tokIndex.getType() == TTCN3LexerTokenTypes.OSTRINGMATCH)
					|| (tokIndex.getType() == TTCN3LexerTokenTypes.HSTRINGMATCH)) {
				offset = (tokIndex.getOffset() - 3 - tokIndex.getText()
						.length());
				endOffset = (tokIndex.getOffset());
				line = tokIndex.getLine();
			}

			else {
				if (tokIndex.getText() != null) {
					offset = (tokIndex.getOffset() - tokIndex.getText().length());
				}
				endOffset = (tokIndex.getOffset());
				line = tokIndex.getLine();
			}
		}

		this.token = tok;
	}

	// ---------------------------------------------------------------------------

	public boolean isImaginaryNode() {
		return (token == null);
	}

	// ---------------------------------------------------------------------------

	@Override
	public int getType() {
		if (token == null)
			return super.getType();

		return token.getType();
	}

	// ---------------------------------------------------------------------------

	public Token getToken() {
		return token;
	}

	// ---------------------------------------------------------------------------

	public LocationAST getNthChild(int depth) {
		if (depth > 1) {
			return getFirstChild().getNthChild(depth - 1);
		}
		return getFirstChild();
	}

	// ---------------------------------------------------------------------------

	public LocationAST getNthParent(int depth) {
		if (depth > 1) {
			return getParent().getNthParent(depth - 1);
		}
		return getParent();
	}

	// ---------------------------------------------------------------------------

	public boolean existsNthChild(int depth) {
		if (depth > 1) {
			if (getFirstChild() != null)
				return getFirstChild().existsNthChild(depth - 1);
			else
				return false;
		}
		if (getFirstChild() != null)
			return true;
		else
			return false;
	}

	// ---------------------------------------------------------------------------

	@Override
	public void setFirstChild(AST anAST) {
		if (anAST == null) {
			super.setFirstChild(null);
		} else {
			LocationAST myAST = (LocationAST) anAST;
			myAST.setParent(this);
			super.setFirstChild(myAST);
		}
	}

	// ---------------------------------------------------------------------------

	@Override
	public void setNextSibling(AST anAST) {
		if (anAST == null) {
			super.setNextSibling(null);
		} else {
			LocationAST myAST = (LocationAST) anAST;
			myAST.setParent(mParent);
			super.setNextSibling(myAST);
		}
	}

	// ---------------------------------------------------------------------------

	@Override
	public void addChild(AST anAST) {
		if (anAST == null) {
			super.addChild(null);
		} else {
			LocationAST myAST = (LocationAST) anAST;
			myAST.setParent(this);
			super.addChild(myAST);
		}
	}

	// ---------------------------------------------------------------------------

	public void setLocation(LocationAST mtok) {
		try {
			offset = mtok.getOffset();
			line = mtok.getLine();
			lexerToken = (TokenWithIndex) mtok.getToken();
		} catch (NullPointerException nex) {
			return;
		}
	}

	// ---------------------------------------------------------------------------

	public void setEndLocation(LocationAST tok) {
		if (tok == null)
			return;

		try {
			endOffset = tok.getEndOffset();
			endLine = tok.getLine();
		} catch (NullPointerException nex) {
			return;
		}
	}

	// ---------------------------------------------------------------------------

	public void setLocation(LocationAST startTok, LocationAST endTok) {
		if (startTok == null || endTok == null)
			return;

		try {
			offset = startTok.getOffset();
			line = startTok.getLine();

			endOffset = endTok.getEndOffset();
			endLine = endTok.getEndLine();

			if (endLine == -1) {
				if (startTok != endTok)
					endLine = endTok.getLine();
				else
					endLine = startTok.getLine();
			}

			lexerToken = (TokenWithIndex) startTok.getToken();
			if (lexerToken == null)
				lexerToken = (TokenWithIndex) startTok.getLexerToken();

		} catch (NullPointerException nex) {
			nex.printStackTrace();
			return;
		}
	}

	// ---------------------------------------------------------------------------

	@Override
	public String toString() {
		// return "[" + getText() + "," + getLine() + ":" + getColumn() + "]";
		String result = "";


		boolean hasScope = (getScope() != null);
		String scope = "";
		if (hasScope) {
			if (getScope().getScopeSymbol() !=null) {
				scope = getScope().getScopeSymbol().getName();
			} else {
				scope = "scope symbol is null!";
			}
		}
		if (this.getType() == TTCN3LexerTokenTypes.IDENTIFIER) {
			return getText()
					+ " (Line: "
					+ getLine()
					+ ") "
					+ " ["
					+ scope
					+ "] ]";
		}


		result += super.toString();
		if (isImaginaryNode()) {
			result += "[L/AST:imaginary]";
		} else {
			result += "[L/AST]";
		}
		result += "[startOfs: "
				+ getOffset()
				+ ", endOfs: "
				+ getEndOffset()
				+ ", line: "
				+ getLine()
				+ ", endline: "
				+ getEndLine()
				+ ", hasScope: "
				+ hasScope
				+ " ["
				+ scope
				+ "]";

		LocationAST moduleNode = ASTUtil.getModuleNode(this);
		if (moduleNode!=null && moduleNode.getScope()!=null) {
			result +=", file: "
				+ moduleNode.getScope().getAssociatedFilename();
		}
		result += " ]";

		return result;
	}

	// ---------------------------------------------------------------------------

	public String toIdString() {
		if (this.getType() == TTCN3LexerTokenTypes.IDENTIFIER) {
			return getText()
					+ " (Line: "
					+ getLine()
					+ ")";
		}
		String result = "";

		if (isImaginaryNode()) {
			result += "[L/AST:imaginary]";
		} else {
			result += "[L/AST]";
		}
		result += "[startOfs: "
				+ getOffset()
				+ ", endOfs: "
				+ getEndOffset()
				+ ", line: "
				+ getLine()
				+ ", endline: "
				+ getEndLine();
		LocationAST moduleNode = ASTUtil.getModuleNode(this);
		if (moduleNode!=null) {
			result +=",\\nfile: "
				+ moduleNode.getScope().getAssociatedFilename().substring(moduleNode.getScope().getAssociatedFilename().lastIndexOf("/")+1);
		}
		result += "]";
		return result;
	}

	// ---------------------------------------------------------------------------

	public String toStringMultiline() {
		String result = "";

		boolean hasScope = (getScope() != null);
		String scope = "";
		if (hasScope) {
			if (getScope().getScopeSymbol() !=null) {
				scope = getScope().getScopeSymbol().getName();
			} else {
				scope = "scope symbol is null!\\n";
			}
		}
		if (this.getType() == TTCN3LexerTokenTypes.IDENTIFIER) {
			return getText()
					+ " (Line: "
					+ getLine()
					+ ") "
					+ " ["
					+ scope
					+ "] ]";
		}


		result += super.toString();
		result += "\\n[line: "
				+ getLine()
				+ ",\\nendline: "
				+ getEndLine();
		LocationAST moduleNode = ASTUtil.getModuleNode(this);
		if (moduleNode!=null) {
			result +=",\\nfile: "
				+ moduleNode.getScope().getAssociatedFilename().substring(moduleNode.getScope().getAssociatedFilename().lastIndexOf("/")+1);
		}
		result += "]";

		return result;
	}


	// ---------------------------------------------------------------------------

	private void setParent(LocationAST aParent) {
		mParent = aParent;
		LocationAST nextSibling = this.getNextSibling();
		while (nextSibling != null) {
			nextSibling.mParent = aParent;
			nextSibling = nextSibling.getNextSibling();
		}
	}

	// ---------------------------------------------------------------------------

	public LocationAST getParent() {
		return mParent;
	}

	// ---------------------------------------------------------------------------

	@Override
	public boolean equals(AST t) {
		boolean superResult = super.equals(t);
		boolean thisResult = false;
		if ((t.getLine() == this.getLine())
				&& (t.getColumn() == this.getColumn()))
			thisResult = true;

		return superResult && thisResult;
	}

	// ---------------------------------------------------------------------------

	public static boolean isOfType(LocationAST node, int type) {
		if (node.getType() == type)
			return true;

		return false;
	}

	// ---------------------------------------------------------------------------

	public static boolean isOfType(LocationAST node, int[] type) {
		for (int element : type) {
			if (node.getType() == element)
				return true;
		}
		return false;
	}

	// ---------------------------------------------------------------------------

	public static LocationAST resolveParentsUntilType(LocationAST node,
			int[] type) {
		LocationAST match = node;

		while (match != null) {
			if (LocationAST.isOfType(match, type)) {
				return match;
			}
			match = match.getParent();
		}

		return null;
	}

	// ---------------------------------------------------------------------------

	public static LocationAST resolveParentsUntilType(LocationAST node, int type) {
		LocationAST match = node;

		while (match != null) {
			if (LocationAST.isOfType(match, type)) {
				return match;
			}
			match = match.getParent();
		}

		return null;
	}

	// ---------------------------------------------------------------------------

	public static boolean isExtendedReference(LocationAST t) {
		if (t == null)
			return false;

		if (t.getParent() == null)
			return false;

		if (t.getParent().getParent() == null)
			return false;

		LocationAST node = t.getParent().getParent();

		if ((node.getType() == TTCN3LexerTokenTypes.ExtendedFieldReference)
				|| (node.getType() == TTCN3LexerTokenTypes.ValueReference))
			return true;

		return false;
	}

	// ---------------------------------------------------------------------------

	public static boolean isPortReference(LocationAST t) {
		if (t == null)
			return false;

		if (t.getParent() == null)
			return false;

		if (t.getParent().getParent() == null)
			return false;

		LocationAST node = t.getParent().getParent();

		if (node.getType() == TTCN3LexerTokenTypes.Port)
			return true;

		return false;
	}

	// ---------------------------------------------------------------------------

	public static boolean isFieldReference(LocationAST t) {
		if (t == null)
			return false;

		if (t.getParent() == null)
			return false;

		if (t.getParent().getParent() == null)
			return false;

		LocationAST node = t.getParent().getParent();

		if (node.getType() == TTCN3LexerTokenTypes.FieldReference)
			return true;

		return false;
	}

	// ---------------------------------------------------------------------------

	public static boolean isEnumeratedValue(LocationAST t) {
		if (t == null)
			return false;

		if (t.getParent() == null)
			return false;

		if (t.getParent().getParent() == null)
			return false;

		LocationAST node = t.getParent().getParent();

		if (node.getType() == TTCN3ParserTokenTypes.EnumeratedValue) {
			//TODO: add supplemental contexts
			//-> switched to common denominator SingleExpression
			//   the specific contexts are delegated to Scope
			// FieldSpec and Assignment can be considered redundant here
			LocationAST test = LocationAST.resolveParentsUntilType(node,
					new int[] { TTCN3ParserTokenTypes.FieldSpec,
							TTCN3ParserTokenTypes.Assignment,
							TTCN3ParserTokenTypes.SingleExpression});
			if (test != null)
				return true;
		}

		return false;
	}
	// ---------------------------------------------------------------------------

	public static boolean isActualParameterAssignment(LocationAST t) {
		if (t == null)
			return false;

		if (t.getParent() == null)
			return false;

		if (t.getParent().getParent() == null)
			return false;

		LocationAST node = t.getParent().getParent();

		if ((node.getType() == TTCN3LexerTokenTypes.TemplateInstanceAssignment) || (node.getType() == TTCN3LexerTokenTypes.ComponentRefAssignment)) {
			return true;
		}

		return false;
	}

	// ---------------------------------------------------------------------------

	public static boolean isFieldDefTypeDef(LocationAST t) {
		if (t == null)
			return false;

		if (t.getParent() == null)
			return false;

		if (t.getParent().getParent() == null)
			return false;

		LocationAST node = t.getParent().getParent();

		if ((node.getType() == TTCN3LexerTokenTypes.TypeReference)) {
			LocationAST fieldDefParent = LocationAST.resolveParentsUntilType(node, new int[]{TTCN3ParserTokenTypes.StructFieldDef, TTCN3ParserTokenTypes.UnionFieldDef});
			if (fieldDefParent!=null) {
				return true;
			}
		}

		return false;
	}

	// ---------------------------------------------------------------------------

	public Scope getChildScope() {
		return childScope;
	}

	// ---------------------------------------------------------------------------

	public void setChildScope(Scope childScope) {
		this.childScope = childScope;
	}

	// ---------------------------------------------------------------------------

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(LocationAST arg0) {
		return this.getText().compareTo(arg0.getText());
	}

	// ---------------------------------------------------------------------------

	@Override
	public int getLine() {
		return line;
	}

	// ---------------------------------------------------------------------------

	@Override
	public LocationAST getNextSibling() {
//		return (LocationAST) super.getNextSibling();
		return (LocationAST) right;
	}

	// ---------------------------------------------------------------------------

	@Override
	public LocationAST getFirstChild() {
//		return (LocationAST) super.getFirstChild();
		return (LocationAST) down;
	}

	// ---------------------------------------------------------------------------

	@Override
	public int hashCode() {
		int hash = 11;
		hash = 31 * hash + offset;
		hash = 31 * hash + endOffset;
		hash = 31 * hash + ((token == null) ? super.getType() : token.getType());
		return hash;
	}

	// ---------------------------------------------------------------------------

	public Symbol getSymbol() {
		//return getScope().resolve(this);
		Scope scope = this.getScope();
		if (scope == null) {
			//TODO: FixMe: Investigate Scope Resolution Problems
			//System.out.println(">>>>>>>Scope ISSUE: "+this);
			return null;
		}
		Symbol symbol = scope.resolve(this);
		return symbol;

	}

	// ---------------------------------------------------------------------------

	private static boolean bXMLPrintNewline = false;
	private static boolean bXMLIndentThisLine = true;
	private static boolean bXMLIndent = false;

	private void xmlFormat(Writer stream, long depth) throws IOException {
		if (bXMLPrintNewline)
				stream.write("\n");

		// indentation bloats XML and makes the generation slow!
		if (bXMLIndent && bXMLIndentThisLine)
			for (long i = 0; i < depth; i++)
				stream.write(" ");

		bXMLPrintNewline = bXMLIndentThisLine = true;
	}

	private String xmlAddAttribute(String name, Object value) {
		return " " + name + "=\"" + value.toString() + "\"";
	}

	private void xmlSerializeTextNode(Writer stream, long depth)
		throws IOException
	{
		String str = encodeXML(getText());
		stream.write(str);
		bXMLPrintNewline = bXMLIndentThisLine = false;
	}

	private String encodeXML(String s) {
		String str = encode(s);
		if (str.equals("!=")) {
			str = "NotEquals";
		}
		else if (str.equals("&amp;")) {
			str = "AndOperator";
		}
		else if (str.equals("+")) {
			str = "PlusOperator";
		}
		else if (str.equals("&lt;")) {
			str = "LessThanOperator";
		}
		else if (str.equals("&gt;")) {
			str = "GreaterThanOperator";
		}
		else if (str.equals("==")) {
			str = "EqualsOperator";
		}
		else if (str.equals("/")) {
			str = "DivideOperator";
		}
		else if (str.equals("-")) {
			str = "MinusOperator";
		}
		else if (str.equals("*")) {
			str = "MultiplyOperator";
		}
		else if (str.equals("&lt;=")) {
			str = "LessOrEqualsOperator";
		}
		else if (str.equals("&gt;=")) {
			str = "GreaterOrEqualsOperator";
		}

		if (str.contains(" "))
			str = str.replaceAll(" ","");

		return str;
	}

	private void xmlSerializeElementOpen(Writer stream, long depth, String fileName)
		throws IOException
	{
		xmlFormat(stream, depth);

		stream.write("<" + encodeXML(getText()));
		if (depth == 1)
			stream.write(xmlAddAttribute("filename", encode(fileName)));
		stream.write(xmlAddAttribute("line", getLine()));
		// add the "line-end" attribute only if its different from the line attr
		if (getLine() != getEndLine())
			stream.write(xmlAddAttribute("line-end", getEndLine()));
		stream.write(xmlAddAttribute("offset", getOffset()));
		stream.write(xmlAddAttribute("offset-end", getEndOffset()));
		stream.write(">");
	}

	private void xmlSerializeElementClose(Writer stream, long depth)
		throws IOException
	{
		xmlFormat(stream, depth);
		stream.write("</" + encodeXML(getText()) + ">");
	}

	public void xmlSerialize(Writer stream, String fileName, boolean indentXML)
		throws IOException
	{
		bXMLIndent = indentXML;
		xmlSerialize(stream, 1, fileName);
	}

	private void xmlSerialize(Writer stream, long depth, String fileName)
		throws IOException
	{
		for (AST node = this; node != null; node = node.getNextSibling()) {
			LocationAST locationNode = (LocationAST)node;

			if (locationNode.getFirstChild() == null) {
				// print guts (class name, attributes)
				locationNode.xmlSerializeTextNode(stream, depth);
			} else {
				// print opening tag
				locationNode.xmlSerializeElementOpen(stream, depth, fileName);
				// print children
				locationNode.getFirstChild().
					xmlSerialize(stream, depth + 1, fileName);
				// print end tag
				locationNode.xmlSerializeElementClose(stream, depth);
			}
		}
	}

	public List<TokenWithIndex> getCommentsBefore() {
		ArrayList<TokenWithIndex> result = new ArrayList<TokenWithIndex>();
		if (lexerToken != null) {
			TokenStreamTracker tracker = lexerToken.getTracker();
			int index = lexerToken.getIndex() - 1;
			while ((index >= 0) && !tracker.isNonDiscardedToken(index))	{
				TokenWithIndex tok = (TokenWithIndex) tracker.getToken(index);
				if ((tok.getType() == TTCN3LexerTokenTypes.SL_COMMENT) ||
				(tok.getType() == TTCN3LexerTokenTypes.ML_COMMENT)) {
					result.add(tok);
				}
				index--;
			}
		}
		return result;
	}

	//TODO: Currently not working, investigate eventually
//	public List<TokenWithIndex> getCommentsAfter() {
//		ArrayList<TokenWithIndex> result = new ArrayList<TokenWithIndex>();
//
//		if (lexerToken != null) {
//			TokenStreamTracker tracker = lexerToken.getTracker();
//			int index = lexerToken.getIndex() + 1;
//			while ((index < tracker.getTokenCount()) && !tracker.isNonDiscardedToken(index))	{
//				TokenWithIndex tok = (TokenWithIndex) tracker.getToken(index);
//				System.out.println(tok.getType());
//				if ((tok.getType() == TTCN3LexerTokenTypes.SL_COMMENT) ||
//				(tok.getType() == TTCN3LexerTokenTypes.ML_COMMENT)) {
//					result.add(tok);
//				}
//				index++;
//			}
//
//		}
//
//		return result;
//	}

	public TokenWithIndex getLexerToken() {
		return lexerToken;
	}

	/**
	 * @param node - a module definition node
	 * @return the type node, null otherwise
	 */

	public static LocationAST getModuleDefinitionTypeNode(LocationAST node) {
		LocationAST typeNode = null;
		if (node.getType() == TTCN3ParserTokenTypes.ModuleDefinition){
			typeNode = node.getFirstChild();
			if (typeNode.getType() == TTCN3ParserTokenTypes.Visibility){
				typeNode = typeNode.getNextSibling();
			}
		}
		return typeNode;
	}

	/**
	 * @param node - a module definition node
	 * @return a list of the identifier nodes of the module definition or empty list if node is not a module definition node
	 */
	public static ArrayList<LocationAST> getModuleDefinitionIdentifiersList(LocationAST node){
		ArrayList<LocationAST> identifierNodesList = new ArrayList<LocationAST>();
		if (node.getType() == TTCN3ParserTokenTypes.ModuleDefinition){
			LocationAST typeNode = node.getFirstChild();
			if (typeNode.getType() == TTCN3ParserTokenTypes.Visibility){
				typeNode = typeNode.getNextSibling();
			}
			switch (typeNode.getType()) {

			//class 1
			case TTCN3ParserTokenTypes.SignatureDef:
			case TTCN3ParserTokenTypes.FunctionDef:
			case TTCN3ParserTokenTypes.TestcaseDef:
			case TTCN3ParserTokenTypes.AltstepDef:
			case TTCN3ParserTokenTypes.GroupDef:
			case TTCN3ParserTokenTypes.ExtFunctionDef:
				identifierNodesList.add(typeNode.getFirstChild());
				break;

			case TTCN3ParserTokenTypes.ModuleParDef:
				LocationAST moduleParNode = typeNode.getFirstChild();
				//reconfigure for MultitypedModuleParLists
				if (moduleParNode.getType() == TTCN3ParserTokenTypes.MultitypedModuleParList){
					moduleParNode = moduleParNode.getFirstChild();
					if (moduleParNode == null){
						//break on empty
						break;
					}
				}
				do{
					//skip on semi-colon
					if (moduleParNode.getType() == TTCN3ParserTokenTypes.ModulePar){
						LocationAST moduleParListNode = moduleParNode.getFirstChild().getNextSibling();
						LocationAST moduleParIdentifier = moduleParListNode.getFirstChild();

						do{
							if (moduleParIdentifier.getType() == TTCN3ParserTokenTypes.Identifier){
								identifierNodesList.add(moduleParIdentifier);
							}
						} while ((moduleParIdentifier = moduleParIdentifier.getNextSibling()) != null);
					}
				} while ((moduleParNode = moduleParNode.getNextSibling()) != null);
				break;

			case TTCN3ParserTokenTypes.TemplateDef:
				LocationAST secondLevelChild = typeNode.getFirstChild();
				if (secondLevelChild.getType() == TTCN3ParserTokenTypes.TemplateRestriction){
					secondLevelChild = secondLevelChild.getNextSibling();
				}
				identifierNodesList.add(secondLevelChild.getFirstChild().getNextSibling());
				break;

			case TTCN3ParserTokenTypes.TypeDef:
				secondLevelChild = typeNode.getFirstChild();
				if (secondLevelChild.getType() == TTCN3ParserTokenTypes.SubTypeDef){
					LocationAST identifierNode = secondLevelChild.getFirstChild().getNextSibling();
					if (identifierNode.getType() == TTCN3ParserTokenTypes.Identifier){
						identifierNodesList.add(identifierNode);
					}
				} else {
					switch (secondLevelChild.getFirstChild().getType()) {
					//class 2
					case TTCN3ParserTokenTypes.PortDef:
					case TTCN3ParserTokenTypes.ComponentDef:
					case TTCN3ParserTokenTypes.EnumDef:
						LocationAST class2identifierNode = secondLevelChild.getFirstChild().getFirstChild();
						if (class2identifierNode.getType() == TTCN3ParserTokenTypes.Identifier){
							identifierNodesList.add(class2identifierNode);
						}
						break;
					//class 3
					case TTCN3ParserTokenTypes.UnionDef:
					case TTCN3ParserTokenTypes.RecordDef:
					case TTCN3ParserTokenTypes.SetDef:
						LocationAST class3identifierNode = secondLevelChild.getFirstChild().getFirstChild().getFirstChild();
						if (class3identifierNode.getType() == TTCN3ParserTokenTypes.Identifier){
							identifierNodesList.add(class3identifierNode);
						}
						break;
					//class 4
					case TTCN3ParserTokenTypes.RecordOfDef:
					case TTCN3ParserTokenTypes.SetOfDef:
						LocationAST fourthLevelChild = secondLevelChild.getFirstChild().getFirstChild();
						if (fourthLevelChild.getType() == TTCN3ParserTokenTypes.StringLength){
							fourthLevelChild = fourthLevelChild.getNextSibling();
						}
						LocationAST class4identifierNode = fourthLevelChild.getFirstChild().getNextSibling();
						if (class4identifierNode.getType() == TTCN3ParserTokenTypes.Identifier){
							identifierNodesList.add(class4identifierNode);
						}
						break;
					default:
						break;
					}
				}

				break;

			case TTCN3ParserTokenTypes.ConstDef:
				LocationAST singleConstDefNode = typeNode.getFirstChild().getNextSibling();
				do{
					identifierNodesList.add(singleConstDefNode.getFirstChild());
				} while ((singleConstDefNode = singleConstDefNode.getNextSibling()) != null);

				break;

			case TTCN3ParserTokenTypes.ExtConstDef:
				LocationAST extConstIdentifierNode = typeNode.getFirstChild().getNextSibling();
				do{
					identifierNodesList.add(extConstIdentifierNode);
				} while ((extConstIdentifierNode = extConstIdentifierNode.getNextSibling()) != null);

				break;
			case TTCN3ParserTokenTypes.ImportDef:
				break;
			default:
				TTCN3CorePlugin.debugMessage("Problem occured determining the type of "+node);
				break;
			}

		}

		return identifierNodesList;
	}

	/**
	 * @returns a list of the identifier nodes of the containing module definition
	 */
	public ArrayList<LocationAST> getContainingModuleDefinitionIdentifiersList(){
		ArrayList<LocationAST> identifierNodesList = new ArrayList<LocationAST>();
		LocationAST moduleDefinitionNode = LocationAST.resolveParentsUntilType(this, TTCN3ParserTokenTypes.ModuleDefinition);
		identifierNodesList = LocationAST.getModuleDefinitionIdentifiersList(moduleDefinitionNode);
		return identifierNodesList;
	}

	/**
	 * @param int - TTCN3ParserTokenType code
	 * @return String - type pretty name of the corresponding node, null for not yet defined types
	 */

	public static String getTTCN3ParserTokenTypeTypePrettyName(int type) {
		String correspondingTypePrettyName = null;
		switch (type) {
		case TTCN3ParserTokenTypes.AltstepDef:
			correspondingTypePrettyName = "Altstep";
			break;
		case TTCN3ParserTokenTypes.TestcaseDef:
			correspondingTypePrettyName = "Testcase";
			break;
		case TTCN3ParserTokenTypes.ConstDef:
			correspondingTypePrettyName = "Constant";
			break;
		case TTCN3ParserTokenTypes.ExtConstDef:
			correspondingTypePrettyName = "External Constant";
			break;
		case TTCN3ParserTokenTypes.FunctionDef:
			correspondingTypePrettyName = "Function";
			break;
		case TTCN3ParserTokenTypes.ExtFunctionDef:
			correspondingTypePrettyName = "External Function";
			break;
		case TTCN3ParserTokenTypes.ModuleParDef:
			correspondingTypePrettyName = "Module Parameter";
			break;
		case TTCN3ParserTokenTypes.SignatureDef:
			correspondingTypePrettyName = "Signature";
			break;
		case TTCN3ParserTokenTypes.TemplateDef:
			correspondingTypePrettyName = "Template";
			break;
		case TTCN3ParserTokenTypes.TypeDef:
			correspondingTypePrettyName = "Type";
			break;
		case TTCN3ParserTokenTypes.GroupDef:
			correspondingTypePrettyName = "Group";
			break;
		case TTCN3ParserTokenTypes.TTCN3ModuleId:
			correspondingTypePrettyName = "Module";
			break;
		default:
			break;
		}
		return correspondingTypePrettyName;
	}
	// -------------------------------------------------------------------------------------------

	/**
	 * @param node - a module or a group node
	 * @return a list of the identifier nodes of all module definitions within the module or group
	 */

	public static ArrayList<LocationAST> getModuleOrGroupLevelIdentifiers(LocationAST moduleOrGroupNode){
		ArrayList<LocationAST> identifierNodesList = new ArrayList<LocationAST>();
		if ((moduleOrGroupNode.getType()==TTCN3ParserTokenTypes.TTCN3Module) || (moduleOrGroupNode.getType() == TTCN3ParserTokenTypes.GroupDef)){
			for (LocationAST moduleDefinitionNode : ASTUtil.findTypeNodes(moduleOrGroupNode, TTCN3ParserTokenTypes.ModuleDefinition)){
				ArrayList<LocationAST> moduleDefinitionIdentifiersList = LocationAST.getModuleDefinitionIdentifiersList(moduleDefinitionNode);
				for (LocationAST moduleDefinitionIdentifier : moduleDefinitionIdentifiersList){
					identifierNodesList.add(moduleDefinitionIdentifier);
					if (moduleDefinitionNode.getFirstChild().getType()==TTCN3ParserTokenTypes.GroupDef){
						identifierNodesList.addAll(getModuleOrGroupLevelIdentifiers(moduleDefinitionNode.getFirstChild()));
					}
				}
			}
		}
		return identifierNodesList;
	}

	/**
	 * @param moduleNode - a module node
	 * @param type - definition type
	 * @return a list of the identifier nodes of all module definitions of type <b>type</b> within the module
	 */

	public static ArrayList<LocationAST> getModuleLevelIdentifiersOfType(LocationAST moduleNode, int type){
		ArrayList<LocationAST> identifierNodesList = new ArrayList<LocationAST>();
		if (moduleNode.getType() == TTCN3ParserTokenTypes.TTCN3Module){
			for (LocationAST moduleDefinitionNode : ASTUtil.findTypeNodes(moduleNode, TTCN3ParserTokenTypes.ModuleDefinition)){
				if (moduleDefinitionNode.getFirstChild().getType() == type){
					ArrayList<LocationAST> moduleDefinitionIdentifiersList = LocationAST.getModuleDefinitionIdentifiersList(moduleDefinitionNode);
					for (LocationAST moduleDefinitionIdentifier : moduleDefinitionIdentifiersList){
						identifierNodesList.add(moduleDefinitionIdentifier);
					}
					if (type == TTCN3ParserTokenTypes.GroupDef){
						identifierNodesList.addAll(LocationAST.getModuleLevelIdentifiersOfType(moduleDefinitionNode.getNthChild(2).getNextSibling(), type));
					}
				}
			}
		}
		return identifierNodesList;
	}

	/**
	 * @param node
	 *            - a module definition node or a module control body node
	 * @return a list of the local definition identifier nodes within that
	 *         module definition or module control body, null if node is not a module definition or module control body node
	 */

	public static ArrayList<LocationAST> getLocalDefinitionsIdentifiersList(
			LocationAST node) {
		//TODO: add control part handling
		ArrayList<Integer> allowedTypes = new ArrayList<Integer>();
		if (node.getType() == TTCN3ParserTokenTypes.ModuleDefinition) {

			allowedTypes.add(TTCN3ParserTokenTypes.FunctionDef);
			allowedTypes.add(TTCN3ParserTokenTypes.AltstepDef);
			allowedTypes.add(TTCN3ParserTokenTypes.TestcaseDef);
			allowedTypes.add(TTCN3ParserTokenTypes.TypeDef);
			if (!allowedTypes.contains(LocationAST	.getModuleDefinitionTypeNode(node)
													.getType())) {
				return null;
			}
			if (LocationAST.getModuleDefinitionTypeNode(node).getType() == TTCN3ParserTokenTypes.TypeDef) {
				LocationAST componentDefNode = (LocationAST) ASTUtil.findChild(node,
						TTCN3ParserTokenTypes.ComponentDef);
				if (componentDefNode == null) {
					return null;
				}
			}
		} else {
			if (node.getType()!=TTCN3ParserTokenTypes.ModuleControlBody) {
				return null;
			}
		}

		ArrayList<LocationAST> identifierNodesList = new ArrayList<LocationAST>();

		identifierNodesList.addAll(collectLocalDefinitionIdentifiersList(node, TTCN3ParserTokenTypes.SingleVarInstance));
		identifierNodesList.addAll(collectLocalDefinitionIdentifiersList(node, TTCN3ParserTokenTypes.SingleTempVarInstance));
		identifierNodesList.addAll(collectLocalDefinitionIdentifiersList(node, TTCN3ParserTokenTypes.SingleConstDef));
		identifierNodesList.addAll(collectLocalDefinitionIdentifiersList(node, TTCN3ParserTokenTypes.SingleTimerInstance));
		identifierNodesList.addAll(collectLocalDefinitionIdentifiersList(node, TTCN3ParserTokenTypes.PortElement));
		identifierNodesList.addAll(collectLocalDefinitionIdentifiersList(node, TTCN3ParserTokenTypes.BaseTemplate));
		return identifierNodesList;
	}

	/**
	 * @param node - module definition node (or any parent node which may contain local definitions)
	 * @param lowLevelLocalDefinitionType - low level local definition type code (e.g. PortElement, SingleTimerInstance, etc.)
	 * @return
	 */
	private static ArrayList<LocationAST> collectLocalDefinitionIdentifiersList(LocationAST node, int lowLevelLocalDefinitionType) {
		ArrayList<LocationAST> identifierNodesList = new ArrayList<LocationAST>();
		LinkedList<LocationAST> localDefLowlevelNodes = ASTUtil.findTypeNodes(node,
				lowLevelLocalDefinitionType);
		for (LocationAST localDefLowlevelNode : localDefLowlevelNodes) {
			if (lowLevelLocalDefinitionType == TTCN3ParserTokenTypes.BaseTemplate) {
				identifierNodesList.add(localDefLowlevelNode.getFirstChild().getNextSibling());
			} else {
				identifierNodesList.add(localDefLowlevelNode.getFirstChild());
			}
		}

		return identifierNodesList;

	}



	/**
	 * Dumps a subtree starting at the given node
	 * @param node - start node
	 * @param depth - starting depth
	 * @param output - the carry-over output
	 * @return - the output
	 */
	public static String dumpTree(LocationAST node, int depth, String output) {
		String spacing = "";
		for (int i = 0; i< depth; i++) {
			spacing+="|  ";
		}
		//System.out.println(spacing+""+node.toString());
		output+=spacing+""+node.toString()+"\n";

		if (node.getFirstChild()!=null) {
			depth++;
			LocationAST firstChild = node.getFirstChild();
			output = dumpTree(firstChild, depth, output);
			LocationAST nextSibling = firstChild.getNextSibling();
			while (nextSibling!=null) {
				output = dumpTree(nextSibling, depth, output);
				nextSibling = nextSibling.getNextSibling();
			}
			depth--;
		}
		return output;
	}


}
