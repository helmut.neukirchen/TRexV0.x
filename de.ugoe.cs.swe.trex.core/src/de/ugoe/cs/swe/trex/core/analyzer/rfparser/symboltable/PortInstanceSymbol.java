package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;

public class PortInstanceSymbol extends Symbol {

	@Override
	public String getSymbolDescription() {
		StringBuffer result = new StringBuffer();
		HTMLPrinter.getInstance().addPageProlog(result);
		HTMLPrinter.getInstance().addSmallHeader(result, "Port Instance: ");
		HTMLPrinter.getInstance().addLine(result, getName());
		
		if (getSurroundingScope().getScopeSymbol() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Scope of:");
			HTMLPrinter.getInstance().addLine(result, getSurroundingScope().getScopeSymbol().getSymbolDescriptionPlain());
		}
		
		HTMLPrinter.getInstance().addPageEpilog(result);
		
		return result.toString();
	}

	@Override
	public String getSymbolDescriptionContentAssist() {
		return getName() + " - port" ;
	}

}
