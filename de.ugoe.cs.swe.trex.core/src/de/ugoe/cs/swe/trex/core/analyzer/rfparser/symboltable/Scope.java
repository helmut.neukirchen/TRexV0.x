package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;
import java.util.Map.Entry;

import de.ugoe.cs.swe.trex.core.TTCN3CorePlugin;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration.AbstractImport;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration.AbstractKindImport;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration.AbstractKindImport.KindEnum;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration.GroupExceptImport;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration.GroupImport;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration.ImportConfiguration;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration.KindImport;
import de.ugoe.cs.swe.trex.core.misc.StringIgnoreCaseComparator;


public class Scope {
	public static String debugVal = null;

	public static boolean debug = ((debugVal != null) && (debugVal.equalsIgnoreCase("true")));

	private SymbolTable symbolTable = new SymbolTable();

	private Scope parent = null;

	private List<Scope> children = new LinkedList<Scope>();

	private List<Scope> knownScopes = new LinkedList<Scope>();

	private boolean isSystemScope = false;

	private Symbol scopeSymbol = null;

	private ImportConfiguration importConfiguration = null;

	private boolean searchParent = true;

	private int startOffset = -1;

	private int endOffset = -1;

	private int scopeLevel = 0;

	private String associatedFilename = null;

	private HashMap<String, TypeSymbol> referencedSymbols = null;

	// ---------------------------------------------------------------------------

	public Scope() {
		// empty constructor
	}

	// ---------------------------------------------------------------------------

	public Scope(Scope parent, boolean searchParent) {
		this.parent = parent;
		this.searchParent = searchParent;
	}

	// ---------------------------------------------------------------------------

	public String getAssociatedFilename() {
		return associatedFilename;
	}

	// ---------------------------------------------------------------------------

	public void setAssociatedFilename(String associatedFilename) {
		this.associatedFilename = associatedFilename;
	}

	// ---------------------------------------------------------------------------

	public int getScopeLevel() {
		return scopeLevel;
	}

	// ---------------------------------------------------------------------------

	public void setScopeLevel(int scopeLevel) {
		this.scopeLevel = scopeLevel;
	}

	// ---------------------------------------------------------------------------

	public int getEndOffset() {
		return endOffset;
	}

	// ---------------------------------------------------------------------------

	public void setEndOffset(int endOffset) {
		this.endOffset = endOffset;
	}

	// ---------------------------------------------------------------------------

	public int getStartOffset() {
		return startOffset;
	}

	// ---------------------------------------------------------------------------

	public void setStartOffset(int startOffset) {
		this.startOffset = startOffset;
	}

	// ---------------------------------------------------------------------------

	public Symbol getScopeSymbol() {
		return scopeSymbol;
	}

	// ---------------------------------------------------------------------------

	public void setScopeSymbol(Symbol scopeSymbol) {
		this.scopeSymbol = scopeSymbol;
	}

	// ---------------------------------------------------------------------------

	public boolean isSearchParent() {
		return searchParent;
	}

	// ---------------------------------------------------------------------------

	public void setSearchParent(boolean searchParent) {
		this.searchParent = searchParent;
	}

	// ---------------------------------------------------------------------------

	public List<Scope> getChildren() {
		return children;
	}

	// ---------------------------------------------------------------------------

	public void addChild(Scope scope) {
		children.add(scope);
	}

	// ---------------------------------------------------------------------------

	public SymbolTable getSymbolTable() {
		return symbolTable;
	}

	// ---------------------------------------------------------------------------

	public void setSymbolTable(SymbolTable symbolTable) {
		this.symbolTable = symbolTable;
	}

	// ---------------------------------------------------------------------------

	public Scope getParent() {
		return parent;
	}

	// ---------------------------------------------------------------------------

	public void setParent(Scope parent) {
		this.parent = parent;
	}

	// ---------------------------------------------------------------------------

	public List<Scope> getKnownScopes() {
		return knownScopes;
	}

	// ---------------------------------------------------------------------------

	public void addKnownScope(Scope s) {
		knownScopes.add(s);
	}

	// ---------------------------------------------------------------------------

	private Symbol findTransitiveReference(String s) {
		final Map<String, Symbol> symbols = symbolTable.getSymbols();

		if (referencedSymbols == null) {
			referencedSymbols = new HashMap<String, TypeSymbol>();

			for (Map.Entry<String, Symbol> entry : symbols.entrySet()) {
				Symbol item = entry.getValue();
				if (item instanceof TypeSymbol) {
					TypeSymbol typeSymb = (TypeSymbol) item;
					if (typeSymb.getReferencedType() != null) {
						referencedSymbols.put(typeSymb.getReferencedType().getIdentifier().getName(), typeSymb);
					}
				}
			}
		}
		TypeSymbol symbolFound = referencedSymbols.get(s);
		if (symbolFound == null)
			return null;

		Scope scope = symbolFound.getSurroundingScope();
		Symbol symbol = scope.resolve(s);

		return symbol;
	}

	// ---------------------------------------------------------------------------

	private Symbol resolveSkipModuleLookup(String s) {
		if (s == null)
			return null;

		// look directly in symboltable
		Symbol symbol = resolveDirect(s);

		if (symbol != null)
			return symbol;

		// look in known scopes
		symbol = resolveKnownScopes(s);

		if(symbol != null)
			return symbol;

		// look in parent
		symbol = resolveParent(s);

		if(symbol != null)
			return symbol;

		return findTransitiveReference(s);
	}

	// ---------------------------------------------------------------------------

	public Symbol resolve(String s, boolean resolveSystem) {
		if (s == null)
			return null;

		// look directly
		Symbol symbol = resolveDirect(s);

		if (symbol != null)
			return symbol;

		// look in known scopes
		boolean scopeFound = false;
		for (int i = 0; i < knownScopes.size(); i++) {
			final Scope scope = knownScopes.get(i);
			if (resolveSystem && scope.isSystemScope() || !resolveSystem && !scope.isSystemScope()) {
				scopeFound = true;
				symbol = scope.symbolTable.resolve(s);
				if (symbol != null) {
					return symbol;
				}
			}
			// if last element and scope not yet found try and resolve anyway
			if (((i+1) >= knownScopes.size()) && (scopeFound == false)) {
				symbol = scope.symbolTable.resolve(s);
				if (symbol != null) {
					return symbol;
				}
			}
		}

		// look in parent
		if (parent != null && searchParent == true) {
			return parent.resolve(s, resolveSystem);
		}

		// look in module
		return resolveModule(s);
	}

	// ---------------------------------------------------------------------------

	public Symbol resolve(String s) {
		if (s == null)
			return null;

		// look directly in symboltable
		Symbol symbol = resolveDirect(s);

		if (symbol != null)
			return symbol;

		// look in known scopes
		symbol = resolveKnownScopes(s);

		if(symbol != null)
			return symbol;

		// look in parent
		symbol = resolveParent(s);

		if(symbol != null)
			return symbol;

		// look in module
		return resolveModule(s);
	}

	// ---------------------------------------------------------------------------

	private Symbol resolveDirect(String s) {
		if (symbolTable == null)
			return null;

		TTCN3CorePlugin.debugMessageSymbolResolver("resolving in direct scope");
		return symbolTable.resolve(s);
	}

	private Symbol resolveKnownScopes(String s) {
		Symbol symbol;
		for (Scope scope : knownScopes) {
			TTCN3CorePlugin.debugMessageSymbolResolver("resolving in known scope");
			if (scope != null) {
				symbol = scope.symbolTable.resolve(s);
				if (symbol != null) {
					return symbol;
				}
			}
		}
		return null;
	}

	private Symbol resolveParent(String s) {
		if (parent == null || searchParent == false)
			return null;

		// search in parent
		TTCN3CorePlugin.debugMessageSymbolResolver("resolving parent");
		return parent.resolve(s);
	}

	//TODO: this is largely copied from resolveModule, consider merging back!
	private Symbol resolveImportedImports(String s, String currentModuleName){
		TTCN3CorePlugin.debugMessageSymbolResolver("resolving imported imports");
		if (importConfiguration!=null){
			for (AbstractImport imp : importConfiguration.getImports()) {
				if (isImportImportAllowed(imp, currentModuleName)){
					Scope moduleScope = TTCN3AnalyzerFlyweightFactory.getInstance().getScopeByModuleName(imp.getModuleName(), this.getAssociatedFilename());
					if (moduleScope != null) {
						Symbol symbol = moduleScope.resolveSkipModuleLookup(s);
						if (isSymbolImportAllowed(symbol, imp)) {
							return symbol;
						} else {
							if (imp instanceof KindImport) {
								KindImport kImp = (KindImport) imp;

								for (AbstractKindImport akImp : kImp.getKinds()) {
									if (akImp instanceof GroupImport) {
										GroupImport gImp = (GroupImport) akImp;
										boolean nameFound = false;
										for (GroupExceptImport gImport : gImp.getRefList()) {
											if (gImport.getGroupName().contains(s))
												nameFound = true;
										}

										if ((nameFound == true) && (symbol instanceof GroupSymbol) && (symbol.getName().equals(s)) ) {
											return symbol;
										}
									} else if (akImp.getType()==KindEnum.ttcn3_import){

										symbol = moduleScope.resolveImportedImports(s, currentModuleName);
										if(symbol != null)
											return symbol;
									}
								}
							}
						}
					}
				}
			}
		}
		return null;
	}

	private Symbol resolveModule(String s) {
		Scope moduleScope = TTCN3AnalyzerFlyweightFactory.getInstance().getScopeByModuleName("trexPredefinedFunctions", this.getAssociatedFilename());
		Symbol symbol;
		if (moduleScope != null) {
			TTCN3CorePlugin.debugMessageSymbolResolver("resolving in predefined functions");
			symbol = moduleScope.resolveSkipModuleLookup(s);
			if (symbol != null)
				return symbol;
		}

		if (importConfiguration != null) {
			for (AbstractImport imp : importConfiguration.getImports()) {
				moduleScope = TTCN3AnalyzerFlyweightFactory.getInstance().getScopeByModuleName(imp.getModuleName(), this.getAssociatedFilename());
				if (moduleScope != null) {
					TTCN3CorePlugin.debugMessageSymbolResolver("resolving in imported module");
					symbol = moduleScope.resolveSkipModuleLookup(s);

					if (isSymbolImportAllowed(symbol, imp)) {
						return symbol;
					} else {
						if (imp instanceof KindImport) {
							KindImport kImp = (KindImport) imp;

							for (AbstractKindImport akImp : kImp.getKinds()) {
								if (akImp instanceof GroupImport) {
									GroupImport gImp = (GroupImport) akImp;
									boolean nameFound = false;
									for (GroupExceptImport gImport : gImp.getRefList()) {
										if (gImport.getGroupName().contains(s))
											nameFound = true;
									}

									if ((nameFound == true) && (symbol instanceof GroupSymbol) && (symbol.getName().equals(s)) ) {
										return symbol;
									}
								} else if (akImp.getType()==KindEnum.ttcn3_import){

									symbol = moduleScope.resolveImportedImports(s, this.getModuleScope().getScopeSymbol().getName());
									if(symbol != null)
										return symbol;
								}
							}
						}
					}

					symbol = null;
				}
			}
		}

		moduleScope = TTCN3AnalyzerFlyweightFactory.getInstance().getScopeByModuleName(s, this.getAssociatedFilename());
		if (moduleScope != null) {
			Symbol scopeSym = moduleScope.getScopeSymbol();

			if (scopeSym != null)
				return scopeSym;
		}

		return null;
	}

	// ---------------------------------------------------------------------------
	public Symbol resolve(LocationAST node) {
		if (node == null) {
			return null;
		}

		// optimisation: don't generate the toString unless we plan to use it
		String nodeStr = debug ? node.toString() : "";

		TTCN3CorePlugin.debugMessageSymbolResolver("resolving node: " + nodeStr);
		long time = System.currentTimeMillis();
		Symbol result = null;

		if (LocationAST.isExtendedReference(node)) {
			TTCN3CorePlugin.debugMessageSymbolResolver("Resolving Extended Reference: " + nodeStr);
			try {
				result = resolveExtendedReferenceNode(node);
			} catch (IllegalArgumentException ia) {
//				//TTCN3CorePlugin.debugMessage(ia.getMessage());
				result = null;
			}
			TTCN3CorePlugin.debugMessageSymbolResolverTimer("...finished in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");
		} else if (LocationAST.isPortReference(node)) {
			TTCN3CorePlugin.debugMessageSymbolResolver("Resolving Port Reference: " + nodeStr);
			result = resolvePortReferenceNode(node);
			TTCN3CorePlugin.debugMessageSymbolResolverTimer("...finished in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");
		} else if (LocationAST.isFieldReference(node)) {
			TTCN3CorePlugin.debugMessageSymbolResolver("Resolving Template Field Reference: " + nodeStr);
			result = resolveFieldReference(node);
			TTCN3CorePlugin.debugMessageSymbolResolverTimer("...finished in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");
		} else if (LocationAST.isEnumeratedValue(node)) {
			TTCN3CorePlugin.debugMessageSymbolResolver("Resolving Enumerated Value: " + nodeStr);
			result = resolveEnumeratedValueNode(node);
			TTCN3CorePlugin.debugMessageSymbolResolverTimer("...finished in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");
		} else if (LocationAST.isActualParameterAssignment(node)){
			TTCN3CorePlugin.debugMessageSymbolResolver("Resolving Actual Parameter Assignment: " + nodeStr);
			result = resolveActualParameterAssignment(node);
			TTCN3CorePlugin.debugMessageSymbolResolverTimer("...finished in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");
		} else if (LocationAST.isFieldDefTypeDef(node)){
			TTCN3CorePlugin.debugMessageSymbolResolver("Resolving Field Definition Type Definition: " + nodeStr);
			result = resolveFieldDefTypeDef(node);
			TTCN3CorePlugin.debugMessageSymbolResolverTimer("...finished in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");
		}

		if (result != null)
			return result;

		if (node.getScope() != null) {
			TTCN3CorePlugin.debugMessageSymbolResolver("Resolving default: " + nodeStr);
			Scope scope = node.getScope();
			Symbol symb = scope.resolve(node.getText());
			TTCN3CorePlugin.debugMessageSymbolResolverTimer("...finished in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");
			if (symb != null)
				return symb;
		}

		//TODO: try to resolve enumerated values




		return null;
	}

	// ---------------------------------------------------------------------------

	//TODO: needs more robustness
	private LocationAST findEnumReferenceInBooleanExpression(
			LocationAST enumNode) {
		// TODO: consider resolving to factor instead
		LocationAST identifierNode = null;
		LocationAST contextRootNode = null;

		// simple binary comparison operators
		// TODO: add function calls to the mix
		// this should never be triggered, but just in case
		contextRootNode = LocationAST.resolveParentsUntilType(enumNode,
				new int[] { TTCN3ParserTokenTypes.MORETHAN,
						TTCN3ParserTokenTypes.LESSTHAN,
						TTCN3ParserTokenTypes.EQUAL,
						TTCN3ParserTokenTypes.MOREOREQUAL,
						TTCN3ParserTokenTypes.LESSOREQUAL,
						TTCN3ParserTokenTypes.NOTEQUALS });
		if (contextRootNode != null) {
			LocationAST factorNode = contextRootNode.getFirstChild();
			identifierNode = (LocationAST) ASTUtil.findChild(factorNode,
					TTCN3ParserTokenTypes.IDENTIFIER);
			if (identifierNode == enumNode) {
				factorNode = factorNode.getNextSibling();
				identifierNode = (LocationAST) ASTUtil.findChild(factorNode,
						TTCN3ParserTokenTypes.IDENTIFIER);
			}
		}
		//TODO: kinda weak, look for better ways to implement this
		if (loopLock!=null && identifierNode == loopLock) {
			//loopLock = null;
			identifierNode = null;
		} else {
			loopLock = enumNode;
		}
		return identifierNode;

	}
	private static LocationAST loopLock = null;

	private Symbol resolveEnumeratedReturnType(LocationAST enumNode) {
		LocationAST contextRootNode = LocationAST.resolveParentsUntilType(enumNode,
				new int[] {TTCN3ParserTokenTypes.ReturnStatement});
		//this should never be triggered, but just in case
		if (contextRootNode != null) {
			LocationAST containingDefNode = LocationAST.resolveParentsUntilType(contextRootNode, new int[]{TTCN3ParserTokenTypes.FunctionDef, TTCN3ParserTokenTypes.AltstepDef});
			LocationAST returnTypeNode = (LocationAST) ASTUtil.findSibling(containingDefNode.getFirstChild(), TTCN3ParserTokenTypes.ReturnType);
			if (returnTypeNode==null) {
				return null;
			}
			LocationAST returnTypeIdentifierNode = (LocationAST) ASTUtil.findChild(returnTypeNode, TTCN3ParserTokenTypes.IDENTIFIER);
			if (returnTypeIdentifierNode!=null && returnTypeIdentifierNode.getSymbol()!=null && returnTypeIdentifierNode.getSymbol().getScope()!=null) {
				Symbol symb = returnTypeIdentifierNode.getSymbol().getScope().resolve(enumNode.getText());
				return symb;
			}
		}
		return null;

	}

	private LocationAST findEnumReferenceInActualPar(LocationAST enumNode) {
		//formal parameters
		LocationAST contextRootNode = LocationAST.resolveParentsUntilType(enumNode,
				new int[] {TTCN3ParserTokenTypes.FunctionActualPar,
						TTCN3ParserTokenTypes.TestcaseActualPar,
						TTCN3ParserTokenTypes.TemplateActualPar});
		//this should never be triggered, just in case
		if (contextRootNode != null) {
			//get the index of the formal parameter
			int actualParIndex = 0;
			LocationAST sibling = contextRootNode.getParent().getFirstChild();
			while (sibling != contextRootNode) {
				actualParIndex++;
				//TODO: check type and check null
				sibling = sibling.getNextSibling();
				if (sibling == null) {
//					System.out.println("@@@@@@@@@@@@ ERROR! Could not determine actual parameter index!");
				}
			}
			LocationAST relatedIdentifier = contextRootNode.getNthParent(2).getNthChild(3);
			if (relatedIdentifier!=null){
				//TODO: when is the relatedIdentifier == 0?
				//System.out.println(":::::"+relatedIdentifier + " | "+contextRootNode);
				Symbol s = relatedIdentifier.getSymbol();
				if (s!=null) {
					LocationAST declarationNode = s.getDeclarationNode();
					if (declarationNode!=null) {
						LocationAST identifierNode = declarationNode.getParent();
						LinkedList<LocationAST> formalParameterNodes = new LinkedList<LocationAST>();
						LocationAST siblingNode = identifierNode.getNextSibling();
						while (siblingNode!=null && LocationAST.isOfType(siblingNode, new int[] {
								TTCN3ParserTokenTypes.FormalValuePar,
								TTCN3ParserTokenTypes.FormalTimerPar,
								TTCN3ParserTokenTypes.FormalTemplatePar,
								TTCN3ParserTokenTypes.FormalPortPar })) {
							formalParameterNodes.add(siblingNode);
							siblingNode = siblingNode.getNextSibling();
						}
						if (!(formalParameterNodes.size() > actualParIndex)) {
							return null;
						}
						LocationAST formalParameterNode = formalParameterNodes.get(actualParIndex);;
						if (formalParameterNode == null) {
							return null;
						}

						//TODO: make robust!!!
						//TODO: clean up code and make robustness a must!!!!!!
						LocationAST firstChild = formalParameterNode.getFirstChild();
						LocationAST nextSibling = firstChild.getNextSibling();
						LocationAST resultingNode = nextSibling.getFirstChild();

						return resultingNode;
					}
				}
			}
		}
		return null;
	}


	// ---------------------------------------------------------------------------

	private LocationAST findEnumReferenceInSelectCase(LocationAST enumNode) {
		LocationAST contextRootNode = LocationAST.resolveParentsUntilType(enumNode,
				new int[] {TTCN3ParserTokenTypes.SelectCase});
		//this should never be triggered, but just in case
		if (contextRootNode != null) {
			LocationAST selectExpressionIdentifier = (LocationAST) ASTUtil.findChild(contextRootNode.getNthParent(2), TTCN3ParserTokenTypes.IDENTIFIER);
			return selectExpressionIdentifier;
		}
		return null;
	}

	// ---------------------------------------------------------------------------

	private Symbol resolveEnumeratedValueNode(LocationAST node) {
		if (node.getScope() == null)
			return null;

		LocationAST contextRootNode = null;
		//simple context of assignments and template fields
		contextRootNode = LocationAST.resolveParentsUntilType(node, new int[] {TTCN3ParserTokenTypes.FieldSpec, TTCN3ParserTokenTypes.Assignment});
		LocationAST enumRefNode = null;
		if (contextRootNode == null) {
			//Try default resolution
			Scope scope = node.getScope();
			Symbol symb = scope.resolve(node.getText());
			if (symb != null) {
				//let default mechanism handle it
				return null;
			}

			//TODO: add imports (and) visibility checks
			//also investigate possible performance hit
			contextRootNode = LocationAST.resolveParentsUntilType(node,
					new int[] { TTCN3ParserTokenTypes.ReturnStatement,
							TTCN3ParserTokenTypes.SelectCase,
							TTCN3ParserTokenTypes.FunctionActualPar,
							TTCN3ParserTokenTypes.TemplateActualPar,
							TTCN3ParserTokenTypes.TestcaseActualPar,
							TTCN3ParserTokenTypes.BooleanExpression,
							TTCN3ParserTokenTypes.SingleVarInstance,
							TTCN3ParserTokenTypes.FormalValuePar,
							TTCN3ParserTokenTypes.ModulePar});
			if (contextRootNode!=null) {
				switch (contextRootNode.getType()) {
				case TTCN3ParserTokenTypes.ReturnStatement:
					// perform direct resolution, skip rest
					symb = resolveEnumeratedReturnType(node);
					return symb;
				case TTCN3ParserTokenTypes.SelectCase:
					enumRefNode = findEnumReferenceInSelectCase(node);
					break;
				case TTCN3ParserTokenTypes.BooleanExpression:
					enumRefNode = findEnumReferenceInBooleanExpression(node);
					// handle function call contexts
					if (enumRefNode!=null && enumRefNode.getSymbol() instanceof FunctionSymbol) {

						LocationAST declarationNode = enumRefNode.getSymbol()
																	.getDeclarationNode();
						if (declarationNode != null) {
							LocationAST returnTypeNode = (LocationAST) ASTUtil.findSibling(declarationNode.getParent(),
									TTCN3ParserTokenTypes.ReturnType);
							if (returnTypeNode != null) {
								LocationAST returnTypeIdentifierNode = (LocationAST) ASTUtil.findChild(returnTypeNode,
										TTCN3ParserTokenTypes.IDENTIFIER);
								if (returnTypeIdentifierNode != null) {
									// check for visibility and conflicts
									// skip rest
									Symbol symbol = null;
									Symbol localTypeSymbol = node.getScope().resolve(returnTypeIdentifierNode.getText());
									Symbol returnTypeSymbol = returnTypeIdentifierNode.getSymbol();
									if (localTypeSymbol!=null && returnTypeSymbol!=null && localTypeSymbol==returnTypeSymbol && returnTypeSymbol.getScope()!=null) {
										symbol = returnTypeSymbol.getScope().resolve(node.getText());
									}
									return symbol;
								}
							}
						}
					}

					break;
				case TTCN3ParserTokenTypes.FunctionActualPar:
				case TTCN3ParserTokenTypes.TemplateActualPar:
				case TTCN3ParserTokenTypes.TestcaseActualPar:
					enumRefNode = findEnumReferenceInActualPar(node);
					break;
				case TTCN3ParserTokenTypes.SingleVarInstance:
					enumRefNode = (LocationAST) ASTUtil.findChild(contextRootNode, TTCN3ParserTokenTypes.IDENTIFIER);
					break;
				case TTCN3ParserTokenTypes.FormalValuePar:
					enumRefNode = (LocationAST) ASTUtil.findChild(contextRootNode.getFirstChild().getNextSibling(), TTCN3ParserTokenTypes.IDENTIFIER);
					break;
				case TTCN3ParserTokenTypes.ModulePar:
					LocationAST typeIdentifierNode = (LocationAST) ASTUtil.findChild(contextRootNode, TTCN3ParserTokenTypes.IDENTIFIER);
					Symbol typeSymbol = typeIdentifierNode.getSymbol();
					if (typeSymbol!=null && typeSymbol.getScope()!=null) {
						Symbol enumSymbol = typeSymbol.getScope().resolve(node.getText());
						return enumSymbol;
					} else {
						return null;
					}
				default:
					break;
				}
			}

		} else {
			enumRefNode = contextRootNode.getNthChild(3);
		}

		Symbol symb = node.getScope().resolve(enumRefNode);
		if (!(symb instanceof TypeSymbol))
			return null;

		TypeSymbol typeSymb = (TypeSymbol) symb;

		Scope lastScope = typeSymb.getSurroundingScope();

		if (typeSymb.getReferencedType() == null)
			return lastScope.resolve(node.getText());
		String referencedEnumType = typeSymb.getReferencedType().getIdentifier().getName();

		symb = lastScope.resolve(referencedEnumType);
		if (!(symb instanceof TypeSymbol))
			return null;

		//safety check
		Scope localScope = node.getScope();
		Symbol locallyResolvedSymbol = localScope.resolve(referencedEnumType);
		if (locallyResolvedSymbol == null || symb == null || locallyResolvedSymbol != symb) {
			return null;
		}
		typeSymb = (TypeSymbol) symb;

		lastScope = typeSymb.getScope();

		// FIXME: sometimes get NPE on lastScope with templates, and references
		// that exist are not found...
		if(lastScope == null)
			return null;
		return lastScope.resolve(node.getText());
	}

	// XXX: slow?
	private Symbol resolveActualParameterAssignment(LocationAST node) {
		LocationAST instanceNode = LocationAST.resolveParentsUntilType(node, new int[]{TTCN3ParserTokenTypes.FunctionInstance, TTCN3ParserTokenTypes.AltstepInstance, TTCN3ParserTokenTypes.TestcaseInstance});
		LocationAST identifierNode = (LocationAST) ASTUtil.findChild(instanceNode, TTCN3ParserTokenTypes.Identifier);
		Symbol instanceSymbol = identifierNode.getFirstChild().getSymbol();

		if (instanceSymbol != null && instanceNode != instanceSymbol.getDeclarationNode()){
			LocationAST defNode = LocationAST.resolveParentsUntilType(instanceSymbol.getDeclarationNode(), new int[]{TTCN3ParserTokenTypes.FunctionDef, TTCN3ParserTokenTypes.AltstepDef, TTCN3ParserTokenTypes.TestcaseDef});
			if (defNode != null){
				Symbol targetSymbol = defNode.getScope().getSymbolTable().resolve(node.getText());
				if (targetSymbol != null && (targetSymbol instanceof FormalParameterSymbol)){
					return targetSymbol;
				}
			}
		}
		return null;

	}
	// ---------------------------------------------------------------------------

	// XXX: slow?
	private Symbol resolveFieldDefTypeDef(LocationAST node) {
		Symbol symbol = null;
		LocationAST fieldDefNode = LocationAST.resolveParentsUntilType(node, new int[]{TTCN3ParserTokenTypes.StructFieldDef,TTCN3ParserTokenTypes.UnionFieldDef});
		Scope fieldDefScope = fieldDefNode.getScope();
		while (fieldDefScope == null && fieldDefNode!=null) {
			fieldDefNode = LocationAST.resolveParentsUntilType(fieldDefNode.getParent(), new int[]{TTCN3ParserTokenTypes.StructFieldDef,TTCN3ParserTokenTypes.UnionFieldDef});
			if (fieldDefNode!=null) {
				fieldDefScope = fieldDefNode.getScope();
			} else {
				fieldDefScope = node.getScope();
			}
		}
		if (fieldDefScope !=null) {
			symbol = fieldDefScope.getParent().resolve(node.getText());
		}

		return symbol;

	}
	// ---------------------------------------------------------------------------


	// XXX: slow?
	private Symbol resolveFieldReference(LocationAST node) {
		Scope scope = node.getScope();

		if (scope == null)
			return null;

		Symbol symb = scope.resolve(node.getText());

		if (symb != null)
			return symb;

		LocationAST fieldSpecNode = LocationAST.resolveParentsUntilType(node, new int []{TTCN3LexerTokenTypes.FieldSpec,TTCN3LexerTokenTypes.FieldExpressionSpec });

		LocationAST fieldSpecNodeStart = fieldSpecNode;
		Stack<String> scopeLookupStack = new Stack<String>();

		try {
			int templateDefType = fieldSpecNode.getNthParent(3).getType();
			do {
				LocationAST identifierNode = fieldSpecNode.getNthChild(3);
				scopeLookupStack.push(identifierNode.getText());

				LocationAST fieldSpecNodeTmp = LocationAST.resolveParentsUntilType(fieldSpecNode.getParent(), new int[] {TTCN3LexerTokenTypes.FieldSpec, TTCN3LexerTokenTypes.FieldExpressionSpec});
				if (fieldSpecNodeTmp == null) {
					break;
				}
				fieldSpecNode = fieldSpecNodeTmp;
				templateDefType = fieldSpecNode.getNthParent(3).getType();
			} while ((templateDefType != TTCN3LexerTokenTypes.TemplateDef) && (templateDefType != TTCN3LexerTokenTypes.InLineTemplate));
		} catch (NullPointerException npe) {
			return null;
		}

		LocationAST fieldRefIdentifierNode;
		if (fieldSpecNode == fieldSpecNodeStart) {
			if (fieldSpecNode.getType() == TTCN3LexerTokenTypes.FieldExpressionSpec) {
				fieldRefIdentifierNode = LocationAST.resolveParentsUntilType(fieldSpecNode, new int[] {TTCN3LexerTokenTypes.SingleVarInstance});
				//	FIXME: this is bad! just a temporary fix to avoid exceptions
				if ((fieldRefIdentifierNode == null) || (fieldRefIdentifierNode.getFirstChild() == null))
					return null;
				fieldRefIdentifierNode = fieldRefIdentifierNode.getFirstChild().getFirstChild();
			} else {
				LocationAST parentTemplateDefNode = fieldSpecNode.getNthParent(3);

				if (parentTemplateDefNode.getType() == TTCN3ParserTokenTypes.SingleTempVarInstance){
					//template is instantiated locally
					parentTemplateDefNode = LocationAST.resolveParentsUntilType(parentTemplateDefNode, TTCN3ParserTokenTypes.VarInstance);
				}
				if ((parentTemplateDefNode.getFirstChild().getType() == TTCN3ParserTokenTypes.TemplateRestriction) || (parentTemplateDefNode.getFirstChild().getType() == TTCN3ParserTokenTypes.RestrictedTemplate)){
					//TODO: this seems to be the cause of the crash
					//but the crash seems to be gone, nonetheless leaving this in place just in case for now
					//the commented out debug statements are to be removed shortly
					try {
//						System.out.println(parentTemplateDefNode);
						fieldRefIdentifierNode = parentTemplateDefNode.getFirstChild().getNextSibling().getNthChild(4);
//						System.out.println(fieldRefIdentifierNode);
					} catch (Exception e) {
						fieldRefIdentifierNode = null;
//						System.err.println("Scope / Symbol Resolution Problem");
//						System.err.println("In: "+this);
//						System.err.println("Caused by: "+parentTemplateDefNode);
//						e.printStackTrace();
					}
				} else {
					//TODO: double check this is correct
					fieldRefIdentifierNode = (LocationAST) ASTUtil.findChild(parentTemplateDefNode, TTCN3ParserTokenTypes.Identifier);
				}
			}
		} else {
			if (fieldSpecNode.getType() == TTCN3LexerTokenTypes.FieldExpressionSpec) {
				fieldRefIdentifierNode = LocationAST.resolveParentsUntilType(fieldSpecNode, new int[] {TTCN3LexerTokenTypes.SingleVarInstance});
				//	FIXME: this is bad! just a temporary fix to avoid exceptions
				if ((fieldRefIdentifierNode == null) || (fieldRefIdentifierNode.getFirstChild() == null))
					return null;
				fieldRefIdentifierNode = fieldRefIdentifierNode.getFirstChild().getFirstChild();
			} else {
				fieldRefIdentifierNode = fieldSpecNode.getNthChild(3);
			}
		}

		if (fieldRefIdentifierNode == null)
			return null;

		symb = scope.resolve(fieldRefIdentifierNode);

		while (!scopeLookupStack.isEmpty()) {
			if (!(symb instanceof TypeSymbol))
				return null;

			TypeSymbol typeSymb = (TypeSymbol) symb;

			Scope lastScope = null;
			if (typeSymb.getReferencedType() != null) {
				lastScope = symb.getSurroundingScope();
				String referencedTypeName = typeSymb.getReferencedType().getIdentifier().getName();
				symb = lastScope.resolve(referencedTypeName);

				if (symb == null)
					return null;

				if (symb.getScope() != null) {
					lastScope = symb.getScope();
				} else if ( (symb.getScope() == null) && (symb.getSurroundingScope() != null) && (scopeLookupStack.size() >= 1)) {
					lastScope = symb.getSurroundingScope();
					Symbol tmp = lastScope.resolve(((TypeSymbol)symb).getReferencedType().identifier.getDeclarationNode());
					if (tmp == null) {
						lastScope = null;
					} else {
						lastScope = tmp.getScope();
					}
				} else {
					return null;
				}
			} else {
				lastScope = symb.getScope();
			}
			//FIXME: it should be interesting when this happens...
			if (lastScope == null)
				return null;

			symb = lastScope.resolve(scopeLookupStack.pop());
		}

		return symb;
	}

	// ---------------------------------------------------------------------------

	private Symbol resolvePortReferenceNode(LocationAST node) {
		LocationAST portRefNode = node.getNthParent(3);

		LocationAST refNode = portRefNode.getFirstChild().getFirstChild();

		if (node.getScope() == null)
			return null;

		Scope scope = node.getScope();
		Symbol symb = null;

		if (refNode.getType() == TTCN3LexerTokenTypes.SystemOp) {
			symb = scope.resolve(node.getText(), true);
		} else if (refNode.getType() == TTCN3LexerTokenTypes.SelfOp) {
			symb = scope.resolve(node.getText(), false);
		} else if (refNode.getType() == TTCN3LexerTokenTypes.VariableRef) {
			LocationAST identifierNode = refNode.getFirstChild().getFirstChild();

			symb = scope.resolve(identifierNode.getText());

			if (symb instanceof TypeSymbol) {
				TypeSymbol typeSymb = (TypeSymbol) symb;

				String referencedComponentName = typeSymb.getReferencedType().getIdentifier().getName();
				symb = typeSymb.getDeclarationNode().getScope().resolve(referencedComponentName);

				//FIXME: this is bad! just a temporary fix to avoid exceptions
				if ((symb == null) || (symb.getScope() == null))
					return null;

				Scope targetScope = symb.getScope();
				symb = targetScope.resolve(node.getText());
			} else {
				symb = null;
			}
		} else {
			symb = scope.resolve(node.getText());
		}

		return symb;
	}

	// ---------------------------------------------------------------------------

	/**
	 * FIXME: As mentioned below, we certainly should clarify which 'return
	 * null' statements are correct (i.e., when no resolution should occur and
	 * the returned result should be nothing) and which should be exceptions
	 * e.g., incorrect input that could not be resolved.
	 *
	 * @param node
	 * @return
	 */
	private Symbol resolveExtendedReferenceNode(LocationAST node) {
		LocationAST refNode = node.getNthParent(3); // VariableRef/referenced value/...
		if (refNode == null)
			throw new IllegalArgumentException("Couldn't get parent refNode for node");
		LocationAST extendedRefNode = null;

		Symbol mainIdentifierSymbol = null;
		LocationAST valueRefIdentifierNode = null;

		if (refNode.getType() == TTCN3LexerTokenTypes.ReferencedValue) {
			try {
				LocationAST valueRef = refNode.getFirstChild();
				LocationAST valueRefBody = valueRef.getFirstChild();
				extendedRefNode = valueRef.getNextSibling();
				LocationAST moduleIdentifierNode = valueRefBody.getFirstChild().getFirstChild();
				mainIdentifierSymbol = resolve(moduleIdentifierNode.getText());
				valueRefIdentifierNode = valueRefBody.getNextSibling().getFirstChild();
			} catch (NullPointerException npe) {
				throw new IllegalArgumentException("Malformed 'ReferencedValue' "
						+ ASTUtil.getDescendantText(refNode) + " @ offset " + refNode.getOffset() + " in " + associatedFilename);
			}

		} else { // VariableRef/...
			try {
				extendedRefNode = node.getParent().getParent();
//				LocationAST identifierNode = refNode.getFirstChild().getFirstChild();
				LocationAST identifierNode = (LocationAST) ASTUtil.findChild(refNode, TTCN3LexerTokenTypes.IDENTIFIER);
				LocationAST globalModuleNode = identifierNode.getNthParent(2);
				if (globalModuleNode.getType() == TTCN3LexerTokenTypes.GlobalModuleId){
					identifierNode = globalModuleNode.getNextSibling().getFirstChild();
				}
				mainIdentifierSymbol = resolve(identifierNode.getText());
			} catch (NullPointerException npe) {
				throw new IllegalArgumentException();
			}
		}

		if (!(mainIdentifierSymbol instanceof SubtypeSymbol))
			throw new IllegalArgumentException("mainIdentifierSymbol is not a SubtypeSymbol");

		SubtypeSymbol typeSymb = (SubtypeSymbol) mainIdentifierSymbol;
		Scope lastSymbolScope = mainIdentifierSymbol.getDeclarationNode().getScope();

		if (valueRefIdentifierNode != null) {
			Reference referencedType = typeSymb.getReferencedType();
			Symbol referencedTypeIdentifier = (referencedType == null) ? null : referencedType.getIdentifier();
			if (referencedTypeIdentifier == null)
				throw new IllegalArgumentException("could not resolve referencedTypeIdentifier");

			String referencedTypeName = referencedTypeIdentifier.getName();
			Symbol referencedTypeSymbol = lastSymbolScope.resolve(referencedTypeName);
			if (referencedTypeSymbol == null)
				throw new IllegalArgumentException("could not resolve referencedTypeSymbol");

			lastSymbolScope = referencedTypeSymbol.getDeclarationNode().getScope();
			lastSymbolScope = lastSymbolScope.resolve(referencedTypeName).getScope();

			if (lastSymbolScope == null)
				return null;

			typeSymb = (TypeSymbol) lastSymbolScope.resolve(valueRefIdentifierNode.getText());
			if (typeSymb == null)
				throw new IllegalArgumentException("could not resolve typeSymb");

			lastSymbolScope = typeSymb.getDeclarationNode().getScope();

			if ((extendedRefNode == null) || (valueRefIdentifierNode == node)) {
				return typeSymb;
			}
		}

		if(extendedRefNode == null)
			throw new IllegalArgumentException("extendedRefNode is null");

		final int numChildren = extendedRefNode.getNumberOfChildren();
		LocationAST child = extendedRefNode.getFirstChild();
		Symbol nextSymbol = null;

		for (int i=0; i < numChildren; i++) {
			Reference referencedType = typeSymb.getReferencedType();
			Symbol referencedTypeIdentifier = (referencedType == null) ? null : referencedType.getIdentifier();
			if (referencedTypeIdentifier == null)
				throw new IllegalArgumentException("could not resolve referencedTypeIdentifier");

			String referencedTypeName = referencedTypeIdentifier.getName();
			Symbol referencedTypeSymbol = lastSymbolScope.resolve(referencedTypeName);
			if (referencedTypeSymbol == null)
				throw new IllegalArgumentException("could not resolve referencedTypeSymbol");

			lastSymbolScope = referencedTypeSymbol.getDeclarationNode().getScope();

			String identifierName = child.getFirstChild().getText();
			nextSymbol = lastSymbolScope.resolve(referencedTypeName);

			if (identifierName == null)
				return null;

			if (identifierName.equals(node.getText()))
				break;

			if (nextSymbol == null) {
				typeSymb=null;
			} else {
				if (nextSymbol.getScope() == null)
					return null;

				typeSymb = (TypeSymbol) nextSymbol.getScope().resolve(identifierName);
			}

//			typeSymb = (nextSymbol == null) ? null : (TypeSymbol) nextSymbol.getScope().resolve(identifierName);
			if (typeSymb == null)
				throw new IllegalArgumentException("typeSymb is null");

			child = child.getNextSibling();
		}

		Scope finalScope = (nextSymbol == null) ? null : nextSymbol.getScope();

		return (finalScope == null) ? null : finalScope.resolve(node.getText());
	}

	// ---------------------------------------------------------------------------

	public Map<String, Symbol> getSymbols() {
		Map<String, Symbol> symbols = new TreeMap<String, Symbol>(new StringIgnoreCaseComparator());

		if ((parent != null) && (searchParent == true)) {
			symbols.putAll(parent.getSymbols());
		}

		if ((importConfiguration != null)) {
			//TODO: Handle imported imports here as well? -> Yes
			final List<AbstractImport> imports = importConfiguration.getImports();
			for (AbstractImport imp : imports) {
				Scope moduleScope = TTCN3AnalyzerFlyweightFactory.getInstance().getScopeByModuleName(imp.getModuleName(), this.getAssociatedFilename());
				if (moduleScope != null) {
					Map<String, Symbol> importedSymbols = moduleScope.getSymbols();

					for (Entry<String, Symbol> entry : importedSymbols.entrySet()) {
						if (isSymbolImportAllowed(entry.getValue(), imp)) {
							symbols.put(entry.getKey(), entry.getValue());

							if (imp instanceof GroupImport) {
								Symbol s = entry.getValue();
								Symbol s1 = s.getScope().getScopeSymbol();
								symbols.put(s.getGroupName(), s1);
							}
						}
					}
				}
			}
		}

		symbols.putAll(symbolTable.getSymbols());

		for (Scope scope : knownScopes) {
			symbols.putAll(scope.getSymbols());
		}

		return symbols;
	}


	// ---------------------------------------------------------------------------

	//pretty much duplicates isSymbolImportAllowed
	public boolean isImportImportAllowed(AbstractImport imp, String currentModuleName) {
		boolean allowed = false;
		if (imp.isPrivate()){
			allowed = false;
		} else if (imp.isFriend()){

			Symbol s = this.getScopeSymbol();

			if (s != null){
				ModuleSymbol importedModuleSymbol = (ModuleSymbol) s;
				if (importedModuleSymbol.isFriendModule(currentModuleName)){
					allowed = true;
				}
			}


		} else if (imp.isPublic()){
			allowed = true;
		} else {
			//TODO: is it possible???
			//=> for debugging
			TTCN3CorePlugin.debugMessage("FIXME: (\""+imp+"\") Import visibility mode not set!");
		}
		return allowed;
	}

	// ---------------------------------------------------------------------------

	public boolean isSymbolImportAllowed(Symbol s, AbstractImport imp) {
		boolean allowed = false;
		if (s != null){
			if (imp.isAllowed(s, symbolToKind(s))){
				if (s.isPrivate()){
					allowed = false;
				} else if (s.isFriend()){
					String currentModuleName = getModuleScope().getScopeSymbol().getName(); //current module name
					ModuleSymbol declaringModuleSymbol = (ModuleSymbol) s.getSurroundingScope().getModuleScope().getScopeSymbol();
					if (declaringModuleSymbol.isFriendModule(currentModuleName)){
						allowed = true;
					}

				} else if (s.isPublic()){
					allowed = true;
				} else {
					//TODO: is it possible???
					//=> for debugging
					TTCN3CorePlugin.debugMessage("FIXME: (\""+s+"\") Symbol visibility mode not set!");
				}
			}
		}

		return allowed;
	}

	// ---------------------------------------------------------------------------

	public AbstractKindImport.KindEnum symbolToKind(Symbol s) {
		if (s instanceof ConstantSymbol) {
			return AbstractKindImport.KindEnum.ttcn3_const;
		} else if (s instanceof AltstepSymbol) {
			return AbstractKindImport.KindEnum.ttcn3_altstep;
		} else if (s instanceof FunctionSymbol) {
			return AbstractKindImport.KindEnum.ttcn3_function;
		} else if (s instanceof GroupSymbol) {
			return AbstractKindImport.KindEnum.ttcn3_group;
		} else if (s instanceof ModuleParameterSymbol) {
			return AbstractKindImport.KindEnum.ttcn3_modulepar;
		} else if (s instanceof SignatureSymbol) {
			return AbstractKindImport.KindEnum.ttcn3_signature;
		} else if (s instanceof TemplateSymbol) {
			return AbstractKindImport.KindEnum.ttcn3_template;
		} else if (s instanceof TestcaseSymbol) {
			return AbstractKindImport.KindEnum.ttcn3_testcase;
		} else if (s instanceof TypeSymbol) {
			return AbstractKindImport.KindEnum.ttcn3_type;
		}
		return null;
	}

	// ---------------------------------------------------------------------------

	public Symbol addSymbol(Symbol s) {
		if (s.getName() != null) {
			s.setSurroundingScope(this);
			symbolTable.put(s.getName(), s);
		}

		return s;
	}

	// ---------------------------------------------------------------------------

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("associated filename: ").append(getAssociatedFilename()).append("\n");
		result.append(" scope start offset: ").append(getStartOffset()).append("\n");
		result.append("   scope end offset: ").append(getEndOffset()).append("\n");
		result.append("        scope level: ").append(getScopeLevel()).append("\n");
		if (getScopeSymbol()!=null) {
			result.append("       scope symbol: ").append(getScopeSymbol().getSymbolDescriptionPlain()).append("\n");
		} else {
			result.append("       scope symbol: ").append(getScopeSymbol()).append("\n");
		}
		result.append("\n");
		result.append(symbolTable.toString());

		return result.toString();
	}

	// ---------------------------------------------------------------------------

	public ImportConfiguration getImportConfiguration() {
		return importConfiguration;
	}

	// ---------------------------------------------------------------------------

	public void setImportConfiguration(ImportConfiguration importConfiguration) {
		this.importConfiguration = importConfiguration;
	}

	// ---------------------------------------------------------------------------

	public static Scope findDeepestScope(Scope s, int offset) {

		Scope result = null;
		for (Scope child : s.getChildren()) {
			if ((child.getStartOffset() <= offset) && (offset <= child.getEndOffset())) {
				result = child;
				Scope recursionNode = findDeepestScope(result, offset);
				if (recursionNode != null)
					result = recursionNode;
				break;
			}
		}
		if (result == null)
			result = s;

		return result;
	}

	// ---------------------------------------------------------------------------

	public String toScopeTreeString() {
		return toScopeTreeStringRecursion(this);
	}

	// ---------------------------------------------------------------------------

	private String toScopeTreeStringRecursion(Scope s) {
		StringBuffer result = new StringBuffer();
		List<Scope> scopeChildren = s.getChildren();
		int size = scopeChildren.size();

		result.append("--------------------------------------\n");
		result.append("->Passed Scope: \n").append(s);
		result.append("\n->Number of Children: ").append(size).append("\n");
		for (int i=0; i < size; i++) {
			result.append("  ->child ").append(i).append(": \n");
			result.append(toScopeTreeStringRecursion(scopeChildren.get(i)));
		}

		return result.toString();
	}

	// ---------------------------------------------------------------------------

	public Scope getModuleScope() {
		if (this.getScopeSymbol() instanceof ModuleSymbol)
			return this;

		return parent.getModuleScope();
	}

	// ---------------------------------------------------------------------------

	public boolean isSystemScope() {
		return isSystemScope;
	}

	// ---------------------------------------------------------------------------

	public void setSystemScope(boolean isSystemScope) {
		this.isSystemScope = isSystemScope;
	}


}
