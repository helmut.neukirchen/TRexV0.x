package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;

import antlr.MismatchedTokenException;
import antlr.RecognitionException;
import antlr.Token;
import antlr.TokenStreamException;
import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.TTCN3CorePlugin;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceFinder;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterParameters;
import de.ugoe.cs.swe.trex.core.metrics.TTCN3Metrics;
import de.ugoe.cs.swe.trex.core.misc.IntRangeMap;
import de.ugoe.cs.swe.trex.core.visitor.CheckAST;
import de.ugoe.cs.swe.trex.core.visitor.TTCN3SemanticAnalysisVisitor;

public class TTCN3Analyzer {
	private boolean semanticAnalysisEnabled = false;

	static class CommonDiff implements Comparable<CommonDiff> {
		Map<String, String> common;
		List<String> diff;

		public CommonDiff(Map<String, String> common, List<String> diff) {
			this.common = common;
			this.diff = diff;
		}

		/* (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(CommonDiff o) {
			if(equals(o))
				return 0;

			return diff.size() - o.getDiff().size();
		}

		@Override
		public boolean equals (Object other) {
			if(other instanceof CommonDiff) {
				final CommonDiff o = (CommonDiff)other;
				return (common.equals(o.getCommon()) &&
						diff.equals(o.getDiff()));
			}

			return false;
		}

		/**
		 * @return the common fields
		 */
		public Map<String, String> getCommon() {
			return common;
		}

		/**
		 * @return the differing fields
		 */
		public List<String> getDiff() {
			return diff;
		}

		@Override
		public int hashCode() {
			int hash = 11;
			hash = 31 * hash + (null == common ? 0 : common.hashCode());
			hash = 31 * hash + (null == diff ? 0 : diff.hashCode());
			return hash;
		}
	}

	// -------------------------------------------------------------------------

	private static final Comparator<AST> AST_STRING_COMPARATOR = new Comparator<AST>() {
		public int compare(AST ast0, AST ast1) {
			return ast0.toString().compareTo(ast1.toString());
		}
	};

	public final static Map<IFile, TTCN3Metrics> FILE_TO_METRIC_SCORES = new HashMap<IFile, TTCN3Metrics>();

	public final static Map<IFile, Map<String, List<ReferenceWithContext>>> FILE_TO_REFERENCES = new HashMap<IFile, Map<String, List<ReferenceWithContext>>>();

	private static final Comparator<Set<LocationAST>> SET_SIZE_COMPARATOR = new Comparator<Set<LocationAST>>() {
		public int compare(Set<LocationAST> o1, Set<LocationAST> o2) {
			return o1.size() - o2.size();
		}
	};

	public final static Map<LocationAST, PairTemplateListAndFields> TEMPLATE_TO_SIMILAR = new HashMap<LocationAST, PairTemplateListAndFields>();

	/**
     * Simple utility function to sort a <code>HashMap</code> by its values
     * rather than its keys.
     *
     * @param passedMap The <code>Map</code> to sort.
     * @param ascending A boolean to indicate whether the data should be
     * sorted in ascending or descending order.
     */
    public static Map<CommonDiff, Set<LocationAST>> sortHashMapByValues(final Map<CommonDiff, Set<LocationAST>> passedMap, final boolean ascending) {
        final List<CommonDiff> mapKeys = new ArrayList<CommonDiff>(passedMap.keySet());
        final List<Set<LocationAST>> mapValues = new ArrayList<Set<LocationAST>>(passedMap.values());
        Collections.sort(mapValues, SET_SIZE_COMPARATOR);
        Collections.sort(mapKeys);

        if (!ascending)
            Collections.reverse(mapValues);

        final Map<CommonDiff, Set<LocationAST>> someMap = new LinkedHashMap<CommonDiff, Set<LocationAST>>();
        for (final Set<LocationAST> val : mapValues) {
            for(final CommonDiff key : mapKeys) {
                final Set<LocationAST> passedVal = passedMap.get(key);
				if (passedVal != null && passedVal.toString().equals(val.toString())) {
                    passedMap.remove(key);
                    mapKeys.remove(key);
                    someMap.put(key, val);
                    break;
                }
            }
        }
        return someMap;
    }

	// -------------------------------------------------------------------------

	private String code;

	IFile file;

	private String filename;

	private Scope fileScope;

	private TTCN3Lexer lexer = null;

	private List<String> modules = null;

	private Map<String, Scope> moduleScopeMap = null;

	private int numMarkers;

	private TTCN3Parser parser = null;

	private boolean parserSuccess = false;

	private LocationAST rootNode;

	private IntRangeMap<Scope> scopeOffsetMap;

	private ArrayList<TTCN3SemanticException> semanticExceptions = null;

	private boolean symbolTableSuccess = false;

	private TTCN3SymbolTableTreeParser symbolTableWalker;

	private final Map<String, List<LocationAST>> typesToTemplates = new HashMap<String, List<LocationAST>>();

	private ArrayList<RecognitionException> exceptions;

	// -------------------------------------------------------------------------

	public TTCN3Analyzer(String filename) {
		this.filename = filename;
		setFile(filename);
	}

	// -------------------------------------------------------------------------

	public TTCN3Analyzer(String filename, String code, boolean isStandalone) {
		this.filename = filename;
		this.code = code;
		if (!isStandalone)
			setFile(filename);
	}

	// -------------------------------------------------------------------------

	private void addMarker(final LocationAST ast, final String message, final String solution, final String type) {
		// mark sure there is room for at least 50 rule8 markers (for demo purposes)
		if(!solution.equals("") && numMarkers >= 150)
			return;

		Map<String, Object> attributes = new HashMap<String, Object>();

		try {
			final LocationAST ident = ASTUtil.getIdentNode(ast);
			attributes.put("IdentifierOffset", Integer.valueOf(ident.getOffset()));
			attributes.put("TTCN3Solution", solution);
			attributes.put(IMarker.LINE_NUMBER, Integer.valueOf(ast.getLine()));
			attributes.put(IMarker.CHAR_START, Integer.valueOf(ast.getOffset()));
			attributes.put(IMarker.CHAR_END, Integer.valueOf(ast.getEndOffset()));
			attributes.put(IMarker.MESSAGE, message);
			attributes.put(IMarker.SEVERITY, Integer.valueOf(IMarker.SEVERITY_WARNING));
			attributes.put(IMarker.TRANSIENT, true);

			final IMarker marker = file.createMarker(type);
			marker.setAttributes(attributes);
			numMarkers++;
		} catch (final CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// -------------------------------------------------------------------------

	private void addMarkersForMerges(Set<LocationAST> mergeableTemplates, List<String> fields) {
		final String mergeRule = "de.ugoe.cs.swe.trex.ui.mergerulemarker";

		if(mergeableTemplates.size() > 1) {
			final StringBuffer buf = new StringBuffer();

			LocationAST main = null;
			int numTemplates = 0;
			for(final AST template : mergeableTemplates) {
				if(numTemplates == 0) {
					main = (LocationAST)template;
				} else {
					buf.append(ASTUtil.getIdentNode(template).getText());
					if (numTemplates != mergeableTemplates.size()-1) {
						buf.append(", ");
						// width is at a premium so don't list all matching templates if too long
						if(/*numTemplates >= 3 ||*/ buf.length() > 72) {
							numTemplates++;
							buf.append("& "+(mergeableTemplates.size()-numTemplates)+" more");
							break;
						}
					}
				}
				numTemplates++;
			}
			mergeableTemplates.remove(main);
			numTemplates--;

			// only allowed to suggest one potential merge per template
			if(TEMPLATE_TO_SIMILAR.get(main) != null)
				return;

			TEMPLATE_TO_SIMILAR.put(main, new PairTemplateListAndFields(mergeableTemplates, fields));

			final String pluralTemplates = (numTemplates > 1) ? mergeableTemplates.size()+" templates" : "template";

			final int numFields = fields.size();
			final String pluralFields = (numFields > 1) ? numFields+" fields" : "field";
			if(numFields > 0) {
				String fieldsStr;
				if(numFields > 3) {
					fieldsStr = listToString(fields.subList(0, 2))+", & "+(numFields-2)+" more";
				} else {
					fieldsStr = listToString(fields);
				}

				addMarker(
						main,
						"This and " + pluralTemplates + " '"
						+ buf.toString()+ "' could be parametrised on "
						+ pluralFields + ": '" + fieldsStr + "'.",
				        ""/*TODO: resolution*/,
				        mergeRule);
			} else {
				addMarker(
						main,
						"This is a duplicate of "+pluralTemplates+" '"+buf.toString()+"'.",
				        ""/*TODO: resolution*/,
				        mergeRule);
			}
		}
	}

	// -------------------------------------------------------------------------

	public void analyze() throws Exception, RecognitionException,
			TokenStreamException, MismatchedTokenException {
		long time;
		symbolTableSuccess = false;
		parserSuccess = false;

		time = System.currentTimeMillis();
		TTCN3CorePlugin.debugMessage("  parsing file and creating AST: " + filename);
		parse();
		TTCN3CorePlugin.debugMessageTimer("  ...finished parsing and ast creation in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");

		if(TTCN3CorePlugin.isCheckAST()){
			CheckAST checkAst = new CheckAST();
			checkAst.searchASTDepthFirst(rootNode);
		}

		time = System.currentTimeMillis();
		TTCN3CorePlugin.debugMessage("  creating symbol table and scopes...");
		createSymbolTable();
		TTCN3CorePlugin.debugMessageTimer("  ...finished symbol table and scopes creation in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");

		time = System.currentTimeMillis();
		TTCN3CorePlugin.debugMessage("  semantic analysis...");
		semanticalAnalysis(isSemanticAnalysisEnabled());
		TTCN3CorePlugin.debugMessageTimer("  ...finished semantic analysis in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");
	}

	// -------------------------------------------------------------------------

	/**
	 * Process a template to add markers where refactoring rules may be applied.
	 * Rule 1 > Rule 3 > Rule 2 > Rule 4 > Rule 5? (1,2,3,4,5)
	 * FIXME: need some sort of preferences to choose which rules to run
	 *
	 * @param ast the <code>TemplateDef</code> node to process.
	 * @param numReferences the number of references to this <code>TemplateDef</code> node.
	 */
	void applyBasicRefactoringRules(LocationAST ast, int numReferences, Map<String, List<ReferenceWithContext>> references) {
		final String unreferencedRule = "de.ugoe.cs.swe.trex.ui.unreferencedrulemarker";
		final String referencesRule = "de.ugoe.cs.swe.trex.ui.referencesrulemarker";

		if(numReferences < 1) {
			// RULE1: unreferenced
			addMarker(ast, "Template is never referenced." + " Consider removing.", "remove template", unreferencedRule);
		} else if (numReferences == 1) {
			// RULE2: only 1 reference
			addMarker(ast, "Template referenced only once." + " Consider inlining.", "inline template", referencesRule);
		} else {
			if(ASTUtil.isParameterizedTemplate(ast)) {
				applyParamRefactoringRules(ast, numReferences, references);
			}
		}

		// TTC-82: can disable comparison of parameterised templates here
//		if(ASTUtil.isParameterizedTemplate(ast))
//			return;

		// build up hashtable of types to templates for processing by remaining rules
		final String type = ASTUtil.getDescendantNode(ast).getText();
		List<LocationAST> list = typesToTemplates.get(type);
		if(list == null)
			list = new ArrayList<LocationAST>();
		list.add(ast);
		typesToTemplates.put(type, list);
	}


	// -------------------------------------------------------------------------

	void applyMergeRefactoringRules() {
		final Map<CommonDiff, Set<LocationAST>> mergeableTemplates = new HashMap<CommonDiff, Set<LocationAST>>();

		// loop for all types of template
		for(final String type : typesToTemplates.keySet()) {

			processTypeForMerges(type, mergeableTemplates);

			// sort by number of similar templates
			final Map<CommonDiff, Set<LocationAST>> sorted = sortHashMapByValues(mergeableTemplates, false);
			// loop all merge suggestions and add markers for them
			for(Map.Entry<CommonDiff, Set<LocationAST>> entry : sorted.entrySet()) {
				final List<String> fields = entry.getKey().getDiff();
				final Set<LocationAST> difftemplates = entry.getValue();

				boolean usedInDerived = false;

				usedInDerived = isReferencedInDerivedTemplate(difftemplates);

				if (!usedInDerived) {
					addMarkersForMerges(difftemplates, fields);
				}

				if(numMarkers >= 200)
					return;
			}
			mergeableTemplates.clear();
		}
	}

	// -------------------------------------------------------------------------

	private boolean isReferencedInDerivedTemplate(
			final Set<LocationAST> diffTemplates) {
		// extract the identifier nodes from the set of difftemplates
		List<LocationAST> diffNodes = new ArrayList<LocationAST>(diffTemplates.size());

		for(LocationAST ast : diffTemplates) {
			diffNodes.add(ASTUtil.getIdentNode(ast));
		}

		// get the precalculated set of references for all elements in the file
		Map<String, List<ReferenceWithContext>> results = TTCN3Analyzer.FILE_TO_REFERENCES
				.get(file);

		// loop through references
		for (List<ReferenceWithContext> item : results.values()) {
			for (ReferenceWithContext match : item) {
				// only interested in those who reference one of the ident nodes
				// we're interested in
				int found = Collections.binarySearch(diffNodes, match.getMatchNode());
				// if found, check if template is derived
				if (found >= 0
						&& TTCN3ParserTokenTypes.DerivedDef == match
								.getReferenceNode().getParent().getParent()
								.getType()) {
					return true;
				}
			}
		}
		return false;
	}

	// -------------------------------------------------------------------------

	private void applyParamRefactoringRules(LocationAST ast, int numReferences, Map<String, List<ReferenceWithContext>> references) {
		final String paramsRule = "de.ugoe.cs.swe.trex.ui.paramsrulemarker";

		final List<AST> params = ASTUtil.getFormalParameters(ast);
		final List<AST> fields = ASTUtil.getFieldValueIdents(ast);
		if(params != null && fields != null) {
			final int numParams = params.size();
			final int numFields = fields.size();

			final Set<AST> usedParams = new HashSet<AST>();
			final Set<AST> usedFields = new HashSet<AST>();

			if (numParams > 0 && numFields > 0) {
				// loop for all fields and see which are assigned to from params
				for(final AST field : fields) {
					for(final AST param : params) {
						if(field.getText().equals(param.getText())) {
							usedFields.add(field);
							usedParams.add(param);
							break;
						}
					}
				}
				// RULE5: if all fields are defined by formal parameters then inline
				if(numFields == usedFields.size()) {
					addMarker(
							ast,
							"Template's fields are all defined by formal parameters." + " Consider inlining.",
							"inline template", paramsRule);
				} else if (numParams == usedParams.size()) {
					// check if all references to this parameterised
					// template use the same actual paramaters and if so
					// suggest inlining the param(s)

					final AST ident = ASTUtil.getIdentNode(ast);
					List<LocationAST> lastparams = null;
					boolean differentParams = false;

					TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
					formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
					formatterWalker.setFormatterParameters(new TTCN3FormatterParameters());

					// loop for all files this template is referenced in
					for(Map.Entry<String, List<ReferenceWithContext>> entry : references.entrySet()) {
						// get the reference information
						final List<ReferenceWithContext> refs = entry.getValue();
						final ListIterator<ReferenceWithContext> refsIt = refs.listIterator( refs.size() );

						// loop for all references...
						while(refsIt.hasPrevious() && !differentParams) {
							final ReferenceWithContext prev = refsIt.previous();
							// ... to this template ...
							if(ident.equals(prev.getMatchNode())) {
								// ... and compare its actual parameters to those we have used before
								final List<LocationAST> refparams = ASTUtil.getActualParameters(prev.getReferenceNode(), false);
								if(lastparams != null) {
									if(lastparams.size() == refparams.size()) {
										for(int i=0; i<refparams.size(); i++) {
											try {
												final String refparamStr = formatterWalker.pr_TemplateInstance(refparams.get(i)).toString();
												final String lastparamStr = formatterWalker.pr_TemplateInstance(lastparams.get(i)).toString();
												if (refparamStr.equals(lastparamStr)) {
													continue;
												}
											} catch (RecognitionException e) {
												e.printStackTrace();
												TTCN3CorePlugin.debugMessage("Reference to "+ast.toString()+ " in "+entry.getKey());
											}
											differentParams = true;
											break;
										}
									} else {
										differentParams = true;
									}
								}
								lastparams = refparams;
							}
						}
					}

					// if the actual paramaters were the same each time then suggest parameter inlining
					if(!differentParams) {
						addMarker(ast, "Template is always referenced with same parameters. Consider inlining the parameters.", "inline param", paramsRule);
					}
				} else {
					// RULE6: formal params that aren't actually used should be removed.
					for(final AST param : params) {
						if(!usedParams.contains(param)) {
							final LocationAST formalPar = ((LocationAST)param).getParent().getParent();
							addMarker(formalPar, "Param '"+param.getText()+"' is not used. Consider removing. ", "remove param", paramsRule);
						}
					}
				}
			} else {
				// RULE3: multiple references, no formal parameters, "maximise
				// maintainability", inline
				addMarker(
						ast,
						"Template has multiple references but no formal params." + " Consider inlining." /*+ " to maximise maintainability at the expense of reusability."*/,
						"inline template in " + numReferences + " places.", paramsRule);
			}
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Finds all references to nodes in the given list and adds them to the
	 * given metrics object.
	 *
	 * @param nodes
	 *            the <code>List</code> of <code>LocationAST</code> nodes to
	 *            find references for
	 * @param mi
	 *            the <code>TTCN3Metrics</code> object to store references in
	 */
	private Map<String, List<ReferenceWithContext>> calcReferences(List<LocationAST> nodes) throws IllegalArgumentException {
		final List<Symbol> symbols = getIdentSymbols(nodes);

		if(symbols == null)
			throw new IllegalArgumentException ();

		final Map<String, List<ReferenceWithContext>> references = new ReferenceFinder().findReferences(symbols);

		return references;
	}

	// -------------------------------------------------------------------------

	public void createSymbolTable() throws Exception, RecognitionException {
		if (!parserSuccess)
			return;

		symbolTableWalker = new TTCN3SymbolTableTreeParser();
		symbolTableWalker.setFilename(filename);
		symbolTableWalker.getASTFactory().setASTNodeClass(LocationAST.class);

		symbolTableWalker.pr_TTCN3File(rootNode);
		moduleScopeMap = symbolTableWalker.getModuleScopeMap();
		fileScope = symbolTableWalker.getFileScope();
		modules = symbolTableWalker.getModules();
		scopeOffsetMap = symbolTableWalker.getScopeOffsetMap();
		exceptions.addAll(symbolTableWalker.getExceptions());
		symbolTableSuccess = true;
	}

	// -------------------------------------------------------------------------

	public void displayTokenStream(TTCN3Lexer l) throws TokenStreamException {
		Token t;

		t=l.nextToken();
		while (t.getType() != Token.EOF_TYPE) {
			final String type = TTCN3Parser._tokenNames[t.getType()];
//			System.out.println(type + ": '" + t.getText() + "' " + t.toString());
			TTCN3CorePlugin.debugMessage(type + ":\t\t\t" + t.toString() + "\n");
			t = l.nextToken();
		}
		l.rewind(0);
	}

	// -------------------------------------------------------------------------

	public String getCode() {
		return code;
	}

	// -------------------------------------------------------------------------

	public IFile getFile() {
		return file;
	}

	// -------------------------------------------------------------------------

	public String getFilename() {
		return filename;
	}

	// -------------------------------------------------------------------------

	public Scope getFileScope() {
		return fileScope;
	}

	// -------------------------------------------------------------------------

	public IdentifierCache getIdentifierCache() {
		if (parserSuccess)
			return parser.getIdentifierCache();

		return null;
	}

	// -------------------------------------------------------------------------

	private Symbol getIdentSymbol(Object obj) {
		if(file != null && obj != null && obj instanceof LocationAST) {
			LocationAST ast = (LocationAST)obj;

			// find identifier node and do getscope on that
			ast = ASTUtil.getIdentNode(ast);
			final Scope scope = ast.getScope();
			final Symbol symb = scope == null ? null : scope.resolve(ast);
			return symb;
		}

		return null;
	}

	// -------------------------------------------------------------------------

	private List<Symbol> getIdentSymbols(List<LocationAST> nodes) {
		final List<Symbol> symbols = new ArrayList<Symbol>();
		Symbol symbol;

		for(final LocationAST node : nodes) {
			symbol = getIdentSymbol(node);
			if(symbol != null) {
				symbols.add(symbol);
			}
		}

		return symbols;
	}

	// -------------------------------------------------------------------------

	public TTCN3Lexer getLexer() {
		return lexer;
	}

	// -------------------------------------------------------------------------

	public List<String> getModules() {
		if (symbolTableSuccess)
			return modules;

		return null;
	}

	// -------------------------------------------------------------------------

	public Map<String, Scope> getModuleScopeMap() {
		return moduleScopeMap;
	}

	// -------------------------------------------------------------------------

	public TTCN3Parser getParser() {
		return parser;
	}

	// -------------------------------------------------------------------------

	public LocationAST getRootNode() {
		return rootNode;
	}

	// -------------------------------------------------------------------------

	public IntRangeMap<Scope> getScopeOffsetMap() {
		if (scopeOffsetMap != null)
			return scopeOffsetMap;

		return null;
	}

	// -------------------------------------------------------------------------

	public ArrayList<TTCN3SemanticException> getSemanticExceptions() {
		return semanticExceptions;
	}

	// -------------------------------------------------------------------------

	public boolean isAnalyzed() {
		return parserSuccess && symbolTableSuccess;
	}

	// -------------------------------------------------------------------------

	private String listToString(List<String> fields) {
		final int size = fields.size();
		if(size < 1)
			return null;

		final StringBuffer buf = new StringBuffer(fields.get(0));

		for(int i=1; i<size; i++) {
			buf.append(", ").append(fields.get(i));
		}

		return buf.toString();
	}

	// -------------------------------------------------------------------------

    public void parse() throws RecognitionException, TokenStreamException {
		//resetAnalyzer();
		exceptions = new ArrayList<RecognitionException>();
		lexer = new TTCN3Lexer(new StringReader(code));
		lexer.setTabSize(1);
		lexer.setTokenObjectClass(de.ugoe.cs.swe.trex.core.analyzer.rfparser.TokenWithIndex.class);

		final TokenStreamTracker tracker = new TokenStreamTracker(lexer);
		tracker.discard(TTCN3LexerTokenTypes.WS);
		tracker.discard(TTCN3LexerTokenTypes.ML_COMMENT);
		tracker.discard(TTCN3LexerTokenTypes.SL_COMMENT);

		parser = new TTCN3Parser(tracker);
		parser.setASTFactory(new TTCN3ASTFactory(parser.getTokenTypeToASTClassMap()));
		parser.getASTFactory().setASTNodeClass(
				de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST.class);
		parser.getASTFactory().setASTNodeClass(
				de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST.class);

//		System.out.println("=================================================");
//
//		Token t = tracker.nextToken();
//		while (t.getText() != null) {
//			System.out.println("token: " + t.getText() + " - Type: " + parser.getTokenName(t.getType()));
//			t = tracker.nextToken();
//		}
//		System.out.println("=================================================");

		parser.pr_TTCN3File();
		rootNode = (LocationAST) parser.getAST();
		exceptions.addAll(parser.getExceptions());


		parserSuccess = true;
	}

	// -------------------------------------------------------------------------

	public void postProcess() {
		if (symbolTableWalker != null){
			symbolTableWalker.connectKnownScopes();
		}
	}

	// -------------------------------------------------------------------------

	private void processTypeForMerges(String type, Map<CommonDiff, Set<LocationAST>> mergeableTemplates) {
		final List<LocationAST> templates = typesToTemplates.get(type);
		final int max = templates.size();
		// loops for all templates of this type
		for(int count=0; count < max; count++) {
			// get a template
			final LocationAST template = templates.get(count);
			// loop for all other templates in the list and compare fields
			for(int j=count+1; j < max; j++) {
				final LocationAST other = templates.get(j);
				try {
					final Map<String, String> common = ASTUtil.getCommonFields(template, other);
					final List<AST> diffsAST = ASTUtil.getDifferingFields(template, other);
					final List<String> diffs = new ArrayList<String>(diffsAST.size());

					for(final AST field : diffsAST) {
						diffs.add(field.getText());
					}

					final int numCommon = common.size();
					final int numDiffs = diffs.size();
					// XXX: rule8 states 'if the number of differing fields is
					// greater than or equal to a third of the total number of
					// fields'

					// maximum 4 parameters for now
					if(numCommon >= 1 && (numDiffs >= 0 && numDiffs <= 4)) {

						// for each field, check that the type has been defined
						boolean allTyped = true;
						for(final AST diff : diffsAST) {
							if(ASTUtil.getTypeSymbol(diff) == null) {
								allTyped = false;
							}
						}

						// if all field types are known or templates are exact duplicates then...
						if(allTyped || numDiffs == 0) {
							final CommonDiff cd = new CommonDiff(common, diffs);
							// ... add the templates to the list associated with these differences
							Set<LocationAST> temp = mergeableTemplates.get(cd);
							if(temp == null) {
								temp = new TreeSet<LocationAST>(AST_STRING_COMPARATOR);
							}
							temp.add(template);
							temp.add(other);
							mergeableTemplates.put(cd, temp);
						}
					}
				} catch (final IllegalArgumentException iae) {
					// templates whose fields cannot be resolved
					TTCN3CorePlugin.debugMessage
									("Template field cannot be resolved @ "
									+ template.getOffset() + " or "
									+ other.getOffset());
				}
			}
		}
	}

	// -------------------------------------------------------------------------

	public void resetAnalyzer() {
		lexer = null;
		parser = null;
		modules = null;
		rootNode = null;
		moduleScopeMap = null;
		symbolTableWalker = null;
		scopeOffsetMap = null;
		fileScope = null;
		semanticExceptions = null;
	}

	// -------------------------------------------------------------------------

	public boolean runMetricAnalysis() {
		if (!symbolTableSuccess)
			return false;

		numMarkers = 0;

		final TTCN3Metrics mi = new TTCN3Metrics();

		long time = System.currentTimeMillis();
		TTCN3CorePlugin.debugMessage("  Processing Metrics on file " + filename + "...");
		final List<LocationAST> nodes = mi.processTree(rootNode);
		TTCN3CorePlugin.debugMessageTimer("  ...completed processtree in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");

		TTCN3CorePlugin.debugMessage("  Calculating References...");
		time = System.currentTimeMillis();
		final Map<String, List<ReferenceWithContext>> references = calcReferences(nodes);
		TTCN3CorePlugin.debugMessageTimer("  ...completed calculating references in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");

		TTCN3CorePlugin.debugMessage("  Storing References...");
		time = System.currentTimeMillis();
		TTCN3Analyzer.FILE_TO_REFERENCES.put(file, references);
		mi.storeReferences(references);
		TTCN3CorePlugin.debugMessageTimer("  ...completed storing references in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");

		time = System.currentTimeMillis();
		TTCN3CorePlugin.debugMessage("  Processing Templates...");
		typesToTemplates.clear();

		long time1 = System.currentTimeMillis();
		TTCN3CorePlugin.debugMessage("    sorting nodes...");
		Collections.sort(nodes);
		TTCN3CorePlugin.debugMessageTimer("    ...sorted nodes in " + Long.valueOf(System.currentTimeMillis()-time1) + " ms");

		time1 = System.currentTimeMillis();
		TTCN3CorePlugin.debugMessage("    applying basic refactoring rules...");
		for(final LocationAST ast : nodes) {
			if(TTCN3ParserTokenTypes.TemplateDef == ast.getType()) {
				applyBasicRefactoringRules(ast, mi.getReferences(ast), references);
			}
		}
		TTCN3CorePlugin.debugMessageTimer("    ...applied basic refactoring rules in " + Long.valueOf(System.currentTimeMillis()-time1) + " ms");
		time1 = System.currentTimeMillis();
		TTCN3CorePlugin.debugMessage("    applying merge template refactoring rule...");
		applyMergeRefactoringRules();
		TTCN3CorePlugin.debugMessageTimer("    ...applied merge template refactoring rule in " + Long.valueOf(System.currentTimeMillis()-time1) + " ms");
		time1 = System.currentTimeMillis();
		TTCN3CorePlugin.debugMessage("    filing metric scores...");
		TTCN3Analyzer.FILE_TO_METRIC_SCORES.put(file, mi);
		TTCN3CorePlugin.debugMessageTimer("    ...filed metric scores in " + Long.valueOf(System.currentTimeMillis()-time1) + " ms");

		TTCN3CorePlugin.debugMessageTimer("  ...processed template in " + Long.valueOf(System.currentTimeMillis()-time) + " ms");
		TTCN3CorePlugin.debugMessage("  " + numMarkers + " Markers Added...");

		return true;
	}

	// -------------------------------------------------------------------------

	public void semanticalAnalysis(boolean enabled) throws Exception {
		if (!symbolTableSuccess)
			return;

		if (enabled) {
//			TTCN3SemanticAnalysisTreeParser semanticAnalysisWalker = new TTCN3SemanticAnalysisTreeParser();
			TTCN3SemanticAnalysisVisitor semanticAnalysisWalker = new TTCN3SemanticAnalysisVisitor(filename);
			//semanticAnalysisWalker.getASTFactory().setASTNodeClass(LocationAST.class);

			semanticAnalysisWalker.acceptDFS(rootNode);

			semanticExceptions = semanticAnalysisWalker.getExceptions();
			Collections.sort(semanticExceptions);
		}
	}

	// -------------------------------------------------------------------------

	public void setCode(String code) {
		this.code = code;
	}

	// -------------------------------------------------------------------------

	private void setFile(String filename) {
		final IWorkspace ws = ResourcesPlugin.getWorkspace();
		final IWorkspaceRoot root = ws.getRoot();
		final IResource member = root.findMember(filename);
		file = (IFile) member;
	}

	// -------------------------------------------------------------------------

	public void setFilename(String filename) {
		this.filename = filename;
		setFile(filename);
	}

	// -------------------------------------------------------------------------

	public ArrayList<RecognitionException> getExceptions() {
		return exceptions;
	}

	public void setSemanticAnalysisEnabled(boolean semanticAnalysisEnabled) {
		this.semanticAnalysisEnabled = semanticAnalysisEnabled;
	}

	public boolean isSemanticAnalysisEnabled() {
		return semanticAnalysisEnabled;
	}



}
