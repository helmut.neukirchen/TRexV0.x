/* -----------------------------------------------------------------------------*
 *      								TreeParser                              *
 * -----------------------------------------------------------------------------*/

header {
package de.ugoe.cs.swe.trex.core.analyzer.rfparser;
}

{
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterParameters;

@SuppressWarnings("unused")
}

class TTCN3FormatterTreeParser extends TreeParser;

options
{
  k = 1;
  importVocab = TTCN3Parser;
  defaultErrorHandler=false;
}

{ // begin code block
	TokenStreamTracker tracker;
	TTCN3FormatterParameters parameters;
	int currentNesting = 0;
	int lastIndex = 0;
	boolean inGroup = false;
	
	public void setTokenStreamTracker(TokenStreamTracker t) {
		tracker = t;
	}
	
	public void setFormatterParameters(TTCN3FormatterParameters t) {
		parameters = t;
	}
	
	private String getSpacesCountString() {
		StringBuffer s = new StringBuffer();
		
		for (int i=0; i < parameters.getSpacesCount(); i++) {
			s.append(" ");
		}
		return s.toString();
	}

	private String getSpacing() {
		StringBuffer spacing = new StringBuffer();
		for (int i=0; i < currentNesting; i++) {
			if (parameters.isTabs()) {
				spacing.append("\t");
			} else {
				spacing.append(getSpacesCountString());
			}
		}
		
		return spacing.toString();
	}

	private String getLines(int lines) {
		StringBuffer newLines = new StringBuffer();
		for (int i=0; i < lines; i++) {
			newLines.append(getNewline());
		}
		return newLines.toString();
	}

	private String getNewline() {
		if (parameters.isUnixNewline()) {
			return "\n";
		} else {
			return "\r\n";
		}
	}

	public String getNewlines(int lineCount) {
		StringBuffer newlines = new StringBuffer();
		for (int i=0; i < lineCount; i++) {
			newlines.append(getNewline());
		}
		return newlines.toString();
	}
 	
	private boolean isComment(TokenWithIndex t) {
		if ((t.getType() == SL_COMMENT) || (t.getType() == ML_COMMENT))
			return true;
		else
			return false;		
	}
	
	private int getIndex(AST t) {
		return ((TokenWithIndex)((LocationAST)t).getToken()).getIndex();
	}

	private String scanBackward(AST t) {
		if (t == null)
			return "";

		LocationAST ast = (LocationAST) t;

		if (ast.isImaginaryNode())
			return "";

		StringBuffer result = new StringBuffer();

		TokenWithIndex tok = (TokenWithIndex) ast.getToken();
		int tokenIndex = tok.getIndex();
		
		for (int i=lastIndex; i < tokenIndex; i++) {
			tok = (TokenWithIndex) tracker.getToken(i);
			if (isComment(tok))
				result.append(tok.getText());
		}
		lastIndex = tokenIndex;
		
		return result.toString();
	}
	
	private String getAssignment() {
		if (parameters.isSpacesBetweenAssignment()) {
			return " := ";
		} else {
			return ":=";
		}
	}
	
	private String getComma() {
		if (parameters.isSpaceAfterComma()) {
			return ", ";
		} else {
			return ",";
		}
	}

	private String getOpeningBrace() {
		if (parameters.isKRstyle()) {
			return " {" + getNewline();
		} else {
			return getNewline() + getSpacing() + "{" + getNewline();
		}
	}

	private String getOpeningBraceNoSpace() {
		if (parameters.isKRstyle()) {
			return "{" + getNewline();
		} else {
			return getNewline() + getSpacing() + "{" + getNewline();
		}
	}

	private String getKRNewline() {
		if (parameters.isKRstyle()) {
			return "";
		} else {
			return getNewline() + getSpacing();
		}
	}

	//TODO: avoid repetition, parametrize??
	
	private String getNewlineBeforeRunsOn() {
		if(parameters.isNewlineBeforeRunsOn()) {
			return getNewline() + getSpacing();
		} else {
			return " ";
		}
	}

	private String getNewlineBeforeSystem() {
		if(parameters.isNewlineBeforeSystem()) {
			return getNewline() + getSpacing();
		} else {
			return " ";
		}
	}


	private String getNewlineBeforeReturn() {
		if(parameters.isNewlineBeforeReturn()) {
			return getNewline() + getSpacing();
		} else {
			return "";
		}
	}

	private String getNewlineBeforeExceptionSpec() {
		if(parameters.isNewlineBeforeExceptionSpec()) {
			return getNewline() + getSpacing();
		} else {
			return "";
		}
	}

	private String getNewlineBeforeFormalPar() {
		if(parameters.isNewlineBeforeFormalPar()) {
			return getNewline() + getSpacing() + getSpacesCountString();
		} else {
			return "";
		}
	}

	private String getNewlineAfterFormalParList() {
		if(parameters.isNewlineAfterFormalParList()) {
			return getNewline() + getSpacing();
		} else {
			return getSpacesAroundParentheses(); //questionable, mixing concepts, but ultimately useful
			//return "";			
		}
	}
	
	private String getSpacesAroundParentheses() {
		if(parameters.isSpacesAroundParentheses()) {
			return " ";
		} else {
			return "";
		}
	}

	private String getOpenParenthesis() {
		return getSpacesAroundParentheses()+"(";
	}

	private String getClosedParenthesis() {
		return ")";
	}



} // end code block	


//Proposed improvement by Gustaf Johansson
pr_TTCN3File returns [StringBuffer s]
{
    s = new StringBuffer();
    StringBuffer module = new StringBuffer();
}
:
    #(TTCN3File 
        (
            EOF
            |
            (
                module=pr_TTCN3Module 
                { 
                    s.append(module); 
                }
                (
                    {
                        s.append(getNewlines(parameters.getLinesBetweenModules())); 
                    }
                    module=pr_TTCN3Module 
                    { 
                        s.append(module); 
                    }
                )*
            )
        )
    )
;
 

/*****1.6  - TTCN Module *****/

pr_TTCN3Module returns [StringBuffer s]
{
	s = new StringBuffer();
	StringBuffer moduleId;
	StringBuffer withStatement = null;
	StringBuffer controlPart = null;
	StringBuffer moduleDefinitions = null;
	StringBuffer semi = new StringBuffer();
}
:
	#(TTCN3Module moduleId=pr_TTCN3ModuleId 
		( {currentNesting++;} moduleDefinitions=pr_ModuleDefinitionsPart {currentNesting--;} )? 
		( {currentNesting++;} controlPart = pr_ModuleControlPart {currentNesting--;} )? 
		( withStatement=pr_WithStatement )? 
		( SemiColon {semi.append(";");} )?
	 )
{
	s.append("module ");
	s.append(moduleId);
	s.append(getOpeningBrace());
	if (moduleDefinitions != null)
		s.append(moduleDefinitions);
	
	if (controlPart != null) {
		s.append(controlPart);
	}
	
	s.append("}");
	if (withStatement != null) {
		s.append(" ");
		s.append(withStatement);
	}
	s.append(semi);
}
;

pr_TTCN3ModuleId returns [StringBuffer s]
{
	s = new StringBuffer();
	StringBuffer a;
}
:
	#(TTCN3ModuleId a=pr_ModuleId)
{
	s.append(a);
}
;

//pr_DefinitiveObjIdComponent returns [StringBuffer s]:
//	s=pr_NameForm
//	| s=pr_DefinitiveNumberForm
//	| s=pr_DefinitiveNameAndNumberForm
//;

pr_Number returns [StringBuffer s] {s = new StringBuffer();}:
	#(Number a:NUMBER {s.append(a.getText());})
;
 

pr_DefinitiveNumberForm returns [StringBuffer s]:
	s=pr_Number
;


pr_DefinitiveNameAndNumberForm returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(DefinitiveNameAndNumberForm a=pr_Identifier {s.append(a);} b=pr_DefinitiveNumberForm {s.append(b);})
;


pr_ModulePar returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ModulePar a=pr_Type {s.append(a + " ");} b=pr_ModuleParList {s.append(b);}) 
;


pr_ModuleParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b; boolean first=true;}:
	#(ModuleParList (a=pr_Identifier {if (first) s.append(a); else s.append(getComma() + a);} ( b=pr_ConstantExpression {s.append(getAssignment() + b);} )? {first=false;})+ )
;


/************************* 1.6.1 - Module Definitions Part *********************/


pr_ModuleDefinitionsPart returns [StringBuffer s]{s=new StringBuffer();StringBuffer a;}:
	#( ModuleDefinitionsPart 
		#(ModuleDefinitionList
			(	{s.append(getNewlines(parameters.getLinesAfterModuleDefinition()));}
				( 	
					a=pr_ModuleDefinition 
					{s.append(a);} 
					(SemiColon {s.append(";");})? 
					{
						s.append(getNewline()); 
						//if (!inGroup)
						int type = 0;
						if (_t!=null && _t.getFirstChild()!=null){
							type = _t.getFirstChild().getType(); 
							if (type == TTCN3ParserTokenTypes.Visibility) {
						 		type = _t.getFirstChild().getNextSibling().getType();
						 	}
						}
					 	if (type == TTCN3ParserTokenTypes.ImportDef) {
					 		s.append(getNewlines(parameters.getLinesBetweenImportDefinitions()));
					 	}	
					 	else {
					 		s.append(getNewlines(parameters.getLinesAfterModuleDefinition()));
					 	} 
					 		
					}
				)+ 
			)
		)
	)
;


pr_ModuleDefinition returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b,c,d,e,f,g,h,i,j,k,l,m,n,o;}:
	{s.append(getSpacing());}
	#(ModuleDefinition 
		( n=pr_Visibility {s.append(n);}
		)?
	
		(
		a=pr_TypeDef {s.append(a);}
		| b=pr_ConstDef {s.append(b);}
		| c=pr_TemplateDef {s.append(c);}
		| d=pr_FunctionDef {s.append(d);}
		| e=pr_SignatureDef {s.append(e);}
		| f=pr_TestcaseDef {s.append(f);}
		| g=pr_ModuleParDef {s.append(g);}
		| h=pr_ImportDef {s.append(h);}
		| i=pr_GroupDef {s.append(i);}
		| j=pr_ExtFunctionDef {s.append(j);}
		| k=pr_ExtConstDef {s.append(k);}
		| l=pr_AltstepDef{s.append(l);}
		| o=pr_FriendModuleDef{s.append(o);}
		)
		( m=pr_WithStatement {s.append(" "+m);})? 
	) 
//	{if (!inGroup) s.append(getNewlines(parameters.getLinesAfterModuleDefinition())); }
//	{s.append(getNewline());}
;


pr_Visibility returns [StringBuffer s] {s = new StringBuffer();}:
	#(Visibility
		( PUBLIC {s.append("public ");}
		| PRIVATE {s.append("private ");}
		| FRIEND {s.append("friend ");}
		)
	)
;

pr_FriendModuleDef returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b,c;}:
	#(FriendModuleDef
		{s.append("friend module ");} 
		a=pr_Identifier
		{s.append(a);} 
		(	{s.append(getComma());}
			a=pr_Identifier
			{s.append(a);} 
		)*
	)
;


/***** 1.6.1.1 - Typedef Definitions *****/




pr_TypeDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; }:
	#(TypeDef
   		{s.append("type ");} 
   		(	a=pr_StructuredTypeDef 
   			{s.append(a);}
   		  | a=pr_SubTypeDef 
   		  	{s.append(a);}
   		)
   	) 
;

pr_StructuredTypeDef returns [StringBuffer s]:
	#(StructuredTypeDef
 		(	s=pr_RecordDef
		  | s=pr_UnionDef
		  | s=pr_SetDef
		  | s=pr_RecordOfDef
		  | s=pr_SetOfDef
		  | s=pr_EnumDef
		  | s=pr_PortDef
		  | s=pr_ComponentDef
		)
	)
;


pr_RecordDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(RecordDef
		{s.append("record ");} 
		a=pr_StructDefBody 
		{s.append(a);}
	)
;


pr_StructDefBody returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,d; boolean first=true;}:   // no root node needed
	#(StructDefBody
		(	a=pr_Identifier 
			{s.append(a);} 
			(	b=pr_StructDefFormalParList 
				//apparently also brackets are missing here!!!
				//{s.append(b);} //substituted through
				{s.append(getOpenParenthesis() + b + getClosedParenthesis());}
			)? 
			{s.append(getOpeningBrace());}
		  | AddressKeyword 
		  	{s.append("address" + getOpeningBrace());}
		) 
		{currentNesting++;} 
		(	d=pr_StructFieldDef 
			{if (first) s.append(getSpacing() + d); else s.append("," + getNewline() + getSpacing() + d); first=false;}
		)* 
		{currentNesting--;} 
		{s.append(getNewline() + getSpacing() + "}" );}
	)
;

//FOR REFERENCE ONLY: Old incorrect version
//pr_StructDefFormalParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
// 	#(StructDefFormalParList //No explicit StructDefFormalParList node is present in the parse tree
//    	(	a=pr_StructDefFormalPar 
//it seems to be all wrong anyway, apparently multiple parameters are not to be separated by space and comma 
//    		{s.append(a);}
//    	)*
//    )

pr_StructDefFormalParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b; boolean multiparam=false;}:
	a=pr_StructDefFormalPar 
//	{s.append(a);} 
	(	b=pr_StructDefFormalPar 
//		{s.append(getComma() + b);}
		{multiparam=true;}
		{a.append(getComma() + getNewlineBeforeFormalPar() + b);}	
 	)*
// 	{if (multiparam) s.append(getNewlineBeforeFormalPar()); else s.append(getSpacesAroundParentheses());}
    {s.append(a);}
// 	{if (multiparam) s.append(getNewlineAfterFormalParList()); else s.append(getSpacesAroundParentheses());}

 	{if (multiparam) {
 		s.insert(0,getNewlineBeforeFormalPar());
		s.insert(0,getSpacesAroundParentheses());
 		s.append(getNewlineAfterFormalParList());
 		
 	 } else {
 	 	s.insert(0,getSpacesAroundParentheses());
 		s.append(getSpacesAroundParentheses());
 	 }
 	}
;


pr_StructDefFormalPar returns [StringBuffer s]:
    s=pr_FormalValuePar
;


pr_StructFieldDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d,e;}:

  #( StructFieldDef 
    ( a=pr_Type {s.append(a + " ");}|b=pr_NestedTypeDef {s.append(b + " ");}) c=pr_Identifier {s.append(c);}( d=pr_ArrayDef {s.append(d);})? ( e=pr_SubTypeSpec {s.append(e);} )? ( OPTIONAL {s.append(" optional");})? ) //{s.append(getNewline());}

;

pr_NestedTypeDef returns [StringBuffer s]:

#(NestedTypeDef(
  s=pr_NestedRecordDef |
  s=pr_NestedUnionDef |
  s=pr_NestedSetDef |
  s=pr_NestedRecordOfDef |
  s=pr_NestedSetOfDef |
  s=pr_NestedEnumDef
  ))
;                      
                   
pr_NestedRecordDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
 #(NestedRecordDef {s.append("record " + getOpeningBraceNoSpace()); currentNesting++;} ((a=pr_StructFieldDef {s.append(getSpacing() + a);} ( b=pr_StructFieldDef {s.append(getComma() + getNewline() + getSpacing() + b);})*)? {currentNesting--; s.append(getNewline() + getSpacing() + "}");}
  ))
;
 
pr_NestedUnionDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
#(NestedUnionDef ( {s.append("union " + getOpeningBraceNoSpace()); currentNesting++;}
 a=pr_UnionFieldDef {s.append(getSpacing() + a);} ( b=pr_UnionFieldDef {s.append(getComma() + getNewline() + getSpacing() + b);})* {currentNesting--; s.append(getNewline() + getSpacing() + "}");}))
 ;
 
pr_NestedSetDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
#(NestedSetDef(
 (a=pr_StructFieldDef {s.append("set " + getOpeningBraceNoSpace()); currentNesting++; s.append(getSpacing() + a);}( b=pr_StructFieldDef {s.append("," + getNewline() + getSpacing() + b);})*)?)) {currentNesting--; s.append(getNewline() + getSpacing() + "}");}
 ;
 
pr_NestedRecordOfDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(NestedRecordOfDef( {s.append("record ");} 
		(a=pr_StringLength {s.append(a + " ");})?  (b=pr_Type {s.append("of " + b);} | c=pr_NestedTypeDef {s.append("of " + c);}))) 
;

pr_NestedSetOfDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}: 
#(NestedSetOfDef( {s.append("set "); } 
 (a=pr_StringLength {s.append(a + " ");})?  (b=pr_Type {s.append("of " + b);}| c=pr_NestedTypeDef {s.append("of " + c);})))
;

pr_NestedEnumDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
 #(NestedEnumDef {s.append("enumerated ");}
	 a=pr_EnumerationList {s.append(a);}
  )              
;

pr_UnionDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,d; boolean first=true;}:
	#(UnionDef 
		#(UnionDefBody
			{s.append("union ");}
			(	a=pr_Identifier 
				{s.append(a);} 
				(	b=pr_StructDefFormalParList 
					{s.append(b);} 
				)?
			  | AddressKeyword 
			 	{s.append("address");} 
			)
			{s.append(getOpeningBrace()); currentNesting++;} 
			(	d=pr_UnionFieldDef 
				{if (first) s.append(getSpacing() + d); else s.append(getComma() + getNewline() + getSpacing() + d); first=false;}
			)* 
			{currentNesting--; s.append(getNewline() + getSpacing() + "}");} 
		)
	)
;


pr_UnionFieldDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d,e;}:

  #( UnionFieldDef 
     (a=pr_Type {s.append(a + " ");}|b=pr_NestedTypeDef {s.append(b + " ");}) 
     c=pr_Identifier {s.append(c);} 
     ( d=pr_ArrayDef {s.append(d);})? 
     ( e=pr_SubTypeSpec {s.append(" " + e);})? 
   )
;


pr_SetDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:

  #( SetDef {s.append("set ");}
     a=pr_StructDefBody {s.append(a);})

;


pr_RecordOfDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:

  #( RecordOfDef {s.append("record ");}
     ( a=pr_StringLength {s.append(a + " ");})? b=pr_StructOfDefBody {s.append("of " + b);})

;


pr_StructOfDefBody returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,e;}:

#(StructOfDefBody(
  (a=pr_Type {s.append(a + " ");}|b=pr_NestedTypeDef {s.append(b + " ");})
  (   c=pr_Identifier {s.append(c);}
    |  AddressKeyword {s.append("address");})
  ( e=pr_SubTypeSpec {s.append(e);})?
))
;


pr_SetOfDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
  #( SetOfDef {s.append("set ");}
     ( a=pr_StringLength {s.append(a + " ");})? b=pr_StructOfDefBody {s.append("of " + b);})
;


pr_EnumDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,c;}:

  #( EnumDef {s.append("enumerated ");}
     (   a=pr_Identifier {s.append(a + " ");}
       |  AddressKeyword {s.append("address ");})
     c=pr_EnumerationList {s.append(c);} )

;
pr_EnumerationList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:

 #(EnumerationList 
   a=pr_Enumeration { s.append(getOpeningBraceNoSpace()); currentNesting++; s.append(getSpacing() + a);} (b=pr_Enumeration {s.append("," + getNewline() + getSpacing() + b);})* {currentNesting--; s.append(getNewline() + getSpacing() + "}");}
)

;

pr_Enumeration returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c=new StringBuffer();}:
//  #( NamedValue
	#(Enumeration 
		a=pr_Identifier 
		{s.append(a);} 
		(	(	Minus 
				{c.append("-");}
			)? 
			b=pr_Number 
			{s.append(getOpenParenthesis() + 
			getSpacesAroundParentheses() + 
			c + b + 
			getSpacesAroundParentheses() +
			getClosedParenthesis());}
		)? 
	)
;

pr_SubTypeDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d;}:
  	#(SubTypeDef
     	a=pr_Type 
     	{s.append(a + " ");}
     	(	b=pr_Identifier 
     		{s.append(b);} 
     	  | AddressKeyword 
     	  	{s.append("address");}
     	) 
     	(	c=pr_ArrayDef 
     		{s.append(c);} 
     	)? 
     	(	d=pr_SubTypeSpec 
     		{s.append(d);}
     	)? 
   )
;


pr_SubTypeSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
  	#(SubTypeSpec 
  		(	(	a=pr_AllowedValues 
  				{s.append(getOpenParenthesis() + 
  				getSpacesAroundParentheses() + 
  				a + 
  				getSpacesAroundParentheses() + 
  				getClosedParenthesis());} 
  				(	b=pr_StringLength 
  					{s.append(" " + b);} 
  				)? 
  			) 
  		  | c=pr_StringLength 
  		  	{s.append(" " + c );} 
  		  ) 
  	)
;


pr_AllowedValues returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; boolean first=true;}:
  	#(AllowedValues 
  		(	(	a=pr_ValueOrRange 
  				{if (first) s.append(a); else s.append(getComma() + a); first=false;}
  			)+
  		  | (	s=pr_CharStringMatch 
  		  	)
  		)
  	)
;

pr_CharStringMatch returns [StringBuffer s] {s=new StringBuffer(); StringBuffer b,d;}:
	#(CharStringMatch
		( 
			a:CSTRING 
			{s.append("pattern \"" + a.getText() + "\"");}
		  | b=pr_ReferencedValue
		  	{s.append("pattern " + b);}
		)
		(
			c:CSTRING 
			{s.append(" & \"" + c.getText() + "\"");}
		  | d=pr_ReferencedValue
		  	{s.append(" & " + d);}
		)*
	)
;

pr_ValueOrRange returns [StringBuffer s]:
	#( ValueOrRange (s=pr_RangeDef | s=pr_ConstantExpression | s=pr_Type))
;


pr_RangeDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
  #( RangeDef a=pr_LowerBound b=pr_UpperBound ) {s.append(a + " .. " + b);}
;


pr_StringLength returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(StringLength
     	a=pr_SingleConstExpression 
     	{s.append("length" + 
     	getOpenParenthesis() + 
     	getSpacesAroundParentheses() + 
     	a);} 
     	(	b=pr_UpperBound
     		{s.append(" .. " + b);}
     	)? 
     	{s.append(getSpacesAroundParentheses() + getClosedParenthesis());}
     )
;


pr_PortType returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
  #( PortType
     ( ( pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId {s.append(a);})? b=pr_Identifier {s.append(b);})
;


pr_PortDef returns [StringBuffer s]{s=new StringBuffer(); StringBuffer a,b;}:

  #( PortDef {s.append("port ");}
    a=pr_Identifier {s.append(a + " ");} b=pr_PortDefAttribs {s.append(b);})

;


pr_PortDefAttribs returns [StringBuffer s]:
   #(PortDefAttribs 
     (
    s=pr_MessageAttribs
  | s=pr_ProcedureAttribs
  | s=pr_MixedAttribs
   ))
;


pr_MessageAttribs returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:

  #( MessageAttribs {s.append("message" + getOpeningBrace()); currentNesting++;}
     ( 	(	a=pr_MessageList {s.append(a);}
     	  | a=pr_AddressDecl {s.append(a);}
     	  | a=pr_ConfigParamDef {s.append(a);}
     	) 
     	(	SemiColon {s.append(";");}
     	)? 
     	{s.append(getNewline());}
     )* 
   )
   {currentNesting--; s.append(getSpacing() + "}");}

;

pr_AddressDecl returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a;}: 
	#(AddressDecl
		{s.append(getSpacing()+"address ");}
		a=pr_Type {s.append(a);}
	)
;

pr_ConfigParamDef returns [StringBuffer s]:
	#(ConfigParamDef
		(	s=pr_MapParamDef 
		  | s=pr_UnmapParamDef
		)
	)
;

pr_MapParamDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(MapParamDef
		{s.append(getSpacing()+"map param"+getOpenParenthesis()+getSpacesAroundParentheses());}
		a=pr_FormalValuePar {s.append(a);}
		(	a=pr_FormalValuePar {s.append(getComma()+a);}
		)*
		{s.append(getSpacesAroundParentheses()+getClosedParenthesis());}
	)
;

pr_UnmapParamDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(UnmapParamDef
		{s.append(getSpacing()+"unmap param"+getOpenParenthesis()+getSpacesAroundParentheses());}
		a=pr_FormalValuePar {s.append(a);}
		(	a=pr_FormalValuePar {s.append(getComma()+a);}
		)*
		{s.append(getSpacesAroundParentheses()+getClosedParenthesis());}
	)
;

pr_MessageList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b; boolean first=true;}:

  #(MessageList
     a=pr_Direction {s.append(getSpacing() + a + " ");} ( AllKeyword {s.append("all");} | ( b=pr_Type {if (first) s.append(b); else s.append(getComma() + b); first=false;} )* ) )

;

pr_Direction returns [StringBuffer s] {s=new StringBuffer();}:
 
   #(Direction
    (
    InParKeyword {s.append("in");}
  | OutParKeyword {s.append("out");}
  | InOutParKeyword {s.append("inout");}
     ))
;


pr_ProcedureAttribs returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:

  #( ProcedureAttribs {s.append("procedure" + getOpeningBrace()); currentNesting++;}
     ( 	(	a=pr_ProcedureList {s.append(a);}
          | a=pr_AddressDecl {s.append(a);}
     	  | a=pr_ConfigParamDef {s.append(a);}
    	) 
     	(	SemiColon {s.append(";");}
     	)? 
     	{s.append(getNewline());}
     )* 
   )
   {currentNesting--;s.append(getSpacing() + "}");}

;

pr_ProcedureList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:

  #( ProcedureList
//     a=pr_Direction {s.append(a);} ( AllKeyword {s.append("all");} | ( b=pr_Signature {s.append(b);} )* ) )
     a=pr_Direction {s.append(getSpacing() + a + " ");} ( AllKeyword {s.append("all");} | b=pr_SignatureList {s.append(b);} ) ) 
;


pr_SignatureList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(SignatureList a=pr_Signature {s.append(a);} (b=pr_Signature {s.append(getComma() + b);})* )
;

pr_MixedAttribs returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
  #( MixedAttribs { s.append("mixed" + getOpeningBrace()); currentNesting++;}
     (	(	a=pr_MixedList {s.append(a);}
      	  | a=pr_AddressDecl {s.append(a);}
     	  | a=pr_ConfigParamDef {s.append(a);}
     	) 
     	(	SemiColon {s.append(";");}
     	)? 
     	{s.append(getNewline());}
     )* 
   )
   {currentNesting--; s.append(getSpacing() + "}");}
   
;


pr_MixedList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,c; boolean first=true;}:

  #( MixedList
     a=pr_Direction {s.append(getSpacing() + a + " ");} ( AllKeyword {s.append("all");} | ( c=pr_ProcOrType {if (first) s.append(c); else s.append(getComma() + c); first=false;})* ) )

;


pr_ProcOrType returns [StringBuffer s]:
	#(ProcOrType
		(s=pr_Type | s=pr_Signature)
	)
;


pr_ComponentDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d,e;}: 
{ boolean first=true;}
  #( ComponentDef
     a=pr_Identifier {s.append("component " + a);}
     ( 	{s.append(" extends ");} 
     	b=pr_ComponentType {s.append(b);} 
     	(	c=pr_ComponentType {s.append(getComma() + c);}
     	)*
     )?
     { 	s.append(getOpeningBrace()); 
     	currentNesting++;
     }
     ( 	d=pr_ComponentElementDef {s.append(getSpacing() + d);} 
     	(	e=pr_WithStatement {s.append(" "+e);}
     	)?
     	(	SemiColon {s.append(";");}
     	)? 
     	{s.append(getNewline());}
     )* 
   ) 
   {	currentNesting--; 
   		s.append(getSpacing() + "}");
   }

;


pr_ComponentType returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:

  #( ComponentType
     ( (pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId {s.append(a + ".");})? b=pr_Identifier {s.append(b);})

;


pr_ComponentElementDef returns [StringBuffer s]:

  #(ComponentElementDef
  (  s=pr_PortInstance
  | s=pr_VarInstance
  | s=pr_TimerInstance
  | s=pr_ConstDef
))
;


pr_PortInstance returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b; boolean first=true;}:
  #( PortInstance {s.append("port ");}
     a=pr_PortType {s.append(a + " ");} ( b=pr_PortElement {if (first) s.append(b); else s.append(getComma() + b); first=false;} )* )
;


pr_PortElement returns [StringBuffer s]{s=new StringBuffer(); StringBuffer a,b;}:

  #( PortElement
     a=pr_Identifier {s.append(a);} ( b=pr_ArrayDef {s.append(b);})? )

;




/***** 1.6.1.2 - Constant Definitions  *****/

pr_ConstDef returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b=null; boolean first=true;}: 
	#(ConstDef
     	a=pr_Type 
     	{s.append("const " + a + " ");} 
     	(	b=pr_SingleConstDef 
     		{if (first) s.append(b); else s.append(getComma() + b); first=false;}
     	)*
     )
;


pr_SingleConstDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c=null;} : 
  	#(SingleConstDef
    	a=pr_Identifier 
    	{s.append(a);} 
    	(	b=pr_ArrayDef 
    		{s.append(b);} 
    	)? 
    	c=pr_ConstantExpression 
    	{s.append(getAssignment() + c);} 
    )
;


/***** 1.6.1.3 - Template Definitions  *****/




pr_TemplateDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,d,c=null;}:
	#(TemplateDef 
		{s.append("template ");}
		(	d=pr_TemplateRestriction
		  	{s.append(d);}
		)?
     	a=pr_BaseTemplate 
     	{s.append(a);}
     	(	b=pr_DerivedDef 
     		{s.append(" " + b); }
     	)?
     	{s.append(getAssignment());}
     	c=pr_TemplateBody 
     	{s.append(c);}
     )
;


pr_BaseTemplate returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d=null;}:
  	#(BaseTemplate 
  		(	a=pr_Type 
  			{s.append(a);}
  		  | b=pr_Signature 
  		  	{s.append(b);}
  		)
  		c=pr_Identifier 
  		{s.append(" " + c);}
  		(	d=pr_TemplateFormalParList 
  			{s.append(getOpenParenthesis() + d + getClosedParenthesis());}
  		)?
  	)
;


pr_DerivedDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b=null;}:
 	#(DerivedDef 
 		{s.append("modifies ");}
 		(	a=pr_GlobalModuleId 
 			{s.append(a + ".");}
 		)?
 		b=pr_Identifier
 		{s.append(b);}
 	)
;

//New Entries

pr_TemplateRestriction returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(TemplateRestriction
		{s.append(getOpenParenthesis() + getSpacesAroundParentheses());} 
		(	a=pr_OmitKeyword
			{s.append(a);}
		  |	VALUE //add derivational rule?
	  		{s.append("value");}	
		  |	PRESENT ////add derivational rule?
	  		{s.append("present");}	
		)
		{s.append(getSpacesAroundParentheses() + getClosedParenthesis() + " ");}
	)
;

pr_RestrictedTemplate returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(RestrictedTemplate
		(	(	a=pr_OmitKeyword
				{s.append(a + " ");} //been looking for that empty space for a couple of hours !!!
			)
		  | (	TemplateKeyword
		  		{s.append("template");}
		  		b=pr_TemplateRestriction
		  		{s.append(b);}
		  	)
		)
	)
;

//---


pr_TemplateFormalParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b; boolean multiparam=false;}:
	a=pr_TemplateFormalPar 
//	{s.append(a);} 
	(	b=pr_TemplateFormalPar 
//		{s.append(getComma() + b);}
		{multiparam=true;}
		{a.append(getComma() + getNewlineBeforeFormalPar() + b);}	
 	)*
// 	{if (multiparam) s.append(getNewlineBeforeFormalPar()); else s.append(getSpacesAroundParentheses());}
//    {s.append(a);}
// 	{if (multiparam) s.append(getNewlineAfterFormalParList()); else s.append(getSpacesAroundParentheses());}

//optimized to:
    {s.append(a);}

 	{if (multiparam) {
 		s.insert(0,getNewlineBeforeFormalPar());
 		s.insert(0,getSpacesAroundParentheses());
 		s.append(getNewlineAfterFormalParList());
 	 } else {
 	 	s.insert(0,getSpacesAroundParentheses());
 		s.append(getSpacesAroundParentheses());
 	 }
 	}

 	
;


pr_TemplateFormalPar returns [StringBuffer s]:
    s=pr_FormalValuePar | s=pr_FormalTemplatePar
;


pr_TemplateBody returns [StringBuffer s]{StringBuffer a;}:
	#(TemplateBody 
		(	s=pr_SimpleSpec
	  	  | s=pr_FieldSpecList
		  | s=pr_ArrayValueOrAttrib
		) 
		(	a=pr_ExtraMatchingAttributes 
			{s.append(" " + a);}
		)? 
	)
;


pr_SimpleSpec returns [StringBuffer s] {StringBuffer a;}:
  	s=pr_SingleValueOrAttrib
/*  	(	(	s=pr_SingleExpression
			(	a=pr_SingleTemplateSpec {s.append(" & "+a);}
			)?
		)
	  | (	s=pr_SingleTemplateSpec
	  	)
	)
*/	
;

pr_SingleTemplateSpec returns [StringBuffer s]:
	s=pr_SingleTemplateExpression
;

pr_SingleTemplateExpression returns [StringBuffer s] {StringBuffer a,b;}:
	(	(	s=pr_MatchingSymbol
		  | (	s=pr_TemplateRefWithParList
		  		(	a=pr_ExtendedFieldReference {s.append(a);}
		  		)?
		  	)
		)
		(	b=pr_SimpleSpec {s.append(" & "+b);}
		)? 
	)
;

pr_FieldSpecList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; boolean first=true;}:
  #( FieldSpecList {s.append(getOpeningBraceNoSpace()); currentNesting++;}
     ( a=pr_FieldSpec {if (first) s.append(getSpacing() + a ); else s.append(getComma() + getNewline() + getSpacing() + a); first=false;})* 
   ) {currentNesting--;s.append(getNewline() + getSpacing() + "}");}

;


pr_FieldSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:

  #( FieldSpec
     a=pr_FieldReference 
     (	b=pr_TemplateBody {s.append(a + getAssignment() + b);}
      | Minus {s.append(a + getAssignment() + "-");}
     )
  )
;


pr_FieldReference returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a = null;}:

 #(FieldReference
 (
    (pr_StructFieldRef)=> a=pr_StructFieldRef
  | a=pr_ArrayOrBitRef
  | (pr_ParRef )=>a=pr_ParRef
))
{
	s.append(a);
}
;


pr_StructFieldRef returns [StringBuffer s] {s = new StringBuffer();}:
  s=pr_PredefinedType | s=pr_Identifier
;


pr_ParRef returns [StringBuffer s] {s = new StringBuffer();}:
  s=pr_SignatureParIdentifier
;


pr_SignatureParIdentifier returns [StringBuffer s] {s = new StringBuffer();}:
 s=pr_Identifier
;


pr_ArrayOrBitRef returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a=null;}:
  #( ArrayOrBitRef a=pr_SingleExpression {s.append("[" + a + "]");})
;


pr_SingleValueOrAttrib returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d,e,f;}:
#(SingleValueOrAttrib
(
    ( pr_TemplateRefWithParList ) => a=pr_TemplateRefWithParList {s.append(a);}
  | c=pr_MatchingSymbol {s.append(c);}
  | e=pr_SingleExpression {s.append(e);}
)
(	f=pr_SimpleSpec {s.append(" & "+f);}
)?

)
;


pr_ArrayValueOrAttrib returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; boolean first=true;}:
	#(ArrayValueOrAttrib 
		{s.append("{");}
		(	a=pr_ArrayElementSpec 
			{if (first) s.append(a); else s.append(getComma() + a); first=false;} 
		)* 
		{s.append("}");}
	)
;


pr_ArrayElementSpec returns [StringBuffer s] {s=new StringBuffer();}:
#(ArrayElementSpec
	(NotUsedSymbol {s.append("-");} | s=pr_TemplateBody | s=pr_PermutationMatch)
 )
;

pr_PermutationMatch returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(PermutationMatch a=pr_ListOfTemplates {s.append("permutation" + a);})
;
/*
pr_PermutationList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	a=pr_TemplateBody 
	{s.append(getOpenParenthesis() + getSpacesAroundParentheses() +	a);} 
	(	b=pr_TemplateBody 
		{s.append(getComma() + b);}
	)* 
	{s.append(getSpacesAroundParentheses() + getClosedParenthesis());}
;
*/
/*
pr_TemplateBody returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d=null;}:
	#(TemplateBody
		(	(	a=pr_SimpleSpec 
			{s.append(a);} 
		  | b=pr_FieldSpecList 
		  	{s.append(b);}
		  | c=pr_ArrayValueOrAttrib 
		  	{s.append(c);} 
			) 
		  | (	d=pr_ExtraMatchingAttributes 
		  		{s.append(d);}
		  	)? 
	 	) 
	)
;
*/

pr_MatchingSymbol returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
 #(MatchingSymbol
 (  
    s=pr_Complement
  | (	AnyValue {s.append("?");}
  		(	a=pr_WildcardLengthMatch {s.append(" "+a);}	
  		)?
  	)
  | (	AnyOrOmit {s.append("*");}
  		(	a=pr_WildcardLengthMatch {s.append(" "+a);}	
  		)?
  	)
  | s=pr_ListOfTemplates
  | s=pr_Range
  | (pr_BitStringMatch)=> s=pr_BitStringMatch
  | s=pr_HexStringMatch
  | s=pr_OctetStringMatch
  | s=pr_CharStringMatch
  | (pr_SubsetMatch)=> s=pr_SubsetMatch
  | (pr_SupersetMatch) => s=pr_SupersetMatch 

))
;

pr_ListOfTemplates returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ListOfTemplates
		(	{s.append(getOpenParenthesis() + getSpacesAroundParentheses());}
			a=pr_TemplateListItem {s.append(a);}
			(	b=pr_TemplateListItem 
				{s.append(getComma() + b);}
			)*
			{s.append(getSpacesAroundParentheses() + getClosedParenthesis());}
		)
	)
;

pr_TemplateListItem returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	(
		a=pr_TemplateBody {s.append(a);}
	  | b=pr_AllElementsFrom {s.append(b);}
	)
;

pr_AllElementsFrom  returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(AllElementsFrom
		(
			a=pr_TemplateBody
			{s.append("all"+" "+"from"+" "+a);}
		)
	)
;


pr_ValueOrAttribList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ValueOrAttribList
		(	{s.append(getOpenParenthesis() + getSpacesAroundParentheses());}
			a=pr_TemplateBody {s.append(a);}
			(	b=pr_TemplateBody 
				{s.append(getComma() + b);}
			)*
			{s.append(getSpacesAroundParentheses() + getClosedParenthesis());}
		)
	)
;


pr_ExtraMatchingAttributes returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ExtraMatchingAttributes 
		(
			(a=pr_LengthMatch {s.append(" " + a);} (IfPresentMatch {s.append(" ifpresent");})? ) | IfPresentMatch {s.append(" ifpresent");} 
		)
	)
//    (pr_LengthMatch)=> pr_LengthMatch
//  | IfPresentMatch
//  | pr_LengthMatch IfPresentMatch
//	)
;

pr_SubsetMatch returns [StringBuffer s]{s=new StringBuffer(); StringBuffer a;}:
   #(SubsetMatch a=pr_ListOfTemplates {s.append("subset" + a);})
;

pr_PermutaionMatch returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
   a=pr_ValueList {s.append("permutation" + a);}
;

pr_SupersetMatch returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
   #(SupersetMatch a=pr_ListOfTemplates {s.append("superset" + a);})
;

pr_BitStringMatch returns [StringBuffer s] {s=new StringBuffer();}:
  #(BitStringMatch 
  	(//	a:BSTRING 
  	 //	{s.append("'" + a.getText() + "'B");}
  	 // | 
  	    b:BSTRINGMATCH 
  	    {s.append("'" + b.getText() + "'B");}
  	) 
  )
;


pr_HexStringMatch returns [StringBuffer s] {s=new StringBuffer();}:
  #(HexStringMatch 
  	( //a:HSTRING {s.append("'" + a.getText() + "'H");}
  	  //| 
  	    b:HSTRINGMATCH {s.append("'" + b.getText()+ "'H");}
  	) 
  )
;


pr_OctetStringMatch returns [StringBuffer s] {s=new StringBuffer();}:
  #(OctetStringMatch 
  	( //a:OSTRING {s.append("'" + a.getText() + "'O");}
  	  //| 
  	    b:OSTRINGMATCH {s.append("'" + b.getText()+ "'O");}
  	) 
  )
;

pr_Complement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#( Complement {s.append("complement");}
    	(	a=pr_ListOfTemplates //changed from pr_ValueList
    	) 
    	{s.append(a);})

;


pr_ValueList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; boolean first=true;}:
	#(ValueList 
		{s.append(getOpenParenthesis());}
		(	a=pr_ConstantExpression 
			{if (first) s.append(a); else s.append(getComma() + a); first=false;}
		)* 
		{if (!first) {
			s.insert(0,getSpacesAroundParentheses()); 
			s.append(getSpacesAroundParentheses());
		 }
		}
		{s.append(getClosedParenthesis());} 
	)
;


pr_LengthMatch returns [StringBuffer s]:
	s=pr_StringLength 
;

pr_WildcardLengthMatch returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(WildcardLengthMatch
		{s.append("length"+getOpenParenthesis()+getSpacesAroundParentheses());}
		a=pr_ConstantExpression {s.append(a);}
		{s.append(getSpacesAroundParentheses()+getClosedParenthesis());}
	)
;


pr_Range returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
 	#(Range 
 		a=pr_LowerBound 
 		{s.append(getOpenParenthesis()+getSpacesAroundParentheses()+a+" .. ");} 
 		b=pr_UpperBound 
 		{s.append(b+getSpacesAroundParentheses()+getClosedParenthesis());})

;

pr_LowerBound returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#( LowerBound
		(	ExclamationMark {s.append("!");}
		)? 
		(	a=pr_SingleConstExpression {s.append(a);}
	      | (	Minus Infinity) {s.append("-infinity");}
	    )
	)
;

pr_UpperBound returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#( UpperBound 
		(	ExclamationMark {s.append("!");}
		)? 
		(	a=pr_SingleConstExpression {s.append(a);}
		  | Infinity {s.append("infinity");}
		)
	)
;

pr_TemplateInstance returns [StringBuffer s]:
	#(TemplateInstance s=pr_InLineTemplate)
;


pr_TemplateInstanceAssignment returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(TemplateInstanceAssignment 
		a=pr_Identifier
		b=pr_InLineTemplate
		{s.append(a).append(getAssignment()).append(b);}
		
	)
;



pr_TemplateRefWithParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(TemplateRefWithParList 
		(	a=pr_GlobalModuleId 
			{s.append(a + ".");}
		)? 
		b=pr_Identifier 
		{s.append(b);} 
		(	c=pr_TemplateActualParList 
			{s.append(getOpenParenthesis() + 
			getSpacesAroundParentheses() + 
			c + 
			getSpacesAroundParentheses() +
			getClosedParenthesis());}
		)?
	)
;

pr_InLineTemplate returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d;}:
#(InLineTemplate
  		( a=pr_Type {s.append(a + ":");} )? 
        ( c=pr_DerivedRefWithParList {s.append(c + getAssignment());} )? 
        d=pr_TemplateBody {s.append(d);}
)
;

pr_DerivedRefWithParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
  #( DerivedRefWithParList b=pr_TemplateRefWithParList {s.append(" modifies " + b);} )
;

pr_TemplateActualParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; boolean first=true;}:
  #( TemplateActualParList
  	(
	     (pr_TemplateActualPar)=>( 	a=pr_TemplateActualPar 
	     	{	if (first) 
	     			s.append(a); 
	     		else 
	     			s.append(getComma() + a); 
	     		first=false;
	     	}
	     )*
	   | (  a=pr_TemplateActualParAssignment 
	     	{	if (first) 
	     			s.append(a); 
	     		else 
	     			s.append(getComma() + a); 
	     		first=false;
	     	}
	   	 )*
   	) 
  )
;

pr_TemplateActualPar returns [StringBuffer s = new StringBuffer();]:
  	#(TemplateActualPar
  		( 	s=pr_TemplateInstance
  		  | Dash {s.append("-");}
  		)
  	)
;

pr_TemplateActualParAssignment returns [StringBuffer s = new StringBuffer();]:
  	#(TemplateActualParAssignment
  		s=pr_TemplateInstanceAssignment
  	)
;


pr_TemplateOps returns [StringBuffer s]:
	#(TemplateOps
		(	s=pr_MatchOp
		  | s=pr_ValueofOp
		)
	)
;


pr_MatchOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
  	#(MatchOp
  	{s.append("match" + getOpenParenthesis() + getSpacesAroundParentheses());}
    a=pr_Expression 
    {s.append(a + getComma());} 
    b=pr_TemplateInstance 
    {s.append(b + getSpacesAroundParentheses() + getClosedParenthesis());})
;


pr_ValueofOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
  	#(ValueofOp 
  		a=pr_TemplateInstance 
  		{s.append("valueof" +
  		getOpenParenthesis() + 
  		getSpacesAroundParentheses() + 
  		a + 
  		getSpacesAroundParentheses() + 
  		getClosedParenthesis());} 
  	)

;




/***** 1.6.1.4 - Function Definitions  *****/




pr_FunctionDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d,e;}: 
	#(FunctionDef
		{s.append("function ");} 
		a=pr_Identifier 
		{s.append(a).append(getOpenParenthesis());} 
		(	b=pr_FunctionFormalParList 
			{s.append(b);}
		)? 
		{s.append(getClosedParenthesis());} 
		(	c=pr_RunsOnSpec 
			{s.append(getNewlineBeforeRunsOn() + "runs on " + c);}
		)? 
		(	d=pr_ReturnType 
			{s.append(" " + d);}
		)? 
		{s.append(getOpeningBrace()); currentNesting++; } 
		e=pr_StatementBlock 
		{currentNesting--; s.append(e).append(getSpacing() + "}");}
	)
;


pr_FunctionFormalParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b; boolean multiparam=false;}:
	a=pr_FunctionFormalPar 
//    {s.append(a);} 
    (	b=pr_FunctionFormalPar 
//     	{s.append(getComma() + getNewlineBeforeFormalPar() + b);}
		{multiparam=true;}
		{a.append(getComma() + getNewlineBeforeFormalPar() + b);}
    )*
//    {if (multiparam) s.append(getNewlineBeforeFormalPar());}
//    {s.append(a);}
//    {if (multiparam) s.append(getNewlineAfterFormalParList());}
    
    {s.append(a);}

 	{if (multiparam) {
 		s.insert(0,getNewlineBeforeFormalPar());
 		s.insert(0,getSpacesAroundParentheses());
 		s.append(getNewlineAfterFormalParList());
 	 } else {
 	 	s.insert(0,getSpacesAroundParentheses());
 		s.append(getSpacesAroundParentheses());
 	 }
 	}
    
;


pr_FunctionFormalPar returns [StringBuffer s]:
    s=pr_FormalValuePar
  | s=pr_FormalTimerPar
  | s=pr_FormalTemplatePar
  | s=pr_FormalPortPar
;


pr_ReturnType returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ReturnType 
  		{s.append(getNewlineBeforeReturn() + "return ");} 
  		(	TemplateKeyword 
  			{s.append("template ");}
  		  | b=pr_RestrictedTemplate
  		  	{s.append(b);}
  		)? 
  		a=pr_Type 
  		{s.append(a);} 
  	)
;


pr_RunsOnSpec returns [StringBuffer s]:
  	#(RunsOnSpec 
  		s=pr_ComponentType
  	)
;


pr_StatementBlock returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;;}:
	#(StatementBlock
		(	(	(	a=pr_FunctionLocalDefOrInst
					{s.append(getSpacing() + a);} 
				)
		  	  | (	b=pr_FunctionStatement
		  	  		{s.append(getSpacing() + b);}
					(	SemiColon	
						{s.append(";");}
					)?
				)
			)
			{s.append(getNewline());}
		)*
	)
;


pr_FunctionLocalDefOrInst returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
//	#(upper:FunctionLocalDefOrInst
		( 	(  	s=pr_FunctionLocalDef
		  	  | s=pr_FunctionLocalInst
		  	)	 
		)
		(	a=pr_WithStatement {s.append(" "+a);}
     	)?
		(	SemiColon
			{s.append(";");}
		)?
//	)
;

//TODO: deprecated
pr_FunctionStatementList returns [StringBuffer s] {s=new StringBuffer();}:
		(	s=pr_FunctionStatement
			(	SemiColon
				{s.append(";");}
			)?
		)*
;

/* 
//TODO: Deprecated
pr_FunctionStatementOrDef returns [StringBuffer s]:
	#(FunctionStatementOrDef
		(	s=pr_FunctionLocalDef
		  | s=pr_FunctionLocalInst
		  | s=pr_FunctionStatement
		)
	)
;
*/

pr_FunctionLocalInst returns [StringBuffer s]:
	#(FunctionLocalInst
		(	s=pr_VarInstance
		  | s=pr_TimerInstance
		)
	)
;


pr_FunctionLocalDef returns [StringBuffer s]:
	#(FunctionLocalDef
		(	s=pr_ConstDef|s=pr_TemplateDef
		)
	)
;


pr_FunctionStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(FunctionStatement
		(   a=pr_ConfigurationStatements
		  | a=pr_TimerStatements
		  | a=pr_CommunicationStatements
		  | a=pr_BasicStatements
		  | a=pr_BehaviourStatements
		  | a=pr_VerdictStatements
		  | a=pr_SUTStatements
		  | a=pr_TestcaseOperation
	 	) 
	 	{s.append(a);} 
	 )
;


pr_FunctionInstance returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
  	#(FunctionInstance 
    	a=pr_FunctionRef 
    	{s.append(a);} 
    	{s.append(getOpenParenthesis());} 
    	(	b=pr_FunctionActualParList 
    		{s.append(b);} 
    	)? 
    	{s.append(getClosedParenthesis());}
    )
;


pr_FunctionRef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(FunctionRef
		(	(	(	pr_GlobalModuleId pr_Identifier 
				) => a=pr_GlobalModuleId 
				{s.append(a + ".");}
			)? 
			b=pr_Identifier 
			{s.append(b);}
	  	)
	)
;


pr_FunctionActualParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; boolean first=true;}:
 	#(FunctionActualParList   // -> FunctionActualParList
 		(	(pr_FunctionActualPar)=>(	a=pr_FunctionActualPar 
 				{if (first) s.append(a); else s.append(getComma() + a); first=false;}
	 		)*
	 	  | (	a=pr_FunctionActualParAssignment 
 				{if (first) s.append(a); else s.append(getComma() + a); first=false;}
	 	  	)*
	 	)	
 		{	if (!first) {
				s.insert(0,getSpacesAroundParentheses()); 
				s.append(getSpacesAroundParentheses());
		 	}
		}
 	)
;


pr_FunctionActualParAssignment returns [StringBuffer s]:
	#(FunctionActualParAssignment 
		(	(pr_TemplateInstanceAssignment ) => s=pr_TemplateInstanceAssignment
		  | (pr_ComponentRefAssignment)=>s=pr_ComponentRefAssignment
		  | (pr_PortRefAssignment)=>s=pr_PortRefAssignment
		  | s=pr_TimerRefAssignment
		)
	)
;

pr_FunctionActualPar returns [StringBuffer s = new StringBuffer();]:
	#(FunctionActualPar 
		(
			(pr_TemplateInstance) => s=pr_TemplateInstance
		  | (pr_TimerRef) => s=pr_TimerRef
		  | (pr_Port) => s=pr_Port
		  | s=pr_ComponentRef
		  | Dash {s.append("-");}
		)
	)
;




/*****1.6.2.5 - Signature Definitions  *****/

pr_SignatureDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d;}:
	#( SignatureDef 
		{s.append("signature ");}
		a=pr_Identifier 
		{s.append(a);s.append(getOpenParenthesis());} 
		(	b=pr_SignatureFormalParList 
			{s.append(b);} 
		)? 
		{s.append(getClosedParenthesis());}
		(	c=pr_ReturnType 
			{s.append(" " +c);}
		  | NoBlockKeyword 
		  	{s.append(" noblock");})? 
		(	d=pr_ExceptionSpec 
			{s.append(" " + d);}
		)? 
	)
;

pr_SignatureFormalParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; StringBuffer b=new StringBuffer(); boolean first=true; boolean multiparam=false;}:
	#(SignatureFormalParList
		(	a=pr_SignatureFormalPar 
//			{if (first) s.append(a); else s.append(getComma() + a); first=false;}
			{if (first) {b.append(a); first=false;} else {b.append(getComma() + getNewlineBeforeFormalPar() + a); multiparam=true;} }
		)* 
//		{if (multiparam) s.append(getNewlineBeforeFormalPar());}
//		{s.append(b);} 
//		{if (multiparam) s.append(getNewlineAfterFormalParList());}
		
		{s.append(b);}

	 	{if (multiparam) {
	 		s.insert(0,getNewlineBeforeFormalPar());
			s.insert(0,getSpacesAroundParentheses());
	 		s.append(getNewlineAfterFormalParList());
	 	 } else {
	 	 	s.insert(0,getSpacesAroundParentheses());
	 		s.append(getSpacesAroundParentheses());
	 	 }
	 	}
 	
		
	)
;


pr_SignatureFormalPar returns [StringBuffer s]:
  	s=pr_FormalValuePar
;


pr_ExceptionSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ExceptionSpec 
     	{s.append(getNewlineBeforeExceptionSpec() + 
     	"exception" +
     	getOpenParenthesis() +
     	getSpacesAroundParentheses());} 
     	a=pr_Type 
     	{s.append(a);} 
     	(	b=pr_Type 	
     		{s.append(getComma() + b);}
     	)* 
     	{s.append(getSpacesAroundParentheses() + getClosedParenthesis());}) 
;


pr_Signature returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b=null;}:

#(Signature(
  ( ( pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId {s.append(a + ".");})? b=pr_Identifier {s.append(b);}
))
;




/***** 1.6.2.6 - Testcase Definitions  *****/

pr_TestcaseDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d;}: 
	#(TestcaseDef 
		{s.append("testcase " );}
		a=pr_Identifier 
		{s.append(a);}
		{s.append(getOpenParenthesis());} 
		(	b=pr_TestcaseFormalParList 
			{s.append(b);} 
		)? 
		{s.append(getClosedParenthesis());}
		c=pr_ConfigSpec 
		{s.append(c);}
		{s.append(getOpeningBrace()); currentNesting++;}
		d=pr_StatementBlock 
		{currentNesting--; s.append(d + getSpacing() + "}");}
	)
;


pr_TestcaseFormalParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b; boolean multiparam=false;}:
	a=pr_TestcaseFormalPar 
//	{s.append(a);} 
	(	b=pr_TestcaseFormalPar 
//		{s.append(getComma() + b);}
		{multiparam=true;}
		{a.append(getComma() + getNewlineBeforeFormalPar() + b);}
	)*
//	{if (multiparam) s.append(getNewlineBeforeFormalPar());}
//    {s.append(a);}
//    {if (multiparam) s.append(getNewlineAfterFormalParList());}
    {s.append(a);}

 	{if (multiparam) {
 		s.insert(0,getNewlineBeforeFormalPar());
		s.insert(0,getSpacesAroundParentheses());
 		s.append(getNewlineAfterFormalParList());
 	 } else {
 	 	s.insert(0,getSpacesAroundParentheses());
 		s.append(getSpacesAroundParentheses());
 	 }
 	}


;


pr_TestcaseFormalPar returns [StringBuffer s]:
    s=pr_FormalValuePar
  | s=pr_FormalTemplatePar

;


pr_ConfigSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ConfigSpec
     	{s.append(getNewlineBeforeRunsOn() + "runs on ");} 
     	a=pr_RunsOnSpec 
     	{s.append(a);} 
     	(	b=pr_SystemSpec 
     		{s.append(getNewlineBeforeSystem() + "system " + b);}
     	)?
     )
;


pr_SystemSpec returns [StringBuffer s]:
  	#(SystemSpec
     	s=pr_ComponentType 
     )
;


pr_TestcaseInstance returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d;}:
	#(TestcaseInstance
		( 	{s.append("execute" + getOpenParenthesis() + getSpacesAroundParentheses());}
			a=pr_TestcaseRef 
			{s.append(a);} 
			{s.append(getOpenParenthesis());}
			(	b=pr_TestcaseActualParList 
				{s.append(b);}
			)? 
			{s.append(getClosedParenthesis());}
			(	{s.append(getComma());}	
				(	Minus {s.append("-");}
			  	  |	c=pr_TimerValue {s.append(c);}
			  	)
			  	(	d=pr_SingleExpression {s.append(getComma()+d);}
			  	)?
			)?
			{s.append(getSpacesAroundParentheses() + getClosedParenthesis());}
		)
	)
;


pr_TestcaseRef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
 	#(	TestcaseRef
     	(	(	pr_GlobalModuleId pr_Identifier
     		) => a=pr_GlobalModuleId 
     		{s.append(a + ".");}
     	)? 
     	b=pr_Identifier 
     	{s.append(b);}
     )
;


pr_TestcaseActualParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; boolean first=true;}:
	#( TestcaseActualParList 
		(	(pr_TestcaseActualPar)=>(	a=pr_TestcaseActualPar 
				{if (first) s.append(a); else s.append(getComma() + a); first=false; } 
			)*
		  | (	a=pr_TestcaseActualParAssignment
				{if (first) s.append(a); else s.append(getComma() + a); first=false; } 
		  	)*	
		)
		{	if (!first) {
				s.insert(0,getSpacesAroundParentheses()); 
				s.append(getSpacesAroundParentheses());
		 	}
		}		
	)
;

pr_TestcaseActualParAssignment returns [StringBuffer s]:
	#(TestcaseActualParAssignment 
		s=pr_TemplateInstanceAssignment
	)
;

pr_TestcaseActualPar returns [StringBuffer s = new StringBuffer();]:
  	#(TestcaseActualPar 
  		(	s=pr_TemplateInstance
    	  | Dash {s.append("-");}
    	)
  	)
;

/***** 1.6.1.7 - Altstep Definitions  *****/

pr_AltstepDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d,e;}: 
	#(AltstepDef 
		{s.append("altstep ");}
		(	a=pr_Identifier 
			{s.append(a + getOpenParenthesis());}
			(	b=pr_AltstepFormalParList 
				{s.append(b);} 
			)? 
			{s.append(getClosedParenthesis());}
			(	c=pr_RunsOnSpec 
				{s.append(getNewlineBeforeRunsOn() + "runs on " + c);} 
			)?
			{s.append(getOpeningBrace());currentNesting++; }
			(	d=pr_AltstepLocalDefList {s.append(d);}
			)?
			(	e=pr_AltGuardList {s.append(e);}
			)?
			{ currentNesting--; s.append(getSpacing()+ "}");}
		)
	)

;

pr_AltstepFormalParList returns [StringBuffer s]:
  	s=pr_FunctionFormalParList
;

pr_AltstepLocalDefList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
// see parser.
		#(AltstepLocalDefList
		    a=pr_AltstepLocalDef {s.append(getSpacing() + a);}
		    (	b=pr_WithStatement {s.append(" "+b);}
     		)?
			(
				(SemiColon {s.append(";" + getNewline());})? 
				a=pr_AltstepLocalDef {s.append(getSpacing() + a);}
				(	b=pr_WithStatement {s.append(" "+b);}
     			)?
			)*	
		    /* obligatory SemiColon */ {s.append(";" + getNewline());}
		)
;

pr_AltstepLocalDef returns [StringBuffer s]:
#(AltstepLocalDef(
     s=pr_VarInstance 
   | s=pr_TimerInstance 
   | s=pr_ConstDef 
   | s=pr_TemplateDef))
;


pr_AltstepInstance returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(AltstepInstance   
    	a=pr_AltstepRef 
    	{s.append(a+getOpenParenthesis());} 
    	(	b=pr_FunctionActualParList 
    		{s.append(b);}
    	)? 
    	{s.append(getClosedParenthesis());})
;


pr_AltstepRef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	(	(	pr_GlobalModuleId pr_Identifier 
		) => a=pr_GlobalModuleId 
		{s.append(a + ".");} 
	)? 
	b=pr_Identifier 
	{s.append(b);}
;




/***** 1.6.1.8 - Import Definitions  *****/




pr_ImportDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(ImportDef
		(
			a=pr_ImportFromSpec {s.append(a);}
			( b=pr_AllWithExcepts {s.append(b);} | c=pr_ImportSpec {s.append(c);} )
		)
	)
;

pr_AllWithExcepts returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; boolean withExcepts=false;}:
	#(AllWithExcepts {s.append(" all");}
		( 
			( {withExcepts=true; s.append(" except" + getOpeningBrace()); currentNesting++; } a=pr_ExceptsDef {s.append(a); currentNesting--;s.append(getSpacing() + "}");})? 
		)
//		{if (!withExcepts) s.append(";");}
	)
;

pr_ExceptsDef returns [StringBuffer s]:
	#(ExceptsDef s=pr_ExceptSpec)
;

pr_ExceptSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ExceptSpec (a=pr_ExceptElement {s.append(getSpacing() + a);} (SemiColon {s.append(";");})? {s.append(getNewline());})*)
;

pr_ExceptElement returns [StringBuffer s]:
	#(ExceptElement
		(
		s=pr_ExceptGroupSpec
		| s=pr_ExceptTypeDefSpec 
		| s=pr_ExceptTemplateSpec 
		| s=pr_ExceptConstSpec 
		| s=pr_ExceptTestcaseSpec 
		| s=pr_ExceptAltstepSpec  
		| s=pr_ExceptFunctionSpec 
		| s=pr_ExceptSignatureSpec 
		| s=pr_ExceptModuleParSpec 
		)
	)
;

//////////////////////////////////////////////////////////////

pr_ExceptGroupSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
   
   #(ExceptGroupSpec {s.append("group ");}
//     ( (a=pr_Identifier {s.append(a);})+ | AllKeyword {s.append("all");})
     ( a=pr_ExceptGroupRefList {s.append(a);} | AllKeyword {s.append("all");})
   )
;

pr_ExceptGroupRefList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ExceptGroupRefList (a=pr_FullGroupIdentifier {s.append(a);}) (b=pr_FullGroupIdentifier {s.append(getComma() + b);})*)
;


pr_ExceptTypeDefSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
   
  #(ExceptTypeDefSpec {s.append("type ");}
     (
     a=pr_TypeRefList {s.append(a);}
     |
     AllKeyword {s.append("all");}
     )
    )
;


pr_ExceptTemplateSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
  
  #(ExceptTemplateSpec {s.append("template ");}
     (
       a=pr_TemplateRefList {s.append(a);}
     | 
       AllKeyword {s.append("all");}
    )
  )
; 


pr_ExceptConstSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
    #(ExceptConstSpec {s.append("const ");}
    
    (
     a=pr_ConstRefList {s.append(a);}
     |
      AllKeyword {s.append("all");}
     )
    )
;


pr_ExceptTestcaseSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
    #(ExceptTestcaseSpec {s.append("testcase ");}
    
     (
      a=pr_TestcaseRefList {s.append(a);}
      |
      AllKeyword  {s.append("all");}
     )
    )
;


pr_ExceptAltstepSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
     
     #(ExceptAltstepSpec {s.append("altstep ");}
      
       (
       a=pr_AltstepRefList {s.append(a);}
       | 
       AllKeyword {s.append("all");}
       )
    )
;


pr_ExceptFunctionSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
        #(ExceptFunctionSpec {s.append("function ");}
        
          (
          a=pr_FunctionRefList {s.append(a);}
          | 
          AllKeyword {s.append("all");}
          )
        )
;


pr_ExceptSignatureSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
         #(ExceptSignatureSpec {s.append("signature ");}
         
            (
           a=pr_SignatureRefList {s.append(a);}
            | 
          AllKeyword {s.append("all");} 
            )
         )
;


pr_ExceptModuleParSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
        
          #(ExceptModuleParSpec {s.append("modulepar ");}
           
           (
			a=pr_ModuleParRefList {s.append(a);}
             | 
             AllKeyword {s.append("all");}
           )
          )
;          

//////////////////////////////////////////////////////////////////

pr_ImportSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; }:
	#(ImportSpec {s.append(getOpeningBrace()); currentNesting++; }
		(
			( a=pr_ImportElement {s.append(getSpacing() + a); } (SemiColon {s.append(";");})? {s.append(getNewline());})*
		)
		{currentNesting--; s.append(getSpacing() + "}");}
	)
;

pr_ImportElement returns [StringBuffer s]:
	#(ImportElement (
		s=pr_ImportGroupSpec
		| s=pr_ImportTypeDefSpec
		| s=pr_ImportTemplateSpec
		| s=pr_ImportConstSpec
		| s=pr_ImportTestcaseSpec
		| s=pr_ImportAltstepSpec
		| s=pr_ImportFunctionSpec
		| s=pr_ImportSignatureSpec
		| s=pr_ImportModuleParSpec
		| s=pr_ImportImportSpec
		)
	)
;

pr_ImportImportSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ImportImportSpec ( {s.append("import all");} ))
;

pr_ImportFromSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:

  #( ImportFromSpec {s.append("import from ");}
     a=pr_ModuleId {s.append(a);} ( RecursiveKeyword {s.append(" recursive");} )?)

;


pr_ModuleId returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ModuleId (a=pr_GlobalModuleId {s.append(a);} ( b=pr_LanguageSpec {s.append(" language " + b);} )?) )
;


pr_LanguageSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(LanguageSpec
		a=pr_FreeText
		{s.append(a);}
		(	a=pr_FreeText
			{s.append(getComma()+a);}
		)*
	)
;


pr_GlobalModuleId returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a;}:
	#(GlobalModuleId (s=pr_Identifier) ({s.append(".");}a=pr_ObjectIdentifierValue {s.append(a);})?)
;

// ---

pr_ObjectIdentifierValue returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b,c,d;}:
	#(OBJID 
		{s.append("objid { ");}
		(
		(a=pr_ReferencedValue {s.append(a);}
		| b=pr_NumberForm {s.append(b);}
		| c=pr_NameAndNumberForm {s.append(c);}
		| d=pr_NameForm {s.append(d);})
		{s.append(" ");}
		)+
		{s.append(" }");}
	)
;

pr_NumberForm returns [StringBuffer s] {s = new StringBuffer();}:
	#(NumberForm a:NUMBER {s.append(a.getText());})
;

pr_NameAndNumberForm returns [StringBuffer s] {s = new StringBuffer(); StringBuffer b = new StringBuffer();}:
	#(NameAndNumberForm 
		a:IDENTIFIER 
		b=pr_NumberForm 
		{s.append(a.getText() + 
		getOpenParenthesis() + 
		b.toString() + 
		getClosedParenthesis());})
;

pr_NameForm returns [StringBuffer s] {s = new StringBuffer();}:
	#(NameForm a:IDENTIFIER {s.append(a.getText());})
;

// -----

pr_ImportModuleParSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ImportModuleParSpec ( {s.append("modulepar ");} (a=pr_ModuleParRefList | a=pr_AllModuleParWithExcept) {s.append(a);}))
;

pr_AllModuleParWithExcept returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(AllModuleParWithExcept {s.append("all");} ({s.append(" except ");} a=pr_ModuleParRefList {s.append(a);})?)
;

pr_ModuleParRefList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ModuleParRefList a=pr_Identifier {s.append(a);} (b=pr_Identifier {s.append(getComma() + b);})*)
;

pr_ImportGroupSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ImportGroupSpec 
		( {s.append("group ");}
			(a=pr_GroupRefListWithExcept | a=pr_AllGroupsWithExcept) {s.append(a);}
		)
	)  
;

pr_GroupRefListWithExcept returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b; boolean first=true; boolean innerLoop=false;}:
	#(GroupRefListWithExcept 
		a=pr_FullGroupIdentifierWithExcept {s.append(a); }
		(
			(b=pr_FullGroupIdentifierWithExcept {s.append(getComma() + b);})*
		)
	)
;

pr_AllGroupsWithExcept returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(AllGroupsWithExcept {s.append("all"); } ({s.append(" except ");} a=pr_GroupRefList {s.append(a);})?)
;


pr_FullGroupIdentifierWithExcept returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
    #(FullGroupIdentifierWithExcept
		a=pr_FullGroupIdentifier {s.append(a);} 
		({currentNesting++; s.append(" except" + getOpeningBrace());} b=pr_ExceptsDef {s.append(b); currentNesting--; s.append(getSpacing() + "}");} )?	
	)
;

pr_GroupRefList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(GroupRefList a=pr_FullGroupIdentifier {s.append(a);} (b=pr_FullGroupIdentifier {s.append(getComma() + b);})* )
;

pr_FullGroupIdentifier returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a;}:
   #(FullGroupIdentifier a=pr_Identifier {s.append(a);} (Dot a=pr_Identifier {s.append("." + a);})*)
;


pr_ImportTypeDefSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ImportTypeDefSpec {s.append("type ");} (a=pr_TypeRefList | a=pr_AllTypesWithExcept) {s.append(a);})
;

pr_AllTypesWithExcept returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(AllTypesWithExcept {s.append("all");} ({s.append(" except ");} a=pr_TypeRefList {s.append(a);})?)
;

pr_TypeRefList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(TypeRefList a=pr_TypeDefIdentifier {s.append(a);} (b=pr_TypeDefIdentifier {s.append(getComma() + b);})*)
;

pr_TypeDefIdentifier returns [StringBuffer s]:
	s=pr_Identifier
;

pr_ImportTemplateSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ImportTemplateSpec (
		{s.append("template ");} ( a=pr_TemplateRefList | a=pr_AllTemplsWithExcept ) {s.append(a);}
	) )
;

pr_TemplateRefList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(TemplateRefList a=pr_Identifier {s.append(a);} (b=pr_Identifier {s.append(getComma() + b);})* )
;

pr_AllTemplsWithExcept returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(AllTemplsWithExcept {s.append("all");} ({s.append(" except ");} a=pr_TemplateRefList {s.append(a);})?)
;

pr_ImportConstSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ImportConstSpec
		({s.append("const ");}( a=pr_ConstRefList | a=pr_AllConstsWithExcept) {s.append(a);})
	)
;

pr_AllConstsWithExcept returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(AllConstsWithExcept {s.append("all");}({s.append(" except ");} a=pr_ConstRefList {s.append(a);})?)
;

pr_Identifier returns [StringBuffer s] {s = new StringBuffer(); }:
	#(Identifier a:IDENTIFIER {s.append(a.getText()); })
;

pr_ConstRefList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ConstRefList a=pr_Identifier {s.append(a);} (b=pr_Identifier {s.append(getComma() + b);})*)
;

pr_ImportAltstepSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ImportAltstepSpec
		( {s.append("altstep ");} ( a=pr_AltstepRefList | a=pr_AllAltstepsWithExcept ) {s.append(a);})
	)
;

pr_AltstepRefList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(AltstepRefList a=pr_Identifier {s.append(a);} (b=pr_Identifier {s.append(getComma() + b);})* )
;

pr_AllAltstepsWithExcept returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(AllAltstepsWithExcept {s.append("all");} ({s.append(" except ");} a=pr_AltstepRefList {s.append(a);})? )
;

pr_ImportTestcaseSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#( ImportTestcaseSpec
		( {s.append("testcase ");} ( a=pr_TestcaseRefList | a=pr_AllTestcasesWithExcept ) {s.append(a);} )
	)
;

pr_TestcaseRefList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(TestcaseRefList a=pr_Identifier {s.append(a);} (b=pr_Identifier {s.append(getComma() + b);})* )
;

pr_AllTestcasesWithExcept returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(AllTestcasesWithExcept {s.append("all");} ({s.append(" except ");} a=pr_TestcaseRefList {s.append(a);})? )
;

pr_ImportFunctionSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#( ImportFunctionSpec
		( {s.append("function ");} ( a=pr_FunctionRefList | a=pr_AllFunctionsWithExcept) {s.append(a);})
	)
;

pr_FunctionRefList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(FunctionRefList a=pr_Identifier {s.append(a);} (b=pr_Identifier {s.append(getComma() + b);})*)
;

pr_AllFunctionsWithExcept returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(AllFunctionsWithExcept {s.append("all");} ({s.append(" except ");} a=pr_FunctionRefList {s.append(a);})?)
;

pr_ImportSignatureSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#( ImportSignatureSpec
		( {s.append("signature ");} ( a=pr_SignatureRefList | a=pr_AllSignaturesWithExcept  ) {s.append(a);} )
	)
;

pr_SignatureRefList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(SignatureRefList a=pr_Identifier {s.append(a);} (b=pr_Identifier {s.append(getComma() + b);})* )
;

pr_AllSignaturesWithExcept returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(AllSignaturesWithExcept {s.append("all");} ({s.append(" except ");} a=pr_SignatureRefList {s.append(a);})?)
;


/*****  1.6.1.9 - Group Definitions  *****/

pr_GroupDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(GroupDef 
		{	inGroup=true; 
			s.append("group ");
		} 
		a=pr_Identifier 
		{	s.append(a + getOpeningBrace()); 
			currentNesting++;
		} 
		(	b=pr_ModuleDefinitionsPart 
			{s.append(b);} 
		)? 
		{	currentNesting--; 
			s.append(getSpacing() + "} ");
		} 
		{inGroup=false;} )
;


/***** 1.6.1.10 - External Function Definitions  *****/

pr_ExtFunctionDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(ExtFunctionDef 
		a=pr_Identifier 
		{s.append("external function " + a + getOpenParenthesis());} 
		(	b=pr_FunctionFormalParList 
			{s.append(b);} 
		)? 
		{s.append(getClosedParenthesis());}
		(	c=pr_ReturnType 
			{s.append(" " + c);}
		)? 
	)
;



/***** 1.6.1.11 - External Constant Definitions  *****/

pr_ExtConstDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b; boolean first=true;}:
	#(ExtConstDef 
		a=pr_Type 
		{s.append("external const " + a + " ");}
		//added support for list of identifiers
		(	b=pr_Identifier
			{if (first) s.append(b); else s.append(getComma() + b); first=false;}
		)+
	) 
;

/***** 1.6.1.12 - Module parameter definitions *****/

pr_ModuleParDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ModuleParDef 
		{s.append("modulepar ");} 
		(	(	a=pr_ModulePar 
				{s.append(a);} 
			)+ 
		  | {s.append(getOpeningBraceNoSpace()); currentNesting++;} 
		    a=pr_MultitypedModuleParList 
		    {s.append(a); currentNesting--; s.append(getSpacing() + "} ");}
		 ) 
	)
;

pr_MultitypedModuleParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(MultitypedModuleParList 
		(	a=pr_ModulePar 
			{s.append(getSpacing() + a);} 
			(	SemiColon 
				{s.append(";");}
			)? 
			{s.append(getNewline());}
		)* //changed from + 
	)
;

/***** 1.6.2 - Control Part  *****/

pr_ModuleControlPart returns [StringBuffer s] {s=new StringBuffer(); StringBuffer with,body; with=body=null;}: 
	#(ModuleControlPart {s.append(getSpacing() + "control" + getOpeningBrace());}
		{currentNesting++;} 
		body=pr_ModuleControlBody {s.append(body ); currentNesting--; s.append(getSpacing() + "}");}
		( with=pr_WithStatement {s.append(getKRNewline() + with);})? 
		(SemiColon {s.append(";");})?
	)
{
	s.append(getNewlines(parameters.getLinesAfterControlPart()));
}
;

pr_ModuleControlBody returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ModuleControlBody ( a=pr_ControlStatementOrDef {s.append(getSpacing() + a);} (SemiColon {s.append(";");})? {s.append(getNewline());})* )
;

pr_ControlStatementOrDef returns [StringBuffer s] {StringBuffer a;}:
	#(ControlStatementOrDef
		(
			(	s=pr_FunctionLocalInst
				(	a=pr_WithStatement {s.append(" "+a);}
     			)?
			)
		  | (	s=pr_ControlStatement
			)
		  | (	s=pr_FunctionLocalDef
				(	a=pr_WithStatement {s.append(" "+a);}
     			)?
			) 
		)
	)
;

pr_ControlStatement returns [StringBuffer s] {s=new StringBuffer();}:
	#(ControlStatement
		(
			s=pr_TimerStatements
			| s=pr_BasicStatements
			| s=pr_BehaviourStatements
			| s=pr_SUTStatements
			| STOP {s.append("stop");}
		)
	)
;


/***** 1.6.3.1 - Variable Instantiation  *****/

pr_VarInstance returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,d,c=null;}:
	#(VarInstance 
		{s.append("var ");} 
		(	(	(	TemplateKeyword 
					{s.append("template ");}
				  | d=pr_RestrictedTemplate
				  	{s.append(d);}
				)?
				a=pr_Type 
				{s.append(a + " ");} 
				( 	b=pr_VarList 
					{s.append(b);}
				  | c=pr_TempVarList 
				  	{s.append(c);} 
				) 
			) 
		) 
	) 
;

pr_VarList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a=null; boolean first=true;}:
	#(VarList (a=pr_SingleVarInstance {if (!first) s.append(getComma()); s.append(a); first=false; })+)
;

pr_TempVarList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(TempVarList (a=pr_SingleTempVarInstance {s.append(a);} (b=pr_SingleTempVarInstance {s.append(getComma() + b);})*))
;


pr_SingleTempVarInstance returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(SingleTempVarInstance (a=pr_Identifier {s.append(a);} (b=pr_ArrayDef {s.append(b);})? ({s.append(getAssignment());} c=pr_TemplateBody {s.append(c);})? ))
;
                 
pr_SingleVarInstance returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c=null;}: 
	#(SingleVarInstance a=pr_Identifier {s.append(a);} ( b=pr_ArrayDef {s.append(b);})? (c=pr_Expression {s.append(getAssignment() + c);} )? )
;

pr_VariableRef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(VariableRef 
		(	a=pr_Identifier 
			{s.append(a);} 
			( 	(	pr_ExtendedFieldReference)=> b=pr_ExtendedFieldReference 
					{s.append(b);}
				)?
			)
		)
;


/*****    1.6.2.2 - Timer Instantiation  *****/

pr_TimerInstance returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a=null; boolean first=true;}:
	#( TimerInstance {s.append("timer ");} ( a=pr_SingleTimerInstance {if (first) s.append(a); else s.append(getComma() + a); first=false;} )+ )
;


pr_SingleTimerInstance returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c=null;}: 
	#( SingleTimerInstance a=pr_Identifier {s.append(a);} ( b=pr_ArrayDef {s.append(b);})? (c=pr_TimerValue {s.append(" := " + c);})? ) 
;


pr_TimerValue returns [StringBuffer s]:
	s=pr_Expression
;


pr_TimerRef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b=null; }:
	#(TimerRef (a=pr_Identifier {s.append(a); } ( b=pr_ArrayOrBitRef {s.append(b);} )*))
;

pr_TimerRefAssignment returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b=null; }:
	#(TimerRefAssignment 
		a=pr_Identifier
		b=pr_TimerRef 
		{s.append(a).append(getAssignment()).append(b);}
	)
;

/***** 1.6.2.3 - Component Operations  *****/


pr_ConfigurationStatements returns [StringBuffer s] {s=new StringBuffer();}:
	#(ConfigurationStatements 
		(
    	s=pr_ConnectStatement
		| s=pr_MapStatement
		| s=pr_DisconnectStatement
		| s=pr_UnmapStatement
		| s=pr_DoneStatement
		| s=pr_KilledStatement
		| s=pr_StartTCStatement
		| s=pr_StopTCStatement
		| s=pr_KillTCStatement
		)
	)
;

pr_KillTCStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(KillTCStatement 
		(
			KillKeyword {s.append("kill");}
			| a=pr_ComponentReferenceOrLiteral {s.append(a+".kill");} 
			| AllKeyword {s.append("all component.kill");}
		)
	)		   
;

pr_KilledStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(KilledStatement a=pr_ComponentId {s.append(a + ".killed");})
;

pr_ConfigurationOps returns [StringBuffer s]:
	#(ConfigurationOps 
		(
		s=pr_CreateOp
		| s=pr_SelfOp
		| s=pr_SystemOp
		| s=pr_MTCOp
		| s=pr_RunningOp
		| s=pr_AliveOp
		)
	)
;

pr_CreateOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(CreateOp 
		a=pr_ComponentType 
		{s.append(a + ".create");} 
		(	{s.append(getOpenParenthesis() + 
			getSpacesAroundParentheses());}
			(	c=pr_SingleExpression {s.append(c);}
			  | Minus {s.append("-");}
			)
			
			(	b=pr_SingleExpression {s.append(getComma()+b);}
			)?
			{s.append(getSpacesAroundParentheses() + 
			getClosedParenthesis());}
		)?
		(	AliveKeyword 
			{s.append(" alive");}
		)?
	)
;

pr_SystemOp returns [StringBuffer s] {s=new StringBuffer();}:
	SystemOp {s.append("system");}
;

pr_AliveOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(AliveOp a=pr_ComponentId {s.append(a+".alive");})
;

pr_SelfOp returns [StringBuffer s] {s=new StringBuffer();}:
	SelfOp {s.append("self");}
;


pr_MTCOp returns [StringBuffer s] {s=new StringBuffer();}:
	MTCOp {s.append("mtc");}
;


pr_DoneStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(DoneStatement a=pr_ComponentId {s.append(a+".done");})

;

pr_ComponentId returns [StringBuffer s] {s=new StringBuffer();}:
	#(ComponentId ( (s=pr_VariableRef |s=pr_FunctionInstance) | AnyKeyword {s.append("any component");} | AllKeyword {s.append("all component");}))
;

pr_RunningOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(RunningOp a=pr_ComponentId {s.append(a + ".running");})
;

pr_ConnectStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ConnectStatement 
		a=pr_SingleConnectionSpec 
		{s.append("connect"+a);})
;

pr_SingleConnectionSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(SingleConnectionSpec 
		(	a=pr_PortRef 
			b=pr_PortRef
		)
	) 
	{s.append(getOpenParenthesis() + 
	getSpacesAroundParentheses() + 
	a + 
	getComma() + 
	b +
	getSpacesAroundParentheses() +  
	getClosedParenthesis()
	);}
;

pr_PortSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(PortSpec 
		a=pr_PortRef 
		b=pr_PortRef
	) 
	{s.append(getOpenParenthesis() + 
	getSpacesAroundParentheses() + 
	a + 
	getComma() + 
	b + 
	getSpacesAroundParentheses() + 
	getClosedParenthesis());}
;

pr_PortRef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(PortRef a=pr_ComponentRef b=pr_Port ) {s.append(a + ":" + b);}
;

pr_PortRefAssignment returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(PortRefAssignment
		a=pr_Identifier
		b=pr_Port
		{s.append(a).append(getAssignment()).append(b);}
	)
;

pr_ComponentRef returns [StringBuffer s]:
	#(ComponentRef (s=pr_ComponentOrDefaultReference | s=pr_SystemOp | s=pr_SelfOp | s=pr_MTCOp))
;


pr_ComponentRefAssignment returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ComponentRefAssignment
		a=pr_Identifier
		b=pr_ComponentRef
		{s.append(a).append(getAssignment()).append(b);}
	)
;

pr_ComponentOrDefaultReference returns [StringBuffer s]:
	s=pr_VariableRef | s=pr_FunctionInstance
;

pr_DisconnectStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(DisconnectStatement 
		{s.append("disconnect");} 
		(	a=pr_SingleOrMultiConnectionSpec
			{s.append(a);}
		)?
	)
;

pr_SingleOrMultiConnectionSpec returns [StringBuffer s] {StringBuffer a,b;}:
	#(SingleOrMultiConnectionSpec
		(	(	s=pr_SingleConnectionSpec
				(	a=pr_ParamClause {s.append(" "+a);}
				)?
			) 
	      | (	s=pr_AllConnectionsSpec
	      		(	b=pr_ParamClause {s.append(" "+b);}
				)?
			)
	      | s=pr_AllPortsSpec 
	      | s=pr_AllCompsAllPortsSpec
	    
		)
	)
;

pr_AllConnectionsSpec returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a;}:
	a=pr_PortRef 
	{s.append(getOpenParenthesis() + 
	getSpacesAroundParentheses() + 
	a + 
	getSpacesAroundParentheses() + 
	getClosedParenthesis());}
;

pr_AllPortsSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	a=pr_ComponentRef 
	{s.append(getOpenParenthesis() + 
	getSpacesAroundParentheses() + 
	a + 
	":all port" +
	getSpacesAroundParentheses() + 
	getClosedParenthesis());}
;
 
pr_AllCompsAllPortsSpec returns [StringBuffer s] {s=new StringBuffer();}:
	{s.append(getOpenParenthesis() + 
	getSpacesAroundParentheses());} 
	AllKeyword {s.append("all ");}
	COMPONENT {s.append("component:");}
	AllKeyword {s.append("all ");}
	PortKeyword  {s.append("port");}
	{s.append(getSpacesAroundParentheses() + 
	getClosedParenthesis());}
;


pr_MapStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(MapStatement 
		{s.append("map");} 
		a=pr_SingleConnectionSpec 
		{s.append(a);}
		(	b=pr_ParamClause {s.append(" "+b);}
		)?
	)
;

pr_ParamClause	returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ParamClause
		{s.append("param"+getOpenParenthesis());}
		a=pr_FunctionActualParList
		{s.append(a+getClosedParenthesis());}
	)
;


pr_UnmapStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(UnmapStatement 
		{s.append("unmap");}
		(	a=pr_SingleOrMultiConnectionSpec 
			{s.append(a);}
		)? 
		
	)
;


pr_StartTCStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(StartTCStatement 
		a=pr_ComponentOrDefaultReference 
		b=pr_FunctionInstance 
	) 
	{s.append(a + 
	".start" + 
	getOpenParenthesis() + 
	getSpacesAroundParentheses() + 
	b + 
	getSpacesAroundParentheses() + 
	getClosedParenthesis());}
;


pr_StopTCStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,c,d; StringBuffer b = new StringBuffer(); boolean first=true;}:
	#(StopTCStatement 
		(	(	a=pr_ComponentReferenceOrLiteral 
				{s.append(a+".");} 
				STOP
			) 
		  | (	AllKeyword 
		  		COMPONENT 
		  		STOP 
		  		{s.append("all component.");}
		  	)
		  | (	TESTCASE
		  		{s.append("testcase.");}
		  		STOP
		  		(	{	if (first) {
		  		  			first=false;
		  		  			b.append(getOpenParenthesis()+getSpacesAroundParentheses());
		  		  		} else {
		  		  			b.append(getComma());
		  		  		} 
		  		  	}
		  			(	c=pr_FreeText {b.append(c);}
		  		  	  | d=pr_TemplateInstance {b.append(d);}
		  		  	)
		  		)*
		  	)
		)?
		{if (b.length()>0) b.append(getSpacesAroundParentheses()+getClosedParenthesis());}
		{s.append("stop"+b);}
	)
;

pr_ComponentReferenceOrLiteral returns [StringBuffer s]:
	#(ComponentReferenceOrLiteral
		(
		s=pr_ComponentOrDefaultReference 
		| s=pr_MTCOp 
		| s=pr_SelfOp
		)
	)
;

pr_ComponentIdentifier returns [StringBuffer s]:
	s=pr_FunctionInstance | s=pr_VariableRef
;

/*****  1.6.3.4 - Port Operations  *****/

pr_Port returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(Port (a=pr_Identifier {s.append(a);} ( b=pr_ArrayOrBitRef {s.append(b);})*))
;


pr_CommunicationStatements returns [StringBuffer s]:
	#(CommunicationStatements
		(
		s=pr_SendStatement
		| s=pr_CallStatement
		| s=pr_ReplyStatement
		| s=pr_RaiseStatement
		| s=pr_ReceiveStatement
		| s=pr_TriggerStatement
		| s=pr_GetCallStatement
		| s=pr_GetReplyStatement
		| s=pr_CatchStatement
		| s=pr_CheckStatement
		| s=pr_ClearStatement
		| s=pr_StartStatement
		| s=pr_StopStatement
		| s=pr_HaltStatement // see parser
		| s=pr_CheckStateStatement
		)
	)
;


pr_SendStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(SendStatement 
		a=pr_Port 
		{s.append(a + 
		".send" + 
		getOpenParenthesis() + 
		getSpacesAroundParentheses());} 
		b=pr_SendParameter 
		{s.append(b + 
		getSpacesAroundParentheses() + 
		getClosedParenthesis());} 
		(	c=pr_ToClause 
			{s.append(" " + c);} 
		)? 
	)
;

pr_SendParameter returns [StringBuffer s]:
	s=pr_TemplateInstance
;


pr_ToClause returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}: //removed c from the list of StringBuffers
	#(ToClause 
		{s.append("to");}
		(	a=pr_AddressRef 
			{s.append(" "+a);}
		  | b=pr_AddressRefList 
		  	{s.append(b);}
		  | (	AllKeyword 
		  		COMPONENT
		  	) 
		  	{s.append(" all component");}
		) 
	)
;

pr_AddressRefList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(AddressRefList 
		a=pr_AddressRef 
		{s.append(getOpenParenthesis() + 
		getSpacesAroundParentheses() +
		a);} 
		(	b=pr_AddressRef 
			{s.append(getComma() + b);}
		)*
		{s.append(getSpacesAroundParentheses() + 
		getClosedParenthesis());}
	)
;

pr_AddressRef returns [StringBuffer s]:
	#(AddressRef (s=pr_TemplateInstance))
;


pr_CallStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d;}:
	#(CallStatement 
		a=pr_Port 
		{s.append(a + ".call");} 
		b=pr_CallParameters 
		{s.append(getOpenParenthesis() + 
		getSpacesAroundParentheses() + 
		b + 
		getSpacesAroundParentheses() + 
		getClosedParenthesis());} 
		(	c=pr_ToClause
			{s.append(" " + c);}
		)? 
		(	d=pr_PortCallBody 
			{s.append(d);}
		)?
	)
;


pr_CallParameters returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(CallParameters 
		a=pr_TemplateInstance 
		{s.append(a);} 
		(	b=pr_CallTimerValue 
			{s.append(getComma() + b);}
		  | NowaitKeyword 
		  	{s.append(getComma() + "nowait");}
		)?
	)
;


pr_CallTimerValue returns [StringBuffer s] {s=new StringBuffer();}:
	#(CallTimerValue (s=pr_TimerValue | NowaitKeyword {s.append("nowait");}))
;


pr_PortCallBody returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	{s.append(getOpeningBrace()); currentNesting++;}
	a=pr_CallBodyStatement {s.append(a);} 
	( 
		b=pr_CallBodyStatement {s.append(b);} 
		(SemiColon {s.append(";");})?
	)* 
	{currentNesting--; s.append(getSpacing() + "}");}
;


pr_CallBodyStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(CallBodyStatement 
		a=pr_CallBodyGuard {s.append(getSpacing() + a);} 
		{s.append(getOpeningBrace()); currentNesting++;} 
		b=pr_StatementBlock 
		{s.append(b); currentNesting--; s.append(getSpacing() + "}" + getNewline()); }
	)
;


pr_CallBodyGuard returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(CallBodyGuard a=pr_AltGuardChar {s.append(a + " ");} b=pr_CallBodyOps {s.append(b);})
;


pr_CallBodyOps returns [StringBuffer s]:
	#(CallBodyOps (s=pr_GetReplyStatement | s=pr_CatchStatement))
;


pr_ReplyStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d;}:
	#(ReplyStatement 
		a=pr_Port {s.append(a + ".");} 
		b=pr_PortReplyOp {s.append(b);}
	)
;

pr_PortReplyOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(PortReplyOp
		(	{s.append("reply(");}
			a=pr_TemplateInstance {s.append(a);} 
			( b=pr_ReplyValue {s.append(" " + b);} )? 
			{s.append(getClosedParenthesis());}
			( c=pr_ToClause {s.append(" " + c);})? 
		)
	)
;


pr_ReplyValue returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	a=pr_Expression {s.append("value " + a); }
;


pr_RaiseStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(RaiseStatement 
		a=pr_Port {s.append(a + ".");}
		b=pr_PortRaiseOp {s.append(b);}
	)
;

pr_PortRaiseOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d;}:
	#(PortRaiseOp
		(	{s.append("raise(");}
			a=pr_Signature {s.append(a);}
			b=pr_TemplateInstance {s.append(getComma() + b);}
			{s.append(getClosedParenthesis());}
			( c=pr_ToClause {s.append(" " + c);} )? 
		)
	)
;

pr_ReceiveStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ReceiveStatement a=pr_PortOrAny b=pr_PortReceiveOp {s.append(a + ".").append(b);})
;


pr_PortOrAny returns [StringBuffer s] {s=new StringBuffer();}:
	#(PortOrAny (s=pr_Port | AnyKeyword PortKeyword {s.append("any port");}))
;


pr_PortReceiveOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(PortReceiveOp 
		{s.append("receive");} 
		(	a=pr_ReceiveParameter 
			{s.append(getOpenParenthesis() + 
			getSpacesAroundParentheses() + 
			a + 
			getSpacesAroundParentheses() + 
			getClosedParenthesis());} 
		)? 
		(	b=pr_FromClause 
			{s.append(" " + b);}
		)? 
		(	c=pr_PortRedirect 
			{s.append(c);}
		)? 
	)
;


pr_ReceiveParameter returns [StringBuffer s]:
  s=pr_TemplateInstance
;


pr_FromClause returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}: //removed c from the list of StringBuffers
	#(FromClause
		{s.append("from");}
		(	a=pr_AddressRef 
			{s.append(" "+a);}
		  | b=pr_AddressRefList 
		  	{s.append(b);}
		  | (	AnyKeyword 
		  		COMPONENT
		  	) 
		  	{s.append(" any component");}
		) 
	)
;


pr_PortRedirect returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(PortRedirect 
		{s.append(" -> ");} 
		(	a=pr_ValueSpec 
			{s.append(a);} 
			(	b=pr_SenderSpec 
				{s.append(" " + b);}
			)?
		  | c=pr_SenderSpec 
		  	{s.append(c);}) 
		)
;

pr_SingleValueSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(SingleValueSpec
	//TODO: what is to happen here?
		a=pr_VariableRef
		{s.append(a);}
		(	b=pr_FieldReference
			c=pr_ExtendedFieldReference
			{s.append(getAssignment()+b+c);}
		)?
	)
;





pr_ValueSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ValueSpec 
		{s.append("value ");}
		(	(	pr_VariableRef)=>(a=pr_VariableRef 
				{s.append(a);}
			)
		  | (	{s.append(getSpacesAroundParentheses()+getOpenParenthesis());}
		  		b=pr_SingleValueSpec
		  		{s.append(b);}
		  		(	b=pr_SingleValueSpec
		  			{s.append(getComma()+b);}
		  		)*
		  		{s.append(getOpenParenthesis()+getSpacesAroundParentheses());}
		  	)
		)
	)
;


pr_SenderSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(SenderSpec 
		a=pr_VariableRef 
		{s.append("sender " + a);} 
	)
;


pr_TriggerStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(TriggerStatement 
		a=pr_PortOrAny {s.append(a + ".");} 
		b=pr_PortTriggerOp {s.append(b);}
	)
;

pr_PortTriggerOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(PortTriggerOp 
		{s.append("trigger");}
		(	a=pr_ReceiveParameter 
			{s.append(getOpenParenthesis() + 
			getSpacesAroundParentheses() + 
			a + 
			getSpacesAroundParentheses() + 
			getClosedParenthesis());}
		)? 
		(	b=pr_FromClause 
			{s.append(" " + b);}
		)? 
		(	c=pr_PortRedirect 
			{s.append(c);}
		)? 
	)
;

pr_GetCallStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(GetCallStatement 
		a=pr_PortOrAny 
		{s.append(a + ".");} 
		b=pr_PortGetCallOp 
		{s.append(b);}
	)
;

pr_PortGetCallOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(PortGetCallOp 
		{s.append("getcall");}
		(	a=pr_ReceiveParameter 
			{s.append(getOpenParenthesis() + 
			getSpacesAroundParentheses() + 
			a + 
			getSpacesAroundParentheses() + 
			getClosedParenthesis());}
		)? 
		(	b=pr_FromClause 
			{s.append(" " + b);}
		)? 
		(	c=pr_PortRedirectWithParam 
			{s.append(" " + c);}
		)? 
	)
;


pr_PortRedirectWithParam returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	a=pr_RedirectWithParamSpec {s.append("->" + a);}
;

pr_PortRedirectWithValueAndParam returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(PortRedirectWithValueAndParam a=pr_RedirectWithValueAndParamSpec {s.append("->" + a);})
;

pr_RedirectWithValueAndParamSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d;}:
	#(RedirectWithValueAndParamSpec (a=pr_ValueSpec {s.append(" " + a);} (b=pr_ParaSpec {s.append(" " + b);})? (c=pr_SenderSpec {s.append(" " + c);})? | d=pr_RedirectWithParamSpec {s.append(d);}))
;

pr_RedirectWithParamSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(RedirectWithParamSpec 
		(	(	a=pr_ParaSpec 
				{s.append(" " + a);} 
				(	b=pr_SenderSpec 
					{s.append(" " + b);}
				)?
			  | c=pr_SenderSpec 
			  	{s.append(" " + c);}
			)
		)
	)
;


pr_ParaSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ParaSpec 
		a=pr_ParaAssignmentList 
		{s.append("param " + a);}
	)
;

pr_ParaAssignmentList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
  	#(ParaAssignmentList 
  		{s.append(getOpenParenthesis() + getSpacesAroundParentheses());}
  		(	a=pr_AssignmentList 
  			{s.append(a);}
  		  | b=pr_VariableList 
  		  	{s.append(b);}
  		) 
  		{s.append(getSpacesAroundParentheses() + getClosedParenthesis());}
  	)
;

pr_AssignmentList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; boolean first=true;}:
	#(AssignmentList 
		(	a=pr_VariableAssignment 
			{if (first) s.append(a); else s.append(getComma() + a); first=false;}
		)* 
	)
;

pr_VariableAssignment returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(VariableAssignment 
		a=pr_VariableRef 
		{s.append(a);} 
		b=pr_ParameterIdentifier 
		{s.append(getAssignment() + b);}
	)
;

pr_ParameterIdentifier returns [StringBuffer s]:
	s=pr_Identifier
;

pr_VariableList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; boolean first=true;}:
	#(VariableList ( a=pr_VariableEntry {if (first) s.append(a); else s.append(getComma() + a); first=false;} )* )
;


pr_VariableEntry returns [StringBuffer s] {s=new StringBuffer();}:
	#(VariableEntry (s=pr_VariableRef | NotUsedSymbol {s.append("-");}) )
;


pr_GetReplyStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#( GetReplyStatement a=pr_PortOrAny b=pr_PortGetReplyOp {s.append(a + "." ).append(b);} )
;


pr_PortGetReplyOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d;}:
	#(PortGetReplyOp 
		{s.append("getreply");}
		(	{s.append(getOpenParenthesis() + getSpacesAroundParentheses());}
			a=pr_ReceiveParameter 
			{s.append(a);} 
			(	b=pr_ValueMatchSpec 
				{s.append(" " + b);} 
			)? 
			{s.append(getSpacesAroundParentheses() + getClosedParenthesis());}
		)? 
		(	c=pr_FromClause 
			{s.append(" " + c);}
		)? 
		(	d=pr_PortRedirectWithValueAndParam 
			{s.append(" " + d);}
		)? 
	)
;

pr_ValueMatchSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ValueMatchSpec a=pr_TemplateInstance {s.append("value " + a);})
;

pr_CheckStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(CheckStatement 
		a=pr_PortOrAny {s.append(a + ".");} 
		b=pr_PortCheckOp {s.append(b);}
	)
;

pr_PortCheckOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(PortCheckOp {s.append("check");}
		(	b=pr_CheckParameter 
			{s.append(getOpenParenthesis()+ 
			getSpacesAroundParentheses() + 
			b + 
			getSpacesAroundParentheses() + 
			getClosedParenthesis());} 
		)? 
	)
;

pr_CheckParameter returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(CheckParameter
		(
			a=pr_CheckPortOpsPresent {s.append(a);}
			| b=pr_FromClausePresent {s.append( b);}
			| c=pr_RedirectPresent {s.append(c);}
		)
	)
;

pr_RedirectPresent returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(RedirectPresent
		a=pr_SenderSpec {s.append("-> " + a);}
	)
;

pr_FromClausePresent returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(FromClausePresent
		a=pr_FromClause {s.append(a);}
		( b=pr_SenderSpec {s.append(b);})?
	)
;

pr_CheckPortOpsPresent returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(CheckPortOpsPresent
		(
			s=pr_PortReceiveOp
			| s=pr_PortGetCallOp
			| s=pr_PortGetReplyOp
			| s=pr_PortCatchOp
		)
	)
;

pr_CatchStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#( CatchStatement a=pr_PortOrAny b=pr_PortCatchOp ) {s.append(a + "." ).append(b);}
;

pr_PortCatchOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#( PortCatchOp 
		{s.append("catch");} 
		(	a=pr_CatchOpParameter 
			{s.append(getOpenParenthesis() + 
			getSpacesAroundParentheses() + 
			a + 
			getSpacesAroundParentheses() + 
			getClosedParenthesis());} 
		)? 
		(	b=pr_FromClause 
			{s.append(" " + b);}
		)? 
		(	c=pr_PortRedirect 
			{s.append(c);}
		)? 
	)
;


pr_CatchOpParameter returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#( CatchOpParameter ( a=pr_Signature b=pr_TemplateInstance {s.append(a + getComma()).append(b);} | TimeoutKeyword {s.append("timeout");} ) )
;


pr_ClearStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#( ClearStatement a=pr_PortOrAll {s.append(a + ".clear");})
;


pr_PortOrAll returns [StringBuffer s]:
	#(PortOrAll (s=pr_Port | s=pr_AllPort))
;

pr_AllPort returns [StringBuffer s] {s=new StringBuffer();}:
	#(AllPort {s.append("all port");})
;

pr_StartStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(StartStatement a=pr_PortOrAll {s.append(a + ".start");})
;


pr_StopStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(StopStatement a=pr_PortOrAll {s.append(a + ".stop");})
;

//see parser
pr_HaltStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(HaltStatement a=pr_PortOrAll {s.append(a + ".halt");})
;

pr_CheckStateStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}	:
	#(CheckStateStatement
		a=pr_PortOrAllOrAny {s.append(a+"."+"checkstate");}
		b=pr_SingleExpression 
		{s.append(getOpenParenthesis() + 
		getSpacesAroundParentheses() +
		b +
		getSpacesAroundParentheses() + 
		getClosedParenthesis());}
	)
;

pr_PortOrAllOrAny returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(PortOrAllOrAny
		(	a=pr_PortOrAll {s.append(a);}
		  | AnyKeyword {s.append("any port");}
		)
	)
;


/***** 1.6.2.5 - Timer Operations  *****/

pr_TimerStatements returns [StringBuffer s]:
	#(TimerStatements (s=pr_StartTimerStatement | s=pr_StopTimerStatement | s=pr_TimeoutStatement))
;

pr_TimerOps returns [StringBuffer s]:
	#(TimerOps (s=pr_ReadTimerOp | s=pr_RunningTimerOp))
;

pr_StartTimerStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(StartTimerStatement
		a=pr_TimerRef 
		{s.append(a + ".start");}
		(	b=pr_TimerValue 
			{s.append(getOpenParenthesis() + 
			getSpacesAroundParentheses() + 
			b + 
			getSpacesAroundParentheses() + 
			getClosedParenthesis());}
		)? 
	)
;


pr_StopTimerStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(StopTimerStatement a=pr_TimerRefOrAll {s.append(a+".stop");})
;

pr_TimerRefOrAll returns [StringBuffer s] {s=new StringBuffer();}:
	#(TimerRefOrAll (s=pr_TimerRef | AllKeyword TimerKeyword {s.append("all timer"); } ))
;

pr_ReadTimerOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ReadTimerOp a=pr_TimerRef {s.append(a + ".read");})
;

pr_RunningTimerOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#( RunningTimerOp a=pr_TimerRefOrAny {s.append(a+".running");})
;

pr_TimeoutStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#( TimeoutStatement a=pr_TimerRefOrAny {s.append(a+".timeout");} )
;

pr_TimerRefOrAny returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a=null;}:
	#(TimerRefOrAny (a=pr_TimerRef {s.append(a);} | AnyKeyword TimerKeyword {s.append("any timer");}))
;

pr_TestcaseOperation returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(TestcaseOperation
		{s.append("testcase.stop");}
		(	{s.append(getOpenParenthesis() + 
			getSpacesAroundParentheses());}
			(	a=pr_FreeText
		      | a=pr_InLineTemplate
		    )
		    {s.append(a);}
			(	(	a=pr_FreeText
		      	  | a=pr_InLineTemplate
		      	)
		      	{s.append(getComma()+a);}
		    )*
		    {s.append(getSpacesAroundParentheses() + 
			getClosedParenthesis());}
		)?
	)
;


/*****  1.6.3 - Type  *****/


pr_Type returns [StringBuffer s]:
	#(Type (s=pr_PredefinedType | s=pr_ReferencedType))
;


pr_PredefinedType returns [StringBuffer s] {s = new StringBuffer();}:
	#(PredefinedType
		(
		a:BitStringKeyword {s.append("bitstring");}
		| b:BooleanKeyword {s.append("boolean");}
		| c:CharStringKeyword {s.append("charstring");}
		| d:UniversalCharString {s.append("universal charstring");}
		| e:IntegerKeyword {s.append("integer");}
		| f:OctetStringKeyword {s.append("octetstring");}
		| g:HexStringKeyword {s.append("hexstring");}
		| h:VerdictTypeKeyword {s.append("verdicttype");}
		| i:FloatKeyword {s.append("float");}
		| j:AddressKeyword {s.append("address");}
		| k:DefaultKeyword {s.append("default");}
		| l:AnyTypeKeyword {s.append("anytype");}
		| m:OBJID {s.append("objid");}
		 )
	)
;


pr_ReferencedType returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c=null;}:
	#(ReferencedType
		(
			( ( pr_GlobalModuleId pr_TypeReference ) => a=pr_GlobalModuleId {s.append(a + ".");} )?
			b=pr_TypeReference {s.append(b);} ( (pr_ExtendedFieldReference )=>c=pr_ExtendedFieldReference {s.append(c);} )?
		)
	)
;

pr_TypeReference returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(TypeReference 
		(	a=pr_Identifier 
			{s.append(a);} 
			(	b=pr_TypeActualParList 
				{s.append(getOpenParenthesis() + 
				getSpacesAroundParentheses() + 
				b +
				getSpacesAroundParentheses() +
				getClosedParenthesis());} 
			)?
		) 
	)
;


pr_TypeActualParList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(TypeActualParList
		a=pr_TypeActualPar 
		{s.append(a);}
		(	b=pr_TypeActualPar 
			{s.append(getComma() + b);}
		)* 
	)
	
//	a=pr_TypeActualPar 
//	(	b=pr_TypeActualPar 
//		{a.append(getComma() + b);}	
// 	)*
//    {s.append(a);}
;


pr_TypeActualPar returns [StringBuffer s]:
	#(TypeActualPar //FIXED: Explicit TypeActualPar node is present
		s=pr_ConstantExpression
	)
;

pr_ArrayDef returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(ArrayDef (a=pr_ArrayBounds {s.append("[" + a + "]");}|b=pr_ArrayDefRange {s.append(b);})+ )
;


pr_ArrayDefRange returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b=null;}:
	#(ArrayDefRange a=pr_ArrayBounds b=pr_ArrayBounds {s.append("[" + a + " .. " + b + "]" );})
;

pr_ArrayBounds returns [StringBuffer s]:
	s=pr_SingleConstExpression
;

/*****1.6.3 - Value  *****/


pr_Value returns [StringBuffer s]:
	#(Value (s=pr_ReferencedValue | s=pr_PredefinedValue))
;

pr_PredefinedValue returns [StringBuffer s] {s=new StringBuffer();}:
	#(PredefinedValue
		(
		s=pr_BitStringValue
		| s=pr_BooleanValue
		| s=pr_CharStringValue
		| s=pr_IntegerValue
		| s=pr_OctetStringValue
		| s=pr_ObjectIdentifierValue
		| s=pr_HexStringValue
		| s=pr_VerdictTypeValue
		| s=pr_EnumeratedValue
		| s=pr_FloatValue
		| s=pr_NaNValue
		| s=pr_AddressValue
		| s=pr_OmitValue
		)
	)
;

pr_FloatValue returns [StringBuffer s] {s=new StringBuffer();}:
	#(FloatValue a:FLOATVALUE) {s.append(a.getText());}
;

pr_NaNValue returns [StringBuffer s] {s=new StringBuffer();}:
	#(NaNValue a:NAN) {s.append(a.getText());}
;

pr_BitStringValue returns [StringBuffer s] {s=new StringBuffer();}:
	#(BitStringValue a:BSTRING) {s.append("'" + a.getText() + "'B");}
;

pr_BooleanValue returns [StringBuffer s] {s=new StringBuffer();}:
	#(BooleanValue (a:TRUE {s.append("true");}| b:FALSE {s.append("false");}))
;


pr_IntegerValue returns [StringBuffer s] {s=new StringBuffer();}:
	#(IntegerValue b:NUMBER {s.append(b.getText());})
;


pr_OctetStringValue returns [StringBuffer s] {s=new StringBuffer();}:
	#(OctetStringValue a:OSTRING {s.append("'" + a.getText() + "'O");})
;

//pr_ObjIdComponent returns [StringBuffer s]:
//	(pr_NameForm)=>s=pr_NameForm
//	| s=pr_NumberForm
//	| s=pr_NameAndNumberForm
//;


pr_HexStringValue returns [StringBuffer s] {s = new StringBuffer();}:
	#(HexStringValue a:HSTRING {s.append("'" + a.getText() + "'H");})
;


pr_VerdictTypeValue returns [StringBuffer s]:
	#(VerdictTypeValue
		(
		s=pr_Pass
		| s=pr_Fail
		| s=pr_Inconc
		| s=pr_None
		| s=pr_Error
		)
	)
;

pr_Pass returns [StringBuffer s] {s=new StringBuffer();}:
	a:Pass {s.append("pass");}
;

pr_Fail returns [StringBuffer s] {s=new StringBuffer();}:
	a:Fail {s.append("fail");}
;
       
pr_Inconc returns [StringBuffer s] {s=new StringBuffer();}:
	a:Inconc {s.append("inconc");}
;
            
pr_None returns [StringBuffer s] {s=new StringBuffer();}:
	a:None {s.append("none");}
;
            
pr_Error returns [StringBuffer s] {s=new StringBuffer();}:
	a:Error {s.append("error");}
;

pr_EnumeratedValue returns [StringBuffer s] {s=new StringBuffer();}:
	#(EnumeratedValue s=pr_Identifier)
;

pr_CharStringValue returns [StringBuffer s] {s=new StringBuffer();}:
	#(CharStringValue (a:CSTRING {s.append("\"" + a.getText() + "\"");}| s=pr_Quadruple))
;


pr_Quadruple returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d; a=b=c=d=null;}:
	#(Quadruple 
		a=pr_Group  
		b=pr_Plane  
		c=pr_Row 
		d=pr_Cell 
	)
	{s.append("char" +
	getOpenParenthesis() +
	getSpacesAroundParentheses() +  
	a + 
	getComma() + 
	b + 
	getComma() + 
	c + 
	getComma() + 
	d + 
	getSpacesAroundParentheses() + 
	getClosedParenthesis());}
;

pr_Group returns [StringBuffer s] {s=new StringBuffer();}:
	#(Group a:NUMBER) {s.append(a.getText());}
;

pr_Plane returns [StringBuffer s] {s=new StringBuffer();}:
	#(Plane a:NUMBER) {s.append(a.getText());}
;

pr_Row returns [StringBuffer s] {s=new StringBuffer();}:
	#(Row a:NUMBER) {s.append(a.getText());}
;

pr_Cell returns [StringBuffer s] {s=new StringBuffer();}:
	#(Cell a:NUMBER) {s.append(a.getText());}
;

pr_ReferencedValue returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b; a=b=null;}:
	#(ReferencedValue (a=pr_ValueReference {s.append(a);} ( (pr_ExtendedFieldReference)=>b=pr_ExtendedFieldReference {s.append(b);})?))
;


pr_ValueReference returns [StringBuffer s] {s= new StringBuffer(); StringBuffer a,b; a=b=null;}:
	#(ValueReference  (a=pr_GlobalModuleId {s.append(a + ".");})? b=pr_Identifier {s.append(b);})
;


pr_FreeText returns [StringBuffer s] {s=new StringBuffer();}:
	a:CSTRING {s.append("\"" + a.getText() + "\"");} // temporary workaround - see TTCNParser.g
//	IDENTIFIER // temporary workaround - see TTCNParser.g
;


pr_AddressValue returns [StringBuffer s] {s=new StringBuffer();}:
	Null {s.append("null");}
;


pr_OmitValue returns [StringBuffer s] {s=new StringBuffer();}:
	a:OmitValue {s.append("omit");} //can reuse the omit keyword rule 
;

//necessary for the restricted templates
pr_OmitKeyword returns [StringBuffer s] {s=new StringBuffer();}:
	OMIT {s.append("omit");}
;


/*****  SECTION A.1.6.6 - Parameterisation  *****/


pr_FormalValuePar returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(FormalValuePar 
		(	InParKeyword 
			{s.append("in ");} 
		  | InOutParKeyword 
		  	{s.append("inout ");}
		  | OutParKeyword 
		  	{s.append("out ");} 
		)? 
		a=pr_Type 
		{s.append(a + " ");} 
		b=pr_Identifier 
		{s.append(b);}
		(	{s.append(getAssignment());}
			(	c=pr_Expression
				{s.append(c);}
		  	  | Dash
		  	  	{s.append("-");}
		  	)
		)? 
	)
;


pr_FormalTypePar returns [StringBuffer s]:
	#( FormalTypePar s=pr_Identifier )
;


pr_FormalPortPar returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#( FormalPortPar (InOutParKeyword {s.append("inout ");})? a=pr_Identifier b=pr_Identifier ) {s.append(a).append(b);}
;


pr_FormalTimerPar returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(FormalTimerPar (InOutParKeyword {s.append("inout ");})? TimerKeyword a=pr_Identifier ) {s.append("timer " + a);}
;


pr_FormalTemplatePar returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d;}:
	#(FormalTemplatePar 
		(	InParKeyword 
			{s.append("in ");}
		  | OutParKeyword
		  	{s.append("out ");}
		  | InOutParKeyword
		  	{s.append("inout ");}
	  	)? 
	  	(	TemplateKeyword
			{s.append("template ");}
	  	  | c=pr_RestrictedTemplate
	  	  	{s.append(c);}
	  	)
	  	a=pr_Type 
	  	b=pr_Identifier
	
		{s.append(a + " " + b);}
	  	(	{s.append(getAssignment());}
	  		(	d=pr_TemplateInstance
	  			{s.append(d);}
	  		  | Dash
	  		  	{s.append("-");}
	  		)
		)? 
	  	 
	) 
	//{s.append("template " + a + " " + b);}
;

/***** 1.6.6 - The With Statement  *****/

pr_WithStatement returns [StringBuffer s]
{
	s = new StringBuffer();
	StringBuffer a = null;
	StringBuffer singleWithAttrib = new StringBuffer();
}
:
	#( WithStatement {currentNesting++;} 
		( a=pr_SingleWithAttrib 
			{singleWithAttrib.append(getSpacing() + a); }
		(SemiColon {singleWithAttrib.append(";");})? {singleWithAttrib.append(getNewline());})* {currentNesting--;}
	)
{
	s.append(getKRNewline());
	//if (parameters.isKRstyle()) {
	//	s.append(" ");
	//}
	s.append("with" + getOpeningBrace());
	s.append(singleWithAttrib);
	s.append(getSpacing() + "}");
}
;


pr_SingleWithAttrib returns [StringBuffer s]
{
	s = new StringBuffer();
	StringBuffer attribKeyword = null;
	StringBuffer attribQualifier = null;
	StringBuffer attribSpec = null;
}:
	#( SingleWithAttrib attribKeyword=pr_AttribKeyword ( o:OVERRIDE )? ( attribQualifier=pr_AttribQualifier )? attribSpec=pr_AttribSpec )
{
	s.append(attribKeyword);
	if (o != null)
		s.append(" " + o.getText());
	if (attribQualifier != null)
		s.append(" " + attribQualifier);
	s.append(" " + attribSpec);
}
;

pr_AttribKeyword returns [StringBuffer s]
{
	s = new StringBuffer();
}:
	a:ENCODE {s.append(a.getText());}
  | b:DISPLAY {s.append(b.getText());} 
  | c:EXTENSION {s.append(c.getText());}
  | d:VARIANT {s.append(d.getText());}
  | e:OPTIONAL {s.append(e.getText());}
;


pr_AttribQualifier returns [StringBuffer s] { s = new StringBuffer(); StringBuffer defOrFieldRef = new StringBuffer(); StringBuffer a=null; boolean first=true;}:
	#(AttribQualifier 
		(	a=pr_DefOrFieldRef 
			{if (first) {
				defOrFieldRef.append(a); 
				first=false;
			 } else 
			 	defOrFieldRef.append(getComma() + a);
			} 
		)* 
	)
	{s.append(getOpenParenthesis() + 
	getSpacesAroundParentheses() + 
	defOrFieldRef + 
	getSpacesAroundParentheses() + 
	getClosedParenthesis());}
;


pr_DefOrFieldRef returns [StringBuffer s] { s = new StringBuffer(); StringBuffer a;}:
	#(DefOrFieldRef
		(	(	pr_FieldReference)=>(s=pr_FieldReference
				(	a=pr_ExtendedFieldReference
					{s.append(a);}
				)?
			)
		  | s=pr_SingleExpression
		  | s=pr_DefinitionRef
		  | s=pr_AllRef
		)
	)
;


pr_DefinitionRef returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a=null;}:
	( (pr_Identifier pr_Identifier ) => a=pr_FullGroupIdentifier |  a=pr_Identifier)
{
	s.append(a);
}
;

pr_AllRef returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a=null;}:
	#(AllRef
        (
            (
                        GROUP {s.append("group" + " all");} 
                        ( a=pr_GroupRefList {s.append(" except {" + a + "}");} )? 
            )
            |
            (
                        TYPE {s.append("type" + " all");} 
                        ( a=pr_TypeRefList {s.append(" except {" + a + "}");} )? 
            )
            |
            (
                        TemplateKeyword {s.append("template" + " all");} 
                        ( a=pr_TemplateRefList {s.append(" except {" + a + "}");} )? 
            )
            |
            (
                        CONST {s.append("const" + " all");} 
                        ( a=pr_ConstRefList {s.append(" except {" + a + "}");} )? 
            )
            |
            (
                        AltstepKeyword {s.append("altstep" + " all");} 
                        ( a=pr_AltstepRefList {s.append(" except {" + a + "}");} )? 
            )
            |
            (
                        TESTCASE {s.append("testcase" + " all");} 
                        ( a=pr_TestcaseRefList {s.append(" except {" + a + "}");} )? 
            )
            |
            (
                        FUNCTION {s.append("function" + " all");} 
                        ( a=pr_FunctionRefList {s.append(" except {" + a + "}");} )? 
            )
            |
            (
                        SIGNATURE {s.append("signature" + " all");} 
                        ( a=pr_SignatureRefList {s.append(" except {" + a + "}");} )? 
            )
            |
            (
                        MODULEPAR {s.append("modulepar" + " all");} 
                        ( a=pr_ModuleParRefList {s.append(" except {" + a + "}");} )? 
            )
        )
	)
;

pr_AttribSpec returns [StringBuffer s] :
	#(AttribSpec s=pr_FreeText)
;


/*****  1.6.7 - Behaviour Statements  *****/

pr_BehaviourStatements returns [StringBuffer s]:
	#(BehaviourStatements
		(
		s=pr_TestcaseInstance
		| s=pr_FunctionInstance
		| s=pr_ReturnStatement
		| s=pr_AltConstruct
		| s=pr_InterleavedConstruct
		| s=pr_LabelStatement
		| s=pr_GotoStatement
		| s=pr_ActivateOp
		| s=pr_DeactivateStatement
		| s=pr_AltstepInstance
		| s=pr_RepeatStatement
		| s=pr_BreakStatement
		| s=pr_ContinueStatement
		)
	)
;

pr_VerdictStatements returns [StringBuffer s]:
	s=pr_SetLocalVerdict
;


pr_VerdictOps returns [StringBuffer s]:
	s=pr_GetLocalVerdict
;


pr_SetLocalVerdict returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(SetLocalVerdict 
		a=pr_SingleExpression 
		{s.append("setverdict" + 
		getOpenParenthesis() + 
		getSpacesAroundParentheses() + 
		a);}
		(	b=pr_LogItem
			{s.append(getComma() + b);}
		)*
		{s.append(getSpacesAroundParentheses() + 
		getClosedParenthesis());})
;


pr_GetLocalVerdict returns [StringBuffer s] {s=new StringBuffer();}:
	GETVERDICT 
	{s.append("getverdict");}
;


pr_SUTStatements returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
// see parser
	#(SUTStatements 
		( 	{s.append("action" + getOpenParenthesis() + getSpacesAroundParentheses());}
	      	(	StringOp 
	      		{s.append("& ");}
	      	)?
			(	a=pr_Expression 
				{s.append(a);}
			)?
		  	{s.append(getSpacesAroundParentheses() + getClosedParenthesis());}
		) 
	)
;

pr_ActionText returns [StringBuffer s]:
	s=pr_FreeText 
	| s=pr_Expression
;

pr_ReturnStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
// see parser.
	#( ReturnStatement 
		{s.append("return");} 
		( 
		  {s.append(" ");} 
		  (
		    a=pr_Expression
		  | a=pr_InLineTemplate
		  | a=pr_TemplateRefWithParList
		  | a=pr_MatchingSymbol
		  )
		  {s.append(a);} 
		)? 
) 
;

pr_AltConstruct returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#( AltConstruct 
		{s.append("alt" + getOpeningBrace()); currentNesting++; } 
		(a=pr_AltGuardList {s.append(a);})?
		{ currentNesting--; s.append(getSpacing() + "}");} 
	)
;

pr_AltGuardList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(AltGuardList 
		(
			(
		    	a=pr_GuardStatement {s.append(a);} | 
		    	b=pr_ElseStatement {s.append(b);} 
		    )
		    (SemiColon {s.append(";");})?
		    {s.append(getNewline());}
		)*
	)
;

pr_GuardStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d,e;}:
	#(GuardStatement a=pr_AltGuardChar {s.append(getSpacing() + a + " ");}
		(
			( 
				b=pr_AltstepInstance {s.append(b); } 
				(
					{s.append(getOpeningBrace()); currentNesting++; } 
					c=pr_StatementBlock 
					{ s.append(c); currentNesting--; s.append(getSpacing() + "}" );}
				)?
			)
			| 
			(
				d=pr_GuardOp {s.append(d);}
				{s.append(getOpeningBrace()); currentNesting++; } 
				e=pr_StatementBlock
				{s.append(e); currentNesting--; s.append(getSpacing() + "}");}
			)
		) 
	)
;

pr_ElseStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ElseStatement 
		{s.append(getSpacing() + "[else]" + getOpeningBrace()); currentNesting++;}
		a=pr_StatementBlock 
		{s.append(a); currentNesting--; s.append(getSpacing() + "}" + getNewline()); }
	)
;

pr_AltGuardChar returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(AltGuardChar ({s.append("[");} ( a=pr_BooleanExpression {s.append(a);} )? {s.append("]");}) )
;

pr_GuardOp returns [StringBuffer s]:
	#(GuardOp
		(
		s=pr_TimeoutStatement
		| s=pr_ReceiveStatement
		| s=pr_TriggerStatement
		| s=pr_GetCallStatement
		| s=pr_CatchStatement
		| s=pr_CheckStatement
		| s=pr_GetReplyStatement
		| s=pr_DoneStatement
		| s=pr_KilledStatement
		)
	)
;

pr_InterleavedConstruct returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; }:
	#(InterleavedConstruct 
		{s.append("interleave" + getOpeningBrace()); currentNesting++;}
		( 
			a=pr_InterleavedGuardElement {s.append(getSpacing() + a + getNewline());} 
			(SemiColon {s.append(";");})? 
		)* 
		{currentNesting--; s.append(getSpacing() + "}");}
	)
;

pr_InterleavedGuardElement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(InterleavedGuardElement 
		a=pr_InterleavedGuard 
		b=pr_InterleavedAction 
	) 
	{s.append(a).append(b);}
;

pr_InterleavedGuard returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(InterleavedGuard 
		a=pr_GuardOp {s.append("[] " + a);}
	)
;

pr_InterleavedAction returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(InterleavedAction {s.append(getOpeningBrace()); currentNesting++;} a=pr_StatementBlock {currentNesting--; s.append( a  + getSpacing() + "} ");} )
;

pr_LabelStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(LabelStatement a=pr_Identifier {s.append("label " + a);})
;


pr_GotoStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(GotoStatement a=pr_Identifier {s.append("goto " + a);})
;


pr_RepeatStatement returns [StringBuffer s] {s=new StringBuffer();}:
	#(RepeatStatement 
		REPEAT 
		{s.append("repeat");}
	)
;

pr_BreakStatement returns [StringBuffer s] {s=new StringBuffer();}:
	#(BreakStatement 
		BREAK 
		{s.append("break");}
	)
;

pr_ContinueStatement returns [StringBuffer s] {s=new StringBuffer();}:
	#(ContinueStatement 
		CONTINUE 
		{s.append("continue");}
	)
;




pr_ActivateOp returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ActivateOp 
		a=pr_AltstepInstance 
		{s.append("activate" +
		getOpenParenthesis() + 
		getSpacesAroundParentheses() + 
		a + 
		getSpacesAroundParentheses() + 
		getClosedParenthesis());}
	)
;


pr_DeactivateStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(DeactivateStatement	
		{s.append("deactivate");}
		(	a=pr_ComponentOrDefaultReference 
			{s.append(getOpenParenthesis() + 
			getSpacesAroundParentheses() + 
			a + 
			getSpacesAroundParentheses() + 
			getClosedParenthesis());}
		)?
	)

;

/***** 1.6.8 - Basic Statements  *****/

pr_BasicStatements returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(BasicStatements
		(
		a=pr_Assignment {s.append(a);}
		| a=pr_LogStatement {s.append(a);}
		| a=pr_LoopConstruct {s.append(a);}
		| a=pr_ConditionalConstruct {s.append(a);}
		| a=pr_SelectCaseConstruct {s.append(a);}
		| (	{s.append(getOpeningBraceNoSpace()); currentNesting++; }
			a=pr_StatementBlock
		  	{s.append(a); currentNesting--; s.append(getSpacing() + "}" );}
		  )
		)
		
	)
;

pr_SelectCaseConstruct returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(SelectCaseConstruct 
		( 	a=pr_SingleExpression 
			{s.append("select (" + 
			//getOpenParenthesis() + 
			getSpacesAroundParentheses() + 
			a + 
			getSpacesAroundParentheses() + 
			getClosedParenthesis() + 
			getOpeningBrace()); 
			currentNesting++; 
			s.append(getSpacing());}
			b=pr_SelectCaseBody
			{s.append(b);}
			{currentNesting--; s.append(getNewline() + getSpacing() + "}");}
		)
	)
;

pr_SelectCaseBody returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; boolean first=true;}:
	#(SelectCaseBody 
		( {if (!first) s.append(getNewline() + getSpacing());} a=pr_SelectCase {s.append(a); first=false;})+ 
	)
;  

pr_SelectCase returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c; boolean first=true;}:
	#(SelectCase 
		(	{s.append("case ");}
			(	//{s.append(getOpenParenthesis() + getSpacesAroundParentheses());}
				{s.append("(" + getSpacesAroundParentheses());}
				(	b=pr_TemplateInstance 
					{if (first) s.append(b); else s.append(getComma() + b); first=false;}
				)* 
				{s.append(getSpacesAroundParentheses() + getClosedParenthesis());} 
			  | ELSE 
			    {s.append("else");}
			)
			{s.append(getOpeningBrace()); currentNesting++;}
			c=pr_StatementBlock {s.append(c);}
			{currentNesting--; s.append(getSpacing() + "} " /*getKRNewline()*/);}
		)
	)
;

pr_Expression returns [StringBuffer s]:
	#(Expression
		(
		s=pr_SingleExpression
		| s=pr_CompoundExpression
		)
	)
;


pr_CompoundExpression returns [StringBuffer s]:
	#(CompoundExpression (s=pr_FieldExpressionList | s=pr_ArrayExpression))
;


pr_FieldExpressionList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a=null; boolean first=true;}:
	#( FieldExpressionList {s.append(getOpeningBraceNoSpace()); currentNesting++;} ( a=pr_FieldExpressionSpec {if (first) s.append(getSpacing() + a); else s.append("," + getNewline() + getSpacing() + a); first=false;} )* ) {currentNesting--; s.append(getNewline() + getSpacing() + "}");}
;


pr_FieldExpressionSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
//	#( FieldExpressionSpec a=pr_FieldReference {s.append(a);} (b=pr_Expression {s.append(b);}|NotUsedSymbol {s.append("-");}))
	#( FieldExpressionSpec a=pr_FieldReference {s.append(a + getAssignment());} #(NotUsedOrExpression (b=pr_Expression {s.append(b);}| (NotUsedSymbol {s.append("-");} ))))
;

pr_ArrayExpression returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a=null;}:
//	#( ArrayExpression ( #(ArrayElementExpressionList    ( (a=pr_Expression {s.append(a);}| NotUsedSymbol {s.append("-");}) )* ) )? )
	#( ArrayExpression (s=pr_ArrayElementExpressionList)? )
;

pr_ArrayElementExpressionList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a; boolean first=true;}:
	#(ArrayElementExpressionList {s.append("{");} (  ({if (!first) s.append(getComma());}#(NotUsedOrExpression (a=pr_Expression {s.append(a);}| NotUsedSymbol {s.append("-");})) {first=false;})* )) {s.append("}");}
;

pr_ConstantExpression returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ConstantExpression (a=pr_SingleConstExpression {s.append(a);} | a=pr_CompoundConstExpression {s.append(a);}))
;

pr_InnerConstantExpression returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ConstantExpression (a=pr_SingleConstExpression {s.append(a);} | s=pr_CompoundConstExpression))
;

pr_SingleConstExpression returns [StringBuffer s]:
	s=pr_SingleExpression
;

pr_BooleanExpression returns [StringBuffer s]:
	#(BooleanExpression s=pr_SingleExpression)
;

pr_CompoundConstExpression returns [StringBuffer s]:
	s=pr_FieldConstExpressionList | s=pr_ArrayConstExpression
;

pr_FieldConstExpressionList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b=null; boolean first=true;}:
	#( FieldConstExpressionList {s.append(getOpeningBraceNoSpace()); currentNesting++;} ( b=pr_FieldConstExpressionSpec {if (!first) s.append("," + getNewline()); s.append(getSpacing() + b); first=false;})* ) {currentNesting--; s.append(getNewline() + getSpacing() + "}" );}
;

pr_FieldConstExpressionSpec returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b=null;}:
	#( FieldConstExpressionSpec a=pr_FieldReference {s.append(a + getAssignment());} b=pr_InnerConstantExpression {s.append(b);})
;

pr_ArrayConstExpression returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
    #( ArrayConstExpression {s.append("{");} ( a=pr_ArrayElementConstExpressionList {s.append(a);} )? {s.append("}");} )
;
 
pr_ArrayElementConstExpressionList returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a=null; boolean first=true;}:
    #( ArrayElementConstExpressionList ( a=pr_InnerConstantExpression {if (first) s.append(a); else s.append(getComma() + a); first=false;} )* )
;

pr_Assignment returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c;}:
	#(Assignment a=pr_VariableRef {s.append(a + getAssignment());} (b=pr_Expression {s.append(b);}|c=pr_TemplateBody {s.append(c);})) 
;

/*
  see parser.
  there are two mutually exlusive solutions below:
  - strictly v3.1.1 expressions,
  - efficient expressions.
*/

// ---------------------------------------------------------------------------------------

// strictly v3.1.1 expressions - begin
/*

pr_SingleExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}:
	#(SingleExpression
			(
				a=pr_XorExpression {s.append(a);} 
				(
					b=pr_XorExpression {s.append(" or " + b);} 
				)*
			)
	 )
;

pr_XorExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}: 
	#(XorExpression
    		(
    			a=pr_AndExpression {s.append(a);} 
    			( 
    		  		b=pr_AndExpression {s.append(" xor " + b);} 
    		 	)*
    		)
	)
;

pr_AndExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}:
	#(AndExpression
         (
			 a=pr_NotExpression {s.append(a);} 
             ( 
              	b=pr_NotExpression {s.append(" and " + b);}
             )*
          )
	)
;

pr_NotExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a;}:
	#(NotExpression
			( 
				( Not {s.append("not ");} )? a=pr_EqualExpression {s.append(a);}
			)
	)
;

pr_EqualExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}:
	#(EqualExpression
			(
				a=pr_RelExpression {s.append(a);} 
				( 
					b=pr_RelExpression {s.append(" == " + b);} 
				)*
			)
	)
;

pr_RelExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}:
	#(RelExpression
				(
				   a=pr_ShiftExpression {s.append(a);} 
				   (
				   #(RelOp
						(
						  Lessthan {s.append(" < ");}
						| Morethan {s.append(" > ");}
						| Notequals {s.append(" != ");}
						| Moreorequal {s.append(" >= ");}
						| Lessorequal {s.append(" <= ");} 
						)
					)
				     b=pr_ShiftExpression {s.append(b);} 
				    )?
				)
	)
;

pr_ShiftExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}: 
	#(ShiftExpression
				(
				   a=pr_BitOrExpression {s.append(a);} 
				   (
				   #(ShiftOp
						(
						  SHIFTLEFT {s.append(" << ");} 
						| SHIFTRIGHT {s.append(" >> ");}  
						| ROTATELEFT {s.append(" <@ ");} 
						| ROTATERIGHT {s.append(" @> ");}  
						)
					)
				     b=pr_BitOrExpression {s.append(b);}
				   )*
				)
	)
;

pr_BitOrExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}:
	#(BitOrExpression
 				(
 				    a=pr_BitXorExpression {s.append(a);} 
 				    ( 
 				      b=pr_BitXorExpression {s.append(" or4b " + b);} 
 				      )*
 				 )
	)
;

pr_BitXorExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}:
	#(BitXorExpression
 					(
 					   a=pr_BitAndExpression {s.append(a);} 
 					   ( 
 					     b=pr_BitAndExpression {s.append(" xor4b " + b);} 
 					    )*
 					)
	)
;

pr_BitAndExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}:
	#(BitAndExpression
				(
				   a=pr_BitNotExpression {s.append(a);} 
				   ( 
				   b=pr_BitNotExpression {s.append(" and4b " + b);} 
				   )*
				)
	)
;

pr_BitNotExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a;}:
	#(BitNotExpression
      ( 
       ( Not4b {s.append("not4b ");} )? a=pr_AddExpression {s.append(a);} 
      )
	)
;

pr_AddExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}: 
	#(AddExpression
				(
				  a=pr_MulExpression {s.append(a);} 	 		  
				  ( 
				  #(AddOp
					  ( 
					    Plus {s.append(" + ");}
					  | Minus {s.append(" - ");}
			 		  | StringOp {s.append(" & ");}
			 		  ) 
			 	  )
				  b=pr_MulExpression {s.append(b);} 
				  )*
				)
	 )
;

pr_MulExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}:
	#(MulExpression
      (
         a=pr_UnaryExpression {s.append(a);} 
         (
         #(MultiplyOp
			 ( 
			   Star {s.append(" * ");}
			 | Slash {s.append(" / ");}
			 | Mod {s.append(" mod ");}
			 | Rem {s.append(" rem ");}
			 ) 
		 )
         b=pr_UnaryExpression {s.append(b);} 
         )*
      )
	)
;

pr_UnaryExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a;}:
	#(UnaryExpression
  		(
	  		#(UnaryOp
	  		  ( 
	  		    Plus {s.append("+");} 
	  		  | Minus {s.append("-");} 
	  		  )
	  		)
  		)? 
  		a=pr_Primary {s.append(a);}
	)
;
*/

// strictly v3.1.1 expressions - end

// ---------------------------------------------------------------------------------------

// efficient expressions - begin

pr_SingleExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b,c;}: 
	#(SingleExpression s=pr_SimpleExpressions)
;

pr_SimpleExpressions returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}: 
	(AND4B | XOR4B | OR4B | AND | XOR | OR | STRINGOP | SHIFTOP | EQUAL) => (
		#(AND4B a=pr_SimpleExpressions b=pr_SimpleExpressions) {s.append(a + " and4b " + b);} |
		#(XOR4B a=pr_SimpleExpressions b=pr_SimpleExpressions) {s.append(a + " xor4b " + b);} |
		#(OR4B a=pr_SimpleExpressions b=pr_SimpleExpressions) {s.append(a + " or4b " + b);} |
		#(AND a=pr_SimpleExpressions b=pr_SimpleExpressions) {s.append(a + " and " + b);} |
		#(OR a=pr_SimpleExpressions b=pr_SimpleExpressions) {s.append(a + " or " + b);} |
		#(XOR a=pr_SimpleExpressions b=pr_SimpleExpressions) {s.append(a + " xor " + b);} |
		#(STRINGOP a=pr_SimpleExpressions b=pr_SimpleExpressions) {s.append(a + " & " + b);} |
		#(EQUAL a=pr_SimpleExpressions b=pr_SimpleExpressions) {s.append(a + " == " + b);} 
	) 
	| s=pr_SimpleExpression
;

pr_SimpleExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}: 
	s=pr_SubExpression |
	(
		#(LESSTHAN a=pr_SubExpression b=pr_SubExpression) {s.append(a + " < " + b);} |
		#(MORETHAN a=pr_SubExpression b=pr_SubExpression) {s.append(a + " > " + b);} |
		#(NOTEQUALS a=pr_SubExpression b=pr_SubExpression) {s.append(a + " != " + b);} |
		#(MOREOREQUAL a=pr_SubExpression b=pr_SubExpression) {s.append(a + " >= " + b);} |
		#(LESSOREQUAL a=pr_SubExpression b=pr_SubExpression) {s.append(a + " <= " + b);}
	)
;

pr_SubExpression returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}: 
	s=pr_Product |
	(
		#(SHIFTLEFT a=pr_Product b=pr_SubExpression) {s.append(a + " << " + b);} |
		#(SHIFTRIGHT a=pr_Product b=pr_SubExpression) {s.append(a + " >> " + b);} |
		#(ROTATELEFT a=pr_Product b=pr_SubExpression) {s.append(a + " <@ " + b);} |
		#(ROTATERIGHT a=pr_Product b=pr_SubExpression) {s.append(a + " @> " + b);}
	)
;

pr_Product returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}: 
	s=pr_Term |
	(
		#(PLUS a=pr_Product b=pr_Product) {s.append(a + " + " + b);} |
		#(MINUS a=pr_Product b=pr_Product) {s.append(a + " - " + b);} |
		#(STRINGOP a=pr_Product b=pr_Product) {s.append(a + " & " + b);}
	)
;

pr_Term returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b;}: 
	s=pr_Factor |
	(
		#(STAR a=pr_Term b=pr_Term) {s.append(a + " * " + b);} |
		#(SLASH a=pr_Term b=pr_Term) {s.append(a + " / " + b);} |
		#(MOD a=pr_Term b=pr_Term) {s.append(a + " mod " + b);} |
		#(REM a=pr_Term b=pr_Term) {s.append(a + " rem " + b);}
	)
;

pr_Factor returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a;}: 
	#(Factor 
		(
			s=pr_Primary |
			(
				#(PLUS a=pr_Primary) {s.append("+" + a);} |
				#(MINUS a=pr_Primary) {s.append("-" + a); } |
				#(NOT a=pr_Primary) {s.append("not " + a);} |
				#(NOT4B a=pr_Primary) {s.append("not4b " + a);}
			)
		)
	)
;

// efficient expressions - end

// ---------------------------------------------------------------------------------------

pr_Primary returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}: 
	#(Primary 
		(	(	pr_OpCall 
			) => s=pr_OpCall
		  | s=pr_Value
		  | a=pr_SingleExpression 
		    {s.append(getOpenParenthesis() + 
		    getSpacesAroundParentheses() + 
		    a + 
		    getSpacesAroundParentheses() + 
		    getClosedParenthesis());}
		)
	)
;

// see parser
pr_ExtendedFieldReference returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a=null;}:
	#(ExtendedFieldReference 
		(	(	(	(	a=pr_Identifier
		  			  | a=pr_PredefinedType
		  			) 
		  			{s.append(".");}
		  		  | a=pr_ArrayOrBitRef) 
		  		    {s.append(a);}
		  		  | NotUsedSymbol
		  		    {s.append("[-]");}
		  		)+
		  	)
		)
;

pr_OpCall returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(OpCall
		(
		s=pr_ConfigurationOps
		| s=pr_VerdictOps
		| s=pr_TimerOps
		| s=pr_TestcaseInstance
		| s=pr_FunctionInstance
		  (	a=pr_ExtendedFieldReference
		  	{s.append(a);}
		  )?
		| s=pr_TemplateOps
		  (	a=pr_ExtendedFieldReference
		  	{s.append(a);}
		  )?
		| s=pr_ActivateOp
		)
	)
;

// ---------------------------------------------------------------------------------------

pr_LogItem returns [StringBuffer s]:
	#(LogItem( s=pr_FreeText | s=pr_TemplateInstance ))
;

pr_LogStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#( LogStatement 
		{s.append("log" + 
		getOpenParenthesis() + 
		getSpacesAroundParentheses()
		);}
		(
			a=pr_LogItem {s.append(a);} 
			(b=pr_LogItem {s.append(getComma() + b);})*  
		) 
		{s.append(getSpacesAroundParentheses() + getClosedParenthesis());}
	)
;


pr_LoopConstruct returns [StringBuffer s]: 
	#(LoopConstruct
		(	s=pr_ForStatement
		  | s=pr_WhileStatement
		  | s=pr_DoWhileStatement
		)
	)
;

pr_ForStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b,c,d,e;}:
	#(ForStatement 
		{s.append("for ("+
		//getOpenParenthesis() + 
		getSpacesAroundParentheses());} 
		(	a=pr_VarInstance 
			{s.append(a);}
		  | b=pr_Assignment 
		    {s.append(b);} 
		) 
		{s.append("; ");}
		c=pr_BooleanExpression {s.append(c);}
		{s.append("; ");}
		d=pr_Assignment 
		{s.append(d + 
		getSpacesAroundParentheses() + 
		getClosedParenthesis() + 
		getOpeningBrace()); 
		currentNesting++;} 
		e=pr_StatementBlock 
		{ s.append(e); currentNesting--;s.append(getSpacing() + "}");}
	)
;


pr_WhileStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(WhileStatement 
		(	a=pr_BooleanExpression 
			{s.append("while (" + 
			//getOpenParenthesis() + 
			getSpacesAroundParentheses() + 
			a + 
			getSpacesAroundParentheses() + 
			getClosedParenthesis() + 
			getOpeningBrace()); 
			currentNesting++;}
			b=pr_StatementBlock 
			{s.append(b); currentNesting--;s.append(getSpacing() + "}");}			
		)
	)
;


pr_DoWhileStatement returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a,b;}:
	#(DoWhileStatement 
		(	{s.append("do" + getOpeningBrace()); currentNesting++;}
			a=pr_StatementBlock
			{s.append(a); currentNesting--; s.append(getSpacing() + "} " + getKRNewline());}
			b=pr_BooleanExpression 
			{s.append("while (" + 
			//getOpenParenthesis() + 
			getSpacesAroundParentheses() + 
			b + 
			getSpacesAroundParentheses() + 
			getClosedParenthesis());}
		)
	) 
;

pr_ConditionalConstruct returns [StringBuffer s] {s = new StringBuffer(); StringBuffer a,b,c,d=null;}: 
	#(ConditionalConstruct
		a=pr_BooleanExpression 
		{s.append(getNewline() + 
		getSpacing() + 
		"if (" +
		//getOpenParenthesis() + 
		getSpacesAroundParentheses() + 
		a + 
		getSpacesAroundParentheses() + 
		getClosedParenthesis() + 
		getOpeningBrace());}
		{currentNesting++;} 
		b=pr_StatementBlock 
		{s.append(b);}
		{currentNesting--;}
		{s.append(getSpacing() + "}");}
		(	c=pr_ElseIfClause 
			{s.append(" " +c);} 
		)* 
		(	d=pr_ElseClause 
			{s.append(" " + d);}
		)? 
	)
;

pr_ElseIfClause returns [StringBuffer s]{s=new StringBuffer(); StringBuffer a,b;}:
	#(ElseIfClause 
		(	a=pr_BooleanExpression 
			{s.append(getKRNewline() + 
			"else if (" + 
			//getOpenParenthesis() + 
			getSpacesAroundParentheses() + 
			a + 
			getSpacesAroundParentheses() + 
			getClosedParenthesis() + 
			getOpeningBrace()); 
			currentNesting++;}
			b=pr_StatementBlock 
			{s.append(b); currentNesting--; s.append(getSpacing() + "}");}
		) 
	)
;


pr_ElseClause returns [StringBuffer s] {s=new StringBuffer(); StringBuffer a;}:
	#(ElseClause 
		(	{s.append(getKRNewline() + 
			"else" + 
			getOpeningBrace()); 
			currentNesting++;}
			a=pr_StatementBlock
			{s.append(a); currentNesting--; s.append(getSpacing() + "}");}
		)
	)
;

pr_InOutParKeyword returns [StringBuffer s] {s=new StringBuffer();}:
	#(InOutParKeyword a:INOUT {s.append(a.getText());})
;
