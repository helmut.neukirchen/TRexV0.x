package de.ugoe.cs.swe.trex.core.formatter;

import java.util.LinkedList;
import java.util.List;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TokenStreamTracker;

public class TTCN3TokenStreamWeaver {
	TTCN3TokenStream source;

	TTCN3TokenStream target;

	TTCN3FormatterTools tools;

	String currentIndentation = "";

	public TTCN3TokenStreamWeaver(TTCN3TokenStream source,
			TTCN3TokenStream target) {
		this.source = source;
		this.target = target;
	}

	// ------------------------------------------------------------------------------------------------------

	public TTCN3TokenStreamWeaver(TokenStreamTracker source,
			TokenStreamTracker target, TTCN3FormatterTools tools) {
		this.source = new TTCN3TokenStream(source);
		this.target = new TTCN3TokenStream(target);
		this.tools = tools;
	}

	// ------------------------------------------------------------------------------------------------------

	public TTCN3FormatterTools getTools() {
		return tools;
	}

	// ------------------------------------------------------------------------------------------------------

	public void setTools(TTCN3FormatterTools tools) {
		this.tools = tools;
	}

	// ------------------------------------------------------------------------------------------------------

	public TTCN3TokenStream getSource() {
		return source;
	}

	// ------------------------------------------------------------------------------------------------------

	public void setSource(TTCN3TokenStream source) {
		this.source = source;
	}

	// ------------------------------------------------------------------------------------------------------

	public TTCN3TokenStream getTarget() {
		return target;
	}

	// ------------------------------------------------------------------------------------------------------

	public void setTarget(TTCN3TokenStream target) {
		this.target = target;
	}

	// ------------------------------------------------------------------------------------------------------

	public String weave() throws TokenStreamMismatchException, Exception {
		StringBuffer weavedText = new StringBuffer();
		ScanResult scan = new ScanResult();
		List<Comment> commentTokens;

		target.reset();
		source.reset();

		// sync first token
		int lastTargetIndex = 0;
		if (target.getElement().getType() == TTCN3LexerTokenTypes.WS) {
			scan = target.nextTokenSkip(TTCN3TokenStream.SkipTypes.WHITESPACES);
		}
		// if (!target.getElement().getText()
		// .equals(source.getElement().getText())) {
		while (!target.getElement().getText().equals(
				source.getElement().getText())) {
			commentTokens = findCommentTokens();
			StringBuffer comments = evaluateCommentTokens(commentTokens);
			weavedText.append(comments);
			Comment c;
			if (commentTokens.size() > 0) {
				c = commentTokens.get(commentTokens.size() - 1);
				if (!c.isMultiLineComment()) {
					weavedText.append(tools.getNewline());
				}
			}
			source
					.nextTokenSkip(TTCN3TokenStream.SkipTypes.WHITESPACES_AND_COMMENTS);
		}

		while (target.hasNextToken() || (target.index > lastTargetIndex)) {
			String scanBuffer = scan.getStringBuffer().toString();
			weavedText.append(scanBuffer);
			String elementText = target.getElement().getText();
			weavedText.append(elementText);
			lastTargetIndex = target.index;

			scan = target.nextTokenSkip(TTCN3TokenStream.SkipTypes.WHITESPACES);

			if (scan.getIndentationSinceNewline() != null)
				currentIndentation = scan.getIndentationSinceNewline();

			commentTokens = findCommentTokens();
			StringBuffer evaluatedCommentTokens = evaluateCommentTokens(commentTokens);
			weavedText.append(evaluatedCommentTokens);

			source
					.nextTokenSkip(TTCN3TokenStream.SkipTypes.WHITESPACES_AND_COMMENTS);

			if ((!source.hasNextToken()) && (!target.hasNextToken())
					&& (source.isCommentToken())) {
				if (!source.isNewlineBeforeLast()
						&& (source.isPreviousTokenCommentToken())) {
					weavedText.append(tools.getNewline());
				}
				weavedText.append(source.getElement().getText().replaceAll(
						"[\r\n]", tools.getNewline()));
				break;
			}

			if ((target.hasNextToken())
					&& ((!target.getElement().getText().equals(
							source.getElement().getText())))) {
				String eMessage = "TTCN3TokenStreams don't match!\ntarget: "
						+ target.getElement() + "source: "
						+ source.getElement();
				throw new TokenStreamMismatchException(eMessage, source, target);
			}

			if ((commentTokens != null)
					&& (commentTokens.size() > 0)
					&& (!commentTokens.get(commentTokens.size() - 1).multiLineComment)
					&& (target.hasPreviousToken())
					&& (!target.previousTokenContainsNewline()))
				weavedText.append(tools.getNewline());
		}

		return weavedText.toString();
	}

	// ------------------------------------------------------------------------------------------------------

	private List<Comment> findCommentTokens() throws Exception {
		List<Comment> commentTokens = new LinkedList<Comment>();
		commentTokens.clear();
		source.saveIndexPosition();
		while (source.hasNextToken()) {
			if ((source.getElement().getText().equals(target.getElement()
					.getText()))
					&& (source.index > source.getBackupIndexPosition()))
				break;

			if (source.isCommentToken()) {
				Comment comment = Comment
						.createCommentFromTokenStreamPosition(source);
				commentTokens.add(comment);
			}
			source.nextTokenSkip(TTCN3TokenStream.SkipTypes.WHITESPACES);
		}
		source.restoreIndexPosition();
		return commentTokens;
	}

	// ------------------------------------------------------------------------------------------------------

	private StringBuffer evaluateSingleLineComment(Comment c, ScanResult scan,
			boolean newLineSingleLineComment) throws Exception {
		StringBuffer formattedComment = new StringBuffer();

		if (c.isNewlineBefore()) {
			formattedComment.append(tools.getNewline()
//					+ tools.getNewline()
					+ scan.getStringBuffer()
					+ c.getCommentText().get(0));
//			newLineSingleLineComment = true;
		} else {
			if (newLineSingleLineComment) {
				formattedComment.append(tools.getNewline());
				formattedComment.append(scan.getStringBuffer()
						+ c.getCommentText().get(0));
			} else {
				// formattedComment.append(" " +
				// c.getCommentText().get(0) );
				// System.out.println("target element: " +
				// target.getElement());
				if (target.scanBackwardNewlines() > 0) {
					formattedComment.append(" " + c.getCommentText().get(0));
//					newLineSingleLineComment = true;
				} else {
					ScanResult lastSpaces = target.scanBackwardSpaces();
					if (lastSpaces.getWhiteSpaceCount() > 0)
						formattedComment.append(" "
								+ c.getCommentText().get(0)
								+ tools.getNewline()
								+ currentIndentation.substring(0,
										currentIndentation.length()
												- lastSpaces.getWhiteSpaceCount()));
					else
						formattedComment.append(" "
								+ c.getCommentText().get(0)
								+ tools.getNewline()
								+ currentIndentation);
				}
			}
		}
		return formattedComment;
	}

	private StringBuffer evaluateMultilineComment(Comment c, ScanResult scan)
			throws Exception {
		StringBuffer formattedComment = new StringBuffer();
		if (c.isNewlineBefore() || (!target.hasPreviousToken())) {
			if (target.hasPreviousToken()) {
				formattedComment.append(tools.getNewline());
			}
			//Uncomment additional condition to lose new lines before block comments
			if (c.isNewlineBefore()) {// && target.isPreviousTokenCommentToken()) {
				formattedComment.append(tools.getNewline());
			}
			for (int n = 0; n < c.getCommentText().size(); n++) {
				// TODO: add option to preserve line structure of comments
				String mlcPrefix = "";
				String leadInSpace = scan	.getStringBuffer()
											.toString()
											.replaceAll("[\r\n]", "");
				formattedComment.append(leadInSpace);

				String comment = c.getCommentText().get(n);

				comment = comment.replaceAll("\r\n", "");

				//first line
				comment = comment.replaceAll("\\/\\*(\\*?)\\s*([^\\*]+)", "/*"+
						"$1"
						+ tools.getNewline()
						+ leadInSpace
						+ mlcPrefix
						+ "$2".replaceAll("^[\\s\\*]*", mlcPrefix + " * "));

				if (n != 0 && n != c.getCommentText().size()-1) {
					comment = comment.replaceAll("^[\\*\\s]*", mlcPrefix + " * ");
				}

				//last line
				comment = comment.replaceAll("^\\s*\\*\\/", " */");
		//		System.out.println("| "+comment+" |");
				comment = comment.replaceAll("^\\s{2}"," * ");	// leading star is needed to replace leading spaces
				comment = comment.replaceAll("([^\\s\\*]+)\\s*\\*\\/", "$1".replaceAll("^[\\s\\*]", mlcPrefix + " * ")
						+ tools.getNewline()
						+ leadInSpace
						+ " */");
		//		System.out.println("| "+comment+" |");
				formattedComment.append(comment);
				// System.out.println("comment: " +
				// c.getCommentText().get(n));
				// System.out.println("target" + target.getElement());
				// System.out.println("3|"+formattedComment+"|");
				if (n != c.getCommentText().size() - 1) {
					//exclude last line to avoid too much empty space
					if (!(target.previousTokensContainNewline() || (target	.getElement()
																			.getType() == TTCN3LexerTokenTypes.IF))) {
						formattedComment.append(tools.getNewline());
					}
				}

			}
			if (!target.hasPreviousToken()){ //&& !target.isPreviousTokenCommentToken()) {
				//for module level comments
				formattedComment.append(tools.getNewline());
			}

		} else {
			for (int n = 0; n < c.getCommentText().size(); n++) {
				formattedComment.append(" ")
								.append(c	.getCommentText()
											.get(n)
											.replaceAll("\r\n", ""));
			}
		}
		return formattedComment;
	}

	// ------------------------------------------------------------------------------------------------------

	private StringBuffer evaluateCommentTokens(List<Comment> comments)
			throws Exception {
		if (comments == null)
			return new StringBuffer("");
		StringBuffer formattedComment = new StringBuffer();
		boolean newLineSingleLineComment = false;

		ScanResult scan = target.scanBackwardSpaces();

		for (int i = 0; i < comments.size(); i++) {
			Comment c = comments.get(i);
			if (!c.isMultiLineComment()) { // single line comment
				formattedComment.append(evaluateSingleLineComment(c,
						scan,
						newLineSingleLineComment));
			} else { // multiline comment
				formattedComment.append(evaluateMultilineComment(c, scan));
			}
		}
		return formattedComment;
	}

}
