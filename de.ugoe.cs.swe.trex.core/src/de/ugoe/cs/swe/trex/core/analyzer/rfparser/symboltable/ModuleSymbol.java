package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import java.util.ArrayList;
import java.util.List;

import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;

public class ModuleSymbol extends Symbol implements ITTCN3EclipseSymbol {

	private String languageSpecification = "";
	private List<String> friendModules = new ArrayList<String>();
	
	public String getLanguageSpecification() {
		return languageSpecification;
	}

	public void setLanguageSpecification(String languageSpecification) {
		this.languageSpecification = languageSpecification;
	}

	@Override
	public String getSymbolDescription() {
		StringBuffer result = new StringBuffer();
		HTMLPrinter.getInstance().addPageProlog(result);
		HTMLPrinter.getInstance().addSmallHeader(result, "Module Name: ");
		HTMLPrinter.getInstance().addLine(result, getName());
//		if (getLanguageSpecification().length() > 0) {
//			HTMLPrinter.addSmallHeader(result, "Language Specification: ");
//			HTMLPrinter.addLine(result, getLanguageSpecification());
//		}
		HTMLPrinter.getInstance().addPageEpilog(result);
		return result.toString();
	}

	@Override
	public String getSymbolDescriptionPlain() {
		return "module " + getName();
	}

	@Override
	public String getSymbolDescriptionContentAssist() {
		return getName() + " - module" ;
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ModuleSymbol) {
			ModuleSymbol s = (ModuleSymbol) obj;
			if (s.getName().equals(getName()))
				return true;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return getName().hashCode();
	}

	public void addFriendModule(String friendModuleName) {
		this.friendModules.add(friendModuleName);
	}

	public List<String> getFriendModules() {
		return friendModules;
	}
	
	public boolean isFriendModule(String moduleName){
		return this.friendModules.contains(moduleName);
	}

}
