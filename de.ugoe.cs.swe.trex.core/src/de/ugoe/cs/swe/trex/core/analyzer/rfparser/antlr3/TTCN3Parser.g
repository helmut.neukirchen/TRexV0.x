parser grammar TTCN3Parser;

options {
  output = AST;
  ASTLabelType=CommonTree;
  memoize = true;
  backtrack = true;
  tokenVocab = TTCN3Lexer;
}

@header {
package de.ugoe.cs.swe.trex.core.analyzer.rfparser.antlr3;
}

program
  : MODULE IDENTIFIER BEGINCHAR ENDCHAR -> ^(MODULE IDENTIFIER)
  ;