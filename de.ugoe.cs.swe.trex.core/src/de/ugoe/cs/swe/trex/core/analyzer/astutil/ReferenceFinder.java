package de.ugoe.cs.swe.trex.core.analyzer.astutil;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.ModuleSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.TypeSymbol;

/**
 * ReferenceFinder class
 * 
 * Find references of symbols
 * @see ReferenceWithContext
 */
public class ReferenceFinder {

	private static final int[] REFERENCE_INTRODUCING_TYPES = {
			TTCN3LexerTokenTypes.VariableRef,
			TTCN3LexerTokenTypes.PredefinedValue,
			TTCN3LexerTokenTypes.TestcaseRef,
			TTCN3LexerTokenTypes.ComponentType, TTCN3LexerTokenTypes.PortOrAny,
			TTCN3LexerTokenTypes.ReferencedType, TTCN3LexerTokenTypes.TimerRef,
			TTCN3LexerTokenTypes.AltstepInstance,
			TTCN3LexerTokenTypes.FunctionRef,
			TTCN3LexerTokenTypes.ImportFromSpec,
			TTCN3LexerTokenTypes.TypeRefList,
			TTCN3LexerTokenTypes.ConstRefList,
			TTCN3LexerTokenTypes.TemplateRefList,
			TTCN3LexerTokenTypes.ModuleParRefList,
			TTCN3LexerTokenTypes.SignatureRefList,
			TTCN3LexerTokenTypes.FunctionRefList,
			TTCN3LexerTokenTypes.AltstepRefList,
			TTCN3LexerTokenTypes.TestcaseRefList, TTCN3LexerTokenTypes.Port,
			TTCN3LexerTokenTypes.PortType,
			TTCN3LexerTokenTypes.ReferencedValue,
			TTCN3LexerTokenTypes.FieldReference,
			TTCN3LexerTokenTypes.DerivedDef };

	public static final int[] CONTEXT_INTRODUCING_TYPES = {
			TTCN3LexerTokenTypes.TTCN3Module, TTCN3LexerTokenTypes.GroupDef,
			TTCN3LexerTokenTypes.TestcaseDef, TTCN3LexerTokenTypes.FunctionDef,
			TTCN3LexerTokenTypes.AltstepDef,
			TTCN3LexerTokenTypes.ModuleControlPart,
			TTCN3LexerTokenTypes.ComponentDef };

	private final Stack<LocationAST> contextNodeStack = new Stack<LocationAST>();

	private LocationAST referenceIntroducingNode = null;

	private static final int[] RESOLVABLE_PARENTNODE_TYPES = {
			TTCN3LexerTokenTypes.FunctionStatement,
			TTCN3LexerTokenTypes.FunctionLocalDef,
			TTCN3LexerTokenTypes.FunctionLocalInst,
			TTCN3LexerTokenTypes.AltstepLocalDefList,
			TTCN3LexerTokenTypes.ComponentElementDef,
			TTCN3LexerTokenTypes.ControlStatementOrDef,
			TTCN3LexerTokenTypes.FunctionDef, TTCN3LexerTokenTypes.AltstepDef,
			TTCN3LexerTokenTypes.TestcaseDef, TTCN3LexerTokenTypes.TTCN3Module }; // order

	// matters!!!

	// --------------------------------------------------------------

	/**
	 * Find all references to the given <code>Symbol</code>
	 * 
	 * @param s <code>Symbol</code> to search for
	 * @return a <code>Map</code> of filenames (<code>Strings</code>) to
	 *         <code>ReferenceWithContext</code> objects
	 */
	public Map<String, List<ReferenceWithContext>> findReferences(Symbol s) {
		List<Symbol> list = new LinkedList<Symbol>();
		list.add(s);

		return findReferences(list);
	}

	/**
	 * Find all references to the given <code>Symbol</code>
	 * 
	 * @param s <code>Symbol</code> to search for
	 * @param filterFilename Only search in the given filename (or null for all files)
	 * @return a <code>Map</code> of filenames (<code>Strings</code>) to
	 *         <code>ReferenceWithContext</code> objects
	 */	
	public Map<String, List<ReferenceWithContext>> findReferences(Symbol s, String filterFilename) {
		List<Symbol> list = new LinkedList<Symbol>();
		list.add(s);

		return findReferences(list, filterFilename);
	}

	// --------------------------------------------------------------

	/**
	 * Find all references to <code>Symbol</code> in the given
	 * <code>List</code>
	 * 
	 * @param symbols
	 *            the <code>List</code> of symbols to search for
	 * @return a <code>Map</code> of filenames (<code>Strings</code>) to
	 *         <code>ReferenceWithContext</code> objects
	 */
	public Map<String, List<ReferenceWithContext>> findReferences(
			List<Symbol> symbols) {
	
		return findReferencesToSymbols(symbols, null);
	}
	/**
	 * Find all references to <code>Symbol</code> in the given
	 * <code>List</code>
	 * 
	 * @param symbols
	 *            the <code>List</code> of symbols to search for
	 * @param filterFilename Only search in the given filename (or null for all files)            
	 * @return a <code>Map</code> of filenames (<code>Strings</code>) to
	 *         <code>ReferenceWithContext</code> objects
	 */

	public Map<String, List<ReferenceWithContext>> findReferences(
			List<Symbol> symbols, String filterFilename) {
	
		return findReferencesToSymbols(symbols, filterFilename);
	}

	/**
	 * Find all references to <code>Symbol</code> in the given
	 * <code>List</code>
	 * 
	 * @param symbols
	 *            the <code>List</code> of symbols to search for
	 * @param filterFilename Only search in the given filename (or null for all files)
	 * @return a <code>Map</code> of filenames (<code>Strings</code>) to
	 *         <code>ReferenceWithContext</code> objects
	 */
	private Map<String, List<ReferenceWithContext>> findReferencesToSymbols(
			List<Symbol> symbols, String filterFilename) {

		Map<String, List<ReferenceWithContext>> result = new HashMap<String, List<ReferenceWithContext>>();
		Map<Scope, List<LocationAST>> scopeNodes = new HashMap<Scope, List<LocationAST>>();

		for (Symbol symb : symbols) {
			// get declaration node
			LocationAST node = symb == null ? null : symb.getDeclarationNode();

			if (node == null)
				continue;

			List<Scope> validScopes = getValidScopes(symb, node);
			// add nodes to list associated with scope
			for (Scope scope : validScopes) {
				List<LocationAST> nodes = scopeNodes.get(scope);
				if (nodes == null) {
					nodes = new LinkedList<LocationAST>();
				}
				nodes.add(node);
				scopeNodes.put(scope, nodes);
			}
		}

		// find references for every valid scope
		for (Map.Entry<Scope, List<LocationAST>> entry : scopeNodes.entrySet()) {
			final Scope scope = entry.getKey();
			final List<LocationAST> nodes = entry.getValue();
			final Symbol declarationSymbol = scope.getScopeSymbol();

			if (nodes == null || nodes.size() == 0 || declarationSymbol == null) {
				continue;
			}

			if (nodes.size() > 1) {
				// sort the matchnodes array
				Collections.sort(nodes);
			}

			String filename = scope.getAssociatedFilename();
			
			if (filterFilename != null && !filename.equals(filterFilename))
				continue;
				
			List<ReferenceWithContext> list = result.get(filename);
			if (list == null) {
				list = new LinkedList<ReferenceWithContext>();
			}

			LocationAST declarationNode = declarationSymbol
					.getDeclarationNode();
			LocationAST resolvableParentNode = null;

			if (declarationSymbol instanceof ModuleSymbol) {
				// we are inspecting a module scope
				resolvableParentNode = LocationAST.resolveParentsUntilType(
						declarationNode, TTCN3LexerTokenTypes.TTCN3Module);
				if (resolvableParentNode != null) {
					findReferencesRecursion(resolvableParentNode, nodes, list);
				}
			} else {
				// we are inspecting some subscope
				resolvableParentNode = LocationAST.resolveParentsUntilType(
						declarationNode,
						ReferenceFinder.RESOLVABLE_PARENTNODE_TYPES);
				if (resolvableParentNode != null) {
					LocationAST sibling = resolvableParentNode;
					while (sibling != null) {
						findReferencesRecursion(sibling, nodes, list);
						sibling = sibling.getNextSibling();
					}
				}
			}

			// if any references were found add them to the mapping for this
			// filename
			if (list.size() > 0) {
				result.put(filename, list);
			}
		}

		return result;
	}

	// --------------------------------------------------------------

	/**
	 * Counts all references to this symbol.
	 * 
	 * @param symbol
	 *            the symbol
	 * @return the number of references
	 * 
	 *         FIXME: Exception handling?
	 */
	public int countReferences(Symbol symbol) {
		Map<String, List<ReferenceWithContext>> referencesMap = findReferences(symbol);
		int result = 0;
		for (Map.Entry<String, List<ReferenceWithContext>> entry : referencesMap
				.entrySet()) {
			List<ReferenceWithContext> referencesList = entry.getValue();
			if (referencesList != null) {
				result += referencesList.size();
			}
		}
		return result;
	}

	// --------------------------------------------------------------

	/**
	 * Counts all scopes with references to this symbol.
	 * 
	 * @param symbol
	 *            the symbol
	 * @return the number of scopes with references to the symbol
	 * 
	 *         FIXME: Exception handling?
	 */
	public int countReferencingScopes(Symbol symbol) {
		Map<String, List<ReferenceWithContext>> referencesMap = findReferences(symbol);
		Set<Scope> scopeSet = new HashSet<Scope>();
		for (List<ReferenceWithContext> referencesList : referencesMap.values()) {
			for (ReferenceWithContext ref : referencesList) {
				Scope refScope = ref.getReferenceNode().getScope();
				if (!scopeSet.contains(refScope)) {
					scopeSet.add(refScope);
				}
			}
		}
		return scopeSet.size();
	}

	// --------------------------------------------------------------

	/**
	 * Find scopes the <code>Symbol</code> could be visible in.
	 * 
	 * @param symb
	 *            the <code>Symbol</code>
	 * @param node
	 *            the <code>LocationAST</code> node
	 * @return a <code>List</code> of scopes the <code>Symbol</code> is visible
	 *         in
	 */
	private List<Scope> getValidScopes(Symbol symb, LocationAST node) {
		List<Scope> validScopes = new LinkedList<Scope>();

		// check whether the declaration is a module declaration, a
		// function/testcase/altstep/control declaration
		// or on file scope and find out if the symbol could be visible in these
		// scopes
		final Symbol nodeScopeSymbol = node.getScope().getScopeSymbol();
		if ((nodeScopeSymbol instanceof ModuleSymbol)
				|| (symb instanceof ModuleSymbol)
				|| (nodeScopeSymbol instanceof TypeSymbol)) {
			Scope declarationModuleScope = null;

			if (symb instanceof ModuleSymbol)
				declarationModuleScope = node.getChildScope();
			else
				declarationModuleScope = node.getScope();

			if (!(declarationModuleScope.getScopeSymbol() instanceof ModuleSymbol))
				declarationModuleScope = declarationModuleScope
						.getModuleScope();

			// try to resolve the symbol directly
			String assocFilename = declarationModuleScope
					.getAssociatedFilename();
			String moduleName = declarationModuleScope.getScopeSymbol()
					.getName();
			TTCN3AnalyzerFlyweightFactory analyzerFactory = TTCN3AnalyzerFlyweightFactory
					.getInstance();
			List<Scope> foundScopes = analyzerFactory
					.getModuleScopesWithImport(moduleName, symb, assocFilename);

			// try to resolve the scope declaration symbol if nothing was found
			if ((foundScopes == null) || (foundScopes.size() <= 0)) {
				if (symb != null) {
					final Scope surroundingScope = symb.getSurroundingScope();
					final Symbol scopeSymbol = surroundingScope
							.getScopeSymbol();
					if (scopeSymbol instanceof TypeSymbol
							&& ((TypeSymbol) scopeSymbol).getStructuredType() != null) {
						assocFilename = surroundingScope
								.getAssociatedFilename();
						List<Scope> moduleScopes = analyzerFactory
								.getModuleScopesWithImport(
										declarationModuleScope.getScopeSymbol()
												.getName(), scopeSymbol,
										assocFilename);
						if ((moduleScopes != null) && (moduleScopes.size() > 0))
							validScopes.addAll(moduleScopes);
					}
				}
			} else {
				// else add to list of valid scopes
				validScopes.addAll(foundScopes);
			}

			validScopes.add(declarationModuleScope);
		} else {
			validScopes.add(node.getScope());
		}
		return validScopes;
	}

	// --------------------------------------------------------------

	/**
	 * Method to find references and add them to the passed <code>List</code> in
	 * the form of <code>ReferenceWIthContext</code> objects.
	 * 
	 * This function works recursively for the children of the node given
	 * as parameter. If the node is a leaf, then it finds the references
	 * in the leaf. References found are added to the result parameter.
	 * 
	 * FIXME: check the accuracy of these definitions and complete where
	 * appropriate...
	 * 
	 * @param node
	 *            The <code>AST</code> node at which to look for a reference.
	 * @param matchNode
	 *            The <code>List</code> of <code>AST</code> nodes whose
	 *            references should be found.
	 * @param result
	 *            The <code>List</code> object to which the results should be
	 *            added.
	 */
	private void findReferencesRecursion(LocationAST node,
			List<LocationAST> matchNodes, List<ReferenceWithContext> result) {
		LocationAST currentNode = node.getFirstChild();

		if (currentNode == null) { // leaf node
			// check if identifier
			if ((node.getType() == TTCN3LexerTokenTypes.IDENTIFIER)
					&& (referenceIntroducingNode != null)) {

				LocationAST matchNode = null;
				if (matchNodes.size() > 1) {
					int found = Collections.binarySearch(matchNodes, node);
					if (found >= 0) {
						matchNode = matchNodes.get(found);
					}
				} else {
					matchNode = matchNodes.get(0);
				}

				if (matchNode == null)
					return;

				final Scope scope = node.getScope();
				Symbol symbol = (scope == null) ? null : scope.resolve(node);

				if (symbol == null)
					return;

				// if a reference was found, it creates the Reference object
				if (symbol.getDeclarationNode().equals(matchNode)) {
					ReferenceWithContext ref = new ReferenceWithContext();
					ref.setReferenceNode(node);
					ref.setMatchNode(matchNode);
					if (contextNodeStack.empty()) {
						LocationAST contextNode = LocationAST
								.resolveParentsUntilType(node,
										ReferenceFinder.CONTEXT_INTRODUCING_TYPES);
						ref.setContextNode(contextNode);
					} else {
						ref.setContextNode(contextNodeStack.peek());
					}
					result.add(ref);
				}
			}
		} else { // look down the tree for identifier leaf nodes
			while (currentNode != null) {
				if (isContextIntroducingType(currentNode))
					contextNodeStack.push(currentNode);

				if (isReferenceIntroducingType(currentNode)) {
					referenceIntroducingNode = currentNode;
				}

				// recursive call on the child node
				findReferencesRecursion(currentNode, matchNodes, result);

				if (isContextIntroducingType(currentNode))
					contextNodeStack.pop();

				if (isReferenceIntroducingType(currentNode))
					referenceIntroducingNode = null;

				// switch to the next child of the node
				currentNode = currentNode.getNextSibling();
			}
		}
	}

	// --------------------------------------------------------------

	private boolean isReferenceIntroducingType(LocationAST node) {
		final int type = node.getType();
		for (int element : ReferenceFinder.REFERENCE_INTRODUCING_TYPES) {
			if (type == element)
				return true;
		}

		return false;
	}

	// --------------------------------------------------------------

	private boolean isContextIntroducingType(LocationAST node) {
		final int type = node.getType();
		for (int element : ReferenceFinder.CONTEXT_INTRODUCING_TYPES) {
			if (type == element)
				return true;
		}

		return false;
	}

}
