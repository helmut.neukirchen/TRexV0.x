package de.ugoe.cs.swe.trex.core.visitor;

import de.ugoe.cs.swe.trex.core.TTCN3CorePlugin;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.visitor.AbstractVisitor.ContinueStatus;

public class CheckAST {

	public ContinueStatus searchASTDepthFirst(LocationAST node) throws TTCN3BehaviorException {
        if (node == null)
            return ContinueStatus.getInstance(true, true);

        LocationAST next = node;
        while (next != null) {
            ContinueStatus continueStatus = checkASTNode(next);
            if (!continueStatus.continueSearch)
                return ContinueStatus.getInstance(false, false);
            if (continueStatus.goDeeper)
                searchASTDepthFirst(next.getFirstChild());
            next = next.getNextSibling();
        }
        return ContinueStatus.getInstance(true, true);
    }

	private ContinueStatus checkASTNode(LocationAST next) {
		// Check that offsets of each node are reasonable
		if(next.getEndOffset() == -1 || next.getOffset() == -1 || next.getEndOffset() < next.getOffset())
		{
			TTCN3CorePlugin.debugMessage("Warning: AST node "+next.getText()+" has wrong offsets!");
			return ContinueStatus.getInstance(false, false);
		}
		return ContinueStatus.getInstance(true, true);
	}

}
