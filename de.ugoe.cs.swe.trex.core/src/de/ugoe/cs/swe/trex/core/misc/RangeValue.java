package de.ugoe.cs.swe.trex.core.misc;
public class RangeValue<K> {
	Range range = null;

	K value;

	public RangeValue(Range range, K value) {
		this.range = range;
		this.value = value;
	}
	
	public Range getRange() {
		return range;
	}

	public void setRange(Range range) {
		this.range = range;
	}

	public K getValue() {
		return value;
	}

	public void setValue(K value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "range: " + range + " - value " + value;
	}
	
}
