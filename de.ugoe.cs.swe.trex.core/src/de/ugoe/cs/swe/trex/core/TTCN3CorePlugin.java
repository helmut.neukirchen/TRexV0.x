package de.ugoe.cs.swe.trex.core;

import java.lang.reflect.Field;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;

public class TTCN3CorePlugin extends Plugin {

	public static final String PLUGIN_ID = "de.ugoe.cs.swe.trex.core";

	public TTCN3CorePlugin() {
		super();
	}

	public static void debugMessage(String message) {
		String traceValue = Platform.getDebugOption(TTCN3CorePlugin.PLUGIN_ID + "/debug");
		if ((traceValue != null) && (traceValue.equalsIgnoreCase("true")))
			System.out.println(message);
	}

	public static void debugMessageTimer(String message) {
		String traceValue = Platform.getDebugOption(TTCN3CorePlugin.PLUGIN_ID + "/timer");
		if ((traceValue != null) && (traceValue.equalsIgnoreCase("true")))
			System.out.println(message);
	}

	public static void debugMessageSymbolResolver(String message) {
		String traceValue = Platform.getDebugOption(TTCN3CorePlugin.PLUGIN_ID + "/symbolresolver");
		if ((traceValue != null) && (traceValue.equalsIgnoreCase("true")))
			System.out.println(message);
	}

	public static boolean isCPUProfiling() {
		String traceValue = Platform.getDebugOption(TTCN3CorePlugin.PLUGIN_ID + "/cpuprofiling");
		if (isProfilingAgent() && (traceValue != null) && (traceValue.equalsIgnoreCase("true")))
			return true;
		else
			return false;
	}

	public static boolean isMemProfiling() {
		String traceValue = Platform.getDebugOption(TTCN3CorePlugin.PLUGIN_ID + "/memprofiling");
		if (isProfilingAgent() && (traceValue != null) && (traceValue.equalsIgnoreCase("true")))
			return true;
		else
			return false;
	}

	public static boolean isCheckAST() {
		String traceValue = Platform.getDebugOption(TTCN3CorePlugin.PLUGIN_ID + "/checkast");
		if ((traceValue != null) && (traceValue.equalsIgnoreCase("true")))
			return true;
		else
			return false;
	}


	public static boolean isProfilingAgent() {
		int port = -1;

		try {
			ClassLoader loader = ClassLoader.getSystemClassLoader();
			Class<?> aClass = loader.loadClass("com.yourkit.runtime.Core");
			Field field = aClass.getDeclaredField("ourPort");
			Integer i = (Integer) field.get(null);
			port = i.intValue();
		} catch (Throwable e) {
		}
		if (port == -1)
			return false;

		return true;
	}

	public static void debugMessageSymbolResolverTimer(String message) {
		String traceValue = Platform.getDebugOption(TTCN3CorePlugin.PLUGIN_ID + "/timer");
		String traceValue2 = Platform.getDebugOption(TTCN3CorePlugin.PLUGIN_ID + "/symbolresolver");
		if ((traceValue != null) && (traceValue.equalsIgnoreCase("true")) && traceValue2 != null && traceValue2.equalsIgnoreCase("true"))
			System.out.println(message);
	}

}
