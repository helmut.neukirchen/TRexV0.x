package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;

public class FormalParameterSymbol extends TypeSymbol {
	public enum ParameterEnum {
		value, timer, template, port
	}

	private ParameterEnum parameterType = ParameterEnum.value;

	private String portTypeIdentifier;

	public String getPortTypeIdentifier() {
		return portTypeIdentifier;
	}

	public void setPortTypeIdentifier(String portTypeIdentifier) {
		this.portTypeIdentifier = portTypeIdentifier;
	}

	public ParameterEnum getParameterType() {
		return parameterType;
	}

	public void setParameterType(ParameterEnum parameterType) {
		this.parameterType = parameterType;
	}

	@Override
	public String getSymbolDescription() {
		StringBuffer result = new StringBuffer();
		HTMLPrinter.getInstance().addPageProlog(result);

		if ((parameterType == ParameterEnum.value) || (parameterType == ParameterEnum.template)) {
			if (parameterType == ParameterEnum.value)
				HTMLPrinter.getInstance().addSmallHeader(result, "Formal Value Parameter Name: ");
			else
				HTMLPrinter.getInstance().addSmallHeader(result, "Formal Template Parameter Name: ");
			
			HTMLPrinter.getInstance().addLine(result, getName());
			result.append(getFormattedTypeString());
		} else if (parameterType == ParameterEnum.timer) {
			HTMLPrinter.getInstance().addSmallHeader(result, "Formal Timer Parameter Name: ");
			HTMLPrinter.getInstance().addLine(result, getName());
		} else if (parameterType == ParameterEnum.port) {
			HTMLPrinter.getInstance().addSmallHeader(result, "Formal Port Parameter Name: ");
			HTMLPrinter.getInstance().addLine(result, getPortTypeIdentifier() + " " + getName());
		}
			
		if (getSurroundingScope().getScopeSymbol() != null) {
			String plainDescription = getSurroundingScope().getScopeSymbol()
					.getSymbolDescriptionPlain();
			if ((plainDescription != null) && (plainDescription.length() > 0)) {
				HTMLPrinter.getInstance().addSmallHeader(result, "In Scope of:");
				HTMLPrinter.getInstance().addLine(result, plainDescription);
			}
		}

		HTMLPrinter.getInstance().addPageEpilog(result);

		return result.toString();
	}

	@Override
	public String getSymbolDescriptionContentAssist() {
		String result = getName() + " - ";
		
		if (getPredefinedType() != null) {
			result += getPredefinedTypeAsString();
		} else if (getReferencedType() != null) {
			if (getReferencedType().getModule() != null)
				result += getReferencedType().getModule() + ".";
			result += getReferencedType().getIdentifier();
		} else if (getStructuredType() != null) {
			result += getStructuredTypeAsString();
		}
		
		result += " (formal parameter)";
		
		
		return result;
	}

	
}
