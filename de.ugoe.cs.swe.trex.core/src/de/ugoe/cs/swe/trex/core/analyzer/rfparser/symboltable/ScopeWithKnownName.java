package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

public class ScopeWithKnownName {
	private Scope searchScope;

	private Scope targetScope;

	private String scopeName;

	private boolean systemScope = false;
	
	public ScopeWithKnownName(Scope searchScope, Scope targetScope, String scopeName, boolean systemScope) {
		this.searchScope = searchScope;
		this.targetScope = targetScope;
		this.scopeName = scopeName;
		this.systemScope = systemScope;
	}

	public Scope getSearchScope() {
		return searchScope;
	}

	public void setSearchScope(Scope scope) {
		this.searchScope = scope;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public Scope getTargetScope() {
		return targetScope;
	}

	public void setTargetScope(Scope targetScope) {
		this.targetScope = targetScope;
	}

	public boolean isSystemScope() {
		return systemScope;
	}

	public void setSystemScope(boolean systemScope) {
		this.systemScope = systemScope;
	}

	
	
}
