package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import java.util.LinkedList;
import java.util.List;

public class Reference {
	Symbol module = null;

	Symbol identifier = null;

	private boolean systemReference = false;
	
	List<Symbol> extendedReferences = new LinkedList<Symbol>();

	public List<Symbol> getExtendedReferences() {
		return extendedReferences;
	}

	public void setExtendedReferences(List<Symbol> extendedReferences) {
		this.extendedReferences = extendedReferences;
	}

	public Symbol getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Symbol identifier) {
		this.identifier = identifier;
	}

	public Symbol getModule() {
		return module;
	}

	public void setModule(Symbol module) {
		this.module = module;
	}

	public boolean isSystemReference() {
		return systemReference;
	}

	public void setSystemReference(boolean systemReference) {
		this.systemReference = systemReference;
	}
	
	public String toString() {
		return "module: " + getModule() + " - identifier: " + getIdentifier();
	}
		
}
