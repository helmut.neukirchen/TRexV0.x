package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;

public abstract class AbstractImport {

	private String moduleName;

	public abstract boolean isAllowed(Symbol s, AbstractKindImport.KindEnum kind);

	public enum VisibilityMode {visPublic, visPrivate, visFriend};
	
	private VisibilityMode visibility =  VisibilityMode.visPublic; 
	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	
	public void setPrivate(){
		this.visibility = VisibilityMode.visPrivate;
	}

	public void setPublic(){
		this.visibility = VisibilityMode.visPublic;
	}

	public void setFriend(){
		this.visibility = VisibilityMode.visFriend;
	}
	
	public void setVisibility(VisibilityMode visibility) {
		this.visibility = visibility;
	}

	public VisibilityMode getVisibility() {
		return visibility;
	}

	public boolean isPublic() {
		boolean isPublic = false;
		if (visibility.equals(VisibilityMode.visPublic))
			isPublic = true;
		return isPublic;
	}

	public boolean isPrivate() {
		boolean isPrivate = false;
		if (visibility.equals(VisibilityMode.visPrivate))
			isPrivate = true;
		return isPrivate;
	}

	public boolean isFriend() {
		boolean isFriend = false;
		if (visibility.equals(VisibilityMode.visFriend))
			isFriend = true;
		return isFriend;
	}
}
