package de.ugoe.cs.swe.trex.core.formatter;


public class TokenStreamMismatchException extends Exception {
	private TTCN3TokenStream source;
	private TTCN3TokenStream target;
	private String message;
	
	private static final long serialVersionUID = 927603938241118914L;

	public TokenStreamMismatchException(String message, TTCN3TokenStream source, TTCN3TokenStream target) {
		this.message = message;
		this.source = source;
		this.target = target;
	}

	public TTCN3TokenStream getSource() {
		return source;
	}

	public void setSource(TTCN3TokenStream source) {
		this.source = source;
	}

	public TTCN3TokenStream getTarget() {
		return target;
	}

	public void setTarget(TTCN3TokenStream target) {
		this.target = target;
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
}
