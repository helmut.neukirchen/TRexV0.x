package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import java.util.Stack;

public class ScopeStack {
	private Stack<Scope> scopeStack;
	
	public ScopeStack() {
		scopeStack = new Stack<Scope>();
	}

	public Scope push(Scope s) {
		if (!scopeStack.empty())
			s.setParent(scopeStack.peek()); // spaghetti stack
		scopeStack.push(s);
		return s;
	}
	
	public Scope pop() {
		return scopeStack.pop();
	}
	
	public boolean empty() {
		return scopeStack.empty();
	}
	
	public Scope peek() {
		return scopeStack.peek();
	}
	
}
