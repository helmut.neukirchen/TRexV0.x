package de.ugoe.cs.swe.trex.core.formatter;

import antlr.CommonToken;
import antlr.Token;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;

public class TTCN3TokenFilter implements ITokenFilter {

	public Token filterToken(Token t) {
		Token tok;
		switch (t.getType()) {
		case TTCN3LexerTokenTypes.BSTRING:
			tok = cloneToken(t);
			tok.setText("'" + t.getText() + "'B");
			return tok;
		case TTCN3LexerTokenTypes.HSTRING:
			tok = cloneToken(t);
			tok.setText("'" + t.getText() + "'H");
			return tok;
		case TTCN3LexerTokenTypes.OSTRING:
			tok = cloneToken(t);
			tok.setText("'" + t.getText() + "'O");
			return tok;
		case TTCN3LexerTokenTypes.BSTRINGMATCH:
			tok = cloneToken(t);
			tok.setText("'" + t.getText() + "'B");
			return tok;
		case TTCN3LexerTokenTypes.HSTRINGMATCH:
			tok = cloneToken(t);
			tok.setText("'" + t.getText() + "'H");
			return tok;
		case TTCN3LexerTokenTypes.OSTRINGMATCH:
			tok = cloneToken(t);
			tok.setText("'" + t.getText() + "'O");
			return tok;
		case TTCN3LexerTokenTypes.CSTRING:
			tok = cloneToken(t);
			tok.setText("\"" + t.getText() + "\"");
			return tok;
		}

		return t;
	}

	private Token cloneToken(Token t) {
		Token tok = new CommonToken(t.getType(), t.getText());
		return tok;
	}
	
}
