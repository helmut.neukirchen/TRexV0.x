package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration;

import java.util.LinkedList;
import java.util.List;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;


public class KindAllExceptImport extends AbstractKindImport {
	private List<String> names = new LinkedList<String>();

	public enum ModesEnum {
		all, allexcept, except
	}

	private ModesEnum mode = ModesEnum.allexcept;

	public void addName(String s) {
		names.add(s);
	}

	public boolean hasName(String s) {
		return names.contains(s);
	}

	@Override
	public boolean isAllowed(Symbol s, KindEnum kind) { // should not be used!!!
		return true;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();

		result.append("KindAllExceptImport(" + names.size() + "): ");
		for (int i = 0; i < names.size(); i++) {
			result.append(names.get(i)).append(' ');
		}

		return result.toString();
	}

	public List<String> getNames() {
		return names;
	}

	public void setNames(List<String> names) {
		this.names = names;
	}

	public ModesEnum getMode() {
		return mode;
	}

	public void setMode(ModesEnum mode) {
		this.mode = mode;
	}

}
