package de.ugoe.cs.swe.trex.core.formatter;

public class TTCN3FormatterParameters {
	boolean tabs = false;

	boolean unixNewline = false;

	boolean spacesBetweenAssignment = true;
	
	boolean spaceAfterComma = true;

	boolean krStyle = true;

	boolean newlineBeforeRunsOn = true;
	
	boolean newlineBeforeSystem = true;

	boolean newlineBeforeReturn = true;
	
	boolean newlineBeforeExceptionSpec = true;
	
	boolean newlineBeforeFormalPar = true;
	
	boolean newlineAfterFormalParList = true;
	
	boolean spacesAroundParentheses = true;
	
	int spacesCount = 2;

	int linesBetweenModules = 2;

	int linesAfterControlPart = 1;
		
	int linesAfterModuleDefinition = 1;

	private int linesBetweenImportDefinitions = 1;

	
	public boolean isTabs() {
		return tabs;
	}

	public void setTabs(boolean tabs) {
		this.tabs = tabs;
	}

	public int getSpacesCount() {
		return spacesCount;
	}

	public void setSpacesCount(int spacesCount) {
		this.spacesCount = spacesCount;
	}

	public int getLinesBetweenModules() {
		return linesBetweenModules;
	}

	public void setLinesBetweenModules(int linesBetweenModules) {
		this.linesBetweenModules = linesBetweenModules;
	}

	public boolean isUnixNewline() {
		return unixNewline;
	}

	public void setUnixNewline(boolean unixNewline) {
		this.unixNewline = unixNewline;
	}

	public int getLinesAfterControlPart() {
		return linesAfterControlPart;
	}

	public void setLinesAfterControlPart(int linesAfterControlPart) {
		this.linesAfterControlPart = linesAfterControlPart;
	}

	public boolean isSpacesBetweenAssignment() {
		return spacesBetweenAssignment;
	}

	public void setSpacesBetweenAssignment(boolean spacesBetweenAssignment) {
		this.spacesBetweenAssignment = spacesBetweenAssignment;
	}

	public boolean isSpaceAfterComma() {
		return spaceAfterComma;
	}

	public void setSpaceAfterComma(boolean spaceAfterComma) {
		this.spaceAfterComma = spaceAfterComma;
	}

	public int getLinesAfterModuleDefinition() {
		return linesAfterModuleDefinition;
	}

	public void setLinesAfterModuleDefinition(int linesAfterModuleDefinition) {
		this.linesAfterModuleDefinition = linesAfterModuleDefinition;
	}

	public boolean isKRstyle() {
		return krStyle;
	}

	public void setKRstyle(boolean rstyle) {
		krStyle = rstyle;
	}

	public void setNewlineBeforeRunsOn(boolean newlineBeforeRunsOn) {
		this.newlineBeforeRunsOn = newlineBeforeRunsOn;
	}

	public boolean isNewlineBeforeRunsOn() {
		return newlineBeforeRunsOn;
	}

	public void setNewlineBeforeReturn(boolean newlineBeforeReturn) {
		this.newlineBeforeReturn = newlineBeforeReturn;
	}

	public boolean isNewlineBeforeReturn() {
		return newlineBeforeReturn;
	}

	public void setNewlineBeforeFormalPar(boolean newlineBeforeFormalPar) {
		this.newlineBeforeFormalPar = newlineBeforeFormalPar;
	}

	public boolean isNewlineBeforeFormalPar() {
		return newlineBeforeFormalPar;
	}

	public void setNewlineBeforeExceptionSpec(boolean newlineBeforeExceptionSpec) {
		this.newlineBeforeExceptionSpec = newlineBeforeExceptionSpec;
	}

	public boolean isNewlineBeforeExceptionSpec() {
		return newlineBeforeExceptionSpec;
	}

	public void setNewlineBeforeSystem(boolean newlineBeforeSystem) {
		this.newlineBeforeSystem = newlineBeforeSystem;
	}

	public boolean isNewlineBeforeSystem() {
		return newlineBeforeSystem;
	}

	public void setNewlineAfterFormalParList(boolean newlineAfterFormalParList) {
		this.newlineAfterFormalParList = newlineAfterFormalParList;
	}

	public boolean isNewlineAfterFormalParList() {
		return newlineAfterFormalParList;
	}

	public void setSpacesAroundParentheses(boolean spacesAroundParentheses) {
		this.spacesAroundParentheses = spacesAroundParentheses;
	}

	public boolean isSpacesAroundParentheses() {
		return spacesAroundParentheses;
	}

	public void setLinesBetweenImportDefinitions(int linesBetweenImportDefinitions) {
		this.linesBetweenImportDefinitions = linesBetweenImportDefinitions;
	}

	public int getLinesBetweenImportDefinitions() {
		return linesBetweenImportDefinitions;
	}




		
}
