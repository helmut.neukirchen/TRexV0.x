package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;

public class VariableInstanceSymbol extends TypeSymbol {
	@Override
	public String getSymbolDescription() {
		StringBuffer result = new StringBuffer();
		HTMLPrinter.getInstance().addPageProlog(result);
		HTMLPrinter.getInstance().addSmallHeader(result, "Variable Instance: ");
		HTMLPrinter.getInstance().addLine(result, getName());
		
		result.append(getFormattedTypeString());
		
		if (getSurroundingScope().getScopeSymbol() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Scope of:");
			result.append(getSurroundingScope().getScopeSymbol().getSymbolDescriptionPlain());
		}
		
		return result.toString();
		
	}

	@Override
	public String getSymbolDescriptionContentAssist() {
		if (getPredefinedType() != null) {
			return getName() + " - var " + getPredefinedTypeAsString();
		}
		
		String result = getName() + " - var ";
		if (getReferencedType() != null) {
			if (getReferencedType().getModule() != null)
				result += getReferencedType().getModule() + ".";
			if (getReferencedType().getIdentifier() != null)
				result += getReferencedType().getIdentifier().getName();
		}
		return result;
	}

	@Override
	public String getSymbolDescriptionPlain() {
		return "variable " + getName();
	}
	
	
}
