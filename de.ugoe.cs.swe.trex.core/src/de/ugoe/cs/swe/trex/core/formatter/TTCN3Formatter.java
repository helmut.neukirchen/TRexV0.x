package de.ugoe.cs.swe.trex.core.formatter;

import java.io.StringReader;

import antlr.RecognitionException;
import antlr.Token;
import antlr.TokenStreamException;
import de.ugoe.cs.swe.trex.core.TTCN3CorePlugin;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ASTFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3FormatterTreeParser;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Lexer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Parser;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TokenStreamTracker;

public class TTCN3Formatter {

	protected TTCN3Parser parser;

	private TokenStreamTracker tracker;

	// ------------------------------------------------------------------------------------------------------

	protected void configureTokenStreamTracker(TokenStreamTracker t) {
		t.discard(TTCN3LexerTokenTypes.ML_COMMENT);
		t.discard(TTCN3LexerTokenTypes.SL_COMMENT);
		t.discard(TTCN3LexerTokenTypes.WS);
	}

	// ------------------------------------------------------------------------------------------------------

	public static void displayTokenStream(TTCN3Lexer l) throws TokenStreamException {
		Token t;

		t=l.nextToken();
		while (t.getType() != Token.EOF_TYPE) {
			String type = TTCN3Parser._tokenNames[t.getType()];
//			System.out.println(type + ": '" + t.getText() + "' " + t.toString());
			TTCN3CorePlugin.debugMessage(type + ":\t\t\t" + t.toString() + "\n");
			t = l.nextToken();
		}
		l.rewind(0);
	}

	// ------------------------------------------------------------------------------------------------------

	protected TokenStreamTracker parseTTCN3File(String text)
			throws RecognitionException, TokenStreamException {
		TTCN3Lexer lexer = new TTCN3Lexer(new StringReader(text));

		lexer.setTokenObjectClass(de.ugoe.cs.swe.trex.core.analyzer.rfparser.TokenWithIndex.class);
		TokenStreamTracker track = new TokenStreamTracker(lexer);
		configureTokenStreamTracker(track);

		parser = new TTCN3Parser(track);
		parser.setASTFactory(new TTCN3ASTFactory());
		parser.getASTFactory().setASTNodeClass(LocationAST.class);

		parser.pr_TTCN3File();

		if (parser.getExceptions().size() > 0)
			throw parser.getExceptions().get(0);

		return track;
	}

	// ------------------------------------------------------------------------------------------------------

	public void printTokenStream(TokenStreamTracker t) {
		TTCN3CorePlugin.debugMessage(t.toDebugString());
	}

	// ------------------------------------------------------------------------------------------------------

	public String formatTTCN3Source(String text, TTCN3FormatterParameters p)
			throws RecognitionException, TokenStreamException, Exception {

		tracker = parseTTCN3File(text);
		if (tracker.getTokenCount()==0){
			//handle empty files - return source
			//this may have to be moved up or down
			return text;
		}
		String formattedCode = prettyPrintWalker(p);

		TokenStreamTracker formattedTracker = parseTTCN3File(formattedCode);

		TTCN3TokenStreamWeaver streamWeaver = new TTCN3TokenStreamWeaver(tracker, formattedTracker,
				new TTCN3FormatterTools(p));
		streamWeaver.getSource().setFilter(new TTCN3TokenFilter());
		streamWeaver.getTarget().setFilter(new TTCN3TokenFilter());

		formattedCode = streamWeaver.weave();
		return formattedCode;
	}

	// ------------------------------------------------------------------------------------------------------

	public String formatTTCN3SourceStripComments(String text, TTCN3FormatterParameters p)
			throws RecognitionException, TokenStreamException {

		tracker = parseTTCN3File(text);
		printTokenStream(tracker);
		//displayTree();
		String formattedCode = prettyPrintWalker(p);

		return formattedCode;
	}

	// ------------------------------------------------------------------------------------------------------

	public String prettyPrintWalker(TTCN3FormatterParameters p) {
		TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
		formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
		formatterWalker.setTokenStreamTracker(tracker);
		formatterWalker.setFormatterParameters(p);
		LocationAST t = (LocationAST) parser.getAST();
		String formattedCode = null;
		try {
			formattedCode = formatterWalker.pr_TTCN3File(t).toString();
		} catch (RecognitionException e) {
			e.printStackTrace();
		}

		return formattedCode;
	}

}
