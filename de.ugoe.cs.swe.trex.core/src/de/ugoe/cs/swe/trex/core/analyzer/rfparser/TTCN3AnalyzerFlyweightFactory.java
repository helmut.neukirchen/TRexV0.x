package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;

import antlr.MismatchedTokenException;
import antlr.RecognitionException;
import antlr.TokenStreamException;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.ModuleSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.PredefinedFunctions;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;


public class TTCN3AnalyzerFlyweightFactory {
	private Map<String, TTCN3Analyzer> analyzerMap = new HashMap<String, TTCN3Analyzer>();

	private Map<String, TTCN3Analyzer> moduleAnalyzerMap = new HashMap<String, TTCN3Analyzer>();

	private Map<String, List<String>> filenameModuleMap = new HashMap<String, List<String>>();

	private Map<String, List<Scope>> moduleScopeMap = new HashMap<String, List<Scope>>();

	private static TTCN3AnalyzerFlyweightFactory factory = null;

	private IProject lastProjectAnalyzed = null;

	private ArrayList<IProject> analyzedProjects = new ArrayList<IProject>();

	private boolean standaloneUsage = false;

	// --------------------------------------------------------------

	public static TTCN3AnalyzerFlyweightFactory getInstance() {
		if (TTCN3AnalyzerFlyweightFactory.factory == null)
			TTCN3AnalyzerFlyweightFactory.factory = new TTCN3AnalyzerFlyweightFactory();

		return TTCN3AnalyzerFlyweightFactory.factory;
	}

	// --------------------------------------------------------------

	public static void destroy() {
		TTCN3AnalyzerFlyweightFactory.factory = null;
	}


	// --------------------------------------------------------------

//	private String dumpBytes(byte[] bs) {
//        StringBuffer ret = new StringBuffer(bs.length);
//        for (int i = 0; i < bs.length; i++) {
//            String hex = Integer.toHexString(0x0100 + (bs[i] & 0x00FF)).substring(1);
//            ret.append((hex.length() < 2 ? "0" : "") + hex);
//        }
//        return ret.toString();
//    }

	// --------------------------------------------------------------

	public TTCN3Analyzer getTTCN3Analyzer(String filename) {
		return analyzerMap.get(filename);
	}

	// --------------------------------------------------------------

	public void removeTTCN3Analyzer(String filename) {
		analyzerMap.remove(filename);
	}

	// --------------------------------------------------------------

	public TTCN3Analyzer getTTCN3Analyzer(String filename, String code) {
		TTCN3Analyzer analyzer = analyzerMap.get(filename);
		if (analyzer == null) {
			analyzer = newTTCN3Analyzer(filename, code);
		} else {
			analyzer.setCode(code);
		}

		return analyzer;
	}

	// --------------------------------------------------------------

	public TTCN3Analyzer getTTCN3Analyzer(IFile file) {
		return getTTCN3Analyzer(file, false);
	}

	// --------------------------------------------------------------

	public TTCN3Analyzer getTTCN3Analyzer(IFile file, boolean updateCode) {
		final String path = file.getFullPath().toPortableString();
		TTCN3Analyzer analyzer = analyzerMap.get(path);
		if ((updateCode == true) || (analyzer == null) ) {
			String code;
			try {
				code = loadFile(file);
				analyzer = newTTCN3Analyzer(path, code);
			} catch (Exception e) {
				return null;
			}
			if (updateCode)
				updateMaps(path, analyzer);
		}

		return analyzer;
	}

	// --------------------------------------------------------------

	public void removeTTCN3Analyzer(IFile file) {
		final String path = file.getFullPath().toPortableString();
		analyzerMap.remove(path);
	}

	// --------------------------------------------------------------

	public boolean hasAnalyzer(IFile file) {
		final String path = file.getFullPath().toPortableString();
		TTCN3Analyzer analyzer = analyzerMap.get(path);
		if (analyzer != null)
			return true;

		return false;
	}

	// --------------------------------------------------------------

	private void updateMaps(final String path, TTCN3Analyzer analyzer) {
		final List<String> modules = analyzer.getModules();
		if (modules != null) {
			for (String module : modules) {
				moduleAnalyzerMap.put(module, analyzer);
			}
			filenameModuleMap.put(path, modules);
		}
	}

	// --------------------------------------------------------------

	private TTCN3Analyzer newTTCN3Analyzer(String path, String code) {
		final TTCN3Analyzer analyzer;
		if (standaloneUsage)
			analyzer = new TTCN3Analyzer(path, code, true);
		else
			analyzer = new TTCN3Analyzer(path, code, false);
		analyzerMap.put(path, analyzer);
		updateMaps(path, analyzer);

		return analyzer;
	}

	// --------------------------------------------------------------

//	private Document convertLineDelimiters(Document doc) throws BadLocationException {
//		MultiTextEdit edit = new MultiTextEdit();
//		for (int i = 0; i < doc.getNumberOfLines(); i++) {
//			String delimiter = doc.getLineDelimiter(i);
//			if (delimiter != null && delimiter.length() > 0 /*&& !"\n".equals(delimiter)*/) {
//				IRegion r = doc.getLineInformation(i);
//				edit.addChild(new ReplaceEdit(r.getOffset() + r.getLength(), delimiter.length(), "\n"));
//			}
//		}
//		edit.apply(doc);
//		return doc;
//	}

	// --------------------------------------------------------------

	private List<String> getModulesForFilename(String filename) {
		return filenameModuleMap.get(filename);
	}

	// --------------------------------------------------------------

	public List<String> getModulesForFiles(Collection<String> files) {
		List<String> result = new ArrayList<String>();

		for(String filename : files) {
			final List<String> modules = getModulesForFilename(filename);
			if(modules != null)
				result.addAll(modules);
		}

		return result;
	}

	// --------------------------------------------------------------

	public TTCN3Analyzer getTTCN3AnalyzerByModuleName(String moduleName) {
		TTCN3Analyzer analyzer = moduleAnalyzerMap.get(moduleName);

		return analyzer;
	}

	// --------------------------------------------------------------

	public Scope getScopeByModuleName(String moduleName, String associatedFilename) {
		List<Scope> list = moduleScopeMap.get(moduleName);
		if (list == null)
			return null;

		String folderName;
		if (standaloneUsage || (associatedFilename.indexOf("/", 1) < 0)) {
			return list.get(0); // always return the first element. it is perfectly sufficient.
		} else {
			folderName = associatedFilename.substring(0, associatedFilename.indexOf("/", 1));
			for (Scope scope : list) {
				if (scope.getAssociatedFilename().startsWith(folderName)) {
					return scope;
				}
			}
		}

		return null;
	}

	// --------------------------------------------------------------

	// TODO: remove unused code?
//	private List<Scope> getScopeByModuleName(String moduleName) {
//		return moduleScopeMap.get(moduleName);
//	}

	// --------------------------------------------------------------

	private String loadFile(IFile file) throws CoreException, IOException {
		StringBuffer result = new StringBuffer();
		InputStream istream = null;
		try {
			istream = file.getContents();
			BufferedReader reader = new BufferedReader(new InputStreamReader(istream));
			int mychar;

			while ((mychar = reader.read()) != -1) {
				result.append((char) mychar);
			}
		} finally {
			if(istream != null)
				istream.close();
		}

		return result.toString();
	}

	// --------------------------------------------------------------

	// TODO: remove unused code?
//	private Map<String, TTCN3Analyzer> getAnalyzerMap() {
//		return analyzerMap;
//	}

	// --------------------------------------------------------------

	public Map<String, TTCN3Analyzer> getModuleAnalyzerMap() {
		return moduleAnalyzerMap;
	}

	// --------------------------------------------------------------

	// TODO: remove unused code?
//	private void setModuleAnalyzerMap(HashMap<String, TTCN3Analyzer> moduleAnalyzerMap) {
//		this.moduleAnalyzerMap = moduleAnalyzerMap;
//	}

	// --------------------------------------------------------------

	public void postProcess() {
		TTCN3Analyzer analyzer = getTTCN3Analyzer("trexPredefinedFunctions.ttcn3", PredefinedFunctions.PREDEFINED_FUNCTIONS);
		try {
			analyzer.analyze();
			analyzer.postProcess();
		} catch (MismatchedTokenException e) {
			e.printStackTrace();
		} catch (RecognitionException e) {
			e.printStackTrace();
		} catch (TokenStreamException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		moduleScopeMap.clear();
		moduleAnalyzerMap.clear();
		filenameModuleMap.clear();

		for (String str : analyzerMap.keySet()) {
			TTCN3Analyzer a = analyzerMap.get(str);
			final Map<String, Scope> aModScopeMap = a.getModuleScopeMap();
			if (aModScopeMap != null) {
				for (Map.Entry<String, Scope> entry : aModScopeMap.entrySet()) {
					String moduleScopeName = entry.getKey();
					List<Scope> list = moduleScopeMap.get(moduleScopeName);
					if (list == null)
						list = new ArrayList<Scope>();
					list.add(entry.getValue());
					moduleScopeMap.put(moduleScopeName, list);
				}
			}
			final List<String> modules = a.getModules();
			if (modules != null) {
				for(String module : modules) {
					moduleAnalyzerMap.put(module, a);
				}
				filenameModuleMap.put(a.getFilename(), modules);
			}
		}

		for (String str : analyzerMap.keySet()) {
			TTCN3Analyzer a = analyzerMap.get(str);
			a.postProcess();
		}
	}

	// --------------------------------------------------------------

	public List<Scope> getModuleScopesWithImport(String moduleName, Symbol s, String associatedFilename) {
		List<Scope> result = new ArrayList<Scope>();

		for (String moduleScopeKey : moduleScopeMap.keySet()) {
			List<Scope> scopeList = moduleScopeMap.get(moduleScopeKey);

			String folderName = "";
			int x = associatedFilename.indexOf("/", 1);
			if (x > 0){
				folderName = associatedFilename.substring(0, x);
			}

			Scope value = null;
			for (Scope item : scopeList) {
				if (item != null && item.getAssociatedFilename().startsWith(folderName)) {
					value = item;

					if ((value.getScopeSymbol() instanceof ModuleSymbol) && (value.getScopeSymbol().getName().equals(moduleName)))
						continue;

					if (value.getImportConfiguration().importsFromModule(moduleName)) {
						if (value.resolve(s.getName()) != null)
							result.add(value);
					}
				}
			}
		}

		return result;
	}

	// --------------------------------------------------------------

	public IProject getLastProjectAnalyzed() {
		return lastProjectAnalyzed;
	}

	// --------------------------------------------------------------

	public void setLastProjectAnalyzed(IProject lastProjectAnalyzed) {
		this.lastProjectAnalyzed = lastProjectAnalyzed;
	}

	// --------------------------------------------------------------

	public ArrayList<IProject> getAnalyzedProjects() {
		return analyzedProjects;
	}

	// --------------------------------------------------------------

	public void setAnalyzedProjects(ArrayList<IProject> analyzedProjects) {
		this.analyzedProjects = analyzedProjects;
	}

	// --------------------------------------------------------------

	public void addAnalyzedProject(IProject p) {
		this.analyzedProjects.add(p);
	}

	// --------------------------------------------------------------

	public boolean hasAnalyzedProject(IProject p) {
		return this.analyzedProjects.contains(p);
	}

	// --------------------------------------------------------------

	public boolean isStandaloneUsage() {
		return standaloneUsage;
	}

	// --------------------------------------------------------------

	public void setStandaloneUsage(boolean standaloneUsage) {
		this.standaloneUsage = standaloneUsage;
	}

}
