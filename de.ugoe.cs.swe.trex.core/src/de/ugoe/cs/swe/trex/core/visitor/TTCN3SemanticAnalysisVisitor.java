package de.ugoe.cs.swe.trex.core.visitor;

import java.util.ArrayList; 

import antlr.RecognitionException;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3SemanticException;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;

public class TTCN3SemanticAnalysisVisitor extends AbstractVisitor {

	private TTCN3Analyzer analyzer;
	private	String filename = null;
	private ArrayList<TTCN3SemanticException> exceptions = null;
	private ArrayList<RecognitionException> recognitionExceptions = new ArrayList<RecognitionException>();

	public TTCN3SemanticAnalysisVisitor(String filename) {
		exceptions = new ArrayList<TTCN3SemanticException>();
		this.filename = filename;
	}
	
	public TTCN3Analyzer getAnalyzer() {
		return analyzer;
	}

	public void setAnalyzer(TTCN3Analyzer a) {
		analyzer = a;
	}

	public ArrayList<TTCN3SemanticException> getExceptions() {
		return exceptions;
	}

	public void reportError(RecognitionException ex) {
	   	recognitionExceptions.add(ex);
	}
		
	public ArrayList<RecognitionException> getRecognitionExceptions() {
		return recognitionExceptions;
	}	

	
	@Override
	public void init() {
	}

	@Override
	public void finish() {
	}
	
    public ContinueStatus visitIDENTIFIER(LocationAST node) throws TTCN3BehaviorException {//return ContinueStatus.getInstance(true,true);}
//    public ContinueStatus visitIdentifier(LocationAST node) throws TTCN3BehaviorException {
    	StringBuffer s = new StringBuffer();
    	s.append(node.getText());
		if (LocationAST.resolveParentsUntilType(node, TTCN3ParserTokenTypes.ImportDef) == null) {
	    	Scope scope = node.getScope();
	    	
	    	if (scope != null) {
				Symbol symb = scope.resolve(node);
				if (symb == null) {		
					exceptions.add(
							new TTCN3SemanticException(
									node.getText() + " is not defined" , 
									filename, 
									node.getLine(), node.getOffset(),
									node.getOffset()+node.getText().length(),
									node.getColumn(),
									TTCN3SemanticException.warningEnum.WARNING)
							);
				}
	    	} else {
	    		//TODO: add proper handling
	    		exceptions.add(new TTCN3SemanticException(node.getText() + " has an invalid scope (needs further investigation)", filename, node.getLine(), node.getColumn(), TTCN3SemanticException.warningEnum.ERROR));
	    	}
		}
    	return ContinueStatus.getInstance(true,true);
    }
}
