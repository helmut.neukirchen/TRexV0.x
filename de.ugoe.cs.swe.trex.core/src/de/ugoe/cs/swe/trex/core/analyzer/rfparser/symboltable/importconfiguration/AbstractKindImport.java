package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration;

public abstract class AbstractKindImport extends AbstractImport {

	public enum KindEnum {
		ttcn3_group, ttcn3_type, ttcn3_template, ttcn3_const, ttcn3_testcase, ttcn3_altstep, ttcn3_function, ttcn3_signature, ttcn3_modulepar, ttcn3_import
	}

	private KindEnum type;

	public KindEnum getType() {
		return type;
	}

	public void setType(KindEnum type) {
		this.type = type;
	}
	
}
