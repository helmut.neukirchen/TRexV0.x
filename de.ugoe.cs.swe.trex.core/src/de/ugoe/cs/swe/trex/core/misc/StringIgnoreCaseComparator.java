package de.ugoe.cs.swe.trex.core.misc;

import java.io.Serializable;
import java.util.Comparator;

public class StringIgnoreCaseComparator implements Comparator<String>, Serializable {
	private static final long serialVersionUID = 1061162671166966928L;

	public int compare(String arg0, String arg1) {
		String first = arg0.toLowerCase();
		String second = arg1.toLowerCase();
		return first.compareTo(second);
	}

}
