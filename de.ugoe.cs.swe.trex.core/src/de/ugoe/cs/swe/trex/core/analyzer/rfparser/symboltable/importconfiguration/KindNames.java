package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration;

import java.util.LinkedList;
import java.util.List;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;


public class KindNames extends AbstractKindImport {
	private List<String> names = new LinkedList<String>();

	public enum ModeEnum {
		accept, allexcept, unspecified
	}

	private ModeEnum mode = ModeEnum.unspecified;

	public void addName(String s) {
		names.add(s);
	}

	public boolean hasName(String s) {
		return names.contains(s);
	}

	@Override
	public boolean isAllowed(Symbol s, KindEnum kind) {
		if (kind != getType())
			return false;
		
		if (mode == ModeEnum.accept) {
			if (names == null)
				return false;
			
			if (names.contains(s.getName())) {
				return true;
			}
		} else if (mode == ModeEnum.allexcept) {
			if ((names != null) && (names.contains(s.getName()))) {
				return false;
			}
			return true;
		}
		
		return false;
	}
	
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();

		result.append("kindNames: ");
		if (getType() != null)
			result.append("type: " + getType().name() + " - ");
		else
			result.append("type: null - ");
		
		result.append("mode: " + getMode().name() + " - ");
		
		if (names == null)
			return result.toString();
		
		for (int i = 0; i < names.size(); i++) {
			result.append(names.get(i));
			if (i < names.size() - 1)
				result.append(", ");
		}

		return result.toString();
	}

	public ModeEnum getMode() {
		return mode;
	}

	public void setMode(ModeEnum mode) {
		this.mode = mode;
	}

	public List<String> getNames() {
		return names;
	}

	public void setNames(List<String> names) {
		this.names = names;
	}

}
