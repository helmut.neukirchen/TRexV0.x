package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import java.util.Map;
import java.util.TreeMap;
//import java.util.Map.Entry;

//import de.ugoe.cs.swe.trex.core.misc.StringIgnoreCaseComparator;


public class SymbolTable {
//	private Map<String, Symbol> symbolMap = new TreeMap<String, Symbol>(new StringIgnoreCaseComparator());
	private Map<String, Symbol> symbolMap = new TreeMap<String, Symbol>();

	// --------------------------------------------------------------

	public Symbol resolve(String s) {
		if ((symbolMap == null) || (s == null))
			return null;

		return symbolMap.get(s); 
	}
	
	// --------------------------------------------------------------

	public Symbol put(String s, Symbol i) {
		return symbolMap.put(s, i);
	}
	
	// --------------------------------------------------------------

	public void remove(String s) {
		symbolMap.remove(s);
	}
	
	// --------------------------------------------------------------

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer("Symbol Table Contents: \n");
				
		for (String key : symbolMap.keySet()) {
			result.append("  key: " + key + "\n");
		}
		
		return result.toString();
	}

	// --------------------------------------------------------------

	public Map<String, Symbol> getSymbols() {	
//		Map<String, Symbol> symbols = new TreeMap<String, Symbol>(new StringIgnoreCaseComparator());
//		
//		for (Entry<String, Symbol> item : symbolMap.entrySet()) {
//			symbols.put(item.getKey(), item.getValue());
//		}
//		
//		return symbols;
		return symbolMap;
	}
	
}
