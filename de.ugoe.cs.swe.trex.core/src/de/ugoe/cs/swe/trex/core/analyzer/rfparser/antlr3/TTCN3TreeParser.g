tree grammar TTCN3TreeParser;

options {
  tokenVocab = TTCN3Lexer;
  ASTLabelType = CommonTree;
}

@header {
package de.ugoe.cs.swe.trex.core.analyzer.rfparser.antlr3;
}

program
  :
  ^(MODULE IDENTIFIER)
  {
    System.out.println("Module Name: " + $IDENTIFIER.text);
  }
  ;