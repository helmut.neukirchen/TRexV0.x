package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;

public class ModuleParameterSymbol extends SubtypeSymbol {
	@Override
	public String getSymbolDescription() {
		StringBuffer result = new StringBuffer();
		HTMLPrinter.getInstance().addPageProlog(result);
		HTMLPrinter.getInstance().addSmallHeader(result, "Module Parameter Name: ");
		HTMLPrinter.getInstance().addLine(result, getName());
		
		result.append(getFormattedTypeString());
		
		if (getGroupName() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Group:");
			HTMLPrinter.getInstance().addLine(result, getGroupName());
		}


		if (getSurroundingScope().getScopeSymbol() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Scope of:");
			result.append(getSurroundingScope().getScopeSymbol().getSymbolDescriptionPlain());
		}

		HTMLPrinter.getInstance().addPageEpilog(result);

		return result.toString();
	}

	@Override
	public String getSymbolDescriptionPlain() {
		return "module " + getName();
	}

	@Override
	public String getSymbolDescriptionContentAssist() {
		String result = getName() + " - modulepar " ;
		
		if (getReferencedType() != null) {
			if (getReferencedType().getModule() != null)
				result += getReferencedType().getModule() + ".";
			if (getReferencedType().getIdentifier() != null)
				result += getReferencedType().getIdentifier().getName();
		}
		
		return result;
	}

}
