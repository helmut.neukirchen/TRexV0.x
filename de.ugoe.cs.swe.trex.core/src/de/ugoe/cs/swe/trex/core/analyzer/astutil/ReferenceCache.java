package de.ugoe.cs.swe.trex.core.analyzer.astutil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;

public final class ReferenceCache {
	
	private HashMap<Symbol,Map<String,List<ReferenceWithContext>>> symbolMap = null;
	private static ReferenceCache instance;
	
	private ReferenceCache(){
		init();
	}

	private void init() {
		this.symbolMap = new HashMap<Symbol, Map<String,List<ReferenceWithContext>>>();
		this.populateSymbolMap();
	}
	
	public static synchronized ReferenceCache getInstance(){
		if (instance == null){
			instance = new ReferenceCache();
		}
		return instance;
	}

	private void populateSymbolMap(){
//		long startTime;
//		long endTime;
//		long elapsed;
//		double elapsedMinutes;
//		String elapsedMinutesString;
		//TODO: Transfer back to Tools
//		System.out.println("==========================================");
//		System.out.println("Populating reference cache on demand...");
//		startTime = System.currentTimeMillis();

		TTCN3AnalyzerFlyweightFactory analyzerFactory = TTCN3AnalyzerFlyweightFactory
		.getInstance();
		Map<String, TTCN3Analyzer> moduleAnalyzerMap = analyzerFactory.getModuleAnalyzerMap();
		for (TTCN3Analyzer analyzer : moduleAnalyzerMap.values()){
//			System.out.println("Getting cache for: "+analyzer.getFilename());
			this.acceptDFS((LocationAST) analyzer.getParser().getAST());
		}
		//this.findAndMapSymbolListReferences();
		
//		endTime = System.currentTimeMillis();
//		elapsed = endTime - startTime;
//		elapsedMinutes = ((double) elapsed) / 1000.0 / 60.0;
//		elapsedMinutesString = new DecimalFormat("0.00").format(elapsedMinutes);
//		System.out.println("Population finished in " + elapsed + "ms ("
//				+ elapsedMinutesString + " minutes).");
//		System.out.println("==========================================");

	}

//	private void findAndMapSymbolListReferences() {
//		Map<String, List<ReferenceWithContext>> references = this.findReferences(this.symbolList);
//		for (Entry<String, List<ReferenceWithContext>> entry : references.entrySet()){
//			String filename = entry.getKey();
//			for (ReferenceWithContext reference : entry.getValue()){
//				this.mapSymbolReferences(reference.getMatchNode().getSymbol(), filename, entry.getValue());
//				reference = null;
//			}
//		}
//	}

	@SuppressWarnings("unused")
	private void mapSymbolReferences(Symbol symbol,
			String filename,
			List<ReferenceWithContext> referenceList) {
		if (symbol!=null){
			if (!this.symbolMap.containsKey(symbol)){
				this.symbolMap.put(symbol, new HashMap<String,List<ReferenceWithContext>>());
			}
			if (!this.symbolMap.get(symbol).containsKey(filename)){
				this.symbolMap.get(symbol).put(filename, new ArrayList<ReferenceWithContext>());
			}

			this.symbolMap.get(symbol).get(filename).addAll(referenceList);
		}
	}

	private void visit(LocationAST node){
		if (node.getType() == TTCN3ParserTokenTypes.IDENTIFIER){
			this.addSymbol(node);
		}
	}
	
    private void addSymbol(LocationAST node) {
    	Symbol symbol = node.getSymbol();
		if (symbol!=null){
			this.addReference(node);
		}
	}

	private void acceptDFS(LocationAST node){
        //init();
        this.searchASTDepthFirst(node);
        //finish();
    }
	
    private void searchASTDepthFirst(LocationAST node){
        if (node == null)
            return;

        LocationAST next = node;
        while (next != null) {
            this.visit(next);
            this.searchASTDepthFirst(next.getFirstChild());
            next = next.getNextSibling();
        }
    }
    
	public Map<String, List<ReferenceWithContext>> getReferences(Symbol symbol){
		Map<String, List<ReferenceWithContext>> referenceMap = new HashMap<String, List<ReferenceWithContext>>(); 
		if (this.symbolMap.get(symbol)!=null){
			referenceMap.putAll(this.symbolMap.get(symbol));
		}
		return referenceMap;
	}

    //TODO: this is amazingly fast, but not 100% correct
	// one issue is with duplicated definitions - they are resolved as references, not sure whether there may be other causes for this
	// other than that the reference with context may be incoherent 
    //TODO: seek a way to use this as it is much faster, albeit not yet 100% correct
	private void addReference(LocationAST referenceNode){
		Symbol symbol = referenceNode.getSymbol();
		if (symbol!=null){
			if (!this.symbolMap.containsKey(symbol)){
				this.symbolMap.put(symbol, new HashMap<String,List<ReferenceWithContext>>());
			}
			if (referenceNode.getSymbol().getDeclarationNode() != referenceNode){

				String filename = referenceNode.getScope().getAssociatedFilename();
				if (!this.symbolMap.get(symbol).containsKey(filename)){
					this.symbolMap.get(symbol).put(filename, new ArrayList<ReferenceWithContext>());
				}
				this.symbolMap.get(symbol).get(filename).add(createReferenceWithContext(referenceNode));
				//this.symbolMap.get(symbol).get(filename).addAll(referenceList);
			}
		}
//		if (symbol!=null){
////			System.out.println(" derived " +symbol + "; declaration node "+symbol.getDeclarationNode());
//			if (!this.symbolMap.containsKey(symbol)){
////				System.out.println("  =>new");
////				this.symbolMap.put(symbol, new ArrayList<LocationAST>());
//			} else {
////				System.out.println("  =>existing");
//			}
//			if (referenceNode.getSymbol().getDeclarationNode() != referenceNode){
////				System.out.println("  =>reference");
////				this.symbolMap.get(symbol).add(referenceNode);
//			} else {
////				System.out.println("  =>new entry");
//			}
//		}
	}

	//TODO: Adapt for later usage, once further optimized cache is working properly
	private ReferenceWithContext createReferenceWithContext(LocationAST referencedNode){
		ReferenceWithContext ref = new ReferenceWithContext();
		ref.setReferenceNode(referencedNode);
		ref.setMatchNode(referencedNode.getSymbol().getDeclarationNode());
		LocationAST contextNode = LocationAST.resolveParentsUntilType(
				referencedNode, ReferenceFinder.CONTEXT_INTRODUCING_TYPES);
		ref.setContextNode(contextNode);
		return ref;
	}

}
