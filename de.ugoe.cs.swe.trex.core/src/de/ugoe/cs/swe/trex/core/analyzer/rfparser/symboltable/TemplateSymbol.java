package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;

public class TemplateSymbol extends SubtypeSymbol {
	@Override
	public String getSymbolDescription() {
		StringBuffer result = new StringBuffer();
		HTMLPrinter.getInstance().addPageProlog(result);
		result.append(getVisibilityDescription());
		HTMLPrinter.getInstance().addSmallHeader(result, "Template Name: ");
		HTMLPrinter.getInstance().addLine(result, getName());
		if (getPredefinedType() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "Of Predefined Type: ");
			HTMLPrinter.getInstance().addLine(result, getPredefinedTypeAsString());
		} 
		if (getReferencedType() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "Of Referenced Type: ");
			if (getReferencedType().getModule() != null)
				HTMLPrinter.getInstance().addLine(result, getReferencedType().getModule() + ".");
			if (getReferencedType().getIdentifier() != null)
				HTMLPrinter.getInstance().addLine(result, getReferencedType().getIdentifier().getName());
		} 
		if (getGroupName() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Group:");
			HTMLPrinter.getInstance().addLine(result, getGroupName());
		}
		if (getSurroundingScope().getScopeSymbol() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Scope of:");
			result.append(getSurroundingScope().getScopeSymbol().getSymbolDescriptionPlain());
		}
	
		HTMLPrinter.getInstance().addPageEpilog(result);
				
		return result.toString();
	}

	@Override
	public String getSymbolDescriptionContentAssist() {
		String result = getName() + " - template " ;
		
		if (getPredefinedType() != null) {
			result += getPredefinedTypeAsString();
		} else {
			if (getReferencedType() != null) {
				if (getReferencedType().getModule() != null)
					result += getReferencedType().getModule() + ".";
				if (getReferencedType().getIdentifier() != null)
					result += getReferencedType().getIdentifier().getName();
			}
		}
		return result;
	}
	
	@Override
	public String getSymbolDescriptionPlain() {
		String result = ""; 

		if (getPredefinedType() != null) {
			result += getPredefinedTypeAsString();
		} else {
			if (getReferencedType() != null) {
				if (getReferencedType().getModule() != null)
					result += getReferencedType().getModule() + ".";
				if (getReferencedType().getIdentifier() != null)
					result += getReferencedType().getIdentifier().getName();
			}
		}
		
		result += " template " + getName();
		return result;
	}


	
}
