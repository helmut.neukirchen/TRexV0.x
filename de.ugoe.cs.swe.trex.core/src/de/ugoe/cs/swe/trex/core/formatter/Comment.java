package de.ugoe.cs.swe.trex.core.formatter;

import java.util.LinkedList;
import java.util.List;

import antlr.Token;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;

public class Comment {
	boolean multiLineComment = false;

	boolean newlineBefore = false;

	int indentationLevel = 0;

	List<String> commentText = new LinkedList<String>();

	// ------------------------------------------------------------------------------------------------------

	public boolean isMultiLineComment() {
		return multiLineComment;
	}

	// ------------------------------------------------------------------------------------------------------

	public void setMultiLineComment(boolean multiLineComment) {
		this.multiLineComment = multiLineComment;
	}

	// ------------------------------------------------------------------------------------------------------

	public boolean isNewlineBefore() {
		return newlineBefore;
	}

	// ------------------------------------------------------------------------------------------------------

	public void setNewlineBefore(boolean newlineBefore) {
		this.newlineBefore = newlineBefore;
	}

	// ------------------------------------------------------------------------------------------------------

	public List<String> getCommentText() {
		return commentText;
	}

	// ------------------------------------------------------------------------------------------------------

	public void setCommentText(List<String> commentText) {
		this.commentText = commentText;
	}

	// ------------------------------------------------------------------------------------------------------

	public void addCommentText(String text) {
		this.commentText.add(text);
	}

	// ------------------------------------------------------------------------------------------------------

	public int getIndentationLevel() {
		return indentationLevel;
	}

	// ------------------------------------------------------------------------------------------------------

	public void setIndentationLevel(int indentationLevel) {
		this.indentationLevel = indentationLevel;
	}

	// ------------------------------------------------------------------------------------------------------

	public static List<String> splitCommentToken(Token t) {
		String[] splittedComment = t.getText().split("[\r\n]");
		List<String> result = new LinkedList<String>();

		for (String element : splittedComment) {
			if (element.replaceAll("[\r\n]", "").length() > 0)
				result.add(element.replaceAll("[\r\n]", ""));
		}

		return result;
	}

	// ------------------------------------------------------------------------------------------------------

	public static Comment createCommentFromTokenStreamPosition(
			TTCN3TokenStream t) throws Exception {
		if (!((t.getElement().getType() == TTCN3LexerTokenTypes.SL_COMMENT) || (t
				.getElement().getType() == TTCN3LexerTokenTypes.ML_COMMENT)))
			throw new Exception(
					"Trying to create comment from non-comment token.");

		Comment comment = new Comment();

		if (t.isNewlineBeforeLast())
			comment.setNewlineBefore(true);
		else
			comment.setNewlineBefore(false);

		if (t.getElement().getType() == TTCN3LexerTokenTypes.SL_COMMENT)
			comment.setMultiLineComment(false);

		if (t.getElement().getType() == TTCN3LexerTokenTypes.ML_COMMENT)
			comment.setMultiLineComment(true);

		ScanResult scan = t.scanBackwardSpaces();
		comment.setIndentationLevel(scan.getWhiteSpaceCount());
		comment.setNewlineBefore(t.previousTokenContainsNewline());
		
		comment.setCommentText(Comment.splitCommentToken(t.getElement()));
		comment.stripPrecedingIndentations();

		return comment;
	}

	// ------------------------------------------------------------------------------------------------------

	private void stripPrecedingIndentations() {
		List<String> s = getCommentText();

		if (s.size() <= 1)
			return;

		for (int i = 1; i < s.size(); i++) {
			String str = s.get(i);

			for (int n = 0; n < indentationLevel; n++) {
				if (str.startsWith(" ") || str.startsWith("\t"))
					str = str.substring(1);
				else
					break;
			}
		}

		setCommentText(s);
	}

	// ------------------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("-[comment object]-------------------------------\n");
		result.append("multiline comment: " + multiLineComment + "\n");
		result.append("newline before   : " + newlineBefore + "\n");
		result.append("indentation level: " + indentationLevel + "\n");
		result.append("comment text     : " + commentText + "\n");
		
		return result.toString();
	}
	
}
