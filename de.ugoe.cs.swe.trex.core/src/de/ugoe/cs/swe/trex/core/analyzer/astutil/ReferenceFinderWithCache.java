package de.ugoe.cs.swe.trex.core.analyzer.astutil;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;

public class ReferenceFinderWithCache extends ReferenceFinder {

	private ReferenceCache referenceCache;
	
	public ReferenceFinderWithCache(){
		referenceCache = ReferenceCache.getInstance();
	}

	// --------------------------------------------------------------

	/**
	 * Find all references to the given <code>Symbol</code>
	 * 
	 * @param s
	 *            the symbol to search for
	 * @return a <code>Map</code> of filenames (<code>Strings</code>) to
	 *         <code>ReferenceWithContext</code> objects
	 */
	public Map<String, List<ReferenceWithContext>> findReferences(Symbol s) {
		List<Symbol> list = new LinkedList<Symbol>();
		list.add(s);

		return findReferences(list);
	}

	// --------------------------------------------------------------

	/**
	 * Find all references to <code>Symbols</code> in the given
	 * <code>List</code>
	 * 
	 * @param symbols
	 *            the <code>List</code> of symbols to search for
	 * @return a <code>Map</code> of filenames (<code>Strings</code>) to
	 *         <code>ReferenceWithContext</code> objects
	 */

	public Map<String, List<ReferenceWithContext>> findReferences(
			List<Symbol> symbols) {
		Map<String, List<ReferenceWithContext>> result = new HashMap<String, List<ReferenceWithContext>>();
		for (Symbol symbol : symbols) {
			Map<String, List<ReferenceWithContext>> references = referenceCache.getReferences(symbol);
			
			for (Entry<String, List<ReferenceWithContext>> entry : references.entrySet()) {

				if (!result.containsKey(entry.getKey())) {
					result.put(entry.getKey(), new LinkedList<ReferenceWithContext>());
				}
				result.get(entry.getKey()).addAll(entry.getValue());
			}
		}
		return result;
	}	

	// --------------------------------------------------------------

	// --------------------------------------------------------------
	//TODO: Revise and enable once further improvements in cache are added
		
//	public Map<String, List<ReferenceWithContext>> findReferences2(Symbol s){
//		Map<String, List<ReferenceWithContext>> result = new HashMap<String, List<ReferenceWithContext>>();		
//		ArrayList<LocationAST> references = this.referenceCache.getReferences(s);
//		for (LocationAST reference : references){
//			ReferenceWithContext referenceWithContext = createReferenceWithContext(reference);
//			String associatedFilename = reference.getScope().getAssociatedFilename();
//			if (!result.containsKey(associatedFilename)){
//				result.put(associatedFilename, new ArrayList<ReferenceWithContext>());
//			}
//			result.get(associatedFilename).add(referenceWithContext);
//			
//		}
//		return result;
//	}
//
//	//RAW method
//	
//	public ArrayList<LocationAST> findRawReferences(Symbol s){
//		ArrayList<LocationAST> references = this.referenceCache.getReferences(s);
//		return references;
//	}
//
//	public int rawCountReferences(Symbol s){
//		ArrayList<LocationAST> references = this.referenceCache.getReferences(s);
//		if (references == null){
//			System.out.println(s +" is not initialized?");
//			return 0;
//		}
//		return references.size();
//	}

}
