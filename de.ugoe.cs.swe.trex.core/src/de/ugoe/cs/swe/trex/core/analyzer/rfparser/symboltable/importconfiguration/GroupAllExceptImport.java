package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration;

import java.util.LinkedList;
import java.util.List;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;


public class GroupAllExceptImport extends AbstractKindImport {
	private List<AbstractKindImport> exceptionList = new LinkedList<AbstractKindImport>();

	public void addForbiddenKind(AbstractKindImport k) {
		exceptionList.add(k);
	}

	@Override
	public boolean isAllowed(Symbol s, KindEnum kind) {
//		boolean result = false;
//		for (int i=0; i < exceptionList.size(); i++) {
//			AbstractKindImport k = exceptionList.get(i);
//			if (k.getType() == kind) {
//				if (k instanceof KindAllExceptImport) { // twofold except not allowed
//					result = true;
//				} else if (k instanceof KindNames) {
//					KindNames ktmp = (KindNames) k;
//					if (!ktmp.hasName(s))
//						result=true;
//				}
//			}
//		}
//		
//		return result;
		return true;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		
		result.append("GroupAllExceptImport: ");
		result.append("ExceptionList: ");
		
		for (int i=0; i < exceptionList.size(); i++) {
			AbstractKindImport kindImp = exceptionList.get(i);
			result.append(kindImp.toString());
		}
		
		return result.toString();
	}

}
