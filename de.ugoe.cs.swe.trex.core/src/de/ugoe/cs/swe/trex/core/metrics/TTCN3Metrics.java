package de.ugoe.cs.swe.trex.core.metrics;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.TTCN3CorePlugin;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;

public class TTCN3Metrics {
	static final Logger LOG = Logger.getLogger(TTCN3Metrics.class.getName());

	public static int getValueCouplingScore(final AST ast) {
		ValueCoupling vc;

		if(ast.getType() == TTCN3ParserTokenTypes.SendStatement || ast.getType() == TTCN3ParserTokenTypes.ReceiveStatement) {
			vc = processSendReceiveStatement(ast);
			return vc == null ? -1 : vc.getScore();
		}

		return -1;
	}

	private static ValueCoupling processSendReceiveStatement(final AST ast) {
		try {
			AST aChild = ast.getFirstChild().getNextSibling();  // ignore PortOrAny

			// case for port.receive; (no params)
			if(aChild.getType() == TTCN3ParserTokenTypes.PortReceiveOp) {
				if(aChild.getFirstChild() == null) {
					TTCN3Metrics.LOG.fine("PORT.RECEIVE ALONE @ "+aChild.getLine());
					return new ValueCoupling(1, 1);
				}
				aChild = aChild.getFirstChild();
			}

			// get to templatebody
			aChild = aChild.getFirstChild().getFirstChild();

			// case for type specific e.g. port.receive(TYPE: TEMPLATE);
			if(aChild.getType() == TTCN3ParserTokenTypes.Type) {
				aChild = aChild.getNextSibling();
			}

			// once reached TemplateBody, keep looping down till find key
			// AST from which we can derive complexity
			while(aChild != null) {
				// Value    = non parameterised
				// OpCall   = parameterised
				// ArrayVal = inlined
				// AnyValue = wildcards
				// FieldSpecList = empty inline
				// XXX: what about parameterised with inlined templates as parameters?
				// e.g. CEenv.send(Ce_GroupPoolsList_Reply_t: Ce_GroupPoolsList_Reply1(1, Ce_TransactionInfo_Ie_AppspData_t:{0,0} , 1));
				if(aChild.getType() == TTCN3ParserTokenTypes.Value) {
					TTCN3Metrics.LOG.fine("NON PARAMATERISED @ "+aChild.getLine());
					return new ValueCoupling(1, 1);
				} else if(aChild.getType() == TTCN3ParserTokenTypes.OpCall) {
					TTCN3Metrics.LOG.fine("PARAMATERISED @ "+aChild.getLine());
					return new ValueCoupling(2, 1);
				} else if(aChild.getType() == TTCN3ParserTokenTypes.ArrayValueOrAttrib) {
					TTCN3Metrics.LOG.fine("INLINED @ "+aChild.getLine());
					return new ValueCoupling(3, 1);
				} else if(aChild.getType() == TTCN3ParserTokenTypes.AnyValue) {
					TTCN3Metrics.LOG.fine("WILDCARDS @ "+aChild.getLine());
					return new ValueCoupling(1, 1);
				} else if(aChild.getType() == TTCN3ParserTokenTypes.FieldSpecList) {
					TTCN3Metrics.LOG.fine("EMPTY INLINE @ "+aChild.getLine());
					return new ValueCoupling(1, 1);
				}
				aChild = aChild.getFirstChild();
			}

			return null;
		} catch (NullPointerException npe) {
			npe.printStackTrace();
			TTCN3Metrics.LOG.severe("NPE: "+ast.getLine());
			return null;
		}
	}

	// -------------------------------------------------------------------------

	List<AST> altsteps, components, functions, others, ports, templates, testcases, types;

	Map<AST, Integer> astToRef;

	List<AST> couplings;

	ValueCoupling valueCoupling;

	// -------------------------------------------------------------------------

	public TTCN3Metrics() {
		this.altsteps = new LinkedList<AST>();
		this.components = new LinkedList<AST>();
		this.functions = new LinkedList<AST>();
		this.others = new LinkedList<AST>();
		this.ports = new LinkedList<AST>();
		this.templates = new LinkedList<AST>();
		this.testcases = new LinkedList<AST>();
		this.types = new LinkedList<AST>();
		this.couplings = new LinkedList<AST>();
		this.astToRef = new HashMap<AST, Integer>();
	}

	// -------------------------------------------------------------------------

	public void addAltstep(final AST altstep) {
		this.altsteps.add(altstep);
	}

	// -------------------------------------------------------------------------

	public void addComponent(final AST component) {
		this.components.add(component);
	}

	// -------------------------------------------------------------------------

	public void addFunction(final AST function) {
		this.functions.add(function);
	}

	// -------------------------------------------------------------------------

	public void addOther(final AST other) {
		this.others.add(other);
	}

	// -------------------------------------------------------------------------

	public void addPort(final AST port) {
		this.ports.add(port);
	}

	// -------------------------------------------------------------------------

	public void addTemplate(final AST template) {
		this.templates.add(template);
	}

	// -------------------------------------------------------------------------

	public void addTestCase(final AST testcase) {
		this.testcases.add(testcase);
	}

	// -------------------------------------------------------------------------

	public void addType(final AST type) {
		this.types.add(type);
	}

	// -------------------------------------------------------------------------

	private void calcValueCoupling() {
		/*
			To compute this behaviour statements contained within testcases, functions, and altsteps are analysed as follows:
				o	The scoring for each behaviour statement:
				?	Statement that references a template without parameters e.g. port.output(TemplateRef) etc  = 1
				?	Statement that references a template with parameters e.g. port.output(TemplateRef (1,�aa�)) etc  = 2
				?	Statement that uses an in-line template e.g. port.output({1,��aa�})etc  = 3
				?	Port.receive(?) (wildcards only) = 1 (note that this depends upon later usage of variables if used, but is ignored for now)
		 */

		// empty the list of coupling AST objects
		couplings.clear();

		valueCoupling = new ValueCoupling();

		valueCoupling.add(doAltsteps());
		valueCoupling.add(doFunctions());
		valueCoupling.add(doTestcases());
	}

	// -------------------------------------------------------------------------

	private ValueCoupling doAltsteps() {
		final Iterator<AST> iter = this.altsteps.iterator();
		AST ast;
		final ValueCoupling vc = new ValueCoupling();

		while(iter.hasNext()) {
			ast = iter.next();

			vc.add(processAltstepDef(ast));

		}
		return vc;
	}

	// -------------------------------------------------------------------------

	/**
	 * Process a FunctionDef, looking for statements to tally up a Value
	 * Coupling Metric.
	 *
	 * @param iter
	 * @return
	 */
	private ValueCoupling doFunctions() {
		final Iterator<AST> iter = functions.iterator();
		AST ast;
		final ValueCoupling vc = new ValueCoupling();

		while(iter.hasNext()) {
			ast = iter.next();

			vc.add(processFunctionDef(ast));

		}
		return vc;
	}

	// -------------------------------------------------------------------------

	private ValueCoupling doTestcases() {
		final Iterator<AST> iter = testcases.iterator();
		AST ast;
		final ValueCoupling vc = new ValueCoupling();

		while(iter.hasNext()) {
			ast = iter.next();

			vc.add(processTestCaseDef(ast));

		}
		return vc;
	}

	// -------------------------------------------------------------------------

	public List<AST> get(String metric) {
		if (metric.startsWith("Number of")) {
			String metricType = metric.substring(10);
			if ("altsteps".equals(metricType)) {
				return this.altsteps;
			} else if ("components".equals(metricType)) {
				return components;
			} else if ("functions".equals(metricType)) {
				return functions;
			} else if ("others".equals(metricType)) {
				return others;
			} else if ("ports".equals(metricType)) {
				return ports;
			} else if ("templates".equals(metricType)) {
				return templates;
			} else if ("test cases".equals(metricType)) {
				return testcases;
			} else if ("types".equals(metricType)) {
				return types;
			}
		}

		if ("Template Coupling Metric".equals(metric)) {
			return couplings;
		}

		return null;
	}

	// -------------------------------------------------------------------------

	/**
	 * @return Returns the altsteps.
	 */
	public List<AST> getAltsteps() {
		return this.altsteps;
	}

	// -------------------------------------------------------------------------

	/**
	 * @return Returns the components.
	 */
	public List<AST> getComponents() {
		return this.components;
	}

	// -------------------------------------------------------------------------

	/**
	 * @return Returns the functions.
	 */
	public List<AST> getFunctions() {
		return this.functions;
	}

	// -------------------------------------------------------------------------

	public int getNum(final String metricName) {
		final List<AST> list = get(metricName);

		if(list != null) {
			return list.size();
		}
		return -1;
	}

	// -------------------------------------------------------------------------

	/**
	 * @return Returns the others.
	 */
	public List<AST> getOthers() {
		return this.others;
	}

	// -------------------------------------------------------------------------

	/**
	 * @return Returns the ports.
	 */
	public List<AST> getPorts() {
		return this.ports;
	}

	// -------------------------------------------------------------------------

	public int getReferences(LocationAST ast) {
		// if identifier then get number of references
		if(ast.getType() == TTCN3LexerTokenTypes.IDENTIFIER) {
			Integer temp = this.astToRef.get(ast);
			return temp == null ? 0 : temp.intValue();
		}

		// otherwise try to re-run using getIdent
		final LocationAST identNode = ASTUtil.getIdentNode(ast);
		if(identNode.getType() == TTCN3LexerTokenTypes.IDENTIFIER)
			return getReferences(identNode);

		// otherwise, not valid
		return -1;
	}

	// -------------------------------------------------------------------------

	/**
	 * @return Returns the templates.
	 */
	public List<AST> getTemplates() {
		return this.templates;
	}

	// -------------------------------------------------------------------------

	/**
	 * @return Returns the testcases.
	 */
	public List<AST> getTestcases() {
		return this.testcases;
	}

	// -------------------------------------------------------------------------

	/**
	 * @return Returns the types.
	 */
	public List<AST> getTypes() {
		return this.types;
	}

	// -------------------------------------------------------------------------

	public double getValueCoupling() {
		calcValueCoupling();

		if(valueCoupling != null) {
			final double score = valueCoupling.getScore();
			final double num = valueCoupling.getNumStatements();
			if(num == 0) {
				return 0;
			}
			// return score/num rounded to three decimal places
			return (double)Math.round(score*1000/num)/1000;
		}

		TTCN3Metrics.LOG.severe("NO VALUECOUPLING FOUND");
		return -1;
	}

	// -------------------------------------------------------------------------

	public boolean has(final String metricName) {
		return getNum(metricName) > 0;
	}

	// -------------------------------------------------------------------------

	/**
	 * Process an AltGuardList. TODO Javadoc
	 * @param ast the AltGuardList
	 * @return a ValueCoupling object holding information for the AltGuardList including nested StatementBlocks
	 */
	private ValueCoupling processAltGuardList(final AST ast) {
		TTCN3Metrics.LOG.fine("PAGL ast: "+ast.getText()+":"+ast.getLine());
		final ValueCoupling vc = new ValueCoupling();

		// FIXME: improve and document
		AST aChild = ast.getFirstChild().getFirstChild().getNextSibling(); // first guardop

		vc.add(processCommunicationStatements(aChild));

		// look for any nested statementblocks and process them also
		aChild = aChild.getNextSibling();
		if(aChild != null && aChild.getType() == TTCN3ParserTokenTypes.StatementBlock) {
			vc.add(processStatementBlock(aChild));
		}

		return vc;
	}

	// -------------------------------------------------------------------------

	/**
	 * Loop through first child and siblings until find an AltGuardList.
	 * @param ast the AltstepDef node whose child and siblings should be processed.
	 * @return the ValueCoupling for the AltGuardList.
	 * @return null if no AltGuardList found.
	 */
	private ValueCoupling processAltstepDef(final AST ast) {
		TTCN3Metrics.LOG.fine("PAD ast: "+ast.getText()+":"+ast.getLine());
		AST aChild = ast.getFirstChild();

		while(aChild != null) {
			if(aChild.getType() == TTCN3ParserTokenTypes.AltGuardList) {
				return processAltGuardList(aChild);
			}
			aChild = aChild.getNextSibling();
		}

		return null;
	}

	// -------------------------------------------------------------------------

	private ValueCoupling processBehaviourStatements(final AST ast) {
		TTCN3Metrics.LOG.fine("PBS ast: "+ast.getText()+":"+ast.getLine());

		AST aChild = ast.getFirstChild().getFirstChild();
		if(aChild != null && aChild.getType() == TTCN3ParserTokenTypes.AltGuardList) {
			return processAltGuardList(aChild);
		}

		return null;
	}

	// -------------------------------------------------------------------------

	private ValueCoupling processCommunicationStatements(final AST ast) {
		TTCN3Metrics.LOG.fine("PCS ast: "+ast.getText()+":"+ast.getLine());
		final AST aChild = ast.getFirstChild();

		if(aChild.getType() == TTCN3ParserTokenTypes.SendStatement || aChild.getType() == TTCN3ParserTokenTypes.ReceiveStatement) {
			final ValueCoupling vc =  processSendReceiveStatement(aChild);

			if(vc == null) {
				TTCN3Metrics.LOG.info("FAIL AST: "+ast.getText()+" : "+ast.getType()+ " : "+ast.getLine());
			} else {
				// add to couplings List for display on metric view
				couplings.add(aChild);
				return vc;
			}
		}

		return null;
	}

	// -------------------------------------------------------------------------

	private void processDefinition(LocationAST aChild, List<LocationAST> list) {
		LocationAST bChild  = aChild.getFirstChild();
		int bType = bChild.getType();

		// groups should be broken down into constituent parts
		if(bType == TTCN3ParserTokenTypes.GroupDef) {
			processTree(bChild, list);
			return;
		}

		switch (bType) {
		case TTCN3ParserTokenTypes.AltstepDef:
			addAltstep(bChild);
			break;
		case TTCN3ParserTokenTypes.ComponentDef:
			addComponent(bChild);
			break;
		case TTCN3ParserTokenTypes.FunctionDef:
			addFunction(bChild);
			break;
		case TTCN3ParserTokenTypes.PortDef:
			addPort(bChild);
			break;
		case TTCN3ParserTokenTypes.TemplateDef:
			addTemplate(bChild);
			break;
		case TTCN3ParserTokenTypes.TestcaseDef:
			addTestCase(bChild);
			break;
		case TTCN3ParserTokenTypes.TypeDef:
			addType(bChild);
			break;
		default:
			addOther(bChild);
		break;
		}

		list.add(bChild);
	}

	// -------------------------------------------------------------------------

	private ValueCoupling processFunctionDef(final AST ast) {
		TTCN3Metrics.LOG.fine("PFD ast: "+ast.getText());
		return processTillStatementBlock(ast);
	}

	// -------------------------------------------------------------------------

	private ValueCoupling processFunctionStatementOrDef(final AST ast) {
		TTCN3Metrics.LOG.fine("PFSOD ast: "+ast.getText()+":"+ast.getLine());

		// go down two steps to see type of FunctionStatement and process if
		// communication or behaviour(that could contain communication
		// later)
		final AST aChild = (ast.getFirstChild()).getFirstChild();
		if(aChild.getType() == TTCN3ParserTokenTypes.BehaviourStatements) {
			return processBehaviourStatements(aChild);
		} else if(aChild.getType() == TTCN3ParserTokenTypes.CommunicationStatements) {
			return processCommunicationStatements(aChild);
		}

		return null;
	}

	// -------------------------------------------------------------------------

	public List<LocationAST> processMetrics(Object anElement) {
		List<LocationAST> list = new LinkedList<LocationAST>();
		processTree(anElement, list);
		return list;
	}

	// -------------------------------------------------------------------------

	private ValueCoupling processStatementBlock(final AST ast) {
		TTCN3Metrics.LOG.fine("PSB ast: "+ast.getText());

		final ValueCoupling vc = new ValueCoupling();
		AST aChild = ast.getFirstChild();
		while(aChild != null) {
			if((aChild.getType() == TTCN3ParserTokenTypes.FunctionStatement) ||
					(aChild.getType() == TTCN3ParserTokenTypes.FunctionLocalDef) ||
					(aChild.getType() == TTCN3ParserTokenTypes.FunctionLocalInst)) {
				vc.add(processFunctionStatementOrDef(aChild));
			}
			aChild=aChild.getNextSibling();
		}
		return vc;
	}

	// -------------------------------------------------------------------------

	// TestCaseDef, loop till StatementBlock, enter and loop through all
	// FunctionStatementDef's until
	// FunctionStatement goes to CommunicationStatement then use generic
	// processor to loop down that and work out if parameterised or not
	private ValueCoupling processTestCaseDef(final AST ast) {
		TTCN3Metrics.LOG.fine("PTC ast: "+ast.getText());
		return processTillStatementBlock(ast);
	}

	// -------------------------------------------------------------------------

	/**
	 * Loop through first child and siblings until find a StatementBlock.
	 * @param ast the AST node whose children should be processed.
	 * @return the ValueCoupling for the StatementBlock.
	 * @return null if no StatementBlock found.
	 */
	private ValueCoupling processTillStatementBlock(final AST ast) {
		AST aChild = ast.getFirstChild();

		while(aChild != null) {
			if(aChild.getType() == TTCN3ParserTokenTypes.StatementBlock) {
				return processStatementBlock(aChild);
			}
			aChild = aChild.getNextSibling();
		}

		return null;
	}

	// -------------------------------------------------------------------------

	public List<LocationAST> processTree(Object anElement) {
		List<LocationAST> list = new LinkedList<LocationAST>();
		processTree(anElement, list);
		return list;
	}

	// -------------------------------------------------------------------------

	public void processTree(Object anElement, List<LocationAST> list) {
		if (anElement instanceof LocationAST
				&& ((LocationAST) anElement).getFirstChild() != null) {

			LocationAST aChild = ((LocationAST) anElement).getFirstChild();
			while (aChild != null) {
				int type = aChild.getType();
				if (type == TTCN3ParserTokenTypes.TTCN3Module
						|| type == TTCN3ParserTokenTypes.ModuleControlPart
						|| type == TTCN3ParserTokenTypes.ModuleControlBody
						|| type == TTCN3ParserTokenTypes.ModuleDefinitionsPart
						|| type == TTCN3ParserTokenTypes.ModuleDefinitionList) {

					processTree(aChild, list);

				} else if (type == TTCN3ParserTokenTypes.ModuleDefinition) {
					processDefinition(aChild, list);
				}

				aChild = aChild.getNextSibling();
			}
		}
	}

	// -------------------------------------------------------------------------

	public void saveReferences(LocationAST ast) {
		LocationAST node = ast;
		if(node.getType() != TTCN3LexerTokenTypes.IDENTIFIER) {
			node = ASTUtil.getIdentNode(node);
		}

		int num = getReferences(node);
		astToRef.put(node, Integer.valueOf(num+1));
	}

	// -------------------------------------------------------------------------

	public void storeReferences(Map<String, List<ReferenceWithContext>> references) {
		for(Map.Entry <String, List<ReferenceWithContext>> refSet : references.entrySet()) {
			for(ReferenceWithContext ref : refSet.getValue()) {
				LocationAST matchNode = ref.getMatchNode();
				saveReferences(matchNode);
			}
		}
		TTCN3CorePlugin.debugMessage("  ...Stored "+references.size()+" References...");
	}

}
