// $ANTLR 2.7.7 (2006-11-01): "TTCN3SemanticAnalysisTreeParser.g" -> "TTCN3SemanticAnalysisTreeParser.java"$

package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

public interface TTCN3SemanticAnalysisTreeParserTokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int AddExpression = 4;
	int AddressRefList = 5;
	int AliveKeyword = 6;
	int AliveOp = 7;
	int AllAltstepsWithExcept = 8;
	int AllCompsAllPortsSpec = 9;
	int AllConnectionsSpec = 10;
	int AllConstsWithExcept = 11;
	int AllFunctionsWithExcept = 12;
	int AllGroupsWithExcept = 13;
	int AllModuleParWithExcept = 14;
	int AllPortsSpec = 15;
	int AllSignaturesWithExcept = 16;
	int AllTemplsWithExcept = 17;
	int AllTestcasesWithExcept = 18;
	int AllTypesWithExcept = 19;
	int AllWithExcepts = 20;
	int AltstepInstance = 21;
	int AltstepLocalDef = 22;
	int AltstepLocalDefList = 23;
	int AltstepRef = 24;
	int AltstepRefList = 25;
	int AndExpression = 26;
	int AnyTypeKeyword = 27;
	int AllRef = 28;
	int BitAndExpression = 29;
	int BitNotExpression = 30;
	int BitOrExpression = 31;
	int BitXorExpression = 32;
	int BreakStatement = 33;
	int CaseKeyword = 34;
	int CheckPortOpsPresent = 35;
	int CompenentIdentifierOrLiteral = 36;
	int ComponentIdentifierOrLiteral = 37;
	int ComponentReferenceOrLiteral = 38;
	int ConstRefList = 39;
	int ContinueStatement = 40;
	int DefaultKeyword = 41;
	int DerivedRefWithParList = 42;
	int EqualExpression = 43;
	int ExceptAltstepSpec = 44;
	int ExceptConstSpec = 45;
	int ExceptElement = 46;
	int ExceptFunctionSpec = 47;
	int ExceptGroupRefList = 48;
	int ExceptGroupSpec = 49;
	int ExceptModuleParSpec = 50;
	int ExceptSignatureSpec = 51;
	int ExceptSpec = 52;
	int ExceptTemplateSpec = 53;
	int ExceptTestcaseSpec = 54;
	int ExceptTypeDefSpec = 55;
	int ExceptsDef = 56;
	int ExtendsKeyword = 57;
	int FloatDotNotation = 58;
	int FloatENotation = 59;
	int FromClausePresent = 60;
	int FullGroupIdentifier = 61;
	int FullGroupIdentifierWithExcept = 62;
	int FunctionRefList = 63;
	int GroupRefList = 64;
	int GroupRefListWithExcept = 65;
	int ImportAltstepSpec = 66;
	int ImportModuleParSpec = 67;
	int KillKeyword = 68;
	int KillTCStatement = 69;
	int KilledStatement = 70;
	int LogItem = 71;
	int ModuleDefinitionList = 72;
	int ModuleParRefList = 73;
	int MulExpression = 74;
	int NestedEnumDef = 75;
	int NestedRecordDef = 76;
	int NestedRecordOfDef = 77;
	int NestedSetDef = 78;
	int NestedSetOfDef = 79;
	int NestedTypeDef = 80;
	int NestedUnionDef = 81;
	int NotExpression = 82;
	int OmitValue = 83;
	int PortRedirectSymbol = 84;
	int PortRedirectWithValueAndParam = 85;
	int RedirectPresent = 86;
	int RedirectWithParamSpec = 87;
	int RedirectWithValueAndParamSpec = 88;
	int RelExpression = 89;
	int RepeatStatement = 90;
	int RestrictedTemplate = 91;
	int SelectCase = 92;
	int SelectCaseBody = 93;
	int SelectCaseConstruct = 94;
	int SelectKeyword = 95;
	int ShiftExpression = 96;
	int SignatureRefList = 97;
	int SingleConnectionSpec = 98;
	int SingleOrMultiConnectionSpec = 99;
	int SingleTimerInstance = 100;
	int TempVarList = 101;
	int TemplateRefList = 102;
	int TemplateRestriction = 103;
	int TestcaseRefList = 104;
	int TimerList = 105;
	int TypeRefList = 106;
	int UnaryExpression = 107;
	int NameForm = 108;
	int NumberForm = 109;
	int NameAndNumberForm = 110;
	int VariationKeyword = 111;
	int DefinitiveIdentifier = 112;
	int DefinitiveNumberForm = 113;
	int XorExpression = 114;
	int DefinitiveNameAndNumberForm = 115;
	int ActivateOp = 116;
	int AddOp = 117;
	int AddressKeyword = 118;
	int AddressDecl = 119;
	int AddressRef = 120;
	int AddressValue = 121;
	int AllKeyword = 122;
	int AllElementsFrom = 123;
	int AllOrSignatureList = 124;
	int AllOrTypeList = 125;
	int AllPort = 126;
	int AllowedValues = 127;
	int AltConstruct = 128;
	int AltGuardChar = 129;
	int AltGuardElement = 130;
	int AltGuardElements = 131;
	int AltGuardList = 132;
	int AltKeyword = 133;
	int AltstepDef = 134;
	int AltstepKeyword = 135;
	int And4b = 136;
	int And = 137;
	int AnyKeyword = 138;
	int AnyOrOmit = 139;
	int AnyValue = 140;
	int ArrayBounds = 141;
	int ArrayConstExpression = 142;
	int ArrayDef = 143;
	int ArrayDefRange = 144;
	int ArrayElementConstExpressionList = 145;
	int ArrayElementExpressionList = 146;
	int ArrayElementSpec = 147;
	int ArrayElementSpecList = 148;
	int ArrayExpression = 149;
	int ArrayOrBitRef = 150;
	int ArrayValueOrAttrib = 151;
	int Assignment = 152;
	int AssignmentChar = 153;
	int AssignmentList = 154;
	int AttribKeyword = 155;
	int AttribQualifier = 156;
	int AttribSpec = 157;
	int BaseTemplate = 158;
	int BasicStatements = 159;
	int BeginChar = 160;
	int BehaviourStatements = 161;
	int BinOrMatch = 162;
	int BitOp = 163;
	int BitStringKeyword = 164;
	int BitStringMatch = 165;
	int BitStringValue = 166;
	int BooleanExpression = 167;
	int BooleanKeyword = 168;
	int BooleanValue = 169;
	int CallBodyGuard = 170;
	int CallBodyOps = 171;
	int CallBodyStatement = 172;
	int CallBodyStatementList = 173;
	int CallParameters = 174;
	int CallStatement = 175;
	int CallTimerValue = 176;
	int CatchOpParameter = 177;
	int CatchStatement = 178;
	int Cell = 179;
	int CharKeyword = 180;
	int CharStringKeyword = 181;
	int CharStringMatch = 182;
	int CharStringPattern = 183;
	int CharStringValue = 184;
	int CharStringValueId = 185;
	int CheckParameter = 186;
	int CheckStatement = 187;
	int CheckStateStatement = 188;
	int ClearStatement = 189;
	int Colon = 190;
	int Comma = 191;
	int CommunicationStatements = 192;
	int Complement = 193;
	int ComponentDef = 194;
	int ComponentDefList = 195;
	int ComponentElementDef = 196;
	int ComponentId = 197;
	int ComponentIdentifier = 198;
	int ComponentRef = 199;
	int ComponentType = 200;
	int ComponentRefAssignment = 201;
	int CompoundConstExpression = 202;
	int CompoundExpression = 203;
	int ConditionalConstruct = 204;
	int ConfigParamDef = 205;
	int ConfigSpec = 206;
	int ConfigurationOps = 207;
	int ConfigurationStatements = 208;
	int ConnectStatement = 209;
	int ConstDef = 210;
	int ConstList = 211;
	int ConstantExpression = 212;
	int ControlStatement = 213;
	int ControlStatementOrDef = 214;
	int ControlStatementOrDefList = 215;
	int CreateOp = 216;
	int Cstring = 217;
	int Dash = 218;
	int DeactivateStatement = 219;
	int DefIndentClose = 220;
	int DefIndentOpen = 221;
	int DefKeyword = 222;
	int DefOrFieldRef = 223;
	int DefOrFieldRefList = 224;
	int DerivedDef = 225;
	int DerivedRegWithParList = 226;
	int Direction = 227;
	int DisconnectStatement = 228;
	int DisplayKeyword = 229;
	int DoWhileStatement = 230;
	int DoneStatement = 231;
	int Dot = 232;
	int DoubleColon = 233;
	int ElseClause = 234;
	int ElseIfClause = 235;
	int ElseIfClauses = 236;
	int ElseStatement = 237;
	int EncodeKeyword = 238;
	int EndChar = 239;
	int EnumDef = 240;
	int EnumeratedValue = 241;
	int Enumeration = 242;
	int EnumerationList = 243;
	int Equal = 244;
	int Error = 245;
	int ExceptionSpec = 246;
	int ExceptionTypeList = 247;
	int ExclamationMark = 248;
	int Expression = 249;
	int ExtConstDef = 250;
	int ExtFunctionDef = 251;
	int ExtendedFieldReference = 252;
	int ExtensionKeyword = 253;
	int ExtraMatchingAttributes = 254;
	int Factor = 255;
	int Fail = 256;
	int False = 257;
	int FieldConstExpressionList = 258;
	int FieldConstExpressionSpec = 259;
	int FieldExpressionList = 260;
	int FieldExpressionSpec = 261;
	int FieldReference = 262;
	int FieldSpec = 263;
	int FieldSpecList = 264;
	int FieldValueOrAttrib = 265;
	int Final = 266;
	int FloatKeyword = 267;
	int FloatValue = 268;
	int ForStatement = 269;
	int FormalPortPar = 270;
	int FormalTemplatePar = 271;
	int FormalTimerPar = 272;
	int FormalTypePar = 273;
	int FormalValuePar = 274;
	int FromClause = 275;
	int FunctionActualParAssignment = 276;
	int FriendKeyword = 277;
	int FriendModuleDef = 278;
	int FunctionActualPar = 279;
	int FunctionActualParList = 280;
	int FunctionBody = 281;
	int FunctionDef = 282;
	int FunctionLocalDefOrInst = 283;
	int FunctionLocalDefOrInstList = 284;
	int FunctionStatementList = 285;
	int FunctionFormalPar = 286;
	int FunctionInstance = 287;
	int FunctionLocalDef = 288;
	int FunctionLocalInst = 289;
	int FunctionRef = 290;
	int FunctionStatement = 291;
	int GetCallStatement = 292;
	int GetLocalVerdict = 293;
	int GetReplyStatement = 294;
	int GlobalModuleId = 295;
	int GotoStatement = 296;
	int Group = 297;
	int GroupDef = 298;
	int GuardOp = 299;
	int GuardStatement = 300;
	int HexOrMatch = 301;
	int HexStringKeyword = 302;
	int HexStringMatch = 303;
	int HexStringValue = 304;
	int HaltStatement = 305;
	int Identifier = 306;
	int IfPresentMatch = 307;
	int ImportAllSpec = 308;
	int ImportConstSpec = 309;
	int ImportDef = 310;
	int ImportElement = 311;
	int ImportFromSpec = 312;
	int ImportFunctionSpec = 313;
	int ImportGroupSpec = 314;
	int ImportNamedAltSpec = 315;
	int ImportSignatureSpec = 316;
	int ImportSpec = 317;
	int ImportTemplateSpec = 318;
	int ImportTestcaseSpec = 319;
	int ImportTypeDefSpec = 320;
	int ImportImportSpec = 321;
	int InLineTemplate = 322;
	int InOutParKeyword = 323;
	int InParKeyword = 324;
	int Inconc = 325;
	int Infinity = 326;
	int Initial = 327;
	int IntegerKeyword = 328;
	int IntegerValue = 329;
	int InterleavedAction = 330;
	int InterleavedConstruct = 331;
	int InterleavedGuard = 332;
	int InterleavedGuardElement = 333;
	int InterleavedGuardList = 334;
	int KilledKeyword = 335;
	int LParen = 336;
	int LabelStatement = 337;
	int LanguageSpec = 338;
	int LengthMatch = 339;
	int Lessorequal = 340;
	int Lessthan = 341;
	int ListOfTemplates = 342;
	int LogStatement = 343;
	int LoopConstruct = 344;
	int LowerBound = 345;
	int MTCOp = 346;
	int MapParamDef = 347;
	int MapStatement = 348;
	int MatchOp = 349;
	int MatchingSymbol = 350;
	int MessageAttribs = 351;
	int MessageList = 352;
	int Minus = 353;
	int MixedAttribs = 354;
	int MixedList = 355;
	int Mod = 356;
	int ModifiesKeyword = 357;
	int ModuleControlBody = 358;
	int ModuleControlPart = 359;
	int ModuleDefinition = 360;
	int ModuleDefinitionsList = 361;
	int ModuleDefinitionsPart = 362;
	int ModuleId = 363;
	int ModulePar = 364;
	int ModuleParDef = 365;
	int ModuleParList = 366;
	int Moreorequal = 367;
	int Morethan = 368;
	int MultiWithAttrib = 369;
	int MultiplyOp = 370;
	int MultitypedModuleParList = 371;
	int NamedAltActualPar = 372;
	int NamedAltActualParList = 373;
	int NamedAltDef = 374;
	int NamedAltFormalPar = 375;
	int NamedAltFormalParList = 376;
	int NamedAltInstance = 377;
	int NamedAltList = 378;
	int NamedAltRef = 379;
	int NamedAltRefList = 380;
	int NamedKeyword = 381;
	int NamedValue = 382;
	int NamedValueList = 383;
	int NoBlockKeyword = 384;
	int None = 385;
	int Not4b = 386;
	int Not = 387;
	int NotUsedOrExpression = 388;
	int NotUsedSymbol = 389;
	int Notequals = 390;
	int NowaitKeyword = 391;
	int Null = 392;
	int Number = 393;
	int NaN = 394;
	int OctOrMatch = 395;
	int OctetStringKeyword = 396;
	int OctetStringMatch = 397;
	int OctetStringValue = 398;
	int OmitKeyword = 399;
	int OpCall = 400;
	int OpValue = 401;
	int OptionalKeyword = 402;
	int Or4b = 403;
	int Or = 404;
	int OutParKeyword = 405;
	int OverrideKeyword = 406;
	int ParRef = 407;
	int ParaAssignmentList = 408;
	int ParamClause = 409;
	int ParaSpec = 410;
	int Pass = 411;
	int PermutationMatch = 412;
	int Plane = 413;
	int Plus = 414;
	int Port = 415;
	int PortCallBody = 416;
	int PortCallOp = 417;
	int PortCatchOp = 418;
	int PortCheckOp = 419;
	int PortClearOp = 420;
	int PortDef = 421;
	int PortDefAttribs = 422;
	int PortDefBody = 423;
	int PortElement = 424;
	int PortGetCallOp = 425;
	int PortGetReplyOp = 426;
	int PortInstance = 427;
	int PortKeyword = 428;
	int PortOrAll = 429;
	int PortOrAllOrAny = 430;
	int PortOrAny = 431;
	int PortRaiseOp = 432;
	int PortReceiveOp = 433;
	int PortRedirect = 434;
	int PortRedirectWithParam = 435;
	int PortRef = 436;
	int PortReplyOp = 437;
	int PortSendOp = 438;
	int PortRefAssignment = 439;
	int PortSpec = 440;
	int PortStartOp = 441;
	int PortStopOp = 442;
	int PortTriggerOp = 443;
	int PortType = 444;
	int PredefinedType = 445;
	int PredefinedValue = 446;
	int Primary = 447;
	int ProcOrType = 448;
	int ProcOrTypeList = 449;
	int ProcedureAttribs = 450;
	int ProcedureList = 451;
	int Product = 452;
	int PortHaltOp = 453;
	int PrivateKeyword = 454;
	int PublicKeyword = 455;
	int Quadruple = 456;
	int QuestionMark = 457;
	int RParen = 458;
	int RaiseStatement = 459;
	int Range = 460;
	int RangeDef = 461;
	int RangeOp = 462;
	int ReadTimerOp = 463;
	int ReceiveParameter = 464;
	int ReceiveStatement = 465;
	int RecordDef = 466;
	int RecordOfDef = 467;
	int RecordRef = 468;
	int RecursiveKeyword = 469;
	int ReferencedType = 470;
	int ReferencedValue = 471;
	int RelOp = 472;
	int Rem = 473;
	int ReplyStatement = 474;
	int ReturnStatement = 475;
	int ReturnType = 476;
	int Row = 477;
	int RunningOp = 478;
	int RunningTimerOp = 479;
	int RunsOnSpec = 480;
	int SUTAction = 481;
	int SUTStatements = 482;
	int SelfOp = 483;
	int SemiColon = 484;
	int SendParameter = 485;
	int SendStatement = 486;
	int SenderSpec = 487;
	int SetDef = 488;
	int SetLocalVerdict = 489;
	int SetOfDef = 490;
	int ShiftOp = 491;
	int Signature = 492;
	int SignatureDef = 493;
	int SignatureFormalPar = 494;
	int SignatureFormalParList = 495;
	int SignatureList = 496;
	int SimpleExpression = 497;
	int SimpleExpressions = 498;
	int SingleConstDef = 499;
	int SingleExpression = 500;
	int SingleQuote = 501;
	int SingleTempVarInstance = 502;
	int SingleValueOrAttrib = 503;
	int SingleVarInstance = 504;
	int SingleValueSpec = 505;
	int SingleWithAttrib = 506;
	int Slash = 507;
	int SquareClose = 508;
	int SquareOpen = 509;
	int Star = 510;
	int StartStatement = 511;
	int StartTCStatement = 512;
	int StartTimerStatement = 513;
	int StatementBlock = 514;
	int Step = 515;
	int StopStatement = 516;
	int StopTCStatement = 517;
	int StopTimerStatement = 518;
	int StringAnyOrOmit = 519;
	int StringAnyValue = 520;
	int StringLength = 521;
	int StringMatchOp = 522;
	int StringOp = 523;
	int StructDefBody = 524;
	int StructDefFormalPar = 525;
	int StructDefFormalParList = 526;
	int StructFieldDef = 527;
	int StructFieldRef = 528;
	int StructOfDefBody = 529;
	int StructuredTypeDef = 530;
	int SubExpression = 531;
	int SubTypeDef = 532;
	int SubTypeSpec = 533;
	int SubsetMatch = 534;
	int SupersetMatch = 535;
	int SystemOp = 536;
	int SystemSpec = 537;
	int TTCN3File = 538;
	int TTCN3Module = 539;
	int TTCN3ModuleId = 540;
	int TemplateActualPar = 541;
	int TemplateActualParList = 542;
	int TemplateBody = 543;
	int TemplateActualParAssignment = 544;
	int TemplateDef = 545;
	int TemplateFormalPar = 546;
	int TemplateFormalParList = 547;
	int TemplateInstanceAssignment = 548;
	int TemplateInstance = 549;
	int TemplateKeyword = 550;
	int TemplateOps = 551;
	int TemplateRef = 552;
	int TemplateRefWithParList = 553;
	int Term = 554;
	int TestcaseActualParAssignment = 555;
	int TestcaseActualPar = 556;
	int TestcaseActualParList = 557;
	int TestcaseDef = 558;
	int TestcaseInstance = 559;
	int TestcaseOperation = 560;
	int TestcaseRef = 561;
	int TimeoutKeyword = 562;
	int TimeoutStatement = 563;
	int TimerInstance = 564;
	int TimerKeyword = 565;
	int TimerOps = 566;
	int TimerRef = 567;
	int TimerRefOrAll = 568;
	int TimerRefAssignment = 569;
	int TimerRefOrAny = 570;
	int TimerStatements = 571;
	int TimerValue = 572;
	int ToClause = 573;
	int TriggerStatement = 574;
	int True = 575;
	int Type = 576;
	int TypeActualPar = 577;
	int TypeActualParList = 578;
	int TypeDef = 579;
	int TypeDefIdentifier = 580;
	int TypeList = 581;
	int TypeReference = 582;
	int UnaryOp = 583;
	int Underscore = 584;
	int UnionDef = 585;
	int UnionDefBody = 586;
	int UnionFieldDef = 587;
	int UniversalChar = 588;
	int UniversalCharString = 589;
	int UniversalKeyword = 590;
	int UnmapParamDef = 591;
	int UnmapStatement = 592;
	int UpperBound = 593;
	int Value = 594;
	int ValueList = 595;
	int ValueMatchSpec = 596;
	int ValueOrAttribList = 597;
	int ValueOrRange = 598;
	int ValueParOrTypePar = 599;
	int ValueReference = 600;
	int ValueSpec = 601;
	int ValueofOp = 602;
	int VarInitialValue = 603;
	int VarInstance = 604;
	int VarList = 605;
	int VariableAssignment = 606;
	int VariableEntry = 607;
	int VariableList = 608;
	int VariableRef = 609;
	int VerdictOps = 610;
	int VerdictStatements = 611;
	int VerdictTypeKeyword = 612;
	int VerdictTypeValue = 613;
	int Visibility = 614;
	int WhileStatement = 615;
	int WildcardLengthMatch = 616;
	int WithAttribList = 617;
	int WithStatement = 618;
	int Xor4b = 619;
	int Xor = 620;
	int BSTRING = 621;
	int BSTRINGMATCH = 622;
	int DOT = 623;
	int FLOATVALUE = 624;
	int HSTRING = 625;
	int HSTRINGMATCH = 626;
	int OSTRING = 627;
	int OSTRINGMATCH = 628;
	int RANGEOP = 629;
	int ACTION = 630;
	int ACTIVATE = 631;
	int ADDRESS = 632;
	int ALIVE = 633;
	int ALL = 634;
	int ALT = 635;
	int ALTSTEP = 636;
	int AND = 637;
	int AND4B = 638;
	int ANY = 639;
	int ANYTYPE = 640;
	int BITSTRING = 641;
	int BOOLEAN = 642;
	int BREAK = 643;
	int CALL = 644;
	int CASE = 645;
	int CATCH = 646;
	int CHAR = 647;
	int CHARSTRING = 648;
	int CHECK = 649;
	int CHECKSTATE = 650;
	int CLEAR = 651;
	int COMPLEMENT = 652;
	int COMPONENT = 653;
	int CONNECT = 654;
	int CONST = 655;
	int CONTINUE = 656;
	int CONTROL = 657;
	int CREATE = 658;
	int DEACTIVATE = 659;
	int DEFAULT = 660;
	int DISCONNECT = 661;
	int DISPLAY = 662;
	int DO = 663;
	int DONE = 664;
	int ELSE = 665;
	int ENCODE = 666;
	int ENUMERATED = 667;
	int ERROR = 668;
	int EXCEPT = 669;
	int EXCEPTION = 670;
	int EXECUTE = 671;
	int EXTENDS = 672;
	int EXTENSION = 673;
	int EXTERNAL = 674;
	int FAIL = 675;
	int FALSE = 676;
	int FLOAT = 677;
	int FOR = 678;
	int FROM = 679;
	int FUNCTION = 680;
	int FRIEND = 681;
	int GETCALL = 682;
	int GETREPLY = 683;
	int GETVERDICT = 684;
	int GOTO = 685;
	int GROUP = 686;
	int HALT = 687;
	int HEXSTRING = 688;
	int IF = 689;
	int IFPRESENT = 690;
	int IMPORT = 691;
	int IN = 692;
	int INCONC = 693;
	int INFINITY = 694;
	int INOUT = 695;
	int INTEGER = 696;
	int INTERLEAVE = 697;
	int KILL = 698;
	int KILLED = 699;
	int LABEL = 700;
	int LANGUAGE = 701;
	int LENGTH = 702;
	int LOG = 703;
	int MAP = 704;
	int MATCH = 705;
	int MESSAGE = 706;
	int MIXED = 707;
	int MOD = 708;
	int MODIFIES = 709;
	int MODULE = 710;
	int MODULEPAR = 711;
	int MTC = 712;
	int NAN = 713;
	int NOBLOCK = 714;
	int NONE = 715;
	int NOT = 716;
	int NOT4B = 717;
	int NOWAIT = 718;
	int NULL = 719;
	int OBJID = 720;
	int OCTETSTRING = 721;
	int OF = 722;
	int OMIT = 723;
	int ON = 724;
	int OPTIONAL = 725;
	int OR = 726;
	int OR4B = 727;
	int OUT = 728;
	int OVERRIDE = 729;
	int PARAM = 730;
	int PASS = 731;
	int PATTERN = 732;
	int PERMUTATION = 733;
	int PORT = 734;
	int PRESENT = 735;
	int PRIVATE = 736;
	int PROCEDURE = 737;
	int PUBLIC = 738;
	int RAISE = 739;
	int READ = 740;
	int RECEIVE = 741;
	int RECORD = 742;
	int RECURSIVE = 743;
	int REM = 744;
	int REPEAT = 745;
	int REPLY = 746;
	int RETURN = 747;
	int RUNNING = 748;
	int RUNS = 749;
	int SELECT = 750;
	int SELF = 751;
	int SEND = 752;
	int SENDER = 753;
	int SET = 754;
	int SETVERDICT = 755;
	int SIGNATURE = 756;
	int START = 757;
	int STOP = 758;
	int SUBSET = 759;
	int SUPERSET = 760;
	int SYSTEM = 761;
	int TEMPLATE = 762;
	int TESTCASE = 763;
	int TIMEOUT = 764;
	int TIMER = 765;
	int TO = 766;
	int TRIGGER = 767;
	int TRUE = 768;
	int TYPE = 769;
	int UNION = 770;
	int UNIVERSAL = 771;
	int UNMAP = 772;
	int VALUE = 773;
	int VALUEOF = 774;
	int VAR = 775;
	int VARIANT = 776;
	int VERDICTTYPE = 777;
	int WHILE = 778;
	int WITH = 779;
	int XOR = 780;
	int XOR4B = 781;
	int WS = 782;
	int SL_COMMENT = 783;
	int ML_COMMENT = 784;
	int IDENTIFIER = 785;
	int ASSIGNMENTCHAR = 786;
	int PORTREDIRECTSYMBOL = 787;
	int BEGINCHAR = 788;
	int CSTRING = 789;
	int STRING = 790;
	int NUMBER = 791;
	int EXPONENT = 792;
	int SEMICOLON = 793;
	int COMMA = 794;
	int DOUBLECOLON = 795;
	int COLON = 796;
	int UNDERSCORE = 797;
	int ENDCHAR = 798;
	int SQUAREOPEN = 799;
	int SQUARECLOSE = 800;
	int LPAREN = 801;
	int RPAREN = 802;
	int LESSTHAN = 803;
	int MORETHAN = 804;
	int NOTEQUALS = 805;
	int MOREOREQUAL = 806;
	int LESSOREQUAL = 807;
	int EQUAL = 808;
	int PLUS = 809;
	int MINUS = 810;
	int STAR = 811;
	int SLASH = 812;
	int SINGLEQUOTE = 813;
	int DOUBLEQUOTE = 814;
	int QUESTIONMARK = 815;
	int EXCLAMATIONMARK = 816;
	int SHIFTLEFT = 817;
	int SHIFTRIGHT = 818;
	int ROTATELEFT = 819;
	int ROTATERIGHT = 820;
	int STRINGANYVALUE = 821;
	int STRINGANYOROMIT = 822;
	int QUOTESYMBOL = 823;
	int STRINGOP = 824;
	int Zero = 825;
	int Nonzeronum = 826;
	int Nonzeronumber = 827;
	int Decimalnumber = 828;
	int Num = 829;
	int BIN = 830;
	int HEX = 831;
	int OCT = 832;
	int Char = 833;
	int GeneralTextCharacter = 834;
	int Alphanumeric = 835;
	int Letter = 836;
	int UppercaseLetter = 837;
	int LowercaseLetter = 838;
	int Special = 839;
	int OtherSpecial = 840;
	int OtherCharacter = 841;
	int Space = 842;
	int PreProcessorMacro = 843;
	int SHIFTOP = 844;
}
