package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

public interface ITTCN3EclipseSymbol {
	public String getSymbolDescription();
	public String getSymbolDescriptionPlain();
	public String getSymbolDescriptionContentAssist();
}
