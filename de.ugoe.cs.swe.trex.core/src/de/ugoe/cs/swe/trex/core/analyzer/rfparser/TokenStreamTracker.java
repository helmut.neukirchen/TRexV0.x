package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import java.util.ArrayList;
import java.util.List;

import antlr.Token;
import antlr.TokenStream;
import antlr.TokenStreamException;
import antlr.collections.impl.BitSet;

public class TokenStreamTracker implements TokenStream {
	/** Track the incoming list of tokens */
	protected List<TokenWithIndex> tokens = new ArrayList<TokenWithIndex>();

	/** track index of tokens; first token position is 0 */
	int index = 0;

	/** Who do we suck tokens from? */
	TokenStream stream;

	public TokenStream getStream() {
		return stream;
	}

	/** Which (whitespace) token(s) to throw out */
	protected BitSet discardMask = new BitSet();

	public TokenStreamTracker(TokenStream upstream) {
		stream = upstream;
	}

	public Token nextToken() throws TokenStreamException {
		TokenWithIndex t;
		// suck tokens until end of stream or we find a non-discarded token
		do {
			t = (TokenWithIndex) stream.nextToken();
			if (t != null && t.getType() != Token.EOF_TYPE) {
				t.setIndex(index); // what is t's index in list?
				t.setTracker(this);
				tokens.add(t); // track all tokens
				index++; // move to next position
			}
		} while (t != null && discardMask.member(t.getType()));
		return t;
	}

	public void discard(int ttype) {
		discardMask.add(ttype);
	}

	public Token getToken(int i) {
		return tokens.get(i);
	}

	public int getTokenCount() {
		return tokens.size();
	}

	public boolean isNonDiscardedToken(int i) {
		Token t = tokens.get(i);
		if (discardMask.member(t.getType()))
			return false;
		
		return true;
	}

	public int getTokenStreamSize() {
		return tokens.size();
	}

	@Override
	public String toString() {
		return toString(0, getTokenStreamSize());
	}

	public String toString(int start, int end) {
		StringBuffer buf = new StringBuffer();
		for (int i = start; i >= 0 && i <= end && i < tokens.size(); i++) {
			buf.append(getToken(i).getText());
		}
		return buf.toString();
	}

	public String toDebugString() {
		return toDebugString(0, getTokenStreamSize());
	}

	public String toDebugString(int start, int end) {
		StringBuffer buf = new StringBuffer();
		for (int i = start; i >= 0 && i <= end && i < tokens.size(); i++) {
			buf.append(getToken(i));
		}
		return buf.toString();
	}
}
