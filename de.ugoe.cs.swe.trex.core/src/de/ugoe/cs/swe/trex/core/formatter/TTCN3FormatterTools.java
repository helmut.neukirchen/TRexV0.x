package de.ugoe.cs.swe.trex.core.formatter;

public class TTCN3FormatterTools {

	TTCN3FormatterParameters parameters;

	public TTCN3FormatterTools(TTCN3FormatterParameters p) {
		parameters = p;
	}
	
	// ------------------------------------------------------------------------------------------------------

	public TTCN3FormatterParameters getParameters() {
		return parameters;
	}

	// ------------------------------------------------------------------------------------------------------

	public void setParameters(TTCN3FormatterParameters parameters) {
		this.parameters = parameters;
	}

	// ------------------------------------------------------------------------------------------------------

	public String getSpacesCountString() {
		StringBuffer s = new StringBuffer();
		
		for (int i=0; i < parameters.getSpacesCount(); i++) {
			s.append(" ");
		}
		return s.toString();
	}

	// ------------------------------------------------------------------------------------------------------

	public String getSpacing(int indentationDepth) {
		StringBuffer spacing = new StringBuffer();
		for (int i=0; i < indentationDepth; i++) {
			if (parameters.isTabs()) {
				spacing.append("\t");
			} else {
				spacing.append(getSpacesCountString());
			}
		}
		
		return spacing.toString();
	}

	// ------------------------------------------------------------------------------------------------------

	public String getLines(int lines) {
		StringBuffer newLines = new StringBuffer();
		for (int i=0; i < lines; i++) {
			newLines.append(getNewline());
		}
		return newLines.toString();
	}

	// ------------------------------------------------------------------------------------------------------

	public String getNewline() {
		if (parameters.isUnixNewline()) {
			return "\n";
		}
		
		return "\r\n";
	}

	// ------------------------------------------------------------------------------------------------------

	public String getNewlines(int lineCount) {
		StringBuffer newlines = new StringBuffer();
		for (int i=0; i < lineCount; i++) {
			newlines.append(getNewline());
		}
		return newlines.toString();
	}
	
}

