header {
package de.ugoe.cs.swe.trex.core.analyzer.rfparser;
}

{
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import de.ugoe.cs.swe.trex.core.TTCN3CorePlugin;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.*;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration.*;
import de.ugoe.cs.swe.trex.core.misc.IntRangeMap;

@SuppressWarnings("unused")
}

class TTCN3SymbolTableTreeParser extends TreeParser;

options
{
  importVocab = TTCN3Parser;
  defaultErrorHandler=true;
}

{
	ScopeStack scopeStack = new ScopeStack();
	ScopeStack preparedScopeStack = new ScopeStack();
	Stack<String> groupStack = new Stack<String>();
	List<ScopeWithKnownName> knownScopes = new ArrayList<ScopeWithKnownName>();
	List<String> modules = new ArrayList<String>();
	Map<String, Scope> moduleScopeMap = new HashMap<String, Scope>();
	IntRangeMap<Scope> scopeOffsetMap = new IntRangeMap<Scope>();
	String filename = null;
	Scope fileScope = null;
	Symbol tempSymbol = null;
	private ArrayList<RecognitionException> exceptions = new ArrayList<RecognitionException>();

/*	//TODO: consider implementing a check during parsing  
	private ArrayList<Symbol> unresolvableActualParameterSymbols = new ArrayList<Symbol>();
	
	private void addUnresolvableActualParameterSymbol(Symbol s){
		unresolvableActualParameterSymbols.add(s);
	}
	
	private ArrayList<Symbol> getUnresolvableActualParameterSymbols() {
		return unresolvableActualParameterSymbols;
	}
	
	private void resolveActualParameterSymbols(){
		//This may be usable to check the parameters during parsing if necessary
		
		//TODO: move to resolve() in SymbolTable
		for (Symbol s : getUnresolvableActualParameterSymbols()){
			LocationAST instanceNode = LocationAST.resolveParentsUntilType(s.getDeclarationNode(), new int[]{TTCN3ParserTokenTypes.FunctionInstance, TTCN3ParserTokenTypes.AltstepInstance, TTCN3ParserTokenTypes.TestcaseInstance});
			LocationAST identifierNode = (LocationAST) ASTUtil.findChild(instanceNode, TTCN3ParserTokenTypes.Identifier);
			Symbol instanceSymbol = identifierNode.getFirstChild().getSymbol();
//			System.out.println(identifierNode.getFirstChild().getSymbol());
			
			if (instanceSymbol != null && instanceNode != instanceSymbol.getDeclarationNode()){
				//System.out.println(s + " : " +instanceSymbol);
				//System.out.println(instanceSymbol.getDeclarationNode());
				LocationAST defNode = LocationAST.resolveParentsUntilType(instanceSymbol.getDeclarationNode(), new int[]{TTCN3ParserTokenTypes.FunctionDef, TTCN3ParserTokenTypes.AltstepDef, TTCN3ParserTokenTypes.TestcaseDef});
				Symbol targetSymbol = defNode.getScope().getSymbolTable().resolve(s.getName());
				if (targetSymbol != null && (targetSymbol instanceof FormalParameterSymbol)){
					s = targetSymbol;
					//System.out.println(s);
				}
			}
		}
	}
*/	
	
	public void setFilename(String name) {
		filename = name;
	}

	public String getFilename() {
		return filename;
	}

	public IntRangeMap<Scope> getScopeOffsetMap() {
		return scopeOffsetMap;
	}

	public Map<String, Scope> getModuleScopeMap() {
		return moduleScopeMap;
	}

	public List<String> getModules() {
		return modules;
	}

	private Scope pushNewScope(boolean searchParent) {
		Scope newScope;
		if ((scopeStack.empty())) {
			newScope = new Scope();
			newScope.setAssociatedFilename(filename);
		} else {
			newScope = new Scope(scopeStack.peek(), searchParent);
			newScope.setAssociatedFilename(scopeStack.peek().getAssociatedFilename());
			scopeStack.peek().addChild(newScope);
			newScope.setScopeLevel(scopeStack.peek().getScopeLevel()+1);
		}

		scopeStack.push(newScope); 
		
		return newScope;
	}
	
	private void pushPreparedScope() {
		if (!(scopeStack.empty())) {
			preparedScopeStack.peek().setScopeLevel(scopeStack.peek().getScopeLevel()+1);
		}
		scopeStack.push(preparedScopeStack.pop());
	}
	
	private Scope prepareNewScope(boolean searchParent) {
		Scope newScope;
		
		if ((scopeStack.empty())) {
			newScope = new Scope();
			newScope.setAssociatedFilename(filename);
		} else {
			newScope = new Scope(scopeStack.peek(), searchParent);
			newScope.setAssociatedFilename(scopeStack.peek().getAssociatedFilename());
			scopeStack.peek().addChild(newScope);		
		}
		preparedScopeStack.push(newScope);
		return newScope;
	}
	
	private void addSymbolToScope(Symbol s) {
		if (scopeStack.empty())
			return;
		s.setSurroundingScope(scopeStack.peek());
		scopeStack.peek().addSymbol(s);
	}
	
	public void connectKnownScopes() {
//		long time = System.currentTimeMillis();
//		TTCN3UIPlugin.debugMessage("Connecting known scopes...");

		for	(ScopeWithKnownName scopeName : knownScopes) {	
			Scope scope = scopeName.getSearchScope();
			Symbol symbol = scope.resolve(scopeName.getScopeName());
			if (symbol != null) {
				if (scopeName.isSystemScope()) {
					symbol.getScope().setSystemScope(true);
				}
				scopeName.getTargetScope().addKnownScope(symbol.getScope());
			}
		}
		//resolveActualParameterSymbols();
//		TTCN3UIPlugin.debugMessageTimer("...finished in " +  new Long(System.currentTimeMillis()-time) + " ms");
	}
	
	private void addScopeToOffsetMap() {
		if (scopeStack.empty())
			return;

		if ((scopeStack.peek().getStartOffset() == -1) || (scopeStack.peek().getEndOffset() == -1))
			return;

		int startOffset = scopeStack.peek().getStartOffset();
		int endOffset = scopeStack.peek().getEndOffset();
		scopeOffsetMap.put(startOffset, endOffset, scopeStack.peek());
	}
	
	private void connectToScope(AST node) {
		if (!(node instanceof LocationAST)) 
			return;

		LocationAST ast = (LocationAST) node;
		
		ast.setScope(scopeStack.peek());
		updateScopeOffset(node);
	}

	private void updateScopeOffset(AST node) {
		if ((node == null) || (!(node instanceof LocationAST))) 
			return;

		LocationAST ast = (LocationAST) node;

		Scope scope = scopeStack.peek();
		
		if (scopeStack.empty())
			return;
		
		if (scope.getStartOffset() == -1) {
			if (ast.getOffset() == -1) {
				TTCN3CorePlugin.debugMessage("[updatescopeoffset] ERROR! AST Node with missing offsets!");
				TTCN3CorePlugin.debugMessage("[updatescopeoffset] node: " + ast);
			}
			scope.setStartOffset(ast.getOffset());
		}
		
		if (scope.getEndOffset() == -1) {
			if (ast.getOffset() == -1) {
				TTCN3CorePlugin.debugMessage("[updatescopeoffset] ERROR! AST Node with missing offsets!");
				TTCN3CorePlugin.debugMessage("[updatescopeoffset] node: " + ast);
			}
			scope.setEndOffset(ast.getEndOffset());
		}
		
		if ((ast.getOffset() == -1) || (scope.getStartOffset() == -1)) {
			TTCN3CorePlugin.debugMessage("[updatescopeoffset] ERROR! AST Node with missing offsets!");
			TTCN3CorePlugin.debugMessage("[updatescopeoffset] node: " + ast);
			return;
		}
		
		
		if (ast.getOffset() < scope.getStartOffset()) {
			scope.setStartOffset(ast.getOffset());
		}
		
		if ((ast.getEndOffset() == -1) || (scope.getEndOffset() == -1)) {
			TTCN3CorePlugin.debugMessage("[updatescopeoffset] ERROR! AST Node with missing offsets!");
			TTCN3CorePlugin.debugMessage("[updatescopeoffset] node: " + ast);
			return;
		}
		
		if (ast.getEndOffset() > scope.getEndOffset() ) {
			scope.setEndOffset(ast.getEndOffset());
		}

	}
	
	private void popToScope(Scope t) {
		if (scopeStack.empty())
			return;
		while (scopeStack.peek() != t) {
			scopeStack.peek().setEndOffset(t.getEndOffset());
			scopeStack.pop();
		}
	}
	
	private void passEndOffsetToScopeChildren(Scope s, int offset) {
		s.setEndOffset(offset);
		for(Scope scope : s.getChildren()) {
			passEndOffsetToScopeChildren(scope, offset);
		}
	}

	private void passEndOffsetToScopeChildren() {
		passEndOffsetToScopeChildren(scopeStack.peek(), scopeStack.peek().getEndOffset());
	}
	
	public Scope getFileScope() {
		return fileScope;
	}
	
	public void reportError(RecognitionException ex) {
    	exceptions.add(ex);
    }
	
	public ArrayList<RecognitionException> getExceptions() {
		return exceptions;
	}
	
	private void setImportVisibility(AST node, AbstractImport imp){
		LocationAST moduleDefinitionNode = LocationAST.resolveParentsUntilType((LocationAST) node, TTCN3ParserTokenTypes.ModuleDefinition);
		
		if (moduleDefinitionNode == null){
			return;
		}
		
		if (moduleDefinitionNode.getFirstChild().getType()==TTCN3ParserTokenTypes.Visibility){
			switch (moduleDefinitionNode.getFirstChild().getFirstChild().getType()) {
			case TTCN3ParserTokenTypes.PUBLIC:
				imp.setPublic();
				break;
			case TTCN3ParserTokenTypes.PRIVATE:
				imp.setPrivate();
				break;
			case TTCN3ParserTokenTypes.FRIEND:
				imp.setFriend();
				break;

			default:
				break;
			}
		}
		
	}
	
	private void setVisibility(Symbol s){
		if (s==null){
			//TODO: seems to be an issue with record: when there is no identifier
			return;	
		}
		LocationAST declarationNode = s.getDeclarationNode(); 
		if (declarationNode == null){
			return;
		}
		
		LocationAST moduleDefinitionNode = LocationAST.resolveParentsUntilType(declarationNode, TTCN3ParserTokenTypes.ModuleDefinition);
		
		if (moduleDefinitionNode == null){
			return;
		}
		
		if (moduleDefinitionNode.getFirstChild().getType()==TTCN3ParserTokenTypes.Visibility){
			switch (moduleDefinitionNode.getFirstChild().getFirstChild().getType()) {
			case TTCN3ParserTokenTypes.PUBLIC:
				s.setPublic();
				break;
			case TTCN3ParserTokenTypes.PRIVATE:
				s.setPrivate();
				break;
			case TTCN3ParserTokenTypes.FRIEND:
				s.setFriend();
				break;

			default:
				break;
			}
		}
	}
		
}


//Proposed fix for empty files by Gustaf Johansson
pr_TTCN3File {fileScope = pushNewScope(false);}:
    #(TTCN3File
        {
            ((LocationAST)#TTCN3File).setScope(fileScope);
        } 
        (
            EOF 
            |
            (
                (nodea:pr_TTCN3Module {connectToScope(nodea);})*
            )
        ) 
    )
{
    fileScope.setStartOffset(0);
//  addScopeToOffsetMap();
    scopeStack.pop();
}
;


// =============================================================================================

pr_TTCN3Module {ModuleSymbol symbol = new ModuleSymbol(); Symbol a; }:
	#(n:TTCN3Module 
		{prepareNewScope(false);}
		a=id:pr_TTCN3ModuleId 
		{ 
			symbol.setName(a.getName());
			symbol.setDeclarationNode(a.getDeclarationNode());
			modules.add(a.getName());
			moduleScopeMap.put(a.getName(), preparedScopeStack.peek());
			symbol.setScope(a.getScope());
			addSymbolToScope(symbol);
			preparedScopeStack.peek().setScopeSymbol(symbol);
			ImportConfiguration importConf = new ImportConfiguration();
			importConf.setModuleName(a.getName());
			preparedScopeStack.peek().setImportConfiguration(importConf);

			pushPreparedScope();
			((LocationAST)#n).setScope(scopeStack.peek());
		} 
		( nodea:pr_ModuleDefinitionsPart {connectToScope(nodea);} )? 
		( nodeb:pr_ModuleControlPart {connectToScope(nodeb);})? 
		( nodec:pr_WithStatement {connectToScope(nodec);})? 
		{
			scopeStack.peek().setStartOffset(((LocationAST)#id).getEndOffset());
			scopeStack.peek().setEndOffset(((LocationAST)#n).getEndOffset());
			addScopeToOffsetMap();
			scopeStack.pop();
		}
		(SemiColon)?
	)
;

pr_ModuleId returns [Symbol s=null] {String a;}:
	#(ModuleId 
		(
			s=pr_GlobalModuleId 
			( a=pr_LanguageSpec )?
		) 
	)
;

pr_TTCN3ModuleId returns [Symbol s=null]:
	#(TTCN3ModuleId s=pr_ModuleId)
;

pr_DefinitiveObjIdComponent :
	pr_NameForm
	| pr_DefinitiveNumberForm
	| pr_DefinitiveNameAndNumberForm
;

pr_Number  :
	#(Number a:NUMBER )
;
 

pr_DefinitiveNumberForm :
	pr_Number
;


pr_DefinitiveNameAndNumberForm {Symbol a;} :
	#(DefinitiveNameAndNumberForm a=pr_Identifier pr_DefinitiveNumberForm )
;


pr_ModulePar {TypeSymbol typeSymbol = new TypeSymbol();}:
	#(ModulePar 
		pr_Type[typeSymbol] 
		pr_ModuleParList[typeSymbol] 
	) 
;


pr_ModuleParList[TypeSymbol typeSymbol] {Symbol a;}:
	#(ModuleParList 
		(
			a=pr_Identifier
			{
				//fixes issue with module parameters being assigned incorrect symbols
				ModuleParameterSymbol symbol = new ModuleParameterSymbol();
				symbol.setName(a.getName());
				symbol.setDeclarationNode(a.getDeclarationNode());

//TODO: This appears redundant
//				if ((typeSymbol != null) && (typeSymbol.getPredefinedType() != null)) {
//					symbol.setPredefinedType(typeSymbol.getPredefinedType());
//				}
				symbol.setReferencedType(typeSymbol.getReferencedType());
				symbol.setPredefinedType(typeSymbol.getPredefinedType());
				setVisibility(symbol);
				addSymbolToScope(symbol);
				//tempSymbol = symbol;
			}
			( pr_ConstantExpression  )? 
		)+ 
	)
;

/************************* 1.6.1 - Module Definitions Part *********************/


pr_ModuleDefinitionsPart :
	#( ModuleDefinitionsPart 
		#(ModuleDefinitionList 
			( 
				(pr_ModuleDefinition  (SemiColon )?)
			)+ 
		) 
	)
;

pr_ModuleDefinition  :
	#(ModuleDefinition 
		( pr_Visibility
		)?
		(
		pr_TypeDef 
		| pr_ConstDef 
		| pr_TemplateDef 
		| pr_FunctionDef 
		| pr_SignatureDef 
		| pr_TestcaseDef 
		| pr_ModuleParDef 
		| pr_ImportDef 
		| pr_GroupDef 
		| pr_ExtFunctionDef 
		| pr_ExtConstDef 
		| pr_AltstepDef
		| pr_FriendModuleDef
		)
		( pr_WithStatement )? 
	) 
;

pr_Visibility:
	#(Visibility
		( PUBLIC
		| PRIVATE
		| FRIEND
		)
	)
;

pr_FriendModuleDef{Symbol s = null;}:
	#(FriendModuleDef
		(	s=pr_Identifier
		{	ModuleSymbol currentModuleSymbol = (ModuleSymbol) s.getSurroundingScope().getModuleScope().getScopeSymbol();
			currentModuleSymbol.addFriendModule(s.getName());
		}
		)+
	)
;

/***** 1.6.1.1 - Typedef Definitions *****/

pr_TypeDef :
	#(TypeDef
    	( pr_StructuredTypeDef  | pr_SubTypeDef )
	) 
;

pr_StructuredTypeDef 
{ 
	TypeSymbol symbol = new TypeSymbol(TypeSymbol.TypeEnum.structuredType);
	Symbol a=null;
}:
	#(StructuredTypeDef
		(
			a=pr_RecordDef {symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_record);}
			| a=pr_UnionDef {symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_union);}
			| a=pr_SetDef {symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_set);}
			| a=pr_RecordOfDef 
			  {
			  	symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_recordof);
			  	if (a instanceof TypeSymbol) {
				  	symbol.setReferencedType(((TypeSymbol) a).getReferencedType());
			  	}
			  }
			| a=pr_SetOfDef 
			  {
			  	symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_setof); 
			  	if (a instanceof TypeSymbol) {
				  	symbol.setReferencedType(((TypeSymbol) a).getReferencedType());
			  	}
			  }
			| a=pr_EnumDef {symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_enumerated);}
			| a=pr_PortDef {symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_port);}
			| a=pr_ComponentDef {symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_component); }
		)
	)
	{ 
		symbol.setName(a.getName());
		symbol.setScope(a.getScope());
		symbol.setDeclarationNode(a.getDeclarationNode());
		symbol.setGroupName(a.getGroupName());
		setVisibility(symbol);
		addSymbolToScope(symbol); 
		if (a.getScope() != null)
			a.getScope().setScopeSymbol(symbol);
	}
;

pr_RecordDef returns [Symbol s = new Symbol();] :
  #( RecordDef  s=pr_StructDefBody )
;

pr_StructDefBody returns [Symbol s = new Symbol();]:
	#(n:StructDefBody 
		(
			{ prepareNewScope(true); }
			((s=a:pr_Identifier  ( pr_StructDefFormalParList )?  | c:AddressKeyword )  	
			{ 
				pushPreparedScope(); 
				if (a != null) {
					scopeStack.peek().setStartOffset(((LocationAST)#a).getEndOffset());
				} else if (c != null) {
					scopeStack.peek().setStartOffset(((LocationAST)#c).getEndOffset());
				}
			}
			( nodea:pr_StructFieldDef {connectToScope(nodea);} )*)
			{
				scopeStack.peek().setEndOffset(((LocationAST)#n).getEndOffset());
//				addScopeToOffsetMap();
				scopeStack.pop();
			}
		)
	)
;


pr_StructDefFormalParList  :
  //#( StructDefFormalParList ( pr_StructDefFormalPar )* ) //Fixed : no explicit StructDefFormalParList node is pressent
  pr_StructDefFormalPar ( pr_StructDefFormalPar )* 
;


pr_StructDefFormalPar :
    pr_FormalValuePar
;

pr_StructFieldDef {Symbol a; TypeSymbol symbol = new TypeSymbol();}:
	#( StructFieldDef 
    	(
    		( pr_Type[symbol] | pr_NestedTypeDef ) 
    		a=pr_Identifier 
    		( pr_ArrayDef )? 
    		( pr_SubTypeSpec  )? 
    		( OPTIONAL )? 
    	)
    	{ 
    		symbol.setName(a.getName()); 
			  symbol.setDeclarationNode(a.getDeclarationNode());   		
    	}
		{ addSymbolToScope(symbol); }
	)
;

pr_NestedTypeDef :
	{ pushNewScope(false); }
	#(NestedTypeDef
		(
			nodea:pr_NestedRecordDef {connectToScope(nodea);} |
			nodeb:pr_NestedUnionDef {connectToScope(nodeb);} |
			nodec:pr_NestedSetDef {connectToScope(nodec);} |
			noded:pr_NestedRecordOfDef {connectToScope(noded);} |
			nodee:pr_NestedSetOfDef {connectToScope(nodee);} |
			nodef:pr_NestedEnumDef {connectToScope(nodef);}
		)
	)
	{ 
//		addScopeToOffsetMap();
		scopeStack.pop(); 
	}
;                      
                   
pr_NestedRecordDef  :
 #(NestedRecordDef  ( (pr_StructFieldDef  ( pr_StructFieldDef )* )?
  ))
;
 
pr_NestedUnionDef  :
#(NestedUnionDef ( 
 pr_UnionFieldDef  ( pr_UnionFieldDef )* ))
 ;
 
pr_NestedSetDef  :
#(NestedSetDef(
 (pr_StructFieldDef ( pr_StructFieldDef )*)?)) 
 ;
 
pr_NestedRecordOfDef  :
	#(NestedRecordOfDef(  
		(pr_StringLength )?  (pr_Type[null]  | pr_NestedTypeDef ))) 
;

pr_NestedSetOfDef  : 
#(NestedSetOfDef(  
 (pr_StringLength )?  (pr_Type[null] | pr_NestedTypeDef )))
;

pr_NestedEnumDef  :
 #(NestedEnumDef 
	 pr_EnumerationList 
  )              
;

pr_UnionDef returns [Symbol s=new Symbol()]:
	#( UnionDef 
		#(n:UnionDefBody
		{ prepareNewScope(true); }
		(  s=a:pr_Identifier  ( pr_StructDefFormalParList  )? |  b:AddressKeyword  )  
		//{ pushNewScope(true); }
		{ pushPreparedScope(); }
		( nodea:pr_UnionFieldDef {connectToScope(nodea);})*  
		{
			if (a != null)
				scopeStack.peek().setStartOffset(((LocationAST)#a).getEndOffset()); 
			else if (b != null)
				scopeStack.peek().setStartOffset(((LocationAST)#b).getEndOffset()); 

			scopeStack.peek().setEndOffset(((LocationAST)#n).getEndOffset()); 
			addScopeToOffsetMap();
			scopeStack.pop(); 
		}
		)
	)
;


pr_UnionFieldDef {Symbol a; TypeSymbol symbol = new TypeSymbol();}:
	#( UnionFieldDef 
		(pr_Type[symbol] | pr_NestedTypeDef ) 
		a=pr_Identifier  
		( pr_ArrayDef )? 
		( pr_SubTypeSpec )? 
	)
	{ 
		symbol.setName(a.getName()); 
		symbol.setDeclarationNode(a.getDeclarationNode());
	}
	{ addSymbolToScope(symbol); }
	
;

pr_SetDef returns [Symbol s=null] :
  #( SetDef s=pr_StructDefBody )
;


pr_RecordOfDef returns [TypeSymbol s=null]:
	#( RecordOfDef 
		( pr_StringLength )? 
		s=pr_StructOfDefBody 
	)
;


pr_StructOfDefBody returns [TypeSymbol s = new TypeSymbol();] {Symbol a = null; }:
	#(StructOfDefBody
		(
			(pr_Type[s] |pr_NestedTypeDef )
			( a=pr_Identifier | AddressKeyword )
			( pr_SubTypeSpec )?
			{
				if (a != null) {
		    		s.setName(a.getName()); 
					s.setDeclarationNode(a.getDeclarationNode());
				}  
			}
		)
	)
;




pr_SetOfDef returns [TypeSymbol s=null] :
	#( SetOfDef 
		( pr_StringLength )? 
		s=pr_StructOfDefBody 
	)
;


pr_EnumDef returns [Symbol s=new Symbol()]:
	#(n:EnumDef 
		{
			prepareNewScope(false);
		}
		(s=a:pr_Identifier | b:AddressKeyword )
		{ 
			pushPreparedScope(); 
		}
		nodea:pr_EnumerationList {connectToScope(nodea);}
		{
			if (a != null)
				scopeStack.peek().setStartOffset(((LocationAST)#a).getEndOffset());
			else if (b!= null)
				scopeStack.peek().setStartOffset(((LocationAST)#b).getEndOffset());

			scopeStack.peek().setEndOffset(((LocationAST)#n).getEndOffset());

//			addScopeToOffsetMap();
			scopeStack.pop();
		}
	)

;
pr_EnumerationList  :
//	{ pushNewScope(false); } 
	#(EnumerationList 
		pr_Enumeration (pr_Enumeration)* 
	)
//	{ scopeStack.pop(); }
;

pr_Enumeration {Symbol a; EnumSymbol symbol = new EnumSymbol();}:
	#(Enumeration 
		a=pr_Identifier ( (Minus)? pr_Number )? 
	)
	{
		symbol.setName(a.getName());
		symbol.setDeclarationNode(a.getDeclarationNode());
	}
	{addSymbolToScope(symbol);}
;

pr_SubTypeDef { TypeSymbol symbol = new TypeSymbol(); Symbol a=null;} :
	#( SubTypeDef
		pr_Type[symbol]
		( 
			a=pr_Identifier 
			{ 
				symbol.setName(a.getName()); 
				symbol.setGroupName(a.getGroupName()); 
				symbol.setDeclarationNode(a.getDeclarationNode());
			} 
			| 
			b:AddressKeyword 
			{
				symbol.setName("address");
				symbol.setScope(scopeStack.peek());
				symbol.setSurroundingScope(scopeStack.peek());
				((LocationAST)#b).setScope(scopeStack.peek());
				((LocationAST)#b).setText("address");
				symbol.setDeclarationNode((LocationAST) #b);
			} 
		) 
		( pr_ArrayDef )? 
		( pr_SubTypeSpec )?
		{ setVisibility(symbol);
		  addSymbolToScope(symbol); 
		}
	)
;


pr_SubTypeSpec  :
  #(SubTypeSpec ( (pr_AllowedValues  (pr_StringLength  )? ) | pr_StringLength  ) )
;


pr_AllowedValues  :
  #( AllowedValues (( pr_ValueOrRange )+ | (pr_CharStringMatch )  ))
;

pr_CharStringMatch  :
	#(CharStringMatch 
		(	a:CSTRING 
		  | pr_ReferencedValue
		)+
	)
;

pr_ValueOrRange :
	#( 	ValueOrRange 
		(	pr_RangeDef
		  | pr_ConstantExpression
		  | pr_Type[null] //TODO: resolve symbol properly
		)
	)
;


pr_RangeDef  :
  #( RangeDef pr_LowerBound pr_UpperBound ) 
;


pr_StringLength  :

  #( StringLength
     pr_SingleConstExpression  ( pr_UpperBound )? )

;


pr_PortType {Symbol a,b;}:
  #( PortType
     ( ( pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId )? b=pr_Identifier )
;


pr_PortDef returns [Symbol s=null]:
	#(PortDef 
		{ prepareNewScope(true); }
		s=pr_Identifier  
		{ pushPreparedScope();}
		nodea:pr_PortDefAttribs {connectToScope(nodea);}
		{ 
//			addScopeToOffsetMap();
			scopeStack.pop(); 
		}
	)
;


pr_PortDefAttribs :
	#(PortDefAttribs 
		(
			pr_MessageAttribs
			| pr_ProcedureAttribs
			| pr_MixedAttribs
		)
	)
;


pr_MessageAttribs:
	#(MessageAttribs 
		( (pr_AddressDecl | pr_MessageList | pr_ConfigParamDef) (SemiColon )? )* 
	)
;

pr_AddressDecl:
	#(AddressDecl 
		pr_Type[null]
	)
;
//TODO: check if scoping and symbol extensions are needed
pr_ConfigParamDef:
	#(ConfigParamDef
		(	pr_MapParamDef 
		  | pr_UnmapParamDef
		)
	)
;

pr_MapParamDef:
	#(MapParamDef
		(pr_FormalValuePar)*
	)
;

pr_UnmapParamDef:
	#(UnmapParamDef
		(pr_FormalValuePar)*
	)
;


pr_MessageList:
	#(MessageList
		pr_Direction  
		( AllKeyword  | ( pr_Type[null]  )* ) 
	)
;

pr_Direction :
	#(Direction
		(
			InParKeyword 
			| OutParKeyword 
			| InOutParKeyword 
		)
	)
;


pr_ProcedureAttribs  :
	#( ProcedureAttribs 
		( (pr_AddressDecl | pr_ProcedureList | pr_ConfigParamDef) (SemiColon )? )* 
	)
;

pr_ProcedureList  :
	#(ProcedureList
		pr_Direction 
		( AllKeyword  | pr_SignatureList  ) 
	) 
;

pr_SignatureList  :
	#(SignatureList pr_Signature  (pr_Signature )* )
;

pr_MixedAttribs  :
	#(MixedAttribs 
		( (pr_AddressDecl | pr_MixedList | pr_ConfigParamDef) (SemiColon )? )* 
	)
;

pr_MixedList  :
	#(MixedList
		pr_Direction  
		( AllKeyword  | ( pr_ProcOrType )* ) 
	)
;

pr_ProcOrType :
	#(ProcOrType
		(pr_Type[null] | pr_Signature)
	)
;

pr_ComponentDef returns [Symbol s=null] {Reference r;}: 
	#(n:ComponentDef
		{ 
			prepareNewScope(true); 
		}
		s=id:pr_Identifier 
		
		( 
			r=pr_ComponentType 
			{
		 		if (r != null) {
		 			ScopeWithKnownName scopeWithName = new ScopeWithKnownName(scopeStack.peek(), preparedScopeStack.peek(), r.getIdentifier().getName(), false);
		 			knownScopes.add(scopeWithName);
		 		}
			}
			(
				r=pr_ComponentType
				{
			 		if (r != null) {
			 			ScopeWithKnownName scopeWithName = new ScopeWithKnownName(scopeStack.peek(), preparedScopeStack.peek(), r.getIdentifier().getName(), false);
			 			knownScopes.add(scopeWithName);
			 		}
				}
			)*
		)? // extends...
		
		{ 
			preparedScopeStack.peek().setScopeSymbol(s);
			pushPreparedScope(); 
			scopeStack.peek().setStartOffset(((LocationAST)#id).getEndOffset());
			((LocationAST)#n).setScope(scopeStack.peek());
		}
		( 	nodea:pr_ComponentElementDef {connectToScope(nodea);} 
			( 	nodec:pr_WithStatement {connectToScope(nodec);}
			)? 
			( 	nodeb:SemiColon {connectToScope(nodeb);} 
			)? 
		)* 
		{ 
			scopeStack.peek().setEndOffset(((LocationAST)#n).getEndOffset());
//			addScopeToOffsetMap();
			scopeStack.pop(); 
		}
	) 
;

pr_ComponentType returns [Reference r=new Reference();] {Symbol a,b; } :
	#(ComponentType
		( (pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId {r.setModule(a);} )? 
		b=pr_Identifier 
		{r.setIdentifier(b);}
	)
;

pr_ComponentElementDef :
	#(ComponentElementDef
		(
			pr_PortInstance
			| pr_VarInstance
			| pr_TimerInstance
			| pr_ConstDef
		)
	)
;

pr_PortInstance  :
	#(PortInstance 
		pr_PortType  
		( pr_PortElement  )* 
	)
;

pr_PortElement {Symbol a; PortInstanceSymbol symbol = new PortInstanceSymbol();}:
	#(PortElement
		a=pr_Identifier 
		{
			symbol.setName(a.getName()); 
			symbol.setDeclarationNode(a.getDeclarationNode());
		}
		{ addSymbolToScope(symbol); }
		( pr_ArrayDef )? 
	)
;

/***** 1.6.1.2 - Constant Definitions  *****/

pr_ConstDef {ConstantSymbol symbol = new ConstantSymbol();}: 
	#(ConstDef
		pr_Type[symbol] 
		(	pr_SingleConstDef[symbol] 
		)*
	)
;


pr_SingleConstDef[Symbol typeSymbol] {Symbol a; ConstantSymbol symbol = new ConstantSymbol();}: 
	#(SingleConstDef
		a=pr_Identifier  
		{
			symbol.setName(a.getName()); 
			symbol.setPredefinedType(((ConstantSymbol)typeSymbol).getPredefinedType()); 
			symbol.setReferencedType(((ConstantSymbol)typeSymbol).getReferencedType());
			symbol.setDeclarationNode(a.getDeclarationNode());
			setVisibility(symbol);
			addSymbolToScope(symbol);
			tempSymbol = symbol;
		}
		( pr_ArrayDef  )? 
		pr_ConstantExpression  
	)
;


/***** 1.6.1.3 - Template Definitions  *****/

pr_TemplateDef:
	#(upperx:TemplateDef 
		{ prepareNewScope(true); }
		(	pr_TemplateRestriction //before or after the above statement?
		)?
		{((LocationAST)#upperx).setScope(scopeStack.peek()); }
		pr_BaseTemplate  
		( pr_DerivedDef )?  
		nodea:pr_TemplateBody {connectToScope(nodea);}
		{ 	
		//	popToScope(scopeToPop); 
		 	passEndOffsetToScopeChildren(); 
			
//			addScopeToOffsetMap();
			scopeStack.pop(); 
		}
	)
;

pr_BaseTemplate {Symbol a; TemplateSymbol symbol = new TemplateSymbol(); TypeSymbol typeSymb = new TypeSymbol();}:
	#(upperx: BaseTemplate 
		( pr_Type[typeSymb]  | pr_Signature ) 
		
		a=pr_Identifier
		{ 
			symbol.setName(a.getName());
			symbol.setDeclarationNode(a.getDeclarationNode());
			symbol.setScope(a.getScope());
			symbol.setGroupName(a.getGroupName());
			symbol.setReferencedType(typeSymb.getReferencedType());
			symbol.setPredefinedType(typeSymb.getPredefinedType());
			setVisibility(symbol);
			addSymbolToScope(symbol);
			preparedScopeStack.peek().setScopeSymbol(symbol);
			tempSymbol = symbol;
		}		
		
		{ 
			if ((typeSymb.getReferencedType() != null) && (!(preparedScopeStack.empty()))) {
				ScopeWithKnownName scopeWithName = new ScopeWithKnownName(scopeStack.peek(),
					preparedScopeStack.peek(), typeSymb.getReferencedType().getIdentifier().getName(), false);
				knownScopes.add(scopeWithName);
			}
		}
		{ 				
			pushPreparedScope(); 
			((LocationAST)#upperx).setScope(scopeStack.peek());
		}
		( nodex:pr_TemplateFormalParList {connectToScope(nodex);} )? 
	)
;



pr_DerivedDef {Symbol a,b;}:
	#( DerivedDef 
		(a=pr_GlobalModuleId)? 
		b=pr_Identifier 
	)
;

//New Entries

//shortcuts, valid?
//pr_TemplateRestriction  :
//	a:TemplateRestriction //"a"needed?
//;

//pr_RestrictedTemplate  :
//	a:RestrictedTemplate //"a"needed?
//;

pr_TemplateRestriction  :
	#(TemplateRestriction
		(	OMIT
		  | VALUE
		  | PRESENT
		)
	)
;

pr_RestrictedTemplate  :
	#(RestrictedTemplate
		(	OMIT
		  | (	TemplateKeyword
		  		pr_TemplateRestriction
		  	)
		)
	)
;

//---

pr_TemplateFormalParList  :
	pr_TemplateFormalPar  ( pr_TemplateFormalPar  )* 
;


pr_TemplateFormalPar :
    pr_FormalValuePar | pr_FormalTemplatePar
;


pr_TemplateBody :
	#( TemplateBody (
		pr_SimpleSpec
		| pr_FieldSpecList
		| pr_ArrayValueOrAttrib) ( pr_ExtraMatchingAttributes )?
	)
;


pr_SimpleSpec :
  pr_SingleValueOrAttrib
/*	(	(	pr_SingleExpression
			(	pr_SingleTemplateSpec
			)?
		)
	  | (	pr_SingleTemplateSpec
	  	)
	)
/**/	
;

pr_SingleTemplateSpec :
	pr_SingleTemplateExpression
;

pr_SingleTemplateExpression{List<Symbol> b;}:
	(	(	pr_MatchingSymbol
		  | (	pr_TemplateRefWithParList
		  		(	b=pr_ExtendedFieldReference
		  		)?
		  	)
		)
		(	pr_SimpleSpec
		)? 
	)
;

pr_FieldSpecList  :
  #( FieldSpecList 
     ( pr_FieldSpec )* 
   ) 
;

pr_FieldSpec  :
  #( FieldSpec
     pr_FieldReference 
     (	pr_TemplateBody
      | Minus
     )   
  )
;


pr_FieldReference  :
 #(FieldReference
 (
    (pr_StructFieldRef) => pr_StructFieldRef
  | pr_ArrayOrBitRef
  | (pr_ParRef )=>pr_ParRef
))
;


pr_StructFieldRef {TypeSymbol.PredefinedTypeEnum a; Symbol b; }:
  a=pr_PredefinedType | b=pr_Identifier
;


pr_ParRef  :
  pr_SignatureParIdentifier
;


pr_SignatureParIdentifier {Symbol a;}:
	a=pr_Identifier
;


pr_ArrayOrBitRef  :
  #( ArrayOrBitRef pr_SingleExpression )
;


pr_SingleValueOrAttrib  :
#(SingleValueOrAttrib
(
    ( (pr_GlobalModuleId)? pr_Identifier ) => pr_TemplateRefWithParList
  | pr_MatchingSymbol
  | pr_SingleExpression
)
( pr_SimpleSpec
)?
)
;


pr_ArrayValueOrAttrib  :
	#( ArrayValueOrAttrib 
		( pr_ArrayElementSpec  )* 
	)
;


pr_ArrayElementSpec  :
#(ArrayElementSpec
	(NotUsedSymbol  | pr_TemplateBody | pr_PermutationMatch)
 )
;

pr_PermutationMatch  :
	#(PermutationMatch pr_ListOfTemplates )
;

/*
pr_PermutationList  :
	pr_TemplateBody  (pr_TemplateBody )* 
;
*/

pr_MatchingSymbol  :
 #(MatchingSymbol
 ( 
    pr_Complement
  | (	AnyValue
  		(	pr_WildcardLengthMatch
  		)?
  	) 
  | (	AnyOrOmit
  		(	pr_WildcardLengthMatch
  		)?
  	) 
  | pr_ListOfTemplates
  | pr_Range
  | (pr_BitStringMatch)=> pr_BitStringMatch
  | pr_HexStringMatch
  | pr_OctetStringMatch
  | pr_CharStringMatch
  | (pr_SubsetMatch)=> pr_SubsetMatch
  | (pr_SupersetMatch) => pr_SupersetMatch 

))
;

pr_ListOfTemplates  :
	#(ListOfTemplates
		(
			pr_TemplateListItem (pr_TemplateListItem )*
		)
	)
;

pr_TemplateListItem  :
	(
		pr_TemplateBody 
	  | pr_AllElementsFrom
	)
;

pr_AllElementsFrom  :
	#(AllElementsFrom
		(
			pr_TemplateBody
		)
	)
;


pr_ValueOrAttribList  :
	#(ValueOrAttribList
		(
			pr_TemplateBody (pr_TemplateBody )*
		)
	)
;


pr_ExtraMatchingAttributes  :
	#(ExtraMatchingAttributes 
		(
			(pr_LengthMatch  (IfPresentMatch )? ) | IfPresentMatch  
		)
	)
;

pr_SubsetMatch :
   #(SubsetMatch pr_ListOfTemplates )
;

pr_PermutaionMatch  :
   pr_ValueList 
;

pr_SupersetMatch  :
   #(SupersetMatch pr_ListOfTemplates )
;

pr_BitStringMatch  :
  #(BitStringMatch (b:BSTRINGMATCH ) )
;

pr_HexStringMatch  :
  #(HexStringMatch (b:HSTRINGMATCH ) )
;

pr_OctetStringMatch  :
  #(OctetStringMatch (b:OSTRINGMATCH ) )
;

pr_Complement  :
	#(Complement 
     	(	pr_ListOfTemplates //changed from pr_ValueList 
     	) 
    )
;

pr_ValueList  :

  #( ValueList 
     ( pr_ConstantExpression )*  )

;


pr_LengthMatch :
	pr_StringLength 
;

pr_WildcardLengthMatch:
	#(WildcardLengthMatch
		pr_ConstantExpression
	)
;

pr_Range  :
  #( Range pr_LowerBound  pr_UpperBound )

;

pr_LowerBound  :
	#( LowerBound (pr_ExclamationMark)? (pr_SingleConstExpression  | (Minus Infinity) ))
;

pr_UpperBound  :
	#( UpperBound (pr_ExclamationMark)? (pr_SingleConstExpression  | Infinity ))
;

pr_ExclamationMark : 
	#(ExclamationMark
		EXCLAMATIONMARK
	)
;

pr_TemplateInstance :
	#(TemplateInstance pr_InLineTemplate)
;

pr_TemplateInstanceAssignment {Symbol a; Symbol b;}:
	#(TemplateInstanceAssignment 
		//TODO: Add known scope
		{
		
		//	System.out.println("Instance Symbol "+instanceSymbol);
//	System.out.println("Scope "+instanceSymbol.getDeclarationNode());
//	System.out.println("==>"+((LocationAST) _t).getContainingModuleDefinitionIdentifiersList().get(0));
//	//TODO; Testcase context has to be considered as well
//	if (instanceSymbol != null && (!(preparedScopeStack.empty()))){
//		//((FunctionSymbol)instanceSymbol).get.getScope();
//		ScopeWithKnownName scopeWithName = new ScopeWithKnownName(scopeStack.peek(),
//				preparedScopeStack.peek(), instanceSymbol.getName(), false);
//		knownScopes.add(scopeWithName);
//
//	
//	} else {
//		System.out.println("Symbol not resolved! "+instanceSymbol);
//	}
//		
//		if ((typeSymb.getReferencedType() != null) && (!(preparedScopeStack.empty()))) {
//				ScopeWithKnownName scopeWithName = new ScopeWithKnownName(scopeStack.peek(),
//					preparedScopeStack.peek(), typeSymb.getReferencedType().getIdentifier().getName(), false);
//				knownScopes.add(scopeWithName);
//		}
		}
		a=pr_Identifier
		//{addUnresolvableActualParameterSymbol(a);}
		pr_InLineTemplate
	)
;


pr_TemplateRefWithParList {Symbol a,b;}:
	#(TemplateRefWithParList 
		( 	a=pr_GlobalModuleId 
		)? 
		b=pr_Identifier  
		(	pr_TemplateActualParList 
		)?
	)
;

pr_InLineTemplate  :
	#(InLineTemplate
	  	( 	pr_Type[null] 
	  	)? 
	  	( 	pr_DerivedRefWithParList  
	  	)? 
	    pr_TemplateBody 
	)
;

pr_DerivedRefWithParList:
  	#(DerivedRefWithParList 
  		pr_TemplateRefWithParList 
  	)
;

pr_TemplateActualParList  :
  	#(TemplateActualParList
  		(  	(pr_TemplateActualPar)=>( 	pr_TemplateActualPar 
	     	)* 
	      | (	pr_TemplateActualParAssignment
	      	)*
	    )
    )
;

pr_TemplateActualPar :
  #(TemplateActualPar
  		(	pr_TemplateInstance | Dash )
  )
;

pr_TemplateActualParAssignment :
  #(TemplateActualParAssignment
  		(	pr_TemplateInstanceAssignment )
  )
;


pr_TemplateOps :
	#(TemplateOps
		(pr_MatchOp | pr_ValueofOp)
	)
;


pr_MatchOp  :
  #( MatchOp 
     pr_Expression  pr_TemplateInstance )
;


pr_ValueofOp  :

  #( ValueofOp pr_TemplateInstance  )

;

/***** 1.6.1.4 - Function Definitions  *****/

pr_FunctionDef {Symbol a; FunctionSymbol symbol = new FunctionSymbol(); Reference r; Scope preparedScope=null; Scope lastScope=null; Scope scopeToPop=null;}: 
	#(upper:FunctionDef
		 { prepareNewScope(true); }
		 a=pr_Identifier  
		 { 
		 	symbol.setName(a.getName()); 
			symbol.setDeclarationNode(a.getDeclarationNode());
			symbol.setScope(a.getScope());
			symbol.setGroupName(a.getGroupName());
			setVisibility(symbol);
			addSymbolToScope(symbol); 
			preparedScopeStack.peek().setScopeSymbol(symbol);
		 }
		 { 
		 	lastScope = scopeStack.peek();
		 	preparedScope = preparedScopeStack.peek();
		 	pushPreparedScope(); 
		 	scopeToPop = scopeStack.peek();
		 	((LocationAST)#upper).setScope(scopeStack.peek());			
		 }
		 (nodea:pr_FunctionFormalParList {connectToScope(nodea);})?  
		 (
		 	r=nodeb:pr_RunsOnSpec {connectToScope(nodeb);}
		 	{
		 		if (r != null) {
		 			ScopeWithKnownName scopeWithName = new ScopeWithKnownName(lastScope, preparedScope, r.getIdentifier().getName(), false);
		 			knownScopes.add(scopeWithName);
		 		}
		 	}
		 )? 
		 (nodec:pr_ReturnType {connectToScope(nodec);})?  
		 noded:pr_StatementBlock 
		 {
		 	//TODO: What is this one for
		 	//scopeStack.peek().setEndOffset(((LocationAST)#upper).getEndOffset());
		 	popToScope(scopeToPop); 
		 	connectToScope(noded); 
		 	passEndOffsetToScopeChildren(); 
		 }
		 { 
//			addScopeToOffsetMap();
		 	scopeStack.pop(); 
		 } 
	)
;


pr_FunctionFormalParList  :
     pr_FunctionFormalPar  ( pr_FunctionFormalPar )*
;


pr_FunctionFormalPar :
    pr_FormalValuePar
  | pr_FormalTimerPar
  | pr_FormalTemplatePar
  | pr_FormalPortPar
;


pr_ReturnType  :
  	#(ReturnType  
  		(	TemplateKeyword 
  		  | pr_RestrictedTemplate
  		)? 
  		pr_Type[null]  
  	)
;


pr_RunsOnSpec returns [Reference r=null]:
  #( RunsOnSpec r=pr_ComponentType )
;


pr_StatementBlock{Scope scopeToPop = null;}  :
	#(StatementBlock
		{	scopeToPop = scopeStack.peek();
			pushNewScope(true); 
		}
		(
			(	pr_FunctionLocalDefOrInst 
			)
	  	  | (	pr_FunctionStatement
				(	SemiColon	
				)?
			)
		)*
		{	popToScope(scopeToPop); 	
		}
	)
;
pr_FunctionLocalDefOrInst{Scope lastScope=null; AST a = null;}:
//	#(upper:FunctionLocalDefOrInst
		( 	{	lastScope = scopeStack.peek(); 
		   		pushNewScope(true); 
		   		tempSymbol = null;
		   	}
		   	(  	b:pr_FunctionLocalDef {a = #b;}
		  	  | c:pr_FunctionLocalInst {a = #c; }
		  	)	 
	    	{ 
	    		lastScope.setEndOffset(((LocationAST)#a).getEndOffset()); 
				scopeStack.peek().setScopeSymbol(tempSymbol);
				tempSymbol=null;
	    		scopeStack.peek().setStartOffset(((LocationAST)#a).getEndOffset()+1);

//				popToScope(lastScope);
//				lastScope = null; 
//	    		scopeStack.pop();  
//	    		((LocationAST)#upper).setScope(scopeStack.peek());
	    	}
		)
		(	pr_WithStatement
		)?
		(	SemiColon
		)?
//	)
;

//TODO: Deprecated
pr_FunctionStatementList:
//	#(FunctionStatementList
		(	pr_FunctionStatement
			(	SemiColon
			)?
		)*
//	)
;

/*
pr_FunctionStatementOrDef {Scope lastScope=null;} :
 #(upper:FunctionStatementOrDef(
    (
    	{
    		lastScope = scopeStack.peek(); 
    		pushNewScope(true); 
    		tempSymbol = null;
    	} 
    	a:pr_FunctionLocalDef 
    	{ 
    		lastScope.setEndOffset(((LocationAST)#a).getEndOffset()); 
			scopeStack.peek().setScopeSymbol(tempSymbol);
			tempSymbol=null;
    		scopeStack.peek().setStartOffset(((LocationAST)#a).getEndOffset()+1); 
    		((LocationAST)#upper).setScope(scopeStack.peek());
    	}
    )
	| 
	(
		{
			lastScope = scopeStack.peek(); 
			pushNewScope(true); 
    		tempSymbol = null;
		} 
		b:pr_FunctionLocalInst 
		{ 
			lastScope.setEndOffset(((LocationAST)#b).getEndOffset()); 
			scopeStack.peek().setStartOffset(((LocationAST)#b).getEndOffset()+1); 
			scopeStack.peek().setScopeSymbol(tempSymbol);
    		((LocationAST)#upper).setScope(scopeStack.peek());
			tempSymbol=null;
		}
	)
	| pr_FunctionStatement
))
;
*/

pr_FunctionLocalInst :

#(FunctionLocalInst(
    pr_VarInstance
  | pr_TimerInstance
))
;


pr_FunctionLocalDef :
	#(FunctionLocalDef
		(	pr_ConstDef| pr_TemplateDef 
		)
	)
;


pr_FunctionStatement  :
#(FunctionStatement(
    pr_ConfigurationStatements
  | pr_TimerStatements
  | pr_CommunicationStatements
  | pr_BasicStatements
  | pr_BehaviourStatements
  | pr_VerdictStatements
  | pr_SUTStatements
  | pr_TestcaseOperation
 )  )
;


pr_FunctionInstance {Symbol a;} :
  #( FunctionInstance 
     pr_FunctionRef   ( pr_FunctionActualParList  )? )
;


pr_FunctionRef {Symbol a,b;}:
#(FunctionRef(
  ( ( pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId )? b=pr_Identifier 
  ))
;


pr_FunctionActualParList:
 	 #(FunctionActualParList   // -=> FunctionActualParList
        (	(pr_FunctionActualPar)=>(	pr_FunctionActualPar 
     		)*
          |	(	pr_FunctionActualParAssignment
      		)* 
     	)
     )
;

pr_FunctionActualParAssignment :
	#(FunctionActualParAssignment 
		(	(pr_TemplateInstanceAssignment ) => pr_TemplateInstanceAssignment
		  | (pr_ComponentRefAssignment) => pr_ComponentRefAssignment
		  | (pr_PortRefAssignment) => pr_PortRefAssignment
		  | pr_TimerRefAssignment
		)
	)
;

pr_FunctionActualPar :
	#(FunctionActualPar (
		( pr_TemplateInstance ) => pr_TemplateInstance
		| (pr_TimerRef) => pr_TimerRef
		| (pr_Port) => pr_Port
		| pr_ComponentRef
		| Dash
		)
	)
;

/*****1.6.2.5 - Signature Definitions  *****/

pr_SignatureDef {Symbol a; SignatureSymbol symbol = new SignatureSymbol(); }:
	#( SignatureDef 
		a=pr_Identifier  
		{
			symbol.setName(a.getName());
			symbol.setDeclarationNode(a.getDeclarationNode());
			symbol.setGroupName(a.getGroupName());
			symbol.setScope(a.getScope());
			setVisibility(symbol);
			addSymbolToScope(symbol); 
		}
		( pr_SignatureFormalParList  )? 
		( pr_ReturnType  | NoBlockKeyword )? 
		( pr_ExceptionSpec )? 
	)
;

pr_SignatureFormalParList  :
	#( SignatureFormalParList
		( pr_SignatureFormalPar )* 
	)
;


pr_SignatureFormalPar :
  pr_FormalValuePar
;

pr_ExceptionSpec:
     #(ExceptionSpec 
     	pr_Type[null]  (pr_Type[null] )* 
     ) 
;


pr_Signature {Symbol a,b;}:
#(Signature(
  ( ( pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId )? b=pr_Identifier 
))
;

/***** 1.6.2.6 - Testcase Definitions  *****/

pr_TestcaseDef {Symbol a; TestcaseSymbol symbol = new TestcaseSymbol(); List<Reference> reflist; Scope lastScope=null; Scope preparedScope=null; Scope scopeToPop=null;}: 
	#( upper:TestcaseDef 
		{ prepareNewScope(true); }
		a=pr_Identifier 
		{ 
			symbol.setName(a.getName()); 
			symbol.setDeclarationNode(a.getDeclarationNode());
			symbol.setScope(a.getScope());
			symbol.setGroupName(a.getGroupName());
			setVisibility(symbol);
			addSymbolToScope(symbol); 
			preparedScopeStack.peek().setScopeSymbol(symbol);
		}
		{
			lastScope = scopeStack.peek();
			preparedScope = preparedScopeStack.peek();
			pushPreparedScope();
			scopeToPop = scopeStack.peek();
			((LocationAST)#upper).setScope(scopeStack.peek());
		}
		( nodea:pr_TestcaseFormalParList {connectToScope(nodea);} )? 
		reflist=nodeb:pr_ConfigSpec {connectToScope(nodeb);}
		{
			if (reflist.size() > 0) {
				for (int i=0; i < reflist.size(); i ++) {
					Reference r=reflist.get(i);
					if (r!=null && r.getIdentifier()!=null){
						ScopeWithKnownName scopeWithName = new ScopeWithKnownName(lastScope, preparedScope, r.getIdentifier().getName(), r.isSystemReference());
						knownScopes.add(scopeWithName);
					}
				}
			}
		}
		nodec:pr_StatementBlock
		{
			popToScope(scopeToPop); 
			connectToScope(nodec); 
			passEndOffsetToScopeChildren(); 
		}

		{ 
//			addScopeToOffsetMap();
			scopeStack.pop(); 
		} 
	)
;

pr_TestcaseFormalParList  :
	pr_TestcaseFormalPar  (pr_TestcaseFormalPar )*
;

pr_TestcaseFormalPar :
    pr_FormalValuePar
  | pr_FormalTemplatePar
;

pr_ConfigSpec returns [List<Reference> reflist=new ArrayList<Reference>();] {Reference r; Reference system;}:
	#(ConfigSpec
		r=pr_RunsOnSpec  {reflist.add(r);}
		( system=pr_SystemSpec { system.setSystemReference(true); reflist.add(system);} )? 
	)
;

pr_SystemSpec returns [Reference r=null]:
	#(SystemSpec 
		r=pr_ComponentType 
	)
;

pr_TestcaseInstance {Symbol a;} :
	#(TestcaseInstance
		( 
			pr_TestcaseRef  
			( 	pr_TestcaseActualParList 
			)? 
			( 	(	Minus
			      | pr_TimerValue
				)
				(	pr_SingleExpression
				)?
				 
			)?
		)
	)
;

pr_TestcaseRef {Symbol a,b;}:
  #( TestcaseRef
     ( ( pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId )? b=pr_Identifier )
;

pr_TestcaseActualParList :
  	#(TestcaseActualParList
     	(	(pr_TestcaseActualPar)=>( 	pr_TestcaseActualPar  
     		)* 
		  | (	pr_TestcaseActualParAssignment
		  	)*
		)
    )
;

pr_TestcaseActualParAssignment :
	#(TestcaseActualParAssignment 
		pr_TemplateInstanceAssignment
	)
;


pr_TestcaseActualPar :
  	#(TestcaseActualPar 
  		(	pr_TemplateInstance | Dash )
  	)
;

/***** 1.6.1.7 - Altstep Definitions  *****/

pr_AltstepDef {Symbol a; Reference r; AltstepSymbol symbol = new AltstepSymbol(); Scope lastScope=null; Scope preparedScope=null; Scope scopeToPop=null;}: 
	#( n:AltstepDef 
		(
			{ prepareNewScope(true); }
			a=pr_Identifier 
			{ 
				symbol.setName(a.getName()); 
				symbol.setDeclarationNode(a.getDeclarationNode());
				symbol.setScope(a.getScope());
				symbol.setGroupName(a.getGroupName());
				setVisibility(symbol);
				addSymbolToScope(symbol); 
				preparedScopeStack.peek().setScopeSymbol(symbol);
				lastScope = scopeStack.peek();
				preparedScope = preparedScopeStack.peek();
				pushPreparedScope();
				scopeToPop = scopeStack.peek();
				((LocationAST)#n).setScope(scopeStack.peek());
			}
			( nodea:pr_AltstepFormalParList {connectToScope(nodea);})? 
			( 
				r=nodeb:pr_RunsOnSpec {connectToScope(nodeb);} 
				{
					if (r != null) {
						ScopeWithKnownName scopeWithName = new ScopeWithKnownName(lastScope, preparedScope, r.getIdentifier().getName(), false);
						knownScopes.add(scopeWithName);
					}
				}
			)?
			(nodec:pr_AltstepLocalDefList
			{
//				popToScope(scopeToPop); 
//				connectToScope(nodec); 
			})?
			(noded:pr_AltGuardList {
//				popToScope(scopeToPop); 
//				connectToScope(noded);
			})?
			{
				popToScope(scopeToPop); 
				connectToScope(noded);



//				scopeStack.peek().setEndOffset(((LocationAST)#n).getEndOffset());
				 
				passEndOffsetToScopeChildren(); 
//				addScopeToOffsetMap();
				scopeStack.pop();
			}
		)
	)
;

pr_AltstepFormalParList :
  pr_FunctionFormalParList
;

pr_AltstepLocalDefList {Scope lastScope=null;}:
	#(upper:AltstepLocalDefList
    	{
    		lastScope = scopeStack.peek(); 
    		pushNewScope(true); 
    		tempSymbol=null;
    		
    	} 
		a:pr_AltstepLocalDef  
    	{ 
    		lastScope.setEndOffset(((LocationAST)#a).getEndOffset()); 
    		scopeStack.peek().setScopeSymbol(tempSymbol);
    		tempSymbol=null;
    		scopeStack.peek().setStartOffset(((LocationAST)#a).getEndOffset()+1); 
    		((LocationAST)#upper).setScope(scopeStack.peek());
    	}
    	(	pr_WithStatement
    	)?
		(
			SemiColon  

	    	{
	    		lastScope = scopeStack.peek(); 
	    		pushNewScope(true); 
	    		tempSymbol=null;
	    		
	    	} 
			b:pr_AltstepLocalDef  
	    	{ 
	    		lastScope.setEndOffset(((LocationAST)#a).getEndOffset()); 
	    		scopeStack.peek().setScopeSymbol(tempSymbol);
	    		tempSymbol=null;
	    		scopeStack.peek().setStartOffset(((LocationAST)#a).getEndOffset()+1); 
	    		((LocationAST)#upper).setScope(scopeStack.peek());
	    	}
	    	(	pr_WithStatement
    		)?
		)*
	)
;

pr_AltstepLocalDef :
#(AltstepLocalDef(
     pr_VarInstance 
   | pr_TimerInstance 
   | pr_ConstDef 
   | pr_TemplateDef))
;


pr_AltstepInstance {Symbol a;} :
  #( AltstepInstance   
     pr_AltstepRef  ( pr_FunctionActualParList )? )
;


pr_AltstepRef {Symbol a,b;}:
  ( ( pr_GlobalModuleId pr_Identifier ) => a=pr_GlobalModuleId  )? b=pr_Identifier 
;

/***** 1.6.1.8 - Import Definitions  *****/

pr_ImportDef { AbstractImport imp=null; Symbol a;} :
	#(upper:ImportDef
		(
			a=pr_ImportFromSpec 
			( 
				imp=pr_AllWithExcepts
				| 
				imp=pr_ImportSpec 
			)
		)
		{
			if (imp != null) {
				imp.setModuleName(a.getName());
				setImportVisibility(#upper, imp);
				scopeStack.peek().getImportConfiguration().addImport(imp);
			}
		}
	)
;

pr_AllWithExcepts returns [AbstractImport imp] {imp = new AllExceptImport(); List<KindAllExceptImport> klist;}:
	#(AllWithExcepts 
		( 
			( 
				klist=pr_ExceptsDef 
				{
					((AllExceptImport)imp).setExceptionList(klist);
				}
			)? 
		)
	)
;

pr_ExceptsDef returns [List<KindAllExceptImport> klist=null]:
	#(ExceptsDef klist=pr_ExceptSpec)
;

pr_ExceptSpec returns [List<KindAllExceptImport> klist=new ArrayList<KindAllExceptImport>();] {KindAllExceptImport k;}:
	#(ExceptSpec (k=pr_ExceptElement {klist.add(k);} (SemiColon )? )*)
;

pr_ExceptElement returns [KindAllExceptImport k=null]:
	#(ExceptElement
		(
			k=pr_ExceptGroupSpec
			| k=pr_ExceptTypeDefSpec 
			| k=pr_ExceptTemplateSpec 
			| k=pr_ExceptConstSpec 
			| k=pr_ExceptTestcaseSpec 
			| k=pr_ExceptAltstepSpec  
			| k=pr_ExceptFunctionSpec 
			| k=pr_ExceptSignatureSpec 
			| k=pr_ExceptModuleParSpec 
		)
	)
;

//////////////////////////////////////////////////////////////

pr_ExceptGroupSpec returns [KindAllExceptImport k = new KindAllExceptImport();] {List<String> reflist;}:
	#(ExceptGroupSpec 
		{
			k.setType(AbstractKindImport.KindEnum.ttcn3_group);
		}
		( 
			reflist=pr_ExceptGroupRefList {k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);}
			| 
			AllKeyword {k.setMode(KindAllExceptImport.ModesEnum.all);}
		)
	)
;

pr_ExceptGroupRefList returns [List<String> s=new ArrayList<String>();] {Symbol a,b;}:
	#(ExceptGroupRefList a=pr_FullGroupIdentifier {s.add(a.getName());} (b=pr_FullGroupIdentifier {s.add(b.getName());})*)
;

pr_ExceptTypeDefSpec returns [KindAllExceptImport k = new KindAllExceptImport();] {List<String> reflist;}:
	#(ExceptTypeDefSpec 
		{
			k.setType(AbstractKindImport.KindEnum.ttcn3_type);
		}
		(
			reflist=pr_TypeRefList {k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);}
			|
			AllKeyword {k.setMode(KindAllExceptImport.ModesEnum.all);}
		)
	)
;

pr_TypeRefList returns [List<String> s=new ArrayList<String>();] {Symbol a,b;}:
	#(TypeRefList a=pr_Identifier {s.add(a.getName());} (b=pr_Identifier {s.add(b.getName());})*)
;

pr_ExceptTemplateSpec returns [KindAllExceptImport k = new KindAllExceptImport();] {List<String> reflist;}:
	#(ExceptTemplateSpec 
		{
			k.setType(AbstractKindImport.KindEnum.ttcn3_template);
		}
		(
			reflist=pr_TemplateRefList {k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);}
			| 
			AllKeyword {k.setMode(KindAllExceptImport.ModesEnum.all);}
		)
	)
; 

pr_TemplateRefList returns [List<String> s=new ArrayList<String>();] {Symbol a,b;}:
	#(TemplateRefList a=pr_Identifier {s.add(a.getName());} (b=pr_Identifier {s.add(b.getName());})*)
;

pr_ExceptConstSpec returns [KindAllExceptImport k = new KindAllExceptImport();] {List<String> reflist;}:
	#(ExceptConstSpec 
		{
			k.setType(AbstractKindImport.KindEnum.ttcn3_const);
		}
		(
			reflist=pr_ConstRefList {k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);}
			|
			AllKeyword {k.setMode(KindAllExceptImport.ModesEnum.all);}
		)
	)
;


pr_ExceptTestcaseSpec returns [KindAllExceptImport k = new KindAllExceptImport();] {List<String> reflist;}:
	#(ExceptTestcaseSpec 
		{
			k.setType(AbstractKindImport.KindEnum.ttcn3_testcase);
		}
		(
			reflist=pr_TestcaseRefList {k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);}
			|
			AllKeyword {k.setMode(KindAllExceptImport.ModesEnum.all);}
		)
	)
;

pr_ExceptAltstepSpec returns [KindAllExceptImport k = new KindAllExceptImport();] {List<String> reflist;}:
	#(ExceptAltstepSpec 
		{
			k.setType(AbstractKindImport.KindEnum.ttcn3_altstep);
		}
		(
			reflist=pr_AltstepRefList {k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);}
			| 
			AllKeyword {k.setMode(KindAllExceptImport.ModesEnum.all);}
		)
	)
;


pr_ExceptFunctionSpec returns [KindAllExceptImport k = new KindAllExceptImport();] {List<String> reflist;}:
	#(ExceptFunctionSpec 
		{
			k.setType(AbstractKindImport.KindEnum.ttcn3_function);
		}
		(
			reflist=pr_FunctionRefList {k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);}
			| 
			AllKeyword {k.setMode(KindAllExceptImport.ModesEnum.all);}
		)
	)
;


pr_ExceptSignatureSpec returns [KindAllExceptImport k = new KindAllExceptImport();] {List<String> reflist;}:
	#(ExceptSignatureSpec 
		{
			k.setType(AbstractKindImport.KindEnum.ttcn3_signature);
		}
		(
			reflist=pr_SignatureRefList {k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);}
			| 
			AllKeyword {k.setMode(KindAllExceptImport.ModesEnum.all);} 
		)
	)
;


pr_ExceptModuleParSpec returns [KindAllExceptImport k = new KindAllExceptImport();] {List<String> reflist;}:
	#(ExceptModuleParSpec 
		{
			k.setType(AbstractKindImport.KindEnum.ttcn3_modulepar);
		}
		(
			reflist=pr_ModuleParRefList {k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);}
			| 
			AllKeyword {k.setMode(KindAllExceptImport.ModesEnum.all);}
		)
	)
;          

//////////////////////////////////////////////////////////////////

pr_ImportSpec returns [KindImport kindImport=new KindImport();] {AbstractKindImport kindNames;}:
	#(ImportSpec 
		(
			( kindNames=pr_ImportElement {kindImport.addKind(kindNames);} (SemiColon )? )*
		)
	)
;

pr_ImportElement returns [AbstractKindImport kindNames=null]:
	#(ImportElement 
		(
			kindNames=pr_ImportGroupSpec
			| kindNames=pr_ImportTypeDefSpec
			| kindNames=pr_ImportTemplateSpec
			| kindNames=pr_ImportConstSpec
			| kindNames=pr_ImportTestcaseSpec
			| kindNames=pr_ImportAltstepSpec
			| kindNames=pr_ImportFunctionSpec
			| kindNames=pr_ImportSignatureSpec
			| kindNames=pr_ImportModuleParSpec
			| kindNames=pr_ImportImportSpec
		)
	)
;

pr_ImportImportSpec returns [KindNames kindNames=new KindNames();]:
	#(ImportImportSpec
		{
			kindNames.setType(KindNames.KindEnum.ttcn3_import);
			kindNames.setMode(KindNames.ModeEnum.allexcept);
		}
	)
;

pr_ImportFromSpec returns [Symbol s=null] :
	#(ImportFromSpec 
		s=pr_ModuleId  
		(RecursiveKeyword)?
	)
;

pr_LanguageSpec returns [String s=null]:
	#(LanguageSpec 
		(	s=pr_FreeText
		)+
	)
;


pr_GlobalModuleId returns [Symbol s=null]:
	#(GlobalModuleId (s=pr_Identifier) (pr_ObjectIdentifierValue)?)
;

// ---

pr_ObjectIdentifierValue:
	#(OBJID 
		(
			(pr_ReferencedValue
			| pr_NumberForm
			| pr_NameAndNumberForm 
			| pr_NameForm
			)
		)+
	)
;

pr_NumberForm:
	#(NumberForm a:NUMBER)
;

pr_NameAndNumberForm:
	#(NameAndNumberForm a:IDENTIFIER pr_NumberForm)
;

pr_NameForm:
	#(NameForm a:IDENTIFIER)
;

// -----


pr_ImportModuleParSpec returns [KindNames kindNames=new KindNames();] {List<String> reflist;}  :
	#(ImportModuleParSpec 
		{
			kindNames.setType(KindNames.KindEnum.ttcn3_modulepar);
		}
		(  
			reflist=pr_ModuleParRefList {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept); }
			| 
			reflist=pr_AllModuleParWithExcept {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);}
		)
	)
;

pr_AllModuleParWithExcept returns [List<String> reflist=null;] :
	#(AllModuleParWithExcept  
		( reflist=pr_ModuleParRefList )?
	)
;

pr_ModuleParRefList returns [List<String> s=new ArrayList<String>();] {Symbol a,b;}:
	#(ModuleParRefList 
		a=pr_Identifier {s.add(a.getName());}
		(b=pr_Identifier {s.add(b.getName());} )*
	)
;

pr_ImportGroupSpec returns [AbstractKindImport kindImport=null]{List<String> reflist; GroupImport imp = new GroupImport();} :
	#(ImportGroupSpec 
		( 
			imp=pr_GroupRefListWithExcept 
			| 
			reflist=pr_AllGroupsWithExcept {imp.setExceptionList(reflist); imp.setMode(GroupImport.ModeEnum.allGroupWithExcept);}
		)
	)
	{
		imp.setType(AbstractKindImport.KindEnum.ttcn3_group);
		kindImport = imp;
	}
;

pr_GroupRefListWithExcept returns [GroupImport imp = new GroupImport();] {GroupExceptImport a,b;} :
	#(GroupRefListWithExcept 
		{imp.setMode(GroupImport.ModeEnum.groupRefListWithExcept);}
		a=pr_FullGroupIdentifierWithExcept {imp.addToRefList(a);}
		(
			(b=pr_FullGroupIdentifierWithExcept {imp.addToRefList(b);} )*
		)
	)
;

pr_AllGroupsWithExcept returns [List<String> s=null] :
	#(AllGroupsWithExcept 
		( s=pr_GroupRefList )?
	)
;


pr_FullGroupIdentifierWithExcept returns [GroupExceptImport g = new GroupExceptImport();] { List<KindAllExceptImport> klist; Symbol a;} :
    #(FullGroupIdentifierWithExcept
		a=pr_FullGroupIdentifier
		{
			g.setGroupName(a.getGroupName());
		}
		( klist=pr_ExceptsDef {g.setExceptionList(klist);} )?	
	)
;

pr_GroupRefList returns [List<String> s=new ArrayList<String>();] {Symbol a,b;}:
	#(GroupRefList 
		a=pr_FullGroupIdentifier 
		{
			s.add(a.getGroupName());
		} 
		(
			b=pr_FullGroupIdentifier 
			{s.add(b.getGroupName());} 
		)* 
	)
;

pr_FullGroupIdentifier returns [Symbol s=new Symbol();] {Symbol a;}:
	#(FullGroupIdentifier
		a=pr_Identifier 
		{
			s.setGroupName(a.getName());
		} 
		(
			Dot 
			a=pr_Identifier 
			{
				String temp = s.getGroupName() + "." + a.getName();
				s.setGroupName(temp);
			}
		)*
	) 
;

pr_ImportTypeDefSpec returns [KindNames kindNames=new KindNames();] {List<String> reflist;} :
	#(ImportTypeDefSpec 
	{
		kindNames.setType(KindNames.KindEnum.ttcn3_type);
	}
		(
			reflist=pr_TypeRefList {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept); }
			| 
			reflist=pr_AllTypesWithExcept {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);}
		)
	)
;

pr_AllTypesWithExcept returns [List<String> reflist=null]:
	#(AllTypesWithExcept  
		( reflist=pr_TypeRefList )?
	)
;

pr_TypeDefIdentifier {Symbol a;}:
	a=pr_Identifier
;

pr_ImportTemplateSpec returns [KindNames kindNames=new KindNames();] {List<String> reflist;}:
	#(ImportTemplateSpec 
		{
			kindNames.setType(KindNames.KindEnum.ttcn3_template);
		}
		(
			reflist=pr_TemplateRefList {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept); }
			| 
			reflist=pr_AllTemplsWithExcept {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);}
		) 
	)
;

pr_AllTemplsWithExcept returns [List<String> reflist=null]:
	#(AllTemplsWithExcept 
		( reflist=pr_TemplateRefList )?
	)
;

pr_ImportConstSpec returns [KindNames kindNames=new KindNames();] {List<String> reflist;}:
	#(ImportConstSpec
		{
			kindNames.setType(KindNames.KindEnum.ttcn3_const);
		}
		(
			reflist=pr_ConstRefList {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept); }
			| 
			reflist=pr_AllConstsWithExcept {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);}
		)
	)
;

pr_AllConstsWithExcept returns [List<String> reflist=null]:
	#(AllConstsWithExcept 
		( reflist=pr_ConstRefList )?
	)
;

pr_Identifier returns [Symbol s=new Symbol();]  :
	#(Identifier a:IDENTIFIER )
	{
		s.setName(a.getText());
		s.setDeclarationNode((LocationAST) a);
		if (!(groupStack.empty()))
			s.setGroupName(groupStack.peek());
		if (!(scopeStack.empty())) {
			if (!(preparedScopeStack.empty())) {
				LocationAST ast = (LocationAST) a;
				ast.setScope(scopeStack.peek()); 
				ast.setChildScope(preparedScopeStack.peek());
				s.setScope(preparedScopeStack.peek());
				s.setSurroundingScope(scopeStack.peek());
				s.setDeclarationNode(ast);
			} else {
				LocationAST ast = (LocationAST) a;
				ast.setScope(scopeStack.peek()); 
				s.setSurroundingScope(scopeStack.peek());
				s.setDeclarationNode(ast);
			}
		}
	}
;

pr_ConstRefList returns [List<String> s=new ArrayList<String>();]{Symbol a,b;}:
	#(ConstRefList 
		a=pr_Identifier {s.add(a.getName());}
		(b=pr_Identifier {s.add(b.getName());})*
	)
;

pr_ImportAltstepSpec returns [KindNames kindNames=new KindNames();] {List<String> reflist;}:
	#(ImportAltstepSpec
		{
			kindNames.setType(KindNames.KindEnum.ttcn3_altstep);
		}
		(  
			reflist=pr_AltstepRefList {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept); }
			| 
			reflist=pr_AllAltstepsWithExcept {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);}
		)
	)
;

pr_AltstepRefList returns [List<String> s = new ArrayList<String>();]{Symbol a,b;}:
	#(AltstepRefList 
		a=pr_Identifier {s.add(a.getName());} (b=pr_Identifier {s.add(b.getName());})* 
	)
;

pr_AllAltstepsWithExcept returns [List<String> reflist=null]:
	#(AllAltstepsWithExcept 
		( reflist=pr_AltstepRefList )? 
	)
;

pr_ImportTestcaseSpec returns [KindNames kindNames=new KindNames();] {List<String> reflist;}:
	#( ImportTestcaseSpec
		{
			kindNames.setType(KindNames.KindEnum.ttcn3_testcase);
		}
		(  
			reflist=pr_TestcaseRefList {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept); }
			| 
			reflist=pr_AllTestcasesWithExcept {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);}
		)
	)
;

pr_TestcaseRefList returns [List<String> s = new ArrayList<String>();]{Symbol a,b;}:
	#(TestcaseRefList 
		a=pr_Identifier {s.add(a.getName());} (b=pr_Identifier {s.add(b.getName());})* 
	)
;

pr_AllTestcasesWithExcept returns [List<String> reflist=null]:
	#(AllTestcasesWithExcept 
		( reflist=pr_TestcaseRefList )? 
	)
;

pr_ImportFunctionSpec returns [KindNames kindNames=new KindNames();] {List<String> reflist;}:
	#( ImportFunctionSpec
		{
			kindNames.setType(KindNames.KindEnum.ttcn3_function);
		}
		(  
			reflist=pr_FunctionRefList {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept); }
			| 
			reflist=pr_AllFunctionsWithExcept {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);}
		)
	)
;

pr_FunctionRefList returns [List<String> s=new ArrayList<String>();] {Symbol a,b;}:
	#(FunctionRefList 
		a=pr_Identifier {s.add(a.getName());} (b=pr_Identifier {s.add(b.getName());})*
	)
;

pr_AllFunctionsWithExcept returns [List<String> reflist=null]:
	#(AllFunctionsWithExcept 
		( reflist=pr_FunctionRefList )?
	)
;

pr_ImportSignatureSpec returns [KindNames kindNames=new KindNames();] {List<String> reflist;}:
	#(ImportSignatureSpec
		{
			kindNames.setType(KindNames.KindEnum.ttcn3_signature);
		}
		(  
			reflist=pr_SignatureRefList {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept); }
			| 
			reflist=pr_AllSignaturesWithExcept {kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);}
		)
	)
;

pr_SignatureRefList returns [List<String> s=new ArrayList<String>();] {Symbol a,b;}:
	#(SignatureRefList 
		a=pr_Identifier {s.add(a.getName());} (b=pr_Identifier {s.add(b.getName());})* 
	)
;

pr_AllSignaturesWithExcept returns [List<String> reflist=null]:
	#(AllSignaturesWithExcept 
		( reflist=pr_SignatureRefList )?
	)
;


/*****  1.6.1.9 - Group Definitions  *****/

pr_GroupDef {Symbol a; GroupSymbol symbol = new GroupSymbol();}:
	#(upper:GroupDef  
		a=pr_Identifier  
		{
			symbol.setName(a.getName());
			symbol.setDeclarationNode(a.getDeclarationNode());
			addSymbolToScope(symbol);
			groupStack.push(a.getName());
			((LocationAST)#upper).setScope(scopeStack.peek());
		}
		( pr_ModuleDefinitionsPart  )?   
		{groupStack.pop();}
	)
;


/***** 1.6.1.10 - External Function Definitions  *****/

pr_ExtFunctionDef {Symbol a; FunctionSymbol symbol = new FunctionSymbol(); Scope preparedScope=null; Scope lastScope=null; Scope scopeToPop=null;}:
	#(upper:ExtFunctionDef
		{ prepareNewScope(true);  }
  		a=pr_Identifier
  		{
  			symbol.setName(a.getName());
			symbol.setDeclarationNode(a.getDeclarationNode());
  			symbol.setGroupName(a.getGroupName());
  			symbol.setExternal(true);
  			setVisibility(symbol);
  			addSymbolToScope(symbol);
  			preparedScopeStack.peek().setScopeSymbol(symbol);
  			lastScope = scopeStack.peek();
		 	preparedScope = preparedScopeStack.peek();
		 	pushPreparedScope(); 
		 	scopeToPop = scopeStack.peek();
		 	((LocationAST)#upper).setScope(scopeStack.peek());
  		}
  		( nodea:pr_FunctionFormalParList  {connectToScope(nodea);})? 
  		( nodec:pr_ReturnType {connectToScope(nodec);} )? 
  		 { 
  		 	popToScope(scopeToPop); 
  		 	passEndOffsetToScopeChildren(); 
//			addScopeToOffsetMap();
		 	scopeStack.pop(); 
		 } 
	)
;



/***** 1.6.1.11 - External Constant Definitions  *****/

pr_ExtConstDef {Symbol a; ConstantSymbol symbol = new ConstantSymbol();}:
	#(ExtConstDef 
		pr_Type[symbol] 
		(	a=pr_Identifier 
			{
				symbol.setName(a.getName());
				symbol.setDeclarationNode(a.getDeclarationNode());
				symbol.setGroupName(a.getGroupName());
				symbol.setExternal(true);
				setVisibility(symbol);
	  			addSymbolToScope(symbol);
			}
		)* //to support lists
	) 
;

/***** 1.6.1.12 - Module parameter definitions *****/

pr_ModuleParDef  :
	#(ModuleParDef  
		(	(	pr_ModulePar  
			)+ 
		 |  pr_MultitypedModuleParList 
		) 
	)
;

pr_MultitypedModuleParList  :
	#(MultitypedModuleParList 
		(	pr_ModulePar  
			(	SemiColon
			)?  
		)* //changed from "+"
	)
;

/***** 1.6.2 - Control Part  *****/

pr_ModuleControlPart {Scope scopeToPop=null;} : 
	#(n:ModuleControlPart 
		 { 
		 	prepareNewScope(true);
			pushPreparedScope(); 
			scopeToPop = scopeStack.peek();
			((LocationAST)#n).setScope(scopeStack.peek());
		 }
		pr_ModuleControlBody 
		{
			popToScope(scopeToPop);
		}
		( pr_WithStatement )? 
		(SemiColon )?
		{ 
			scopeStack.peek().setStartOffset(((LocationAST)#n).getOffset());
			scopeStack.peek().setEndOffset(((LocationAST)#n).getEndOffset());
		 	passEndOffsetToScopeChildren(); 
			
//			addScopeToOffsetMap();
		 	scopeStack.pop(); 
		} 
		
	)
;

pr_ModuleControlBody  :
	#(n:ModuleControlBody 
		{ 	((LocationAST)n).setScope(((LocationAST)n).getParent().getScope()); 
		} 
		( 	pr_ControlStatementOrDef  
			(	SemiColon 
			)? 
		)* 
	)
;			

pr_ControlStatementOrDef {Scope lastScope=null;}:
	#(upper:ControlStatementOrDef
		(	(
				{
	    			lastScope = scopeStack.peek(); 
		    		pushNewScope(true); 
		    		tempSymbol = null;
	    		} 
				a:pr_FunctionLocalInst
		    	{ 
		    		lastScope.setEndOffset(((LocationAST)#a).getEndOffset()); 
		    		scopeStack.peek().setStartOffset(((LocationAST)#a).getEndOffset()+1); 
		    		scopeStack.peek().setScopeSymbol(tempSymbol);
		    		((LocationAST)#upper).setScope(scopeStack.peek());
		    		tempSymbol=null;
		    	}
		    	(	pr_WithStatement
		    	)?
	    	)
			| 
			(
				{
					lastScope = scopeStack.peek(); 
					pushNewScope(true); 
		    		tempSymbol = null;
				} 
				b:pr_FunctionLocalDef 
				{ 
					lastScope.setEndOffset(((LocationAST)#b).getEndOffset()); 
					scopeStack.peek().setStartOffset(((LocationAST)#b).getEndOffset()+1); 
		    		scopeStack.peek().setScopeSymbol(tempSymbol);
		    		((LocationAST)#upper).setScope(scopeStack.peek());
		    		tempSymbol=null;
				}
				(	pr_WithStatement
		    	)?
			)
			| pr_ControlStatement
		)
	)
;

pr_ControlStatement  :
	#(ControlStatement
		(
			pr_TimerStatements
			| pr_BasicStatements
			| pr_BehaviourStatements
			| pr_SUTStatements
			| STOP 
		)
	)
;


/***** 1.6.3.1 - Variable Instantiation  *****/

pr_VarInstance {TypeSymbol typeResult = new TypeSymbol();} :
	#(VarInstance  
		(	(	(	TemplateKeyword
				  | pr_RestrictedTemplate
				)?
				pr_Type[typeResult]
				(	pr_VarList[typeResult]
				  | pr_TempVarList[typeResult]  
				) 
			) 
		) 
	) 
;

pr_VarList[TypeSymbol typeSymbol] :
	#(VarList 
		(	pr_SingleVarInstance[typeSymbol] 
		)+
	)
;

pr_TempVarList[TypeSymbol typeSymbol]  :
	#(TempVarList (pr_SingleTempVarInstance[typeSymbol]  (pr_SingleTempVarInstance[typeSymbol] )*))
;


pr_SingleTempVarInstance[TypeSymbol typeSymbol] {Symbol a; TemplateInstanceSymbol symbol = new TemplateInstanceSymbol();}:
	#(SingleTempVarInstance 
		(
			a=pr_Identifier  
			{
				symbol.setName(a.getName());
				symbol.setDeclarationNode(a.getDeclarationNode());
				symbol.setReferencedType(typeSymbol.getReferencedType());
				symbol.setPredefinedType(typeSymbol.getPredefinedType());
				addSymbolToScope(symbol);
				tempSymbol = symbol;
			}
			(pr_ArrayDef )? 
			( pr_TemplateBody )? 
		)
	)
;
                 
pr_SingleVarInstance[TypeSymbol typeSymbol] {Scope lastScope=null; Symbol a; VariableInstanceSymbol symbol = new VariableInstanceSymbol(); }: 
	#(SingleVarInstance
		{	
//TODO: NOTE: This is a working approach that has to be applied to all other
// local entities, this will however introduce many code duplicates, for 
//reference of addditional lines see also pr_FunctionLocalDefOrInst rule 
//				lastScope = scopeStack.peek(); 
//		   		pushNewScope(true); 
		}


		a=pr_Identifier 
		{
			symbol.setName(a.getName());
			symbol.setReferencedType(typeSymbol.getReferencedType());
			symbol.setPredefinedType(typeSymbol.getPredefinedType());
			symbol.setDeclarationNode(a.getDeclarationNode());
			addSymbolToScope(symbol);
		}
		( pr_ArrayDef )? 
		(pr_Expression  )? 
		{
			tempSymbol = symbol;
		}

	    	{ 

//TODO: NOTE: This is a working approach that has to be applied to all other
// local entities, this will however introduce many code duplicates, for 
//reference of addditional lines see also pr_FunctionLocalDefOrInst rule 
//				scopeStack.peek().setScopeSymbol(tempSymbol);
	    	}
		

	)
;

pr_VariableRef {Symbol a; List<Symbol> b;}:
	#(VariableRef 
		(	a=pr_Identifier  
			( 	(	pr_ExtendedFieldReference)=> b=pr_ExtendedFieldReference 
			)?
		)
	)
;


/*****    1.6.2.2 - Timer Instantiation  *****/

pr_TimerInstance  :
	#( TimerInstance ( pr_SingleTimerInstance  )+ )
;


pr_SingleTimerInstance {Symbol a; TimerInstanceSymbol symbol = new TimerInstanceSymbol(); }: 
	#(SingleTimerInstance 
		a=pr_Identifier  
		{ 
			symbol.setName(a.getName());
			symbol.setDeclarationNode(a.getDeclarationNode());
			addSymbolToScope(symbol); 
			tempSymbol = symbol;
		}
		(pr_ArrayDef)? 
		(pr_TimerValue)? 
	) 
;


pr_TimerValue :
	pr_Expression
;


pr_TimerRef {Symbol a;}:
	#(TimerRef (a=pr_Identifier  ( pr_ArrayOrBitRef  )*))
;

pr_TimerRefAssignment {Symbol a;}:
	#(TimerRefAssignment 
		(	//TODO: add known scope
			a=pr_Identifier  
			pr_TimerRef
		)
	)
;


/***** 1.6.2.3 - Component Operations  *****/

pr_ConfigurationStatements  :
	#(ConfigurationStatements 
		(
    	pr_ConnectStatement
		| pr_MapStatement
		| pr_DisconnectStatement
		| pr_UnmapStatement
		| pr_DoneStatement
		| pr_KilledStatement
		| pr_StartTCStatement
		| pr_StopTCStatement
		| pr_KillTCStatement
		)
	)
;

pr_KillTCStatement  :
	#(KillTCStatement 
		(
			KillKeyword
			| pr_ComponentReferenceOrLiteral  
			| AllKeyword 
		)
	)		   
;

pr_KilledStatement  :
	#(KilledStatement pr_ComponentId )
;

pr_ConfigurationOps :
	#(ConfigurationOps 
		(
		pr_CreateOp
		| pr_SelfOp
		| pr_SystemOp
		| pr_MTCOp
		| pr_RunningOp
		| pr_AliveOp
		)
	)
;

pr_CreateOp {Reference r;} :
	#(CreateOp 
		r=pr_ComponentType  
		(	(	pr_SingleExpression
		  	  | Minus
		  	)
		  	(	pr_SingleExpression
		  	)? 
		)?
		(AliveKeyword )?
	)
;

pr_SystemOp  :
	SystemOp 
;

pr_AliveOp  :
	#(AliveOp pr_ComponentId )
;

pr_SelfOp  :
	SelfOp 
;


pr_MTCOp  :
	MTCOp 
;


pr_DoneStatement  :
	#(DoneStatement pr_ComponentId )

;

pr_ComponentId  :
	#(ComponentId ( (pr_VariableRef |pr_FunctionInstance) | AnyKeyword  | AllKeyword ))
;

pr_RunningOp  :
	#(RunningOp pr_ComponentId )
;

pr_ConnectStatement  :
	#(ConnectStatement pr_SingleConnectionSpec )
;

pr_SingleConnectionSpec  :
	#(SingleConnectionSpec (pr_PortRef pr_PortRef)) 
;

pr_PortSpec  :
	#(PortSpec pr_PortRef pr_PortRef) 
;

pr_PortRef  :
	#(PortRef pr_ComponentRef pr_Port ) 
;

pr_PortRefAssignment{Symbol a;}  :
	#(PortRefAssignment 
		(	a=pr_Identifier 
			pr_Port 
		)
	) 
;

pr_ComponentRef :
	#(ComponentRef (pr_ComponentOrDefaultReference | pr_SystemOp | pr_SelfOp | pr_MTCOp))
;


pr_ComponentRefAssignment{Symbol a;}:
	#(ComponentRefAssignment
		//TODO: Add known scope
		a=pr_Identifier
		pr_ComponentRef
	)
;


pr_ComponentOrDefaultReference :
	pr_VariableRef | pr_FunctionInstance
;

pr_DisconnectStatement  :
	#(DisconnectStatement  (pr_SingleOrMultiConnectionSpec )?)
;

pr_SingleOrMultiConnectionSpec :
	#(SingleOrMultiConnectionSpec
		(	(	(	pr_SingleConnectionSpec
					(	pr_ParamClause
					)?
				)
		      | (	pr_AllConnectionsSpec
		      		(	pr_ParamClause
					)?
				)
		      | pr_AllPortsSpec
		      | pr_AllCompsAllPortsSpec
		    )
		)
	)
;

pr_AllConnectionsSpec :
	pr_PortRef
;

pr_AllPortsSpec  :
	pr_ComponentRef 
;
 
pr_AllCompsAllPortsSpec  :
	AllKeyword 
	COMPONENT 
	AllKeyword 
	PortKeyword  
;
   
pr_MapStatement  :
	#(MapStatement  
		pr_SingleConnectionSpec
		(	pr_ParamClause
		)? 
	)
;

pr_ParamClause	:
	#(ParamClause
		pr_FunctionActualParList
	)
;

pr_UnmapStatement  :
	#(UnmapStatement (pr_SingleOrMultiConnectionSpec )? )
;


pr_StartTCStatement  :
	#(StartTCStatement pr_ComponentOrDefaultReference pr_FunctionInstance ) 
;


pr_StopTCStatement {String a;}:
	#(StopTCStatement 
		(	(	pr_ComponentReferenceOrLiteral  STOP
			)
		  |	(	AllKeyword COMPONENT STOP 
		  	)
		  | (	TESTCASE STOP
		  		(	a=pr_FreeText | pr_TemplateInstance
		  		)*
		  	)
		)?
	)
;

pr_ComponentReferenceOrLiteral :
	#(ComponentReferenceOrLiteral
		(
		pr_ComponentOrDefaultReference 
		| pr_MTCOp 
		| pr_SelfOp
		)
	)
;

pr_ComponentIdentifier :
	pr_FunctionInstance | pr_VariableRef
;

/*****  1.6.3.4 - Port Operations  *****/

pr_Port {Symbol a;}:
	#(Port (a=pr_Identifier  ( pr_ArrayOrBitRef )*))
;

pr_CommunicationStatements :
	#(CommunicationStatements
		(
		pr_SendStatement
		| pr_CallStatement
		| pr_ReplyStatement
		| pr_RaiseStatement
		| pr_ReceiveStatement
		| pr_TriggerStatement
		| pr_GetCallStatement
		| pr_GetReplyStatement
		| pr_CatchStatement
		| pr_CheckStatement
		| pr_ClearStatement
		| pr_StartStatement
		| pr_StopStatement
		| pr_HaltStatement // see parser
		| pr_CheckStateStatement // see parser
		)
	)
;

pr_SendStatement  :
	#(SendStatement pr_Port  pr_SendParameter  ( pr_ToClause  )? )
;

pr_SendParameter :
	pr_TemplateInstance
;


pr_ToClause  :
	#(ToClause 
		(	pr_AddressRef 
		  | pr_AddressRefList 
		  | (	AllKeyword 
		  		COMPONENT
		  	) 
		) 
	)
;

pr_AddressRefList  :
	#(AddressRefList pr_AddressRef  (pr_AddressRef )*)
;

pr_AddressRef :
	#(AddressRef (pr_TemplateInstance))
;

pr_CallStatement  :
	#( CallStatement pr_Port pr_CallParameters  ( pr_ToClause )? ( pr_PortCallBody )? )
;

pr_CallParameters  :
	#(CallParameters pr_TemplateInstance  ( pr_CallTimerValue  | NowaitKeyword )? )
;

pr_CallTimerValue  :
	#(CallTimerValue (pr_TimerValue | NowaitKeyword ))
;

pr_PortCallBody  :
	pr_CallBodyStatement  
	( 
		pr_CallBodyStatement  
		(SemiColon )?
	)* 
;


pr_CallBodyStatement  :
	#(CallBodyStatement 
		pr_CallBodyGuard  
		pr_StatementBlock 
	)
;


pr_CallBodyGuard  :
	#(CallBodyGuard pr_AltGuardChar  pr_CallBodyOps )
;


pr_CallBodyOps :
	#(CallBodyOps (pr_GetReplyStatement | pr_CatchStatement))
;


pr_ReplyStatement  :
	#(ReplyStatement 
		pr_Port  
		pr_PortReplyOp 
	)
;

pr_PortReplyOp  :
	#(PortReplyOp
		(	
			pr_TemplateInstance  
			( pr_ReplyValue  )? 
			
			( pr_ToClause )? 
		)
	)
;

pr_ReplyValue  :
	pr_Expression 
;

pr_RaiseStatement  :
	#(RaiseStatement 
		pr_Port 
		pr_PortRaiseOp 
	)
;

pr_PortRaiseOp  :
	#(PortRaiseOp
		(	
			pr_Signature 
			pr_TemplateInstance 
			( pr_ToClause  )? 
		)
	)
;

pr_ReceiveStatement  :
	#(ReceiveStatement pr_PortOrAny pr_PortReceiveOp )
;

pr_PortOrAny  :
	#(PortOrAny (pr_Port | AnyKeyword PortKeyword ))
;

pr_PortReceiveOp  :
	#(PortReceiveOp  ( pr_ReceiveParameter  )? ( pr_FromClause )? ( pr_PortRedirect )? )
;

pr_ReceiveParameter :
  pr_TemplateInstance
;

pr_FromClause  :
	#(FromClause 
		(	pr_AddressRef 
		  | pr_AddressRefList 
		  | (	AnyKeyword 
		  		COMPONENT
		  	) 
		) 
	)
;

pr_PortRedirect  :
	#(PortRedirect  ( pr_ValueSpec  ( pr_SenderSpec )? | pr_SenderSpec ) )
;

pr_SingleValueSpec	{List<Symbol> c;}:
	#(SingleValueSpec
	//TODO: what is to happen here?
		pr_VariableRef
		(	pr_FieldReference
			c=pr_ExtendedFieldReference
		)?
	)
;

pr_ValueSpec  :
	#(ValueSpec 
		(	(pr_VariableRef)=>pr_VariableRef
		  | (	pr_SingleValueSpec
		  	)+
		) 
	)
;

pr_SenderSpec  :
	#(SenderSpec pr_VariableRef  )
;

pr_TriggerStatement  :
	#(TriggerStatement 
		pr_PortOrAny  
		pr_PortTriggerOp 
	)
;

pr_PortTriggerOp  :
	#(PortTriggerOp 
		( pr_ReceiveParameter )? 
		( pr_FromClause )? 
		( pr_PortRedirect )? 
	)
;

pr_GetCallStatement  :
	#(GetCallStatement 
		pr_PortOrAny  
		pr_PortGetCallOp 
	)
;

pr_PortGetCallOp  :
	#(PortGetCallOp ( pr_ReceiveParameter )? 
	( pr_FromClause )? 
	( pr_PortRedirectWithParam )? )
;


pr_PortRedirectWithParam  :
	pr_RedirectWithParamSpec 
;

pr_PortRedirectWithValueAndParam  :
	#(PortRedirectWithValueAndParam pr_RedirectWithValueAndParamSpec )
;

pr_RedirectWithValueAndParamSpec  :
	#(RedirectWithValueAndParamSpec (pr_ValueSpec  (pr_ParaSpec )? (pr_SenderSpec )? | pr_RedirectWithParamSpec ))
;

pr_RedirectWithParamSpec  :
	#( RedirectWithParamSpec ((pr_ParaSpec  (pr_SenderSpec )? | pr_SenderSpec )))
;


pr_ParaSpec  :
	#(ParaSpec pr_ParaAssignmentList )
;

pr_ParaAssignmentList  :
  #(ParaAssignmentList ( pr_AssignmentList  | pr_VariableList ) )
;

pr_AssignmentList  :
	#( AssignmentList ( pr_VariableAssignment  )* )
;

pr_VariableAssignment  :
	#( VariableAssignment pr_VariableRef  pr_ParameterIdentifier )
;

pr_ParameterIdentifier {Symbol a;}:
	a=pr_Identifier
;

pr_VariableList  :
	#(VariableList ( pr_VariableEntry  )* )
;

pr_VariableEntry  :
	#(VariableEntry (pr_VariableRef | NotUsedSymbol ) )
;

pr_GetReplyStatement  :
	#( GetReplyStatement pr_PortOrAny pr_PortGetReplyOp  )
;

pr_PortGetReplyOp  :
	#(PortGetReplyOp 
		( 
			pr_ReceiveParameter  
			( 
				pr_ValueMatchSpec  
			)? 
		)? 
		( pr_FromClause )? 
		( pr_PortRedirectWithValueAndParam )? 
	)
;

pr_ValueMatchSpec  :
	#(ValueMatchSpec pr_TemplateInstance )
;

pr_CheckStatement  :
	#(CheckStatement 
		pr_PortOrAny  
		pr_PortCheckOp 
	)
;

pr_PortCheckOp  :
	#(PortCheckOp 
		( pr_CheckParameter  )? 
	)
;

pr_CheckParameter  :
	#(CheckParameter
		(
			pr_CheckPortOpsPresent 
			| pr_FromClausePresent 
			| pr_RedirectPresent 
		)
	)
;

pr_RedirectPresent  :
	#(RedirectPresent
		pr_SenderSpec 
	)
;

pr_FromClausePresent  :
	#(FromClausePresent
		pr_FromClause 
		( pr_SenderSpec )?
	)
;

pr_CheckPortOpsPresent  :
	#(CheckPortOpsPresent
		(
			pr_PortReceiveOp
			| pr_PortGetCallOp
			| pr_PortGetReplyOp
			| pr_PortCatchOp
		)
	)
;

pr_CatchStatement  :
	#( CatchStatement pr_PortOrAny pr_PortCatchOp ) 
;

pr_PortCatchOp  :
	#( PortCatchOp 
		 
		( pr_CatchOpParameter  )? 
		( pr_FromClause )? 
		( pr_PortRedirect )? 
	)
;

pr_CatchOpParameter  :
	#( CatchOpParameter ( pr_Signature pr_TemplateInstance  | TimeoutKeyword  ) )
;

pr_ClearStatement  :
	#( ClearStatement pr_PortOrAll )
;


pr_PortOrAll :
	#(PortOrAll (pr_Port | pr_AllPort))
;

pr_AllPort  :
	#(AllPort ())
;

pr_StartStatement  :
	#(StartStatement pr_PortOrAll )
;

pr_StopStatement  :
	#(StopStatement pr_PortOrAll )
;

// see parser
pr_HaltStatement  :
	#(HaltStatement pr_PortOrAll )
;

pr_CheckStateStatement 	:
	#(CheckStateStatement
		pr_PortOrAllOrAny
		pr_SingleExpression
	)
;

pr_PortOrAllOrAny	:
	#(PortOrAllOrAny
		(	pr_PortOrAll
		  | AnyKeyword
		)
	)
;

/***** 1.6.2.5 - Timer Operations  *****/

pr_TimerStatements :
	#(TimerStatements (pr_StartTimerStatement | pr_StopTimerStatement | pr_TimeoutStatement))
;

pr_TimerOps :
	#(TimerOps (pr_ReadTimerOp | pr_RunningTimerOp))
;

pr_StartTimerStatement  :
	#(StartTimerStatement pr_TimerRef  ( pr_TimerValue )? )
;

pr_StopTimerStatement  :
	#(StopTimerStatement pr_TimerRefOrAll )
;

pr_TimerRefOrAll  :
	#(TimerRefOrAll (pr_TimerRef | AllKeyword TimerKeyword  ))
;

pr_ReadTimerOp  :
	#(ReadTimerOp pr_TimerRef )
;

pr_RunningTimerOp :
	#( RunningTimerOp pr_TimerRefOrAny )
;

pr_TimeoutStatement  :
	#( TimeoutStatement pr_TimerRefOrAny  )
;

pr_TimerRefOrAny  :
	#(TimerRefOrAny (pr_TimerRef  | AnyKeyword TimerKeyword ))
;

pr_TestcaseOperation {String a;} :
	#(TestcaseOperation
		(	(	a=pr_FreeText
		      | pr_InLineTemplate
		    )*
		)
	)
;

/*****  1.6.3 - Type  *****/

pr_Type[Symbol symbol] {TypeSymbol.PredefinedTypeEnum a; Reference b;} :
	#(Type 
		(
			a=pr_PredefinedType 
			{
				if ((symbol != null) && (symbol instanceof TypeSymbol) ) {
					((SubtypeSymbol)symbol).setType(TypeSymbol.TypeEnum.predefinedType);
					((SubtypeSymbol)symbol).setPredefinedType(a);
				}
			} 
			| b=pr_ReferencedType
			{
				if ((symbol != null) && (symbol instanceof TypeSymbol) ) {
					((SubtypeSymbol)symbol).setType(TypeSymbol.TypeEnum.referencedType);
					((SubtypeSymbol)symbol).setReferencedType(b);
				}
			}
		)
	)
;

pr_PredefinedType returns [TypeSymbol.PredefinedTypeEnum typeEnum=null]:
	#(PredefinedType
		(
			a:BitStringKeyword {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_bitstring;}
			| b:BooleanKeyword {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_boolean;}
			| c:CharStringKeyword {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_charstring;}
			| d:UniversalCharString {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_universalcharstring;}
			| e:IntegerKeyword {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_integer;}
			| f:OctetStringKeyword {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_octetstring;}
			| g:HexStringKeyword {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_hexstring;}
			| h:VerdictTypeKeyword {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_verdicttype;}
			| i:FloatKeyword {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_float;}
			| j:AddressKeyword {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_address;}
			| k:DefaultKeyword {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_default;}
			| l:AnyTypeKeyword {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_anytype;}
			| m:OBJID {typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_objid;}
		)
	)
;


pr_ReferencedType returns [Reference r=new Reference();]{Symbol a,b; List<Symbol> c;}:
	#(ReferencedType
		(
			( ( pr_GlobalModuleId pr_TypeReference ) => a=pr_GlobalModuleId  {r.setModule(a);})?
			b=pr_TypeReference {r.setIdentifier(b);} 
			( (pr_ExtendedFieldReference )=>c=pr_ExtendedFieldReference {r.setExtendedReferences(c);} )?
		)
	)
;

pr_TypeReference returns [Symbol s=null]:
	#( TypeReference ( s=pr_Identifier ( pr_TypeActualParList  )?) )
;


pr_TypeActualParList  :
	#( TypeActualParList ( pr_TypeActualPar )* )
;


pr_TypeActualPar :
	//pr_ConstantExpression
	#(TypeActualPar pr_ConstantExpression) //FIXED: explicit TypeActualPar node is present
;

pr_ArrayDef  :
	#(ArrayDef (pr_ArrayBounds |pr_ArrayDefRange )+ )
;


pr_ArrayDefRange  :
	#(ArrayDefRange pr_ArrayBounds pr_ArrayBounds )
;

pr_ArrayBounds :
	pr_SingleConstExpression
;

/*****1.6.3 - Value  *****/


pr_Value :
	#(Value (pr_ReferencedValue | pr_PredefinedValue))
;

pr_PredefinedValue  :
	#(PredefinedValue
		(
		pr_BitStringValue
		| pr_BooleanValue
		| pr_CharStringValue
		| pr_IntegerValue
		| pr_OctetStringValue
		| pr_ObjectIdentifierValue
		| pr_HexStringValue
		| pr_VerdictTypeValue
		| pr_EnumeratedValue
		| pr_FloatValue
		| pr_NaNValue
		| pr_AddressValue
		| pr_OmitValue
		)
	)
;

pr_FloatValue  :
	#(FloatValue a:FLOATVALUE) 
;

pr_NaNValue :
	#(NaN a:NAN)
;

pr_BitStringValue  :
	#(BitStringValue a:BSTRING) 
;

pr_BooleanValue  :
	#(BooleanValue (a:TRUE | b:FALSE ))
;


pr_IntegerValue  :
	#(IntegerValue b:NUMBER )
;


pr_OctetStringValue  :
	#(OctetStringValue a:OSTRING )
;

pr_ObjIdComponent :
	(pr_NameForm)=>pr_NameForm
	| pr_NumberForm
	| pr_NameAndNumberForm
;

pr_HexStringValue  :
	#(HexStringValue a:HSTRING )
;


pr_VerdictTypeValue :
	#(VerdictTypeValue
		(
		pr_Pass
		| pr_Fail
		| pr_Inconc
		| pr_None
		| pr_Error
		)
	)
;

pr_Pass  :
	a:Pass 
;

pr_Fail  :
	a:Fail 
;
       
pr_Inconc  :
	a:Inconc 
;
            
pr_None  :
	a:None 
;
            
pr_Error  :
	a:Error 
;

pr_EnumeratedValue {Symbol a;}:
	#(EnumeratedValue a=pr_Identifier)
;

pr_CharStringValue  :
	#(CharStringValue (a:CSTRING | pr_Quadruple))
;


pr_Quadruple  :
	#(Quadruple pr_Group  pr_Plane  pr_Row pr_Cell )
;

pr_Group  :
	#(Group a:NUMBER) 
;

pr_Plane  :
	#(Plane a:NUMBER) 
;

pr_Row  :
	#(Row a:NUMBER) 
;

pr_Cell  :
	#(Cell a:NUMBER) 
;

pr_ReferencedValue {List<Symbol> s;} :
	#(ReferencedValue (pr_ValueReference  ( (pr_ExtendedFieldReference)=>s=pr_ExtendedFieldReference )?))
;

pr_ValueReference {Symbol a,b;}:
	#(ValueReference  (a=pr_GlobalModuleId )? b=pr_Identifier )
;

pr_FreeText returns [String s=null] :
	a:CSTRING {s = a.getText();}
;

pr_AddressValue  :
	Null 
;

pr_OmitValue  :
	a:OmitValue 
;

/*****  SECTION A.1.6.6 - Parameterisation  *****/

pr_FormalValuePar {Symbol a; FormalParameterSymbol symbol = new FormalParameterSymbol(); }:
	#(FormalValuePar 
		( InParKeyword  | InOutParKeyword  | OutParKeyword  )? 
		pr_Type[symbol] 
		a=pr_Identifier
		{
			symbol.setName(a.getName());
			symbol.setDeclarationNode(a.getDeclarationNode());
			symbol.setParameterType(FormalParameterSymbol.ParameterEnum.value);
			addSymbolToScope(symbol);
		}
		(	
			pr_Expression
		  | Dash
		)?
	)
;

pr_FormalTypePar {Symbol a;}:
	#( FormalTypePar a=pr_Identifier )
;

pr_FormalPortPar {Symbol a,b; FormalParameterSymbol symbol = new FormalParameterSymbol();}:
	#(FormalPortPar
		(InOutParKeyword)? 
		a=pr_Identifier
		b=pr_Identifier
		{
			symbol.setPortTypeIdentifier(b.getName());
			symbol.setName(a.getName());
			symbol.setDeclarationNode(a.getDeclarationNode());
			symbol.setParameterType(FormalParameterSymbol.ParameterEnum.port);
			addSymbolToScope(symbol);
		}
	) 
;

pr_FormalTimerPar {Symbol a; FormalParameterSymbol symbol = new FormalParameterSymbol();}:
	#(FormalTimerPar 
		(InOutParKeyword)? 
		TimerKeyword 
		a=pr_Identifier 
		{
			symbol.setName(a.getName());
			symbol.setDeclarationNode(a.getDeclarationNode());
			symbol.setParameterType(FormalParameterSymbol.ParameterEnum.timer);
			addSymbolToScope(symbol);
		}
	) 
;

pr_FormalTemplatePar {Symbol a; FormalParameterSymbol symbol = new FormalParameterSymbol();}:
	#(FormalTemplatePar 
		( InParKeyword | OutParKeyword | InOutParKeyword )? 
		( TemplateKeyword | pr_RestrictedTemplate )?
		pr_Type[symbol] 
		a=pr_Identifier
		{
			symbol.setName(a.getName());
			symbol.setDeclarationNode(a.getDeclarationNode());
			symbol.setParameterType(FormalParameterSymbol.ParameterEnum.template);
			addSymbolToScope(symbol);
		}
		(	
			pr_TemplateInstance
		  | Dash
		)?
		
	) 
;

/***** 1.6.6 - The With Statement  *****/

pr_WithStatement 
:
	#( WithStatement  
		( pr_SingleWithAttrib 
		(SemiColon )? )* 
	)
;

pr_SingleWithAttrib:
	#( SingleWithAttrib pr_AttribKeyword ( o:OVERRIDE )? ( pr_AttribQualifier )? pr_AttribSpec )
;

pr_AttribKeyword:
	a:ENCODE  | b:DISPLAY  
	| c:EXTENSION  | d:VARIANT | e:OPTIONAL
;

pr_AttribQualifier  :
	#( AttribQualifier ( pr_DefOrFieldRef  )* )
;


pr_DefOrFieldRef {List<Symbol> c;} :
	#(DefOrFieldRef
		(	(	pr_FieldReference
				(	c=pr_ExtendedFieldReference
				)?
			) 
		  | pr_SingleExpression
		  | pr_DefinitionRef
		  | pr_AllRef
		)
	)
;


pr_DefinitionRef {Symbol a;}:
	( (pr_Identifier pr_Identifier ) => a=pr_FullGroupIdentifier |  a=pr_Identifier)
;

pr_AllRef {List<String> s=null;}:
	#(AllRef
        (
            (
                        GROUP 
                        ( s=pr_GroupRefList )? 
            )
            |
            (
                        TYPE 
                        ( s=pr_TypeRefList )? 
            )
            |
            (
                        TemplateKeyword
                        ( s=pr_TemplateRefList )? 
            )
            |
            (
                        CONST
                        ( s=pr_ConstRefList )? 
            )
            |
            (
                        AltstepKeyword
                        ( s=pr_AltstepRefList )? 
            )
            |
            (
                        TESTCASE
                        ( s=pr_TestcaseRefList )? 
            )
            |
            (
                        FUNCTION
                        ( s=pr_FunctionRefList )? 
            )
            |
            (
                        SIGNATURE
                        ( s=pr_SignatureRefList )? 
            )
            |
            (
                        MODULEPAR
                        ( s=pr_ModuleParRefList )? 
            )
        )
	)
;

pr_AttribSpec {String a;}:
	#(AttribSpec a=pr_FreeText)
;

/*****  1.6.7 - Behaviour Statements  *****/

pr_BehaviourStatements :
	#(BehaviourStatements
		(
		pr_TestcaseInstance
		| pr_FunctionInstance
		| pr_ReturnStatement
		| pr_AltConstruct
		| pr_InterleavedConstruct
		| pr_LabelStatement
		| pr_GotoStatement
		| pr_ActivateOp
		| pr_DeactivateStatement
		| pr_AltstepInstance
		| pr_RepeatStatement
		| pr_BreakStatement
		| pr_ContinueStatement
		)
	)
;

pr_VerdictStatements :
	pr_SetLocalVerdict
;

pr_VerdictOps :
	pr_GetLocalVerdict
;

pr_SetLocalVerdict  :
	#(SetLocalVerdict 
		pr_SingleExpression 
		(	pr_LogItem
		)*
	)
;

pr_GetLocalVerdict  :
	GETVERDICT 
;

pr_SUTStatements  :
	#(SUTStatements 
		(
	      (StringOp)?
		  (pr_Expression)?
		)
	)
;

pr_ActionText {String a;}:
	a=pr_FreeText 
	| pr_Expression
;

pr_ReturnStatement  :
// see parser.
	#( ReturnStatement 
	   ( pr_Expression | pr_InLineTemplate | pr_TemplateRefWithParList | pr_MatchingSymbol )? 
) 
;

pr_AltConstruct  :
	#( AltConstruct 
		(pr_AltGuardList )?
	)
;

pr_AltGuardList  :
	#(AltGuardList 
		( ( pr_GuardStatement | pr_ElseStatement ) (SemiColon)? )*
	)
;

pr_GuardStatement  :
	#(GuardStatement pr_AltGuardChar 
		(
			( 
				pr_AltstepInstance  
				(
					pr_StatementBlock 
				)?
				
			)
			| 
			(
				pr_GuardOp 
				pr_StatementBlock 
			)
		) 
	)
;

pr_ElseStatement  :
	#(ElseStatement 
		pr_StatementBlock 
	)
;

pr_AltGuardChar  :
	#(AltGuardChar ( ( pr_BooleanExpression  )? ) )
;

pr_GuardOp :
	#(GuardOp
		(
		pr_TimeoutStatement
		| pr_ReceiveStatement
		| pr_TriggerStatement
		| pr_GetCallStatement
		| pr_CatchStatement
		| pr_CheckStatement
		| pr_GetReplyStatement
		| pr_DoneStatement
		| pr_KilledStatement
		)
	)
;

pr_InterleavedConstruct  :
	#(InterleavedConstruct 
		( 
			pr_InterleavedGuardElement  
			(SemiColon )? 
		)* 
		
	)
;

pr_InterleavedGuardElement  :
	#(InterleavedGuardElement 
		pr_InterleavedGuard 
		pr_InterleavedAction 
	) 
	
;

pr_InterleavedGuard  :
	#(InterleavedGuard 
		pr_GuardOp 
	)
;

pr_InterleavedAction  :
	#(InterleavedAction  pr_StatementBlock  )
;

pr_LabelStatement {Symbol a;}:
	#(LabelStatement a=pr_Identifier )
;

pr_GotoStatement {Symbol a;}:
	#(GotoStatement a=pr_Identifier )
;

pr_RepeatStatement  :
	#(RepeatStatement 
		REPEAT 
	)
;

pr_BreakStatement  :
	#(BreakStatement 
		BREAK 
	)
;

pr_ContinueStatement  :
	#(ContinueStatement 
		CONTINUE 
	)
;


pr_ActivateOp  :
	#(ActivateOp pr_AltstepInstance )
;


pr_DeactivateStatement  :
	#(DeactivateStatement (pr_ComponentOrDefaultReference )? )

;

/***** 1.6.8 - Basic Statements  *****/

pr_BasicStatements  :
	#(BasicStatements
		(
		pr_Assignment 
		| pr_LogStatement 
		| pr_LoopConstruct 
		| pr_ConditionalConstruct 
		| pr_SelectCaseConstruct 
		| (nodea:pr_StatementBlock
//TODO: check for correctness
//			{ connectToScope(nodea);
//			  passEndOffsetToScopeChildren();
//			  scopeStack.pop()
//			}
		  )
		)
		
	)
;

pr_SelectCaseConstruct  :
	#(SelectCaseConstruct 
		( 
			pr_SingleExpression 
			pr_SelectCaseBody
		)
	)
;

pr_SelectCaseBody  :
	#(SelectCaseBody 
		( pr_SelectCase  )+ 
	)
;  

pr_SelectCase  :
	#(SelectCase 
		(
			( (pr_TemplateInstance)* | ELSE )
			pr_StatementBlock 
		)
	)
;

pr_Expression :
	#(Expression
		(
		pr_SingleExpression
		| pr_CompoundExpression
		)
	)
;

pr_CompoundExpression :
	#(CompoundExpression (pr_FieldExpressionList | pr_ArrayExpression))
;

pr_FieldExpressionList  :
	#( FieldExpressionList  ( pr_FieldExpressionSpec  )* ) 
;

pr_FieldExpressionSpec  :
	#( FieldExpressionSpec pr_FieldReference  #(NotUsedOrExpression (pr_Expression | (NotUsedSymbol  ))))
;

pr_ArrayExpression  :
	#( ArrayExpression (pr_ArrayElementExpressionList)? )
;

pr_ArrayElementExpressionList  :
	#(ArrayElementExpressionList  (  (#(NotUsedOrExpression (pr_Expression | NotUsedSymbol )) )* )) 
;

pr_ConstantExpression  :
	#(ConstantExpression (pr_SingleConstExpression  | pr_CompoundConstExpression ))
;

pr_InnerConstantExpression  :
	#(ConstantExpression (pr_SingleConstExpression  | pr_CompoundConstExpression))
;

pr_SingleConstExpression :
	pr_SingleExpression
;

pr_BooleanExpression :
	#(BooleanExpression pr_SingleExpression)
;

pr_CompoundConstExpression :
	pr_FieldConstExpressionList | pr_ArrayConstExpression
;

pr_FieldConstExpressionList  :
	#( FieldConstExpressionList  ( pr_FieldConstExpressionSpec )* ) 
;

pr_FieldConstExpressionSpec  :
	#( FieldConstExpressionSpec pr_FieldReference  pr_InnerConstantExpression )
;

pr_ArrayConstExpression  :
    #( ArrayConstExpression ( pr_ArrayElementConstExpressionList )? )
;
 
pr_ArrayElementConstExpressionList :
	#( ArrayElementConstExpressionList  ( pr_InnerConstantExpression  )* ) 
;

pr_Assignment  :
	#(Assignment pr_VariableRef  (pr_Expression |pr_TemplateBody )) 
;

/*
  see parser.
  there are two mutually exlusive solutions below:
  - strictly v3.1.1 expressions,
  - efficient expressions.
*/

// ---------------------------------------------------------------------------------------

// strictly v3.1.1 expressions - begin
/*
pr_SingleExpression  :
	#(SingleExpression
			(
				pr_XorExpression 
				(
					pr_XorExpression 
				)*
			)
	 )
;

pr_XorExpression  : 
	#(XorExpression
    		(
    			pr_AndExpression 
    			( 
    		  		pr_AndExpression 
    		 	)*
    		)
	)
;

pr_AndExpression  :
	#(AndExpression
         (
			 pr_NotExpression 
             ( 
              	pr_NotExpression
             )*
          )
	)
;

pr_NotExpression  :
	#(NotExpression
			( 
				( Not )? pr_EqualExpression
			)
	)
;

pr_EqualExpression  :
	#(EqualExpression
			(
				pr_RelExpression 
				( 
					pr_RelExpression 
				)*
			)
	)
;

pr_RelExpression  :
	#(RelExpression
				(
				   pr_ShiftExpression 
				   (
				   #(RelOp
						(
						  Lessthan
						| Morethan
						| Notequals
						| Moreorequal
						| Lessorequal 
						)
					)
				     pr_ShiftExpression 
				    )?
				)
	)
;

pr_ShiftExpression  : 
	#(ShiftExpression
				(
				   pr_BitOrExpression 
				   (
				   #(ShiftOp				   
						(
						  SHIFTLEFT 
						| SHIFTRIGHT  
						| ROTATELEFT 
						| ROTATERIGHT  
						)
				   )
				   pr_BitOrExpression
				   )*
				)
	)
;

pr_BitOrExpression  :
	#(BitOrExpression
 				(
 				    pr_BitXorExpression 
 				    ( 
 				      pr_BitXorExpression 
 				      )*
 				 )
	)
;

pr_BitXorExpression  :
	#(BitXorExpression
 					(
 					   pr_BitAndExpression 
 					   ( 
 					     pr_BitAndExpression 
 					    )*
 					)
	)
;

pr_BitAndExpression  :
	#(BitAndExpression
				(
				   pr_BitNotExpression 
				   ( 
				   pr_BitNotExpression 
				   )*
				)
	)
;

pr_BitNotExpression  :
	#(BitNotExpression
      ( 
       ( Not4b )? pr_AddExpression 
      )
	)
;

pr_AddExpression  : 
	#(AddExpression
				(
				  pr_MulExpression 	 		  
				  ( 
				  #(AddOp
					  ( 
					    Plus
					  | Minus
			 		  | StringOp
			 		  ) 
			      )
				  pr_MulExpression 
				  )*
				)
	 )
;

pr_MulExpression  :
	#(MulExpression
      (
         pr_UnaryExpression 
         (
         #(MultiplyOp
			 ( 
			   Star
			 | Slash
			 | Mod
			 | Rem
			 ) 
	     )
         pr_UnaryExpression 
         )*
      )
	)
;

pr_UnaryExpression  :
	#(UnaryExpression
  		(
	  		#(UnaryOp
		  		  ( 
		  		    Plus 
		  		  | Minus 
		  		  )
		    )
  		)? 
  		pr_Primary
	)
;
*/

// strictly v3.1.1 expressions - end

// ---------------------------------------------------------------------------------------

// efficient expressions - begin

pr_SingleExpression  : 
	#(SingleExpression pr_SimpleExpressions)
;

pr_SimpleExpressions  : 
	(AND4B | XOR4B | OR4B | AND | XOR | OR | STRINGOP | SHIFTOP | EQUAL) => (
		#(AND4B pr_SimpleExpressions pr_SimpleExpressions)  |
		#(XOR4B pr_SimpleExpressions pr_SimpleExpressions)  |
		#(OR4B pr_SimpleExpressions pr_SimpleExpressions)  |
		#(AND pr_SimpleExpressions pr_SimpleExpressions)  |
		#(OR pr_SimpleExpressions pr_SimpleExpressions)  |
		#(XOR pr_SimpleExpressions pr_SimpleExpressions)  |
		#(STRINGOP pr_SimpleExpressions pr_SimpleExpressions)  |
		#(EQUAL pr_SimpleExpressions pr_SimpleExpressions)  
	) 
	| pr_SimpleExpression
;

pr_SimpleExpression  : 
	pr_SubExpression |
	(
		#(LESSTHAN pr_SubExpression pr_SubExpression)  |
		#(MORETHAN pr_SubExpression pr_SubExpression)  |
		#(NOTEQUALS pr_SubExpression pr_SubExpression)  |
		#(MOREOREQUAL pr_SubExpression pr_SubExpression)  |
		#(LESSOREQUAL pr_SubExpression pr_SubExpression) 
	)
;

pr_SubExpression  : 
	pr_Product |
	(
		#(SHIFTLEFT pr_Product pr_SubExpression)  |
		#(SHIFTRIGHT pr_Product pr_SubExpression)  |
		#(ROTATELEFT pr_Product pr_SubExpression)  |
		#(ROTATERIGHT pr_Product pr_SubExpression) 
	)
;

pr_Product  : 
	pr_Term |
	(
		#(PLUS pr_Product pr_Product)  |
		#(MINUS pr_Product pr_Product)  |
		#(STRINGOP pr_Product pr_Product) 
	)
;

pr_Term  : 
	pr_Factor |
	(
		#(STAR pr_Term pr_Term)  |
		#(SLASH pr_Term pr_Term)  |
		#(MOD pr_Term pr_Term)  |
		#(REM pr_Term pr_Term) 
	)
;

pr_Factor  : 
	#(Factor 
		(
			pr_Primary |
			(
				#(PLUS pr_Primary)  |
				#(MINUS pr_Primary)  |
				#(NOT pr_Primary)  |
				#(NOT4B pr_Primary) 
			)
		)
	)
;

// efficient expressions - end

// ---------------------------------------------------------------------------------------

pr_Primary  : 
	#(Primary 
		(
			( pr_OpCall ) => pr_OpCall
			| pr_Value
			| pr_SingleExpression 
		)
	)
;

// see parser
pr_ExtendedFieldReference returns [List<Symbol> r=new ArrayList<Symbol>();] {Symbol a; TypeSymbol.PredefinedTypeEnum t=null;}:
	#(ExtendedFieldReference 
		(	(	(	a=pr_Identifier {r.add(a);} 
		          | t=pr_PredefinedType 
		          | pr_ArrayOrBitRef
		          | NotUsedSymbol
		        ) 
		    )+
		)
	)
;

pr_OpCall  {List<Symbol> c;}:
	#(OpCall
		(
		pr_ConfigurationOps
		| pr_VerdictOps
		| pr_TimerOps
		| pr_TestcaseInstance
		| (	pr_FunctionInstance
				(	c=pr_ExtendedFieldReference
				)?
		  )
		| ( pr_TemplateOps
				(	c=pr_ExtendedFieldReference
				)?
		  )
		| pr_ActivateOp
		)
	)
;

// ---------------------------------------------------------------------------------------

pr_LogItem {String a;}:
	#(LogItem( a=pr_FreeText | pr_TemplateInstance ))
;

pr_LogStatement  :
	#( LogStatement 
		(	pr_LogItem  
			(pr_LogItem )*  
		) 
	)
;


pr_LoopConstruct : 
	#(LoopConstruct (pr_ForStatement | pr_WhileStatement | pr_DoWhileStatement))
;

pr_ForStatement  :
	#(ForStatement  
		( pr_VarInstance  | pr_Assignment  ) 
		pr_BooleanExpression 
		pr_Assignment  
		pr_StatementBlock 
		
	)
;

pr_WhileStatement  :
	#(WhileStatement 
		(
			pr_BooleanExpression 
			pr_StatementBlock 
		)
	)
;

pr_DoWhileStatement  :
	#(DoWhileStatement 
		(
			pr_StatementBlock
			pr_BooleanExpression 
		)
	) 
;

pr_ConditionalConstruct  : 
	#(ConditionalConstruct
		pr_BooleanExpression 
		pr_StatementBlock 
		( pr_ElseIfClause  )* 
		( pr_ElseClause )? 
	)
;

pr_ElseIfClause :
	#(ElseIfClause 
		( 
			pr_BooleanExpression 
			pr_StatementBlock 
		) 
	)
;


pr_ElseClause  :
	#(ElseClause 
		(
			pr_StatementBlock
		)
	)
;

pr_InOutParKeyword  :
	#(InOutParKeyword a:INOUT )
;


