package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;


public class AltstepSymbol extends Symbol {
	
	@Override
	public String getSymbolDescription() {
		StringBuffer result = new StringBuffer();
		HTMLPrinter.getInstance().addPageProlog(result);
		result.append(getVisibilityDescription());
		HTMLPrinter.getInstance().addSmallHeader(result, "Altstep Name: ");
		HTMLPrinter.getInstance().addLine(result, getName());

		if (getGroupName() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Group:");
			HTMLPrinter.getInstance().addLine(result, getGroupName());
		}

		if (getSurroundingScope().getScopeSymbol() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Scope of:");
			result.append(getSurroundingScope().getScopeSymbol().getSymbolDescriptionPlain());
		}

		HTMLPrinter.getInstance().addPageEpilog(result);

		return result.toString();
	}

	@Override
	public String getSymbolDescriptionPlain() {
		return "altstep " + getName();
	}

	@Override
	public String getSymbolDescriptionContentAssist() {
		return getName() + " - altstep";
	}

	
}
