// $ANTLR 2.7.7 (2006-11-01): "TTCN3SymbolTableTreeParser.g" -> "TTCN3SymbolTableTreeParser.java"$

package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import antlr.TreeParser;
import antlr.Token;
import antlr.collections.AST;
import antlr.RecognitionException;
import antlr.ANTLRException;
import antlr.NoViableAltException;
import antlr.MismatchedTokenException;
import antlr.SemanticException;
import antlr.collections.impl.BitSet;
import antlr.ASTPair;
import antlr.collections.impl.ASTArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import de.ugoe.cs.swe.trex.core.TTCN3CorePlugin;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.*;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.importconfiguration.*;
import de.ugoe.cs.swe.trex.core.misc.IntRangeMap;

@SuppressWarnings("unused")


public class TTCN3SymbolTableTreeParser extends antlr.TreeParser       implements TTCN3SymbolTableTreeParserTokenTypes
 {

	ScopeStack scopeStack = new ScopeStack();
	ScopeStack preparedScopeStack = new ScopeStack();
	Stack<String> groupStack = new Stack<String>();
	List<ScopeWithKnownName> knownScopes = new ArrayList<ScopeWithKnownName>();
	List<String> modules = new ArrayList<String>();
	Map<String, Scope> moduleScopeMap = new HashMap<String, Scope>();
	IntRangeMap<Scope> scopeOffsetMap = new IntRangeMap<Scope>();
	String filename = null;
	Scope fileScope = null;
	Symbol tempSymbol = null;
	private ArrayList<RecognitionException> exceptions = new ArrayList<RecognitionException>();

/*	//TODO: consider implementing a check during parsing  
	private ArrayList<Symbol> unresolvableActualParameterSymbols = new ArrayList<Symbol>();
	
	private void addUnresolvableActualParameterSymbol(Symbol s){
		unresolvableActualParameterSymbols.add(s);
	}
	
	private ArrayList<Symbol> getUnresolvableActualParameterSymbols() {
		return unresolvableActualParameterSymbols;
	}
	
	private void resolveActualParameterSymbols(){
		//This may be usable to check the parameters during parsing if necessary
		
		//TODO: move to resolve() in SymbolTable
		for (Symbol s : getUnresolvableActualParameterSymbols()){
			LocationAST instanceNode = LocationAST.resolveParentsUntilType(s.getDeclarationNode(), new int[]{TTCN3ParserTokenTypes.FunctionInstance, TTCN3ParserTokenTypes.AltstepInstance, TTCN3ParserTokenTypes.TestcaseInstance});
			LocationAST identifierNode = (LocationAST) ASTUtil.findChild(instanceNode, TTCN3ParserTokenTypes.Identifier);
			Symbol instanceSymbol = identifierNode.getFirstChild().getSymbol();
//			System.out.println(identifierNode.getFirstChild().getSymbol());
			
			if (instanceSymbol != null && instanceNode != instanceSymbol.getDeclarationNode()){
				//System.out.println(s + " : " +instanceSymbol);
				//System.out.println(instanceSymbol.getDeclarationNode());
				LocationAST defNode = LocationAST.resolveParentsUntilType(instanceSymbol.getDeclarationNode(), new int[]{TTCN3ParserTokenTypes.FunctionDef, TTCN3ParserTokenTypes.AltstepDef, TTCN3ParserTokenTypes.TestcaseDef});
				Symbol targetSymbol = defNode.getScope().getSymbolTable().resolve(s.getName());
				if (targetSymbol != null && (targetSymbol instanceof FormalParameterSymbol)){
					s = targetSymbol;
					//System.out.println(s);
				}
			}
		}
	}
*/	
	
	public void setFilename(String name) {
		filename = name;
	}

	public String getFilename() {
		return filename;
	}

	public IntRangeMap<Scope> getScopeOffsetMap() {
		return scopeOffsetMap;
	}

	public Map<String, Scope> getModuleScopeMap() {
		return moduleScopeMap;
	}

	public List<String> getModules() {
		return modules;
	}

	private Scope pushNewScope(boolean searchParent) {
		Scope newScope;
		if ((scopeStack.empty())) {
			newScope = new Scope();
			newScope.setAssociatedFilename(filename);
		} else {
			newScope = new Scope(scopeStack.peek(), searchParent);
			newScope.setAssociatedFilename(scopeStack.peek().getAssociatedFilename());
			scopeStack.peek().addChild(newScope);
			newScope.setScopeLevel(scopeStack.peek().getScopeLevel()+1);
		}

		scopeStack.push(newScope); 
		
		return newScope;
	}
	
	private void pushPreparedScope() {
		if (!(scopeStack.empty())) {
			preparedScopeStack.peek().setScopeLevel(scopeStack.peek().getScopeLevel()+1);
		}
		scopeStack.push(preparedScopeStack.pop());
	}
	
	private Scope prepareNewScope(boolean searchParent) {
		Scope newScope;
		
		if ((scopeStack.empty())) {
			newScope = new Scope();
			newScope.setAssociatedFilename(filename);
		} else {
			newScope = new Scope(scopeStack.peek(), searchParent);
			newScope.setAssociatedFilename(scopeStack.peek().getAssociatedFilename());
			scopeStack.peek().addChild(newScope);		
		}
		preparedScopeStack.push(newScope);
		return newScope;
	}
	
	private void addSymbolToScope(Symbol s) {
		if (scopeStack.empty())
			return;
		s.setSurroundingScope(scopeStack.peek());
		scopeStack.peek().addSymbol(s);
	}
	
	public void connectKnownScopes() {
//		long time = System.currentTimeMillis();
//		TTCN3UIPlugin.debugMessage("Connecting known scopes...");

		for	(ScopeWithKnownName scopeName : knownScopes) {	
			Scope scope = scopeName.getSearchScope();
			Symbol symbol = scope.resolve(scopeName.getScopeName());
			if (symbol != null) {
				if (scopeName.isSystemScope()) {
					symbol.getScope().setSystemScope(true);
				}
				scopeName.getTargetScope().addKnownScope(symbol.getScope());
			}
		}
		//resolveActualParameterSymbols();
//		TTCN3UIPlugin.debugMessageTimer("...finished in " +  new Long(System.currentTimeMillis()-time) + " ms");
	}
	
	private void addScopeToOffsetMap() {
		if (scopeStack.empty())
			return;

		if ((scopeStack.peek().getStartOffset() == -1) || (scopeStack.peek().getEndOffset() == -1))
			return;

		int startOffset = scopeStack.peek().getStartOffset();
		int endOffset = scopeStack.peek().getEndOffset();
		scopeOffsetMap.put(startOffset, endOffset, scopeStack.peek());
	}
	
	private void connectToScope(AST node) {
		if (!(node instanceof LocationAST)) 
			return;

		LocationAST ast = (LocationAST) node;
		
		ast.setScope(scopeStack.peek());
		updateScopeOffset(node);
	}

	private void updateScopeOffset(AST node) {
		if ((node == null) || (!(node instanceof LocationAST))) 
			return;

		LocationAST ast = (LocationAST) node;

		Scope scope = scopeStack.peek();
		
		if (scopeStack.empty())
			return;
		
		if (scope.getStartOffset() == -1) {
			if (ast.getOffset() == -1) {
				TTCN3CorePlugin.debugMessage("[updatescopeoffset] ERROR! AST Node with missing offsets!");
				TTCN3CorePlugin.debugMessage("[updatescopeoffset] node: " + ast);
			}
			scope.setStartOffset(ast.getOffset());
		}
		
		if (scope.getEndOffset() == -1) {
			if (ast.getOffset() == -1) {
				TTCN3CorePlugin.debugMessage("[updatescopeoffset] ERROR! AST Node with missing offsets!");
				TTCN3CorePlugin.debugMessage("[updatescopeoffset] node: " + ast);
			}
			scope.setEndOffset(ast.getEndOffset());
		}
		
		if ((ast.getOffset() == -1) || (scope.getStartOffset() == -1)) {
			TTCN3CorePlugin.debugMessage("[updatescopeoffset] ERROR! AST Node with missing offsets!");
			TTCN3CorePlugin.debugMessage("[updatescopeoffset] node: " + ast);
			return;
		}
		
		
		if (ast.getOffset() < scope.getStartOffset()) {
			scope.setStartOffset(ast.getOffset());
		}
		
		if ((ast.getEndOffset() == -1) || (scope.getEndOffset() == -1)) {
			TTCN3CorePlugin.debugMessage("[updatescopeoffset] ERROR! AST Node with missing offsets!");
			TTCN3CorePlugin.debugMessage("[updatescopeoffset] node: " + ast);
			return;
		}
		
		if (ast.getEndOffset() > scope.getEndOffset() ) {
			scope.setEndOffset(ast.getEndOffset());
		}

	}
	
	private void popToScope(Scope t) {
		if (scopeStack.empty())
			return;
		while (scopeStack.peek() != t) {
			scopeStack.peek().setEndOffset(t.getEndOffset());
			scopeStack.pop();
		}
	}
	
	private void passEndOffsetToScopeChildren(Scope s, int offset) {
		s.setEndOffset(offset);
		for(Scope scope : s.getChildren()) {
			passEndOffsetToScopeChildren(scope, offset);
		}
	}

	private void passEndOffsetToScopeChildren() {
		passEndOffsetToScopeChildren(scopeStack.peek(), scopeStack.peek().getEndOffset());
	}
	
	public Scope getFileScope() {
		return fileScope;
	}
	
	public void reportError(RecognitionException ex) {
    	exceptions.add(ex);
    }
	
	public ArrayList<RecognitionException> getExceptions() {
		return exceptions;
	}
	
	private void setImportVisibility(AST node, AbstractImport imp){
		LocationAST moduleDefinitionNode = LocationAST.resolveParentsUntilType((LocationAST) node, TTCN3ParserTokenTypes.ModuleDefinition);
		
		if (moduleDefinitionNode == null){
			return;
		}
		
		if (moduleDefinitionNode.getFirstChild().getType()==TTCN3ParserTokenTypes.Visibility){
			switch (moduleDefinitionNode.getFirstChild().getFirstChild().getType()) {
			case TTCN3ParserTokenTypes.PUBLIC:
				imp.setPublic();
				break;
			case TTCN3ParserTokenTypes.PRIVATE:
				imp.setPrivate();
				break;
			case TTCN3ParserTokenTypes.FRIEND:
				imp.setFriend();
				break;

			default:
				break;
			}
		}
		
	}
	
	private void setVisibility(Symbol s){
		if (s==null){
			//TODO: seems to be an issue with record: when there is no identifier
			return;	
		}
		LocationAST declarationNode = s.getDeclarationNode(); 
		if (declarationNode == null){
			return;
		}
		
		LocationAST moduleDefinitionNode = LocationAST.resolveParentsUntilType(declarationNode, TTCN3ParserTokenTypes.ModuleDefinition);
		
		if (moduleDefinitionNode == null){
			return;
		}
		
		if (moduleDefinitionNode.getFirstChild().getType()==TTCN3ParserTokenTypes.Visibility){
			switch (moduleDefinitionNode.getFirstChild().getFirstChild().getType()) {
			case TTCN3ParserTokenTypes.PUBLIC:
				s.setPublic();
				break;
			case TTCN3ParserTokenTypes.PRIVATE:
				s.setPrivate();
				break;
			case TTCN3ParserTokenTypes.FRIEND:
				s.setFriend();
				break;

			default:
				break;
			}
		}
	}
		
public TTCN3SymbolTableTreeParser() {
	tokenNames = _tokenNames;
}

	public final void pr_TTCN3File(AST _t) throws RecognitionException {
		
		AST pr_TTCN3File_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST nodea = null;
		fileScope = pushNewScope(false);
		
		try {      // for error handling
			AST __t2 = _t;
			AST tmp1_AST_in = (AST)_t;
			match(_t,TTCN3File);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
				((LocationAST)tmp1_AST_in).setScope(fileScope);
				
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case EOF:
			{
				AST tmp2_AST_in = (AST)_t;
				match(_t,Token.EOF_TYPE);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			case TTCN3Module:
			{
				{
				{
				_loop6:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==TTCN3Module)) {
						nodea = _t==ASTNULL ? null : (AST)_t;
						pr_TTCN3Module(_t);
						_t = _retTree;
						if ( inputState.guessing==0 ) {
							connectToScope(nodea);
						}
					}
					else {
						break _loop6;
					}
					
				} while (true);
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t2;
			_t = _t.getNextSibling();
			if ( inputState.guessing==0 ) {
				
				fileScope.setStartOffset(0);
				//  addScopeToOffsetMap();
				scopeStack.pop();
				
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TTCN3Module(AST _t) throws RecognitionException {
		
		AST pr_TTCN3Module_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST n = null;
		AST id = null;
		AST nodea = null;
		AST nodeb = null;
		AST nodec = null;
		ModuleSymbol symbol = new ModuleSymbol(); Symbol a;
		
		try {      // for error handling
			AST __t8 = _t;
			n = _t==ASTNULL ? null :(AST)_t;
			match(_t,TTCN3Module);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				prepareNewScope(false);
			}
			id = _t==ASTNULL ? null : (AST)_t;
			a=pr_TTCN3ModuleId(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName());
							symbol.setDeclarationNode(a.getDeclarationNode());
							modules.add(a.getName());
							moduleScopeMap.put(a.getName(), preparedScopeStack.peek());
							symbol.setScope(a.getScope());
							addSymbolToScope(symbol);
							preparedScopeStack.peek().setScopeSymbol(symbol);
							ImportConfiguration importConf = new ImportConfiguration();
							importConf.setModuleName(a.getName());
							preparedScopeStack.peek().setImportConfiguration(importConf);
				
							pushPreparedScope();
							((LocationAST)n).setScope(scopeStack.peek());
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ModuleDefinitionsPart:
			{
				nodea = _t==ASTNULL ? null : (AST)_t;
				pr_ModuleDefinitionsPart(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodea);
				}
				break;
			}
			case 3:
			case ModuleControlPart:
			case SemiColon:
			case WithStatement:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ModuleControlPart:
			{
				nodeb = _t==ASTNULL ? null : (AST)_t;
				pr_ModuleControlPart(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodeb);
				}
				break;
			}
			case 3:
			case SemiColon:
			case WithStatement:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case WithStatement:
			{
				nodec = _t==ASTNULL ? null : (AST)_t;
				pr_WithStatement(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodec);
				}
				break;
			}
			case 3:
			case SemiColon:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				
							scopeStack.peek().setStartOffset(((LocationAST)id).getEndOffset());
							scopeStack.peek().setEndOffset(((LocationAST)n).getEndOffset());
							addScopeToOffsetMap();
							scopeStack.pop();
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SemiColon:
			{
				AST tmp3_AST_in = (AST)_t;
				match(_t,SemiColon);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t8;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final Symbol  pr_TTCN3ModuleId(AST _t) throws RecognitionException {
		Symbol s=null;
		
		AST pr_TTCN3ModuleId_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t18 = _t;
			AST tmp4_AST_in = (AST)_t;
			match(_t,TTCN3ModuleId);
			_t = _t.getFirstChild();
			s=pr_ModuleId(_t);
			_t = _retTree;
			_t = __t18;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
/************************* 1.6.1 - Module Definitions Part *********************/
	public final void pr_ModuleDefinitionsPart(AST _t) throws RecognitionException {
		
		AST pr_ModuleDefinitionsPart_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t33 = _t;
			AST tmp5_AST_in = (AST)_t;
			match(_t,ModuleDefinitionsPart);
			_t = _t.getFirstChild();
			AST __t34 = _t;
			AST tmp6_AST_in = (AST)_t;
			match(_t,ModuleDefinitionList);
			_t = _t.getFirstChild();
			{
			int _cnt38=0;
			_loop38:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ModuleDefinition)) {
					{
					pr_ModuleDefinition(_t);
					_t = _retTree;
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp7_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case ModuleDefinition:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
					}
				}
				else {
					if ( _cnt38>=1 ) { break _loop38; } else {throw new NoViableAltException(_t);}
				}
				
				_cnt38++;
			} while (true);
			}
			_t = __t34;
			_t = _t.getNextSibling();
			_t = __t33;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.2 - Control Part  *****/
	public final void pr_ModuleControlPart(AST _t) throws RecognitionException {
		
		AST pr_ModuleControlPart_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST n = null;
		Scope scopeToPop=null;
		
		try {      // for error handling
			AST __t811 = _t;
			n = _t==ASTNULL ? null :(AST)_t;
			match(_t,ModuleControlPart);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
						 	prepareNewScope(true);
							pushPreparedScope(); 
							scopeToPop = scopeStack.peek();
							((LocationAST)n).setScope(scopeStack.peek());
						
			}
			pr_ModuleControlBody(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							popToScope(scopeToPop);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case WithStatement:
			{
				pr_WithStatement(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case SemiColon:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SemiColon:
			{
				AST tmp8_AST_in = (AST)_t;
				match(_t,SemiColon);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				
							scopeStack.peek().setStartOffset(((LocationAST)n).getOffset());
							scopeStack.peek().setEndOffset(((LocationAST)n).getEndOffset());
						 	passEndOffsetToScopeChildren(); 
							
				//			addScopeToOffsetMap();
						 	scopeStack.pop(); 
						
			}
			_t = __t811;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.6 - The With Statement  *****/
	public final void pr_WithStatement(AST _t) throws RecognitionException {
		
		AST pr_WithStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1287 = _t;
			AST tmp9_AST_in = (AST)_t;
			match(_t,WithStatement);
			_t = _t.getFirstChild();
			{
			_loop1290:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==SingleWithAttrib)) {
					pr_SingleWithAttrib(_t);
					_t = _retTree;
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp10_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case SingleWithAttrib:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop1290;
				}
				
			} while (true);
			}
			_t = __t1287;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final Symbol  pr_ModuleId(AST _t) throws RecognitionException {
		Symbol s=null;
		
		AST pr_ModuleId_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		String a;
		
		try {      // for error handling
			AST __t14 = _t;
			AST tmp11_AST_in = (AST)_t;
			match(_t,ModuleId);
			_t = _t.getFirstChild();
			{
			s=pr_GlobalModuleId(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case LanguageSpec:
			{
				a=pr_LanguageSpec(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t14;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final Symbol  pr_GlobalModuleId(AST _t) throws RecognitionException {
		Symbol s=null;
		
		AST pr_GlobalModuleId_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t678 = _t;
			AST tmp12_AST_in = (AST)_t;
			match(_t,GlobalModuleId);
			_t = _t.getFirstChild();
			{
			s=pr_Identifier(_t);
			_t = _retTree;
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case OBJID:
			{
				pr_ObjectIdentifierValue(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t678;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final String  pr_LanguageSpec(AST _t) throws RecognitionException {
		String s=null;
		
		AST pr_LanguageSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t674 = _t;
			AST tmp13_AST_in = (AST)_t;
			match(_t,LanguageSpec);
			_t = _t.getFirstChild();
			{
			int _cnt676=0;
			_loop676:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==CSTRING)) {
					s=pr_FreeText(_t);
					_t = _retTree;
				}
				else {
					if ( _cnt676>=1 ) { break _loop676; } else {throw new NoViableAltException(_t);}
				}
				
				_cnt676++;
			} while (true);
			}
			_t = __t674;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final void pr_DefinitiveObjIdComponent(AST _t) throws RecognitionException {
		
		AST pr_DefinitiveObjIdComponent_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case NameForm:
			{
				pr_NameForm(_t);
				_t = _retTree;
				break;
			}
			case Number:
			{
				pr_DefinitiveNumberForm(_t);
				_t = _retTree;
				break;
			}
			case DefinitiveNameAndNumberForm:
			{
				pr_DefinitiveNameAndNumberForm(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_NameForm(AST _t) throws RecognitionException {
		
		AST pr_NameForm_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t691 = _t;
			AST tmp14_AST_in = (AST)_t;
			match(_t,NameForm);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,IDENTIFIER);
			_t = _t.getNextSibling();
			_t = __t691;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_DefinitiveNumberForm(AST _t) throws RecognitionException {
		
		AST pr_DefinitiveNumberForm_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_Number(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_DefinitiveNameAndNumberForm(AST _t) throws RecognitionException {
		
		AST pr_DefinitiveNameAndNumberForm_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t24 = _t;
			AST tmp15_AST_in = (AST)_t;
			match(_t,DefinitiveNameAndNumberForm);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			pr_DefinitiveNumberForm(_t);
			_t = _retTree;
			_t = __t24;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Number(AST _t) throws RecognitionException {
		
		AST pr_Number_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t21 = _t;
			AST tmp16_AST_in = (AST)_t;
			match(_t,Number);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,NUMBER);
			_t = _t.getNextSibling();
			_t = __t21;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final Symbol  pr_Identifier(AST _t) throws RecognitionException {
		Symbol s=new Symbol();;
		
		AST pr_Identifier_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t744 = _t;
			AST tmp17_AST_in = (AST)_t;
			match(_t,Identifier);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,IDENTIFIER);
			_t = _t.getNextSibling();
			_t = __t744;
			_t = _t.getNextSibling();
			if ( inputState.guessing==0 ) {
				
						s.setName(a.getText());
						s.setDeclarationNode((LocationAST) a);
						if (!(groupStack.empty()))
							s.setGroupName(groupStack.peek());
						if (!(scopeStack.empty())) {
							if (!(preparedScopeStack.empty())) {
								LocationAST ast = (LocationAST) a;
								ast.setScope(scopeStack.peek()); 
								ast.setChildScope(preparedScopeStack.peek());
								s.setScope(preparedScopeStack.peek());
								s.setSurroundingScope(scopeStack.peek());
								s.setDeclarationNode(ast);
							} else {
								LocationAST ast = (LocationAST) a;
								ast.setScope(scopeStack.peek()); 
								s.setSurroundingScope(scopeStack.peek());
								s.setDeclarationNode(ast);
							}
						}
					
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final void pr_ModulePar(AST _t) throws RecognitionException {
		
		AST pr_ModulePar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		TypeSymbol typeSymbol = new TypeSymbol();
		
		try {      // for error handling
			AST __t26 = _t;
			AST tmp18_AST_in = (AST)_t;
			match(_t,ModulePar);
			_t = _t.getFirstChild();
			pr_Type(_t,typeSymbol);
			_t = _retTree;
			pr_ModuleParList(_t,typeSymbol);
			_t = _retTree;
			_t = __t26;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/*****  1.6.3 - Type  *****/
	public final void pr_Type(AST _t,
		Symbol symbol
	) throws RecognitionException {
		
		AST pr_Type_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		TypeSymbol.PredefinedTypeEnum a; Reference b;
		
		try {      // for error handling
			AST __t1179 = _t;
			AST tmp19_AST_in = (AST)_t;
			match(_t,Type);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case PredefinedType:
			{
				a=pr_PredefinedType(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					
									if ((symbol != null) && (symbol instanceof TypeSymbol) ) {
										((SubtypeSymbol)symbol).setType(TypeSymbol.TypeEnum.predefinedType);
										((SubtypeSymbol)symbol).setPredefinedType(a);
									}
								
				}
				break;
			}
			case ReferencedType:
			{
				b=pr_ReferencedType(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					
									if ((symbol != null) && (symbol instanceof TypeSymbol) ) {
										((SubtypeSymbol)symbol).setType(TypeSymbol.TypeEnum.referencedType);
										((SubtypeSymbol)symbol).setReferencedType(b);
									}
								
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1179;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ModuleParList(AST _t,
		TypeSymbol typeSymbol
	) throws RecognitionException {
		
		AST pr_ModuleParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t28 = _t;
			AST tmp20_AST_in = (AST)_t;
			match(_t,ModuleParList);
			_t = _t.getFirstChild();
			{
			int _cnt31=0;
			_loop31:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Identifier)) {
					a=pr_Identifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						
										//fixes issue with module parameters being assigned incorrect symbols
										ModuleParameterSymbol symbol = new ModuleParameterSymbol();
										symbol.setName(a.getName());
										symbol.setDeclarationNode(a.getDeclarationNode());
						
						//TODO: This appears redundant
						//				if ((typeSymbol != null) && (typeSymbol.getPredefinedType() != null)) {
						//					symbol.setPredefinedType(typeSymbol.getPredefinedType());
						//				}
										symbol.setReferencedType(typeSymbol.getReferencedType());
										symbol.setPredefinedType(typeSymbol.getPredefinedType());
										setVisibility(symbol);
										addSymbolToScope(symbol);
										//tempSymbol = symbol;
									
					}
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case ConstantExpression:
					{
						pr_ConstantExpression(_t);
						_t = _retTree;
						break;
					}
					case 3:
					case Identifier:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					if ( _cnt31>=1 ) { break _loop31; } else {throw new NoViableAltException(_t);}
				}
				
				_cnt31++;
			} while (true);
			}
			_t = __t28;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ConstantExpression(AST _t) throws RecognitionException {
		
		AST pr_ConstantExpression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1444 = _t;
			AST tmp21_AST_in = (AST)_t;
			match(_t,ConstantExpression);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SingleExpression:
			{
				pr_SingleConstExpression(_t);
				_t = _retTree;
				break;
			}
			case ArrayConstExpression:
			case FieldConstExpressionList:
			{
				pr_CompoundConstExpression(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1444;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ModuleDefinition(AST _t) throws RecognitionException {
		
		AST pr_ModuleDefinition_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t40 = _t;
			AST tmp22_AST_in = (AST)_t;
			match(_t,ModuleDefinition);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Visibility:
			{
				pr_Visibility(_t);
				_t = _retTree;
				break;
			}
			case AltstepDef:
			case ConstDef:
			case ExtConstDef:
			case ExtFunctionDef:
			case FriendModuleDef:
			case FunctionDef:
			case GroupDef:
			case ImportDef:
			case ModuleParDef:
			case SignatureDef:
			case TemplateDef:
			case TestcaseDef:
			case TypeDef:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TypeDef:
			{
				pr_TypeDef(_t);
				_t = _retTree;
				break;
			}
			case ConstDef:
			{
				pr_ConstDef(_t);
				_t = _retTree;
				break;
			}
			case TemplateDef:
			{
				pr_TemplateDef(_t);
				_t = _retTree;
				break;
			}
			case FunctionDef:
			{
				pr_FunctionDef(_t);
				_t = _retTree;
				break;
			}
			case SignatureDef:
			{
				pr_SignatureDef(_t);
				_t = _retTree;
				break;
			}
			case TestcaseDef:
			{
				pr_TestcaseDef(_t);
				_t = _retTree;
				break;
			}
			case ModuleParDef:
			{
				pr_ModuleParDef(_t);
				_t = _retTree;
				break;
			}
			case ImportDef:
			{
				pr_ImportDef(_t);
				_t = _retTree;
				break;
			}
			case GroupDef:
			{
				pr_GroupDef(_t);
				_t = _retTree;
				break;
			}
			case ExtFunctionDef:
			{
				pr_ExtFunctionDef(_t);
				_t = _retTree;
				break;
			}
			case ExtConstDef:
			{
				pr_ExtConstDef(_t);
				_t = _retTree;
				break;
			}
			case AltstepDef:
			{
				pr_AltstepDef(_t);
				_t = _retTree;
				break;
			}
			case FriendModuleDef:
			{
				pr_FriendModuleDef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case WithStatement:
			{
				pr_WithStatement(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t40;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Visibility(AST _t) throws RecognitionException {
		
		AST pr_Visibility_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t45 = _t;
			AST tmp23_AST_in = (AST)_t;
			match(_t,Visibility);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case PUBLIC:
			{
				AST tmp24_AST_in = (AST)_t;
				match(_t,PUBLIC);
				_t = _t.getNextSibling();
				break;
			}
			case PRIVATE:
			{
				AST tmp25_AST_in = (AST)_t;
				match(_t,PRIVATE);
				_t = _t.getNextSibling();
				break;
			}
			case FRIEND:
			{
				AST tmp26_AST_in = (AST)_t;
				match(_t,FRIEND);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t45;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.1.1 - Typedef Definitions *****/
	public final void pr_TypeDef(AST _t) throws RecognitionException {
		
		AST pr_TypeDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t52 = _t;
			AST tmp27_AST_in = (AST)_t;
			match(_t,TypeDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case StructuredTypeDef:
			{
				pr_StructuredTypeDef(_t);
				_t = _retTree;
				break;
			}
			case SubTypeDef:
			{
				pr_SubTypeDef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t52;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.1.2 - Constant Definitions  *****/
	public final void pr_ConstDef(AST _t) throws RecognitionException {
		
		AST pr_ConstDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		ConstantSymbol symbol = new ConstantSymbol();
		
		try {      // for error handling
			AST __t264 = _t;
			AST tmp28_AST_in = (AST)_t;
			match(_t,ConstDef);
			_t = _t.getFirstChild();
			pr_Type(_t,symbol);
			_t = _retTree;
			{
			_loop266:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==SingleConstDef)) {
					pr_SingleConstDef(_t,symbol);
					_t = _retTree;
				}
				else {
					break _loop266;
				}
				
			} while (true);
			}
			_t = __t264;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.1.3 - Template Definitions  *****/
	public final void pr_TemplateDef(AST _t) throws RecognitionException {
		
		AST pr_TemplateDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST upperx = null;
		AST nodea = null;
		
		try {      // for error handling
			AST __t271 = _t;
			upperx = _t==ASTNULL ? null :(AST)_t;
			match(_t,TemplateDef);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				prepareNewScope(true);
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateRestriction:
			{
				pr_TemplateRestriction(_t);
				_t = _retTree;
				break;
			}
			case BaseTemplate:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				((LocationAST)upperx).setScope(scopeStack.peek());
			}
			pr_BaseTemplate(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case DerivedDef:
			{
				pr_DerivedDef(_t);
				_t = _retTree;
				break;
			}
			case TemplateBody:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			nodea = _t==ASTNULL ? null : (AST)_t;
			pr_TemplateBody(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				connectToScope(nodea);
			}
			if ( inputState.guessing==0 ) {
					
						//	popToScope(scopeToPop); 
						 	passEndOffsetToScopeChildren(); 
							
				//			addScopeToOffsetMap();
							scopeStack.pop(); 
						
			}
			_t = __t271;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.1.4 - Function Definitions  *****/
	public final void pr_FunctionDef(AST _t) throws RecognitionException {
		
		AST pr_FunctionDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST upper = null;
		AST nodea = null;
		AST nodeb = null;
		AST nodec = null;
		AST noded = null;
		Symbol a; FunctionSymbol symbol = new FunctionSymbol(); Reference r; Scope preparedScope=null; Scope lastScope=null; Scope scopeToPop=null;
		
		try {      // for error handling
			AST __t446 = _t;
			upper = _t==ASTNULL ? null :(AST)_t;
			match(_t,FunctionDef);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				prepareNewScope(true);
			}
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
						 	symbol.setName(a.getName()); 
							symbol.setDeclarationNode(a.getDeclarationNode());
							symbol.setScope(a.getScope());
							symbol.setGroupName(a.getGroupName());
							setVisibility(symbol);
							addSymbolToScope(symbol); 
							preparedScopeStack.peek().setScopeSymbol(symbol);
						
			}
			if ( inputState.guessing==0 ) {
				
						 	lastScope = scopeStack.peek();
						 	preparedScope = preparedScopeStack.peek();
						 	pushPreparedScope(); 
						 	scopeToPop = scopeStack.peek();
						 	((LocationAST)upper).setScope(scopeStack.peek());			
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FormalPortPar:
			case FormalTemplatePar:
			case FormalTimerPar:
			case FormalValuePar:
			{
				nodea = _t==ASTNULL ? null : (AST)_t;
				pr_FunctionFormalParList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodea);
				}
				break;
			}
			case ReturnType:
			case RunsOnSpec:
			case StatementBlock:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case RunsOnSpec:
			{
				nodeb = _t==ASTNULL ? null : (AST)_t;
				r=pr_RunsOnSpec(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodeb);
				}
				if ( inputState.guessing==0 ) {
					
							 		if (r != null) {
							 			ScopeWithKnownName scopeWithName = new ScopeWithKnownName(lastScope, preparedScope, r.getIdentifier().getName(), false);
							 			knownScopes.add(scopeWithName);
							 		}
							 	
				}
				break;
			}
			case ReturnType:
			case StatementBlock:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ReturnType:
			{
				nodec = _t==ASTNULL ? null : (AST)_t;
				pr_ReturnType(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodec);
				}
				break;
			}
			case StatementBlock:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			noded = _t==ASTNULL ? null : (AST)_t;
			pr_StatementBlock(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
						 	//TODO: What is this one for
						 	//scopeStack.peek().setEndOffset(((LocationAST)#upper).getEndOffset());
						 	popToScope(scopeToPop); 
						 	connectToScope(noded); 
						 	passEndOffsetToScopeChildren(); 
						
			}
			if ( inputState.guessing==0 ) {
				
				//			addScopeToOffsetMap();
						 	scopeStack.pop(); 
						
			}
			_t = __t446;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/*****1.6.2.5 - Signature Definitions  *****/
	public final void pr_SignatureDef(AST _t) throws RecognitionException {
		
		AST pr_SignatureDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; SignatureSymbol symbol = new SignatureSymbol();
		
		try {      // for error handling
			AST __t521 = _t;
			AST tmp29_AST_in = (AST)_t;
			match(_t,SignatureDef);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName());
							symbol.setDeclarationNode(a.getDeclarationNode());
							symbol.setGroupName(a.getGroupName());
							symbol.setScope(a.getScope());
							setVisibility(symbol);
							addSymbolToScope(symbol); 
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SignatureFormalParList:
			{
				pr_SignatureFormalParList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case ExceptionSpec:
			case NoBlockKeyword:
			case ReturnType:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ReturnType:
			{
				pr_ReturnType(_t);
				_t = _retTree;
				break;
			}
			case NoBlockKeyword:
			{
				AST tmp30_AST_in = (AST)_t;
				match(_t,NoBlockKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			case ExceptionSpec:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ExceptionSpec:
			{
				pr_ExceptionSpec(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t521;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.2.6 - Testcase Definitions  *****/
	public final void pr_TestcaseDef(AST _t) throws RecognitionException {
		
		AST pr_TestcaseDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST upper = null;
		AST nodea = null;
		AST nodeb = null;
		AST nodec = null;
		Symbol a; TestcaseSymbol symbol = new TestcaseSymbol(); List<Reference> reflist; Scope lastScope=null; Scope preparedScope=null; Scope scopeToPop=null;
		
		try {      // for error handling
			AST __t541 = _t;
			upper = _t==ASTNULL ? null :(AST)_t;
			match(_t,TestcaseDef);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				prepareNewScope(true);
			}
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName()); 
							symbol.setDeclarationNode(a.getDeclarationNode());
							symbol.setScope(a.getScope());
							symbol.setGroupName(a.getGroupName());
							setVisibility(symbol);
							addSymbolToScope(symbol); 
							preparedScopeStack.peek().setScopeSymbol(symbol);
						
			}
			if ( inputState.guessing==0 ) {
				
							lastScope = scopeStack.peek();
							preparedScope = preparedScopeStack.peek();
							pushPreparedScope();
							scopeToPop = scopeStack.peek();
							((LocationAST)upper).setScope(scopeStack.peek());
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FormalTemplatePar:
			case FormalValuePar:
			{
				nodea = _t==ASTNULL ? null : (AST)_t;
				pr_TestcaseFormalParList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodea);
				}
				break;
			}
			case ConfigSpec:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			nodeb = _t==ASTNULL ? null : (AST)_t;
			reflist=pr_ConfigSpec(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				connectToScope(nodeb);
			}
			if ( inputState.guessing==0 ) {
				
							if (reflist.size() > 0) {
								for (int i=0; i < reflist.size(); i ++) {
									Reference r=reflist.get(i);
									if (r!=null && r.getIdentifier()!=null){
										ScopeWithKnownName scopeWithName = new ScopeWithKnownName(lastScope, preparedScope, r.getIdentifier().getName(), r.isSystemReference());
										knownScopes.add(scopeWithName);
									}
								}
							}
						
			}
			nodec = _t==ASTNULL ? null : (AST)_t;
			pr_StatementBlock(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							popToScope(scopeToPop); 
							connectToScope(nodec); 
							passEndOffsetToScopeChildren(); 
						
			}
			if ( inputState.guessing==0 ) {
				
				//			addScopeToOffsetMap();
							scopeStack.pop(); 
						
			}
			_t = __t541;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.1.12 - Module parameter definitions *****/
	public final void pr_ModuleParDef(AST _t) throws RecognitionException {
		
		AST pr_ModuleParDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t801 = _t;
			AST tmp31_AST_in = (AST)_t;
			match(_t,ModuleParDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ModulePar:
			{
				{
				int _cnt804=0;
				_loop804:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==ModulePar)) {
						pr_ModulePar(_t);
						_t = _retTree;
					}
					else {
						if ( _cnt804>=1 ) { break _loop804; } else {throw new NoViableAltException(_t);}
					}
					
					_cnt804++;
				} while (true);
				}
				break;
			}
			case MultitypedModuleParList:
			{
				pr_MultitypedModuleParList(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t801;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.1.8 - Import Definitions  *****/
	public final void pr_ImportDef(AST _t) throws RecognitionException {
		
		AST pr_ImportDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST upper = null;
		AbstractImport imp=null; Symbol a;
		
		try {      // for error handling
			AST __t603 = _t;
			upper = _t==ASTNULL ? null :(AST)_t;
			match(_t,ImportDef);
			_t = _t.getFirstChild();
			{
			a=pr_ImportFromSpec(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AllWithExcepts:
			{
				imp=pr_AllWithExcepts(_t);
				_t = _retTree;
				break;
			}
			case ImportSpec:
			{
				imp=pr_ImportSpec(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			if ( inputState.guessing==0 ) {
				
							if (imp != null) {
								imp.setModuleName(a.getName());
								setImportVisibility(upper, imp);
								scopeStack.peek().getImportConfiguration().addImport(imp);
							}
						
			}
			_t = __t603;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/*****  1.6.1.9 - Group Definitions  *****/
	public final void pr_GroupDef(AST _t) throws RecognitionException {
		
		AST pr_GroupDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST upper = null;
		Symbol a; GroupSymbol symbol = new GroupSymbol();
		
		try {      // for error handling
			AST __t790 = _t;
			upper = _t==ASTNULL ? null :(AST)_t;
			match(_t,GroupDef);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName());
							symbol.setDeclarationNode(a.getDeclarationNode());
							addSymbolToScope(symbol);
							groupStack.push(a.getName());
							((LocationAST)upper).setScope(scopeStack.peek());
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ModuleDefinitionsPart:
			{
				pr_ModuleDefinitionsPart(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				groupStack.pop();
			}
			_t = __t790;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.1.10 - External Function Definitions  *****/
	public final void pr_ExtFunctionDef(AST _t) throws RecognitionException {
		
		AST pr_ExtFunctionDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST upper = null;
		AST nodea = null;
		AST nodec = null;
		Symbol a; FunctionSymbol symbol = new FunctionSymbol(); Scope preparedScope=null; Scope lastScope=null; Scope scopeToPop=null;
		
		try {      // for error handling
			AST __t793 = _t;
			upper = _t==ASTNULL ? null :(AST)_t;
			match(_t,ExtFunctionDef);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				prepareNewScope(true);
			}
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName());
							symbol.setDeclarationNode(a.getDeclarationNode());
							symbol.setGroupName(a.getGroupName());
							symbol.setExternal(true);
							setVisibility(symbol);
							addSymbolToScope(symbol);
							preparedScopeStack.peek().setScopeSymbol(symbol);
							lastScope = scopeStack.peek();
						 	preparedScope = preparedScopeStack.peek();
						 	pushPreparedScope(); 
						 	scopeToPop = scopeStack.peek();
						 	((LocationAST)upper).setScope(scopeStack.peek());
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FormalPortPar:
			case FormalTemplatePar:
			case FormalTimerPar:
			case FormalValuePar:
			{
				nodea = _t==ASTNULL ? null : (AST)_t;
				pr_FunctionFormalParList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodea);
				}
				break;
			}
			case 3:
			case ReturnType:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ReturnType:
			{
				nodec = _t==ASTNULL ? null : (AST)_t;
				pr_ReturnType(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodec);
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				
						 	popToScope(scopeToPop); 
						 	passEndOffsetToScopeChildren(); 
				//			addScopeToOffsetMap();
						 	scopeStack.pop(); 
						
			}
			_t = __t793;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.1.11 - External Constant Definitions  *****/
	public final void pr_ExtConstDef(AST _t) throws RecognitionException {
		
		AST pr_ExtConstDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; ConstantSymbol symbol = new ConstantSymbol();
		
		try {      // for error handling
			AST __t797 = _t;
			AST tmp32_AST_in = (AST)_t;
			match(_t,ExtConstDef);
			_t = _t.getFirstChild();
			pr_Type(_t,symbol);
			_t = _retTree;
			{
			_loop799:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Identifier)) {
					a=pr_Identifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						
										symbol.setName(a.getName());
										symbol.setDeclarationNode(a.getDeclarationNode());
										symbol.setGroupName(a.getGroupName());
										symbol.setExternal(true);
										setVisibility(symbol);
							  			addSymbolToScope(symbol);
									
					}
				}
				else {
					break _loop799;
				}
				
			} while (true);
			}
			_t = __t797;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.1.7 - Altstep Definitions  *****/
	public final void pr_AltstepDef(AST _t) throws RecognitionException {
		
		AST pr_AltstepDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST n = null;
		AST nodea = null;
		AST nodeb = null;
		AST nodec = null;
		AST noded = null;
		Symbol a; Reference r; AltstepSymbol symbol = new AltstepSymbol(); Scope lastScope=null; Scope preparedScope=null; Scope scopeToPop=null;
		
		try {      // for error handling
			AST __t579 = _t;
			n = _t==ASTNULL ? null :(AST)_t;
			match(_t,AltstepDef);
			_t = _t.getFirstChild();
			{
			if ( inputState.guessing==0 ) {
				prepareNewScope(true);
			}
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
								symbol.setName(a.getName()); 
								symbol.setDeclarationNode(a.getDeclarationNode());
								symbol.setScope(a.getScope());
								symbol.setGroupName(a.getGroupName());
								setVisibility(symbol);
								addSymbolToScope(symbol); 
								preparedScopeStack.peek().setScopeSymbol(symbol);
								lastScope = scopeStack.peek();
								preparedScope = preparedScopeStack.peek();
								pushPreparedScope();
								scopeToPop = scopeStack.peek();
								((LocationAST)n).setScope(scopeStack.peek());
							
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FormalPortPar:
			case FormalTemplatePar:
			case FormalTimerPar:
			case FormalValuePar:
			{
				nodea = _t==ASTNULL ? null : (AST)_t;
				pr_AltstepFormalParList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodea);
				}
				break;
			}
			case 3:
			case AltstepLocalDefList:
			case AltGuardList:
			case RunsOnSpec:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case RunsOnSpec:
			{
				nodeb = _t==ASTNULL ? null : (AST)_t;
				r=pr_RunsOnSpec(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodeb);
				}
				if ( inputState.guessing==0 ) {
					
										if (r != null) {
											ScopeWithKnownName scopeWithName = new ScopeWithKnownName(lastScope, preparedScope, r.getIdentifier().getName(), false);
											knownScopes.add(scopeWithName);
										}
									
				}
				break;
			}
			case 3:
			case AltstepLocalDefList:
			case AltGuardList:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AltstepLocalDefList:
			{
				nodec = _t==ASTNULL ? null : (AST)_t;
				pr_AltstepLocalDefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					
					//				popToScope(scopeToPop); 
					//				connectToScope(nodec); 
								
				}
				break;
			}
			case 3:
			case AltGuardList:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AltGuardList:
			{
				noded = _t==ASTNULL ? null : (AST)_t;
				pr_AltGuardList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					
					//				popToScope(scopeToPop); 
					//				connectToScope(noded);
								
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				
								popToScope(scopeToPop); 
								connectToScope(noded);
				
				
				
				//				scopeStack.peek().setEndOffset(((LocationAST)#n).getEndOffset());
								 
								passEndOffsetToScopeChildren(); 
				//				addScopeToOffsetMap();
								scopeStack.pop();
							
			}
			}
			_t = __t579;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FriendModuleDef(AST _t) throws RecognitionException {
		
		AST pr_FriendModuleDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol s = null;
		
		try {      // for error handling
			AST __t48 = _t;
			AST tmp33_AST_in = (AST)_t;
			match(_t,FriendModuleDef);
			_t = _t.getFirstChild();
			{
			int _cnt50=0;
			_loop50:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Identifier)) {
					s=pr_Identifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
							ModuleSymbol currentModuleSymbol = (ModuleSymbol) s.getSurroundingScope().getModuleScope().getScopeSymbol();
									currentModuleSymbol.addFriendModule(s.getName());
								
					}
				}
				else {
					if ( _cnt50>=1 ) { break _loop50; } else {throw new NoViableAltException(_t);}
				}
				
				_cnt50++;
			} while (true);
			}
			_t = __t48;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_StructuredTypeDef(AST _t) throws RecognitionException {
		
		AST pr_StructuredTypeDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
			TypeSymbol symbol = new TypeSymbol(TypeSymbol.TypeEnum.structuredType);
			Symbol a=null;
		
		
		try {      // for error handling
			AST __t55 = _t;
			AST tmp34_AST_in = (AST)_t;
			match(_t,StructuredTypeDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case RecordDef:
			{
				a=pr_RecordDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_record);
				}
				break;
			}
			case UnionDef:
			{
				a=pr_UnionDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_union);
				}
				break;
			}
			case SetDef:
			{
				a=pr_SetDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_set);
				}
				break;
			}
			case RecordOfDef:
			{
				a=pr_RecordOfDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					
								  	symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_recordof);
								  	if (a instanceof TypeSymbol) {
									  	symbol.setReferencedType(((TypeSymbol) a).getReferencedType());
								  	}
								
				}
				break;
			}
			case SetOfDef:
			{
				a=pr_SetOfDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					
								  	symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_setof); 
								  	if (a instanceof TypeSymbol) {
									  	symbol.setReferencedType(((TypeSymbol) a).getReferencedType());
								  	}
								
				}
				break;
			}
			case EnumDef:
			{
				a=pr_EnumDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_enumerated);
				}
				break;
			}
			case PortDef:
			{
				a=pr_PortDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_port);
				}
				break;
			}
			case ComponentDef:
			{
				a=pr_ComponentDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					symbol.setStructuredType(TypeSymbol.StructuredTypeEnum.ttcn3_component);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t55;
			_t = _t.getNextSibling();
			if ( inputState.guessing==0 ) {
				
						symbol.setName(a.getName());
						symbol.setScope(a.getScope());
						symbol.setDeclarationNode(a.getDeclarationNode());
						symbol.setGroupName(a.getGroupName());
						setVisibility(symbol);
						addSymbolToScope(symbol); 
						if (a.getScope() != null)
							a.getScope().setScopeSymbol(symbol);
					
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SubTypeDef(AST _t) throws RecognitionException {
		
		AST pr_SubTypeDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST b = null;
		TypeSymbol symbol = new TypeSymbol(); Symbol a=null;
		
		try {      // for error handling
			AST __t148 = _t;
			AST tmp35_AST_in = (AST)_t;
			match(_t,SubTypeDef);
			_t = _t.getFirstChild();
			pr_Type(_t,symbol);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Identifier:
			{
				a=pr_Identifier(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					
									symbol.setName(a.getName()); 
									symbol.setGroupName(a.getGroupName()); 
									symbol.setDeclarationNode(a.getDeclarationNode());
								
				}
				break;
			}
			case AddressKeyword:
			{
				b = (AST)_t;
				match(_t,AddressKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					
									symbol.setName("address");
									symbol.setScope(scopeStack.peek());
									symbol.setSurroundingScope(scopeStack.peek());
									((LocationAST)b).setScope(scopeStack.peek());
									((LocationAST)b).setText("address");
									symbol.setDeclarationNode((LocationAST) b);
								
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ArrayDef:
			{
				pr_ArrayDef(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case SubTypeSpec:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SubTypeSpec:
			{
				pr_SubTypeSpec(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				setVisibility(symbol);
						  addSymbolToScope(symbol); 
						
			}
			_t = __t148;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final Symbol  pr_RecordDef(AST _t) throws RecognitionException {
		Symbol s = new Symbol();;
		
		AST pr_RecordDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t58 = _t;
			AST tmp36_AST_in = (AST)_t;
			match(_t,RecordDef);
			_t = _t.getFirstChild();
			s=pr_StructDefBody(_t);
			_t = _retTree;
			_t = __t58;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final Symbol  pr_UnionDef(AST _t) throws RecognitionException {
		Symbol s=new Symbol();
		
		AST pr_UnionDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST n = null;
		AST a = null;
		AST b = null;
		AST nodea = null;
		
		try {      // for error handling
			AST __t111 = _t;
			AST tmp37_AST_in = (AST)_t;
			match(_t,UnionDef);
			_t = _t.getFirstChild();
			AST __t112 = _t;
			n = _t==ASTNULL ? null :(AST)_t;
			match(_t,UnionDefBody);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				prepareNewScope(true);
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Identifier:
			{
				a = _t==ASTNULL ? null : (AST)_t;
				s=pr_Identifier(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case FormalValuePar:
				{
					pr_StructDefFormalParList(_t);
					_t = _retTree;
					break;
				}
				case 3:
				case UnionFieldDef:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			case AddressKeyword:
			{
				b = (AST)_t;
				match(_t,AddressKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				pushPreparedScope();
			}
			{
			_loop116:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==UnionFieldDef)) {
					nodea = _t==ASTNULL ? null : (AST)_t;
					pr_UnionFieldDef(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						connectToScope(nodea);
					}
				}
				else {
					break _loop116;
				}
				
			} while (true);
			}
			if ( inputState.guessing==0 ) {
				
							if (a != null)
								scopeStack.peek().setStartOffset(((LocationAST)a).getEndOffset()); 
							else if (b != null)
								scopeStack.peek().setStartOffset(((LocationAST)b).getEndOffset()); 
				
							scopeStack.peek().setEndOffset(((LocationAST)n).getEndOffset()); 
							addScopeToOffsetMap();
							scopeStack.pop(); 
						
			}
			_t = __t112;
			_t = _t.getNextSibling();
			_t = __t111;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final Symbol  pr_SetDef(AST _t) throws RecognitionException {
		Symbol s=null;
		
		AST pr_SetDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t123 = _t;
			AST tmp38_AST_in = (AST)_t;
			match(_t,SetDef);
			_t = _t.getFirstChild();
			s=pr_StructDefBody(_t);
			_t = _retTree;
			_t = __t123;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final TypeSymbol  pr_RecordOfDef(AST _t) throws RecognitionException {
		TypeSymbol s=null;
		
		AST pr_RecordOfDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t125 = _t;
			AST tmp39_AST_in = (AST)_t;
			match(_t,RecordOfDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case StringLength:
			{
				pr_StringLength(_t);
				_t = _retTree;
				break;
			}
			case StructOfDefBody:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			s=pr_StructOfDefBody(_t);
			_t = _retTree;
			_t = __t125;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final TypeSymbol  pr_SetOfDef(AST _t) throws RecognitionException {
		TypeSymbol s=null;
		
		AST pr_SetOfDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t134 = _t;
			AST tmp40_AST_in = (AST)_t;
			match(_t,SetOfDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case StringLength:
			{
				pr_StringLength(_t);
				_t = _retTree;
				break;
			}
			case StructOfDefBody:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			s=pr_StructOfDefBody(_t);
			_t = _retTree;
			_t = __t134;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final Symbol  pr_EnumDef(AST _t) throws RecognitionException {
		Symbol s=new Symbol();
		
		AST pr_EnumDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST n = null;
		AST a = null;
		AST b = null;
		AST nodea = null;
		
		try {      // for error handling
			AST __t137 = _t;
			n = _t==ASTNULL ? null :(AST)_t;
			match(_t,EnumDef);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							prepareNewScope(false);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Identifier:
			{
				a = _t==ASTNULL ? null : (AST)_t;
				s=pr_Identifier(_t);
				_t = _retTree;
				break;
			}
			case AddressKeyword:
			{
				b = (AST)_t;
				match(_t,AddressKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				
							pushPreparedScope(); 
						
			}
			nodea = _t==ASTNULL ? null : (AST)_t;
			pr_EnumerationList(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				connectToScope(nodea);
			}
			if ( inputState.guessing==0 ) {
				
							if (a != null)
								scopeStack.peek().setStartOffset(((LocationAST)a).getEndOffset());
							else if (b!= null)
								scopeStack.peek().setStartOffset(((LocationAST)b).getEndOffset());
				
							scopeStack.peek().setEndOffset(((LocationAST)n).getEndOffset());
				
				//			addScopeToOffsetMap();
							scopeStack.pop();
						
			}
			_t = __t137;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final Symbol  pr_PortDef(AST _t) throws RecognitionException {
		Symbol s=null;
		
		AST pr_PortDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST nodea = null;
		
		try {      // for error handling
			AST __t181 = _t;
			AST tmp41_AST_in = (AST)_t;
			match(_t,PortDef);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				prepareNewScope(true);
			}
			s=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				pushPreparedScope();
			}
			nodea = _t==ASTNULL ? null : (AST)_t;
			pr_PortDefAttribs(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				connectToScope(nodea);
			}
			if ( inputState.guessing==0 ) {
				
				//			addScopeToOffsetMap();
							scopeStack.pop(); 
						
			}
			_t = __t181;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final Symbol  pr_ComponentDef(AST _t) throws RecognitionException {
		Symbol s=null;
		
		AST pr_ComponentDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST n = null;
		AST id = null;
		AST nodea = null;
		AST nodec = null;
		AST nodeb = null;
		Reference r;
		
		try {      // for error handling
			AST __t240 = _t;
			n = _t==ASTNULL ? null :(AST)_t;
			match(_t,ComponentDef);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							prepareNewScope(true); 
						
			}
			id = _t==ASTNULL ? null : (AST)_t;
			s=pr_Identifier(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ComponentType:
			{
				r=pr_ComponentType(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					
							 		if (r != null) {
							 			ScopeWithKnownName scopeWithName = new ScopeWithKnownName(scopeStack.peek(), preparedScopeStack.peek(), r.getIdentifier().getName(), false);
							 			knownScopes.add(scopeWithName);
							 		}
								
				}
				{
				_loop243:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==ComponentType)) {
						r=pr_ComponentType(_t);
						_t = _retTree;
						if ( inputState.guessing==0 ) {
							
										 		if (r != null) {
										 			ScopeWithKnownName scopeWithName = new ScopeWithKnownName(scopeStack.peek(), preparedScopeStack.peek(), r.getIdentifier().getName(), false);
										 			knownScopes.add(scopeWithName);
										 		}
											
						}
					}
					else {
						break _loop243;
					}
					
				} while (true);
				}
				break;
			}
			case 3:
			case ComponentElementDef:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				
							preparedScopeStack.peek().setScopeSymbol(s);
							pushPreparedScope(); 
							scopeStack.peek().setStartOffset(((LocationAST)id).getEndOffset());
							((LocationAST)n).setScope(scopeStack.peek());
						
			}
			{
			_loop247:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ComponentElementDef)) {
					nodea = _t==ASTNULL ? null : (AST)_t;
					pr_ComponentElementDef(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						connectToScope(nodea);
					}
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case WithStatement:
					{
						nodec = _t==ASTNULL ? null : (AST)_t;
						pr_WithStatement(_t);
						_t = _retTree;
						if ( inputState.guessing==0 ) {
							connectToScope(nodec);
						}
						break;
					}
					case 3:
					case ComponentElementDef:
					case SemiColon:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						nodeb = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						if ( inputState.guessing==0 ) {
							connectToScope(nodeb);
						}
						break;
					}
					case 3:
					case ComponentElementDef:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop247;
				}
				
			} while (true);
			}
			if ( inputState.guessing==0 ) {
				
							scopeStack.peek().setEndOffset(((LocationAST)n).getEndOffset());
				//			addScopeToOffsetMap();
							scopeStack.pop(); 
						
			}
			_t = __t240;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final Symbol  pr_StructDefBody(AST _t) throws RecognitionException {
		Symbol s = new Symbol();;
		
		AST pr_StructDefBody_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST n = null;
		AST a = null;
		AST c = null;
		AST nodea = null;
		
		try {      // for error handling
			AST __t60 = _t;
			n = _t==ASTNULL ? null :(AST)_t;
			match(_t,StructDefBody);
			_t = _t.getFirstChild();
			{
			if ( inputState.guessing==0 ) {
				prepareNewScope(true);
			}
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Identifier:
			{
				a = _t==ASTNULL ? null : (AST)_t;
				s=pr_Identifier(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case FormalValuePar:
				{
					pr_StructDefFormalParList(_t);
					_t = _retTree;
					break;
				}
				case 3:
				case StructFieldDef:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			case AddressKeyword:
			{
				c = (AST)_t;
				match(_t,AddressKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				
								pushPreparedScope(); 
								if (a != null) {
									scopeStack.peek().setStartOffset(((LocationAST)a).getEndOffset());
								} else if (c != null) {
									scopeStack.peek().setStartOffset(((LocationAST)c).getEndOffset());
								}
							
			}
			{
			_loop66:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==StructFieldDef)) {
					nodea = _t==ASTNULL ? null : (AST)_t;
					pr_StructFieldDef(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						connectToScope(nodea);
					}
				}
				else {
					break _loop66;
				}
				
			} while (true);
			}
			}
			if ( inputState.guessing==0 ) {
				
								scopeStack.peek().setEndOffset(((LocationAST)n).getEndOffset());
				//				addScopeToOffsetMap();
								scopeStack.pop();
							
			}
			}
			_t = __t60;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final void pr_StructDefFormalParList(AST _t) throws RecognitionException {
		
		AST pr_StructDefFormalParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_StructDefFormalPar(_t);
			_t = _retTree;
			{
			_loop69:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FormalValuePar)) {
					pr_StructDefFormalPar(_t);
					_t = _retTree;
				}
				else {
					break _loop69;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_StructFieldDef(AST _t) throws RecognitionException {
		
		AST pr_StructFieldDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; TypeSymbol symbol = new TypeSymbol();
		
		try {      // for error handling
			AST __t72 = _t;
			AST tmp42_AST_in = (AST)_t;
			match(_t,StructFieldDef);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Type:
			{
				pr_Type(_t,symbol);
				_t = _retTree;
				break;
			}
			case NestedTypeDef:
			{
				pr_NestedTypeDef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			a=pr_Identifier(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ArrayDef:
			{
				pr_ArrayDef(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case SubTypeSpec:
			case OPTIONAL:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SubTypeSpec:
			{
				pr_SubTypeSpec(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case OPTIONAL:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case OPTIONAL:
			{
				AST tmp43_AST_in = (AST)_t;
				match(_t,OPTIONAL);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			if ( inputState.guessing==0 ) {
				
						symbol.setName(a.getName()); 
							  symbol.setDeclarationNode(a.getDeclarationNode());   		
					
			}
			if ( inputState.guessing==0 ) {
				addSymbolToScope(symbol);
			}
			_t = __t72;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_StructDefFormalPar(AST _t) throws RecognitionException {
		
		AST pr_StructDefFormalPar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_FormalValuePar(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/*****  SECTION A.1.6.6 - Parameterisation  *****/
	public final void pr_FormalValuePar(AST _t) throws RecognitionException {
		
		AST pr_FormalValuePar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; FormalParameterSymbol symbol = new FormalParameterSymbol();
		
		try {      // for error handling
			AST __t1270 = _t;
			AST tmp44_AST_in = (AST)_t;
			match(_t,FormalValuePar);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case InParKeyword:
			{
				AST tmp45_AST_in = (AST)_t;
				match(_t,InParKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case InOutParKeyword:
			{
				AST tmp46_AST_in = (AST)_t;
				match(_t,InOutParKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case OutParKeyword:
			{
				AST tmp47_AST_in = (AST)_t;
				match(_t,OutParKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case Type:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			pr_Type(_t,symbol);
			_t = _retTree;
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName());
							symbol.setDeclarationNode(a.getDeclarationNode());
							symbol.setParameterType(FormalParameterSymbol.ParameterEnum.value);
							addSymbolToScope(symbol);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Expression:
			{
				pr_Expression(_t);
				_t = _retTree;
				break;
			}
			case Dash:
			{
				AST tmp48_AST_in = (AST)_t;
				match(_t,Dash);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1270;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_NestedTypeDef(AST _t) throws RecognitionException {
		
		AST pr_NestedTypeDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST nodea = null;
		AST nodeb = null;
		AST nodec = null;
		AST noded = null;
		AST nodee = null;
		AST nodef = null;
		
		try {      // for error handling
			if ( inputState.guessing==0 ) {
				pushNewScope(false);
			}
			AST __t79 = _t;
			AST tmp49_AST_in = (AST)_t;
			match(_t,NestedTypeDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case NestedRecordDef:
			{
				nodea = _t==ASTNULL ? null : (AST)_t;
				pr_NestedRecordDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodea);
				}
				break;
			}
			case NestedUnionDef:
			{
				nodeb = _t==ASTNULL ? null : (AST)_t;
				pr_NestedUnionDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodeb);
				}
				break;
			}
			case NestedSetDef:
			{
				nodec = _t==ASTNULL ? null : (AST)_t;
				pr_NestedSetDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodec);
				}
				break;
			}
			case NestedRecordOfDef:
			{
				noded = _t==ASTNULL ? null : (AST)_t;
				pr_NestedRecordOfDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(noded);
				}
				break;
			}
			case NestedSetOfDef:
			{
				nodee = _t==ASTNULL ? null : (AST)_t;
				pr_NestedSetOfDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodee);
				}
				break;
			}
			case NestedEnumDef:
			{
				nodef = _t==ASTNULL ? null : (AST)_t;
				pr_NestedEnumDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodef);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t79;
			_t = _t.getNextSibling();
			if ( inputState.guessing==0 ) {
				
				//		addScopeToOffsetMap();
						scopeStack.pop(); 
					
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ArrayDef(AST _t) throws RecognitionException {
		
		AST pr_ArrayDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1204 = _t;
			AST tmp50_AST_in = (AST)_t;
			match(_t,ArrayDef);
			_t = _t.getFirstChild();
			{
			int _cnt1206=0;
			_loop1206:
			do {
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case SingleExpression:
				{
					pr_ArrayBounds(_t);
					_t = _retTree;
					break;
				}
				case ArrayDefRange:
				{
					pr_ArrayDefRange(_t);
					_t = _retTree;
					break;
				}
				default:
				{
					if ( _cnt1206>=1 ) { break _loop1206; } else {throw new NoViableAltException(_t);}
				}
				}
				_cnt1206++;
			} while (true);
			}
			_t = __t1204;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SubTypeSpec(AST _t) throws RecognitionException {
		
		AST pr_SubTypeSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t153 = _t;
			AST tmp51_AST_in = (AST)_t;
			match(_t,SubTypeSpec);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AllowedValues:
			{
				{
				pr_AllowedValues(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case StringLength:
				{
					pr_StringLength(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case StringLength:
			{
				pr_StringLength(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t153;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_NestedRecordDef(AST _t) throws RecognitionException {
		
		AST pr_NestedRecordDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t82 = _t;
			AST tmp52_AST_in = (AST)_t;
			match(_t,NestedRecordDef);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case StructFieldDef:
			{
				pr_StructFieldDef(_t);
				_t = _retTree;
				{
				_loop86:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==StructFieldDef)) {
						pr_StructFieldDef(_t);
						_t = _retTree;
					}
					else {
						break _loop86;
					}
					
				} while (true);
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t82;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_NestedUnionDef(AST _t) throws RecognitionException {
		
		AST pr_NestedUnionDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t88 = _t;
			AST tmp53_AST_in = (AST)_t;
			match(_t,NestedUnionDef);
			_t = _t.getFirstChild();
			{
			pr_UnionFieldDef(_t);
			_t = _retTree;
			{
			_loop91:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==UnionFieldDef)) {
					pr_UnionFieldDef(_t);
					_t = _retTree;
				}
				else {
					break _loop91;
				}
				
			} while (true);
			}
			}
			_t = __t88;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_NestedSetDef(AST _t) throws RecognitionException {
		
		AST pr_NestedSetDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t93 = _t;
			AST tmp54_AST_in = (AST)_t;
			match(_t,NestedSetDef);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case StructFieldDef:
			{
				pr_StructFieldDef(_t);
				_t = _retTree;
				{
				_loop97:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==StructFieldDef)) {
						pr_StructFieldDef(_t);
						_t = _retTree;
					}
					else {
						break _loop97;
					}
					
				} while (true);
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t93;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_NestedRecordOfDef(AST _t) throws RecognitionException {
		
		AST pr_NestedRecordOfDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t99 = _t;
			AST tmp55_AST_in = (AST)_t;
			match(_t,NestedRecordOfDef);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case StringLength:
			{
				pr_StringLength(_t);
				_t = _retTree;
				break;
			}
			case NestedTypeDef:
			case Type:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Type:
			{
				pr_Type(_t,null);
				_t = _retTree;
				break;
			}
			case NestedTypeDef:
			{
				pr_NestedTypeDef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t99;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_NestedSetOfDef(AST _t) throws RecognitionException {
		
		AST pr_NestedSetOfDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t104 = _t;
			AST tmp56_AST_in = (AST)_t;
			match(_t,NestedSetOfDef);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case StringLength:
			{
				pr_StringLength(_t);
				_t = _retTree;
				break;
			}
			case NestedTypeDef:
			case Type:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Type:
			{
				pr_Type(_t,null);
				_t = _retTree;
				break;
			}
			case NestedTypeDef:
			{
				pr_NestedTypeDef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t104;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_NestedEnumDef(AST _t) throws RecognitionException {
		
		AST pr_NestedEnumDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t109 = _t;
			AST tmp57_AST_in = (AST)_t;
			match(_t,NestedEnumDef);
			_t = _t.getFirstChild();
			pr_EnumerationList(_t);
			_t = _retTree;
			_t = __t109;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_UnionFieldDef(AST _t) throws RecognitionException {
		
		AST pr_UnionFieldDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; TypeSymbol symbol = new TypeSymbol();
		
		try {      // for error handling
			AST __t118 = _t;
			AST tmp58_AST_in = (AST)_t;
			match(_t,UnionFieldDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Type:
			{
				pr_Type(_t,symbol);
				_t = _retTree;
				break;
			}
			case NestedTypeDef:
			{
				pr_NestedTypeDef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			a=pr_Identifier(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ArrayDef:
			{
				pr_ArrayDef(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case SubTypeSpec:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SubTypeSpec:
			{
				pr_SubTypeSpec(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t118;
			_t = _t.getNextSibling();
			if ( inputState.guessing==0 ) {
				
						symbol.setName(a.getName()); 
						symbol.setDeclarationNode(a.getDeclarationNode());
					
			}
			if ( inputState.guessing==0 ) {
				addSymbolToScope(symbol);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_StringLength(AST _t) throws RecognitionException {
		
		AST pr_StringLength_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t173 = _t;
			AST tmp59_AST_in = (AST)_t;
			match(_t,StringLength);
			_t = _t.getFirstChild();
			pr_SingleConstExpression(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case UpperBound:
			{
				pr_UpperBound(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t173;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_EnumerationList(AST _t) throws RecognitionException {
		
		AST pr_EnumerationList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t140 = _t;
			AST tmp60_AST_in = (AST)_t;
			match(_t,EnumerationList);
			_t = _t.getFirstChild();
			pr_Enumeration(_t);
			_t = _retTree;
			{
			_loop142:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Enumeration)) {
					pr_Enumeration(_t);
					_t = _retTree;
				}
				else {
					break _loop142;
				}
				
			} while (true);
			}
			_t = __t140;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final TypeSymbol  pr_StructOfDefBody(AST _t) throws RecognitionException {
		TypeSymbol s = new TypeSymbol();;
		
		AST pr_StructOfDefBody_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a = null;
		
		try {      // for error handling
			AST __t128 = _t;
			AST tmp61_AST_in = (AST)_t;
			match(_t,StructOfDefBody);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Type:
			{
				pr_Type(_t,s);
				_t = _retTree;
				break;
			}
			case NestedTypeDef:
			{
				pr_NestedTypeDef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Identifier:
			{
				a=pr_Identifier(_t);
				_t = _retTree;
				break;
			}
			case AddressKeyword:
			{
				AST tmp62_AST_in = (AST)_t;
				match(_t,AddressKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SubTypeSpec:
			{
				pr_SubTypeSpec(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				
								if (a != null) {
						    		s.setName(a.getName()); 
									s.setDeclarationNode(a.getDeclarationNode());
								}  
							
			}
			}
			_t = __t128;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final void pr_Enumeration(AST _t) throws RecognitionException {
		
		AST pr_Enumeration_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; EnumSymbol symbol = new EnumSymbol();
		
		try {      // for error handling
			AST __t144 = _t;
			AST tmp63_AST_in = (AST)_t;
			match(_t,Enumeration);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Minus:
			case Number:
			{
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case Minus:
				{
					AST tmp64_AST_in = (AST)_t;
					match(_t,Minus);
					_t = _t.getNextSibling();
					break;
				}
				case Number:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				pr_Number(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t144;
			_t = _t.getNextSibling();
			if ( inputState.guessing==0 ) {
				
						symbol.setName(a.getName());
						symbol.setDeclarationNode(a.getDeclarationNode());
					
			}
			if ( inputState.guessing==0 ) {
				addSymbolToScope(symbol);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AllowedValues(AST _t) throws RecognitionException {
		
		AST pr_AllowedValues_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t158 = _t;
			AST tmp65_AST_in = (AST)_t;
			match(_t,AllowedValues);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ValueOrRange:
			{
				{
				int _cnt161=0;
				_loop161:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==ValueOrRange)) {
						pr_ValueOrRange(_t);
						_t = _retTree;
					}
					else {
						if ( _cnt161>=1 ) { break _loop161; } else {throw new NoViableAltException(_t);}
					}
					
					_cnt161++;
				} while (true);
				}
				break;
			}
			case CharStringMatch:
			{
				{
				pr_CharStringMatch(_t);
				_t = _retTree;
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t158;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ValueOrRange(AST _t) throws RecognitionException {
		
		AST pr_ValueOrRange_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t168 = _t;
			AST tmp66_AST_in = (AST)_t;
			match(_t,ValueOrRange);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case RangeDef:
			{
				pr_RangeDef(_t);
				_t = _retTree;
				break;
			}
			case ConstantExpression:
			{
				pr_ConstantExpression(_t);
				_t = _retTree;
				break;
			}
			case Type:
			{
				pr_Type(_t,null);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t168;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CharStringMatch(AST _t) throws RecognitionException {
		
		AST pr_CharStringMatch_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t164 = _t;
			AST tmp67_AST_in = (AST)_t;
			match(_t,CharStringMatch);
			_t = _t.getFirstChild();
			{
			int _cnt166=0;
			_loop166:
			do {
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case CSTRING:
				{
					a = (AST)_t;
					match(_t,CSTRING);
					_t = _t.getNextSibling();
					break;
				}
				case ReferencedValue:
				{
					pr_ReferencedValue(_t);
					_t = _retTree;
					break;
				}
				default:
				{
					if ( _cnt166>=1 ) { break _loop166; } else {throw new NoViableAltException(_t);}
				}
				}
				_cnt166++;
			} while (true);
			}
			_t = __t164;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ReferencedValue(AST _t) throws RecognitionException {
		
		AST pr_ReferencedValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<Symbol> s;
		
		try {      // for error handling
			AST __t1258 = _t;
			AST tmp68_AST_in = (AST)_t;
			match(_t,ReferencedValue);
			_t = _t.getFirstChild();
			{
			pr_ValueReference(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ExtendedFieldReference:
			{
				s=pr_ExtendedFieldReference(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t1258;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_RangeDef(AST _t) throws RecognitionException {
		
		AST pr_RangeDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t171 = _t;
			AST tmp69_AST_in = (AST)_t;
			match(_t,RangeDef);
			_t = _t.getFirstChild();
			pr_LowerBound(_t);
			_t = _retTree;
			pr_UpperBound(_t);
			_t = _retTree;
			_t = __t171;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_LowerBound(AST _t) throws RecognitionException {
		
		AST pr_LowerBound_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t399 = _t;
			AST tmp70_AST_in = (AST)_t;
			match(_t,LowerBound);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ExclamationMark:
			{
				pr_ExclamationMark(_t);
				_t = _retTree;
				break;
			}
			case Minus:
			case SingleExpression:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SingleExpression:
			{
				pr_SingleConstExpression(_t);
				_t = _retTree;
				break;
			}
			case Minus:
			{
				{
				AST tmp71_AST_in = (AST)_t;
				match(_t,Minus);
				_t = _t.getNextSibling();
				AST tmp72_AST_in = (AST)_t;
				match(_t,Infinity);
				_t = _t.getNextSibling();
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t399;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_UpperBound(AST _t) throws RecognitionException {
		
		AST pr_UpperBound_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t404 = _t;
			AST tmp73_AST_in = (AST)_t;
			match(_t,UpperBound);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ExclamationMark:
			{
				pr_ExclamationMark(_t);
				_t = _retTree;
				break;
			}
			case Infinity:
			case SingleExpression:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SingleExpression:
			{
				pr_SingleConstExpression(_t);
				_t = _retTree;
				break;
			}
			case Infinity:
			{
				AST tmp74_AST_in = (AST)_t;
				match(_t,Infinity);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t404;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleConstExpression(AST _t) throws RecognitionException {
		
		AST pr_SingleConstExpression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_SingleExpression(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortType(AST _t) throws RecognitionException {
		
		AST pr_PortType_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t176 = _t;
			AST tmp75_AST_in = (AST)_t;
			match(_t,PortType);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GlobalModuleId:
			{
				a=pr_GlobalModuleId(_t);
				_t = _retTree;
				break;
			}
			case Identifier:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			b=pr_Identifier(_t);
			_t = _retTree;
			_t = __t176;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortDefAttribs(AST _t) throws RecognitionException {
		
		AST pr_PortDefAttribs_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t183 = _t;
			AST tmp76_AST_in = (AST)_t;
			match(_t,PortDefAttribs);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case MessageAttribs:
			{
				pr_MessageAttribs(_t);
				_t = _retTree;
				break;
			}
			case ProcedureAttribs:
			{
				pr_ProcedureAttribs(_t);
				_t = _retTree;
				break;
			}
			case MixedAttribs:
			{
				pr_MixedAttribs(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t183;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_MessageAttribs(AST _t) throws RecognitionException {
		
		AST pr_MessageAttribs_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t186 = _t;
			AST tmp77_AST_in = (AST)_t;
			match(_t,MessageAttribs);
			_t = _t.getFirstChild();
			{
			_loop190:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==AddressDecl||_t.getType()==ConfigParamDef||_t.getType()==MessageList)) {
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case AddressDecl:
					{
						pr_AddressDecl(_t);
						_t = _retTree;
						break;
					}
					case MessageList:
					{
						pr_MessageList(_t);
						_t = _retTree;
						break;
					}
					case ConfigParamDef:
					{
						pr_ConfigParamDef(_t);
						_t = _retTree;
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp78_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case AddressDecl:
					case ConfigParamDef:
					case MessageList:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop190;
				}
				
			} while (true);
			}
			_t = __t186;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ProcedureAttribs(AST _t) throws RecognitionException {
		
		AST pr_ProcedureAttribs_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t213 = _t;
			AST tmp79_AST_in = (AST)_t;
			match(_t,ProcedureAttribs);
			_t = _t.getFirstChild();
			{
			_loop217:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==AddressDecl||_t.getType()==ConfigParamDef||_t.getType()==ProcedureList)) {
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case AddressDecl:
					{
						pr_AddressDecl(_t);
						_t = _retTree;
						break;
					}
					case ProcedureList:
					{
						pr_ProcedureList(_t);
						_t = _retTree;
						break;
					}
					case ConfigParamDef:
					{
						pr_ConfigParamDef(_t);
						_t = _retTree;
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp80_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case AddressDecl:
					case ConfigParamDef:
					case ProcedureList:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop217;
				}
				
			} while (true);
			}
			_t = __t213;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_MixedAttribs(AST _t) throws RecognitionException {
		
		AST pr_MixedAttribs_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t226 = _t;
			AST tmp81_AST_in = (AST)_t;
			match(_t,MixedAttribs);
			_t = _t.getFirstChild();
			{
			_loop230:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==AddressDecl||_t.getType()==ConfigParamDef||_t.getType()==MixedList)) {
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case AddressDecl:
					{
						pr_AddressDecl(_t);
						_t = _retTree;
						break;
					}
					case MixedList:
					{
						pr_MixedList(_t);
						_t = _retTree;
						break;
					}
					case ConfigParamDef:
					{
						pr_ConfigParamDef(_t);
						_t = _retTree;
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp82_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case AddressDecl:
					case ConfigParamDef:
					case MixedList:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop230;
				}
				
			} while (true);
			}
			_t = __t226;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AddressDecl(AST _t) throws RecognitionException {
		
		AST pr_AddressDecl_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t192 = _t;
			AST tmp83_AST_in = (AST)_t;
			match(_t,AddressDecl);
			_t = _t.getFirstChild();
			pr_Type(_t,null);
			_t = _retTree;
			_t = __t192;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_MessageList(AST _t) throws RecognitionException {
		
		AST pr_MessageList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t205 = _t;
			AST tmp84_AST_in = (AST)_t;
			match(_t,MessageList);
			_t = _t.getFirstChild();
			pr_Direction(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AllKeyword:
			{
				AST tmp85_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			case Type:
			{
				{
				_loop208:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==Type)) {
						pr_Type(_t,null);
						_t = _retTree;
					}
					else {
						break _loop208;
					}
					
				} while (true);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t205;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ConfigParamDef(AST _t) throws RecognitionException {
		
		AST pr_ConfigParamDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t194 = _t;
			AST tmp86_AST_in = (AST)_t;
			match(_t,ConfigParamDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case MapParamDef:
			{
				pr_MapParamDef(_t);
				_t = _retTree;
				break;
			}
			case UnmapParamDef:
			{
				pr_UnmapParamDef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t194;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_MapParamDef(AST _t) throws RecognitionException {
		
		AST pr_MapParamDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t197 = _t;
			AST tmp87_AST_in = (AST)_t;
			match(_t,MapParamDef);
			_t = _t.getFirstChild();
			{
			_loop199:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FormalValuePar)) {
					pr_FormalValuePar(_t);
					_t = _retTree;
				}
				else {
					break _loop199;
				}
				
			} while (true);
			}
			_t = __t197;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_UnmapParamDef(AST _t) throws RecognitionException {
		
		AST pr_UnmapParamDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t201 = _t;
			AST tmp88_AST_in = (AST)_t;
			match(_t,UnmapParamDef);
			_t = _t.getFirstChild();
			{
			_loop203:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FormalValuePar)) {
					pr_FormalValuePar(_t);
					_t = _retTree;
				}
				else {
					break _loop203;
				}
				
			} while (true);
			}
			_t = __t201;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Direction(AST _t) throws RecognitionException {
		
		AST pr_Direction_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t210 = _t;
			AST tmp89_AST_in = (AST)_t;
			match(_t,Direction);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case InParKeyword:
			{
				AST tmp90_AST_in = (AST)_t;
				match(_t,InParKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case OutParKeyword:
			{
				AST tmp91_AST_in = (AST)_t;
				match(_t,OutParKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case InOutParKeyword:
			{
				AST tmp92_AST_in = (AST)_t;
				match(_t,InOutParKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t210;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ProcedureList(AST _t) throws RecognitionException {
		
		AST pr_ProcedureList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t219 = _t;
			AST tmp93_AST_in = (AST)_t;
			match(_t,ProcedureList);
			_t = _t.getFirstChild();
			pr_Direction(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AllKeyword:
			{
				AST tmp94_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case SignatureList:
			{
				pr_SignatureList(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t219;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SignatureList(AST _t) throws RecognitionException {
		
		AST pr_SignatureList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t222 = _t;
			AST tmp95_AST_in = (AST)_t;
			match(_t,SignatureList);
			_t = _t.getFirstChild();
			pr_Signature(_t);
			_t = _retTree;
			{
			_loop224:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Signature)) {
					pr_Signature(_t);
					_t = _retTree;
				}
				else {
					break _loop224;
				}
				
			} while (true);
			}
			_t = __t222;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Signature(AST _t) throws RecognitionException {
		
		AST pr_Signature_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t535 = _t;
			AST tmp96_AST_in = (AST)_t;
			match(_t,Signature);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GlobalModuleId:
			{
				a=pr_GlobalModuleId(_t);
				_t = _retTree;
				break;
			}
			case Identifier:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			b=pr_Identifier(_t);
			_t = _retTree;
			}
			_t = __t535;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_MixedList(AST _t) throws RecognitionException {
		
		AST pr_MixedList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t232 = _t;
			AST tmp97_AST_in = (AST)_t;
			match(_t,MixedList);
			_t = _t.getFirstChild();
			pr_Direction(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AllKeyword:
			{
				AST tmp98_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			case ProcOrType:
			{
				{
				_loop235:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==ProcOrType)) {
						pr_ProcOrType(_t);
						_t = _retTree;
					}
					else {
						break _loop235;
					}
					
				} while (true);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t232;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ProcOrType(AST _t) throws RecognitionException {
		
		AST pr_ProcOrType_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t237 = _t;
			AST tmp99_AST_in = (AST)_t;
			match(_t,ProcOrType);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Type:
			{
				pr_Type(_t,null);
				_t = _retTree;
				break;
			}
			case Signature:
			{
				pr_Signature(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t237;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final Reference  pr_ComponentType(AST _t) throws RecognitionException {
		Reference r=new Reference();;
		
		AST pr_ComponentType_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t249 = _t;
			AST tmp100_AST_in = (AST)_t;
			match(_t,ComponentType);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GlobalModuleId:
			{
				a=pr_GlobalModuleId(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					r.setModule(a);
				}
				break;
			}
			case Identifier:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			b=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				r.setIdentifier(b);
			}
			_t = __t249;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return r;
	}
	
	public final void pr_ComponentElementDef(AST _t) throws RecognitionException {
		
		AST pr_ComponentElementDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t254 = _t;
			AST tmp101_AST_in = (AST)_t;
			match(_t,ComponentElementDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case PortInstance:
			{
				pr_PortInstance(_t);
				_t = _retTree;
				break;
			}
			case VarInstance:
			{
				pr_VarInstance(_t);
				_t = _retTree;
				break;
			}
			case TimerInstance:
			{
				pr_TimerInstance(_t);
				_t = _retTree;
				break;
			}
			case ConstDef:
			{
				pr_ConstDef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t254;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortInstance(AST _t) throws RecognitionException {
		
		AST pr_PortInstance_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t257 = _t;
			AST tmp102_AST_in = (AST)_t;
			match(_t,PortInstance);
			_t = _t.getFirstChild();
			pr_PortType(_t);
			_t = _retTree;
			{
			_loop259:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==PortElement)) {
					pr_PortElement(_t);
					_t = _retTree;
				}
				else {
					break _loop259;
				}
				
			} while (true);
			}
			_t = __t257;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.3.1 - Variable Instantiation  *****/
	public final void pr_VarInstance(AST _t) throws RecognitionException {
		
		AST pr_VarInstance_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		TypeSymbol typeResult = new TypeSymbol();
		
		try {      // for error handling
			AST __t830 = _t;
			AST tmp103_AST_in = (AST)_t;
			match(_t,VarInstance);
			_t = _t.getFirstChild();
			{
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateKeyword:
			{
				AST tmp104_AST_in = (AST)_t;
				match(_t,TemplateKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case RestrictedTemplate:
			{
				pr_RestrictedTemplate(_t);
				_t = _retTree;
				break;
			}
			case Type:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			pr_Type(_t,typeResult);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case VarList:
			{
				pr_VarList(_t,typeResult);
				_t = _retTree;
				break;
			}
			case TempVarList:
			{
				pr_TempVarList(_t,typeResult);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			}
			_t = __t830;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/*****    1.6.2.2 - Timer Instantiation  *****/
	public final void pr_TimerInstance(AST _t) throws RecognitionException {
		
		AST pr_TimerInstance_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t860 = _t;
			AST tmp105_AST_in = (AST)_t;
			match(_t,TimerInstance);
			_t = _t.getFirstChild();
			{
			int _cnt862=0;
			_loop862:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==SingleTimerInstance)) {
					pr_SingleTimerInstance(_t);
					_t = _retTree;
				}
				else {
					if ( _cnt862>=1 ) { break _loop862; } else {throw new NoViableAltException(_t);}
				}
				
				_cnt862++;
			} while (true);
			}
			_t = __t860;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortElement(AST _t) throws RecognitionException {
		
		AST pr_PortElement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; PortInstanceSymbol symbol = new PortInstanceSymbol();
		
		try {      // for error handling
			AST __t261 = _t;
			AST tmp106_AST_in = (AST)_t;
			match(_t,PortElement);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName()); 
							symbol.setDeclarationNode(a.getDeclarationNode());
						
			}
			if ( inputState.guessing==0 ) {
				addSymbolToScope(symbol);
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ArrayDef:
			{
				pr_ArrayDef(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t261;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleConstDef(AST _t,
		Symbol typeSymbol
	) throws RecognitionException {
		
		AST pr_SingleConstDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; ConstantSymbol symbol = new ConstantSymbol();
		
		try {      // for error handling
			AST __t268 = _t;
			AST tmp107_AST_in = (AST)_t;
			match(_t,SingleConstDef);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName()); 
							symbol.setPredefinedType(((ConstantSymbol)typeSymbol).getPredefinedType()); 
							symbol.setReferencedType(((ConstantSymbol)typeSymbol).getReferencedType());
							symbol.setDeclarationNode(a.getDeclarationNode());
							setVisibility(symbol);
							addSymbolToScope(symbol);
							tempSymbol = symbol;
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ArrayDef:
			{
				pr_ArrayDef(_t);
				_t = _retTree;
				break;
			}
			case ConstantExpression:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			pr_ConstantExpression(_t);
			_t = _retTree;
			_t = __t268;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TemplateRestriction(AST _t) throws RecognitionException {
		
		AST pr_TemplateRestriction_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t282 = _t;
			AST tmp108_AST_in = (AST)_t;
			match(_t,TemplateRestriction);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case OMIT:
			{
				AST tmp109_AST_in = (AST)_t;
				match(_t,OMIT);
				_t = _t.getNextSibling();
				break;
			}
			case VALUE:
			{
				AST tmp110_AST_in = (AST)_t;
				match(_t,VALUE);
				_t = _t.getNextSibling();
				break;
			}
			case PRESENT:
			{
				AST tmp111_AST_in = (AST)_t;
				match(_t,PRESENT);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t282;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_BaseTemplate(AST _t) throws RecognitionException {
		
		AST pr_BaseTemplate_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST upperx = null;
		AST nodex = null;
		Symbol a; TemplateSymbol symbol = new TemplateSymbol(); TypeSymbol typeSymb = new TypeSymbol();
		
		try {      // for error handling
			AST __t275 = _t;
			upperx = _t==ASTNULL ? null :(AST)_t;
			match(_t,BaseTemplate);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Type:
			{
				pr_Type(_t,typeSymb);
				_t = _retTree;
				break;
			}
			case Signature:
			{
				pr_Signature(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName());
							symbol.setDeclarationNode(a.getDeclarationNode());
							symbol.setScope(a.getScope());
							symbol.setGroupName(a.getGroupName());
							symbol.setReferencedType(typeSymb.getReferencedType());
							symbol.setPredefinedType(typeSymb.getPredefinedType());
							setVisibility(symbol);
							addSymbolToScope(symbol);
							preparedScopeStack.peek().setScopeSymbol(symbol);
							tempSymbol = symbol;
						
			}
			if ( inputState.guessing==0 ) {
				
							if ((typeSymb.getReferencedType() != null) && (!(preparedScopeStack.empty()))) {
								ScopeWithKnownName scopeWithName = new ScopeWithKnownName(scopeStack.peek(),
									preparedScopeStack.peek(), typeSymb.getReferencedType().getIdentifier().getName(), false);
								knownScopes.add(scopeWithName);
							}
						
			}
			if ( inputState.guessing==0 ) {
								
							pushPreparedScope(); 
							((LocationAST)upperx).setScope(scopeStack.peek());
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FormalTemplatePar:
			case FormalValuePar:
			{
				nodex = _t==ASTNULL ? null : (AST)_t;
				pr_TemplateFormalParList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					connectToScope(nodex);
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t275;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_DerivedDef(AST _t) throws RecognitionException {
		
		AST pr_DerivedDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t279 = _t;
			AST tmp112_AST_in = (AST)_t;
			match(_t,DerivedDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GlobalModuleId:
			{
				a=pr_GlobalModuleId(_t);
				_t = _retTree;
				break;
			}
			case Identifier:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			b=pr_Identifier(_t);
			_t = _retTree;
			_t = __t279;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TemplateBody(AST _t) throws RecognitionException {
		
		AST pr_TemplateBody_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t293 = _t;
			AST tmp113_AST_in = (AST)_t;
			match(_t,TemplateBody);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SingleValueOrAttrib:
			{
				pr_SimpleSpec(_t);
				_t = _retTree;
				break;
			}
			case FieldSpecList:
			{
				pr_FieldSpecList(_t);
				_t = _retTree;
				break;
			}
			case ArrayValueOrAttrib:
			{
				pr_ArrayValueOrAttrib(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ExtraMatchingAttributes:
			{
				pr_ExtraMatchingAttributes(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t293;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TemplateFormalParList(AST _t) throws RecognitionException {
		
		AST pr_TemplateFormalParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_TemplateFormalPar(_t);
			_t = _retTree;
			{
			_loop290:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FormalTemplatePar||_t.getType()==FormalValuePar)) {
					pr_TemplateFormalPar(_t);
					_t = _retTree;
				}
				else {
					break _loop290;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_RestrictedTemplate(AST _t) throws RecognitionException {
		
		AST pr_RestrictedTemplate_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t285 = _t;
			AST tmp114_AST_in = (AST)_t;
			match(_t,RestrictedTemplate);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case OMIT:
			{
				AST tmp115_AST_in = (AST)_t;
				match(_t,OMIT);
				_t = _t.getNextSibling();
				break;
			}
			case TemplateKeyword:
			{
				{
				AST tmp116_AST_in = (AST)_t;
				match(_t,TemplateKeyword);
				_t = _t.getNextSibling();
				pr_TemplateRestriction(_t);
				_t = _retTree;
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t285;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TemplateFormalPar(AST _t) throws RecognitionException {
		
		AST pr_TemplateFormalPar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FormalValuePar:
			{
				pr_FormalValuePar(_t);
				_t = _retTree;
				break;
			}
			case FormalTemplatePar:
			{
				pr_FormalTemplatePar(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FormalTemplatePar(AST _t) throws RecognitionException {
		
		AST pr_FormalTemplatePar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; FormalParameterSymbol symbol = new FormalParameterSymbol();
		
		try {      // for error handling
			AST __t1282 = _t;
			AST tmp117_AST_in = (AST)_t;
			match(_t,FormalTemplatePar);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case InParKeyword:
			{
				AST tmp118_AST_in = (AST)_t;
				match(_t,InParKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case OutParKeyword:
			{
				AST tmp119_AST_in = (AST)_t;
				match(_t,OutParKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case InOutParKeyword:
			{
				AST tmp120_AST_in = (AST)_t;
				match(_t,InOutParKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case RestrictedTemplate:
			case TemplateKeyword:
			case Type:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateKeyword:
			{
				AST tmp121_AST_in = (AST)_t;
				match(_t,TemplateKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case RestrictedTemplate:
			{
				pr_RestrictedTemplate(_t);
				_t = _retTree;
				break;
			}
			case Type:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			pr_Type(_t,symbol);
			_t = _retTree;
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName());
							symbol.setDeclarationNode(a.getDeclarationNode());
							symbol.setParameterType(FormalParameterSymbol.ParameterEnum.template);
							addSymbolToScope(symbol);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateInstance:
			{
				pr_TemplateInstance(_t);
				_t = _retTree;
				break;
			}
			case Dash:
			{
				AST tmp122_AST_in = (AST)_t;
				match(_t,Dash);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1282;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SimpleSpec(AST _t) throws RecognitionException {
		
		AST pr_SimpleSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_SingleValueOrAttrib(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FieldSpecList(AST _t) throws RecognitionException {
		
		AST pr_FieldSpecList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t305 = _t;
			AST tmp123_AST_in = (AST)_t;
			match(_t,FieldSpecList);
			_t = _t.getFirstChild();
			{
			_loop307:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FieldSpec)) {
					pr_FieldSpec(_t);
					_t = _retTree;
				}
				else {
					break _loop307;
				}
				
			} while (true);
			}
			_t = __t305;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ArrayValueOrAttrib(AST _t) throws RecognitionException {
		
		AST pr_ArrayValueOrAttrib_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t331 = _t;
			AST tmp124_AST_in = (AST)_t;
			match(_t,ArrayValueOrAttrib);
			_t = _t.getFirstChild();
			{
			_loop333:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ArrayElementSpec)) {
					pr_ArrayElementSpec(_t);
					_t = _retTree;
				}
				else {
					break _loop333;
				}
				
			} while (true);
			}
			_t = __t331;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ExtraMatchingAttributes(AST _t) throws RecognitionException {
		
		AST pr_ExtraMatchingAttributes_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t368 = _t;
			AST tmp125_AST_in = (AST)_t;
			match(_t,ExtraMatchingAttributes);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case StringLength:
			{
				{
				pr_LengthMatch(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case IfPresentMatch:
				{
					AST tmp126_AST_in = (AST)_t;
					match(_t,IfPresentMatch);
					_t = _t.getNextSibling();
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case IfPresentMatch:
			{
				AST tmp127_AST_in = (AST)_t;
				match(_t,IfPresentMatch);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t368;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleValueOrAttrib(AST _t) throws RecognitionException {
		
		AST pr_SingleValueOrAttrib_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t324 = _t;
			AST tmp128_AST_in = (AST)_t;
			match(_t,SingleValueOrAttrib);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateRefWithParList:
			{
				pr_TemplateRefWithParList(_t);
				_t = _retTree;
				break;
			}
			case MatchingSymbol:
			{
				pr_MatchingSymbol(_t);
				_t = _retTree;
				break;
			}
			case SingleExpression:
			{
				pr_SingleExpression(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SingleValueOrAttrib:
			{
				pr_SimpleSpec(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t324;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleTemplateSpec(AST _t) throws RecognitionException {
		
		AST pr_SingleTemplateSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_SingleTemplateExpression(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleTemplateExpression(AST _t) throws RecognitionException {
		
		AST pr_SingleTemplateExpression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<Symbol> b;
		
		try {      // for error handling
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case MatchingSymbol:
			{
				pr_MatchingSymbol(_t);
				_t = _retTree;
				break;
			}
			case TemplateRefWithParList:
			{
				{
				pr_TemplateRefWithParList(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case ExtendedFieldReference:
				{
					b=pr_ExtendedFieldReference(_t);
					_t = _retTree;
					break;
				}
				case 3:
				case SingleValueOrAttrib:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SingleValueOrAttrib:
			{
				pr_SimpleSpec(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_MatchingSymbol(AST _t) throws RecognitionException {
		
		AST pr_MatchingSymbol_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t340 = _t;
			AST tmp129_AST_in = (AST)_t;
			match(_t,MatchingSymbol);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Complement:
			{
				pr_Complement(_t);
				_t = _retTree;
				break;
			}
			case AnyValue:
			{
				{
				AST tmp130_AST_in = (AST)_t;
				match(_t,AnyValue);
				_t = _t.getNextSibling();
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case WildcardLengthMatch:
				{
					pr_WildcardLengthMatch(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case AnyOrOmit:
			{
				{
				AST tmp131_AST_in = (AST)_t;
				match(_t,AnyOrOmit);
				_t = _t.getNextSibling();
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case WildcardLengthMatch:
				{
					pr_WildcardLengthMatch(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case ListOfTemplates:
			{
				pr_ListOfTemplates(_t);
				_t = _retTree;
				break;
			}
			case Range:
			{
				pr_Range(_t);
				_t = _retTree;
				break;
			}
			case BitStringMatch:
			{
				pr_BitStringMatch(_t);
				_t = _retTree;
				break;
			}
			case HexStringMatch:
			{
				pr_HexStringMatch(_t);
				_t = _retTree;
				break;
			}
			case OctetStringMatch:
			{
				pr_OctetStringMatch(_t);
				_t = _retTree;
				break;
			}
			case CharStringMatch:
			{
				pr_CharStringMatch(_t);
				_t = _retTree;
				break;
			}
			case SubsetMatch:
			{
				pr_SubsetMatch(_t);
				_t = _retTree;
				break;
			}
			case SupersetMatch:
			{
				pr_SupersetMatch(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t340;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TemplateRefWithParList(AST _t) throws RecognitionException {
		
		AST pr_TemplateRefWithParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t414 = _t;
			AST tmp132_AST_in = (AST)_t;
			match(_t,TemplateRefWithParList);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GlobalModuleId:
			{
				a=pr_GlobalModuleId(_t);
				_t = _retTree;
				break;
			}
			case Identifier:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			b=pr_Identifier(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateActualParList:
			{
				pr_TemplateActualParList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t414;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final List<Symbol>  pr_ExtendedFieldReference(AST _t) throws RecognitionException {
		List<Symbol> r=new ArrayList<Symbol>();;
		
		AST pr_ExtendedFieldReference_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; TypeSymbol.PredefinedTypeEnum t=null;
		
		try {      // for error handling
			AST __t1521 = _t;
			AST tmp133_AST_in = (AST)_t;
			match(_t,ExtendedFieldReference);
			_t = _t.getFirstChild();
			{
			{
			int _cnt1525=0;
			_loop1525:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_tokenSet_0.member(_t.getType()))) {
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case Identifier:
					{
						a=pr_Identifier(_t);
						_t = _retTree;
						if ( inputState.guessing==0 ) {
							r.add(a);
						}
						break;
					}
					case PredefinedType:
					{
						t=pr_PredefinedType(_t);
						_t = _retTree;
						break;
					}
					case ArrayOrBitRef:
					{
						pr_ArrayOrBitRef(_t);
						_t = _retTree;
						break;
					}
					case NotUsedSymbol:
					{
						AST tmp134_AST_in = (AST)_t;
						match(_t,NotUsedSymbol);
						_t = _t.getNextSibling();
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					if ( _cnt1525>=1 ) { break _loop1525; } else {throw new NoViableAltException(_t);}
				}
				
				_cnt1525++;
			} while (true);
			}
			}
			_t = __t1521;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return r;
	}
	
	public final void pr_FieldSpec(AST _t) throws RecognitionException {
		
		AST pr_FieldSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t309 = _t;
			AST tmp135_AST_in = (AST)_t;
			match(_t,FieldSpec);
			_t = _t.getFirstChild();
			pr_FieldReference(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateBody:
			{
				pr_TemplateBody(_t);
				_t = _retTree;
				break;
			}
			case Minus:
			{
				AST tmp136_AST_in = (AST)_t;
				match(_t,Minus);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t309;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FieldReference(AST _t) throws RecognitionException {
		
		AST pr_FieldReference_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t312 = _t;
			AST tmp137_AST_in = (AST)_t;
			match(_t,FieldReference);
			_t = _t.getFirstChild();
			{
			boolean synPredMatched315 = false;
			if (_t==null) _t=ASTNULL;
			if (((_t.getType()==Identifier||_t.getType()==PredefinedType))) {
				AST __t315 = _t;
				synPredMatched315 = true;
				inputState.guessing++;
				try {
					{
					pr_StructFieldRef(_t);
					_t = _retTree;
					}
				}
				catch (RecognitionException pe) {
					synPredMatched315 = false;
				}
				_t = __t315;
inputState.guessing--;
			}
			if ( synPredMatched315 ) {
				pr_StructFieldRef(_t);
				_t = _retTree;
			}
			else if ((_t.getType()==ArrayOrBitRef)) {
				pr_ArrayOrBitRef(_t);
				_t = _retTree;
			}
			else {
				boolean synPredMatched317 = false;
				if (_t==null) _t=ASTNULL;
				if (((_t.getType()==Identifier))) {
					AST __t317 = _t;
					synPredMatched317 = true;
					inputState.guessing++;
					try {
						{
						pr_ParRef(_t);
						_t = _retTree;
						}
					}
					catch (RecognitionException pe) {
						synPredMatched317 = false;
					}
					_t = __t317;
inputState.guessing--;
				}
				if ( synPredMatched317 ) {
					pr_ParRef(_t);
					_t = _retTree;
				}
				else {
					throw new NoViableAltException(_t);
				}
				}
				}
				_t = __t312;
				_t = _t.getNextSibling();
			}
			catch (RecognitionException ex) {
				if (inputState.guessing==0) {
					reportError(ex);
					if (_t!=null) {_t = _t.getNextSibling();}
				} else {
				  throw ex;
				}
			}
			_retTree = _t;
		}
		
	public final void pr_StructFieldRef(AST _t) throws RecognitionException {
		
		AST pr_StructFieldRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		TypeSymbol.PredefinedTypeEnum a; Symbol b;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case PredefinedType:
			{
				a=pr_PredefinedType(_t);
				_t = _retTree;
				break;
			}
			case Identifier:
			{
				b=pr_Identifier(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ArrayOrBitRef(AST _t) throws RecognitionException {
		
		AST pr_ArrayOrBitRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t322 = _t;
			AST tmp138_AST_in = (AST)_t;
			match(_t,ArrayOrBitRef);
			_t = _t.getFirstChild();
			pr_SingleExpression(_t);
			_t = _retTree;
			_t = __t322;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ParRef(AST _t) throws RecognitionException {
		
		AST pr_ParRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_SignatureParIdentifier(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final TypeSymbol.PredefinedTypeEnum  pr_PredefinedType(AST _t) throws RecognitionException {
		TypeSymbol.PredefinedTypeEnum typeEnum=null;
		
		AST pr_PredefinedType_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		AST b = null;
		AST c = null;
		AST d = null;
		AST e = null;
		AST f = null;
		AST g = null;
		AST h = null;
		AST i = null;
		AST j = null;
		AST k = null;
		AST l = null;
		AST m = null;
		
		try {      // for error handling
			AST __t1182 = _t;
			AST tmp139_AST_in = (AST)_t;
			match(_t,PredefinedType);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case BitStringKeyword:
			{
				a = (AST)_t;
				match(_t,BitStringKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_bitstring;
				}
				break;
			}
			case BooleanKeyword:
			{
				b = (AST)_t;
				match(_t,BooleanKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_boolean;
				}
				break;
			}
			case CharStringKeyword:
			{
				c = (AST)_t;
				match(_t,CharStringKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_charstring;
				}
				break;
			}
			case UniversalCharString:
			{
				d = (AST)_t;
				match(_t,UniversalCharString);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_universalcharstring;
				}
				break;
			}
			case IntegerKeyword:
			{
				e = (AST)_t;
				match(_t,IntegerKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_integer;
				}
				break;
			}
			case OctetStringKeyword:
			{
				f = (AST)_t;
				match(_t,OctetStringKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_octetstring;
				}
				break;
			}
			case HexStringKeyword:
			{
				g = (AST)_t;
				match(_t,HexStringKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_hexstring;
				}
				break;
			}
			case VerdictTypeKeyword:
			{
				h = (AST)_t;
				match(_t,VerdictTypeKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_verdicttype;
				}
				break;
			}
			case FloatKeyword:
			{
				i = (AST)_t;
				match(_t,FloatKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_float;
				}
				break;
			}
			case AddressKeyword:
			{
				j = (AST)_t;
				match(_t,AddressKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_address;
				}
				break;
			}
			case DefaultKeyword:
			{
				k = (AST)_t;
				match(_t,DefaultKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_default;
				}
				break;
			}
			case AnyTypeKeyword:
			{
				l = (AST)_t;
				match(_t,AnyTypeKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_anytype;
				}
				break;
			}
			case OBJID:
			{
				m = (AST)_t;
				match(_t,OBJID);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					typeEnum = TypeSymbol.PredefinedTypeEnum.ttcn3_objid;
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1182;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return typeEnum;
	}
	
	public final void pr_SignatureParIdentifier(AST _t) throws RecognitionException {
		
		AST pr_SignatureParIdentifier_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			a=pr_Identifier(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleExpression(AST _t) throws RecognitionException {
		
		AST pr_SingleExpression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1470 = _t;
			AST tmp140_AST_in = (AST)_t;
			match(_t,SingleExpression);
			_t = _t.getFirstChild();
			pr_SimpleExpressions(_t);
			_t = _retTree;
			_t = __t1470;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ArrayElementSpec(AST _t) throws RecognitionException {
		
		AST pr_ArrayElementSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t335 = _t;
			AST tmp141_AST_in = (AST)_t;
			match(_t,ArrayElementSpec);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case NotUsedSymbol:
			{
				AST tmp142_AST_in = (AST)_t;
				match(_t,NotUsedSymbol);
				_t = _t.getNextSibling();
				break;
			}
			case TemplateBody:
			{
				pr_TemplateBody(_t);
				_t = _retTree;
				break;
			}
			case PermutationMatch:
			{
				pr_PermutationMatch(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t335;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PermutationMatch(AST _t) throws RecognitionException {
		
		AST pr_PermutationMatch_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t338 = _t;
			AST tmp143_AST_in = (AST)_t;
			match(_t,PermutationMatch);
			_t = _t.getFirstChild();
			pr_ListOfTemplates(_t);
			_t = _retTree;
			_t = __t338;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ListOfTemplates(AST _t) throws RecognitionException {
		
		AST pr_ListOfTemplates_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t353 = _t;
			AST tmp144_AST_in = (AST)_t;
			match(_t,ListOfTemplates);
			_t = _t.getFirstChild();
			{
			pr_TemplateListItem(_t);
			_t = _retTree;
			{
			_loop356:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==AllElementsFrom||_t.getType()==TemplateBody)) {
					pr_TemplateListItem(_t);
					_t = _retTree;
				}
				else {
					break _loop356;
				}
				
			} while (true);
			}
			}
			_t = __t353;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Complement(AST _t) throws RecognitionException {
		
		AST pr_Complement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t387 = _t;
			AST tmp145_AST_in = (AST)_t;
			match(_t,Complement);
			_t = _t.getFirstChild();
			{
			pr_ListOfTemplates(_t);
			_t = _retTree;
			}
			_t = __t387;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_WildcardLengthMatch(AST _t) throws RecognitionException {
		
		AST pr_WildcardLengthMatch_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t395 = _t;
			AST tmp146_AST_in = (AST)_t;
			match(_t,WildcardLengthMatch);
			_t = _t.getFirstChild();
			pr_ConstantExpression(_t);
			_t = _retTree;
			_t = __t395;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Range(AST _t) throws RecognitionException {
		
		AST pr_Range_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t397 = _t;
			AST tmp147_AST_in = (AST)_t;
			match(_t,Range);
			_t = _t.getFirstChild();
			pr_LowerBound(_t);
			_t = _retTree;
			pr_UpperBound(_t);
			_t = _retTree;
			_t = __t397;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_BitStringMatch(AST _t) throws RecognitionException {
		
		AST pr_BitStringMatch_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST b = null;
		
		try {      // for error handling
			AST __t378 = _t;
			AST tmp148_AST_in = (AST)_t;
			match(_t,BitStringMatch);
			_t = _t.getFirstChild();
			{
			b = (AST)_t;
			match(_t,BSTRINGMATCH);
			_t = _t.getNextSibling();
			}
			_t = __t378;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_HexStringMatch(AST _t) throws RecognitionException {
		
		AST pr_HexStringMatch_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST b = null;
		
		try {      // for error handling
			AST __t381 = _t;
			AST tmp149_AST_in = (AST)_t;
			match(_t,HexStringMatch);
			_t = _t.getFirstChild();
			{
			b = (AST)_t;
			match(_t,HSTRINGMATCH);
			_t = _t.getNextSibling();
			}
			_t = __t381;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_OctetStringMatch(AST _t) throws RecognitionException {
		
		AST pr_OctetStringMatch_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST b = null;
		
		try {      // for error handling
			AST __t384 = _t;
			AST tmp150_AST_in = (AST)_t;
			match(_t,OctetStringMatch);
			_t = _t.getFirstChild();
			{
			b = (AST)_t;
			match(_t,OSTRINGMATCH);
			_t = _t.getNextSibling();
			}
			_t = __t384;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SubsetMatch(AST _t) throws RecognitionException {
		
		AST pr_SubsetMatch_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t373 = _t;
			AST tmp151_AST_in = (AST)_t;
			match(_t,SubsetMatch);
			_t = _t.getFirstChild();
			pr_ListOfTemplates(_t);
			_t = _retTree;
			_t = __t373;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SupersetMatch(AST _t) throws RecognitionException {
		
		AST pr_SupersetMatch_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t376 = _t;
			AST tmp152_AST_in = (AST)_t;
			match(_t,SupersetMatch);
			_t = _t.getFirstChild();
			pr_ListOfTemplates(_t);
			_t = _retTree;
			_t = __t376;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TemplateListItem(AST _t) throws RecognitionException {
		
		AST pr_TemplateListItem_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateBody:
			{
				pr_TemplateBody(_t);
				_t = _retTree;
				break;
			}
			case AllElementsFrom:
			{
				pr_AllElementsFrom(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AllElementsFrom(AST _t) throws RecognitionException {
		
		AST pr_AllElementsFrom_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t360 = _t;
			AST tmp153_AST_in = (AST)_t;
			match(_t,AllElementsFrom);
			_t = _t.getFirstChild();
			{
			pr_TemplateBody(_t);
			_t = _retTree;
			}
			_t = __t360;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ValueOrAttribList(AST _t) throws RecognitionException {
		
		AST pr_ValueOrAttribList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t363 = _t;
			AST tmp154_AST_in = (AST)_t;
			match(_t,ValueOrAttribList);
			_t = _t.getFirstChild();
			{
			pr_TemplateBody(_t);
			_t = _retTree;
			{
			_loop366:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==TemplateBody)) {
					pr_TemplateBody(_t);
					_t = _retTree;
				}
				else {
					break _loop366;
				}
				
			} while (true);
			}
			}
			_t = __t363;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_LengthMatch(AST _t) throws RecognitionException {
		
		AST pr_LengthMatch_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_StringLength(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PermutaionMatch(AST _t) throws RecognitionException {
		
		AST pr_PermutaionMatch_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_ValueList(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ValueList(AST _t) throws RecognitionException {
		
		AST pr_ValueList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t390 = _t;
			AST tmp155_AST_in = (AST)_t;
			match(_t,ValueList);
			_t = _t.getFirstChild();
			{
			_loop392:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ConstantExpression)) {
					pr_ConstantExpression(_t);
					_t = _retTree;
				}
				else {
					break _loop392;
				}
				
			} while (true);
			}
			_t = __t390;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ExclamationMark(AST _t) throws RecognitionException {
		
		AST pr_ExclamationMark_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t408 = _t;
			AST tmp156_AST_in = (AST)_t;
			match(_t,ExclamationMark);
			_t = _t.getFirstChild();
			AST tmp157_AST_in = (AST)_t;
			match(_t,EXCLAMATIONMARK);
			_t = _t.getNextSibling();
			_t = __t408;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TemplateInstance(AST _t) throws RecognitionException {
		
		AST pr_TemplateInstance_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t410 = _t;
			AST tmp158_AST_in = (AST)_t;
			match(_t,TemplateInstance);
			_t = _t.getFirstChild();
			pr_InLineTemplate(_t);
			_t = _retTree;
			_t = __t410;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_InLineTemplate(AST _t) throws RecognitionException {
		
		AST pr_InLineTemplate_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t418 = _t;
			AST tmp159_AST_in = (AST)_t;
			match(_t,InLineTemplate);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Type:
			{
				pr_Type(_t,null);
				_t = _retTree;
				break;
			}
			case DerivedRefWithParList:
			case TemplateBody:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case DerivedRefWithParList:
			{
				pr_DerivedRefWithParList(_t);
				_t = _retTree;
				break;
			}
			case TemplateBody:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			pr_TemplateBody(_t);
			_t = _retTree;
			_t = __t418;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TemplateInstanceAssignment(AST _t) throws RecognitionException {
		
		AST pr_TemplateInstanceAssignment_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; Symbol b;
		
		try {      // for error handling
			AST __t412 = _t;
			AST tmp160_AST_in = (AST)_t;
			match(_t,TemplateInstanceAssignment);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
						
						//	System.out.println("Instance Symbol "+instanceSymbol);
				//	System.out.println("Scope "+instanceSymbol.getDeclarationNode());
				//	System.out.println("==>"+((LocationAST) _t).getContainingModuleDefinitionIdentifiersList().get(0));
				//	//TODO; Testcase context has to be considered as well
				//	if (instanceSymbol != null && (!(preparedScopeStack.empty()))){
				//		//((FunctionSymbol)instanceSymbol).get.getScope();
				//		ScopeWithKnownName scopeWithName = new ScopeWithKnownName(scopeStack.peek(),
				//				preparedScopeStack.peek(), instanceSymbol.getName(), false);
				//		knownScopes.add(scopeWithName);
				//
				//	
				//	} else {
				//		System.out.println("Symbol not resolved! "+instanceSymbol);
				//	}
				//		
				//		if ((typeSymb.getReferencedType() != null) && (!(preparedScopeStack.empty()))) {
				//				ScopeWithKnownName scopeWithName = new ScopeWithKnownName(scopeStack.peek(),
				//					preparedScopeStack.peek(), typeSymb.getReferencedType().getIdentifier().getName(), false);
				//				knownScopes.add(scopeWithName);
				//		}
						
			}
			a=pr_Identifier(_t);
			_t = _retTree;
			pr_InLineTemplate(_t);
			_t = _retTree;
			_t = __t412;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TemplateActualParList(AST _t) throws RecognitionException {
		
		AST pr_TemplateActualParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t424 = _t;
			AST tmp161_AST_in = (AST)_t;
			match(_t,TemplateActualParList);
			_t = _t.getFirstChild();
			{
			boolean synPredMatched427 = false;
			if (_t==null) _t=ASTNULL;
			if (((_t.getType()==3||_t.getType()==TemplateActualPar))) {
				AST __t427 = _t;
				synPredMatched427 = true;
				inputState.guessing++;
				try {
					{
					pr_TemplateActualPar(_t);
					_t = _retTree;
					}
				}
				catch (RecognitionException pe) {
					synPredMatched427 = false;
				}
				_t = __t427;
inputState.guessing--;
			}
			if ( synPredMatched427 ) {
				{
				_loop429:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==TemplateActualPar)) {
						pr_TemplateActualPar(_t);
						_t = _retTree;
					}
					else {
						break _loop429;
					}
					
				} while (true);
				}
			}
			else if ((_t.getType()==3||_t.getType()==TemplateActualParAssignment)) {
				{
				_loop431:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==TemplateActualParAssignment)) {
						pr_TemplateActualParAssignment(_t);
						_t = _retTree;
					}
					else {
						break _loop431;
					}
					
				} while (true);
				}
			}
			else {
				throw new NoViableAltException(_t);
			}
			
			}
			_t = __t424;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_DerivedRefWithParList(AST _t) throws RecognitionException {
		
		AST pr_DerivedRefWithParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t422 = _t;
			AST tmp162_AST_in = (AST)_t;
			match(_t,DerivedRefWithParList);
			_t = _t.getFirstChild();
			pr_TemplateRefWithParList(_t);
			_t = _retTree;
			_t = __t422;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TemplateActualPar(AST _t) throws RecognitionException {
		
		AST pr_TemplateActualPar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t433 = _t;
			AST tmp163_AST_in = (AST)_t;
			match(_t,TemplateActualPar);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateInstance:
			{
				pr_TemplateInstance(_t);
				_t = _retTree;
				break;
			}
			case Dash:
			{
				AST tmp164_AST_in = (AST)_t;
				match(_t,Dash);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t433;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TemplateActualParAssignment(AST _t) throws RecognitionException {
		
		AST pr_TemplateActualParAssignment_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t436 = _t;
			AST tmp165_AST_in = (AST)_t;
			match(_t,TemplateActualParAssignment);
			_t = _t.getFirstChild();
			{
			pr_TemplateInstanceAssignment(_t);
			_t = _retTree;
			}
			_t = __t436;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TemplateOps(AST _t) throws RecognitionException {
		
		AST pr_TemplateOps_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t439 = _t;
			AST tmp166_AST_in = (AST)_t;
			match(_t,TemplateOps);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case MatchOp:
			{
				pr_MatchOp(_t);
				_t = _retTree;
				break;
			}
			case ValueofOp:
			{
				pr_ValueofOp(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t439;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_MatchOp(AST _t) throws RecognitionException {
		
		AST pr_MatchOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t442 = _t;
			AST tmp167_AST_in = (AST)_t;
			match(_t,MatchOp);
			_t = _t.getFirstChild();
			pr_Expression(_t);
			_t = _retTree;
			pr_TemplateInstance(_t);
			_t = _retTree;
			_t = __t442;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ValueofOp(AST _t) throws RecognitionException {
		
		AST pr_ValueofOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t444 = _t;
			AST tmp168_AST_in = (AST)_t;
			match(_t,ValueofOp);
			_t = _t.getFirstChild();
			pr_TemplateInstance(_t);
			_t = _retTree;
			_t = __t444;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Expression(AST _t) throws RecognitionException {
		
		AST pr_Expression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1419 = _t;
			AST tmp169_AST_in = (AST)_t;
			match(_t,Expression);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SingleExpression:
			{
				pr_SingleExpression(_t);
				_t = _retTree;
				break;
			}
			case CompoundExpression:
			{
				pr_CompoundExpression(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1419;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FunctionFormalParList(AST _t) throws RecognitionException {
		
		AST pr_FunctionFormalParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_FunctionFormalPar(_t);
			_t = _retTree;
			{
			_loop452:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_tokenSet_1.member(_t.getType()))) {
					pr_FunctionFormalPar(_t);
					_t = _retTree;
				}
				else {
					break _loop452;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final Reference  pr_RunsOnSpec(AST _t) throws RecognitionException {
		Reference r=null;
		
		AST pr_RunsOnSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t458 = _t;
			AST tmp170_AST_in = (AST)_t;
			match(_t,RunsOnSpec);
			_t = _t.getFirstChild();
			r=pr_ComponentType(_t);
			_t = _retTree;
			_t = __t458;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return r;
	}
	
	public final void pr_ReturnType(AST _t) throws RecognitionException {
		
		AST pr_ReturnType_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t455 = _t;
			AST tmp171_AST_in = (AST)_t;
			match(_t,ReturnType);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateKeyword:
			{
				AST tmp172_AST_in = (AST)_t;
				match(_t,TemplateKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case RestrictedTemplate:
			{
				pr_RestrictedTemplate(_t);
				_t = _retTree;
				break;
			}
			case Type:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			pr_Type(_t,null);
			_t = _retTree;
			_t = __t455;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_StatementBlock(AST _t) throws RecognitionException {
		
		AST pr_StatementBlock_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Scope scopeToPop = null;
		
		try {      // for error handling
			AST __t460 = _t;
			AST tmp173_AST_in = (AST)_t;
			match(_t,StatementBlock);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
					scopeToPop = scopeStack.peek();
							pushNewScope(true); 
						
			}
			{
			_loop465:
			do {
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case FunctionLocalDef:
				case FunctionLocalInst:
				{
					{
					pr_FunctionLocalDefOrInst(_t);
					_t = _retTree;
					}
					break;
				}
				case FunctionStatement:
				{
					{
					pr_FunctionStatement(_t);
					_t = _retTree;
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp174_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case FunctionLocalDef:
					case FunctionLocalInst:
					case FunctionStatement:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
					}
					break;
				}
				default:
				{
					break _loop465;
				}
				}
			} while (true);
			}
			if ( inputState.guessing==0 ) {
					popToScope(scopeToPop); 	
						
			}
			_t = __t460;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FunctionFormalPar(AST _t) throws RecognitionException {
		
		AST pr_FunctionFormalPar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FormalValuePar:
			{
				pr_FormalValuePar(_t);
				_t = _retTree;
				break;
			}
			case FormalTimerPar:
			{
				pr_FormalTimerPar(_t);
				_t = _retTree;
				break;
			}
			case FormalTemplatePar:
			{
				pr_FormalTemplatePar(_t);
				_t = _retTree;
				break;
			}
			case FormalPortPar:
			{
				pr_FormalPortPar(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FormalTimerPar(AST _t) throws RecognitionException {
		
		AST pr_FormalTimerPar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; FormalParameterSymbol symbol = new FormalParameterSymbol();
		
		try {      // for error handling
			AST __t1279 = _t;
			AST tmp175_AST_in = (AST)_t;
			match(_t,FormalTimerPar);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case InOutParKeyword:
			{
				AST tmp176_AST_in = (AST)_t;
				match(_t,InOutParKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case TimerKeyword:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			AST tmp177_AST_in = (AST)_t;
			match(_t,TimerKeyword);
			_t = _t.getNextSibling();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName());
							symbol.setDeclarationNode(a.getDeclarationNode());
							symbol.setParameterType(FormalParameterSymbol.ParameterEnum.timer);
							addSymbolToScope(symbol);
						
			}
			_t = __t1279;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FormalPortPar(AST _t) throws RecognitionException {
		
		AST pr_FormalPortPar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b; FormalParameterSymbol symbol = new FormalParameterSymbol();
		
		try {      // for error handling
			AST __t1276 = _t;
			AST tmp178_AST_in = (AST)_t;
			match(_t,FormalPortPar);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case InOutParKeyword:
			{
				AST tmp179_AST_in = (AST)_t;
				match(_t,InOutParKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case Identifier:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			a=pr_Identifier(_t);
			_t = _retTree;
			b=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setPortTypeIdentifier(b.getName());
							symbol.setName(a.getName());
							symbol.setDeclarationNode(a.getDeclarationNode());
							symbol.setParameterType(FormalParameterSymbol.ParameterEnum.port);
							addSymbolToScope(symbol);
						
			}
			_t = __t1276;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FunctionLocalDefOrInst(AST _t) throws RecognitionException {
		
		AST pr_FunctionLocalDefOrInst_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST b = null;
		AST c = null;
		Scope lastScope=null; AST a = null;
		
		try {      // for error handling
			{
			if ( inputState.guessing==0 ) {
					lastScope = scopeStack.peek(); 
						   		pushNewScope(true); 
						   		tempSymbol = null;
						   	
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FunctionLocalDef:
			{
				b = _t==ASTNULL ? null : (AST)_t;
				pr_FunctionLocalDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					a = b;
				}
				break;
			}
			case FunctionLocalInst:
			{
				c = _t==ASTNULL ? null : (AST)_t;
				pr_FunctionLocalInst(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					a = c;
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				
					    		lastScope.setEndOffset(((LocationAST)a).getEndOffset()); 
								scopeStack.peek().setScopeSymbol(tempSymbol);
								tempSymbol=null;
					    		scopeStack.peek().setStartOffset(((LocationAST)a).getEndOffset()+1);
				
				//				popToScope(lastScope);
				//				lastScope = null; 
				//	    		scopeStack.pop();  
				//	    		((LocationAST)#upper).setScope(scopeStack.peek());
					    	
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case WithStatement:
			{
				pr_WithStatement(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case FunctionLocalDef:
			case FunctionLocalInst:
			case FunctionStatement:
			case SemiColon:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SemiColon:
			{
				AST tmp180_AST_in = (AST)_t;
				match(_t,SemiColon);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			case FunctionLocalDef:
			case FunctionLocalInst:
			case FunctionStatement:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FunctionStatement(AST _t) throws RecognitionException {
		
		AST pr_FunctionStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t482 = _t;
			AST tmp181_AST_in = (AST)_t;
			match(_t,FunctionStatement);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ConfigurationStatements:
			{
				pr_ConfigurationStatements(_t);
				_t = _retTree;
				break;
			}
			case TimerStatements:
			{
				pr_TimerStatements(_t);
				_t = _retTree;
				break;
			}
			case CommunicationStatements:
			{
				pr_CommunicationStatements(_t);
				_t = _retTree;
				break;
			}
			case BasicStatements:
			{
				pr_BasicStatements(_t);
				_t = _retTree;
				break;
			}
			case BehaviourStatements:
			{
				pr_BehaviourStatements(_t);
				_t = _retTree;
				break;
			}
			case SetLocalVerdict:
			{
				pr_VerdictStatements(_t);
				_t = _retTree;
				break;
			}
			case SUTStatements:
			{
				pr_SUTStatements(_t);
				_t = _retTree;
				break;
			}
			case TestcaseOperation:
			{
				pr_TestcaseOperation(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t482;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FunctionLocalDef(AST _t) throws RecognitionException {
		
		AST pr_FunctionLocalDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t479 = _t;
			AST tmp182_AST_in = (AST)_t;
			match(_t,FunctionLocalDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ConstDef:
			{
				pr_ConstDef(_t);
				_t = _retTree;
				break;
			}
			case TemplateDef:
			{
				pr_TemplateDef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t479;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FunctionLocalInst(AST _t) throws RecognitionException {
		
		AST pr_FunctionLocalInst_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t476 = _t;
			AST tmp183_AST_in = (AST)_t;
			match(_t,FunctionLocalInst);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case VarInstance:
			{
				pr_VarInstance(_t);
				_t = _retTree;
				break;
			}
			case TimerInstance:
			{
				pr_TimerInstance(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t476;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FunctionStatementList(AST _t) throws RecognitionException {
		
		AST pr_FunctionStatementList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			{
			_loop474:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FunctionStatement)) {
					pr_FunctionStatement(_t);
					_t = _retTree;
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp184_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case FunctionStatement:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop474;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.2.3 - Component Operations  *****/
	public final void pr_ConfigurationStatements(AST _t) throws RecognitionException {
		
		AST pr_ConfigurationStatements_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t877 = _t;
			AST tmp185_AST_in = (AST)_t;
			match(_t,ConfigurationStatements);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ConnectStatement:
			{
				pr_ConnectStatement(_t);
				_t = _retTree;
				break;
			}
			case MapStatement:
			{
				pr_MapStatement(_t);
				_t = _retTree;
				break;
			}
			case DisconnectStatement:
			{
				pr_DisconnectStatement(_t);
				_t = _retTree;
				break;
			}
			case UnmapStatement:
			{
				pr_UnmapStatement(_t);
				_t = _retTree;
				break;
			}
			case DoneStatement:
			{
				pr_DoneStatement(_t);
				_t = _retTree;
				break;
			}
			case KilledStatement:
			{
				pr_KilledStatement(_t);
				_t = _retTree;
				break;
			}
			case StartTCStatement:
			{
				pr_StartTCStatement(_t);
				_t = _retTree;
				break;
			}
			case StopTCStatement:
			{
				pr_StopTCStatement(_t);
				_t = _retTree;
				break;
			}
			case KillTCStatement:
			{
				pr_KillTCStatement(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t877;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.2.5 - Timer Operations  *****/
	public final void pr_TimerStatements(AST _t) throws RecognitionException {
		
		AST pr_TimerStatements_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1151 = _t;
			AST tmp186_AST_in = (AST)_t;
			match(_t,TimerStatements);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case StartTimerStatement:
			{
				pr_StartTimerStatement(_t);
				_t = _retTree;
				break;
			}
			case StopTimerStatement:
			{
				pr_StopTimerStatement(_t);
				_t = _retTree;
				break;
			}
			case TimeoutStatement:
			{
				pr_TimeoutStatement(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1151;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CommunicationStatements(AST _t) throws RecognitionException {
		
		AST pr_CommunicationStatements_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t966 = _t;
			AST tmp187_AST_in = (AST)_t;
			match(_t,CommunicationStatements);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SendStatement:
			{
				pr_SendStatement(_t);
				_t = _retTree;
				break;
			}
			case CallStatement:
			{
				pr_CallStatement(_t);
				_t = _retTree;
				break;
			}
			case ReplyStatement:
			{
				pr_ReplyStatement(_t);
				_t = _retTree;
				break;
			}
			case RaiseStatement:
			{
				pr_RaiseStatement(_t);
				_t = _retTree;
				break;
			}
			case ReceiveStatement:
			{
				pr_ReceiveStatement(_t);
				_t = _retTree;
				break;
			}
			case TriggerStatement:
			{
				pr_TriggerStatement(_t);
				_t = _retTree;
				break;
			}
			case GetCallStatement:
			{
				pr_GetCallStatement(_t);
				_t = _retTree;
				break;
			}
			case GetReplyStatement:
			{
				pr_GetReplyStatement(_t);
				_t = _retTree;
				break;
			}
			case CatchStatement:
			{
				pr_CatchStatement(_t);
				_t = _retTree;
				break;
			}
			case CheckStatement:
			{
				pr_CheckStatement(_t);
				_t = _retTree;
				break;
			}
			case ClearStatement:
			{
				pr_ClearStatement(_t);
				_t = _retTree;
				break;
			}
			case StartStatement:
			{
				pr_StartStatement(_t);
				_t = _retTree;
				break;
			}
			case StopStatement:
			{
				pr_StopStatement(_t);
				_t = _retTree;
				break;
			}
			case HaltStatement:
			{
				pr_HaltStatement(_t);
				_t = _retTree;
				break;
			}
			case CheckStateStatement:
			{
				pr_CheckStateStatement(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t966;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/***** 1.6.8 - Basic Statements  *****/
	public final void pr_BasicStatements(AST _t) throws RecognitionException {
		
		AST pr_BasicStatements_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST nodea = null;
		
		try {      // for error handling
			AST __t1402 = _t;
			AST tmp188_AST_in = (AST)_t;
			match(_t,BasicStatements);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Assignment:
			{
				pr_Assignment(_t);
				_t = _retTree;
				break;
			}
			case LogStatement:
			{
				pr_LogStatement(_t);
				_t = _retTree;
				break;
			}
			case LoopConstruct:
			{
				pr_LoopConstruct(_t);
				_t = _retTree;
				break;
			}
			case ConditionalConstruct:
			{
				pr_ConditionalConstruct(_t);
				_t = _retTree;
				break;
			}
			case SelectCaseConstruct:
			{
				pr_SelectCaseConstruct(_t);
				_t = _retTree;
				break;
			}
			case StatementBlock:
			{
				{
				nodea = _t==ASTNULL ? null : (AST)_t;
				pr_StatementBlock(_t);
				_t = _retTree;
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1402;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/*****  1.6.7 - Behaviour Statements  *****/
	public final void pr_BehaviourStatements(AST _t) throws RecognitionException {
		
		AST pr_BehaviourStatements_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1333 = _t;
			AST tmp189_AST_in = (AST)_t;
			match(_t,BehaviourStatements);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TestcaseInstance:
			{
				pr_TestcaseInstance(_t);
				_t = _retTree;
				break;
			}
			case FunctionInstance:
			{
				pr_FunctionInstance(_t);
				_t = _retTree;
				break;
			}
			case ReturnStatement:
			{
				pr_ReturnStatement(_t);
				_t = _retTree;
				break;
			}
			case AltConstruct:
			{
				pr_AltConstruct(_t);
				_t = _retTree;
				break;
			}
			case InterleavedConstruct:
			{
				pr_InterleavedConstruct(_t);
				_t = _retTree;
				break;
			}
			case LabelStatement:
			{
				pr_LabelStatement(_t);
				_t = _retTree;
				break;
			}
			case GotoStatement:
			{
				pr_GotoStatement(_t);
				_t = _retTree;
				break;
			}
			case ActivateOp:
			{
				pr_ActivateOp(_t);
				_t = _retTree;
				break;
			}
			case DeactivateStatement:
			{
				pr_DeactivateStatement(_t);
				_t = _retTree;
				break;
			}
			case AltstepInstance:
			{
				pr_AltstepInstance(_t);
				_t = _retTree;
				break;
			}
			case RepeatStatement:
			{
				pr_RepeatStatement(_t);
				_t = _retTree;
				break;
			}
			case BreakStatement:
			{
				pr_BreakStatement(_t);
				_t = _retTree;
				break;
			}
			case ContinueStatement:
			{
				pr_ContinueStatement(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1333;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_VerdictStatements(AST _t) throws RecognitionException {
		
		AST pr_VerdictStatements_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_SetLocalVerdict(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SUTStatements(AST _t) throws RecognitionException {
		
		AST pr_SUTStatements_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1343 = _t;
			AST tmp190_AST_in = (AST)_t;
			match(_t,SUTStatements);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case StringOp:
			{
				AST tmp191_AST_in = (AST)_t;
				match(_t,StringOp);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			case Expression:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Expression:
			{
				pr_Expression(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t1343;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TestcaseOperation(AST _t) throws RecognitionException {
		
		AST pr_TestcaseOperation_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		String a;
		
		try {      // for error handling
			AST __t1174 = _t;
			AST tmp192_AST_in = (AST)_t;
			match(_t,TestcaseOperation);
			_t = _t.getFirstChild();
			{
			{
			_loop1177:
			do {
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case CSTRING:
				{
					a=pr_FreeText(_t);
					_t = _retTree;
					break;
				}
				case InLineTemplate:
				{
					pr_InLineTemplate(_t);
					_t = _retTree;
					break;
				}
				default:
				{
					break _loop1177;
				}
				}
			} while (true);
			}
			}
			_t = __t1174;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FunctionInstance(AST _t) throws RecognitionException {
		
		AST pr_FunctionInstance_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t485 = _t;
			AST tmp193_AST_in = (AST)_t;
			match(_t,FunctionInstance);
			_t = _t.getFirstChild();
			pr_FunctionRef(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FunctionActualParList:
			{
				pr_FunctionActualParList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t485;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FunctionRef(AST _t) throws RecognitionException {
		
		AST pr_FunctionRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t488 = _t;
			AST tmp194_AST_in = (AST)_t;
			match(_t,FunctionRef);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GlobalModuleId:
			{
				a=pr_GlobalModuleId(_t);
				_t = _retTree;
				break;
			}
			case Identifier:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			b=pr_Identifier(_t);
			_t = _retTree;
			}
			_t = __t488;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FunctionActualParList(AST _t) throws RecognitionException {
		
		AST pr_FunctionActualParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t494 = _t;
			AST tmp195_AST_in = (AST)_t;
			match(_t,FunctionActualParList);
			_t = _t.getFirstChild();
			{
			boolean synPredMatched497 = false;
			if (_t==null) _t=ASTNULL;
			if (((_t.getType()==3||_t.getType()==FunctionActualPar))) {
				AST __t497 = _t;
				synPredMatched497 = true;
				inputState.guessing++;
				try {
					{
					pr_FunctionActualPar(_t);
					_t = _retTree;
					}
				}
				catch (RecognitionException pe) {
					synPredMatched497 = false;
				}
				_t = __t497;
inputState.guessing--;
			}
			if ( synPredMatched497 ) {
				{
				_loop499:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==FunctionActualPar)) {
						pr_FunctionActualPar(_t);
						_t = _retTree;
					}
					else {
						break _loop499;
					}
					
				} while (true);
				}
			}
			else if ((_t.getType()==3||_t.getType()==FunctionActualParAssignment)) {
				{
				_loop501:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==FunctionActualParAssignment)) {
						pr_FunctionActualParAssignment(_t);
						_t = _retTree;
					}
					else {
						break _loop501;
					}
					
				} while (true);
				}
			}
			else {
				throw new NoViableAltException(_t);
			}
			
			}
			_t = __t494;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FunctionActualPar(AST _t) throws RecognitionException {
		
		AST pr_FunctionActualPar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t512 = _t;
			AST tmp196_AST_in = (AST)_t;
			match(_t,FunctionActualPar);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateInstance:
			{
				pr_TemplateInstance(_t);
				_t = _retTree;
				break;
			}
			case TimerRef:
			{
				pr_TimerRef(_t);
				_t = _retTree;
				break;
			}
			case Port:
			{
				pr_Port(_t);
				_t = _retTree;
				break;
			}
			case ComponentRef:
			{
				pr_ComponentRef(_t);
				_t = _retTree;
				break;
			}
			case Dash:
			{
				AST tmp197_AST_in = (AST)_t;
				match(_t,Dash);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t512;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FunctionActualParAssignment(AST _t) throws RecognitionException {
		
		AST pr_FunctionActualParAssignment_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t503 = _t;
			AST tmp198_AST_in = (AST)_t;
			match(_t,FunctionActualParAssignment);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateInstanceAssignment:
			{
				pr_TemplateInstanceAssignment(_t);
				_t = _retTree;
				break;
			}
			case ComponentRefAssignment:
			{
				pr_ComponentRefAssignment(_t);
				_t = _retTree;
				break;
			}
			case PortRefAssignment:
			{
				pr_PortRefAssignment(_t);
				_t = _retTree;
				break;
			}
			case TimerRefAssignment:
			{
				pr_TimerRefAssignment(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t503;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ComponentRefAssignment(AST _t) throws RecognitionException {
		
		AST pr_ComponentRefAssignment_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t922 = _t;
			AST tmp199_AST_in = (AST)_t;
			match(_t,ComponentRefAssignment);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			pr_ComponentRef(_t);
			_t = _retTree;
			_t = __t922;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortRefAssignment(AST _t) throws RecognitionException {
		
		AST pr_PortRefAssignment_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t916 = _t;
			AST tmp200_AST_in = (AST)_t;
			match(_t,PortRefAssignment);
			_t = _t.getFirstChild();
			{
			a=pr_Identifier(_t);
			_t = _retTree;
			pr_Port(_t);
			_t = _retTree;
			}
			_t = __t916;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TimerRefAssignment(AST _t) throws RecognitionException {
		
		AST pr_TimerRefAssignment_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t874 = _t;
			AST tmp201_AST_in = (AST)_t;
			match(_t,TimerRefAssignment);
			_t = _t.getFirstChild();
			{
			a=pr_Identifier(_t);
			_t = _retTree;
			pr_TimerRef(_t);
			_t = _retTree;
			}
			_t = __t874;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TimerRef(AST _t) throws RecognitionException {
		
		AST pr_TimerRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t869 = _t;
			AST tmp202_AST_in = (AST)_t;
			match(_t,TimerRef);
			_t = _t.getFirstChild();
			{
			a=pr_Identifier(_t);
			_t = _retTree;
			{
			_loop872:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ArrayOrBitRef)) {
					pr_ArrayOrBitRef(_t);
					_t = _retTree;
				}
				else {
					break _loop872;
				}
				
			} while (true);
			}
			}
			_t = __t869;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/*****  1.6.3.4 - Port Operations  *****/
	public final void pr_Port(AST _t) throws RecognitionException {
		
		AST pr_Port_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t961 = _t;
			AST tmp203_AST_in = (AST)_t;
			match(_t,Port);
			_t = _t.getFirstChild();
			{
			a=pr_Identifier(_t);
			_t = _retTree;
			{
			_loop964:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ArrayOrBitRef)) {
					pr_ArrayOrBitRef(_t);
					_t = _retTree;
				}
				else {
					break _loop964;
				}
				
			} while (true);
			}
			}
			_t = __t961;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ComponentRef(AST _t) throws RecognitionException {
		
		AST pr_ComponentRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t919 = _t;
			AST tmp204_AST_in = (AST)_t;
			match(_t,ComponentRef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FunctionInstance:
			case VariableRef:
			{
				pr_ComponentOrDefaultReference(_t);
				_t = _retTree;
				break;
			}
			case SystemOp:
			{
				pr_SystemOp(_t);
				_t = _retTree;
				break;
			}
			case SelfOp:
			{
				pr_SelfOp(_t);
				_t = _retTree;
				break;
			}
			case MTCOp:
			{
				pr_MTCOp(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t919;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SignatureFormalParList(AST _t) throws RecognitionException {
		
		AST pr_SignatureFormalParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t526 = _t;
			AST tmp205_AST_in = (AST)_t;
			match(_t,SignatureFormalParList);
			_t = _t.getFirstChild();
			{
			_loop528:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FormalValuePar)) {
					pr_SignatureFormalPar(_t);
					_t = _retTree;
				}
				else {
					break _loop528;
				}
				
			} while (true);
			}
			_t = __t526;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ExceptionSpec(AST _t) throws RecognitionException {
		
		AST pr_ExceptionSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t531 = _t;
			AST tmp206_AST_in = (AST)_t;
			match(_t,ExceptionSpec);
			_t = _t.getFirstChild();
			pr_Type(_t,null);
			_t = _retTree;
			{
			_loop533:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Type)) {
					pr_Type(_t,null);
					_t = _retTree;
				}
				else {
					break _loop533;
				}
				
			} while (true);
			}
			_t = __t531;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SignatureFormalPar(AST _t) throws RecognitionException {
		
		AST pr_SignatureFormalPar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_FormalValuePar(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TestcaseFormalParList(AST _t) throws RecognitionException {
		
		AST pr_TestcaseFormalParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_TestcaseFormalPar(_t);
			_t = _retTree;
			{
			_loop545:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FormalTemplatePar||_t.getType()==FormalValuePar)) {
					pr_TestcaseFormalPar(_t);
					_t = _retTree;
				}
				else {
					break _loop545;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final List<Reference>  pr_ConfigSpec(AST _t) throws RecognitionException {
		List<Reference> reflist=new ArrayList<Reference>();;
		
		AST pr_ConfigSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Reference r; Reference system;
		
		try {      // for error handling
			AST __t548 = _t;
			AST tmp207_AST_in = (AST)_t;
			match(_t,ConfigSpec);
			_t = _t.getFirstChild();
			r=pr_RunsOnSpec(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				reflist.add(r);
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SystemSpec:
			{
				system=pr_SystemSpec(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					system.setSystemReference(true); reflist.add(system);
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t548;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return reflist;
	}
	
	public final void pr_TestcaseFormalPar(AST _t) throws RecognitionException {
		
		AST pr_TestcaseFormalPar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FormalValuePar:
			{
				pr_FormalValuePar(_t);
				_t = _retTree;
				break;
			}
			case FormalTemplatePar:
			{
				pr_FormalTemplatePar(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final Reference  pr_SystemSpec(AST _t) throws RecognitionException {
		Reference r=null;
		
		AST pr_SystemSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t551 = _t;
			AST tmp208_AST_in = (AST)_t;
			match(_t,SystemSpec);
			_t = _t.getFirstChild();
			r=pr_ComponentType(_t);
			_t = _retTree;
			_t = __t551;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return r;
	}
	
	public final void pr_TestcaseInstance(AST _t) throws RecognitionException {
		
		AST pr_TestcaseInstance_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t553 = _t;
			AST tmp209_AST_in = (AST)_t;
			match(_t,TestcaseInstance);
			_t = _t.getFirstChild();
			{
			pr_TestcaseRef(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TestcaseActualParList:
			{
				pr_TestcaseActualParList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case Expression:
			case Minus:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Expression:
			case Minus:
			{
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case Minus:
				{
					AST tmp210_AST_in = (AST)_t;
					match(_t,Minus);
					_t = _t.getNextSibling();
					break;
				}
				case Expression:
				{
					pr_TimerValue(_t);
					_t = _retTree;
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case SingleExpression:
				{
					pr_SingleExpression(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t553;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TestcaseRef(AST _t) throws RecognitionException {
		
		AST pr_TestcaseRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t560 = _t;
			AST tmp211_AST_in = (AST)_t;
			match(_t,TestcaseRef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GlobalModuleId:
			{
				a=pr_GlobalModuleId(_t);
				_t = _retTree;
				break;
			}
			case Identifier:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			b=pr_Identifier(_t);
			_t = _retTree;
			_t = __t560;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TestcaseActualParList(AST _t) throws RecognitionException {
		
		AST pr_TestcaseActualParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t565 = _t;
			AST tmp212_AST_in = (AST)_t;
			match(_t,TestcaseActualParList);
			_t = _t.getFirstChild();
			{
			boolean synPredMatched568 = false;
			if (_t==null) _t=ASTNULL;
			if (((_t.getType()==3||_t.getType()==TestcaseActualPar))) {
				AST __t568 = _t;
				synPredMatched568 = true;
				inputState.guessing++;
				try {
					{
					pr_TestcaseActualPar(_t);
					_t = _retTree;
					}
				}
				catch (RecognitionException pe) {
					synPredMatched568 = false;
				}
				_t = __t568;
inputState.guessing--;
			}
			if ( synPredMatched568 ) {
				{
				_loop570:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==TestcaseActualPar)) {
						pr_TestcaseActualPar(_t);
						_t = _retTree;
					}
					else {
						break _loop570;
					}
					
				} while (true);
				}
			}
			else if ((_t.getType()==3||_t.getType()==TestcaseActualParAssignment)) {
				{
				_loop572:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==TestcaseActualParAssignment)) {
						pr_TestcaseActualParAssignment(_t);
						_t = _retTree;
					}
					else {
						break _loop572;
					}
					
				} while (true);
				}
			}
			else {
				throw new NoViableAltException(_t);
			}
			
			}
			_t = __t565;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TimerValue(AST _t) throws RecognitionException {
		
		AST pr_TimerValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_Expression(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TestcaseActualPar(AST _t) throws RecognitionException {
		
		AST pr_TestcaseActualPar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t576 = _t;
			AST tmp213_AST_in = (AST)_t;
			match(_t,TestcaseActualPar);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateInstance:
			{
				pr_TemplateInstance(_t);
				_t = _retTree;
				break;
			}
			case Dash:
			{
				AST tmp214_AST_in = (AST)_t;
				match(_t,Dash);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t576;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TestcaseActualParAssignment(AST _t) throws RecognitionException {
		
		AST pr_TestcaseActualParAssignment_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t574 = _t;
			AST tmp215_AST_in = (AST)_t;
			match(_t,TestcaseActualParAssignment);
			_t = _t.getFirstChild();
			pr_TemplateInstanceAssignment(_t);
			_t = _retTree;
			_t = __t574;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AltstepFormalParList(AST _t) throws RecognitionException {
		
		AST pr_AltstepFormalParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_FunctionFormalParList(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AltstepLocalDefList(AST _t) throws RecognitionException {
		
		AST pr_AltstepLocalDefList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST upper = null;
		AST a = null;
		AST b = null;
		Scope lastScope=null;
		
		try {      // for error handling
			AST __t587 = _t;
			upper = _t==ASTNULL ? null :(AST)_t;
			match(_t,AltstepLocalDefList);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
						lastScope = scopeStack.peek(); 
						pushNewScope(true); 
						tempSymbol=null;
						
					
			}
			a = _t==ASTNULL ? null : (AST)_t;
			pr_AltstepLocalDef(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
						lastScope.setEndOffset(((LocationAST)a).getEndOffset()); 
						scopeStack.peek().setScopeSymbol(tempSymbol);
						tempSymbol=null;
						scopeStack.peek().setStartOffset(((LocationAST)a).getEndOffset()+1); 
						((LocationAST)upper).setScope(scopeStack.peek());
					
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case WithStatement:
			{
				pr_WithStatement(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case SemiColon:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			_loop591:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==SemiColon)) {
					AST tmp216_AST_in = (AST)_t;
					match(_t,SemiColon);
					_t = _t.getNextSibling();
					if ( inputState.guessing==0 ) {
						
							    		lastScope = scopeStack.peek(); 
							    		pushNewScope(true); 
							    		tempSymbol=null;
							    		
							    	
					}
					b = _t==ASTNULL ? null : (AST)_t;
					pr_AltstepLocalDef(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						
							    		lastScope.setEndOffset(((LocationAST)a).getEndOffset()); 
							    		scopeStack.peek().setScopeSymbol(tempSymbol);
							    		tempSymbol=null;
							    		scopeStack.peek().setStartOffset(((LocationAST)a).getEndOffset()+1); 
							    		((LocationAST)upper).setScope(scopeStack.peek());
							    	
					}
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case WithStatement:
					{
						pr_WithStatement(_t);
						_t = _retTree;
						break;
					}
					case 3:
					case SemiColon:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop591;
				}
				
			} while (true);
			}
			_t = __t587;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AltGuardList(AST _t) throws RecognitionException {
		
		AST pr_AltGuardList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1355 = _t;
			AST tmp217_AST_in = (AST)_t;
			match(_t,AltGuardList);
			_t = _t.getFirstChild();
			{
			_loop1359:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ElseStatement||_t.getType()==GuardStatement)) {
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case GuardStatement:
					{
						pr_GuardStatement(_t);
						_t = _retTree;
						break;
					}
					case ElseStatement:
					{
						pr_ElseStatement(_t);
						_t = _retTree;
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp218_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case ElseStatement:
					case GuardStatement:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop1359;
				}
				
			} while (true);
			}
			_t = __t1355;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AltstepLocalDef(AST _t) throws RecognitionException {
		
		AST pr_AltstepLocalDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t593 = _t;
			AST tmp219_AST_in = (AST)_t;
			match(_t,AltstepLocalDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case VarInstance:
			{
				pr_VarInstance(_t);
				_t = _retTree;
				break;
			}
			case TimerInstance:
			{
				pr_TimerInstance(_t);
				_t = _retTree;
				break;
			}
			case ConstDef:
			{
				pr_ConstDef(_t);
				_t = _retTree;
				break;
			}
			case TemplateDef:
			{
				pr_TemplateDef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t593;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AltstepInstance(AST _t) throws RecognitionException {
		
		AST pr_AltstepInstance_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t596 = _t;
			AST tmp220_AST_in = (AST)_t;
			match(_t,AltstepInstance);
			_t = _t.getFirstChild();
			pr_AltstepRef(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FunctionActualParList:
			{
				pr_FunctionActualParList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t596;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AltstepRef(AST _t) throws RecognitionException {
		
		AST pr_AltstepRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GlobalModuleId:
			{
				a=pr_GlobalModuleId(_t);
				_t = _retTree;
				break;
			}
			case Identifier:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			b=pr_Identifier(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final Symbol  pr_ImportFromSpec(AST _t) throws RecognitionException {
		Symbol s=null;
		
		AST pr_ImportFromSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t671 = _t;
			AST tmp221_AST_in = (AST)_t;
			match(_t,ImportFromSpec);
			_t = _t.getFirstChild();
			s=pr_ModuleId(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case RecursiveKeyword:
			{
				AST tmp222_AST_in = (AST)_t;
				match(_t,RecursiveKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t671;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final AbstractImport  pr_AllWithExcepts(AST _t) throws RecognitionException {
		AbstractImport imp;
		
		AST pr_AllWithExcepts_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		imp = new AllExceptImport(); List<KindAllExceptImport> klist;
		
		try {      // for error handling
			AST __t607 = _t;
			AST tmp223_AST_in = (AST)_t;
			match(_t,AllWithExcepts);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ExceptsDef:
			{
				klist=pr_ExceptsDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					
										((AllExceptImport)imp).setExceptionList(klist);
									
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t607;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return imp;
	}
	
	public final KindImport  pr_ImportSpec(AST _t) throws RecognitionException {
		KindImport kindImport=new KindImport();;
		
		AST pr_ImportSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AbstractKindImport kindNames;
		
		try {      // for error handling
			AST __t660 = _t;
			AST tmp224_AST_in = (AST)_t;
			match(_t,ImportSpec);
			_t = _t.getFirstChild();
			{
			{
			_loop664:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ImportElement)) {
					kindNames=pr_ImportElement(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						kindImport.addKind(kindNames);
					}
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp225_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case ImportElement:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop664;
				}
				
			} while (true);
			}
			}
			_t = __t660;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return kindImport;
	}
	
	public final List<KindAllExceptImport>  pr_ExceptsDef(AST _t) throws RecognitionException {
		List<KindAllExceptImport> klist=null;
		
		AST pr_ExceptsDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t611 = _t;
			AST tmp226_AST_in = (AST)_t;
			match(_t,ExceptsDef);
			_t = _t.getFirstChild();
			klist=pr_ExceptSpec(_t);
			_t = _retTree;
			_t = __t611;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return klist;
	}
	
	public final List<KindAllExceptImport>  pr_ExceptSpec(AST _t) throws RecognitionException {
		List<KindAllExceptImport> klist=new ArrayList<KindAllExceptImport>();;
		
		AST pr_ExceptSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		KindAllExceptImport k;
		
		try {      // for error handling
			AST __t613 = _t;
			AST tmp227_AST_in = (AST)_t;
			match(_t,ExceptSpec);
			_t = _t.getFirstChild();
			{
			_loop616:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ExceptElement)) {
					k=pr_ExceptElement(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						klist.add(k);
					}
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp228_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case ExceptElement:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop616;
				}
				
			} while (true);
			}
			_t = __t613;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return klist;
	}
	
	public final KindAllExceptImport  pr_ExceptElement(AST _t) throws RecognitionException {
		KindAllExceptImport k=null;
		
		AST pr_ExceptElement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t618 = _t;
			AST tmp229_AST_in = (AST)_t;
			match(_t,ExceptElement);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ExceptGroupSpec:
			{
				k=pr_ExceptGroupSpec(_t);
				_t = _retTree;
				break;
			}
			case ExceptTypeDefSpec:
			{
				k=pr_ExceptTypeDefSpec(_t);
				_t = _retTree;
				break;
			}
			case ExceptTemplateSpec:
			{
				k=pr_ExceptTemplateSpec(_t);
				_t = _retTree;
				break;
			}
			case ExceptConstSpec:
			{
				k=pr_ExceptConstSpec(_t);
				_t = _retTree;
				break;
			}
			case ExceptTestcaseSpec:
			{
				k=pr_ExceptTestcaseSpec(_t);
				_t = _retTree;
				break;
			}
			case ExceptAltstepSpec:
			{
				k=pr_ExceptAltstepSpec(_t);
				_t = _retTree;
				break;
			}
			case ExceptFunctionSpec:
			{
				k=pr_ExceptFunctionSpec(_t);
				_t = _retTree;
				break;
			}
			case ExceptSignatureSpec:
			{
				k=pr_ExceptSignatureSpec(_t);
				_t = _retTree;
				break;
			}
			case ExceptModuleParSpec:
			{
				k=pr_ExceptModuleParSpec(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t618;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return k;
	}
	
	public final KindAllExceptImport  pr_ExceptGroupSpec(AST _t) throws RecognitionException {
		KindAllExceptImport k = new KindAllExceptImport();;
		
		AST pr_ExceptGroupSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t621 = _t;
			AST tmp230_AST_in = (AST)_t;
			match(_t,ExceptGroupSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							k.setType(AbstractKindImport.KindEnum.ttcn3_group);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ExceptGroupRefList:
			{
				reflist=pr_ExceptGroupRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);
				}
				break;
			}
			case AllKeyword:
			{
				AST tmp231_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					k.setMode(KindAllExceptImport.ModesEnum.all);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t621;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return k;
	}
	
	public final KindAllExceptImport  pr_ExceptTypeDefSpec(AST _t) throws RecognitionException {
		KindAllExceptImport k = new KindAllExceptImport();;
		
		AST pr_ExceptTypeDefSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t628 = _t;
			AST tmp232_AST_in = (AST)_t;
			match(_t,ExceptTypeDefSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							k.setType(AbstractKindImport.KindEnum.ttcn3_type);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TypeRefList:
			{
				reflist=pr_TypeRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);
				}
				break;
			}
			case AllKeyword:
			{
				AST tmp233_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					k.setMode(KindAllExceptImport.ModesEnum.all);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t628;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return k;
	}
	
	public final KindAllExceptImport  pr_ExceptTemplateSpec(AST _t) throws RecognitionException {
		KindAllExceptImport k = new KindAllExceptImport();;
		
		AST pr_ExceptTemplateSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t635 = _t;
			AST tmp234_AST_in = (AST)_t;
			match(_t,ExceptTemplateSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							k.setType(AbstractKindImport.KindEnum.ttcn3_template);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateRefList:
			{
				reflist=pr_TemplateRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);
				}
				break;
			}
			case AllKeyword:
			{
				AST tmp235_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					k.setMode(KindAllExceptImport.ModesEnum.all);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t635;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return k;
	}
	
	public final KindAllExceptImport  pr_ExceptConstSpec(AST _t) throws RecognitionException {
		KindAllExceptImport k = new KindAllExceptImport();;
		
		AST pr_ExceptConstSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t642 = _t;
			AST tmp236_AST_in = (AST)_t;
			match(_t,ExceptConstSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							k.setType(AbstractKindImport.KindEnum.ttcn3_const);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ConstRefList:
			{
				reflist=pr_ConstRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);
				}
				break;
			}
			case AllKeyword:
			{
				AST tmp237_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					k.setMode(KindAllExceptImport.ModesEnum.all);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t642;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return k;
	}
	
	public final KindAllExceptImport  pr_ExceptTestcaseSpec(AST _t) throws RecognitionException {
		KindAllExceptImport k = new KindAllExceptImport();;
		
		AST pr_ExceptTestcaseSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t645 = _t;
			AST tmp238_AST_in = (AST)_t;
			match(_t,ExceptTestcaseSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							k.setType(AbstractKindImport.KindEnum.ttcn3_testcase);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TestcaseRefList:
			{
				reflist=pr_TestcaseRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);
				}
				break;
			}
			case AllKeyword:
			{
				AST tmp239_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					k.setMode(KindAllExceptImport.ModesEnum.all);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t645;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return k;
	}
	
	public final KindAllExceptImport  pr_ExceptAltstepSpec(AST _t) throws RecognitionException {
		KindAllExceptImport k = new KindAllExceptImport();;
		
		AST pr_ExceptAltstepSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t648 = _t;
			AST tmp240_AST_in = (AST)_t;
			match(_t,ExceptAltstepSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							k.setType(AbstractKindImport.KindEnum.ttcn3_altstep);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AltstepRefList:
			{
				reflist=pr_AltstepRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);
				}
				break;
			}
			case AllKeyword:
			{
				AST tmp241_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					k.setMode(KindAllExceptImport.ModesEnum.all);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t648;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return k;
	}
	
	public final KindAllExceptImport  pr_ExceptFunctionSpec(AST _t) throws RecognitionException {
		KindAllExceptImport k = new KindAllExceptImport();;
		
		AST pr_ExceptFunctionSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t651 = _t;
			AST tmp242_AST_in = (AST)_t;
			match(_t,ExceptFunctionSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							k.setType(AbstractKindImport.KindEnum.ttcn3_function);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FunctionRefList:
			{
				reflist=pr_FunctionRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);
				}
				break;
			}
			case AllKeyword:
			{
				AST tmp243_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					k.setMode(KindAllExceptImport.ModesEnum.all);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t651;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return k;
	}
	
	public final KindAllExceptImport  pr_ExceptSignatureSpec(AST _t) throws RecognitionException {
		KindAllExceptImport k = new KindAllExceptImport();;
		
		AST pr_ExceptSignatureSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t654 = _t;
			AST tmp244_AST_in = (AST)_t;
			match(_t,ExceptSignatureSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							k.setType(AbstractKindImport.KindEnum.ttcn3_signature);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SignatureRefList:
			{
				reflist=pr_SignatureRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);
				}
				break;
			}
			case AllKeyword:
			{
				AST tmp245_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					k.setMode(KindAllExceptImport.ModesEnum.all);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t654;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return k;
	}
	
	public final KindAllExceptImport  pr_ExceptModuleParSpec(AST _t) throws RecognitionException {
		KindAllExceptImport k = new KindAllExceptImport();;
		
		AST pr_ExceptModuleParSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t657 = _t;
			AST tmp246_AST_in = (AST)_t;
			match(_t,ExceptModuleParSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							k.setType(AbstractKindImport.KindEnum.ttcn3_modulepar);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ModuleParRefList:
			{
				reflist=pr_ModuleParRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					k.setNames(reflist); k.setMode(KindAllExceptImport.ModesEnum.except);
				}
				break;
			}
			case AllKeyword:
			{
				AST tmp247_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				if ( inputState.guessing==0 ) {
					k.setMode(KindAllExceptImport.ModesEnum.all);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t657;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return k;
	}
	
	public final List<String>  pr_ExceptGroupRefList(AST _t) throws RecognitionException {
		List<String> s=new ArrayList<String>();;
		
		AST pr_ExceptGroupRefList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t624 = _t;
			AST tmp248_AST_in = (AST)_t;
			match(_t,ExceptGroupRefList);
			_t = _t.getFirstChild();
			a=pr_FullGroupIdentifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				s.add(a.getName());
			}
			{
			_loop626:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FullGroupIdentifier)) {
					b=pr_FullGroupIdentifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						s.add(b.getName());
					}
				}
				else {
					break _loop626;
				}
				
			} while (true);
			}
			_t = __t624;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final Symbol  pr_FullGroupIdentifier(AST _t) throws RecognitionException {
		Symbol s=new Symbol();;
		
		AST pr_FullGroupIdentifier_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t721 = _t;
			AST tmp249_AST_in = (AST)_t;
			match(_t,FullGroupIdentifier);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							s.setGroupName(a.getName());
						
			}
			{
			_loop723:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Dot)) {
					AST tmp250_AST_in = (AST)_t;
					match(_t,Dot);
					_t = _t.getNextSibling();
					a=pr_Identifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						
										String temp = s.getGroupName() + "." + a.getName();
										s.setGroupName(temp);
									
					}
				}
				else {
					break _loop723;
				}
				
			} while (true);
			}
			_t = __t721;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final List<String>  pr_TypeRefList(AST _t) throws RecognitionException {
		List<String> s=new ArrayList<String>();;
		
		AST pr_TypeRefList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t631 = _t;
			AST tmp251_AST_in = (AST)_t;
			match(_t,TypeRefList);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				s.add(a.getName());
			}
			{
			_loop633:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Identifier)) {
					b=pr_Identifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						s.add(b.getName());
					}
				}
				else {
					break _loop633;
				}
				
			} while (true);
			}
			_t = __t631;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final List<String>  pr_TemplateRefList(AST _t) throws RecognitionException {
		List<String> s=new ArrayList<String>();;
		
		AST pr_TemplateRefList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t638 = _t;
			AST tmp252_AST_in = (AST)_t;
			match(_t,TemplateRefList);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				s.add(a.getName());
			}
			{
			_loop640:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Identifier)) {
					b=pr_Identifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						s.add(b.getName());
					}
				}
				else {
					break _loop640;
				}
				
			} while (true);
			}
			_t = __t638;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final List<String>  pr_ConstRefList(AST _t) throws RecognitionException {
		List<String> s=new ArrayList<String>();;
		
		AST pr_ConstRefList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t746 = _t;
			AST tmp253_AST_in = (AST)_t;
			match(_t,ConstRefList);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				s.add(a.getName());
			}
			{
			_loop748:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Identifier)) {
					b=pr_Identifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						s.add(b.getName());
					}
				}
				else {
					break _loop748;
				}
				
			} while (true);
			}
			_t = __t746;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final List<String>  pr_TestcaseRefList(AST _t) throws RecognitionException {
		List<String> s = new ArrayList<String>();;
		
		AST pr_TestcaseRefList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t763 = _t;
			AST tmp254_AST_in = (AST)_t;
			match(_t,TestcaseRefList);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				s.add(a.getName());
			}
			{
			_loop765:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Identifier)) {
					b=pr_Identifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						s.add(b.getName());
					}
				}
				else {
					break _loop765;
				}
				
			} while (true);
			}
			_t = __t763;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final List<String>  pr_AltstepRefList(AST _t) throws RecognitionException {
		List<String> s = new ArrayList<String>();;
		
		AST pr_AltstepRefList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t753 = _t;
			AST tmp255_AST_in = (AST)_t;
			match(_t,AltstepRefList);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				s.add(a.getName());
			}
			{
			_loop755:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Identifier)) {
					b=pr_Identifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						s.add(b.getName());
					}
				}
				else {
					break _loop755;
				}
				
			} while (true);
			}
			_t = __t753;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final List<String>  pr_FunctionRefList(AST _t) throws RecognitionException {
		List<String> s=new ArrayList<String>();;
		
		AST pr_FunctionRefList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t773 = _t;
			AST tmp256_AST_in = (AST)_t;
			match(_t,FunctionRefList);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				s.add(a.getName());
			}
			{
			_loop775:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Identifier)) {
					b=pr_Identifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						s.add(b.getName());
					}
				}
				else {
					break _loop775;
				}
				
			} while (true);
			}
			_t = __t773;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final List<String>  pr_SignatureRefList(AST _t) throws RecognitionException {
		List<String> s=new ArrayList<String>();;
		
		AST pr_SignatureRefList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t783 = _t;
			AST tmp257_AST_in = (AST)_t;
			match(_t,SignatureRefList);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				s.add(a.getName());
			}
			{
			_loop785:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Identifier)) {
					b=pr_Identifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						s.add(b.getName());
					}
				}
				else {
					break _loop785;
				}
				
			} while (true);
			}
			_t = __t783;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final List<String>  pr_ModuleParRefList(AST _t) throws RecognitionException {
		List<String> s=new ArrayList<String>();;
		
		AST pr_ModuleParRefList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t699 = _t;
			AST tmp258_AST_in = (AST)_t;
			match(_t,ModuleParRefList);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				s.add(a.getName());
			}
			{
			_loop701:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==Identifier)) {
					b=pr_Identifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						s.add(b.getName());
					}
				}
				else {
					break _loop701;
				}
				
			} while (true);
			}
			_t = __t699;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final AbstractKindImport  pr_ImportElement(AST _t) throws RecognitionException {
		AbstractKindImport kindNames=null;
		
		AST pr_ImportElement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t666 = _t;
			AST tmp259_AST_in = (AST)_t;
			match(_t,ImportElement);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ImportGroupSpec:
			{
				kindNames=pr_ImportGroupSpec(_t);
				_t = _retTree;
				break;
			}
			case ImportTypeDefSpec:
			{
				kindNames=pr_ImportTypeDefSpec(_t);
				_t = _retTree;
				break;
			}
			case ImportTemplateSpec:
			{
				kindNames=pr_ImportTemplateSpec(_t);
				_t = _retTree;
				break;
			}
			case ImportConstSpec:
			{
				kindNames=pr_ImportConstSpec(_t);
				_t = _retTree;
				break;
			}
			case ImportTestcaseSpec:
			{
				kindNames=pr_ImportTestcaseSpec(_t);
				_t = _retTree;
				break;
			}
			case ImportAltstepSpec:
			{
				kindNames=pr_ImportAltstepSpec(_t);
				_t = _retTree;
				break;
			}
			case ImportFunctionSpec:
			{
				kindNames=pr_ImportFunctionSpec(_t);
				_t = _retTree;
				break;
			}
			case ImportSignatureSpec:
			{
				kindNames=pr_ImportSignatureSpec(_t);
				_t = _retTree;
				break;
			}
			case ImportModuleParSpec:
			{
				kindNames=pr_ImportModuleParSpec(_t);
				_t = _retTree;
				break;
			}
			case ImportImportSpec:
			{
				kindNames=pr_ImportImportSpec(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t666;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return kindNames;
	}
	
	public final AbstractKindImport  pr_ImportGroupSpec(AST _t) throws RecognitionException {
		AbstractKindImport kindImport=null;
		
		AST pr_ImportGroupSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist; GroupImport imp = new GroupImport();
		
		try {      // for error handling
			AST __t703 = _t;
			AST tmp260_AST_in = (AST)_t;
			match(_t,ImportGroupSpec);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GroupRefListWithExcept:
			{
				imp=pr_GroupRefListWithExcept(_t);
				_t = _retTree;
				break;
			}
			case AllGroupsWithExcept:
			{
				reflist=pr_AllGroupsWithExcept(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					imp.setExceptionList(reflist); imp.setMode(GroupImport.ModeEnum.allGroupWithExcept);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t703;
			_t = _t.getNextSibling();
			if ( inputState.guessing==0 ) {
				
						imp.setType(AbstractKindImport.KindEnum.ttcn3_group);
						kindImport = imp;
					
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return kindImport;
	}
	
	public final KindNames  pr_ImportTypeDefSpec(AST _t) throws RecognitionException {
		KindNames kindNames=new KindNames();;
		
		AST pr_ImportTypeDefSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t725 = _t;
			AST tmp261_AST_in = (AST)_t;
			match(_t,ImportTypeDefSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
						kindNames.setType(KindNames.KindEnum.ttcn3_type);
					
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TypeRefList:
			{
				reflist=pr_TypeRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept);
				}
				break;
			}
			case AllTypesWithExcept:
			{
				reflist=pr_AllTypesWithExcept(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t725;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return kindNames;
	}
	
	public final KindNames  pr_ImportTemplateSpec(AST _t) throws RecognitionException {
		KindNames kindNames=new KindNames();;
		
		AST pr_ImportTemplateSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t732 = _t;
			AST tmp262_AST_in = (AST)_t;
			match(_t,ImportTemplateSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							kindNames.setType(KindNames.KindEnum.ttcn3_template);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateRefList:
			{
				reflist=pr_TemplateRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept);
				}
				break;
			}
			case AllTemplsWithExcept:
			{
				reflist=pr_AllTemplsWithExcept(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t732;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return kindNames;
	}
	
	public final KindNames  pr_ImportConstSpec(AST _t) throws RecognitionException {
		KindNames kindNames=new KindNames();;
		
		AST pr_ImportConstSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t738 = _t;
			AST tmp263_AST_in = (AST)_t;
			match(_t,ImportConstSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							kindNames.setType(KindNames.KindEnum.ttcn3_const);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ConstRefList:
			{
				reflist=pr_ConstRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept);
				}
				break;
			}
			case AllConstsWithExcept:
			{
				reflist=pr_AllConstsWithExcept(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t738;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return kindNames;
	}
	
	public final KindNames  pr_ImportTestcaseSpec(AST _t) throws RecognitionException {
		KindNames kindNames=new KindNames();;
		
		AST pr_ImportTestcaseSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t760 = _t;
			AST tmp264_AST_in = (AST)_t;
			match(_t,ImportTestcaseSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							kindNames.setType(KindNames.KindEnum.ttcn3_testcase);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TestcaseRefList:
			{
				reflist=pr_TestcaseRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept);
				}
				break;
			}
			case AllTestcasesWithExcept:
			{
				reflist=pr_AllTestcasesWithExcept(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t760;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return kindNames;
	}
	
	public final KindNames  pr_ImportAltstepSpec(AST _t) throws RecognitionException {
		KindNames kindNames=new KindNames();;
		
		AST pr_ImportAltstepSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t750 = _t;
			AST tmp265_AST_in = (AST)_t;
			match(_t,ImportAltstepSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							kindNames.setType(KindNames.KindEnum.ttcn3_altstep);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AltstepRefList:
			{
				reflist=pr_AltstepRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept);
				}
				break;
			}
			case AllAltstepsWithExcept:
			{
				reflist=pr_AllAltstepsWithExcept(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t750;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return kindNames;
	}
	
	public final KindNames  pr_ImportFunctionSpec(AST _t) throws RecognitionException {
		KindNames kindNames=new KindNames();;
		
		AST pr_ImportFunctionSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t770 = _t;
			AST tmp266_AST_in = (AST)_t;
			match(_t,ImportFunctionSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							kindNames.setType(KindNames.KindEnum.ttcn3_function);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FunctionRefList:
			{
				reflist=pr_FunctionRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept);
				}
				break;
			}
			case AllFunctionsWithExcept:
			{
				reflist=pr_AllFunctionsWithExcept(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t770;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return kindNames;
	}
	
	public final KindNames  pr_ImportSignatureSpec(AST _t) throws RecognitionException {
		KindNames kindNames=new KindNames();;
		
		AST pr_ImportSignatureSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t780 = _t;
			AST tmp267_AST_in = (AST)_t;
			match(_t,ImportSignatureSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							kindNames.setType(KindNames.KindEnum.ttcn3_signature);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SignatureRefList:
			{
				reflist=pr_SignatureRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept);
				}
				break;
			}
			case AllSignaturesWithExcept:
			{
				reflist=pr_AllSignaturesWithExcept(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t780;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return kindNames;
	}
	
	public final KindNames  pr_ImportModuleParSpec(AST _t) throws RecognitionException {
		KindNames kindNames=new KindNames();;
		
		AST pr_ImportModuleParSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> reflist;
		
		try {      // for error handling
			AST __t693 = _t;
			AST tmp268_AST_in = (AST)_t;
			match(_t,ImportModuleParSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							kindNames.setType(KindNames.KindEnum.ttcn3_modulepar);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ModuleParRefList:
			{
				reflist=pr_ModuleParRefList(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.accept);
				}
				break;
			}
			case AllModuleParWithExcept:
			{
				reflist=pr_AllModuleParWithExcept(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					kindNames.setNames(reflist); kindNames.setMode(KindNames.ModeEnum.allexcept);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t693;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return kindNames;
	}
	
	public final KindNames  pr_ImportImportSpec(AST _t) throws RecognitionException {
		KindNames kindNames=new KindNames();;
		
		AST pr_ImportImportSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t669 = _t;
			AST tmp269_AST_in = (AST)_t;
			match(_t,ImportImportSpec);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				
							kindNames.setType(KindNames.KindEnum.ttcn3_import);
							kindNames.setMode(KindNames.ModeEnum.allexcept);
						
			}
			_t = __t669;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return kindNames;
	}
	
	public final String  pr_FreeText(AST _t) throws RecognitionException {
		String s=null;
		
		AST pr_FreeText_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			a = (AST)_t;
			match(_t,CSTRING);
			_t = _t.getNextSibling();
			if ( inputState.guessing==0 ) {
				s = a.getText();
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final void pr_ObjectIdentifierValue(AST _t) throws RecognitionException {
		
		AST pr_ObjectIdentifierValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t682 = _t;
			AST tmp270_AST_in = (AST)_t;
			match(_t,OBJID);
			_t = _t.getFirstChild();
			{
			int _cnt685=0;
			_loop685:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_tokenSet_2.member(_t.getType()))) {
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case ReferencedValue:
					{
						pr_ReferencedValue(_t);
						_t = _retTree;
						break;
					}
					case NumberForm:
					{
						pr_NumberForm(_t);
						_t = _retTree;
						break;
					}
					case NameAndNumberForm:
					{
						pr_NameAndNumberForm(_t);
						_t = _retTree;
						break;
					}
					case NameForm:
					{
						pr_NameForm(_t);
						_t = _retTree;
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					if ( _cnt685>=1 ) { break _loop685; } else {throw new NoViableAltException(_t);}
				}
				
				_cnt685++;
			} while (true);
			}
			_t = __t682;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_NumberForm(AST _t) throws RecognitionException {
		
		AST pr_NumberForm_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t687 = _t;
			AST tmp271_AST_in = (AST)_t;
			match(_t,NumberForm);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,NUMBER);
			_t = _t.getNextSibling();
			_t = __t687;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_NameAndNumberForm(AST _t) throws RecognitionException {
		
		AST pr_NameAndNumberForm_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t689 = _t;
			AST tmp272_AST_in = (AST)_t;
			match(_t,NameAndNumberForm);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,IDENTIFIER);
			_t = _t.getNextSibling();
			pr_NumberForm(_t);
			_t = _retTree;
			_t = __t689;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final List<String>  pr_AllModuleParWithExcept(AST _t) throws RecognitionException {
		List<String> reflist=null;;
		
		AST pr_AllModuleParWithExcept_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t696 = _t;
			AST tmp273_AST_in = (AST)_t;
			match(_t,AllModuleParWithExcept);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ModuleParRefList:
			{
				reflist=pr_ModuleParRefList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t696;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return reflist;
	}
	
	public final GroupImport  pr_GroupRefListWithExcept(AST _t) throws RecognitionException {
		GroupImport imp = new GroupImport();;
		
		AST pr_GroupRefListWithExcept_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		GroupExceptImport a,b;
		
		try {      // for error handling
			AST __t706 = _t;
			AST tmp274_AST_in = (AST)_t;
			match(_t,GroupRefListWithExcept);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
				imp.setMode(GroupImport.ModeEnum.groupRefListWithExcept);
			}
			a=pr_FullGroupIdentifierWithExcept(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				imp.addToRefList(a);
			}
			{
			{
			_loop709:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FullGroupIdentifierWithExcept)) {
					b=pr_FullGroupIdentifierWithExcept(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						imp.addToRefList(b);
					}
				}
				else {
					break _loop709;
				}
				
			} while (true);
			}
			}
			_t = __t706;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return imp;
	}
	
	public final List<String>  pr_AllGroupsWithExcept(AST _t) throws RecognitionException {
		List<String> s=null;
		
		AST pr_AllGroupsWithExcept_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t711 = _t;
			AST tmp275_AST_in = (AST)_t;
			match(_t,AllGroupsWithExcept);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GroupRefList:
			{
				s=pr_GroupRefList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t711;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final GroupExceptImport  pr_FullGroupIdentifierWithExcept(AST _t) throws RecognitionException {
		GroupExceptImport g = new GroupExceptImport();;
		
		AST pr_FullGroupIdentifierWithExcept_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<KindAllExceptImport> klist; Symbol a;
		
		try {      // for error handling
			AST __t714 = _t;
			AST tmp276_AST_in = (AST)_t;
			match(_t,FullGroupIdentifierWithExcept);
			_t = _t.getFirstChild();
			a=pr_FullGroupIdentifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							g.setGroupName(a.getGroupName());
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ExceptsDef:
			{
				klist=pr_ExceptsDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					g.setExceptionList(klist);
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t714;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return g;
	}
	
	public final List<String>  pr_GroupRefList(AST _t) throws RecognitionException {
		List<String> s=new ArrayList<String>();;
		
		AST pr_GroupRefList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t717 = _t;
			AST tmp277_AST_in = (AST)_t;
			match(_t,GroupRefList);
			_t = _t.getFirstChild();
			a=pr_FullGroupIdentifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							s.add(a.getGroupName());
						
			}
			{
			_loop719:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FullGroupIdentifier)) {
					b=pr_FullGroupIdentifier(_t);
					_t = _retTree;
					if ( inputState.guessing==0 ) {
						s.add(b.getGroupName());
					}
				}
				else {
					break _loop719;
				}
				
			} while (true);
			}
			_t = __t717;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final List<String>  pr_AllTypesWithExcept(AST _t) throws RecognitionException {
		List<String> reflist=null;
		
		AST pr_AllTypesWithExcept_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t728 = _t;
			AST tmp278_AST_in = (AST)_t;
			match(_t,AllTypesWithExcept);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TypeRefList:
			{
				reflist=pr_TypeRefList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t728;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return reflist;
	}
	
	public final void pr_TypeDefIdentifier(AST _t) throws RecognitionException {
		
		AST pr_TypeDefIdentifier_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			a=pr_Identifier(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final List<String>  pr_AllTemplsWithExcept(AST _t) throws RecognitionException {
		List<String> reflist=null;
		
		AST pr_AllTemplsWithExcept_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t735 = _t;
			AST tmp279_AST_in = (AST)_t;
			match(_t,AllTemplsWithExcept);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateRefList:
			{
				reflist=pr_TemplateRefList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t735;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return reflist;
	}
	
	public final List<String>  pr_AllConstsWithExcept(AST _t) throws RecognitionException {
		List<String> reflist=null;
		
		AST pr_AllConstsWithExcept_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t741 = _t;
			AST tmp280_AST_in = (AST)_t;
			match(_t,AllConstsWithExcept);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ConstRefList:
			{
				reflist=pr_ConstRefList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t741;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return reflist;
	}
	
	public final List<String>  pr_AllAltstepsWithExcept(AST _t) throws RecognitionException {
		List<String> reflist=null;
		
		AST pr_AllAltstepsWithExcept_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t757 = _t;
			AST tmp281_AST_in = (AST)_t;
			match(_t,AllAltstepsWithExcept);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AltstepRefList:
			{
				reflist=pr_AltstepRefList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t757;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return reflist;
	}
	
	public final List<String>  pr_AllTestcasesWithExcept(AST _t) throws RecognitionException {
		List<String> reflist=null;
		
		AST pr_AllTestcasesWithExcept_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t767 = _t;
			AST tmp282_AST_in = (AST)_t;
			match(_t,AllTestcasesWithExcept);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TestcaseRefList:
			{
				reflist=pr_TestcaseRefList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t767;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return reflist;
	}
	
	public final List<String>  pr_AllFunctionsWithExcept(AST _t) throws RecognitionException {
		List<String> reflist=null;
		
		AST pr_AllFunctionsWithExcept_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t777 = _t;
			AST tmp283_AST_in = (AST)_t;
			match(_t,AllFunctionsWithExcept);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FunctionRefList:
			{
				reflist=pr_FunctionRefList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t777;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return reflist;
	}
	
	public final List<String>  pr_AllSignaturesWithExcept(AST _t) throws RecognitionException {
		List<String> reflist=null;
		
		AST pr_AllSignaturesWithExcept_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t787 = _t;
			AST tmp284_AST_in = (AST)_t;
			match(_t,AllSignaturesWithExcept);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SignatureRefList:
			{
				reflist=pr_SignatureRefList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t787;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return reflist;
	}
	
	public final void pr_MultitypedModuleParList(AST _t) throws RecognitionException {
		
		AST pr_MultitypedModuleParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t806 = _t;
			AST tmp285_AST_in = (AST)_t;
			match(_t,MultitypedModuleParList);
			_t = _t.getFirstChild();
			{
			_loop809:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ModulePar)) {
					pr_ModulePar(_t);
					_t = _retTree;
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp286_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case ModulePar:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop809;
				}
				
			} while (true);
			}
			_t = __t806;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ModuleControlBody(AST _t) throws RecognitionException {
		
		AST pr_ModuleControlBody_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST n = null;
		
		try {      // for error handling
			AST __t815 = _t;
			n = _t==ASTNULL ? null :(AST)_t;
			match(_t,ModuleControlBody);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
					((LocationAST)n).setScope(((LocationAST)n).getParent().getScope()); 
						
			}
			{
			_loop818:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ControlStatementOrDef)) {
					pr_ControlStatementOrDef(_t);
					_t = _retTree;
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp287_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case ControlStatementOrDef:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop818;
				}
				
			} while (true);
			}
			_t = __t815;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ControlStatementOrDef(AST _t) throws RecognitionException {
		
		AST pr_ControlStatementOrDef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST upper = null;
		AST a = null;
		AST b = null;
		Scope lastScope=null;
		
		try {      // for error handling
			AST __t820 = _t;
			upper = _t==ASTNULL ? null :(AST)_t;
			match(_t,ControlStatementOrDef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FunctionLocalInst:
			{
				{
				if ( inputState.guessing==0 ) {
					
						    			lastScope = scopeStack.peek(); 
							    		pushNewScope(true); 
							    		tempSymbol = null;
						    		
				}
				a = _t==ASTNULL ? null : (AST)_t;
				pr_FunctionLocalInst(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					
							    		lastScope.setEndOffset(((LocationAST)a).getEndOffset()); 
							    		scopeStack.peek().setStartOffset(((LocationAST)a).getEndOffset()+1); 
							    		scopeStack.peek().setScopeSymbol(tempSymbol);
							    		((LocationAST)upper).setScope(scopeStack.peek());
							    		tempSymbol=null;
							    	
				}
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case WithStatement:
				{
					pr_WithStatement(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case FunctionLocalDef:
			{
				{
				if ( inputState.guessing==0 ) {
					
										lastScope = scopeStack.peek(); 
										pushNewScope(true); 
							    		tempSymbol = null;
									
				}
				b = _t==ASTNULL ? null : (AST)_t;
				pr_FunctionLocalDef(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					
										lastScope.setEndOffset(((LocationAST)b).getEndOffset()); 
										scopeStack.peek().setStartOffset(((LocationAST)b).getEndOffset()+1); 
							    		scopeStack.peek().setScopeSymbol(tempSymbol);
							    		((LocationAST)upper).setScope(scopeStack.peek());
							    		tempSymbol=null;
									
				}
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case WithStatement:
				{
					pr_WithStatement(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case ControlStatement:
			{
				pr_ControlStatement(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t820;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ControlStatement(AST _t) throws RecognitionException {
		
		AST pr_ControlStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t827 = _t;
			AST tmp288_AST_in = (AST)_t;
			match(_t,ControlStatement);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TimerStatements:
			{
				pr_TimerStatements(_t);
				_t = _retTree;
				break;
			}
			case BasicStatements:
			{
				pr_BasicStatements(_t);
				_t = _retTree;
				break;
			}
			case BehaviourStatements:
			{
				pr_BehaviourStatements(_t);
				_t = _retTree;
				break;
			}
			case SUTStatements:
			{
				pr_SUTStatements(_t);
				_t = _retTree;
				break;
			}
			case STOP:
			{
				AST tmp289_AST_in = (AST)_t;
				match(_t,STOP);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t827;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_VarList(AST _t,
		TypeSymbol typeSymbol
	) throws RecognitionException {
		
		AST pr_VarList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t836 = _t;
			AST tmp290_AST_in = (AST)_t;
			match(_t,VarList);
			_t = _t.getFirstChild();
			{
			int _cnt838=0;
			_loop838:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==SingleVarInstance)) {
					pr_SingleVarInstance(_t,typeSymbol);
					_t = _retTree;
				}
				else {
					if ( _cnt838>=1 ) { break _loop838; } else {throw new NoViableAltException(_t);}
				}
				
				_cnt838++;
			} while (true);
			}
			_t = __t836;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TempVarList(AST _t,
		TypeSymbol typeSymbol
	) throws RecognitionException {
		
		AST pr_TempVarList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t840 = _t;
			AST tmp291_AST_in = (AST)_t;
			match(_t,TempVarList);
			_t = _t.getFirstChild();
			{
			pr_SingleTempVarInstance(_t,typeSymbol);
			_t = _retTree;
			{
			_loop843:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==SingleTempVarInstance)) {
					pr_SingleTempVarInstance(_t,typeSymbol);
					_t = _retTree;
				}
				else {
					break _loop843;
				}
				
			} while (true);
			}
			}
			_t = __t840;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleVarInstance(AST _t,
		TypeSymbol typeSymbol
	) throws RecognitionException {
		
		AST pr_SingleVarInstance_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Scope lastScope=null; Symbol a; VariableInstanceSymbol symbol = new VariableInstanceSymbol();
		
		try {      // for error handling
			AST __t850 = _t;
			AST tmp292_AST_in = (AST)_t;
			match(_t,SingleVarInstance);
			_t = _t.getFirstChild();
			if ( inputState.guessing==0 ) {
					
				//TODO: NOTE: This is a working approach that has to be applied to all other
				// local entities, this will however introduce many code duplicates, for 
				//reference of addditional lines see also pr_FunctionLocalDefOrInst rule 
				//				lastScope = scopeStack.peek(); 
				//		   		pushNewScope(true); 
						
			}
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName());
							symbol.setReferencedType(typeSymbol.getReferencedType());
							symbol.setPredefinedType(typeSymbol.getPredefinedType());
							symbol.setDeclarationNode(a.getDeclarationNode());
							addSymbolToScope(symbol);
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ArrayDef:
			{
				pr_ArrayDef(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case Expression:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Expression:
			{
				pr_Expression(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			if ( inputState.guessing==0 ) {
				
							tempSymbol = symbol;
						
			}
			if ( inputState.guessing==0 ) {
				
				
				//TODO: NOTE: This is a working approach that has to be applied to all other
				// local entities, this will however introduce many code duplicates, for 
				//reference of addditional lines see also pr_FunctionLocalDefOrInst rule 
				//				scopeStack.peek().setScopeSymbol(tempSymbol);
					    	
			}
			_t = __t850;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleTempVarInstance(AST _t,
		TypeSymbol typeSymbol
	) throws RecognitionException {
		
		AST pr_SingleTempVarInstance_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; TemplateInstanceSymbol symbol = new TemplateInstanceSymbol();
		
		try {      // for error handling
			AST __t845 = _t;
			AST tmp293_AST_in = (AST)_t;
			match(_t,SingleTempVarInstance);
			_t = _t.getFirstChild();
			{
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
								symbol.setName(a.getName());
								symbol.setDeclarationNode(a.getDeclarationNode());
								symbol.setReferencedType(typeSymbol.getReferencedType());
								symbol.setPredefinedType(typeSymbol.getPredefinedType());
								addSymbolToScope(symbol);
								tempSymbol = symbol;
							
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ArrayDef:
			{
				pr_ArrayDef(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case TemplateBody:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateBody:
			{
				pr_TemplateBody(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t845;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_VariableRef(AST _t) throws RecognitionException {
		
		AST pr_VariableRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; List<Symbol> b;
		
		try {      // for error handling
			AST __t854 = _t;
			AST tmp294_AST_in = (AST)_t;
			match(_t,VariableRef);
			_t = _t.getFirstChild();
			{
			a=pr_Identifier(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ExtendedFieldReference:
			{
				b=pr_ExtendedFieldReference(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t854;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleTimerInstance(AST _t) throws RecognitionException {
		
		AST pr_SingleTimerInstance_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a; TimerInstanceSymbol symbol = new TimerInstanceSymbol();
		
		try {      // for error handling
			AST __t864 = _t;
			AST tmp295_AST_in = (AST)_t;
			match(_t,SingleTimerInstance);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				
							symbol.setName(a.getName());
							symbol.setDeclarationNode(a.getDeclarationNode());
							addSymbolToScope(symbol); 
							tempSymbol = symbol;
						
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ArrayDef:
			{
				pr_ArrayDef(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case Expression:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Expression:
			{
				pr_TimerValue(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t864;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ConnectStatement(AST _t) throws RecognitionException {
		
		AST pr_ConnectStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t907 = _t;
			AST tmp296_AST_in = (AST)_t;
			match(_t,ConnectStatement);
			_t = _t.getFirstChild();
			pr_SingleConnectionSpec(_t);
			_t = _retTree;
			_t = __t907;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_MapStatement(AST _t) throws RecognitionException {
		
		AST pr_MapStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t939 = _t;
			AST tmp297_AST_in = (AST)_t;
			match(_t,MapStatement);
			_t = _t.getFirstChild();
			pr_SingleConnectionSpec(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ParamClause:
			{
				pr_ParamClause(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t939;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_DisconnectStatement(AST _t) throws RecognitionException {
		
		AST pr_DisconnectStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t925 = _t;
			AST tmp298_AST_in = (AST)_t;
			match(_t,DisconnectStatement);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SingleOrMultiConnectionSpec:
			{
				pr_SingleOrMultiConnectionSpec(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t925;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_UnmapStatement(AST _t) throws RecognitionException {
		
		AST pr_UnmapStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t944 = _t;
			AST tmp299_AST_in = (AST)_t;
			match(_t,UnmapStatement);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SingleOrMultiConnectionSpec:
			{
				pr_SingleOrMultiConnectionSpec(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t944;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_DoneStatement(AST _t) throws RecognitionException {
		
		AST pr_DoneStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t899 = _t;
			AST tmp300_AST_in = (AST)_t;
			match(_t,DoneStatement);
			_t = _t.getFirstChild();
			pr_ComponentId(_t);
			_t = _retTree;
			_t = __t899;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_KilledStatement(AST _t) throws RecognitionException {
		
		AST pr_KilledStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t883 = _t;
			AST tmp301_AST_in = (AST)_t;
			match(_t,KilledStatement);
			_t = _t.getFirstChild();
			pr_ComponentId(_t);
			_t = _retTree;
			_t = __t883;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_StartTCStatement(AST _t) throws RecognitionException {
		
		AST pr_StartTCStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t947 = _t;
			AST tmp302_AST_in = (AST)_t;
			match(_t,StartTCStatement);
			_t = _t.getFirstChild();
			pr_ComponentOrDefaultReference(_t);
			_t = _retTree;
			pr_FunctionInstance(_t);
			_t = _retTree;
			_t = __t947;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_StopTCStatement(AST _t) throws RecognitionException {
		
		AST pr_StopTCStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		String a;
		
		try {      // for error handling
			AST __t949 = _t;
			AST tmp303_AST_in = (AST)_t;
			match(_t,StopTCStatement);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ComponentReferenceOrLiteral:
			{
				{
				pr_ComponentReferenceOrLiteral(_t);
				_t = _retTree;
				AST tmp304_AST_in = (AST)_t;
				match(_t,STOP);
				_t = _t.getNextSibling();
				}
				break;
			}
			case AllKeyword:
			{
				{
				AST tmp305_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				AST tmp306_AST_in = (AST)_t;
				match(_t,COMPONENT);
				_t = _t.getNextSibling();
				AST tmp307_AST_in = (AST)_t;
				match(_t,STOP);
				_t = _t.getNextSibling();
				}
				break;
			}
			case TESTCASE:
			{
				{
				AST tmp308_AST_in = (AST)_t;
				match(_t,TESTCASE);
				_t = _t.getNextSibling();
				AST tmp309_AST_in = (AST)_t;
				match(_t,STOP);
				_t = _t.getNextSibling();
				{
				_loop955:
				do {
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case CSTRING:
					{
						a=pr_FreeText(_t);
						_t = _retTree;
						break;
					}
					case TemplateInstance:
					{
						pr_TemplateInstance(_t);
						_t = _retTree;
						break;
					}
					default:
					{
						break _loop955;
					}
					}
				} while (true);
				}
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t949;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_KillTCStatement(AST _t) throws RecognitionException {
		
		AST pr_KillTCStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t880 = _t;
			AST tmp310_AST_in = (AST)_t;
			match(_t,KillTCStatement);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case KillKeyword:
			{
				AST tmp311_AST_in = (AST)_t;
				match(_t,KillKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case ComponentReferenceOrLiteral:
			{
				pr_ComponentReferenceOrLiteral(_t);
				_t = _retTree;
				break;
			}
			case AllKeyword:
			{
				AST tmp312_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t880;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ComponentReferenceOrLiteral(AST _t) throws RecognitionException {
		
		AST pr_ComponentReferenceOrLiteral_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t957 = _t;
			AST tmp313_AST_in = (AST)_t;
			match(_t,ComponentReferenceOrLiteral);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FunctionInstance:
			case VariableRef:
			{
				pr_ComponentOrDefaultReference(_t);
				_t = _retTree;
				break;
			}
			case MTCOp:
			{
				pr_MTCOp(_t);
				_t = _retTree;
				break;
			}
			case SelfOp:
			{
				pr_SelfOp(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t957;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ComponentId(AST _t) throws RecognitionException {
		
		AST pr_ComponentId_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t901 = _t;
			AST tmp314_AST_in = (AST)_t;
			match(_t,ComponentId);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FunctionInstance:
			case VariableRef:
			{
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case VariableRef:
				{
					pr_VariableRef(_t);
					_t = _retTree;
					break;
				}
				case FunctionInstance:
				{
					pr_FunctionInstance(_t);
					_t = _retTree;
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			case AnyKeyword:
			{
				AST tmp315_AST_in = (AST)_t;
				match(_t,AnyKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case AllKeyword:
			{
				AST tmp316_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t901;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ConfigurationOps(AST _t) throws RecognitionException {
		
		AST pr_ConfigurationOps_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t885 = _t;
			AST tmp317_AST_in = (AST)_t;
			match(_t,ConfigurationOps);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case CreateOp:
			{
				pr_CreateOp(_t);
				_t = _retTree;
				break;
			}
			case SelfOp:
			{
				pr_SelfOp(_t);
				_t = _retTree;
				break;
			}
			case SystemOp:
			{
				pr_SystemOp(_t);
				_t = _retTree;
				break;
			}
			case MTCOp:
			{
				pr_MTCOp(_t);
				_t = _retTree;
				break;
			}
			case RunningOp:
			{
				pr_RunningOp(_t);
				_t = _retTree;
				break;
			}
			case AliveOp:
			{
				pr_AliveOp(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t885;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CreateOp(AST _t) throws RecognitionException {
		
		AST pr_CreateOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Reference r;
		
		try {      // for error handling
			AST __t888 = _t;
			AST tmp318_AST_in = (AST)_t;
			match(_t,CreateOp);
			_t = _t.getFirstChild();
			r=pr_ComponentType(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Minus:
			case SingleExpression:
			{
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case SingleExpression:
				{
					pr_SingleExpression(_t);
					_t = _retTree;
					break;
				}
				case Minus:
				{
					AST tmp319_AST_in = (AST)_t;
					match(_t,Minus);
					_t = _t.getNextSibling();
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case SingleExpression:
				{
					pr_SingleExpression(_t);
					_t = _retTree;
					break;
				}
				case 3:
				case AliveKeyword:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			case 3:
			case AliveKeyword:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AliveKeyword:
			{
				AST tmp320_AST_in = (AST)_t;
				match(_t,AliveKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t888;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SelfOp(AST _t) throws RecognitionException {
		
		AST pr_SelfOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST tmp321_AST_in = (AST)_t;
			match(_t,SelfOp);
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SystemOp(AST _t) throws RecognitionException {
		
		AST pr_SystemOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST tmp322_AST_in = (AST)_t;
			match(_t,SystemOp);
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_MTCOp(AST _t) throws RecognitionException {
		
		AST pr_MTCOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST tmp323_AST_in = (AST)_t;
			match(_t,MTCOp);
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_RunningOp(AST _t) throws RecognitionException {
		
		AST pr_RunningOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t905 = _t;
			AST tmp324_AST_in = (AST)_t;
			match(_t,RunningOp);
			_t = _t.getFirstChild();
			pr_ComponentId(_t);
			_t = _retTree;
			_t = __t905;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AliveOp(AST _t) throws RecognitionException {
		
		AST pr_AliveOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t895 = _t;
			AST tmp325_AST_in = (AST)_t;
			match(_t,AliveOp);
			_t = _t.getFirstChild();
			pr_ComponentId(_t);
			_t = _retTree;
			_t = __t895;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleConnectionSpec(AST _t) throws RecognitionException {
		
		AST pr_SingleConnectionSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t909 = _t;
			AST tmp326_AST_in = (AST)_t;
			match(_t,SingleConnectionSpec);
			_t = _t.getFirstChild();
			{
			pr_PortRef(_t);
			_t = _retTree;
			pr_PortRef(_t);
			_t = _retTree;
			}
			_t = __t909;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortRef(AST _t) throws RecognitionException {
		
		AST pr_PortRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t914 = _t;
			AST tmp327_AST_in = (AST)_t;
			match(_t,PortRef);
			_t = _t.getFirstChild();
			pr_ComponentRef(_t);
			_t = _retTree;
			pr_Port(_t);
			_t = _retTree;
			_t = __t914;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortSpec(AST _t) throws RecognitionException {
		
		AST pr_PortSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t912 = _t;
			AST tmp328_AST_in = (AST)_t;
			match(_t,PortSpec);
			_t = _t.getFirstChild();
			pr_PortRef(_t);
			_t = _retTree;
			pr_PortRef(_t);
			_t = _retTree;
			_t = __t912;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ComponentOrDefaultReference(AST _t) throws RecognitionException {
		
		AST pr_ComponentOrDefaultReference_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case VariableRef:
			{
				pr_VariableRef(_t);
				_t = _retTree;
				break;
			}
			case FunctionInstance:
			{
				pr_FunctionInstance(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleOrMultiConnectionSpec(AST _t) throws RecognitionException {
		
		AST pr_SingleOrMultiConnectionSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t928 = _t;
			AST tmp329_AST_in = (AST)_t;
			match(_t,SingleOrMultiConnectionSpec);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SingleConnectionSpec:
			{
				{
				pr_SingleConnectionSpec(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case ParamClause:
				{
					pr_ParamClause(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case PortRef:
			{
				{
				pr_AllConnectionsSpec(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case ParamClause:
				{
					pr_ParamClause(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case ComponentRef:
			{
				pr_AllPortsSpec(_t);
				_t = _retTree;
				break;
			}
			case AllKeyword:
			{
				pr_AllCompsAllPortsSpec(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t928;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ParamClause(AST _t) throws RecognitionException {
		
		AST pr_ParamClause_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t942 = _t;
			AST tmp330_AST_in = (AST)_t;
			match(_t,ParamClause);
			_t = _t.getFirstChild();
			pr_FunctionActualParList(_t);
			_t = _retTree;
			_t = __t942;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AllConnectionsSpec(AST _t) throws RecognitionException {
		
		AST pr_AllConnectionsSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_PortRef(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AllPortsSpec(AST _t) throws RecognitionException {
		
		AST pr_AllPortsSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_ComponentRef(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AllCompsAllPortsSpec(AST _t) throws RecognitionException {
		
		AST pr_AllCompsAllPortsSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST tmp331_AST_in = (AST)_t;
			match(_t,AllKeyword);
			_t = _t.getNextSibling();
			AST tmp332_AST_in = (AST)_t;
			match(_t,COMPONENT);
			_t = _t.getNextSibling();
			AST tmp333_AST_in = (AST)_t;
			match(_t,AllKeyword);
			_t = _t.getNextSibling();
			AST tmp334_AST_in = (AST)_t;
			match(_t,PortKeyword);
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ComponentIdentifier(AST _t) throws RecognitionException {
		
		AST pr_ComponentIdentifier_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FunctionInstance:
			{
				pr_FunctionInstance(_t);
				_t = _retTree;
				break;
			}
			case VariableRef:
			{
				pr_VariableRef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SendStatement(AST _t) throws RecognitionException {
		
		AST pr_SendStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t969 = _t;
			AST tmp335_AST_in = (AST)_t;
			match(_t,SendStatement);
			_t = _t.getFirstChild();
			pr_Port(_t);
			_t = _retTree;
			pr_SendParameter(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ToClause:
			{
				pr_ToClause(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t969;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CallStatement(AST _t) throws RecognitionException {
		
		AST pr_CallStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t984 = _t;
			AST tmp336_AST_in = (AST)_t;
			match(_t,CallStatement);
			_t = _t.getFirstChild();
			pr_Port(_t);
			_t = _retTree;
			pr_CallParameters(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ToClause:
			{
				pr_ToClause(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case CallBodyStatement:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case CallBodyStatement:
			{
				pr_PortCallBody(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t984;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ReplyStatement(AST _t) throws RecognitionException {
		
		AST pr_ReplyStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1005 = _t;
			AST tmp337_AST_in = (AST)_t;
			match(_t,ReplyStatement);
			_t = _t.getFirstChild();
			pr_Port(_t);
			_t = _retTree;
			pr_PortReplyOp(_t);
			_t = _retTree;
			_t = __t1005;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_RaiseStatement(AST _t) throws RecognitionException {
		
		AST pr_RaiseStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1013 = _t;
			AST tmp338_AST_in = (AST)_t;
			match(_t,RaiseStatement);
			_t = _t.getFirstChild();
			pr_Port(_t);
			_t = _retTree;
			pr_PortRaiseOp(_t);
			_t = _retTree;
			_t = __t1013;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ReceiveStatement(AST _t) throws RecognitionException {
		
		AST pr_ReceiveStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1019 = _t;
			AST tmp339_AST_in = (AST)_t;
			match(_t,ReceiveStatement);
			_t = _t.getFirstChild();
			pr_PortOrAny(_t);
			_t = _retTree;
			pr_PortReceiveOp(_t);
			_t = _retTree;
			_t = __t1019;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TriggerStatement(AST _t) throws RecognitionException {
		
		AST pr_TriggerStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1050 = _t;
			AST tmp340_AST_in = (AST)_t;
			match(_t,TriggerStatement);
			_t = _t.getFirstChild();
			pr_PortOrAny(_t);
			_t = _retTree;
			pr_PortTriggerOp(_t);
			_t = _retTree;
			_t = __t1050;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_GetCallStatement(AST _t) throws RecognitionException {
		
		AST pr_GetCallStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1057 = _t;
			AST tmp341_AST_in = (AST)_t;
			match(_t,GetCallStatement);
			_t = _t.getFirstChild();
			pr_PortOrAny(_t);
			_t = _retTree;
			pr_PortGetCallOp(_t);
			_t = _retTree;
			_t = __t1057;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_GetReplyStatement(AST _t) throws RecognitionException {
		
		AST pr_GetReplyStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1096 = _t;
			AST tmp342_AST_in = (AST)_t;
			match(_t,GetReplyStatement);
			_t = _t.getFirstChild();
			pr_PortOrAny(_t);
			_t = _retTree;
			pr_PortGetReplyOp(_t);
			_t = _retTree;
			_t = __t1096;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CatchStatement(AST _t) throws RecognitionException {
		
		AST pr_CatchStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1122 = _t;
			AST tmp343_AST_in = (AST)_t;
			match(_t,CatchStatement);
			_t = _t.getFirstChild();
			pr_PortOrAny(_t);
			_t = _retTree;
			pr_PortCatchOp(_t);
			_t = _retTree;
			_t = __t1122;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CheckStatement(AST _t) throws RecognitionException {
		
		AST pr_CheckStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1106 = _t;
			AST tmp344_AST_in = (AST)_t;
			match(_t,CheckStatement);
			_t = _t.getFirstChild();
			pr_PortOrAny(_t);
			_t = _retTree;
			pr_PortCheckOp(_t);
			_t = _retTree;
			_t = __t1106;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ClearStatement(AST _t) throws RecognitionException {
		
		AST pr_ClearStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1132 = _t;
			AST tmp345_AST_in = (AST)_t;
			match(_t,ClearStatement);
			_t = _t.getFirstChild();
			pr_PortOrAll(_t);
			_t = _retTree;
			_t = __t1132;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_StartStatement(AST _t) throws RecognitionException {
		
		AST pr_StartStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1140 = _t;
			AST tmp346_AST_in = (AST)_t;
			match(_t,StartStatement);
			_t = _t.getFirstChild();
			pr_PortOrAll(_t);
			_t = _retTree;
			_t = __t1140;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_StopStatement(AST _t) throws RecognitionException {
		
		AST pr_StopStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1142 = _t;
			AST tmp347_AST_in = (AST)_t;
			match(_t,StopStatement);
			_t = _t.getFirstChild();
			pr_PortOrAll(_t);
			_t = _retTree;
			_t = __t1142;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_HaltStatement(AST _t) throws RecognitionException {
		
		AST pr_HaltStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1144 = _t;
			AST tmp348_AST_in = (AST)_t;
			match(_t,HaltStatement);
			_t = _t.getFirstChild();
			pr_PortOrAll(_t);
			_t = _retTree;
			_t = __t1144;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CheckStateStatement(AST _t) throws RecognitionException {
		
		AST pr_CheckStateStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1146 = _t;
			AST tmp349_AST_in = (AST)_t;
			match(_t,CheckStateStatement);
			_t = _t.getFirstChild();
			pr_PortOrAllOrAny(_t);
			_t = _retTree;
			pr_SingleExpression(_t);
			_t = _retTree;
			_t = __t1146;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SendParameter(AST _t) throws RecognitionException {
		
		AST pr_SendParameter_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_TemplateInstance(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ToClause(AST _t) throws RecognitionException {
		
		AST pr_ToClause_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t973 = _t;
			AST tmp350_AST_in = (AST)_t;
			match(_t,ToClause);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AddressRef:
			{
				pr_AddressRef(_t);
				_t = _retTree;
				break;
			}
			case AddressRefList:
			{
				pr_AddressRefList(_t);
				_t = _retTree;
				break;
			}
			case AllKeyword:
			{
				{
				AST tmp351_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				AST tmp352_AST_in = (AST)_t;
				match(_t,COMPONENT);
				_t = _t.getNextSibling();
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t973;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AddressRef(AST _t) throws RecognitionException {
		
		AST pr_AddressRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t981 = _t;
			AST tmp353_AST_in = (AST)_t;
			match(_t,AddressRef);
			_t = _t.getFirstChild();
			{
			pr_TemplateInstance(_t);
			_t = _retTree;
			}
			_t = __t981;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AddressRefList(AST _t) throws RecognitionException {
		
		AST pr_AddressRefList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t977 = _t;
			AST tmp354_AST_in = (AST)_t;
			match(_t,AddressRefList);
			_t = _t.getFirstChild();
			pr_AddressRef(_t);
			_t = _retTree;
			{
			_loop979:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==AddressRef)) {
					pr_AddressRef(_t);
					_t = _retTree;
				}
				else {
					break _loop979;
				}
				
			} while (true);
			}
			_t = __t977;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CallParameters(AST _t) throws RecognitionException {
		
		AST pr_CallParameters_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t988 = _t;
			AST tmp355_AST_in = (AST)_t;
			match(_t,CallParameters);
			_t = _t.getFirstChild();
			pr_TemplateInstance(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case CallTimerValue:
			{
				pr_CallTimerValue(_t);
				_t = _retTree;
				break;
			}
			case NowaitKeyword:
			{
				AST tmp356_AST_in = (AST)_t;
				match(_t,NowaitKeyword);
				_t = _t.getNextSibling();
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t988;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortCallBody(AST _t) throws RecognitionException {
		
		AST pr_PortCallBody_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_CallBodyStatement(_t);
			_t = _retTree;
			{
			_loop996:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==CallBodyStatement)) {
					pr_CallBodyStatement(_t);
					_t = _retTree;
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp357_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case CallBodyStatement:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop996;
				}
				
			} while (true);
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CallTimerValue(AST _t) throws RecognitionException {
		
		AST pr_CallTimerValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t991 = _t;
			AST tmp358_AST_in = (AST)_t;
			match(_t,CallTimerValue);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Expression:
			{
				pr_TimerValue(_t);
				_t = _retTree;
				break;
			}
			case NowaitKeyword:
			{
				AST tmp359_AST_in = (AST)_t;
				match(_t,NowaitKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t991;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CallBodyStatement(AST _t) throws RecognitionException {
		
		AST pr_CallBodyStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t998 = _t;
			AST tmp360_AST_in = (AST)_t;
			match(_t,CallBodyStatement);
			_t = _t.getFirstChild();
			pr_CallBodyGuard(_t);
			_t = _retTree;
			pr_StatementBlock(_t);
			_t = _retTree;
			_t = __t998;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CallBodyGuard(AST _t) throws RecognitionException {
		
		AST pr_CallBodyGuard_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1000 = _t;
			AST tmp361_AST_in = (AST)_t;
			match(_t,CallBodyGuard);
			_t = _t.getFirstChild();
			pr_AltGuardChar(_t);
			_t = _retTree;
			pr_CallBodyOps(_t);
			_t = _retTree;
			_t = __t1000;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AltGuardChar(AST _t) throws RecognitionException {
		
		AST pr_AltGuardChar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1369 = _t;
			AST tmp362_AST_in = (AST)_t;
			match(_t,AltGuardChar);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case BooleanExpression:
			{
				pr_BooleanExpression(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t1369;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CallBodyOps(AST _t) throws RecognitionException {
		
		AST pr_CallBodyOps_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1002 = _t;
			AST tmp363_AST_in = (AST)_t;
			match(_t,CallBodyOps);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GetReplyStatement:
			{
				pr_GetReplyStatement(_t);
				_t = _retTree;
				break;
			}
			case CatchStatement:
			{
				pr_CatchStatement(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1002;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortReplyOp(AST _t) throws RecognitionException {
		
		AST pr_PortReplyOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1007 = _t;
			AST tmp364_AST_in = (AST)_t;
			match(_t,PortReplyOp);
			_t = _t.getFirstChild();
			{
			pr_TemplateInstance(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Expression:
			{
				pr_ReplyValue(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case ToClause:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ToClause:
			{
				pr_ToClause(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t1007;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ReplyValue(AST _t) throws RecognitionException {
		
		AST pr_ReplyValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_Expression(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortRaiseOp(AST _t) throws RecognitionException {
		
		AST pr_PortRaiseOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1015 = _t;
			AST tmp365_AST_in = (AST)_t;
			match(_t,PortRaiseOp);
			_t = _t.getFirstChild();
			{
			pr_Signature(_t);
			_t = _retTree;
			pr_TemplateInstance(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ToClause:
			{
				pr_ToClause(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t1015;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortOrAny(AST _t) throws RecognitionException {
		
		AST pr_PortOrAny_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1021 = _t;
			AST tmp366_AST_in = (AST)_t;
			match(_t,PortOrAny);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Port:
			{
				pr_Port(_t);
				_t = _retTree;
				break;
			}
			case AnyKeyword:
			{
				AST tmp367_AST_in = (AST)_t;
				match(_t,AnyKeyword);
				_t = _t.getNextSibling();
				AST tmp368_AST_in = (AST)_t;
				match(_t,PortKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1021;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortReceiveOp(AST _t) throws RecognitionException {
		
		AST pr_PortReceiveOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1024 = _t;
			AST tmp369_AST_in = (AST)_t;
			match(_t,PortReceiveOp);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateInstance:
			{
				pr_ReceiveParameter(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case FromClause:
			case PortRedirect:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FromClause:
			{
				pr_FromClause(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case PortRedirect:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case PortRedirect:
			{
				pr_PortRedirect(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1024;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ReceiveParameter(AST _t) throws RecognitionException {
		
		AST pr_ReceiveParameter_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_TemplateInstance(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FromClause(AST _t) throws RecognitionException {
		
		AST pr_FromClause_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1030 = _t;
			AST tmp370_AST_in = (AST)_t;
			match(_t,FromClause);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AddressRef:
			{
				pr_AddressRef(_t);
				_t = _retTree;
				break;
			}
			case AddressRefList:
			{
				pr_AddressRefList(_t);
				_t = _retTree;
				break;
			}
			case AnyKeyword:
			{
				{
				AST tmp371_AST_in = (AST)_t;
				match(_t,AnyKeyword);
				_t = _t.getNextSibling();
				AST tmp372_AST_in = (AST)_t;
				match(_t,COMPONENT);
				_t = _t.getNextSibling();
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1030;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortRedirect(AST _t) throws RecognitionException {
		
		AST pr_PortRedirect_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1034 = _t;
			AST tmp373_AST_in = (AST)_t;
			match(_t,PortRedirect);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ValueSpec:
			{
				pr_ValueSpec(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case SenderSpec:
				{
					pr_SenderSpec(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			case SenderSpec:
			{
				pr_SenderSpec(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1034;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ValueSpec(AST _t) throws RecognitionException {
		
		AST pr_ValueSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1041 = _t;
			AST tmp374_AST_in = (AST)_t;
			match(_t,ValueSpec);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case VariableRef:
			{
				pr_VariableRef(_t);
				_t = _retTree;
				break;
			}
			case SingleValueSpec:
			{
				{
				int _cnt1046=0;
				_loop1046:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==SingleValueSpec)) {
						pr_SingleValueSpec(_t);
						_t = _retTree;
					}
					else {
						if ( _cnt1046>=1 ) { break _loop1046; } else {throw new NoViableAltException(_t);}
					}
					
					_cnt1046++;
				} while (true);
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1041;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SenderSpec(AST _t) throws RecognitionException {
		
		AST pr_SenderSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1048 = _t;
			AST tmp375_AST_in = (AST)_t;
			match(_t,SenderSpec);
			_t = _t.getFirstChild();
			pr_VariableRef(_t);
			_t = _retTree;
			_t = __t1048;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleValueSpec(AST _t) throws RecognitionException {
		
		AST pr_SingleValueSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<Symbol> c;
		
		try {      // for error handling
			AST __t1038 = _t;
			AST tmp376_AST_in = (AST)_t;
			match(_t,SingleValueSpec);
			_t = _t.getFirstChild();
			pr_VariableRef(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FieldReference:
			{
				pr_FieldReference(_t);
				_t = _retTree;
				c=pr_ExtendedFieldReference(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1038;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortTriggerOp(AST _t) throws RecognitionException {
		
		AST pr_PortTriggerOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1052 = _t;
			AST tmp377_AST_in = (AST)_t;
			match(_t,PortTriggerOp);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateInstance:
			{
				pr_ReceiveParameter(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case FromClause:
			case PortRedirect:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FromClause:
			{
				pr_FromClause(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case PortRedirect:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case PortRedirect:
			{
				pr_PortRedirect(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1052;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortGetCallOp(AST _t) throws RecognitionException {
		
		AST pr_PortGetCallOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1059 = _t;
			AST tmp378_AST_in = (AST)_t;
			match(_t,PortGetCallOp);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateInstance:
			{
				pr_ReceiveParameter(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case RedirectWithParamSpec:
			case FromClause:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FromClause:
			{
				pr_FromClause(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case RedirectWithParamSpec:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case RedirectWithParamSpec:
			{
				pr_PortRedirectWithParam(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1059;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortRedirectWithParam(AST _t) throws RecognitionException {
		
		AST pr_PortRedirectWithParam_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_RedirectWithParamSpec(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_RedirectWithParamSpec(AST _t) throws RecognitionException {
		
		AST pr_RedirectWithParamSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1072 = _t;
			AST tmp379_AST_in = (AST)_t;
			match(_t,RedirectWithParamSpec);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ParaSpec:
			{
				pr_ParaSpec(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case SenderSpec:
				{
					pr_SenderSpec(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			case SenderSpec:
			{
				pr_SenderSpec(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t1072;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortRedirectWithValueAndParam(AST _t) throws RecognitionException {
		
		AST pr_PortRedirectWithValueAndParam_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1065 = _t;
			AST tmp380_AST_in = (AST)_t;
			match(_t,PortRedirectWithValueAndParam);
			_t = _t.getFirstChild();
			pr_RedirectWithValueAndParamSpec(_t);
			_t = _retTree;
			_t = __t1065;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_RedirectWithValueAndParamSpec(AST _t) throws RecognitionException {
		
		AST pr_RedirectWithValueAndParamSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1067 = _t;
			AST tmp381_AST_in = (AST)_t;
			match(_t,RedirectWithValueAndParamSpec);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ValueSpec:
			{
				pr_ValueSpec(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case ParaSpec:
				{
					pr_ParaSpec(_t);
					_t = _retTree;
					break;
				}
				case 3:
				case SenderSpec:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case SenderSpec:
				{
					pr_SenderSpec(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			case RedirectWithParamSpec:
			{
				pr_RedirectWithParamSpec(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1067;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ParaSpec(AST _t) throws RecognitionException {
		
		AST pr_ParaSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1077 = _t;
			AST tmp382_AST_in = (AST)_t;
			match(_t,ParaSpec);
			_t = _t.getFirstChild();
			pr_ParaAssignmentList(_t);
			_t = _retTree;
			_t = __t1077;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ParaAssignmentList(AST _t) throws RecognitionException {
		
		AST pr_ParaAssignmentList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1079 = _t;
			AST tmp383_AST_in = (AST)_t;
			match(_t,ParaAssignmentList);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AssignmentList:
			{
				pr_AssignmentList(_t);
				_t = _retTree;
				break;
			}
			case VariableList:
			{
				pr_VariableList(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1079;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AssignmentList(AST _t) throws RecognitionException {
		
		AST pr_AssignmentList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1082 = _t;
			AST tmp384_AST_in = (AST)_t;
			match(_t,AssignmentList);
			_t = _t.getFirstChild();
			{
			_loop1084:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==VariableAssignment)) {
					pr_VariableAssignment(_t);
					_t = _retTree;
				}
				else {
					break _loop1084;
				}
				
			} while (true);
			}
			_t = __t1082;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_VariableList(AST _t) throws RecognitionException {
		
		AST pr_VariableList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1089 = _t;
			AST tmp385_AST_in = (AST)_t;
			match(_t,VariableList);
			_t = _t.getFirstChild();
			{
			_loop1091:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==VariableEntry)) {
					pr_VariableEntry(_t);
					_t = _retTree;
				}
				else {
					break _loop1091;
				}
				
			} while (true);
			}
			_t = __t1089;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_VariableAssignment(AST _t) throws RecognitionException {
		
		AST pr_VariableAssignment_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1086 = _t;
			AST tmp386_AST_in = (AST)_t;
			match(_t,VariableAssignment);
			_t = _t.getFirstChild();
			pr_VariableRef(_t);
			_t = _retTree;
			pr_ParameterIdentifier(_t);
			_t = _retTree;
			_t = __t1086;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ParameterIdentifier(AST _t) throws RecognitionException {
		
		AST pr_ParameterIdentifier_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			a=pr_Identifier(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_VariableEntry(AST _t) throws RecognitionException {
		
		AST pr_VariableEntry_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1093 = _t;
			AST tmp387_AST_in = (AST)_t;
			match(_t,VariableEntry);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case VariableRef:
			{
				pr_VariableRef(_t);
				_t = _retTree;
				break;
			}
			case NotUsedSymbol:
			{
				AST tmp388_AST_in = (AST)_t;
				match(_t,NotUsedSymbol);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1093;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortGetReplyOp(AST _t) throws RecognitionException {
		
		AST pr_PortGetReplyOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1098 = _t;
			AST tmp389_AST_in = (AST)_t;
			match(_t,PortGetReplyOp);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TemplateInstance:
			{
				pr_ReceiveParameter(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case ValueMatchSpec:
				{
					pr_ValueMatchSpec(_t);
					_t = _retTree;
					break;
				}
				case 3:
				case PortRedirectWithValueAndParam:
				case FromClause:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			case 3:
			case PortRedirectWithValueAndParam:
			case FromClause:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FromClause:
			{
				pr_FromClause(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case PortRedirectWithValueAndParam:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case PortRedirectWithValueAndParam:
			{
				pr_PortRedirectWithValueAndParam(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1098;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ValueMatchSpec(AST _t) throws RecognitionException {
		
		AST pr_ValueMatchSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1104 = _t;
			AST tmp390_AST_in = (AST)_t;
			match(_t,ValueMatchSpec);
			_t = _t.getFirstChild();
			pr_TemplateInstance(_t);
			_t = _retTree;
			_t = __t1104;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortCheckOp(AST _t) throws RecognitionException {
		
		AST pr_PortCheckOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1108 = _t;
			AST tmp391_AST_in = (AST)_t;
			match(_t,PortCheckOp);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case CheckParameter:
			{
				pr_CheckParameter(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1108;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CheckParameter(AST _t) throws RecognitionException {
		
		AST pr_CheckParameter_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1111 = _t;
			AST tmp392_AST_in = (AST)_t;
			match(_t,CheckParameter);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case CheckPortOpsPresent:
			{
				pr_CheckPortOpsPresent(_t);
				_t = _retTree;
				break;
			}
			case FromClausePresent:
			{
				pr_FromClausePresent(_t);
				_t = _retTree;
				break;
			}
			case RedirectPresent:
			{
				pr_RedirectPresent(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1111;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CheckPortOpsPresent(AST _t) throws RecognitionException {
		
		AST pr_CheckPortOpsPresent_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1119 = _t;
			AST tmp393_AST_in = (AST)_t;
			match(_t,CheckPortOpsPresent);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case PortReceiveOp:
			{
				pr_PortReceiveOp(_t);
				_t = _retTree;
				break;
			}
			case PortGetCallOp:
			{
				pr_PortGetCallOp(_t);
				_t = _retTree;
				break;
			}
			case PortGetReplyOp:
			{
				pr_PortGetReplyOp(_t);
				_t = _retTree;
				break;
			}
			case PortCatchOp:
			{
				pr_PortCatchOp(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1119;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FromClausePresent(AST _t) throws RecognitionException {
		
		AST pr_FromClausePresent_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1116 = _t;
			AST tmp394_AST_in = (AST)_t;
			match(_t,FromClausePresent);
			_t = _t.getFirstChild();
			pr_FromClause(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SenderSpec:
			{
				pr_SenderSpec(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1116;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_RedirectPresent(AST _t) throws RecognitionException {
		
		AST pr_RedirectPresent_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1114 = _t;
			AST tmp395_AST_in = (AST)_t;
			match(_t,RedirectPresent);
			_t = _t.getFirstChild();
			pr_SenderSpec(_t);
			_t = _retTree;
			_t = __t1114;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortCatchOp(AST _t) throws RecognitionException {
		
		AST pr_PortCatchOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1124 = _t;
			AST tmp396_AST_in = (AST)_t;
			match(_t,PortCatchOp);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case CatchOpParameter:
			{
				pr_CatchOpParameter(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case FromClause:
			case PortRedirect:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FromClause:
			{
				pr_FromClause(_t);
				_t = _retTree;
				break;
			}
			case 3:
			case PortRedirect:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case PortRedirect:
			{
				pr_PortRedirect(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1124;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CatchOpParameter(AST _t) throws RecognitionException {
		
		AST pr_CatchOpParameter_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1129 = _t;
			AST tmp397_AST_in = (AST)_t;
			match(_t,CatchOpParameter);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Signature:
			{
				pr_Signature(_t);
				_t = _retTree;
				pr_TemplateInstance(_t);
				_t = _retTree;
				break;
			}
			case TimeoutKeyword:
			{
				AST tmp398_AST_in = (AST)_t;
				match(_t,TimeoutKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1129;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortOrAll(AST _t) throws RecognitionException {
		
		AST pr_PortOrAll_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1134 = _t;
			AST tmp399_AST_in = (AST)_t;
			match(_t,PortOrAll);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Port:
			{
				pr_Port(_t);
				_t = _retTree;
				break;
			}
			case AllPort:
			{
				pr_AllPort(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1134;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AllPort(AST _t) throws RecognitionException {
		
		AST pr_AllPort_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1137 = _t;
			AST tmp400_AST_in = (AST)_t;
			match(_t,AllPort);
			_t = _t.getFirstChild();
			{
			}
			_t = __t1137;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PortOrAllOrAny(AST _t) throws RecognitionException {
		
		AST pr_PortOrAllOrAny_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1148 = _t;
			AST tmp401_AST_in = (AST)_t;
			match(_t,PortOrAllOrAny);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case PortOrAll:
			{
				pr_PortOrAll(_t);
				_t = _retTree;
				break;
			}
			case AnyKeyword:
			{
				AST tmp402_AST_in = (AST)_t;
				match(_t,AnyKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1148;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_StartTimerStatement(AST _t) throws RecognitionException {
		
		AST pr_StartTimerStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1157 = _t;
			AST tmp403_AST_in = (AST)_t;
			match(_t,StartTimerStatement);
			_t = _t.getFirstChild();
			pr_TimerRef(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Expression:
			{
				pr_TimerValue(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1157;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_StopTimerStatement(AST _t) throws RecognitionException {
		
		AST pr_StopTimerStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1160 = _t;
			AST tmp404_AST_in = (AST)_t;
			match(_t,StopTimerStatement);
			_t = _t.getFirstChild();
			pr_TimerRefOrAll(_t);
			_t = _retTree;
			_t = __t1160;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TimeoutStatement(AST _t) throws RecognitionException {
		
		AST pr_TimeoutStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1169 = _t;
			AST tmp405_AST_in = (AST)_t;
			match(_t,TimeoutStatement);
			_t = _t.getFirstChild();
			pr_TimerRefOrAny(_t);
			_t = _retTree;
			_t = __t1169;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TimerOps(AST _t) throws RecognitionException {
		
		AST pr_TimerOps_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1154 = _t;
			AST tmp406_AST_in = (AST)_t;
			match(_t,TimerOps);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ReadTimerOp:
			{
				pr_ReadTimerOp(_t);
				_t = _retTree;
				break;
			}
			case RunningTimerOp:
			{
				pr_RunningTimerOp(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1154;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ReadTimerOp(AST _t) throws RecognitionException {
		
		AST pr_ReadTimerOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1165 = _t;
			AST tmp407_AST_in = (AST)_t;
			match(_t,ReadTimerOp);
			_t = _t.getFirstChild();
			pr_TimerRef(_t);
			_t = _retTree;
			_t = __t1165;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_RunningTimerOp(AST _t) throws RecognitionException {
		
		AST pr_RunningTimerOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1167 = _t;
			AST tmp408_AST_in = (AST)_t;
			match(_t,RunningTimerOp);
			_t = _t.getFirstChild();
			pr_TimerRefOrAny(_t);
			_t = _retTree;
			_t = __t1167;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TimerRefOrAll(AST _t) throws RecognitionException {
		
		AST pr_TimerRefOrAll_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1162 = _t;
			AST tmp409_AST_in = (AST)_t;
			match(_t,TimerRefOrAll);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TimerRef:
			{
				pr_TimerRef(_t);
				_t = _retTree;
				break;
			}
			case AllKeyword:
			{
				AST tmp410_AST_in = (AST)_t;
				match(_t,AllKeyword);
				_t = _t.getNextSibling();
				AST tmp411_AST_in = (AST)_t;
				match(_t,TimerKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1162;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TimerRefOrAny(AST _t) throws RecognitionException {
		
		AST pr_TimerRefOrAny_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1171 = _t;
			AST tmp412_AST_in = (AST)_t;
			match(_t,TimerRefOrAny);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TimerRef:
			{
				pr_TimerRef(_t);
				_t = _retTree;
				break;
			}
			case AnyKeyword:
			{
				AST tmp413_AST_in = (AST)_t;
				match(_t,AnyKeyword);
				_t = _t.getNextSibling();
				AST tmp414_AST_in = (AST)_t;
				match(_t,TimerKeyword);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1171;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final Reference  pr_ReferencedType(AST _t) throws RecognitionException {
		Reference r=new Reference();;
		
		AST pr_ReferencedType_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b; List<Symbol> c;
		
		try {      // for error handling
			AST __t1185 = _t;
			AST tmp415_AST_in = (AST)_t;
			match(_t,ReferencedType);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GlobalModuleId:
			{
				a=pr_GlobalModuleId(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					r.setModule(a);
				}
				break;
			}
			case TypeReference:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			b=pr_TypeReference(_t);
			_t = _retTree;
			if ( inputState.guessing==0 ) {
				r.setIdentifier(b);
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ExtendedFieldReference:
			{
				c=pr_ExtendedFieldReference(_t);
				_t = _retTree;
				if ( inputState.guessing==0 ) {
					r.setExtendedReferences(c);
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t1185;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return r;
	}
	
	public final Symbol  pr_TypeReference(AST _t) throws RecognitionException {
		Symbol s=null;
		
		AST pr_TypeReference_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1194 = _t;
			AST tmp416_AST_in = (AST)_t;
			match(_t,TypeReference);
			_t = _t.getFirstChild();
			{
			s=pr_Identifier(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TypeActualParList:
			{
				pr_TypeActualParList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			}
			_t = __t1194;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
		return s;
	}
	
	public final void pr_TypeActualParList(AST _t) throws RecognitionException {
		
		AST pr_TypeActualParList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1198 = _t;
			AST tmp417_AST_in = (AST)_t;
			match(_t,TypeActualParList);
			_t = _t.getFirstChild();
			{
			_loop1200:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==TypeActualPar)) {
					pr_TypeActualPar(_t);
					_t = _retTree;
				}
				else {
					break _loop1200;
				}
				
			} while (true);
			}
			_t = __t1198;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_TypeActualPar(AST _t) throws RecognitionException {
		
		AST pr_TypeActualPar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1202 = _t;
			AST tmp418_AST_in = (AST)_t;
			match(_t,TypeActualPar);
			_t = _t.getFirstChild();
			pr_ConstantExpression(_t);
			_t = _retTree;
			_t = __t1202;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ArrayBounds(AST _t) throws RecognitionException {
		
		AST pr_ArrayBounds_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_SingleConstExpression(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ArrayDefRange(AST _t) throws RecognitionException {
		
		AST pr_ArrayDefRange_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1208 = _t;
			AST tmp419_AST_in = (AST)_t;
			match(_t,ArrayDefRange);
			_t = _t.getFirstChild();
			pr_ArrayBounds(_t);
			_t = _retTree;
			pr_ArrayBounds(_t);
			_t = _retTree;
			_t = __t1208;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
/*****1.6.3 - Value  *****/
	public final void pr_Value(AST _t) throws RecognitionException {
		
		AST pr_Value_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1211 = _t;
			AST tmp420_AST_in = (AST)_t;
			match(_t,Value);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ReferencedValue:
			{
				pr_ReferencedValue(_t);
				_t = _retTree;
				break;
			}
			case PredefinedValue:
			{
				pr_PredefinedValue(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1211;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_PredefinedValue(AST _t) throws RecognitionException {
		
		AST pr_PredefinedValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1214 = _t;
			AST tmp421_AST_in = (AST)_t;
			match(_t,PredefinedValue);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case BitStringValue:
			{
				pr_BitStringValue(_t);
				_t = _retTree;
				break;
			}
			case BooleanValue:
			{
				pr_BooleanValue(_t);
				_t = _retTree;
				break;
			}
			case CharStringValue:
			{
				pr_CharStringValue(_t);
				_t = _retTree;
				break;
			}
			case IntegerValue:
			{
				pr_IntegerValue(_t);
				_t = _retTree;
				break;
			}
			case OctetStringValue:
			{
				pr_OctetStringValue(_t);
				_t = _retTree;
				break;
			}
			case OBJID:
			{
				pr_ObjectIdentifierValue(_t);
				_t = _retTree;
				break;
			}
			case HexStringValue:
			{
				pr_HexStringValue(_t);
				_t = _retTree;
				break;
			}
			case VerdictTypeValue:
			{
				pr_VerdictTypeValue(_t);
				_t = _retTree;
				break;
			}
			case EnumeratedValue:
			{
				pr_EnumeratedValue(_t);
				_t = _retTree;
				break;
			}
			case FloatValue:
			{
				pr_FloatValue(_t);
				_t = _retTree;
				break;
			}
			case NaN:
			{
				pr_NaNValue(_t);
				_t = _retTree;
				break;
			}
			case Null:
			{
				pr_AddressValue(_t);
				_t = _retTree;
				break;
			}
			case OmitValue:
			{
				pr_OmitValue(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1214;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_BitStringValue(AST _t) throws RecognitionException {
		
		AST pr_BitStringValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t1221 = _t;
			AST tmp422_AST_in = (AST)_t;
			match(_t,BitStringValue);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,BSTRING);
			_t = _t.getNextSibling();
			_t = __t1221;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_BooleanValue(AST _t) throws RecognitionException {
		
		AST pr_BooleanValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		AST b = null;
		
		try {      // for error handling
			AST __t1223 = _t;
			AST tmp423_AST_in = (AST)_t;
			match(_t,BooleanValue);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TRUE:
			{
				a = (AST)_t;
				match(_t,TRUE);
				_t = _t.getNextSibling();
				break;
			}
			case FALSE:
			{
				b = (AST)_t;
				match(_t,FALSE);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1223;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CharStringValue(AST _t) throws RecognitionException {
		
		AST pr_CharStringValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t1245 = _t;
			AST tmp424_AST_in = (AST)_t;
			match(_t,CharStringValue);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case CSTRING:
			{
				a = (AST)_t;
				match(_t,CSTRING);
				_t = _t.getNextSibling();
				break;
			}
			case Quadruple:
			{
				pr_Quadruple(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1245;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_IntegerValue(AST _t) throws RecognitionException {
		
		AST pr_IntegerValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST b = null;
		
		try {      // for error handling
			AST __t1226 = _t;
			AST tmp425_AST_in = (AST)_t;
			match(_t,IntegerValue);
			_t = _t.getFirstChild();
			b = (AST)_t;
			match(_t,NUMBER);
			_t = _t.getNextSibling();
			_t = __t1226;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_OctetStringValue(AST _t) throws RecognitionException {
		
		AST pr_OctetStringValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t1228 = _t;
			AST tmp426_AST_in = (AST)_t;
			match(_t,OctetStringValue);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,OSTRING);
			_t = _t.getNextSibling();
			_t = __t1228;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_HexStringValue(AST _t) throws RecognitionException {
		
		AST pr_HexStringValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t1233 = _t;
			AST tmp427_AST_in = (AST)_t;
			match(_t,HexStringValue);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,HSTRING);
			_t = _t.getNextSibling();
			_t = __t1233;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_VerdictTypeValue(AST _t) throws RecognitionException {
		
		AST pr_VerdictTypeValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1235 = _t;
			AST tmp428_AST_in = (AST)_t;
			match(_t,VerdictTypeValue);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Pass:
			{
				pr_Pass(_t);
				_t = _retTree;
				break;
			}
			case Fail:
			{
				pr_Fail(_t);
				_t = _retTree;
				break;
			}
			case Inconc:
			{
				pr_Inconc(_t);
				_t = _retTree;
				break;
			}
			case None:
			{
				pr_None(_t);
				_t = _retTree;
				break;
			}
			case Error:
			{
				pr_Error(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1235;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_EnumeratedValue(AST _t) throws RecognitionException {
		
		AST pr_EnumeratedValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t1243 = _t;
			AST tmp429_AST_in = (AST)_t;
			match(_t,EnumeratedValue);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			_t = __t1243;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FloatValue(AST _t) throws RecognitionException {
		
		AST pr_FloatValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t1217 = _t;
			AST tmp430_AST_in = (AST)_t;
			match(_t,FloatValue);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,FLOATVALUE);
			_t = _t.getNextSibling();
			_t = __t1217;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_NaNValue(AST _t) throws RecognitionException {
		
		AST pr_NaNValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t1219 = _t;
			AST tmp431_AST_in = (AST)_t;
			match(_t,NaN);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,NAN);
			_t = _t.getNextSibling();
			_t = __t1219;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AddressValue(AST _t) throws RecognitionException {
		
		AST pr_AddressValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST tmp432_AST_in = (AST)_t;
			match(_t,Null);
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_OmitValue(AST _t) throws RecognitionException {
		
		AST pr_OmitValue_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			a = (AST)_t;
			match(_t,OmitValue);
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ObjIdComponent(AST _t) throws RecognitionException {
		
		AST pr_ObjIdComponent_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case NameForm:
			{
				pr_NameForm(_t);
				_t = _retTree;
				break;
			}
			case NumberForm:
			{
				pr_NumberForm(_t);
				_t = _retTree;
				break;
			}
			case NameAndNumberForm:
			{
				pr_NameAndNumberForm(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Pass(AST _t) throws RecognitionException {
		
		AST pr_Pass_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			a = (AST)_t;
			match(_t,Pass);
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Fail(AST _t) throws RecognitionException {
		
		AST pr_Fail_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			a = (AST)_t;
			match(_t,Fail);
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Inconc(AST _t) throws RecognitionException {
		
		AST pr_Inconc_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			a = (AST)_t;
			match(_t,Inconc);
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_None(AST _t) throws RecognitionException {
		
		AST pr_None_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			a = (AST)_t;
			match(_t,None);
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Error(AST _t) throws RecognitionException {
		
		AST pr_Error_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			a = (AST)_t;
			match(_t,Error);
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Quadruple(AST _t) throws RecognitionException {
		
		AST pr_Quadruple_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1248 = _t;
			AST tmp433_AST_in = (AST)_t;
			match(_t,Quadruple);
			_t = _t.getFirstChild();
			pr_Group(_t);
			_t = _retTree;
			pr_Plane(_t);
			_t = _retTree;
			pr_Row(_t);
			_t = _retTree;
			pr_Cell(_t);
			_t = _retTree;
			_t = __t1248;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Group(AST _t) throws RecognitionException {
		
		AST pr_Group_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t1250 = _t;
			AST tmp434_AST_in = (AST)_t;
			match(_t,Group);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,NUMBER);
			_t = _t.getNextSibling();
			_t = __t1250;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Plane(AST _t) throws RecognitionException {
		
		AST pr_Plane_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t1252 = _t;
			AST tmp435_AST_in = (AST)_t;
			match(_t,Plane);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,NUMBER);
			_t = _t.getNextSibling();
			_t = __t1252;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Row(AST _t) throws RecognitionException {
		
		AST pr_Row_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t1254 = _t;
			AST tmp436_AST_in = (AST)_t;
			match(_t,Row);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,NUMBER);
			_t = _t.getNextSibling();
			_t = __t1254;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Cell(AST _t) throws RecognitionException {
		
		AST pr_Cell_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t1256 = _t;
			AST tmp437_AST_in = (AST)_t;
			match(_t,Cell);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,NUMBER);
			_t = _t.getNextSibling();
			_t = __t1256;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ValueReference(AST _t) throws RecognitionException {
		
		AST pr_ValueReference_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a,b;
		
		try {      // for error handling
			AST __t1264 = _t;
			AST tmp438_AST_in = (AST)_t;
			match(_t,ValueReference);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GlobalModuleId:
			{
				a=pr_GlobalModuleId(_t);
				_t = _retTree;
				break;
			}
			case Identifier:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			b=pr_Identifier(_t);
			_t = _retTree;
			_t = __t1264;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FormalTypePar(AST _t) throws RecognitionException {
		
		AST pr_FormalTypePar_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t1274 = _t;
			AST tmp439_AST_in = (AST)_t;
			match(_t,FormalTypePar);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			_t = __t1274;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SingleWithAttrib(AST _t) throws RecognitionException {
		
		AST pr_SingleWithAttrib_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST o = null;
		
		try {      // for error handling
			AST __t1292 = _t;
			AST tmp440_AST_in = (AST)_t;
			match(_t,SingleWithAttrib);
			_t = _t.getFirstChild();
			pr_AttribKeyword(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case OVERRIDE:
			{
				o = (AST)_t;
				match(_t,OVERRIDE);
				_t = _t.getNextSibling();
				break;
			}
			case AttribQualifier:
			case AttribSpec:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AttribQualifier:
			{
				pr_AttribQualifier(_t);
				_t = _retTree;
				break;
			}
			case AttribSpec:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			pr_AttribSpec(_t);
			_t = _retTree;
			_t = __t1292;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AttribKeyword(AST _t) throws RecognitionException {
		
		AST pr_AttribKeyword_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		AST b = null;
		AST c = null;
		AST d = null;
		AST e = null;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ENCODE:
			{
				a = (AST)_t;
				match(_t,ENCODE);
				_t = _t.getNextSibling();
				break;
			}
			case DISPLAY:
			{
				b = (AST)_t;
				match(_t,DISPLAY);
				_t = _t.getNextSibling();
				break;
			}
			case EXTENSION:
			{
				c = (AST)_t;
				match(_t,EXTENSION);
				_t = _t.getNextSibling();
				break;
			}
			case VARIANT:
			{
				d = (AST)_t;
				match(_t,VARIANT);
				_t = _t.getNextSibling();
				break;
			}
			case OPTIONAL:
			{
				e = (AST)_t;
				match(_t,OPTIONAL);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AttribQualifier(AST _t) throws RecognitionException {
		
		AST pr_AttribQualifier_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1297 = _t;
			AST tmp441_AST_in = (AST)_t;
			match(_t,AttribQualifier);
			_t = _t.getFirstChild();
			{
			_loop1299:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==DefOrFieldRef)) {
					pr_DefOrFieldRef(_t);
					_t = _retTree;
				}
				else {
					break _loop1299;
				}
				
			} while (true);
			}
			_t = __t1297;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AttribSpec(AST _t) throws RecognitionException {
		
		AST pr_AttribSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		String a;
		
		try {      // for error handling
			AST __t1331 = _t;
			AST tmp442_AST_in = (AST)_t;
			match(_t,AttribSpec);
			_t = _t.getFirstChild();
			a=pr_FreeText(_t);
			_t = _retTree;
			_t = __t1331;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_DefOrFieldRef(AST _t) throws RecognitionException {
		
		AST pr_DefOrFieldRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<Symbol> c;
		
		try {      // for error handling
			AST __t1301 = _t;
			AST tmp443_AST_in = (AST)_t;
			match(_t,DefOrFieldRef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FieldReference:
			{
				{
				pr_FieldReference(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case ExtendedFieldReference:
				{
					c=pr_ExtendedFieldReference(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case SingleExpression:
			{
				pr_SingleExpression(_t);
				_t = _retTree;
				break;
			}
			case FullGroupIdentifier:
			case Identifier:
			{
				pr_DefinitionRef(_t);
				_t = _retTree;
				break;
			}
			case AllRef:
			{
				pr_AllRef(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1301;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_DefinitionRef(AST _t) throws RecognitionException {
		
		AST pr_DefinitionRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FullGroupIdentifier:
			{
				a=pr_FullGroupIdentifier(_t);
				_t = _retTree;
				break;
			}
			case Identifier:
			{
				a=pr_Identifier(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AllRef(AST _t) throws RecognitionException {
		
		AST pr_AllRef_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<String> s=null;
		
		try {      // for error handling
			AST __t1310 = _t;
			AST tmp444_AST_in = (AST)_t;
			match(_t,AllRef);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case GROUP:
			{
				{
				AST tmp445_AST_in = (AST)_t;
				match(_t,GROUP);
				_t = _t.getNextSibling();
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case GroupRefList:
				{
					s=pr_GroupRefList(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case TYPE:
			{
				{
				AST tmp446_AST_in = (AST)_t;
				match(_t,TYPE);
				_t = _t.getNextSibling();
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case TypeRefList:
				{
					s=pr_TypeRefList(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case TemplateKeyword:
			{
				{
				AST tmp447_AST_in = (AST)_t;
				match(_t,TemplateKeyword);
				_t = _t.getNextSibling();
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case TemplateRefList:
				{
					s=pr_TemplateRefList(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case CONST:
			{
				{
				AST tmp448_AST_in = (AST)_t;
				match(_t,CONST);
				_t = _t.getNextSibling();
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case ConstRefList:
				{
					s=pr_ConstRefList(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case AltstepKeyword:
			{
				{
				AST tmp449_AST_in = (AST)_t;
				match(_t,AltstepKeyword);
				_t = _t.getNextSibling();
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case AltstepRefList:
				{
					s=pr_AltstepRefList(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case TESTCASE:
			{
				{
				AST tmp450_AST_in = (AST)_t;
				match(_t,TESTCASE);
				_t = _t.getNextSibling();
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case TestcaseRefList:
				{
					s=pr_TestcaseRefList(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case FUNCTION:
			{
				{
				AST tmp451_AST_in = (AST)_t;
				match(_t,FUNCTION);
				_t = _t.getNextSibling();
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case FunctionRefList:
				{
					s=pr_FunctionRefList(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case SIGNATURE:
			{
				{
				AST tmp452_AST_in = (AST)_t;
				match(_t,SIGNATURE);
				_t = _t.getNextSibling();
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case SignatureRefList:
				{
					s=pr_SignatureRefList(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case MODULEPAR:
			{
				{
				AST tmp453_AST_in = (AST)_t;
				match(_t,MODULEPAR);
				_t = _t.getNextSibling();
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case ModuleParRefList:
				{
					s=pr_ModuleParRefList(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1310;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ReturnStatement(AST _t) throws RecognitionException {
		
		AST pr_ReturnStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1349 = _t;
			AST tmp454_AST_in = (AST)_t;
			match(_t,ReturnStatement);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Expression:
			{
				pr_Expression(_t);
				_t = _retTree;
				break;
			}
			case InLineTemplate:
			{
				pr_InLineTemplate(_t);
				_t = _retTree;
				break;
			}
			case TemplateRefWithParList:
			{
				pr_TemplateRefWithParList(_t);
				_t = _retTree;
				break;
			}
			case MatchingSymbol:
			{
				pr_MatchingSymbol(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1349;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_AltConstruct(AST _t) throws RecognitionException {
		
		AST pr_AltConstruct_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1352 = _t;
			AST tmp455_AST_in = (AST)_t;
			match(_t,AltConstruct);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AltGuardList:
			{
				pr_AltGuardList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1352;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_InterleavedConstruct(AST _t) throws RecognitionException {
		
		AST pr_InterleavedConstruct_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1376 = _t;
			AST tmp456_AST_in = (AST)_t;
			match(_t,InterleavedConstruct);
			_t = _t.getFirstChild();
			{
			_loop1379:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==InterleavedGuardElement)) {
					pr_InterleavedGuardElement(_t);
					_t = _retTree;
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case SemiColon:
					{
						AST tmp457_AST_in = (AST)_t;
						match(_t,SemiColon);
						_t = _t.getNextSibling();
						break;
					}
					case 3:
					case InterleavedGuardElement:
					{
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				else {
					break _loop1379;
				}
				
			} while (true);
			}
			_t = __t1376;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_LabelStatement(AST _t) throws RecognitionException {
		
		AST pr_LabelStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t1387 = _t;
			AST tmp458_AST_in = (AST)_t;
			match(_t,LabelStatement);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			_t = __t1387;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_GotoStatement(AST _t) throws RecognitionException {
		
		AST pr_GotoStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		Symbol a;
		
		try {      // for error handling
			AST __t1389 = _t;
			AST tmp459_AST_in = (AST)_t;
			match(_t,GotoStatement);
			_t = _t.getFirstChild();
			a=pr_Identifier(_t);
			_t = _retTree;
			_t = __t1389;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ActivateOp(AST _t) throws RecognitionException {
		
		AST pr_ActivateOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1397 = _t;
			AST tmp460_AST_in = (AST)_t;
			match(_t,ActivateOp);
			_t = _t.getFirstChild();
			pr_AltstepInstance(_t);
			_t = _retTree;
			_t = __t1397;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_DeactivateStatement(AST _t) throws RecognitionException {
		
		AST pr_DeactivateStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1399 = _t;
			AST tmp461_AST_in = (AST)_t;
			match(_t,DeactivateStatement);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FunctionInstance:
			case VariableRef:
			{
				pr_ComponentOrDefaultReference(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1399;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_RepeatStatement(AST _t) throws RecognitionException {
		
		AST pr_RepeatStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1391 = _t;
			AST tmp462_AST_in = (AST)_t;
			match(_t,RepeatStatement);
			_t = _t.getFirstChild();
			AST tmp463_AST_in = (AST)_t;
			match(_t,REPEAT);
			_t = _t.getNextSibling();
			_t = __t1391;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_BreakStatement(AST _t) throws RecognitionException {
		
		AST pr_BreakStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1393 = _t;
			AST tmp464_AST_in = (AST)_t;
			match(_t,BreakStatement);
			_t = _t.getFirstChild();
			AST tmp465_AST_in = (AST)_t;
			match(_t,BREAK);
			_t = _t.getNextSibling();
			_t = __t1393;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ContinueStatement(AST _t) throws RecognitionException {
		
		AST pr_ContinueStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1395 = _t;
			AST tmp466_AST_in = (AST)_t;
			match(_t,ContinueStatement);
			_t = _t.getFirstChild();
			AST tmp467_AST_in = (AST)_t;
			match(_t,CONTINUE);
			_t = _t.getNextSibling();
			_t = __t1395;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SetLocalVerdict(AST _t) throws RecognitionException {
		
		AST pr_SetLocalVerdict_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1338 = _t;
			AST tmp468_AST_in = (AST)_t;
			match(_t,SetLocalVerdict);
			_t = _t.getFirstChild();
			pr_SingleExpression(_t);
			_t = _retTree;
			{
			_loop1340:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==LogItem)) {
					pr_LogItem(_t);
					_t = _retTree;
				}
				else {
					break _loop1340;
				}
				
			} while (true);
			}
			_t = __t1338;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_VerdictOps(AST _t) throws RecognitionException {
		
		AST pr_VerdictOps_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			pr_GetLocalVerdict(_t);
			_t = _retTree;
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_GetLocalVerdict(AST _t) throws RecognitionException {
		
		AST pr_GetLocalVerdict_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST tmp469_AST_in = (AST)_t;
			match(_t,GETVERDICT);
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_LogItem(AST _t) throws RecognitionException {
		
		AST pr_LogItem_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		String a;
		
		try {      // for error handling
			AST __t1534 = _t;
			AST tmp470_AST_in = (AST)_t;
			match(_t,LogItem);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case CSTRING:
			{
				a=pr_FreeText(_t);
				_t = _retTree;
				break;
			}
			case TemplateInstance:
			{
				pr_TemplateInstance(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1534;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ActionText(AST _t) throws RecognitionException {
		
		AST pr_ActionText_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		String a;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case CSTRING:
			{
				a=pr_FreeText(_t);
				_t = _retTree;
				break;
			}
			case Expression:
			{
				pr_Expression(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_GuardStatement(AST _t) throws RecognitionException {
		
		AST pr_GuardStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1361 = _t;
			AST tmp471_AST_in = (AST)_t;
			match(_t,GuardStatement);
			_t = _t.getFirstChild();
			pr_AltGuardChar(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case AltstepInstance:
			{
				{
				pr_AltstepInstance(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case StatementBlock:
				{
					pr_StatementBlock(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case GuardOp:
			{
				{
				pr_GuardOp(_t);
				_t = _retTree;
				pr_StatementBlock(_t);
				_t = _retTree;
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1361;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ElseStatement(AST _t) throws RecognitionException {
		
		AST pr_ElseStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1367 = _t;
			AST tmp472_AST_in = (AST)_t;
			match(_t,ElseStatement);
			_t = _t.getFirstChild();
			pr_StatementBlock(_t);
			_t = _retTree;
			_t = __t1367;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_GuardOp(AST _t) throws RecognitionException {
		
		AST pr_GuardOp_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1373 = _t;
			AST tmp473_AST_in = (AST)_t;
			match(_t,GuardOp);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case TimeoutStatement:
			{
				pr_TimeoutStatement(_t);
				_t = _retTree;
				break;
			}
			case ReceiveStatement:
			{
				pr_ReceiveStatement(_t);
				_t = _retTree;
				break;
			}
			case TriggerStatement:
			{
				pr_TriggerStatement(_t);
				_t = _retTree;
				break;
			}
			case GetCallStatement:
			{
				pr_GetCallStatement(_t);
				_t = _retTree;
				break;
			}
			case CatchStatement:
			{
				pr_CatchStatement(_t);
				_t = _retTree;
				break;
			}
			case CheckStatement:
			{
				pr_CheckStatement(_t);
				_t = _retTree;
				break;
			}
			case GetReplyStatement:
			{
				pr_GetReplyStatement(_t);
				_t = _retTree;
				break;
			}
			case DoneStatement:
			{
				pr_DoneStatement(_t);
				_t = _retTree;
				break;
			}
			case KilledStatement:
			{
				pr_KilledStatement(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1373;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_BooleanExpression(AST _t) throws RecognitionException {
		
		AST pr_BooleanExpression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1451 = _t;
			AST tmp474_AST_in = (AST)_t;
			match(_t,BooleanExpression);
			_t = _t.getFirstChild();
			pr_SingleExpression(_t);
			_t = _retTree;
			_t = __t1451;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_InterleavedGuardElement(AST _t) throws RecognitionException {
		
		AST pr_InterleavedGuardElement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1381 = _t;
			AST tmp475_AST_in = (AST)_t;
			match(_t,InterleavedGuardElement);
			_t = _t.getFirstChild();
			pr_InterleavedGuard(_t);
			_t = _retTree;
			pr_InterleavedAction(_t);
			_t = _retTree;
			_t = __t1381;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_InterleavedGuard(AST _t) throws RecognitionException {
		
		AST pr_InterleavedGuard_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1383 = _t;
			AST tmp476_AST_in = (AST)_t;
			match(_t,InterleavedGuard);
			_t = _t.getFirstChild();
			pr_GuardOp(_t);
			_t = _retTree;
			_t = __t1383;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_InterleavedAction(AST _t) throws RecognitionException {
		
		AST pr_InterleavedAction_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1385 = _t;
			AST tmp477_AST_in = (AST)_t;
			match(_t,InterleavedAction);
			_t = _t.getFirstChild();
			pr_StatementBlock(_t);
			_t = _retTree;
			_t = __t1385;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Assignment(AST _t) throws RecognitionException {
		
		AST pr_Assignment_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1467 = _t;
			AST tmp478_AST_in = (AST)_t;
			match(_t,Assignment);
			_t = _t.getFirstChild();
			pr_VariableRef(_t);
			_t = _retTree;
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Expression:
			{
				pr_Expression(_t);
				_t = _retTree;
				break;
			}
			case TemplateBody:
			{
				pr_TemplateBody(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1467;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_LogStatement(AST _t) throws RecognitionException {
		
		AST pr_LogStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1537 = _t;
			AST tmp479_AST_in = (AST)_t;
			match(_t,LogStatement);
			_t = _t.getFirstChild();
			{
			pr_LogItem(_t);
			_t = _retTree;
			{
			_loop1540:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==LogItem)) {
					pr_LogItem(_t);
					_t = _retTree;
				}
				else {
					break _loop1540;
				}
				
			} while (true);
			}
			}
			_t = __t1537;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_LoopConstruct(AST _t) throws RecognitionException {
		
		AST pr_LoopConstruct_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1542 = _t;
			AST tmp480_AST_in = (AST)_t;
			match(_t,LoopConstruct);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ForStatement:
			{
				pr_ForStatement(_t);
				_t = _retTree;
				break;
			}
			case WhileStatement:
			{
				pr_WhileStatement(_t);
				_t = _retTree;
				break;
			}
			case DoWhileStatement:
			{
				pr_DoWhileStatement(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1542;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ConditionalConstruct(AST _t) throws RecognitionException {
		
		AST pr_ConditionalConstruct_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1554 = _t;
			AST tmp481_AST_in = (AST)_t;
			match(_t,ConditionalConstruct);
			_t = _t.getFirstChild();
			pr_BooleanExpression(_t);
			_t = _retTree;
			pr_StatementBlock(_t);
			_t = _retTree;
			{
			_loop1556:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ElseIfClause)) {
					pr_ElseIfClause(_t);
					_t = _retTree;
				}
				else {
					break _loop1556;
				}
				
			} while (true);
			}
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ElseClause:
			{
				pr_ElseClause(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1554;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SelectCaseConstruct(AST _t) throws RecognitionException {
		
		AST pr_SelectCaseConstruct_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1406 = _t;
			AST tmp482_AST_in = (AST)_t;
			match(_t,SelectCaseConstruct);
			_t = _t.getFirstChild();
			{
			pr_SingleExpression(_t);
			_t = _retTree;
			pr_SelectCaseBody(_t);
			_t = _retTree;
			}
			_t = __t1406;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SelectCaseBody(AST _t) throws RecognitionException {
		
		AST pr_SelectCaseBody_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1409 = _t;
			AST tmp483_AST_in = (AST)_t;
			match(_t,SelectCaseBody);
			_t = _t.getFirstChild();
			{
			int _cnt1411=0;
			_loop1411:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==SelectCase)) {
					pr_SelectCase(_t);
					_t = _retTree;
				}
				else {
					if ( _cnt1411>=1 ) { break _loop1411; } else {throw new NoViableAltException(_t);}
				}
				
				_cnt1411++;
			} while (true);
			}
			_t = __t1409;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SelectCase(AST _t) throws RecognitionException {
		
		AST pr_SelectCase_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1413 = _t;
			AST tmp484_AST_in = (AST)_t;
			match(_t,SelectCase);
			_t = _t.getFirstChild();
			{
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case StatementBlock:
			case TemplateInstance:
			{
				{
				_loop1417:
				do {
					if (_t==null) _t=ASTNULL;
					if ((_t.getType()==TemplateInstance)) {
						pr_TemplateInstance(_t);
						_t = _retTree;
					}
					else {
						break _loop1417;
					}
					
				} while (true);
				}
				break;
			}
			case ELSE:
			{
				AST tmp485_AST_in = (AST)_t;
				match(_t,ELSE);
				_t = _t.getNextSibling();
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			pr_StatementBlock(_t);
			_t = _retTree;
			}
			_t = __t1413;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CompoundExpression(AST _t) throws RecognitionException {
		
		AST pr_CompoundExpression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1422 = _t;
			AST tmp486_AST_in = (AST)_t;
			match(_t,CompoundExpression);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FieldExpressionList:
			{
				pr_FieldExpressionList(_t);
				_t = _retTree;
				break;
			}
			case ArrayExpression:
			{
				pr_ArrayExpression(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1422;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FieldExpressionList(AST _t) throws RecognitionException {
		
		AST pr_FieldExpressionList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1425 = _t;
			AST tmp487_AST_in = (AST)_t;
			match(_t,FieldExpressionList);
			_t = _t.getFirstChild();
			{
			_loop1427:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FieldExpressionSpec)) {
					pr_FieldExpressionSpec(_t);
					_t = _retTree;
				}
				else {
					break _loop1427;
				}
				
			} while (true);
			}
			_t = __t1425;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ArrayExpression(AST _t) throws RecognitionException {
		
		AST pr_ArrayExpression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1434 = _t;
			AST tmp488_AST_in = (AST)_t;
			match(_t,ArrayExpression);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ArrayElementExpressionList:
			{
				pr_ArrayElementExpressionList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1434;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FieldExpressionSpec(AST _t) throws RecognitionException {
		
		AST pr_FieldExpressionSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1429 = _t;
			AST tmp489_AST_in = (AST)_t;
			match(_t,FieldExpressionSpec);
			_t = _t.getFirstChild();
			pr_FieldReference(_t);
			_t = _retTree;
			AST __t1430 = _t;
			AST tmp490_AST_in = (AST)_t;
			match(_t,NotUsedOrExpression);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Expression:
			{
				pr_Expression(_t);
				_t = _retTree;
				break;
			}
			case NotUsedSymbol:
			{
				{
				AST tmp491_AST_in = (AST)_t;
				match(_t,NotUsedSymbol);
				_t = _t.getNextSibling();
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1430;
			_t = _t.getNextSibling();
			_t = __t1429;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ArrayElementExpressionList(AST _t) throws RecognitionException {
		
		AST pr_ArrayElementExpressionList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1437 = _t;
			AST tmp492_AST_in = (AST)_t;
			match(_t,ArrayElementExpressionList);
			_t = _t.getFirstChild();
			{
			{
			_loop1442:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==NotUsedOrExpression)) {
					AST __t1440 = _t;
					AST tmp493_AST_in = (AST)_t;
					match(_t,NotUsedOrExpression);
					_t = _t.getFirstChild();
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case Expression:
					{
						pr_Expression(_t);
						_t = _retTree;
						break;
					}
					case NotUsedSymbol:
					{
						AST tmp494_AST_in = (AST)_t;
						match(_t,NotUsedSymbol);
						_t = _t.getNextSibling();
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
					_t = __t1440;
					_t = _t.getNextSibling();
				}
				else {
					break _loop1442;
				}
				
			} while (true);
			}
			}
			_t = __t1437;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_CompoundConstExpression(AST _t) throws RecognitionException {
		
		AST pr_CompoundConstExpression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case FieldConstExpressionList:
			{
				pr_FieldConstExpressionList(_t);
				_t = _retTree;
				break;
			}
			case ArrayConstExpression:
			{
				pr_ArrayConstExpression(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_InnerConstantExpression(AST _t) throws RecognitionException {
		
		AST pr_InnerConstantExpression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1447 = _t;
			AST tmp495_AST_in = (AST)_t;
			match(_t,ConstantExpression);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case SingleExpression:
			{
				pr_SingleConstExpression(_t);
				_t = _retTree;
				break;
			}
			case ArrayConstExpression:
			case FieldConstExpressionList:
			{
				pr_CompoundConstExpression(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1447;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FieldConstExpressionList(AST _t) throws RecognitionException {
		
		AST pr_FieldConstExpressionList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1454 = _t;
			AST tmp496_AST_in = (AST)_t;
			match(_t,FieldConstExpressionList);
			_t = _t.getFirstChild();
			{
			_loop1456:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==FieldConstExpressionSpec)) {
					pr_FieldConstExpressionSpec(_t);
					_t = _retTree;
				}
				else {
					break _loop1456;
				}
				
			} while (true);
			}
			_t = __t1454;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ArrayConstExpression(AST _t) throws RecognitionException {
		
		AST pr_ArrayConstExpression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1460 = _t;
			AST tmp497_AST_in = (AST)_t;
			match(_t,ArrayConstExpression);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ArrayElementConstExpressionList:
			{
				pr_ArrayElementConstExpressionList(_t);
				_t = _retTree;
				break;
			}
			case 3:
			{
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1460;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_FieldConstExpressionSpec(AST _t) throws RecognitionException {
		
		AST pr_FieldConstExpressionSpec_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1458 = _t;
			AST tmp498_AST_in = (AST)_t;
			match(_t,FieldConstExpressionSpec);
			_t = _t.getFirstChild();
			pr_FieldReference(_t);
			_t = _retTree;
			pr_InnerConstantExpression(_t);
			_t = _retTree;
			_t = __t1458;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ArrayElementConstExpressionList(AST _t) throws RecognitionException {
		
		AST pr_ArrayElementConstExpressionList_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1463 = _t;
			AST tmp499_AST_in = (AST)_t;
			match(_t,ArrayElementConstExpressionList);
			_t = _t.getFirstChild();
			{
			_loop1465:
			do {
				if (_t==null) _t=ASTNULL;
				if ((_t.getType()==ConstantExpression)) {
					pr_InnerConstantExpression(_t);
					_t = _retTree;
				}
				else {
					break _loop1465;
				}
				
			} while (true);
			}
			_t = __t1463;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SimpleExpressions(AST _t) throws RecognitionException {
		
		AST pr_SimpleExpressions_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			boolean synPredMatched1473 = false;
			if (_t==null) _t=ASTNULL;
			if (((_tokenSet_3.member(_t.getType())))) {
				AST __t1473 = _t;
				synPredMatched1473 = true;
				inputState.guessing++;
				try {
					{
					if (_t==null) _t=ASTNULL;
					switch ( _t.getType()) {
					case AND4B:
					{
						AST tmp500_AST_in = (AST)_t;
						match(_t,AND4B);
						_t = _t.getNextSibling();
						break;
					}
					case XOR4B:
					{
						AST tmp501_AST_in = (AST)_t;
						match(_t,XOR4B);
						_t = _t.getNextSibling();
						break;
					}
					case OR4B:
					{
						AST tmp502_AST_in = (AST)_t;
						match(_t,OR4B);
						_t = _t.getNextSibling();
						break;
					}
					case AND:
					{
						AST tmp503_AST_in = (AST)_t;
						match(_t,AND);
						_t = _t.getNextSibling();
						break;
					}
					case XOR:
					{
						AST tmp504_AST_in = (AST)_t;
						match(_t,XOR);
						_t = _t.getNextSibling();
						break;
					}
					case OR:
					{
						AST tmp505_AST_in = (AST)_t;
						match(_t,OR);
						_t = _t.getNextSibling();
						break;
					}
					case STRINGOP:
					{
						AST tmp506_AST_in = (AST)_t;
						match(_t,STRINGOP);
						_t = _t.getNextSibling();
						break;
					}
					case SHIFTOP:
					{
						AST tmp507_AST_in = (AST)_t;
						match(_t,SHIFTOP);
						_t = _t.getNextSibling();
						break;
					}
					case EQUAL:
					{
						AST tmp508_AST_in = (AST)_t;
						match(_t,EQUAL);
						_t = _t.getNextSibling();
						break;
					}
					default:
					{
						throw new NoViableAltException(_t);
					}
					}
					}
				}
				catch (RecognitionException pe) {
					synPredMatched1473 = false;
				}
				_t = __t1473;
inputState.guessing--;
			}
			if ( synPredMatched1473 ) {
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case AND4B:
				{
					AST __t1475 = _t;
					AST tmp509_AST_in = (AST)_t;
					match(_t,AND4B);
					_t = _t.getFirstChild();
					pr_SimpleExpressions(_t);
					_t = _retTree;
					pr_SimpleExpressions(_t);
					_t = _retTree;
					_t = __t1475;
					_t = _t.getNextSibling();
					break;
				}
				case XOR4B:
				{
					AST __t1476 = _t;
					AST tmp510_AST_in = (AST)_t;
					match(_t,XOR4B);
					_t = _t.getFirstChild();
					pr_SimpleExpressions(_t);
					_t = _retTree;
					pr_SimpleExpressions(_t);
					_t = _retTree;
					_t = __t1476;
					_t = _t.getNextSibling();
					break;
				}
				case OR4B:
				{
					AST __t1477 = _t;
					AST tmp511_AST_in = (AST)_t;
					match(_t,OR4B);
					_t = _t.getFirstChild();
					pr_SimpleExpressions(_t);
					_t = _retTree;
					pr_SimpleExpressions(_t);
					_t = _retTree;
					_t = __t1477;
					_t = _t.getNextSibling();
					break;
				}
				case AND:
				{
					AST __t1478 = _t;
					AST tmp512_AST_in = (AST)_t;
					match(_t,AND);
					_t = _t.getFirstChild();
					pr_SimpleExpressions(_t);
					_t = _retTree;
					pr_SimpleExpressions(_t);
					_t = _retTree;
					_t = __t1478;
					_t = _t.getNextSibling();
					break;
				}
				case OR:
				{
					AST __t1479 = _t;
					AST tmp513_AST_in = (AST)_t;
					match(_t,OR);
					_t = _t.getFirstChild();
					pr_SimpleExpressions(_t);
					_t = _retTree;
					pr_SimpleExpressions(_t);
					_t = _retTree;
					_t = __t1479;
					_t = _t.getNextSibling();
					break;
				}
				case XOR:
				{
					AST __t1480 = _t;
					AST tmp514_AST_in = (AST)_t;
					match(_t,XOR);
					_t = _t.getFirstChild();
					pr_SimpleExpressions(_t);
					_t = _retTree;
					pr_SimpleExpressions(_t);
					_t = _retTree;
					_t = __t1480;
					_t = _t.getNextSibling();
					break;
				}
				case STRINGOP:
				{
					AST __t1481 = _t;
					AST tmp515_AST_in = (AST)_t;
					match(_t,STRINGOP);
					_t = _t.getFirstChild();
					pr_SimpleExpressions(_t);
					_t = _retTree;
					pr_SimpleExpressions(_t);
					_t = _retTree;
					_t = __t1481;
					_t = _t.getNextSibling();
					break;
				}
				case EQUAL:
				{
					AST __t1482 = _t;
					AST tmp516_AST_in = (AST)_t;
					match(_t,EQUAL);
					_t = _t.getFirstChild();
					pr_SimpleExpressions(_t);
					_t = _retTree;
					pr_SimpleExpressions(_t);
					_t = _retTree;
					_t = __t1482;
					_t = _t.getNextSibling();
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
			}
			else if ((_tokenSet_4.member(_t.getType()))) {
				pr_SimpleExpression(_t);
				_t = _retTree;
			}
			else {
				throw new NoViableAltException(_t);
			}
			
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SimpleExpression(AST _t) throws RecognitionException {
		
		AST pr_SimpleExpression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Factor:
			case MOD:
			case REM:
			case PLUS:
			case MINUS:
			case STAR:
			case SLASH:
			case SHIFTLEFT:
			case SHIFTRIGHT:
			case ROTATELEFT:
			case ROTATERIGHT:
			case STRINGOP:
			{
				pr_SubExpression(_t);
				_t = _retTree;
				break;
			}
			case LESSTHAN:
			case MORETHAN:
			case NOTEQUALS:
			case MOREOREQUAL:
			case LESSOREQUAL:
			{
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case LESSTHAN:
				{
					AST __t1485 = _t;
					AST tmp517_AST_in = (AST)_t;
					match(_t,LESSTHAN);
					_t = _t.getFirstChild();
					pr_SubExpression(_t);
					_t = _retTree;
					pr_SubExpression(_t);
					_t = _retTree;
					_t = __t1485;
					_t = _t.getNextSibling();
					break;
				}
				case MORETHAN:
				{
					AST __t1486 = _t;
					AST tmp518_AST_in = (AST)_t;
					match(_t,MORETHAN);
					_t = _t.getFirstChild();
					pr_SubExpression(_t);
					_t = _retTree;
					pr_SubExpression(_t);
					_t = _retTree;
					_t = __t1486;
					_t = _t.getNextSibling();
					break;
				}
				case NOTEQUALS:
				{
					AST __t1487 = _t;
					AST tmp519_AST_in = (AST)_t;
					match(_t,NOTEQUALS);
					_t = _t.getFirstChild();
					pr_SubExpression(_t);
					_t = _retTree;
					pr_SubExpression(_t);
					_t = _retTree;
					_t = __t1487;
					_t = _t.getNextSibling();
					break;
				}
				case MOREOREQUAL:
				{
					AST __t1488 = _t;
					AST tmp520_AST_in = (AST)_t;
					match(_t,MOREOREQUAL);
					_t = _t.getFirstChild();
					pr_SubExpression(_t);
					_t = _retTree;
					pr_SubExpression(_t);
					_t = _retTree;
					_t = __t1488;
					_t = _t.getNextSibling();
					break;
				}
				case LESSOREQUAL:
				{
					AST __t1489 = _t;
					AST tmp521_AST_in = (AST)_t;
					match(_t,LESSOREQUAL);
					_t = _t.getFirstChild();
					pr_SubExpression(_t);
					_t = _retTree;
					pr_SubExpression(_t);
					_t = _retTree;
					_t = __t1489;
					_t = _t.getNextSibling();
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_SubExpression(AST _t) throws RecognitionException {
		
		AST pr_SubExpression_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Factor:
			case MOD:
			case REM:
			case PLUS:
			case MINUS:
			case STAR:
			case SLASH:
			case STRINGOP:
			{
				pr_Product(_t);
				_t = _retTree;
				break;
			}
			case SHIFTLEFT:
			case SHIFTRIGHT:
			case ROTATELEFT:
			case ROTATERIGHT:
			{
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case SHIFTLEFT:
				{
					AST __t1492 = _t;
					AST tmp522_AST_in = (AST)_t;
					match(_t,SHIFTLEFT);
					_t = _t.getFirstChild();
					pr_Product(_t);
					_t = _retTree;
					pr_SubExpression(_t);
					_t = _retTree;
					_t = __t1492;
					_t = _t.getNextSibling();
					break;
				}
				case SHIFTRIGHT:
				{
					AST __t1493 = _t;
					AST tmp523_AST_in = (AST)_t;
					match(_t,SHIFTRIGHT);
					_t = _t.getFirstChild();
					pr_Product(_t);
					_t = _retTree;
					pr_SubExpression(_t);
					_t = _retTree;
					_t = __t1493;
					_t = _t.getNextSibling();
					break;
				}
				case ROTATELEFT:
				{
					AST __t1494 = _t;
					AST tmp524_AST_in = (AST)_t;
					match(_t,ROTATELEFT);
					_t = _t.getFirstChild();
					pr_Product(_t);
					_t = _retTree;
					pr_SubExpression(_t);
					_t = _retTree;
					_t = __t1494;
					_t = _t.getNextSibling();
					break;
				}
				case ROTATERIGHT:
				{
					AST __t1495 = _t;
					AST tmp525_AST_in = (AST)_t;
					match(_t,ROTATERIGHT);
					_t = _t.getFirstChild();
					pr_Product(_t);
					_t = _retTree;
					pr_SubExpression(_t);
					_t = _retTree;
					_t = __t1495;
					_t = _t.getNextSibling();
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Product(AST _t) throws RecognitionException {
		
		AST pr_Product_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Factor:
			case MOD:
			case REM:
			case STAR:
			case SLASH:
			{
				pr_Term(_t);
				_t = _retTree;
				break;
			}
			case PLUS:
			case MINUS:
			case STRINGOP:
			{
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case PLUS:
				{
					AST __t1498 = _t;
					AST tmp526_AST_in = (AST)_t;
					match(_t,PLUS);
					_t = _t.getFirstChild();
					pr_Product(_t);
					_t = _retTree;
					pr_Product(_t);
					_t = _retTree;
					_t = __t1498;
					_t = _t.getNextSibling();
					break;
				}
				case MINUS:
				{
					AST __t1499 = _t;
					AST tmp527_AST_in = (AST)_t;
					match(_t,MINUS);
					_t = _t.getFirstChild();
					pr_Product(_t);
					_t = _retTree;
					pr_Product(_t);
					_t = _retTree;
					_t = __t1499;
					_t = _t.getNextSibling();
					break;
				}
				case STRINGOP:
				{
					AST __t1500 = _t;
					AST tmp528_AST_in = (AST)_t;
					match(_t,STRINGOP);
					_t = _t.getFirstChild();
					pr_Product(_t);
					_t = _retTree;
					pr_Product(_t);
					_t = _retTree;
					_t = __t1500;
					_t = _t.getNextSibling();
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Term(AST _t) throws RecognitionException {
		
		AST pr_Term_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Factor:
			{
				pr_Factor(_t);
				_t = _retTree;
				break;
			}
			case MOD:
			case REM:
			case STAR:
			case SLASH:
			{
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case STAR:
				{
					AST __t1503 = _t;
					AST tmp529_AST_in = (AST)_t;
					match(_t,STAR);
					_t = _t.getFirstChild();
					pr_Term(_t);
					_t = _retTree;
					pr_Term(_t);
					_t = _retTree;
					_t = __t1503;
					_t = _t.getNextSibling();
					break;
				}
				case SLASH:
				{
					AST __t1504 = _t;
					AST tmp530_AST_in = (AST)_t;
					match(_t,SLASH);
					_t = _t.getFirstChild();
					pr_Term(_t);
					_t = _retTree;
					pr_Term(_t);
					_t = _retTree;
					_t = __t1504;
					_t = _t.getNextSibling();
					break;
				}
				case MOD:
				{
					AST __t1505 = _t;
					AST tmp531_AST_in = (AST)_t;
					match(_t,MOD);
					_t = _t.getFirstChild();
					pr_Term(_t);
					_t = _retTree;
					pr_Term(_t);
					_t = _retTree;
					_t = __t1505;
					_t = _t.getNextSibling();
					break;
				}
				case REM:
				{
					AST __t1506 = _t;
					AST tmp532_AST_in = (AST)_t;
					match(_t,REM);
					_t = _t.getFirstChild();
					pr_Term(_t);
					_t = _retTree;
					pr_Term(_t);
					_t = _retTree;
					_t = __t1506;
					_t = _t.getNextSibling();
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Factor(AST _t) throws RecognitionException {
		
		AST pr_Factor_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1508 = _t;
			AST tmp533_AST_in = (AST)_t;
			match(_t,Factor);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case Primary:
			{
				pr_Primary(_t);
				_t = _retTree;
				break;
			}
			case NOT:
			case NOT4B:
			case PLUS:
			case MINUS:
			{
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case PLUS:
				{
					AST __t1511 = _t;
					AST tmp534_AST_in = (AST)_t;
					match(_t,PLUS);
					_t = _t.getFirstChild();
					pr_Primary(_t);
					_t = _retTree;
					_t = __t1511;
					_t = _t.getNextSibling();
					break;
				}
				case MINUS:
				{
					AST __t1512 = _t;
					AST tmp535_AST_in = (AST)_t;
					match(_t,MINUS);
					_t = _t.getFirstChild();
					pr_Primary(_t);
					_t = _retTree;
					_t = __t1512;
					_t = _t.getNextSibling();
					break;
				}
				case NOT:
				{
					AST __t1513 = _t;
					AST tmp536_AST_in = (AST)_t;
					match(_t,NOT);
					_t = _t.getFirstChild();
					pr_Primary(_t);
					_t = _retTree;
					_t = __t1513;
					_t = _t.getNextSibling();
					break;
				}
				case NOT4B:
				{
					AST __t1514 = _t;
					AST tmp537_AST_in = (AST)_t;
					match(_t,NOT4B);
					_t = _t.getFirstChild();
					pr_Primary(_t);
					_t = _retTree;
					_t = __t1514;
					_t = _t.getNextSibling();
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1508;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_Primary(AST _t) throws RecognitionException {
		
		AST pr_Primary_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1516 = _t;
			AST tmp538_AST_in = (AST)_t;
			match(_t,Primary);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case OpCall:
			{
				pr_OpCall(_t);
				_t = _retTree;
				break;
			}
			case Value:
			{
				pr_Value(_t);
				_t = _retTree;
				break;
			}
			case SingleExpression:
			{
				pr_SingleExpression(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1516;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_OpCall(AST _t) throws RecognitionException {
		
		AST pr_OpCall_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		List<Symbol> c;
		
		try {      // for error handling
			AST __t1527 = _t;
			AST tmp539_AST_in = (AST)_t;
			match(_t,OpCall);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case ConfigurationOps:
			{
				pr_ConfigurationOps(_t);
				_t = _retTree;
				break;
			}
			case GETVERDICT:
			{
				pr_VerdictOps(_t);
				_t = _retTree;
				break;
			}
			case TimerOps:
			{
				pr_TimerOps(_t);
				_t = _retTree;
				break;
			}
			case TestcaseInstance:
			{
				pr_TestcaseInstance(_t);
				_t = _retTree;
				break;
			}
			case FunctionInstance:
			{
				{
				pr_FunctionInstance(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case ExtendedFieldReference:
				{
					c=pr_ExtendedFieldReference(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case TemplateOps:
			{
				{
				pr_TemplateOps(_t);
				_t = _retTree;
				{
				if (_t==null) _t=ASTNULL;
				switch ( _t.getType()) {
				case ExtendedFieldReference:
				{
					c=pr_ExtendedFieldReference(_t);
					_t = _retTree;
					break;
				}
				case 3:
				{
					break;
				}
				default:
				{
					throw new NoViableAltException(_t);
				}
				}
				}
				}
				break;
			}
			case ActivateOp:
			{
				pr_ActivateOp(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			_t = __t1527;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ForStatement(AST _t) throws RecognitionException {
		
		AST pr_ForStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1545 = _t;
			AST tmp540_AST_in = (AST)_t;
			match(_t,ForStatement);
			_t = _t.getFirstChild();
			{
			if (_t==null) _t=ASTNULL;
			switch ( _t.getType()) {
			case VarInstance:
			{
				pr_VarInstance(_t);
				_t = _retTree;
				break;
			}
			case Assignment:
			{
				pr_Assignment(_t);
				_t = _retTree;
				break;
			}
			default:
			{
				throw new NoViableAltException(_t);
			}
			}
			}
			pr_BooleanExpression(_t);
			_t = _retTree;
			pr_Assignment(_t);
			_t = _retTree;
			pr_StatementBlock(_t);
			_t = _retTree;
			_t = __t1545;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_WhileStatement(AST _t) throws RecognitionException {
		
		AST pr_WhileStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1548 = _t;
			AST tmp541_AST_in = (AST)_t;
			match(_t,WhileStatement);
			_t = _t.getFirstChild();
			{
			pr_BooleanExpression(_t);
			_t = _retTree;
			pr_StatementBlock(_t);
			_t = _retTree;
			}
			_t = __t1548;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_DoWhileStatement(AST _t) throws RecognitionException {
		
		AST pr_DoWhileStatement_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1551 = _t;
			AST tmp542_AST_in = (AST)_t;
			match(_t,DoWhileStatement);
			_t = _t.getFirstChild();
			{
			pr_StatementBlock(_t);
			_t = _retTree;
			pr_BooleanExpression(_t);
			_t = _retTree;
			}
			_t = __t1551;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ElseIfClause(AST _t) throws RecognitionException {
		
		AST pr_ElseIfClause_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1559 = _t;
			AST tmp543_AST_in = (AST)_t;
			match(_t,ElseIfClause);
			_t = _t.getFirstChild();
			{
			pr_BooleanExpression(_t);
			_t = _retTree;
			pr_StatementBlock(_t);
			_t = _retTree;
			}
			_t = __t1559;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_ElseClause(AST _t) throws RecognitionException {
		
		AST pr_ElseClause_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		
		try {      // for error handling
			AST __t1562 = _t;
			AST tmp544_AST_in = (AST)_t;
			match(_t,ElseClause);
			_t = _t.getFirstChild();
			{
			pr_StatementBlock(_t);
			_t = _retTree;
			}
			_t = __t1562;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	public final void pr_InOutParKeyword(AST _t) throws RecognitionException {
		
		AST pr_InOutParKeyword_AST_in = (_t == ASTNULL) ? null : (AST)_t;
		AST a = null;
		
		try {      // for error handling
			AST __t1565 = _t;
			AST tmp545_AST_in = (AST)_t;
			match(_t,InOutParKeyword);
			_t = _t.getFirstChild();
			a = (AST)_t;
			match(_t,INOUT);
			_t = _t.getNextSibling();
			_t = __t1565;
			_t = _t.getNextSibling();
		}
		catch (RecognitionException ex) {
			if (inputState.guessing==0) {
				reportError(ex);
				if (_t!=null) {_t = _t.getNextSibling();}
			} else {
			  throw ex;
			}
		}
		_retTree = _t;
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"AddExpression",
		"AddressRefList",
		"AliveKeyword",
		"AliveOp",
		"AllAltstepsWithExcept",
		"AllCompsAllPortsSpec",
		"AllConnectionsSpec",
		"AllConstsWithExcept",
		"AllFunctionsWithExcept",
		"AllGroupsWithExcept",
		"AllModuleParWithExcept",
		"AllPortsSpec",
		"AllSignaturesWithExcept",
		"AllTemplsWithExcept",
		"AllTestcasesWithExcept",
		"AllTypesWithExcept",
		"AllWithExcepts",
		"AltstepInstance",
		"AltstepLocalDef",
		"AltstepLocalDefList",
		"AltstepRef",
		"AltstepRefList",
		"AndExpression",
		"AnyTypeKeyword",
		"AllRef",
		"BitAndExpression",
		"BitNotExpression",
		"BitOrExpression",
		"BitXorExpression",
		"BreakStatement",
		"CaseKeyword",
		"CheckPortOpsPresent",
		"CompenentIdentifierOrLiteral",
		"ComponentIdentifierOrLiteral",
		"ComponentReferenceOrLiteral",
		"ConstRefList",
		"ContinueStatement",
		"DefaultKeyword",
		"DerivedRefWithParList",
		"EqualExpression",
		"ExceptAltstepSpec",
		"ExceptConstSpec",
		"ExceptElement",
		"ExceptFunctionSpec",
		"ExceptGroupRefList",
		"ExceptGroupSpec",
		"ExceptModuleParSpec",
		"ExceptSignatureSpec",
		"ExceptSpec",
		"ExceptTemplateSpec",
		"ExceptTestcaseSpec",
		"ExceptTypeDefSpec",
		"ExceptsDef",
		"ExtendsKeyword",
		"FloatDotNotation",
		"FloatENotation",
		"FromClausePresent",
		"FullGroupIdentifier",
		"FullGroupIdentifierWithExcept",
		"FunctionRefList",
		"GroupRefList",
		"GroupRefListWithExcept",
		"ImportAltstepSpec",
		"ImportModuleParSpec",
		"KillKeyword",
		"KillTCStatement",
		"KilledStatement",
		"LogItem",
		"ModuleDefinitionList",
		"ModuleParRefList",
		"MulExpression",
		"NestedEnumDef",
		"NestedRecordDef",
		"NestedRecordOfDef",
		"NestedSetDef",
		"NestedSetOfDef",
		"NestedTypeDef",
		"NestedUnionDef",
		"NotExpression",
		"OmitValue",
		"PortRedirectSymbol",
		"PortRedirectWithValueAndParam",
		"RedirectPresent",
		"RedirectWithParamSpec",
		"RedirectWithValueAndParamSpec",
		"RelExpression",
		"RepeatStatement",
		"RestrictedTemplate",
		"SelectCase",
		"SelectCaseBody",
		"SelectCaseConstruct",
		"SelectKeyword",
		"ShiftExpression",
		"SignatureRefList",
		"SingleConnectionSpec",
		"SingleOrMultiConnectionSpec",
		"SingleTimerInstance",
		"TempVarList",
		"TemplateRefList",
		"TemplateRestriction",
		"TestcaseRefList",
		"TimerList",
		"TypeRefList",
		"UnaryExpression",
		"NameForm",
		"NumberForm",
		"NameAndNumberForm",
		"VariationKeyword",
		"DefinitiveIdentifier",
		"DefinitiveNumberForm",
		"XorExpression",
		"DefinitiveNameAndNumberForm",
		"ActivateOp",
		"AddOp",
		"AddressKeyword",
		"AddressDecl",
		"AddressRef",
		"AddressValue",
		"AllKeyword",
		"AllElementsFrom",
		"AllOrSignatureList",
		"AllOrTypeList",
		"AllPort",
		"AllowedValues",
		"AltConstruct",
		"AltGuardChar",
		"AltGuardElement",
		"AltGuardElements",
		"AltGuardList",
		"AltKeyword",
		"AltstepDef",
		"AltstepKeyword",
		"And4b",
		"And",
		"AnyKeyword",
		"AnyOrOmit",
		"AnyValue",
		"ArrayBounds",
		"ArrayConstExpression",
		"ArrayDef",
		"ArrayDefRange",
		"ArrayElementConstExpressionList",
		"ArrayElementExpressionList",
		"ArrayElementSpec",
		"ArrayElementSpecList",
		"ArrayExpression",
		"ArrayOrBitRef",
		"ArrayValueOrAttrib",
		"Assignment",
		"AssignmentChar",
		"AssignmentList",
		"AttribKeyword",
		"AttribQualifier",
		"AttribSpec",
		"BaseTemplate",
		"BasicStatements",
		"BeginChar",
		"BehaviourStatements",
		"BinOrMatch",
		"BitOp",
		"BitStringKeyword",
		"BitStringMatch",
		"BitStringValue",
		"BooleanExpression",
		"BooleanKeyword",
		"BooleanValue",
		"CallBodyGuard",
		"CallBodyOps",
		"CallBodyStatement",
		"CallBodyStatementList",
		"CallParameters",
		"CallStatement",
		"CallTimerValue",
		"CatchOpParameter",
		"CatchStatement",
		"Cell",
		"CharKeyword",
		"CharStringKeyword",
		"CharStringMatch",
		"CharStringPattern",
		"CharStringValue",
		"CharStringValueId",
		"CheckParameter",
		"CheckStatement",
		"CheckStateStatement",
		"ClearStatement",
		"Colon",
		"Comma",
		"CommunicationStatements",
		"Complement",
		"ComponentDef",
		"ComponentDefList",
		"ComponentElementDef",
		"ComponentId",
		"ComponentIdentifier",
		"ComponentRef",
		"ComponentType",
		"ComponentRefAssignment",
		"CompoundConstExpression",
		"CompoundExpression",
		"ConditionalConstruct",
		"ConfigParamDef",
		"ConfigSpec",
		"ConfigurationOps",
		"ConfigurationStatements",
		"ConnectStatement",
		"ConstDef",
		"ConstList",
		"ConstantExpression",
		"ControlStatement",
		"ControlStatementOrDef",
		"ControlStatementOrDefList",
		"CreateOp",
		"Cstring",
		"Dash",
		"DeactivateStatement",
		"DefIndentClose",
		"DefIndentOpen",
		"DefKeyword",
		"DefOrFieldRef",
		"DefOrFieldRefList",
		"DerivedDef",
		"DerivedRegWithParList",
		"Direction",
		"DisconnectStatement",
		"DisplayKeyword",
		"DoWhileStatement",
		"DoneStatement",
		"Dot",
		"DoubleColon",
		"ElseClause",
		"ElseIfClause",
		"ElseIfClauses",
		"ElseStatement",
		"EncodeKeyword",
		"EndChar",
		"EnumDef",
		"EnumeratedValue",
		"Enumeration",
		"EnumerationList",
		"Equal",
		"Error",
		"ExceptionSpec",
		"ExceptionTypeList",
		"ExclamationMark",
		"Expression",
		"ExtConstDef",
		"ExtFunctionDef",
		"ExtendedFieldReference",
		"ExtensionKeyword",
		"ExtraMatchingAttributes",
		"Factor",
		"Fail",
		"False",
		"FieldConstExpressionList",
		"FieldConstExpressionSpec",
		"FieldExpressionList",
		"FieldExpressionSpec",
		"FieldReference",
		"FieldSpec",
		"FieldSpecList",
		"FieldValueOrAttrib",
		"Final",
		"FloatKeyword",
		"FloatValue",
		"ForStatement",
		"FormalPortPar",
		"FormalTemplatePar",
		"FormalTimerPar",
		"FormalTypePar",
		"FormalValuePar",
		"FromClause",
		"FunctionActualParAssignment",
		"FriendKeyword",
		"FriendModuleDef",
		"FunctionActualPar",
		"FunctionActualParList",
		"FunctionBody",
		"FunctionDef",
		"FunctionLocalDefOrInst",
		"FunctionLocalDefOrInstList",
		"FunctionStatementList",
		"FunctionFormalPar",
		"FunctionInstance",
		"FunctionLocalDef",
		"FunctionLocalInst",
		"FunctionRef",
		"FunctionStatement",
		"GetCallStatement",
		"GetLocalVerdict",
		"GetReplyStatement",
		"GlobalModuleId",
		"GotoStatement",
		"Group",
		"GroupDef",
		"GuardOp",
		"GuardStatement",
		"HexOrMatch",
		"HexStringKeyword",
		"HexStringMatch",
		"HexStringValue",
		"HaltStatement",
		"Identifier",
		"IfPresentMatch",
		"ImportAllSpec",
		"ImportConstSpec",
		"ImportDef",
		"ImportElement",
		"ImportFromSpec",
		"ImportFunctionSpec",
		"ImportGroupSpec",
		"ImportNamedAltSpec",
		"ImportSignatureSpec",
		"ImportSpec",
		"ImportTemplateSpec",
		"ImportTestcaseSpec",
		"ImportTypeDefSpec",
		"ImportImportSpec",
		"InLineTemplate",
		"InOutParKeyword",
		"InParKeyword",
		"Inconc",
		"Infinity",
		"Initial",
		"IntegerKeyword",
		"IntegerValue",
		"InterleavedAction",
		"InterleavedConstruct",
		"InterleavedGuard",
		"InterleavedGuardElement",
		"InterleavedGuardList",
		"KilledKeyword",
		"LParen",
		"LabelStatement",
		"LanguageSpec",
		"LengthMatch",
		"Lessorequal",
		"Lessthan",
		"ListOfTemplates",
		"LogStatement",
		"LoopConstruct",
		"LowerBound",
		"MTCOp",
		"MapParamDef",
		"MapStatement",
		"MatchOp",
		"MatchingSymbol",
		"MessageAttribs",
		"MessageList",
		"Minus",
		"MixedAttribs",
		"MixedList",
		"Mod",
		"ModifiesKeyword",
		"ModuleControlBody",
		"ModuleControlPart",
		"ModuleDefinition",
		"ModuleDefinitionsList",
		"ModuleDefinitionsPart",
		"ModuleId",
		"ModulePar",
		"ModuleParDef",
		"ModuleParList",
		"Moreorequal",
		"Morethan",
		"MultiWithAttrib",
		"MultiplyOp",
		"MultitypedModuleParList",
		"NamedAltActualPar",
		"NamedAltActualParList",
		"NamedAltDef",
		"NamedAltFormalPar",
		"NamedAltFormalParList",
		"NamedAltInstance",
		"NamedAltList",
		"NamedAltRef",
		"NamedAltRefList",
		"NamedKeyword",
		"NamedValue",
		"NamedValueList",
		"NoBlockKeyword",
		"None",
		"Not4b",
		"Not",
		"NotUsedOrExpression",
		"NotUsedSymbol",
		"Notequals",
		"NowaitKeyword",
		"Null",
		"Number",
		"NaN",
		"OctOrMatch",
		"OctetStringKeyword",
		"OctetStringMatch",
		"OctetStringValue",
		"OmitKeyword",
		"OpCall",
		"OpValue",
		"OptionalKeyword",
		"Or4b",
		"Or",
		"OutParKeyword",
		"OverrideKeyword",
		"ParRef",
		"ParaAssignmentList",
		"ParamClause",
		"ParaSpec",
		"Pass",
		"PermutationMatch",
		"Plane",
		"Plus",
		"Port",
		"PortCallBody",
		"PortCallOp",
		"PortCatchOp",
		"PortCheckOp",
		"PortClearOp",
		"PortDef",
		"PortDefAttribs",
		"PortDefBody",
		"PortElement",
		"PortGetCallOp",
		"PortGetReplyOp",
		"PortInstance",
		"PortKeyword",
		"PortOrAll",
		"PortOrAllOrAny",
		"PortOrAny",
		"PortRaiseOp",
		"PortReceiveOp",
		"PortRedirect",
		"PortRedirectWithParam",
		"PortRef",
		"PortReplyOp",
		"PortSendOp",
		"PortRefAssignment",
		"PortSpec",
		"PortStartOp",
		"PortStopOp",
		"PortTriggerOp",
		"PortType",
		"PredefinedType",
		"PredefinedValue",
		"Primary",
		"ProcOrType",
		"ProcOrTypeList",
		"ProcedureAttribs",
		"ProcedureList",
		"Product",
		"PortHaltOp",
		"PrivateKeyword",
		"PublicKeyword",
		"Quadruple",
		"QuestionMark",
		"RParen",
		"RaiseStatement",
		"Range",
		"RangeDef",
		"RangeOp",
		"ReadTimerOp",
		"ReceiveParameter",
		"ReceiveStatement",
		"RecordDef",
		"RecordOfDef",
		"RecordRef",
		"RecursiveKeyword",
		"ReferencedType",
		"ReferencedValue",
		"RelOp",
		"Rem",
		"ReplyStatement",
		"ReturnStatement",
		"ReturnType",
		"Row",
		"RunningOp",
		"RunningTimerOp",
		"RunsOnSpec",
		"SUTAction",
		"SUTStatements",
		"SelfOp",
		"SemiColon",
		"SendParameter",
		"SendStatement",
		"SenderSpec",
		"SetDef",
		"SetLocalVerdict",
		"SetOfDef",
		"ShiftOp",
		"Signature",
		"SignatureDef",
		"SignatureFormalPar",
		"SignatureFormalParList",
		"SignatureList",
		"SimpleExpression",
		"SimpleExpressions",
		"SingleConstDef",
		"SingleExpression",
		"SingleQuote",
		"SingleTempVarInstance",
		"SingleValueOrAttrib",
		"SingleVarInstance",
		"SingleValueSpec",
		"SingleWithAttrib",
		"Slash",
		"SquareClose",
		"SquareOpen",
		"Star",
		"StartStatement",
		"StartTCStatement",
		"StartTimerStatement",
		"StatementBlock",
		"Step",
		"StopStatement",
		"StopTCStatement",
		"StopTimerStatement",
		"StringAnyOrOmit",
		"StringAnyValue",
		"StringLength",
		"StringMatchOp",
		"StringOp",
		"StructDefBody",
		"StructDefFormalPar",
		"StructDefFormalParList",
		"StructFieldDef",
		"StructFieldRef",
		"StructOfDefBody",
		"StructuredTypeDef",
		"SubExpression",
		"SubTypeDef",
		"SubTypeSpec",
		"SubsetMatch",
		"SupersetMatch",
		"SystemOp",
		"SystemSpec",
		"TTCN3File",
		"TTCN3Module",
		"TTCN3ModuleId",
		"TemplateActualPar",
		"TemplateActualParList",
		"TemplateBody",
		"TemplateActualParAssignment",
		"TemplateDef",
		"TemplateFormalPar",
		"TemplateFormalParList",
		"TemplateInstanceAssignment",
		"TemplateInstance",
		"TemplateKeyword",
		"TemplateOps",
		"TemplateRef",
		"TemplateRefWithParList",
		"Term",
		"TestcaseActualParAssignment",
		"TestcaseActualPar",
		"TestcaseActualParList",
		"TestcaseDef",
		"TestcaseInstance",
		"TestcaseOperation",
		"TestcaseRef",
		"TimeoutKeyword",
		"TimeoutStatement",
		"TimerInstance",
		"TimerKeyword",
		"TimerOps",
		"TimerRef",
		"TimerRefOrAll",
		"TimerRefAssignment",
		"TimerRefOrAny",
		"TimerStatements",
		"TimerValue",
		"ToClause",
		"TriggerStatement",
		"True",
		"Type",
		"TypeActualPar",
		"TypeActualParList",
		"TypeDef",
		"TypeDefIdentifier",
		"TypeList",
		"TypeReference",
		"UnaryOp",
		"Underscore",
		"UnionDef",
		"UnionDefBody",
		"UnionFieldDef",
		"UniversalChar",
		"UniversalCharString",
		"UniversalKeyword",
		"UnmapParamDef",
		"UnmapStatement",
		"UpperBound",
		"Value",
		"ValueList",
		"ValueMatchSpec",
		"ValueOrAttribList",
		"ValueOrRange",
		"ValueParOrTypePar",
		"ValueReference",
		"ValueSpec",
		"ValueofOp",
		"VarInitialValue",
		"VarInstance",
		"VarList",
		"VariableAssignment",
		"VariableEntry",
		"VariableList",
		"VariableRef",
		"VerdictOps",
		"VerdictStatements",
		"VerdictTypeKeyword",
		"VerdictTypeValue",
		"Visibility",
		"WhileStatement",
		"WildcardLengthMatch",
		"WithAttribList",
		"WithStatement",
		"Xor4b",
		"Xor",
		"BSTRING",
		"BSTRINGMATCH",
		"DOT",
		"FLOATVALUE",
		"HSTRING",
		"HSTRINGMATCH",
		"OSTRING",
		"OSTRINGMATCH",
		"RANGEOP",
		"\"action\"",
		"\"activate\"",
		"\"address\"",
		"\"alive\"",
		"\"all\"",
		"\"alt\"",
		"\"altstep\"",
		"\"and\"",
		"\"and4b\"",
		"\"any\"",
		"\"anytype\"",
		"\"bitstring\"",
		"\"boolean\"",
		"\"break\"",
		"\"call\"",
		"\"case\"",
		"\"catch\"",
		"\"char\"",
		"\"charstring\"",
		"\"check\"",
		"\"checkstate\"",
		"\"clear\"",
		"\"complement\"",
		"\"component\"",
		"\"connect\"",
		"\"const\"",
		"\"continue\"",
		"\"control\"",
		"\"create\"",
		"\"deactivate\"",
		"\"default\"",
		"\"disconnect\"",
		"\"display\"",
		"\"do\"",
		"\"done\"",
		"\"else\"",
		"\"encode\"",
		"\"enumerated\"",
		"\"error\"",
		"\"except\"",
		"\"exception\"",
		"\"execute\"",
		"\"extends\"",
		"\"extension\"",
		"\"external\"",
		"\"fail\"",
		"\"false\"",
		"\"float\"",
		"\"for\"",
		"\"from\"",
		"\"function\"",
		"\"friend\"",
		"\"getcall\"",
		"\"getreply\"",
		"\"getverdict\"",
		"\"goto\"",
		"\"group\"",
		"\"halt\"",
		"\"hexstring\"",
		"\"if\"",
		"\"ifpresent\"",
		"\"import\"",
		"\"in\"",
		"\"inconc\"",
		"\"infinity\"",
		"\"inout\"",
		"\"integer\"",
		"\"interleave\"",
		"\"kill\"",
		"\"killed\"",
		"\"label\"",
		"\"language\"",
		"\"length\"",
		"\"log\"",
		"\"map\"",
		"\"match\"",
		"\"message\"",
		"\"mixed\"",
		"\"mod\"",
		"\"modifies\"",
		"\"module\"",
		"\"modulepar\"",
		"\"mtc\"",
		"\"not_a_number\"",
		"\"noblock\"",
		"\"none\"",
		"\"not\"",
		"\"not4b\"",
		"\"nowait\"",
		"\"null\"",
		"\"objid\"",
		"\"octetstring\"",
		"\"of\"",
		"\"omit\"",
		"\"on\"",
		"\"optional\"",
		"\"or\"",
		"\"or4b\"",
		"\"out\"",
		"\"override\"",
		"\"param\"",
		"\"pass\"",
		"\"pattern\"",
		"\"permutation\"",
		"\"port\"",
		"\"present\"",
		"\"private\"",
		"\"procedure\"",
		"\"public\"",
		"\"raise\"",
		"\"read\"",
		"\"receive\"",
		"\"record\"",
		"\"recursive\"",
		"\"rem\"",
		"\"repeat\"",
		"\"reply\"",
		"\"return\"",
		"\"running\"",
		"\"runs\"",
		"\"select\"",
		"\"self\"",
		"\"send\"",
		"\"sender\"",
		"\"set\"",
		"\"setverdict\"",
		"\"signature\"",
		"\"start\"",
		"\"stop\"",
		"\"subset\"",
		"\"superset\"",
		"\"system\"",
		"\"template\"",
		"\"testcase\"",
		"\"timeout\"",
		"\"timer\"",
		"\"to\"",
		"\"trigger\"",
		"\"true\"",
		"\"type\"",
		"\"union\"",
		"\"universal\"",
		"\"unmap\"",
		"\"value\"",
		"\"valueof\"",
		"\"var\"",
		"\"variant\"",
		"\"verdicttype\"",
		"\"while\"",
		"\"with\"",
		"\"xor\"",
		"\"xor4b\"",
		"WS",
		"SL_COMMENT",
		"ML_COMMENT",
		"an identifer",
		"an assignment char ':='",
		"a port redirect symbol '->'",
		"a begin char'{'",
		"a cstring",
		"a BIN, HEX or OCT",
		"a Number",
		"EXPONENT",
		"a semicolon ';'",
		"a comma ','",
		"a double colon '::'",
		"a colon ':'",
		"an underscore '_'",
		"an end char '}'",
		"a square bracket open '['",
		"a square bracket close ']'",
		"a left parentheses '('",
		"a right parentheses ')'",
		"a less than '<'",
		"a more than ''",
		"a not equals '!='",
		"a more or equal '>='",
		"a less or equal '<='",
		"an equals '=='",
		"a plus '+'",
		"a minus '-'",
		"a star '*'",
		"a slash '/'",
		"a single quote '''",
		"a double quote '''",
		"a question mark '?'",
		"an exclamation mark '!'",
		"a shift left operator '<<'",
		"a shift right operator '>>'",
		"a rotate left operator '<@'",
		"a rotate right operator '@>'",
		"an ANY Value String '<?>'",
		"an ANY OR OMIT Value String '<*>'",
		"a quote symbol '<\\\">'",
		"a string concat operator '&'",
		"Zero",
		"Nonzeronum",
		"Nonzeronumber",
		"Decimalnumber",
		"Num",
		"BIN",
		"HEX",
		"OCT",
		"Char",
		"GeneralTextCharacter",
		"Alphanumeric",
		"Letter",
		"UppercaseLetter",
		"LowercaseLetter",
		"Special",
		"OtherSpecial",
		"OtherCharacter",
		"Space",
		"PreProcessorMacro",
		"SHIFTOP"
	};
	
	private static final long[] mk_tokenSet_0() {
		long[] data = new long[14];
		data[2]=4194304L;
		data[4]=1125899906842624L;
		data[6]=2305843009213693984L;
		return data;
	}
	public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());
	private static final long[] mk_tokenSet_1() {
		long[] data = new long[14];
		data[4]=376832L;
		return data;
	}
	public static final BitSet _tokenSet_1 = new BitSet(mk_tokenSet_1());
	private static final long[] mk_tokenSet_2() {
		long[] data = new long[16];
		data[1]=123145302310912L;
		data[7]=8388608L;
		return data;
	}
	public static final BitSet _tokenSet_2 = new BitSet(mk_tokenSet_2());
	private static final long[] mk_tokenSet_3() {
		long[] data = new long[26];
		data[9]=6917529027641081856L;
		data[11]=12582912L;
		data[12]=72058693549568000L;
		return data;
	}
	public static final BitSet _tokenSet_3 = new BitSet(mk_tokenSet_3());
	private static final long[] mk_tokenSet_4() {
		long[] data = new long[26];
		data[3]=-9223372036854775808L;
		data[11]=1099511627792L;
		data[12]=80535893839970304L;
		return data;
	}
	public static final BitSet _tokenSet_4 = new BitSet(mk_tokenSet_4());
	}
	
