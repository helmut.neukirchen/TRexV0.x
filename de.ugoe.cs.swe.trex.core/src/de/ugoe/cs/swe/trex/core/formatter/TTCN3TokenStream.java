package de.ugoe.cs.swe.trex.core.formatter;

import antlr.Token;
import de.ugoe.cs.swe.trex.core.TTCN3CorePlugin;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TokenStreamTracker;

public class TTCN3TokenStream {
	int index = 0;

	TokenStreamTracker tracker;

	ITokenFilter filter = null;

	int backupIndex = -1;

	public enum SkipTypes {
		WHITESPACES, WHITESPACES_AND_COMMENTS
	}

	public TTCN3TokenStream(TokenStreamTracker t) {
		tracker = t;
	}

	// ------------------------------------------------------------------------------------------------------

	public TTCN3TokenStream(TokenStreamTracker t, int startIndex) {
		tracker = t;
		index = startIndex;
	}

	// ------------------------------------------------------------------------------------------------------

	public ITokenFilter getFilter() {
		return filter;
	}

	// ------------------------------------------------------------------------------------------------------

	public void setFilter(ITokenFilter filter) {
		this.filter = filter;
	}

	// ------------------------------------------------------------------------------------------------------

	public int tokenCount() {
		return tracker.getTokenCount();
	}

	// ------------------------------------------------------------------------------------------------------

	public boolean hasNextToken() {
		if ((index + 1) >= tokenCount())
			return false;
		if (index < 0)
			return false;

		return true;
	}

	// ------------------------------------------------------------------------------------------------------

	public boolean hasPreviousToken() {
		if ((index - 1) < 0)
			return false;

		return true;
	}

	// ------------------------------------------------------------------------------------------------------
	public void reset() {
		index = 0;
	}

	// ------------------------------------------------------------------------------------------------------

	public void nextToken() throws IndexOutOfBoundsException {
		if ((index + 1) >= tokenCount())
			throw new IndexOutOfBoundsException("SourceTokenStream index " + (index + 1)
					+ " is out of bounds (>= count)");

		index++;
	}

	// ------------------------------------------------------------------------------------------------------

	public void previousToken() throws IndexOutOfBoundsException {
		if (index - 1 < 0)
			throw new IndexOutOfBoundsException("SourceTokenStream index out of bounds (< 0)");

		index--;
	}

	// ------------------------------------------------------------------------------------------------------

	public Token getElement() {
		if (filter != null)
			return filter.filterToken(tracker.getToken(index));

		return tracker.getToken(index);
	}

	// ------------------------------------------------------------------------------------------------------

	public boolean isCommentToken() {
		return ((tracker.getToken(index).getType() == TTCN3LexerTokenTypes.SL_COMMENT) || (tracker
				.getToken(index).getType() == TTCN3LexerTokenTypes.ML_COMMENT));
	}

	// ------------------------------------------------------------------------------------------------------

	public boolean isPreviousTokenCommentToken() {
		int i = index - 1;
		if (i <= 0)
			return false;

		int tokenType = tracker.getToken(i).getType();
		while ((tokenType == TTCN3LexerTokenTypes.WS))  {
			i--;
			if (i <= 0)
				return false;
			tokenType = tracker.getToken(i).getType();
		}

		return ((tokenType == TTCN3LexerTokenTypes.SL_COMMENT) || (tokenType == TTCN3LexerTokenTypes.ML_COMMENT));
	}

	public boolean isNextTokenCommentToken() {
		int i = index + 1;
		if (i>=tokenCount())
			return false;

		int tokenType = tracker.getToken(i).getType();
		while ((tokenType == TTCN3LexerTokenTypes.WS))  {
			i++;
			if (i>=tokenCount())
				return false;
			tokenType = tracker.getToken(i).getType();
		}

		return ((tokenType == TTCN3LexerTokenTypes.SL_COMMENT) || (tokenType == TTCN3LexerTokenTypes.ML_COMMENT));
	}


	// ------------------------------------------------------------------------------------------------------

	public ScanResult nextTokenSkip(SkipTypes ignore) throws Exception {
		ScanResult result = new ScanResult();
		StringBuffer stringBuffer = new StringBuffer();
		StringBuffer indentationSinceNewline = new StringBuffer();
		boolean countSpacesSinceNewline = false;

		while (hasNextToken()) {
			nextToken();
			if ((ignore == SkipTypes.WHITESPACES) && (!isTokenWhitespace()))
				break;
			if ((ignore == SkipTypes.WHITESPACES_AND_COMMENTS)
					&& (!isTokenWhitespaceOrComment()))
				break;
			if (countSpacesSinceNewline && !((getElement().getText().contains("\r") || getElement().getText().contains("\n"))))
				indentationSinceNewline.append(getElement().getText());

			if (getElement().getText().contains("\r") || getElement().getText().contains("\n")) {
				countSpacesSinceNewline = true;
			}

			stringBuffer.append(getElement().getText());
		}

		result.setIndex(index);
		if (indentationSinceNewline.toString().length() > 0)
			result.setIndentationSinceNewline(indentationSinceNewline.toString());
		result.setStringBuffer(stringBuffer);

		return result;
	}

	// ------------------------------------------------------------------------------------------------------

	private boolean isTokenWhitespace() {
		return getElement().getType() == TTCN3LexerTokenTypes.WS;
	}

	// ------------------------------------------------------------------------------------------------------

	private boolean isTokenWhitespaceOrComment() {
		return ((getElement().getType() == TTCN3LexerTokenTypes.WS)
				|| (getElement().getType() == TTCN3LexerTokenTypes.ML_COMMENT) || (getElement()
				.getType() == TTCN3LexerTokenTypes.SL_COMMENT));
	}

	// ------------------------------------------------------------------------------------------------------

	public boolean isNewlineBeforeLast() throws Exception {
		saveIndexPosition();

		boolean newlineBeforeLast = false;

		while (hasPreviousToken()) {
			previousToken();
			if (getElement().getText().contains("\n")
					|| getElement().getText().contains("\r")) {
				newlineBeforeLast = true;
				break;
			}
			if ((!isTokenWhitespace()))
				break;

		}

		restoreIndexPosition();

		return newlineBeforeLast;
	}

	// ------------------------------------------------------------------------------------------------------

	public ScanResult scanBackwardSpaces() throws Exception {
		int indexBackup = index;
		ScanResult result = new ScanResult();
		StringBuffer stringBuffer = new StringBuffer();

		int whiteSpaceCount = 0;

		while (hasPreviousToken()) {
			previousToken();
			if ((!isTokenWhitespace()) || (!containsTabOrSpace(getElement())))
				break;

			stringBuffer.append(getElement().getText());
			whiteSpaceCount++;
		}
		result.setIndex(index);
		result.setStringBuffer(stringBuffer);
		result.setWhiteSpaceCount(whiteSpaceCount);

		index = indexBackup;

		return result;
	}

	// ------------------------------------------------------------------------------------------------------

	public int scanBackwardNewlines() throws Exception {
		int indexBackup = index;

		int newLineCount = 0;

		while (hasPreviousToken()) {
			previousToken();
			if ((!isTokenWhitespace()))
				break;

			if (getElement().getText().contains("\n") || (getElement().getText().contains("\r")))
				newLineCount++;
		}

		index = indexBackup;

		return newLineCount;
	}

	// ------------------------------------------------------------------------------------------------------

	public boolean previousTokenContainsNewline() throws Exception {
		//TODO: Duplicates existing functionality
		saveIndexPosition();

		boolean result = false;
		while (hasPreviousToken()) {
			previousToken();

			if (getElement().getText().contains("\n")
					|| getElement().getText().contains("\r")) {
				result = true;
				break;
			}
			if (getElement().getType() != TTCN3LexerTokenTypes.WS)
				break;

		}

		restoreIndexPosition();

		return result;
	}

	// ------------------------------------------------------------------------------------------------------

	public boolean previousTokensContainNewline() throws Exception {
		//TODO: triplicates existing functionality
		saveIndexPosition();
		boolean result = false;
		while (hasPreviousToken()) {
			previousToken();
			if ((getElement().getType() != TTCN3LexerTokenTypes.WS) || ((getElement().getText().contains("\n")
					|| getElement().getText().contains("\r"))))
				break;

			if (getElement().getText().contains("\n")
					|| getElement().getText().contains("\r")) {
				result = true;
				break;
			}
		}

		restoreIndexPosition();

		return result;
	}

	// ------------------------------------------------------------------------------------------------------

	public boolean nextTokenContainsNewline() throws Exception {
		saveIndexPosition();

		boolean result = false;
		while (hasNextToken()) {
			TTCN3CorePlugin.debugMessage("\t|"+getElement().getText()+"|");
			if (getElement().getType() != TTCN3LexerTokenTypes.WS)
				break;

			if (getElement().getText().contains("\n")
					|| getElement().getText().contains("\r")) {
				result = true;
				break;
			}
			nextToken();
			TTCN3CorePlugin.debugMessage("next|"+getElement().getText()+"|");
		}

		restoreIndexPosition();

		return result;
	}

	// ------------------------------------------------------------------------------------------------------

	private boolean containsTabOrSpace(Token t) {
		if (t.getText().contains("\t") || t.getText().contains(" "))
			return true;

		return false;
	}

	// ------------------------------------------------------------------------------------------------------

	public void saveIndexPosition() {
		backupIndex = index;
	}

	// ------------------------------------------------------------------------------------------------------

	public int getBackupIndexPosition() {
		return backupIndex;
	}

	// ------------------------------------------------------------------------------------------------------

	public void restoreIndexPosition() {
		index = backupIndex;
	}

	// ------------------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		for (int i=0; i < tracker.getTokenCount(); i++) {
			result.append("token " + i + ": " + tracker.getToken(i));
		}

		return result.toString();
	}

	// ------------------------------------------------------------------------------------------------------

	public TokenStreamTracker getTracker() {
		return tracker;
	}

}
