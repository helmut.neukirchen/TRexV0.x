package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import java.io.StringReader;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Hashtable;

import antlr.ANTLRHashString;

public final class KeywordExtractor {

	private HashSet<String> literals = null;
	private static KeywordExtractor instance;

	private KeywordExtractor() {
		init();
	}

	private void init() {
		this.literals = new HashSet<String>();
		this.populateLiterals();
	}

	public static synchronized KeywordExtractor getInstance(){
//		return ReferenceCacheContainer.INSTANCE;
		if (instance == null){
			instance = new KeywordExtractor();
		}
		return instance;
	}

	public HashSet<String> getLiterals(){
		return literals;
	}


	//Alternative implementation, discarded and possibly obsoleted
/*	public static HashSet<String> extractKeywords_alt() {
		Class<?> c = TTCN3LexerTokenTypes.class;
		TTCN3Lexer lexer = new TTCN3Lexer(new StringReader(""));
		int literalCount = 0;
		HashSet<String> literals = new HashSet<String>();
		Hashtable lexerLiterals = lexer.getLiterals();
		for (int i = 0; i < c.getDeclaredFields().length; i++) {
			Field field = c.getDeclaredFields()[i];
			String fieldName = field.getName().toLowerCase();
			try {
				int fieldValue = field.getInt(field);
				if (lexerLiterals.containsValue(new Integer(fieldValue))) {
					ANTLRHashString hashedFieldName = new ANTLRHashString(fieldName,
							lexer);
					int literalTypeValue = (Integer) lexerLiterals.get(hashedFieldName);
					if (literalTypeValue == fieldValue ) {
						literals.add(fieldName);
						literalCount++;
					}
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		}
		//DEBUG
//		System.out.println(literalCount);
//		System.out.println(literals.size());
//		System.out.println(lexerLiterals.size());
		return literals;
	}
*/
	private void populateLiterals() {
		Class<?> c = TTCN3LexerTokenTypes.class;
		TTCN3Lexer lexer = new TTCN3Lexer(new StringReader(""));
		HashSet<ANTLRHashString> hashedLiterals = new HashSet<ANTLRHashString>();
		@SuppressWarnings("rawtypes")
		Hashtable lexerLiterals = lexer.getLiterals();
		for (int i = 0; i < c.getDeclaredFields().length; i++) {
			Field field = c.getDeclaredFields()[i];
			String fieldName = field.getName().toLowerCase();
			ANTLRHashString hashedFieldName = new ANTLRHashString(fieldName,
					lexer);
			if (lexerLiterals.get(hashedFieldName) != null) {
				Integer literalTypeValue = (Integer) lexerLiterals.get(hashedFieldName);
				try {
					int fieldValue = field.getInt(field);
					if (literalTypeValue == fieldValue) {
						this.literals.add(fieldName);
						hashedLiterals.add(hashedFieldName);
					}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}

			}

		}
//		for (Object k : lexerLiterals.keySet()) {
//			if (!hashedLiterals.contains(k)) {
//				System.out.println("missing : " + lexerLiterals.get(k));
//			}
//		}
		//DEBUG
//		System.out.println(literalCount);
//		System.out.println(literals.size());
//		System.out.println(lexerLiterals.size());
	}
}
