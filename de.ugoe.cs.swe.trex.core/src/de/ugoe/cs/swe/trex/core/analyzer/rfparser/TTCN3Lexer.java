// $ANTLR 2.7.7 (2006-11-01): "TTCN3Lexer.g" -> "TTCN3Lexer.java"$

package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import java.io.InputStream;
import antlr.TokenStreamException;
import antlr.TokenStreamIOException;
import antlr.TokenStreamRecognitionException;
import antlr.CharStreamException;
import antlr.CharStreamIOException;
import antlr.ANTLRException;
import java.io.Reader;
import java.util.Hashtable;
import antlr.CharScanner;
import antlr.InputBuffer;
import antlr.ByteBuffer;
import antlr.CharBuffer;
import antlr.Token;
import antlr.CommonToken;
import antlr.RecognitionException;
import antlr.NoViableAltForCharException;
import antlr.MismatchedCharException;
import antlr.TokenStream;
import antlr.ANTLRHashString;
import antlr.LexerSharedInputState;
import antlr.collections.impl.BitSet;
import antlr.SemanticException;

@SuppressWarnings({ "unused", "unchecked", "rawtypes" })

public class TTCN3Lexer extends antlr.CharScanner implements TTCN3LexerTokenTypes, TokenStream
 {

	protected int offset = 0;

	public int getOffset() {
		return offset;
	}

	public void setTokenObjectClass(Class c) {
		tokenObjectClass = c;
	}

	public void consume() throws CharStreamException {
		if (inputState.guessing == 0) {
		    offset++;
		}
		super.consume();
	}

	protected Token makeToken(int t) {
		Token result = super.makeToken(t);
		if (result instanceof TokenWithIndex) {
			((TokenWithIndex)result).setOffset(offset);
		}
		return result;
    }
	public Hashtable getLiterals(){
		return literals;	
	}


public TTCN3Lexer(InputStream in) {
	this(new ByteBuffer(in));
}
public TTCN3Lexer(Reader in) {
	this(new CharBuffer(in));
}
public TTCN3Lexer(InputBuffer ib) {
	this(new LexerSharedInputState(ib));
}
public TTCN3Lexer(LexerSharedInputState state) {
	super(state);
	caseSensitiveLiterals = true;
	setCaseSensitive(true);
	literals = new Hashtable();
	literals.put(new ANTLRHashString("send", this), new Integer(752));
	literals.put(new ANTLRHashString("raise", this), new Integer(739));
	literals.put(new ANTLRHashString("deactivate", this), new Integer(659));
	literals.put(new ANTLRHashString("stop", this), new Integer(758));
	literals.put(new ANTLRHashString("template", this), new Integer(762));
	literals.put(new ANTLRHashString("port", this), new Integer(734));
	literals.put(new ANTLRHashString("complement", this), new Integer(652));
	literals.put(new ANTLRHashString("recursive", this), new Integer(743));
	literals.put(new ANTLRHashString("match", this), new Integer(705));
	literals.put(new ANTLRHashString("mtc", this), new Integer(712));
	literals.put(new ANTLRHashString("create", this), new Integer(658));
	literals.put(new ANTLRHashString("receive", this), new Integer(741));
	literals.put(new ANTLRHashString("trigger", this), new Integer(767));
	literals.put(new ANTLRHashString("language", this), new Integer(701));
	literals.put(new ANTLRHashString("mixed", this), new Integer(707));
	literals.put(new ANTLRHashString("execute", this), new Integer(671));
	literals.put(new ANTLRHashString("pattern", this), new Integer(732));
	literals.put(new ANTLRHashString("none", this), new Integer(715));
	literals.put(new ANTLRHashString("omit", this), new Integer(723));
	literals.put(new ANTLRHashString("pass", this), new Integer(731));
	literals.put(new ANTLRHashString("universal", this), new Integer(771));
	literals.put(new ANTLRHashString("disconnect", this), new Integer(661));
	literals.put(new ANTLRHashString("in", this), new Integer(692));
	literals.put(new ANTLRHashString("boolean", this), new Integer(642));
	literals.put(new ANTLRHashString("catch", this), new Integer(646));
	literals.put(new ANTLRHashString("message", this), new Integer(706));
	literals.put(new ANTLRHashString("mod", this), new Integer(708));
	literals.put(new ANTLRHashString("noblock", this), new Integer(714));
	literals.put(new ANTLRHashString("nowait", this), new Integer(718));
	literals.put(new ANTLRHashString("subset", this), new Integer(759));
	literals.put(new ANTLRHashString("group", this), new Integer(686));
	literals.put(new ANTLRHashString("self", this), new Integer(751));
	literals.put(new ANTLRHashString("rem", this), new Integer(744));
	literals.put(new ANTLRHashString("connect", this), new Integer(654));
	literals.put(new ANTLRHashString("union", this), new Integer(770));
	literals.put(new ANTLRHashString("from", this), new Integer(679));
	literals.put(new ANTLRHashString("continue", this), new Integer(656));
	literals.put(new ANTLRHashString("sender", this), new Integer(753));
	literals.put(new ANTLRHashString("to", this), new Integer(766));
	literals.put(new ANTLRHashString("action", this), new Integer(630));
	literals.put(new ANTLRHashString("infinity", this), new Integer(694));
	literals.put(new ANTLRHashString("record", this), new Integer(742));
	literals.put(new ANTLRHashString("do", this), new Integer(663));
	literals.put(new ANTLRHashString("param", this), new Integer(730));
	literals.put(new ANTLRHashString("clear", this), new Integer(651));
	literals.put(new ANTLRHashString("getreply", this), new Integer(683));
	literals.put(new ANTLRHashString("any", this), new Integer(639));
	literals.put(new ANTLRHashString("out", this), new Integer(728));
	literals.put(new ANTLRHashString("verdicttype", this), new Integer(777));
	literals.put(new ANTLRHashString("repeat", this), new Integer(745));
	literals.put(new ANTLRHashString("charstring", this), new Integer(648));
	literals.put(new ANTLRHashString("override", this), new Integer(729));
	literals.put(new ANTLRHashString("if", this), new Integer(689));
	literals.put(new ANTLRHashString("or", this), new Integer(726));
	literals.put(new ANTLRHashString("bitstring", this), new Integer(641));
	literals.put(new ANTLRHashString("superset", this), new Integer(760));
	literals.put(new ANTLRHashString("while", this), new Integer(778));
	literals.put(new ANTLRHashString("unmap", this), new Integer(772));
	literals.put(new ANTLRHashString("select", this), new Integer(750));
	literals.put(new ANTLRHashString("import", this), new Integer(691));
	literals.put(new ANTLRHashString("altstep", this), new Integer(636));
	literals.put(new ANTLRHashString("system", this), new Integer(761));
	literals.put(new ANTLRHashString("const", this), new Integer(655));
	literals.put(new ANTLRHashString("xor4b", this), new Integer(781));
	literals.put(new ANTLRHashString("on", this), new Integer(724));
	literals.put(new ANTLRHashString("xor", this), new Integer(780));
	literals.put(new ANTLRHashString("float", this), new Integer(677));
	literals.put(new ANTLRHashString("running", this), new Integer(748));
	literals.put(new ANTLRHashString("timeout", this), new Integer(764));
	literals.put(new ANTLRHashString("not", this), new Integer(716));
	literals.put(new ANTLRHashString("not_a_number", this), new Integer(713));
	literals.put(new ANTLRHashString("length", this), new Integer(702));
	literals.put(new ANTLRHashString("integer", this), new Integer(696));
	literals.put(new ANTLRHashString("testcase", this), new Integer(763));
	literals.put(new ANTLRHashString("procedure", this), new Integer(737));
	literals.put(new ANTLRHashString("break", this), new Integer(643));
	literals.put(new ANTLRHashString("private", this), new Integer(736));
	literals.put(new ANTLRHashString("getcall", this), new Integer(682));
	literals.put(new ANTLRHashString("start", this), new Integer(757));
	literals.put(new ANTLRHashString("error", this), new Integer(668));
	literals.put(new ANTLRHashString("log", this), new Integer(703));
	literals.put(new ANTLRHashString("ifpresent", this), new Integer(690));
	literals.put(new ANTLRHashString("done", this), new Integer(664));
	literals.put(new ANTLRHashString("external", this), new Integer(674));
	literals.put(new ANTLRHashString("reply", this), new Integer(746));
	literals.put(new ANTLRHashString("of", this), new Integer(722));
	literals.put(new ANTLRHashString("alive", this), new Integer(633));
	literals.put(new ANTLRHashString("activate", this), new Integer(631));
	literals.put(new ANTLRHashString("valueof", this), new Integer(774));
	literals.put(new ANTLRHashString("kill", this), new Integer(698));
	literals.put(new ANTLRHashString("exception", this), new Integer(670));
	literals.put(new ANTLRHashString("component", this), new Integer(653));
	literals.put(new ANTLRHashString("setverdict", this), new Integer(755));
	literals.put(new ANTLRHashString("public", this), new Integer(738));
	literals.put(new ANTLRHashString("or4b", this), new Integer(727));
	literals.put(new ANTLRHashString("extension", this), new Integer(673));
	literals.put(new ANTLRHashString("alt", this), new Integer(635));
	literals.put(new ANTLRHashString("inconc", this), new Integer(693));
	literals.put(new ANTLRHashString("interleave", this), new Integer(697));
	literals.put(new ANTLRHashString("halt", this), new Integer(687));
	literals.put(new ANTLRHashString("permutation", this), new Integer(733));
	literals.put(new ANTLRHashString("address", this), new Integer(632));
	literals.put(new ANTLRHashString("goto", this), new Integer(685));
	literals.put(new ANTLRHashString("enumerated", this), new Integer(667));
	literals.put(new ANTLRHashString("true", this), new Integer(768));
	literals.put(new ANTLRHashString("set", this), new Integer(754));
	literals.put(new ANTLRHashString("modulepar", this), new Integer(711));
	literals.put(new ANTLRHashString("not4b", this), new Integer(717));
	literals.put(new ANTLRHashString("and", this), new Integer(637));
	literals.put(new ANTLRHashString("return", this), new Integer(747));
	literals.put(new ANTLRHashString("map", this), new Integer(704));
	literals.put(new ANTLRHashString("signature", this), new Integer(756));
	literals.put(new ANTLRHashString("and4b", this), new Integer(638));
	literals.put(new ANTLRHashString("extends", this), new Integer(672));
	literals.put(new ANTLRHashString("encode", this), new Integer(666));
	literals.put(new ANTLRHashString("check", this), new Integer(649));
	literals.put(new ANTLRHashString("control", this), new Integer(657));
	literals.put(new ANTLRHashString("false", this), new Integer(676));
	literals.put(new ANTLRHashString("for", this), new Integer(678));
	literals.put(new ANTLRHashString("default", this), new Integer(660));
	literals.put(new ANTLRHashString("case", this), new Integer(645));
	literals.put(new ANTLRHashString("null", this), new Integer(719));
	literals.put(new ANTLRHashString("modifies", this), new Integer(709));
	literals.put(new ANTLRHashString("read", this), new Integer(740));
	literals.put(new ANTLRHashString("all", this), new Integer(634));
	literals.put(new ANTLRHashString("variant", this), new Integer(776));
	literals.put(new ANTLRHashString("killed", this), new Integer(699));
	literals.put(new ANTLRHashString("runs", this), new Integer(749));
	literals.put(new ANTLRHashString("hexstring", this), new Integer(688));
	literals.put(new ANTLRHashString("display", this), new Integer(662));
	literals.put(new ANTLRHashString("octetstring", this), new Integer(721));
	literals.put(new ANTLRHashString("optional", this), new Integer(725));
	literals.put(new ANTLRHashString("except", this), new Integer(669));
	literals.put(new ANTLRHashString("inout", this), new Integer(695));
	literals.put(new ANTLRHashString("present", this), new Integer(735));
	literals.put(new ANTLRHashString("value", this), new Integer(773));
	literals.put(new ANTLRHashString("getverdict", this), new Integer(684));
	literals.put(new ANTLRHashString("timer", this), new Integer(765));
	literals.put(new ANTLRHashString("friend", this), new Integer(681));
	literals.put(new ANTLRHashString("char", this), new Integer(647));
	literals.put(new ANTLRHashString("type", this), new Integer(769));
	literals.put(new ANTLRHashString("function", this), new Integer(680));
	literals.put(new ANTLRHashString("fail", this), new Integer(675));
	literals.put(new ANTLRHashString("module", this), new Integer(710));
	literals.put(new ANTLRHashString("label", this), new Integer(700));
	literals.put(new ANTLRHashString("checkstate", this), new Integer(650));
	literals.put(new ANTLRHashString("with", this), new Integer(779));
	literals.put(new ANTLRHashString("objid", this), new Integer(720));
	literals.put(new ANTLRHashString("var", this), new Integer(775));
	literals.put(new ANTLRHashString("else", this), new Integer(665));
	literals.put(new ANTLRHashString("anytype", this), new Integer(640));
	literals.put(new ANTLRHashString("call", this), new Integer(644));
}

public Token nextToken() throws TokenStreamException {
	Token theRetToken=null;
tryAgain:
	for (;;) {
		Token _token = null;
		int _ttype = Token.INVALID_TYPE;
		resetText();
		try {   // for char stream error handling
			try {   // for lexical error handling
				switch ( LA(1)) {
				case '\t':  case '\n':  case '\r':  case ' ':
				{
					mWS(true);
					theRetToken=_returnToken;
					break;
				}
				case '{':
				{
					mBEGINCHAR(true);
					theRetToken=_returnToken;
					break;
				}
				case '.':  case '0':  case '1':  case '2':
				case '3':  case '4':  case '5':  case '6':
				case '7':  case '8':  case '9':
				{
					mNUMBER(true);
					theRetToken=_returnToken;
					break;
				}
				case ';':
				{
					mSEMICOLON(true);
					theRetToken=_returnToken;
					break;
				}
				case ',':
				{
					mCOMMA(true);
					theRetToken=_returnToken;
					break;
				}
				case '}':
				{
					mENDCHAR(true);
					theRetToken=_returnToken;
					break;
				}
				case '[':
				{
					mSQUAREOPEN(true);
					theRetToken=_returnToken;
					break;
				}
				case ']':
				{
					mSQUARECLOSE(true);
					theRetToken=_returnToken;
					break;
				}
				case '(':
				{
					mLPAREN(true);
					theRetToken=_returnToken;
					break;
				}
				case ')':
				{
					mRPAREN(true);
					theRetToken=_returnToken;
					break;
				}
				case '=':
				{
					mEQUAL(true);
					theRetToken=_returnToken;
					break;
				}
				case '+':
				{
					mPLUS(true);
					theRetToken=_returnToken;
					break;
				}
				case '*':
				{
					mSTAR(true);
					theRetToken=_returnToken;
					break;
				}
				case '?':
				{
					mQUESTIONMARK(true);
					theRetToken=_returnToken;
					break;
				}
				case '@':
				{
					mROTATERIGHT(true);
					theRetToken=_returnToken;
					break;
				}
				case '&':
				{
					mSTRINGOP(true);
					theRetToken=_returnToken;
					break;
				}
				default:
					if ((LA(1)=='/') && (LA(2)=='/')) {
						mSL_COMMENT(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='/') && (LA(2)=='*')) {
						mML_COMMENT(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)==':') && (LA(2)=='=')) {
						mASSIGNMENTCHAR(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='-') && (LA(2)=='>')) {
						mPORTREDIRECTSYMBOL(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='"') && ((LA(2) >= '\u0000' && LA(2) <= '\ufffe'))) {
						mCSTRING(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='\'') && (_tokenSet_0.member(LA(2)))) {
						mSTRING(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)==':') && (LA(2)==':')) {
						mDOUBLECOLON(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='!') && (LA(2)=='=')) {
						mNOTEQUALS(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='>') && (LA(2)=='=')) {
						mMOREOREQUAL(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='<') && (LA(2)=='=')) {
						mLESSOREQUAL(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='<') && (LA(2)=='<')) {
						mSHIFTLEFT(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='>') && (LA(2)=='>')) {
						mSHIFTRIGHT(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='<') && (LA(2)=='@')) {
						mROTATELEFT(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='<') && (LA(2)=='?')) {
						mSTRINGANYVALUE(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='<') && (LA(2)=='*')) {
						mSTRINGANYOROMIT(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='<') && (LA(2)=='"')) {
						mQUOTESYMBOL(true);
						theRetToken=_returnToken;
					}
					else if ((_tokenSet_1.member(LA(1))) && (true) && (true) && (true)) {
						mIDENTIFIER(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='"') && (true)) {
						mDOUBLEQUOTE(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)==':') && (true)) {
						mCOLON(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='_') && (true) && (true) && (true)) {
						mUNDERSCORE(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='<') && (true)) {
						mLESSTHAN(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='>') && (true)) {
						mMORETHAN(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='-') && (true)) {
						mMINUS(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='/') && (true)) {
						mSLASH(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='\'') && (true)) {
						mSINGLEQUOTE(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='!') && (true)) {
						mEXCLAMATIONMARK(true);
						theRetToken=_returnToken;
					}
				else {
					if (LA(1)==EOF_CHAR) {uponEOF(); _returnToken = makeToken(Token.EOF_TYPE);}
				else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
				}
				}
				if ( _returnToken==null ) continue tryAgain; // found SKIP token
				_ttype = _returnToken.getType();
				_returnToken.setType(_ttype);
				return _returnToken;
			}
			catch (RecognitionException e) {
				throw new TokenStreamRecognitionException(e);
			}
		}
		catch (CharStreamException cse) {
			if ( cse instanceof CharStreamIOException ) {
				throw new TokenStreamIOException(((CharStreamIOException)cse).io);
			}
			else {
				throw new TokenStreamException(cse.getMessage());
			}
		}
	}
}

	public final void mWS(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = WS;
		int _saveIndex;
		
		{
		switch ( LA(1)) {
		case ' ':
		{
			match(' ');
			break;
		}
		case '\t':
		{
			match('\t');
			break;
		}
		case '\n':
		{
			match('\n');
			if ( inputState.guessing==0 ) {
				newline();
			}
			break;
		}
		default:
			if ((LA(1)=='\r') && (LA(2)=='\n')) {
				match("\r\n");
				if ( inputState.guessing==0 ) {
					newline();
				}
			}
			else if ((LA(1)=='\r') && (true)) {
				match('\r');
				if ( inputState.guessing==0 ) {
					newline();
				}
			}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSL_COMMENT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = SL_COMMENT;
		int _saveIndex;
		
		match("//");
		{
		_loop6:
		do {
			if ((_tokenSet_2.member(LA(1)))) {
				{
				match(_tokenSet_2);
				}
			}
			else {
				break _loop6;
			}
			
		} while (true);
		}
		{
		if ( inputState.guessing==0 ) {
			
							if (!(LA(1) == EOF_CHAR)) {
								if ((LA(1) == '\r') && (LA(2) == '\n')) {
									match("\r\n");
									newline();
								} else {
									switch ( LA(1)) {
										case '\n':
										{
											match('\n');
											newline();
											break;
										}
										case '\r':
										{
											match('\r');
											newline();
											break;
										}
									}
								}
							}
						
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mML_COMMENT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ML_COMMENT;
		int _saveIndex;
		
		match("/*");
		{
		_loop11:
		do {
			if (((LA(1)=='*') && ((LA(2) >= '\u0000' && LA(2) <= '\ufffe')) && ((LA(3) >= '\u0000' && LA(3) <= '\ufffe')))&&( LA(2)!='/' )) {
				match('*');
			}
			else if ((LA(1)=='\n')) {
				match('\n');
				if ( inputState.guessing==0 ) {
					newline();
				}
			}
			else if ((_tokenSet_3.member(LA(1)))) {
				{
				match(_tokenSet_3);
				}
			}
			else {
				break _loop11;
			}
			
		} while (true);
		}
		match("*/");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mIDENTIFIER(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = IDENTIFIER;
		int _saveIndex;
		
		switch ( LA(1)) {
		case 'A':  case 'B':  case 'C':  case 'D':
		case 'E':  case 'F':  case 'G':  case 'H':
		case 'I':  case 'J':  case 'K':  case 'L':
		case 'M':  case 'N':  case 'O':  case 'P':
		case 'Q':  case 'R':  case 'S':  case 'T':
		case 'U':  case 'V':  case 'W':  case 'X':
		case 'Y':  case 'Z':  case 'a':  case 'b':
		case 'c':  case 'd':  case 'e':  case 'f':
		case 'g':  case 'h':  case 'i':  case 'j':
		case 'k':  case 'l':  case 'm':  case 'n':
		case 'o':  case 'p':  case 'q':  case 'r':
		case 's':  case 't':  case 'u':  case 'v':
		case 'w':  case 'x':  case 'y':  case 'z':
		{
			{
			{
			switch ( LA(1)) {
			case 'a':  case 'b':  case 'c':  case 'd':
			case 'e':  case 'f':  case 'g':  case 'h':
			case 'i':  case 'j':  case 'k':  case 'l':
			case 'm':  case 'n':  case 'o':  case 'p':
			case 'q':  case 'r':  case 's':  case 't':
			case 'u':  case 'v':  case 'w':  case 'x':
			case 'y':  case 'z':
			{
				matchRange('a','z');
				break;
			}
			case 'A':  case 'B':  case 'C':  case 'D':
			case 'E':  case 'F':  case 'G':  case 'H':
			case 'I':  case 'J':  case 'K':  case 'L':
			case 'M':  case 'N':  case 'O':  case 'P':
			case 'Q':  case 'R':  case 'S':  case 'T':
			case 'U':  case 'V':  case 'W':  case 'X':
			case 'Y':  case 'Z':
			{
				matchRange('A','Z');
				break;
			}
			default:
			{
				throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
			}
			}
			}
			{
			_loop16:
			do {
				switch ( LA(1)) {
				case 'a':  case 'b':  case 'c':  case 'd':
				case 'e':  case 'f':  case 'g':  case 'h':
				case 'i':  case 'j':  case 'k':  case 'l':
				case 'm':  case 'n':  case 'o':  case 'p':
				case 'q':  case 'r':  case 's':  case 't':
				case 'u':  case 'v':  case 'w':  case 'x':
				case 'y':  case 'z':
				{
					matchRange('a','z');
					break;
				}
				case 'A':  case 'B':  case 'C':  case 'D':
				case 'E':  case 'F':  case 'G':  case 'H':
				case 'I':  case 'J':  case 'K':  case 'L':
				case 'M':  case 'N':  case 'O':  case 'P':
				case 'Q':  case 'R':  case 'S':  case 'T':
				case 'U':  case 'V':  case 'W':  case 'X':
				case 'Y':  case 'Z':
				{
					matchRange('A','Z');
					break;
				}
				case '_':
				{
					match('_');
					break;
				}
				case '0':  case '1':  case '2':  case '3':
				case '4':  case '5':  case '6':  case '7':
				case '8':  case '9':
				{
					matchRange('0','9');
					break;
				}
				default:
				{
					break _loop16;
				}
				}
			} while (true);
			}
			}
			break;
		}
		case '_':
		{
			mPreProcessorMacro(false);
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		_ttype = testLiteralsTable(_ttype);
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mPreProcessorMacro(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = PreProcessorMacro;
		int _saveIndex;
		
		if ((LA(1)=='_') && (LA(2)=='_') && (LA(3)=='L')) {
			match("__LINE__");
		}
		else if ((LA(1)=='_') && (LA(2)=='_') && (LA(3)=='F')) {
			match("__FILE__");
		}
		else if ((LA(1)=='_') && (LA(2)=='_') && (LA(3)=='M')) {
			match("__MODULE__");
		}
		else if ((LA(1)=='_') && (LA(2)=='_') && (LA(3)=='S')) {
			match("__SCOPE__");
		}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mASSIGNMENTCHAR(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ASSIGNMENTCHAR;
		int _saveIndex;
		
		match(":=");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mPORTREDIRECTSYMBOL(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = PORTREDIRECTSYMBOL;
		int _saveIndex;
		
		match("->");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mBEGINCHAR(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = BEGINCHAR;
		int _saveIndex;
		
		match('{');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mCSTRING(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = CSTRING;
		int _saveIndex;
		
		_saveIndex=text.length();
		match('"');
		text.setLength(_saveIndex);
		{
		_loop26:
		do {
			boolean synPredMatched23 = false;
			if (((LA(1)=='"') && (LA(2)=='"'))) {
				int _m23 = mark();
				synPredMatched23 = true;
				inputState.guessing++;
				try {
					{
					mDOUBLEQUOTE(false);
					mDOUBLEQUOTE(false);
					}
				}
				catch (RecognitionException pe) {
					synPredMatched23 = false;
				}
				rewind(_m23);
inputState.guessing--;
			}
			if ( synPredMatched23 ) {
				{
				mDOUBLEQUOTE(false);
				mDOUBLEQUOTE(false);
				}
				if ( inputState.guessing==0 ) {
					text.setLength(_begin); text.append("\"\"");
				}
			}
			else if ((_tokenSet_4.member(LA(1)))) {
				{
				match(_tokenSet_4);
				}
			}
			else {
				break _loop26;
			}
			
		} while (true);
		}
		_saveIndex=text.length();
		match('"');
		text.setLength(_saveIndex);
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mDOUBLEQUOTE(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = DOUBLEQUOTE;
		int _saveIndex;
		
		match('"');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSTRING(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = STRING;
		int _saveIndex;
		
		boolean synPredMatched31 = false;
		if (((LA(1)=='\'') && (LA(2)=='\''||LA(2)=='0'||LA(2)=='1') && (_tokenSet_5.member(LA(3))) && (true))) {
			int _m31 = mark();
			synPredMatched31 = true;
			inputState.guessing++;
			try {
				{
				match('\'');
				{
				_loop30:
				do {
					if ((LA(1)=='0'||LA(1)=='1')) {
						mBIN(false);
					}
					else {
						break _loop30;
					}
					
				} while (true);
				}
				match('\'');
				match('B');
				}
			}
			catch (RecognitionException pe) {
				synPredMatched31 = false;
			}
			rewind(_m31);
inputState.guessing--;
		}
		if ( synPredMatched31 ) {
			_saveIndex=text.length();
			match('\'');
			text.setLength(_saveIndex);
			{
			_loop33:
			do {
				if ((LA(1)=='0'||LA(1)=='1')) {
					mBIN(false);
				}
				else {
					break _loop33;
				}
				
			} while (true);
			}
			_saveIndex=text.length();
			match('\'');
			text.setLength(_saveIndex);
			_saveIndex=text.length();
			match('B');
			text.setLength(_saveIndex);
			if ( inputState.guessing==0 ) {
				_ttype = BSTRING;
			}
		}
		else {
			boolean synPredMatched37 = false;
			if (((LA(1)=='\'') && (_tokenSet_6.member(LA(2))) && (_tokenSet_7.member(LA(3))) && (true))) {
				int _m37 = mark();
				synPredMatched37 = true;
				inputState.guessing++;
				try {
					{
					match('\'');
					{
					_loop36:
					do {
						if ((_tokenSet_8.member(LA(1)))) {
							mHEX(false);
						}
						else {
							break _loop36;
						}
						
					} while (true);
					}
					match('\'');
					match('H');
					}
				}
				catch (RecognitionException pe) {
					synPredMatched37 = false;
				}
				rewind(_m37);
inputState.guessing--;
			}
			if ( synPredMatched37 ) {
				_saveIndex=text.length();
				match('\'');
				text.setLength(_saveIndex);
				{
				_loop39:
				do {
					if ((_tokenSet_8.member(LA(1)))) {
						mHEX(false);
					}
					else {
						break _loop39;
					}
					
				} while (true);
				}
				_saveIndex=text.length();
				match('\'');
				text.setLength(_saveIndex);
				_saveIndex=text.length();
				match('H');
				text.setLength(_saveIndex);
				if ( inputState.guessing==0 ) {
					_ttype = HSTRING;
				}
			}
			else {
				boolean synPredMatched43 = false;
				if (((LA(1)=='\'') && (_tokenSet_6.member(LA(2))) && (_tokenSet_9.member(LA(3))) && (true))) {
					int _m43 = mark();
					synPredMatched43 = true;
					inputState.guessing++;
					try {
						{
						match('\'');
						{
						_loop42:
						do {
							if ((_tokenSet_8.member(LA(1)))) {
								mOCT(false);
							}
							else {
								break _loop42;
							}
							
						} while (true);
						}
						match('\'');
						match('O');
						}
					}
					catch (RecognitionException pe) {
						synPredMatched43 = false;
					}
					rewind(_m43);
inputState.guessing--;
				}
				if ( synPredMatched43 ) {
					_saveIndex=text.length();
					match('\'');
					text.setLength(_saveIndex);
					{
					_loop45:
					do {
						if ((_tokenSet_8.member(LA(1)))) {
							mOCT(false);
						}
						else {
							break _loop45;
						}
						
					} while (true);
					}
					_saveIndex=text.length();
					match('\'');
					text.setLength(_saveIndex);
					_saveIndex=text.length();
					match('O');
					text.setLength(_saveIndex);
					if ( inputState.guessing==0 ) {
						_ttype = OSTRING;
					}
				}
				else {
					boolean synPredMatched49 = false;
					if (((LA(1)=='\'') && (_tokenSet_10.member(LA(2))) && (_tokenSet_11.member(LA(3))) && (true))) {
						int _m49 = mark();
						synPredMatched49 = true;
						inputState.guessing++;
						try {
							{
							match('\'');
							{
							_loop48:
							do {
								switch ( LA(1)) {
								case '0':  case '1':
								{
									mBIN(false);
									break;
								}
								case '?':
								{
									match('?');
									break;
								}
								case '*':
								{
									match('*');
									break;
								}
								default:
								{
									break _loop48;
								}
								}
							} while (true);
							}
							match('\'');
							match('B');
							}
						}
						catch (RecognitionException pe) {
							synPredMatched49 = false;
						}
						rewind(_m49);
inputState.guessing--;
					}
					if ( synPredMatched49 ) {
						_saveIndex=text.length();
						match('\'');
						text.setLength(_saveIndex);
						{
						_loop51:
						do {
							switch ( LA(1)) {
							case '0':  case '1':
							{
								mBIN(false);
								break;
							}
							case '?':
							{
								match('?');
								break;
							}
							case '*':
							{
								match('*');
								break;
							}
							default:
							{
								break _loop51;
							}
							}
						} while (true);
						}
						_saveIndex=text.length();
						match('\'');
						text.setLength(_saveIndex);
						_saveIndex=text.length();
						match('B');
						text.setLength(_saveIndex);
						if ( inputState.guessing==0 ) {
							_ttype = BSTRINGMATCH;
						}
					}
					else {
						boolean synPredMatched55 = false;
						if (((LA(1)=='\'') && (_tokenSet_0.member(LA(2))) && (_tokenSet_12.member(LA(3))) && (true))) {
							int _m55 = mark();
							synPredMatched55 = true;
							inputState.guessing++;
							try {
								{
								match('\'');
								{
								_loop54:
								do {
									switch ( LA(1)) {
									case '0':  case '1':  case '2':  case '3':
									case '4':  case '5':  case '6':  case '7':
									case '8':  case '9':  case 'A':  case 'B':
									case 'C':  case 'D':  case 'E':  case 'F':
									case 'a':  case 'b':  case 'c':  case 'd':
									case 'e':  case 'f':
									{
										mHEX(false);
										break;
									}
									case '?':
									{
										match('?');
										break;
									}
									case '*':
									{
										match('*');
										break;
									}
									default:
									{
										break _loop54;
									}
									}
								} while (true);
								}
								match('\'');
								match('H');
								}
							}
							catch (RecognitionException pe) {
								synPredMatched55 = false;
							}
							rewind(_m55);
inputState.guessing--;
						}
						if ( synPredMatched55 ) {
							_saveIndex=text.length();
							match('\'');
							text.setLength(_saveIndex);
							{
							_loop57:
							do {
								switch ( LA(1)) {
								case '0':  case '1':  case '2':  case '3':
								case '4':  case '5':  case '6':  case '7':
								case '8':  case '9':  case 'A':  case 'B':
								case 'C':  case 'D':  case 'E':  case 'F':
								case 'a':  case 'b':  case 'c':  case 'd':
								case 'e':  case 'f':
								{
									mHEX(false);
									break;
								}
								case '?':
								{
									match('?');
									break;
								}
								case '*':
								{
									match('*');
									break;
								}
								default:
								{
									break _loop57;
								}
								}
							} while (true);
							}
							_saveIndex=text.length();
							match('\'');
							text.setLength(_saveIndex);
							_saveIndex=text.length();
							match('H');
							text.setLength(_saveIndex);
							if ( inputState.guessing==0 ) {
								_ttype = HSTRINGMATCH;
							}
						}
						else {
							boolean synPredMatched61 = false;
							if (((LA(1)=='\'') && (_tokenSet_0.member(LA(2))) && (_tokenSet_13.member(LA(3))) && (true))) {
								int _m61 = mark();
								synPredMatched61 = true;
								inputState.guessing++;
								try {
									{
									match('\'');
									{
									_loop60:
									do {
										switch ( LA(1)) {
										case '0':  case '1':  case '2':  case '3':
										case '4':  case '5':  case '6':  case '7':
										case '8':  case '9':  case 'A':  case 'B':
										case 'C':  case 'D':  case 'E':  case 'F':
										case 'a':  case 'b':  case 'c':  case 'd':
										case 'e':  case 'f':
										{
											mOCT(false);
											break;
										}
										case '?':
										{
											match('?');
											break;
										}
										case '*':
										{
											match('*');
											break;
										}
										default:
										{
											break _loop60;
										}
										}
									} while (true);
									}
									match('\'');
									match('O');
									}
								}
								catch (RecognitionException pe) {
									synPredMatched61 = false;
								}
								rewind(_m61);
inputState.guessing--;
							}
							if ( synPredMatched61 ) {
								_saveIndex=text.length();
								match('\'');
								text.setLength(_saveIndex);
								{
								_loop63:
								do {
									switch ( LA(1)) {
									case '0':  case '1':  case '2':  case '3':
									case '4':  case '5':  case '6':  case '7':
									case '8':  case '9':  case 'A':  case 'B':
									case 'C':  case 'D':  case 'E':  case 'F':
									case 'a':  case 'b':  case 'c':  case 'd':
									case 'e':  case 'f':
									{
										mOCT(false);
										break;
									}
									case '?':
									{
										match('?');
										break;
									}
									case '*':
									{
										match('*');
										break;
									}
									default:
									{
										break _loop63;
									}
									}
								} while (true);
								}
								_saveIndex=text.length();
								match('\'');
								text.setLength(_saveIndex);
								_saveIndex=text.length();
								match('O');
								text.setLength(_saveIndex);
								if ( inputState.guessing==0 ) {
									_ttype = OSTRINGMATCH;
								}
							}
							else {
								throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
							}
							}}}}}
							if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
								_token = makeToken(_ttype);
								_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
							}
							_returnToken = _token;
						}
						
	protected final void mBIN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = BIN;
		int _saveIndex;
		
		matchRange('0','1');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mHEX(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = HEX;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '0':  case '1':  case '2':  case '3':
		case '4':  case '5':  case '6':  case '7':
		case '8':  case '9':
		{
			mNum(false);
			break;
		}
		case 'A':
		{
			match('A');
			break;
		}
		case 'B':
		{
			match('B');
			break;
		}
		case 'C':
		{
			match('C');
			break;
		}
		case 'D':
		{
			match('D');
			break;
		}
		case 'E':
		{
			match('E');
			break;
		}
		case 'F':
		{
			match('F');
			break;
		}
		case 'a':
		{
			match('a');
			break;
		}
		case 'b':
		{
			match('b');
			break;
		}
		case 'c':
		{
			match('c');
			break;
		}
		case 'd':
		{
			match('d');
			break;
		}
		case 'e':
		{
			match('e');
			break;
		}
		case 'f':
		{
			match('f');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mOCT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = OCT;
		int _saveIndex;
		
		mHEX(false);
		mHEX(false);
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mNUMBER(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = NUMBER;
		int _saveIndex;
		boolean isDecimal=false;
		
		if ((LA(1)=='.') && (LA(2)=='.')) {
			match("..");
			if ( inputState.guessing==0 ) {
				_ttype = RANGEOP;
			}
		}
		else if ((LA(1)=='.') && (true)) {
			match('.');
			if ( inputState.guessing==0 ) {
				_ttype = DOT;
			}
			{
			if (((LA(1) >= '0' && LA(1) <= '9'))) {
				{
				int _cnt67=0;
				_loop67:
				do {
					if (((LA(1) >= '0' && LA(1) <= '9'))) {
						matchRange('0','9');
					}
					else {
						if ( _cnt67>=1 ) { break _loop67; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
					}
					
					_cnt67++;
				} while (true);
				}
				{
				if ((LA(1)=='E'||LA(1)=='e')) {
					mEXPONENT(false);
				}
				else {
				}
				
				}
				if ( inputState.guessing==0 ) {
					_ttype = FLOATVALUE;
				}
			}
			else {
			}
			
			}
		}
		else if (((LA(1) >= '0' && LA(1) <= '9'))) {
			{
			switch ( LA(1)) {
			case '0':
			{
				match('0');
				if ( inputState.guessing==0 ) {
					isDecimal = true;
				}
				break;
			}
			case '1':  case '2':  case '3':  case '4':
			case '5':  case '6':  case '7':  case '8':
			case '9':
			{
				{
				matchRange('1','9');
				}
				{
				_loop72:
				do {
					if (((LA(1) >= '0' && LA(1) <= '9'))) {
						matchRange('0','9');
					}
					else {
						break _loop72;
					}
					
				} while (true);
				}
				if ( inputState.guessing==0 ) {
					isDecimal=true;
				}
				break;
			}
			default:
			{
				throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
			}
			}
			}
			{
			if (((LA(1)=='.'||LA(1)=='E'||LA(1)=='e'))&&( LA(2)!='.' && LA(3)!='.' && isDecimal)) {
				{
				switch ( LA(1)) {
				case '.':
				{
					match('.');
					{
					_loop76:
					do {
						if (((LA(1) >= '0' && LA(1) <= '9'))) {
							matchRange('0','9');
						}
						else {
							break _loop76;
						}
						
					} while (true);
					}
					{
					if ((LA(1)=='E'||LA(1)=='e')) {
						mEXPONENT(false);
					}
					else {
					}
					
					}
					break;
				}
				case 'E':  case 'e':
				{
					mEXPONENT(false);
					break;
				}
				default:
				{
					throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
				}
				}
				}
				if ( inputState.guessing==0 ) {
					_ttype = FLOATVALUE;
				}
			}
			else {
			}
			
			}
		}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mEXPONENT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = EXPONENT;
		int _saveIndex;
		
		{
		switch ( LA(1)) {
		case 'e':
		{
			{
			match('e');
			}
			break;
		}
		case 'E':
		{
			{
			match('E');
			}
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		{
		switch ( LA(1)) {
		case '-':
		{
			match('-');
			break;
		}
		case '0':  case '1':  case '2':  case '3':
		case '4':  case '5':  case '6':  case '7':
		case '8':  case '9':
		{
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		}
		{
		int _cnt84=0;
		_loop84:
		do {
			if (((LA(1) >= '0' && LA(1) <= '9'))) {
				matchRange('0','9');
			}
			else {
				if ( _cnt84>=1 ) { break _loop84; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());}
			}
			
			_cnt84++;
		} while (true);
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSEMICOLON(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = SEMICOLON;
		int _saveIndex;
		
		match(';');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mCOMMA(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = COMMA;
		int _saveIndex;
		
		match(',');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mDOUBLECOLON(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = DOUBLECOLON;
		int _saveIndex;
		
		match("::");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mCOLON(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = COLON;
		int _saveIndex;
		
		match(':');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mUNDERSCORE(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = UNDERSCORE;
		int _saveIndex;
		
		match('_');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mENDCHAR(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ENDCHAR;
		int _saveIndex;
		
		match('\175');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSQUAREOPEN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = SQUAREOPEN;
		int _saveIndex;
		
		match('[');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSQUARECLOSE(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = SQUARECLOSE;
		int _saveIndex;
		
		match(']');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mLPAREN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = LPAREN;
		int _saveIndex;
		
		match('(');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mRPAREN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = RPAREN;
		int _saveIndex;
		
		match(')');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mLESSTHAN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = LESSTHAN;
		int _saveIndex;
		
		match('<');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mMORETHAN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = MORETHAN;
		int _saveIndex;
		
		match('>');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mNOTEQUALS(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = NOTEQUALS;
		int _saveIndex;
		
		match("!=");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mMOREOREQUAL(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = MOREOREQUAL;
		int _saveIndex;
		
		match(">=");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mLESSOREQUAL(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = LESSOREQUAL;
		int _saveIndex;
		
		match("<=");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mEQUAL(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = EQUAL;
		int _saveIndex;
		
		match("==");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mPLUS(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = PLUS;
		int _saveIndex;
		
		match('+');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mMINUS(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = MINUS;
		int _saveIndex;
		
		match('-');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSTAR(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = STAR;
		int _saveIndex;
		
		match('*');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSLASH(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = SLASH;
		int _saveIndex;
		
		match('/');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSINGLEQUOTE(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = SINGLEQUOTE;
		int _saveIndex;
		
		match('\'');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mQUESTIONMARK(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = QUESTIONMARK;
		int _saveIndex;
		
		match('?');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mEXCLAMATIONMARK(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = EXCLAMATIONMARK;
		int _saveIndex;
		
		match('!');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSHIFTLEFT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = SHIFTLEFT;
		int _saveIndex;
		
		match("<<");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSHIFTRIGHT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = SHIFTRIGHT;
		int _saveIndex;
		
		match(">>");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mROTATELEFT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ROTATELEFT;
		int _saveIndex;
		
		match("<@");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mROTATERIGHT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ROTATERIGHT;
		int _saveIndex;
		
		match("@>");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSTRINGANYVALUE(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = STRINGANYVALUE;
		int _saveIndex;
		
		match("<?>");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSTRINGANYOROMIT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = STRINGANYOROMIT;
		int _saveIndex;
		
		match("<*>");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mQUOTESYMBOL(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = QUOTESYMBOL;
		int _saveIndex;
		
		match("<\">");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mSTRINGOP(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = STRINGOP;
		int _saveIndex;
		
		match('&');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mZero(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = Zero;
		int _saveIndex;
		
		match('0');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mNonzeronum(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = Nonzeronum;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '1':
		{
			match('1');
			break;
		}
		case '2':
		{
			match('2');
			break;
		}
		case '3':
		{
			match('3');
			break;
		}
		case '4':
		{
			match('4');
			break;
		}
		case '5':
		{
			match('5');
			break;
		}
		case '6':
		{
			match('6');
			break;
		}
		case '7':
		{
			match('7');
			break;
		}
		case '8':
		{
			match('8');
			break;
		}
		case '9':
		{
			match('9');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mNonzeronumber(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = Nonzeronumber;
		int _saveIndex;
		
		mNonzeronum(false);
		{
		_loop121:
		do {
			if (((LA(1) >= '0' && LA(1) <= '9'))) {
				mNum(false);
			}
			else {
				break _loop121;
			}
			
		} while (true);
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mNum(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = Num;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '0':
		{
			mZero(false);
			break;
		}
		case '1':  case '2':  case '3':  case '4':
		case '5':  case '6':  case '7':  case '8':
		case '9':
		{
			mNonzeronum(false);
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mDecimalnumber(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = Decimalnumber;
		int _saveIndex;
		
		{
		_loop124:
		do {
			if (((LA(1) >= '0' && LA(1) <= '9'))) {
				mNum(false);
			}
			else {
				break _loop124;
			}
			
		} while (true);
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mChar(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = Char;
		int _saveIndex;
		
		matchRange('\3','\377');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mGeneralTextCharacter(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = GeneralTextCharacter;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '0':  case '1':  case '2':  case '3':
		case '4':  case '5':  case '6':  case '7':
		case '8':  case '9':  case 'A':  case 'B':
		case 'C':  case 'D':  case 'E':  case 'F':
		case 'G':  case 'H':  case 'I':  case 'J':
		case 'K':  case 'L':  case 'M':  case 'N':
		case 'O':  case 'P':  case 'Q':  case 'R':
		case 'S':  case 'T':  case 'U':  case 'V':
		case 'W':  case 'X':  case 'Y':  case 'Z':
		case 'a':  case 'b':  case 'c':  case 'd':
		case 'e':  case 'f':  case 'g':  case 'h':
		case 'i':  case 'j':  case 'k':  case 'l':
		case 'm':  case 'n':  case 'o':  case 'p':
		case 'q':  case 'r':  case 's':  case 't':
		case 'u':  case 'v':  case 'w':  case 'x':
		case 'y':  case 'z':
		{
			mAlphanumeric(false);
			break;
		}
		case '$':  case '%':  case '&':  case '?':
		case '@':  case '\\':  case '^':  case '_':
		case '`':  case '|':  case '~':
		{
			mOtherCharacter(false);
			break;
		}
		case '\u0000':  case '\u0001':  case '\u0002':  case '\u0003':
		case '\u0004':  case '\u0005':  case '\u0006':  case '\u0007':
		case '\u0008':  case '\t':  case '\n':  case '\u000b':
		case '\u000c':  case '\r':  case '\u000e':  case '\u000f':
		case '\u0010':  case '\u0011':  case '\u0012':  case '\u0013':
		case '\u0014':  case '\u0015':  case '\u0016':  case '\u0017':
		case '\u0018':  case '\u0019':  case '\u001a':  case '\u001b':
		case '\u001c':  case '\u001d':  case '\u001e':  case '\u001f':
		case ' ':
		{
			mSpace(false);
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mAlphanumeric(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = Alphanumeric;
		int _saveIndex;
		
		switch ( LA(1)) {
		case 'A':  case 'B':  case 'C':  case 'D':
		case 'E':  case 'F':  case 'G':  case 'H':
		case 'I':  case 'J':  case 'K':  case 'L':
		case 'M':  case 'N':  case 'O':  case 'P':
		case 'Q':  case 'R':  case 'S':  case 'T':
		case 'U':  case 'V':  case 'W':  case 'X':
		case 'Y':  case 'Z':  case 'a':  case 'b':
		case 'c':  case 'd':  case 'e':  case 'f':
		case 'g':  case 'h':  case 'i':  case 'j':
		case 'k':  case 'l':  case 'm':  case 'n':
		case 'o':  case 'p':  case 'q':  case 'r':
		case 's':  case 't':  case 'u':  case 'v':
		case 'w':  case 'x':  case 'y':  case 'z':
		{
			mLetter(false);
			break;
		}
		case '0':  case '1':  case '2':  case '3':
		case '4':  case '5':  case '6':  case '7':
		case '8':  case '9':
		{
			mNum(false);
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mOtherCharacter(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = OtherCharacter;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '$':
		{
			match('$');
			break;
		}
		case '%':
		{
			match('%');
			break;
		}
		case '&':
		{
			match('&');
			break;
		}
		case '?':
		{
			match('?');
			break;
		}
		case '@':
		{
			match('@');
			break;
		}
		case '\\':
		{
			match('\\');
			break;
		}
		case '^':
		{
			match('^');
			break;
		}
		case '_':
		{
			match('_');
			break;
		}
		case '`':
		{
			match('`');
			break;
		}
		case '|':
		{
			match('|');
			break;
		}
		case '~':
		{
			match('~');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mSpace(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = Space;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '\u0000':  case '\u0001':  case '\u0002':  case '\u0003':
		case '\u0004':  case '\u0005':  case '\u0006':  case '\u0007':
		case '\u0008':  case '\t':
		{
			matchRange('\000','\011');
			break;
		}
		case '\n':
		{
			match('\n');
			if ( inputState.guessing==0 ) {
				newline();
			}
			break;
		}
		case '\u000b':  case '\u000c':  case '\r':  case '\u000e':
		case '\u000f':  case '\u0010':  case '\u0011':  case '\u0012':
		case '\u0013':  case '\u0014':  case '\u0015':  case '\u0016':
		case '\u0017':  case '\u0018':  case '\u0019':  case '\u001a':
		case '\u001b':  case '\u001c':  case '\u001d':  case '\u001e':
		case '\u001f':
		{
			matchRange('\013','\037');
			break;
		}
		case ' ':
		{
			match(' ');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mLetter(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = Letter;
		int _saveIndex;
		
		switch ( LA(1)) {
		case 'A':  case 'B':  case 'C':  case 'D':
		case 'E':  case 'F':  case 'G':  case 'H':
		case 'I':  case 'J':  case 'K':  case 'L':
		case 'M':  case 'N':  case 'O':  case 'P':
		case 'Q':  case 'R':  case 'S':  case 'T':
		case 'U':  case 'V':  case 'W':  case 'X':
		case 'Y':  case 'Z':
		{
			mUppercaseLetter(false);
			break;
		}
		case 'a':  case 'b':  case 'c':  case 'd':
		case 'e':  case 'f':  case 'g':  case 'h':
		case 'i':  case 'j':  case 'k':  case 'l':
		case 'm':  case 'n':  case 'o':  case 'p':
		case 'q':  case 'r':  case 's':  case 't':
		case 'u':  case 'v':  case 'w':  case 'x':
		case 'y':  case 'z':
		{
			mLowercaseLetter(false);
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mUppercaseLetter(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = UppercaseLetter;
		int _saveIndex;
		
		{
		matchRange('A','Z');
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mLowercaseLetter(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = LowercaseLetter;
		int _saveIndex;
		
		{
		matchRange('a','z');
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mSpecial(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = Special;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '/':
		{
			match('/');
			break;
		}
		case '*':
		{
			match('*');
			break;
		}
		case '!':  case '#':  case '(':  case ')':
		case '+':  case ',':  case '-':  case '.':
		case ':':  case ';':  case '<':  case '=':
		case '>':  case '[':  case ']':  case '{':
		case '}':
		{
			mOtherSpecial(false);
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	protected final void mOtherSpecial(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = OtherSpecial;
		int _saveIndex;
		
		switch ( LA(1)) {
		case '!':
		{
			match('!');
			break;
		}
		case '#':
		{
			match('#');
			break;
		}
		case '(':
		{
			match('(');
			break;
		}
		case ')':
		{
			match(')');
			break;
		}
		case '+':
		{
			match('+');
			break;
		}
		case ',':
		{
			match(',');
			break;
		}
		case '-':
		{
			match('-');
			break;
		}
		case '.':
		{
			match('.');
			break;
		}
		case ':':
		{
			match(':');
			break;
		}
		case ';':
		{
			match(';');
			break;
		}
		case '<':
		{
			match('<');
			break;
		}
		case '=':
		{
			match('=');
			break;
		}
		case '>':
		{
			match('>');
			break;
		}
		case '[':
		{
			match('[');
			break;
		}
		case ']':
		{
			match(']');
			break;
		}
		case '{':
		{
			match('{');
			break;
		}
		case '}':
		{
			match('}');
			break;
		}
		default:
		{
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine(), getColumn());
		}
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	
	private static final long[] mk_tokenSet_0() {
		long[] data = new long[1025];
		data[0]=-8935418187877449728L;
		data[1]=541165879422L;
		return data;
	}
	public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());
	private static final long[] mk_tokenSet_1() {
		long[] data = new long[1025];
		data[1]=576460745995190270L;
		return data;
	}
	public static final BitSet _tokenSet_1 = new BitSet(mk_tokenSet_1());
	private static final long[] mk_tokenSet_2() {
		long[] data = new long[2048];
		data[0]=-9217L;
		for (int i = 1; i<=1022; i++) { data[i]=-1L; }
		data[1023]=9223372036854775807L;
		return data;
	}
	public static final BitSet _tokenSet_2 = new BitSet(mk_tokenSet_2());
	private static final long[] mk_tokenSet_3() {
		long[] data = new long[2048];
		data[0]=-4398046512129L;
		for (int i = 1; i<=1022; i++) { data[i]=-1L; }
		data[1023]=9223372036854775807L;
		return data;
	}
	public static final BitSet _tokenSet_3 = new BitSet(mk_tokenSet_3());
	private static final long[] mk_tokenSet_4() {
		long[] data = new long[2048];
		data[0]=-17179869185L;
		for (int i = 1; i<=1022; i++) { data[i]=-1L; }
		data[1023]=9223372036854775807L;
		return data;
	}
	public static final BitSet _tokenSet_4 = new BitSet(mk_tokenSet_4());
	private static final long[] mk_tokenSet_5() {
		long[] data = new long[1025];
		data[0]=844974685945856L;
		data[1]=4L;
		return data;
	}
	public static final BitSet _tokenSet_5 = new BitSet(mk_tokenSet_5());
	private static final long[] mk_tokenSet_6() {
		long[] data = new long[1025];
		data[0]=287949450930814976L;
		data[1]=541165879422L;
		return data;
	}
	public static final BitSet _tokenSet_6 = new BitSet(mk_tokenSet_6());
	private static final long[] mk_tokenSet_7() {
		long[] data = new long[1025];
		data[0]=287949450930814976L;
		data[1]=541165879678L;
		return data;
	}
	public static final BitSet _tokenSet_7 = new BitSet(mk_tokenSet_7());
	private static final long[] mk_tokenSet_8() {
		long[] data = new long[1025];
		data[0]=287948901175001088L;
		data[1]=541165879422L;
		return data;
	}
	public static final BitSet _tokenSet_8 = new BitSet(mk_tokenSet_8());
	private static final long[] mk_tokenSet_9() {
		long[] data = new long[1025];
		data[0]=287948901175001088L;
		data[1]=541165912190L;
		return data;
	}
	public static final BitSet _tokenSet_9 = new BitSet(mk_tokenSet_9());
	private static final long[] mk_tokenSet_10() {
		long[] data = new long[1025];
		data[0]=-9222522664122318848L;
		return data;
	}
	public static final BitSet _tokenSet_10 = new BitSet(mk_tokenSet_10());
	private static final long[] mk_tokenSet_11() {
		long[] data = new long[1025];
		data[0]=-9222522664122318848L;
		data[1]=4L;
		return data;
	}
	public static final BitSet _tokenSet_11 = new BitSet(mk_tokenSet_11());
	private static final long[] mk_tokenSet_12() {
		long[] data = new long[1025];
		data[0]=-8935418187877449728L;
		data[1]=541165879678L;
		return data;
	}
	public static final BitSet _tokenSet_12 = new BitSet(mk_tokenSet_12());
	private static final long[] mk_tokenSet_13() {
		long[] data = new long[1025];
		data[0]=-8935418187877449728L;
		data[1]=541165912190L;
		return data;
	}
	public static final BitSet _tokenSet_13 = new BitSet(mk_tokenSet_13());
	
	}
