package de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable;

import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;

public class FunctionSymbol extends Symbol {
	private boolean external = false;

	public boolean isExternal() {
		return external;
	}

	public void setExternal(boolean external) {
		this.external = external;
	}


	@Override
	public String getSymbolDescription() {
		StringBuffer result = new StringBuffer();
		HTMLPrinter.getInstance().addPageProlog(result);
		result.append(getVisibilityDescription());
		String externalStr = "";
		if (external)
			externalStr += "External ";
		HTMLPrinter.getInstance().addSmallHeader(result, externalStr + "Function Name: ");
		HTMLPrinter.getInstance().addLine(result, getName());
		
		if (getGroupName() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Group:");
			HTMLPrinter.getInstance().addLine(result, getGroupName());
		}

		if (getSurroundingScope().getScopeSymbol() != null) {
			HTMLPrinter.getInstance().addSmallHeader(result, "In Scope of:");
			result.append(getSurroundingScope().getScopeSymbol().getSymbolDescriptionPlain());
		}

		
		HTMLPrinter.getInstance().addPageEpilog(result);

		return result.toString();
	}
	
	@Override
	public String getSymbolDescriptionPlain() {
		StringBuffer result = new StringBuffer();
		if (external)
			result.append("external function ");
		else
			result.append("function ");
		
		result.append(getName());

		return result.toString();
	}

	@Override
	public String getSymbolDescriptionContentAssist() {
		StringBuffer result = new StringBuffer(getName()).append(" - ");
		if (external)
			result.append("external function");
		else
			result.append("function");
		
		return result.toString();
	}

}
