package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import java.util.Hashtable;

import antlr.ASTFactory;
//import antlr.ASTPair;
import antlr.collections.AST;
import antlr.collections.impl.ASTArray;

public class TTCN3ASTFactory extends ASTFactory {

	public TTCN3ASTFactory() {
		// empty constructor
	}

	@SuppressWarnings("rawtypes")
	public TTCN3ASTFactory(Hashtable tokenTypeToClassMap) {
		setTokenTypeToASTClassMap(tokenTypeToClassMap);
	}

	@Override
	public Class<LocationAST> getASTNodeType(int i) {
		return LocationAST.class; // force tree type
	}

	@Override
	public AST make(ASTArray nodes) {
		AST t = make(nodes.array);
		return t;
	}

//	@Override
//	public void addASTChild(ASTPair currentAST, AST child) {
//		super.addASTChild(currentAST, child);
//	}
//
//	@Override
//	public void makeASTRoot(ASTPair currentAST, AST root) {
//		super.makeASTRoot(currentAST, root);
//	}

}
