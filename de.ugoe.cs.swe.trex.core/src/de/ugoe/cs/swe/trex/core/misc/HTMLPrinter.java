package de.ugoe.cs.swe.trex.core.misc;

public class HTMLPrinter {
	private HTMLPrinterDelegate delegate = null;
	private static HTMLPrinter instance = null;
	
	public static HTMLPrinter getInstance() {
		if (instance == null)
			instance = new HTMLPrinter();
		return instance;
	}
	
	public void setHTMLPrinterDelegate(HTMLPrinterDelegate newDelegate) {
		delegate = newDelegate;
	}
	
	public void addPageProlog(StringBuffer buffer) {
		if (delegate != null)
			delegate.addPageProlog(buffer);
	}

	public void addPageEpilog(StringBuffer buffer) {
		if (delegate != null)
			delegate.addPageEpilog(buffer);
	}

	public void addSmallHeader(StringBuffer buffer, String header) {
		if (delegate != null)
			delegate.addSmallHeader(buffer, header);
	}

	public void addLine(StringBuffer buffer, String line) {
		if (delegate != null)
			delegate.addLine(buffer, line);
	}

}
