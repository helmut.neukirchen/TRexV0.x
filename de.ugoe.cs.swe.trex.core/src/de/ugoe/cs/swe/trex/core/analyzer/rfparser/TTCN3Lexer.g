// The TTCN-3 lexer and parser are derived from the sources of the open source TTthreeParser.
// These sources are covered by the GPL, but Testing Tech IST GmbH granted the permission to redistribute 
// the derived work under the Eclipse Public License.
//

header {
package de.ugoe.cs.swe.trex.core.analyzer.rfparser;
}

{
@SuppressWarnings({ "unused", "unchecked", "rawtypes" })
}

class TTCN3Lexer extends Lexer;

options {
    exportVocab=TTCN3Lexer;
    testLiterals = false;
    caseSensitive = true;
    k=4;
    defaultErrorHandler=false;
    charVocabulary = '\u0000'..'\uFFFE';
}

tokens
{
	AddExpression;                AddressRefList;                AliveKeyword;
	AliveOp;                      AllAltstepsWithExcept;         AllCompsAllPortsSpec;
	AllConnectionsSpec;           AllConstsWithExcept;           AllFunctionsWithExcept;
	AllGroupsWithExcept;          AllModuleParWithExcept;        AllPortsSpec;
	AllSignaturesWithExcept;      AllTemplsWithExcept;           AllTestcasesWithExcept;
	AllTypesWithExcept;           AllWithExcepts;                AltstepInstance;
	AltstepLocalDef;              AltstepLocalDefList;           AltstepRef;
	AltstepRefList;               AndExpression;                 AnyTypeKeyword;
	AllRef;						 

	BitAndExpression;             BitNotExpression;              BitOrExpression;
	BitXorExpression;			  BreakStatement;

	CaseKeyword;                  CheckPortOpsPresent;           CompenentIdentifierOrLiteral;
	ComponentIdentifierOrLiteral; ComponentReferenceOrLiteral;   ConstRefList;
	ContinueStatement;

	DefaultKeyword;               DerivedRefWithParList;

	EqualExpression;              ExceptAltstepSpec;             ExceptConstSpec;
	ExceptElement;                ExceptFunctionSpec;            ExceptGroupRefList;
	ExceptGroupSpec;              ExceptModuleParSpec;           ExceptSignatureSpec;
	ExceptSpec;                   ExceptTemplateSpec;            ExceptTestcaseSpec;
	ExceptTypeDefSpec;            ExceptsDef;                    ExtendsKeyword;

	FloatDotNotation;             FloatENotation;                FromClausePresent;
	FullGroupIdentifier;          FullGroupIdentifierWithExcept; FunctionRefList;

	GroupRefList;                 GroupRefListWithExcept;

	ImportAltstepSpec;            ImportModuleParSpec;

	KillKeyword;                  KillTCStatement;               KilledStatement;

	LogItem;					  

	ModuleDefinitionList;         ModuleParRefList;              MulExpression;

	NestedEnumDef;                NestedRecordDef;               NestedRecordOfDef;
	NestedSetDef;                 NestedSetOfDef;                NestedTypeDef;
	NestedUnionDef;               NotExpression;

	OmitValue;

	PortRedirectSymbol;           PortRedirectWithValueAndParam;

	RedirectPresent;              RedirectWithParamSpec;         RedirectWithValueAndParamSpec;
	RelExpression;                RepeatStatement;				 RestrictedTemplate;

	SelectCase;                   SelectCaseBody;                SelectCaseConstruct;
	SelectKeyword;                ShiftExpression;               SignatureRefList;
	SingleConnectionSpec;         SingleOrMultiConnectionSpec;   SingleTimerInstance;

	TempVarList;                  TemplateRefList;				 TemplateRestriction;              
	TestcaseRefList; 			  TimerList;                     TypeRefList;

	UnaryExpression;	NameForm;	NumberForm;		NameAndNumberForm;

	VariationKeyword;	DefinitiveIdentifier;	DefinitiveNumberForm;

	XorExpression;		DefinitiveNameAndNumberForm;

    /*----------------------------------------- Nodes ------------------------------------------*/

	ActivateOp;                   AddOp;                       AddressKeyword;
	AddressDecl;
	AddressRef;                   AddressValue;                AllKeyword;
	AllElementsFrom;
	AllOrSignatureList;           AllOrTypeList;               AllPort;
	AllowedValues;                AltConstruct;                AltGuardChar;
	AltGuardElement;              AltGuardElements;            AltGuardList;
	AltKeyword;                   AltstepDef;                  AltstepKeyword;
	And4b;                        And;                         AnyKeyword;
	AnyOrOmit;                    AnyValue;                    ArrayBounds;
	ArrayConstExpression;
	ArrayDef;                     ArrayDefRange;               ArrayElementConstExpressionList;
	ArrayElementExpressionList;   ArrayElementSpec;            ArrayElementSpecList;
	ArrayExpression;              ArrayOrBitRef;               ArrayValueOrAttrib;
	Assignment;                   AssignmentChar;              AssignmentList;
	AttribKeyword;                AttribQualifier;             AttribSpec;

	BaseTemplate;                 BasicStatements;             BeginChar;
	BehaviourStatements;          BinOrMatch;                  BitOp;
	BitStringKeyword;             BitStringMatch;              BitStringValue;
	BooleanExpression;            BooleanKeyword;              BooleanValue;

	CallBodyGuard;                CallBodyOps;                 CallBodyStatement;
	CallBodyStatementList;        CallParameters;              CallStatement;
	CallTimerValue;               CatchOpParameter;            CatchStatement;
	Cell;                         CharKeyword;                 CharStringKeyword;
	CharStringMatch;              CharStringPattern;           CharStringValue;
	CharStringValueId;            CheckParameter;              CheckStatement;
	CheckStateStatement;
	ClearStatement;               Colon;                       Comma;
	CommunicationStatements;      Complement;                  ComponentDef;
	ComponentDefList;             ComponentElementDef;         ComponentId;
	ComponentIdentifier;          ComponentRef;                ComponentType;
	ComponentRefAssignment;
	CompoundConstExpression;      CompoundExpression;          ConditionalConstruct;
	ConfigParamDef;
	ConfigSpec;                   ConfigurationOps;            ConfigurationStatements;
	ConnectStatement;             ConstDef;                    ConstList;
	ConstantExpression;           ControlStatement;            ControlStatementOrDef;
	ControlStatementOrDefList;    CreateOp;                    Cstring;

	Dash;                         DeactivateStatement;         DefIndentClose;
	DefIndentOpen;                DefKeyword;                  DefOrFieldRef;
	DefOrFieldRefList;            
	DerivedDef;                   DerivedRegWithParList;
	Direction;                    DisconnectStatement;         DisplayKeyword;
	DoWhileStatement;             DoneStatement;               Dot;
	DoubleColon;

	ElseClause;                   ElseIfClause;                ElseIfClauses;
	ElseStatement;                EncodeKeyword;               EndChar;
	EnumDef;                      EnumeratedValue;             Enumeration;
	EnumerationList;              Equal;                       Error;
	ExceptionSpec;                ExceptionTypeList;           ExclamationMark;
	Expression;                   ExtConstDef;                 ExtFunctionDef;
	ExtendedFieldReference;       ExtensionKeyword;            ExtraMatchingAttributes;

	Factor;                       Fail;                        False;
	FieldConstExpressionList;     FieldConstExpressionSpec;    FieldExpressionList;
	FieldExpressionSpec;          FieldReference;              FieldSpec;
	FieldSpecList;                FieldValueOrAttrib;          Final;
	FloatKeyword;                 FloatValue;                  ForStatement;
	FormalPortPar;                FormalTemplatePar;           FormalTimerPar;
	FormalTypePar;                FormalValuePar;              FromClause;
	FunctionActualParAssignment;  FriendKeyword;			   FriendModuleDef;
	FunctionActualPar;            FunctionActualParList;       FunctionBody;
	FunctionDef;				  FunctionLocalDefOrInst;	   FunctionLocalDefOrInstList;
	FunctionStatementList;		  FunctionFormalPar;           FunctionInstance;
	FunctionLocalDef;             FunctionLocalInst;           FunctionRef;
	FunctionStatement;            /*FunctionStatementOrDef;*/      /*FunctionStatementOrDefList;*/

	GetCallStatement;             GetLocalVerdict;             GetReplyStatement;
	GlobalModuleId;               GotoStatement;               Group;
	GroupDef;                     GuardOp;                     GuardStatement;

	HexOrMatch;                   HexStringKeyword;            HexStringMatch;
	HexStringValue;               HaltStatement;

	Identifier;                   IfPresentMatch;              ImportAllSpec;
	ImportConstSpec;              ImportDef;                   ImportElement;
	ImportFromSpec;               ImportFunctionSpec;          ImportGroupSpec;
	ImportNamedAltSpec;           ImportSignatureSpec;         ImportSpec;
	ImportTemplateSpec;           ImportTestcaseSpec;          ImportTypeDefSpec;
	ImportImportSpec;
	InLineTemplate;               InOutParKeyword;             InParKeyword;
	Inconc;                       Infinity;                    Initial;
	IntegerKeyword;               IntegerValue;                InterleavedAction;
	InterleavedConstruct;         InterleavedGuard;            InterleavedGuardElement;
	InterleavedGuardList;

	KilledKeyword;

	LParen;                       LabelStatement;              LanguageSpec;
	LengthMatch;                  Lessorequal;                 Lessthan;
	ListOfTemplates;
	LogStatement;                 LoopConstruct;               LowerBound;

	MTCOp;                        
	MapParamDef;				  MapStatement;                MatchOp;
	MatchingSymbol;               MessageAttribs;              MessageList;
	Minus;                        MixedAttribs;                MixedList;
	Mod;                          ModifiesKeyword;             ModuleControlBody;
	ModuleControlPart;            ModuleDefinition;            ModuleDefinitionsList;
	ModuleDefinitionsPart;        ModuleId;                    ModulePar;
	ModuleParDef;                 ModuleParList;               Moreorequal;
	Morethan;                     MultiWithAttrib;             MultiplyOp;
	MultitypedModuleParList;

	NamedAltActualPar;            NamedAltActualParList;
	NamedAltDef;                  NamedAltFormalPar;           NamedAltFormalParList;
	NamedAltInstance;             NamedAltList;                NamedAltRef;
	NamedAltRefList;              NamedKeyword;                NamedValue;
	NamedValueList;               NoBlockKeyword;              None;
	Not4b;                        Not;                         NotUsedOrExpression;
	NotUsedSymbol;                Notequals;                   NowaitKeyword;
	Null;                         Number;       			   NaN;            

	OctOrMatch;                   OctetStringKeyword;
	OctetStringMatch;             OctetStringValue;            OmitKeyword;
	OpCall;                       OpValue;                     OptionalKeyword;
	Or4b;                         Or;                          OutParKeyword;
	OverrideKeyword;

	ParRef;                       ParaAssignmentList;          
	ParamClause;				  ParaSpec;
	Pass;                         PermutationMatch;            Plane;
	Plus;                         Port;                        PortCallBody;
	PortCallOp;                   PortCatchOp;                 PortCheckOp;
	PortClearOp;                  PortDef;                     PortDefAttribs;
	PortDefBody;                  PortElement;                 PortGetCallOp;
	PortGetReplyOp;               PortInstance;                PortKeyword;
	PortOrAll;                    
	PortOrAllOrAny;				  PortOrAny;                   PortRaiseOp;
	PortReceiveOp;                PortRedirect;                PortRedirectWithParam;
	PortRef;                      PortReplyOp;                 PortSendOp;
	PortRefAssignment;
	PortSpec;                     PortStartOp;                 PortStopOp;
	PortTriggerOp;                PortType;                    PredefinedType;
	PredefinedValue;              Primary;                     ProcOrType;
	ProcOrTypeList;               ProcedureAttribs;            ProcedureList;
	Product;                      PortHaltOp;
	PrivateKeyword;				  PublicKeyword;

	Quadruple;                    QuestionMark;

	RParen;                       RaiseStatement;              Range;
	RangeDef;                     RangeOp;                     ReadTimerOp;
	ReceiveParameter;             ReceiveStatement;            RecordDef;
	RecordOfDef;                  RecordRef;                   RecursiveKeyword;
	ReferencedType;               ReferencedValue;             RelOp;
	Rem;                          ReplyStatement;              ReturnStatement;
	ReturnType;                   Row;                         RunningOp;
	RunningTimerOp;               RunsOnSpec;

	SUTAction;                    SUTStatements;               SelfOp;
	SemiColon;                    SendParameter;               SendStatement;
	SenderSpec;                   SetDef;                      SetLocalVerdict;
	SetOfDef;                     ShiftOp;                     Signature;
	SignatureDef;                 SignatureFormalPar;          SignatureFormalParList;
	SignatureList;                SimpleExpression;            SimpleExpressions;
	SingleConstDef;               SingleExpression;            SingleQuote;
	SingleTempVarInstance;        SingleValueOrAttrib;         SingleVarInstance;
	SingleValueSpec;
	SingleWithAttrib;             Slash;                       SquareClose;
	SquareOpen;                   Star;                        StartStatement;
	StartTCStatement;             StartTimerStatement;         StatementBlock;
	Step;                         StopStatement;               StopTCStatement;
	StopTimerStatement;           StringAnyOrOmit;             StringAnyValue;
	StringLength;                 StringMatchOp;               StringOp;
	StructDefBody;                StructDefFormalPar;          StructDefFormalParList;
	StructFieldDef;               StructFieldRef;              StructOfDefBody;
	StructuredTypeDef;            SubExpression;               SubTypeDef;
	SubTypeSpec;                  SubsetMatch;                 SupersetMatch;
	SystemOp;                     SystemSpec;

	TTCN3File;                    TTCN3Module;                 TTCN3ModuleId;
	TemplateActualPar;            TemplateActualParList;       TemplateBody;
	TemplateActualParAssignment;
	TemplateDef;                  TemplateFormalPar;           TemplateFormalParList;
	TemplateInstanceAssignment;
	TemplateInstance;             TemplateKeyword;             TemplateOps;
	TemplateRef;                  TemplateRefWithParList;      Term;
	TestcaseActualParAssignment;
	TestcaseActualPar;            TestcaseActualParList;       TestcaseDef;
	TestcaseInstance;             
	TestcaseOperation;			  TestcaseRef;                 TimeoutKeyword;
	TimeoutStatement;             TimerInstance;               TimerKeyword;
	TimerOps;                     TimerRef;                    TimerRefOrAll;
	TimerRefAssignment;
	TimerRefOrAny;                TimerStatements;             TimerValue;
	ToClause;                     TriggerStatement;            True;
	Type;                         TypeActualPar;               TypeActualParList;
	TypeDef;                      TypeDefIdentifier;           TypeList;
	TypeReference;

	UnaryOp;                      Underscore;                  UnionDef;
	UnionDefBody;                 UnionFieldDef;               UniversalChar;
	UniversalCharString;          UniversalKeyword;            
	UnmapParamDef;				  UnmapStatement;
	UpperBound;

	Value;                        ValueList;                   ValueMatchSpec;
	ValueOrAttribList;            ValueOrRange;                ValueParOrTypePar;
	ValueReference;               ValueSpec;                   ValueofOp;
	VarInitialValue;              VarInstance;                 VarList;
	VariableAssignment;           VariableEntry;               VariableList;
	VariableRef;                                               VerdictOps;
	VerdictStatements;            VerdictTypeKeyword;          VerdictTypeValue;
	Visibility;

	WhileStatement;				  WildcardLengthMatch;               
	WithAttribList;               WithStatement;

	Xor4b;                        Xor;

	/*------------------------------------------------------------------------------------------*/

	/*------ new -------*/

	BSTRING; BSTRINGMATCH;

	DOT;

	FLOATVALUE;

	HSTRING; HSTRINGMATCH;

	OSTRING; OSTRINGMATCH;

	RANGEOP;

	/*------------------------------------------- Keywords -------------------------------------------*/

	ACTION = "action";                 ACTIVATE = "activate";       ADDRESS = "address";
	ALIVE = "alive";                   ALL = "all";                 ALT = "alt";
	ALTSTEP = "altstep";               AND = "and";                 AND4B = "and4b";
	ANY = "any";                       ANYTYPE = "anytype";         

	BITSTRING = "bitstring";           BOOLEAN = "boolean";			BREAK = "break";

	CALL = "call";                     CASE = "case";               
	CATCH = "catch";                   CHAR = "char";  	            CHARSTRING = "charstring";
	CHECK = "check";                   CHECKSTATE = "checkstate";
	CLEAR = "clear";          		   COMPLEMENT = "complement";
	COMPONENT = "component";           CONNECT = "connect";         CONST = "const";
	CONTINUE = "continue";			   CONTROL = "control";			CREATE = "create";

	DEACTIVATE = "deactivate";         DEFAULT = "default";         DISCONNECT = "disconnect";
	DISPLAY = "display";               DO = "do";                   DONE = "done";

	ELSE = "else";                     ENCODE = "encode";           ENUMERATED = "enumerated";
	ERROR = "error";                   EXCEPT = "except";           EXCEPTION = "exception";
	EXECUTE = "execute";               EXTENDS = "extends";
	EXTENSION = "extension";           EXTERNAL = "external";

	FAIL = "fail";                     FALSE = "false";             FLOAT = "float";
	FOR = "for";                       FROM = "from";               FUNCTION = "function";
	FRIEND = "friend";
	                                   GETCALL = "getcall";         GETREPLY = "getreply";
	GETVERDICT = "getverdict";         GOTO = "goto";               GROUP = "group";

	HALT = "halt";                     HEXSTRING = "hexstring";

	IF = "if";                         IFPRESENT = "ifpresent";     IMPORT = "import";
	IN = "in";                         INCONC = "inconc";           INFINITY = "infinity";
	INOUT = "inout";                   INTEGER = "integer";         INTERLEAVE = "interleave";

	KILL ="kill";
	KILLED = "killed";

	LABEL = "label";                   LANGUAGE="language";         LENGTH = "length";
	LOG = "log";

	MAP = "map";                       MATCH = "match";             MESSAGE = "message";
	MIXED="mixed";                     MOD = "mod";                 MODIFIES = "modifies";
	MODULE = "module";                 MODULEPAR = "modulepar";     MTC = "mtc";

    NAN = "not_a_number";              NOBLOCK = "noblock";         NONE = "none";
	NOT = "not";                       NOT4B = "not4b";             NOWAIT = "nowait";
	NULL = "null";

	OBJID = "objid";                   OCTETSTRING = "octetstring"; OF = "of";
	OMIT = "omit";                     ON = "on";                   OPTIONAL = "optional";
	OR = "or";                         OR4B = "or4b";               OUT = "out";
	OVERRIDE = "override";

	PARAM = "param";                   PASS = "pass";               PATTERN = "pattern";
	PERMUTATION = "permutation";       PORT = "port";               PRESENT = "present";
	PRIVATE = "private";			   PROCEDURE = "procedure";		PUBLIC = "public";		

	RAISE = "raise";                   READ = "read";               RECEIVE = "receive";
	RECORD = "record";                 RECURSIVE = "recursive";     REM = "rem";
	REPEAT = "repeat";                 REPLY = "reply";             RETURN = "return";
	RUNNING= "running";                RUNS = "runs";

	SELECT = "select";                 SELF = "self";               SEND = "send";
	SENDER = "sender";                 SET = "set";                 SETVERDICT = "setverdict";
	SIGNATURE = "signature";                                        START = "start";
	STOP = "stop";                     SUBSET = "subset";           SUPERSET = "superset";
                                       SYSTEM ="system";

	TEMPLATE = "template";             TESTCASE = "testcase";       TIMEOUT = "timeout";
	TIMER = "timer";                   TO = "to";                   TRIGGER = "trigger";
	TRUE = "true";                                                  TYPE = "type";

	UNION = "union";                   UNIVERSAL = "universal";     UNMAP = "unmap";

	VALUE = "value";                   VALUEOF = "valueof";         VAR = "var";
	VARIANT = "variant";                                            VERDICTTYPE = "verdicttype";

	WHILE = "while";                   WITH = "with";

	XOR = "xor";                       XOR4B = "xor4b";

	/*------------------------------------------------------------------------------------------------*/
}

{
	protected int offset = 0;

	public int getOffset() {
		return offset;
	}

	public void setTokenObjectClass(Class c) {
		tokenObjectClass = c;
	}

	public void consume() throws CharStreamException {
		if (inputState.guessing == 0) {
		    offset++;
		}
		super.consume();
	}

	protected Token makeToken(int t) {
		Token result = super.makeToken(t);
		if (result instanceof TokenWithIndex) {
			((TokenWithIndex)result).setOffset(offset);
		}
		return result;
    }
	public Hashtable getLiterals(){
		return literals;	
	}


}

WS
        :
            (' '
            |'\t'
            |'\n'  { newline(); }
            |'\r' { newline(); }
            | "\r\n" { newline(); }
            )
//            { $setType(Token.SKIP); }
        ;

SL_COMMENT :
        "//"
        (~('\n'|'\r'))* //('\n' | '\r')
        (
			{
				if (!(LA(1) == EOF_CHAR)) {
					if ((LA(1) == '\r') && (LA(2) == '\n')) {
						match("\r\n");
						newline();
					} else {
						switch ( LA(1)) {
							case '\n':
							{
								match('\n');
								newline();
								break;
							}
							case '\r':
							{
								match('\r');
								newline();
								break;
							}
						}
					}
				}
			}
        )
//        { _ttype = Token.SKIP; newline(); }
//        { newline(); }
        ;

ML_COMMENT:
                "/*"
                (       
                	{ LA(2)!='/' }? '*'
	                | '\n' { newline(); }
                	| ~('*'|'\n')
                )*
                "*/"
//                { $setType(Token.SKIP); }
        ;

IDENTIFIER
options {
  testLiterals = true;
  paraphrase = "an identifer";
}
        :       
        	(('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*)
        	| PreProcessorMacro
        	
        ;

ASSIGNMENTCHAR
options {
  paraphrase = "an assignment char ':='";
}

        : ":="
        ;

PORTREDIRECTSYMBOL
options {
  paraphrase = "a port redirect symbol '->'";
}
        : "->"
        ;

BEGINCHAR
options {
  paraphrase = "a begin char'{'";
}
        : '{'
        ;


CSTRING
options {
  paraphrase = "a cstring";
}
        :       '"'! 
                (   options { greedy = true; }:
                    (DOUBLEQUOTE DOUBLEQUOTE) => (DOUBLEQUOTE DOUBLEQUOTE) { $setText("\"\""); }
                    | ~('"')
				)*
				'"'!
        ;


STRING
options {
  paraphrase = "a BIN, HEX or OCT";
}
        :
                ( '\'' ( BIN )* '\'' 'B' ) => '\''! ( BIN )* '\''! 'B'!
                    { _ttype = BSTRING; }
        |       ( '\'' ( HEX )* '\'' 'H' ) => '\''! ( HEX )* '\''! 'H'!
                    { _ttype = HSTRING; }
        |       ( '\'' ( OCT )* '\'' 'O' ) => '\''! ( OCT )* '\''! 'O'!
                    { _ttype = OSTRING; }
        |       ( '\'' ( BIN | '?' | '*' )* '\'' 'B' ) => '\''! ( BIN | '?' | '*' )* '\''! 'B'!
                    { _ttype = BSTRINGMATCH; }
        |       ( '\'' ( HEX | '?' | '*')* '\'' 'H' ) => '\''! ( HEX | '?' | '*')* '\''! 'H'!
                    { _ttype = HSTRINGMATCH; }
        |       ( '\'' ( OCT | '?' | '*')* '\'' 'O' ) => '\''! ( OCT | '?' | '*')* '\''! 'O'!
                    { _ttype = OSTRINGMATCH; }
        ;

NUMBER
options {
  paraphrase = "a Number";
}
    {boolean isDecimal=false;}
    :
        ".." {_ttype = RANGEOP;}
    |   '.'  {_ttype = DOT;}
        (('0'..'9')+ (EXPONENT)? { _ttype = FLOATVALUE; })?
    |   (   '0' {isDecimal = true;} // special case for just '0'
        |   ('1'..'9') ('0'..'9')*  {isDecimal=true;}       // non-zero decimal
        )
        // only check to see if it's a float if looks like decimal so far
        (   { LA(2)!='.' && LA(3)!='.' && isDecimal}?
            (   '.' ('0'..'9')* (EXPONENT)?
            |   EXPONENT
            )
            { _ttype = FLOATVALUE; }
        )?
    ;



protected
EXPONENT
        :       (('e') | ('E')) ('-')? ('0'..'9')+
        ;

SEMICOLON
options {
  paraphrase = "a semicolon ';'";
}
        :  ';'
        ;

//CSTRDOUBLEQUOTE
//options {
//	paraphrase = "cstrdoublequote";
//}
//	:	"abc"
//	;

COMMA
options {
  paraphrase = "a comma ','";
}
        : ','
        ;

DOUBLECOLON
options {
  paraphrase = "a double colon '::'";
}
        : "::"
        ;

COLON
options {
  paraphrase = "a colon ':'";
}

        : ':'
        ;

UNDERSCORE
options {
  paraphrase = "an underscore '_'";
}
        : 
/*
        {((LA(1)=='_') && (LA(2) != '_'))}? (options {
		        warnWhenFollowAmbig=false;
		    }: '_')
*/		    
		'_'
        ;

ENDCHAR
options {
  paraphrase = "an end char '}'";
}
        : '\175'
        ;

SQUAREOPEN
options {
  paraphrase = "a square bracket open '['";
}
        : '['
        ;

SQUARECLOSE
options {
  paraphrase = "a square bracket close ']'";
}
        : ']'
        ;

LPAREN
options {
  paraphrase = "a left parentheses '('";
}
        : '('
        ;

RPAREN
options {
  paraphrase = "a right parentheses ')'";
}
        : ')'
        ;

LESSTHAN
options {
  paraphrase = "a less than '<'";
}
        : '<'
        ;

MORETHAN
options {
  paraphrase = "a more than ''";
}
        : '>'
        ;

NOTEQUALS
options {
  paraphrase = "a not equals '!='";
}
        : "!="
        ;

MOREOREQUAL
options {
  paraphrase = "a more or equal '>='";
}
        : ">="
        ;

LESSOREQUAL
options {
  paraphrase = "a less or equal '<='";
}
        : "<="
        ;

EQUAL
options {
  paraphrase = "an equals '=='";
}
        : "=="
        ;

PLUS
options {
  paraphrase = "a plus '+'";
}
        : '+'
        ;

MINUS
options {
  paraphrase = "a minus '-'";
}
        : '-'
        ;

STAR
options {
  paraphrase = "a star '*'";
}
        : '*'
        ;

SLASH
options {
  paraphrase = "a slash '/'";
}
        : '/'
        ;

SINGLEQUOTE
options {
  paraphrase = "a single quote '''";
}
        : '\''
        ;

//toremove
DOUBLEQUOTE
options {
  paraphrase = "a double quote '''";
}
        : '"'
        ;


QUESTIONMARK
options {
  paraphrase = "a question mark '?'";
}
        : '?'
        ;

EXCLAMATIONMARK
options {
  paraphrase = "an exclamation mark '!'";
}
        : '!'
        ;


SHIFTLEFT
options {
  paraphrase = "a shift left operator '<<'";
}
        : "<<"
        ;

SHIFTRIGHT
options {
  paraphrase = "a shift right operator '>>'";
}
        : ">>"
        ;

ROTATELEFT
options {
  paraphrase = "a rotate left operator '<@'";
}
        : "<@"
        ;

ROTATERIGHT
options {
  paraphrase = "a rotate right operator '@>'";
}
        : "@>"
        ;


STRINGANYVALUE
options {
  paraphrase = "an ANY Value String '<?>'";
}
        : "<?>"
        ;

STRINGANYOROMIT
options {
  paraphrase = "an ANY OR OMIT Value String '<*>'";
}
        : "<*>"
        ;

QUOTESYMBOL
options {
  paraphrase = "a quote symbol '<\">'";
}

        : "<\">"
;

STRINGOP
options {
  paraphrase = "a string concat operator '&'";
}

        : '&'
;


///////////////////
// Lexical Rules
//////////////////


protected
Zero
        :       '0'
        ;

protected
Nonzeronum
        :       '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
        ;


protected
Nonzeronumber:
        Nonzeronum
     (
        Num
     )*
   ;

protected
Decimalnumber:
     (
        Num
     )*
   ;

protected
Num:
        Zero
     |
        Nonzeronum
   ;


protected
BIN
        :       '0' .. '1'
        ;

protected
HEX
        :       Num | 'A' | 'B' | 'C' | 'D' | 'E' | 'F'| 'a' | 'b' | 'c' | 'd' | 'e' | 'f'
        ;

protected
OCT     : HEX HEX
        ;


protected
Char
        :       '\3'..'\377'
        ;


protected GeneralTextCharacter
        :
                    Alphanumeric
                  | OtherCharacter
                  | Space
        ;

protected Alphanumeric
        :
          Letter
        | Num

        ;

protected Letter :

    UppercaseLetter
  | LowercaseLetter

  ;

protected UppercaseLetter :

  ( 'A' .. 'Z' )

  ;

protected LowercaseLetter :

  ( 'a' .. 'z' )


;
protected Special :

    '/'
  | '*'
  | OtherSpecial
  ;

protected OtherSpecial :

    '!' | '#' | '(' | ')' | '+' | ',' | '-'
  | '.' | ':' | ';' | '<' | '=' | '>'
  | '[' | ']' | '{' | '}'

  ;

protected OtherCharacter :

    '$' | '%' | '&' | '?' | '@'
  | '\\' | '^' | '_' | '`' | '|' | '~'

  ;

protected Space :
    '\000' .. '\011'
  | '\n' { newline(); }
  | '\013' .. '\037'
  | ' '
  ;


protected PreProcessorMacro:
	"__LINE__"
	| "__FILE__"
	| "__MODULE__"
	| "__SCOPE__"
;


