package de.ugoe.cs.swe.trex.core.misc;

public abstract class HTMLPrinterDelegate {

	public abstract void addPageProlog(StringBuffer buffer);

	public abstract void addPageEpilog(StringBuffer buffer);

	public abstract void addSmallHeader(StringBuffer buffer, String header);

	public abstract void addLine(StringBuffer buffer, String line);
}
