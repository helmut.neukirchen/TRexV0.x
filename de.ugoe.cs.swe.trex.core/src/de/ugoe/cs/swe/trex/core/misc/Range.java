package de.ugoe.cs.swe.trex.core.misc;
@SuppressWarnings("rawtypes")
public class Range implements Comparable {
	int from, to;

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public Range(final int pFrom, final int pTo) {
		this.from = pFrom;
		this.to = pTo;
	}

	@Override
	public String toString() {
		return "[(RANGE) " + getFrom() + " - " + getTo() + "]";
	}
	
	public final int compareTo(Object o) {
		if (o instanceof Range) {
			Range oRange = (Range) o;
			if ((this.from < oRange.from) && (this.to < oRange.from) ) {
				return -1;
			} else if ((this.from > oRange.to) && (this.to > oRange.to) ) {
				return 1;
			} else {
				return 0;
			}
		} else if (o instanceof Point) {
			Point oPoint = (Point) o;
			if (this.from <= oPoint.point && oPoint.point <= this.to) {
				return 0;
			} else if (this.to < oPoint.point) {
				return -1;
			}
			return 1;
		} else {
			throw new IllegalArgumentException(o.getClass().getName());
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Range) {
			Range r = (Range) obj;
			
			if ((r.getFrom() == getFrom()) && (r.getTo() == getTo()))
				return true;
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		assert false : "hashCode not implemented for Range objects";
		return 0; 
	}
}
