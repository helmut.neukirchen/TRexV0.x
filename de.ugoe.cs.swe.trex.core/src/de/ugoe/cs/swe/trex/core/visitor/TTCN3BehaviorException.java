package de.ugoe.cs.swe.trex.core.visitor;

public class TTCN3BehaviorException extends Exception {

	private static final long serialVersionUID = 3801467473799856594L;

	public TTCN3BehaviorException(String message) {
		super("TTCN-3 Behavior Exception: " + message);
	}

}
