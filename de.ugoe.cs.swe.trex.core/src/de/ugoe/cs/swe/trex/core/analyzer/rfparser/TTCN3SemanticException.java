package de.ugoe.cs.swe.trex.core.analyzer.rfparser;

import antlr.SemanticException;

public class TTCN3SemanticException extends SemanticException implements Comparable<TTCN3SemanticException> {

	private static final long serialVersionUID = 1L;

	public enum warningEnum {
		ERROR, WARNING
	};

	private warningEnum warningType;
	
	private int startChar = 0;
	
	private int endChar = 0;
	
	private boolean hasStartEndPosition = false;
	
	// --------------------------------------------------------------

	public TTCN3SemanticException(String s, String fileName, int line,
			int column, warningEnum warningType) {
		super(s, fileName, line, column);
		this.warningType = warningType;
	}
	
	public TTCN3SemanticException(String s, String fileName, int line,
			int startPosition, int endPosition, int column, warningEnum warningType) {
		//added to simplify marker creation and location handling for underlining 
		super(s, fileName, line, column);
		this.startChar = startPosition;
		this.endChar = endPosition;
		this.warningType = warningType;
		this.hasStartEndPosition = true;
	}

	public warningEnum getWarningType() {
		return warningType;
	}

	public void setWarningType(warningEnum warningType) {
		this.warningType = warningType;
	}
	
	public int getStartChar() {
		return this.startChar;
	}
	
	public int getEndChar() {
		return this.endChar;
	}
	
	public boolean hasStartEndPosition() {
		return this.hasStartEndPosition;
	}
	
	public int compareTo(TTCN3SemanticException o) {
		if (this.getLine() < o.getLine())
			return -1;
		else if (this.getLine() == o.getLine())
			return 0;
		else
			return 1;
	}
	
//    public String toString() { // added for debugging purposes
//    	String output = super.toString();
//   	output = output
//   			+ "\nTCCN3SemanticException part\n"
//   			+ "Serial Version ID: "
//   			+ serialVersionUID + "\n"
//   			+ "Warning type : " + this.warningType + "\n"
//				+ ((this.isUnderlined()) ? "Underline character start : " + this.startChar
//				+ "\n" + "Underline character end : " + this.endChar + "\n" 
//				: "\n");
//    	return output;
//    }
	
}
