#!/bin/bash

#
# use at own risk!!!!!!!!!!!! this script deletes files!!!
#

WORKSPACE_PATH=/home/helmut/eclipse-workspace-oxygen

function deleteGeneratedFiles() {
  rm ${1}.java
  rm ${1}TokenTypes.java
  rm ${1}TokenTypes.txt
  rm ${1}.smap
}

function processFile() {
  deleteGeneratedFiles $1
  /home/helmut/antlr/antlr ${1}.g
}

function copyParserTokenTypes() {
  cp ${WORKSPACE_PATH}/de.ugoe.cs.swe.trex.core/src/de/ugoe/cs/swe/trex/core/analyzer/rfparser/TTCN3ParserTokenTypes.txt .
}

cd ${WORKSPACE_PATH}/de.ugoe.cs.swe.trex.core/src/de/ugoe/cs/swe/trex/core/analyzer/rfparser/

currentpath=`pwd`
echo "Processing files in Path $currentpath"

processFile TTCN3Lexer
processFile TTCN3Parser
processFile TTCN3SymbolTableTreeParser
processFile TTCN3FormatterTreeParser
processFile TTCN3SemanticAnalysisTreeParser

if [ -d "${WORKSPACE_PATH}/de.ugoe.cs.swe.trex.core.callgraphanalysis/src/de/ugoe/cs/swe/trex/core/callgraphanalysis/" ]; then
  cd ${WORKSPACE_PATH}/de.ugoe.cs.swe.trex.core.callgraphanalysis/src/de/ugoe/cs/swe/trex/core/callgraphanalysis/
  currentpath=`pwd`
  echo "Processing files in Path $currentpath"
  processFile CallGraphAnalysisTreeParser
  copyParserTokenTypes
fi

if [ -d "${WORKSPACE_PATH}/de.ugoe.cs.swe.trex.core.controlflowanalysis/src/de/ugoe/cs/swe/trex/core/controlflowanalysis/" ]; then
  cd ${WORKSPACE_PATH}/de.ugoe.cs.swe.trex.core.controlflowanalysis/src/de/ugoe/cs/swe/trex/core/controlflowanalysis/
  currentpath=`pwd`
  echo "Processing files in Path $currentpath"
  processFile ControlFlowAnalysisTreeParser
  copyParserTokenTypes
fi

if [ -d "${WORKSPACE_PATH}/de.ugoe.cs.swe.trex.swemetrics.sizemetrics/src/de/ugoe/cs/swe/trex/swemetrics/sizemetrics/" ]; then
  cd ${WORKSPACE_PATH}/de.ugoe.cs.swe.trex.swemetrics.sizemetrics/src/de/ugoe/cs/swe/trex/swemetrics/sizemetrics/
  currentpath=`pwd`
  echo "Processing files in Path $currentpath"
  processFile SizeMetricsTreeParser
  copyParserTokenTypes
fi
