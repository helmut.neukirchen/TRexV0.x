package de.ugoe.cs.swe.trex.core.refactoring.core.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;

final class MyTree {
	private MyTree next, child;
	private LocationAST node;
	private String name;
	private boolean parity;
	private List<LocationAST> importedElements;
	private List<LocationAST> notImportedElements;
	private int behaviourNumber;

	public MyTree() {
		super();
		next = null;
		child = null;
		node = null;
		name = null;
		parity = false;
		importedElements = null;
		notImportedElements = null;
		behaviourNumber = -1;
	}
	
	public MyTree(LocationAST x) {
		super();
		next = null;
		child = null;
		node = null;
		name = null;
		parity = false;
		node = x;
	}
	
	public MyTree clone() {
		MyTree x = new MyTree();
		if (node!=null) x.setNode(node);
		if (name!=null) x.setName(name);
		x.setParity(parity);
		
		if (importedElements!=null) { List<LocationAST> g = new LinkedList<LocationAST>(); g = importedElements; 
										x.setImportedElements(g);}
		if (notImportedElements!=null) { List<LocationAST> g = new LinkedList<LocationAST>(); g = notImportedElements; 
										x.setNotImportedElements(g);}
		if (child!=null) x.setChild(child.clone());
		if (next!=null) x.setNextSibling(next.clone());
		return x;
	}
	
	public void initializeName() {  				// works for Groups only!
		LocationAST x = node;
		if (x==null) return;
		while(x.getFirstChild()!=null) x = x.getFirstChild();
		name = "group "+x.getText();
	}
	
	public MyTree getNextSibling() {
		return next;
	}

	public void setNextSibling(MyTree next) {
		this.next = next;
	}

	public MyTree getChild() {
		return child;
	}

	public void setChild(MyTree child) {
		this.child = child;
	}

	public LocationAST getNode() {
		return node;
	}

	public void setNode(LocationAST node) {
		this.node = node;
	}

	public boolean getParity() {
		return parity;
	}

	public void setParity(boolean parity) {
		this.parity = parity;
	}
	
	public boolean hasSibling(){
		return (next!=null);
	}
	
	public boolean hasChild(){
		return (child!=null);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<LocationAST> getImportedElements() {
		return importedElements;
	}

	public void setImportedElements(List<LocationAST> elements) {
		this.importedElements = elements;
	}

	public List<LocationAST> getNotImportedElements() {
		return notImportedElements;
	}

	public void setNotImportedElements(List<LocationAST> notImportedElements) {
		this.notImportedElements = notImportedElements;
	}

	public int getBehaviourNumber() {
		return behaviourNumber;
	}

	public void setBehaviourNumber(int behaviourNumber) {
		this.behaviourNumber = behaviourNumber;
	}
	
	
}

final class IntWrapper{
	int value;

	public IntWrapper(int value) {
		super();
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
}

public class ConfigurationIterator {

	LocationAST 		module;
	Set<IntWrapper>		 			behaviours = new HashSet<IntWrapper>();
/*	boolean		typeParity,
				templateParity,
				constParity,
				testcaseParity,
				altstepParity,
				functionParity,
				signatureParity,
				moduleparParity;*/
	MyTree 		groups;
//	LocationAST groups;
	List<LocationAST> allGroups;
	int exponent;
	int runner;
	int runnerLimit;
	int blaster;
	Set<LocationAST> alreadyConsidered = new HashSet<LocationAST>();
	
	public ConfigurationIterator(LocationAST module) {
		super();
		this.module = module;
	}
	
	
	private int processCountSubtrees(MyTree x)
	{
		if (x == null) return 0;
		return 1+(processCountSubtrees(x.getChild()))+(processCountSubtrees(x.getNextSibling()));
		
	}
	
	
	public void setUpIterator()
	{
		int exponent = processCountSubtrees(groups.getChild());
		runnerLimit = (int)Math.pow(2,exponent);
		runner = -1;
	}
	
	public boolean hasNext()
	{
		if (runner < runnerLimit) return true;
		return false;
	}
	
	private  int blast()
	{
		blaster++;
		int theta = runner/((int)Math.pow(2,blaster));
		int h = theta%2;
		
		return h;
	}
	private MyTree getModifiedTree(){
		MyTree x = groups.clone();
		blaster = -1;
		processGetModifiedTree(x.getChild());
		
		return x;
	}
	
	
	private void processGetModifiedTree(MyTree x)
	{
		if (x==null) return;
		if (blast()==0) x.setParity(false);
		else x.setParity(true);
		processGetModifiedTree(x.getChild());
		processGetModifiedTree(x.getNextSibling());
		
	}
	
	private String descriptions(LocationAST x)
	{
		
		LocationAST y = LocationAST.resolveParentsUntilType(x, TTCN3ParserTokenTypes.TypeDef);
		if (y!=null) return "type";
		y = LocationAST.resolveParentsUntilType(x, TTCN3ParserTokenTypes.TemplateDef);
		if (y!=null) return "template";
		y = LocationAST.resolveParentsUntilType(x, TTCN3ParserTokenTypes.ConstDef);
		if (y!=null) return "const";
		y = LocationAST.resolveParentsUntilType(x, TTCN3ParserTokenTypes.TestcaseDef);
		if (y!=null) return "testcase";
		y = LocationAST.resolveParentsUntilType(x, TTCN3ParserTokenTypes.AltstepDef);
		if (y!=null) return "altstep";
		y = LocationAST.resolveParentsUntilType(x, TTCN3ParserTokenTypes.FunctionDef);
		if (y!=null) return "function";
		y = LocationAST.resolveParentsUntilType(x, TTCN3ParserTokenTypes.SignatureDef);
		if (y!=null) return "signature";
		y = LocationAST.resolveParentsUntilType(x, TTCN3ParserTokenTypes.ModuleParDef);
		if (y!=null) return "modulepar";
//		behaviours.add(new intWrapper(TTCN3ParserTokenTypes.TypeDef));
//		behaviours.add(new intWrapper(TTCN3ParserTokenTypes.TemplateDef));
//		behaviours.add(new intWrapper(TTCN3ParserTokenTypes.ConstDef));
//		behaviours.add(new intWrapper(TTCN3ParserTokenTypes.TestcaseDef));
//		behaviours.add(new intWrapper(TTCN3ParserTokenTypes.AltstepDef));
//		behaviours.add(new intWrapper(TTCN3ParserTokenTypes.FunctionDef));
//		behaviours.add(new intWrapper(TTCN3ParserTokenTypes.SignatureDef));
//		behaviours.add(new intWrapper(TTCN3ParserTokenTypes.ModuleParDef));
//		
		
		
		
		return "";
	}
	
	private String iterationOne(MyTree x)
	{
		if (x==null) return "";
		String ret = "";
		boolean goInChilds = false;
		if (x.getParity()==false) 
		{
			Iterator<LocationAST> myIterator = x.getNotImportedElements().iterator();
			while (myIterator.hasNext())
			{
				LocationAST y = myIterator.next();
				y = getRightName(y);
				ret = ret + descriptions(y) + " " + y.getText() + ";\n";
			}
			goInChilds = true;
		}
		else 
		{
			int killer = 0;
			killer = killer + x.getImportedElements().size();
			killer = killer + yetAnotherProcess(x.getChild());
			if (killer == 0)  ret = x.getName() + ";\n";	
			
		}
		
		
		if (goInChilds) return ret + iterationOne(x.getChild()) + iterationOne(x.getNextSibling());
		else return ret + iterationOne(x.getNextSibling());
		//return ret;
	}
	
	
	
	
	
	private String yetAnotherSpecialCaseTreatment(MyTree x)
	{
		//String ret = "";
		//if (x.getParity()==false)
		//{
			return iterationOne(x);
		//}
		//return ret;
	}
	
	private String processElements(MyTree x)
	{
		String ret ="";
		if (x==null) return ret;
		if (x.getParity()== true)
		{
			if (x.getNode()!=null) {
				
				
				// Auch bei Gruppen keine Schachtelungen erlaubt!!
				
				
			ret = ret + x.getName();				//change:ret = ret + x.getName() + " all"; 
			//String s = processElements(x.getChild());
			String s = yetAnotherSpecialCaseTreatment(x.getChild());
			//String s = "";
			String t = "";
			List<LocationAST> u = x.getNotImportedElements();
			Iterator<LocationAST> myIterator = u.iterator();
			while (myIterator.hasNext())
			{
				LocationAST ast = myIterator.next();
				alreadyConsidered.add(ast);
				ast = getRightName(ast);
				
				t = t + descriptions(ast) +" " +ast.getText();
				if (myIterator.hasNext()) t = t + ";\n"; else t = t + ";\n";
				// change: if (myIterator.hasNext()) t = t + ",\n"; else t = t + ";\n";
			}
			
			
			if (s.equals("")) { 
				if (!t.equals("")) ret = ret + " except {\n" + t + "}\n";
				else ret = ret + ";\n";
			
			
			
			}
			else
			{
				ret = ret + " except {\n" + s + t + "}\n";
				
			}
			}
			else {						// special handling for non-groups
				
				
				
				ret = ret + x.getName() + " all";				 
				
				
				String dyer = "";
				Iterator<LocationAST> myIterator = x.getNotImportedElements().iterator();
				
				while (myIterator.hasNext())
				{
					LocationAST u = myIterator.next();
					dyer = dyer + getRightName(u).getText()+", ";      
				}
				dyer = dyer + processExceptions(x.getChild());
				if (dyer.length()>0) dyer = (String)dyer.subSequence(0, dyer.length()-2);
				
				if (dyer.length()>0) ret = ret + " except " + dyer +"; \n"; else ret = ret + ";"; 
				
				
				
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		}
			
			
			
			
			
			
			else {
			String catcher="";
			List<LocationAST> u = x.getImportedElements();
			Iterator<LocationAST> myIterator = u.iterator();
			while (myIterator.hasNext())
			{
				LocationAST ast = myIterator.next();
				
//				boolean stepOut = false;
//				Iterator<LocationAST> z = alreadyConsidered.iterator();
//				while (z.hasNext()) { LocationAST pe = z.next();
//					if (pe.equals(ast)) stepOut = true;
//				}
//				if (stepOut) continue;
				
				ast = getRightName(ast);
				ret = ret + descriptions(ast)+ " "+ast.getText(); catcher = catcher + ast.getText();
				if (myIterator.hasNext()) ret = ret + ";\n";    // CHANGE: semicolon
			}
			if (!ret.equals("")) ret = ret + ";\n"; //else ret = ret + "\n";
			//if (catcher.equals("")) ret = ret + "\n";
			}
		
		if (x.getParity()==false) ret = ret + processElements(x.getChild());
		ret = ret + processElements(x.getNextSibling());
		return ret;
	}
	
	private LocationAST getRightName(LocationAST x)
	{
		LocationAST ret = x;
		while (ret.getFirstChild()!=null)
		{
			if (ret.getType()==TTCN3ParserTokenTypes.Type) ret = ret.getNextSibling();
			ret = ret.getFirstChild();
		}
//		ret = ret.getNextSibling();
//		while ((ret!=null)&&(ret.getFirstChild()!=null))
//		{
//			//if (ret.getType()==TTCN3ParserTokenTypes.Type) break;
//			ret = ret.getFirstChild();
//		}
//		
		
		return ret;
	}
	
	
//	private boolean easyCase(myTree x)   // checks if there are any non-imported elements
//	{
//		if (x==null) return true;
//		if (!x.getNotImportedElements().isEmpty()) return false;
//		if (!easyCase(x.getChild())) return false;
//		if (!easyCase(x.getNextSibling())) return false;
//		return true;
//	}
	
	
	private String makeUp(MyTree x)
	{
		
//		if (easyCase(groups.getChild())) return "import from "+x.getName()+" all;";
		
		
		
		
		String ret = "";
	//	do {
			String appender = "";
			appender = processElements(x.getChild());
			
			
			
			//if (!appender.equals("")) ret = ret + appender;
		//	x = x.getNextSibling();
	//	} while (x.hasSibling());
		
		if (!appender.equals("")) ret = "import from " + x.getName() + " {\n" + appender + "}\n";
		
		
		
		return ret;
	}
	
	private String processExceptions(MyTree x) // process the handling for subgroups of behaviours 
	{
		if (x==null) return "";
		
		String ret = "";
		int killer = 0;
		killer = killer + x.getImportedElements().size();
		killer = killer + yetAnotherProcess(x.getChild());
		if (killer == 0)  ret = x.getName().subSequence(6, x.getName().length()) + ", ";
		else {
			Iterator<LocationAST> myIterator = x.getNotImportedElements().iterator();
			while (myIterator.hasNext())
			{
				LocationAST bla = myIterator.next();
				ret = ret + getRightName(bla).getText() + ", ";
			}
		}
		if (killer != 0) ret = ret + processExceptions(x.getChild());
		ret = ret + processExceptions(x.getNextSibling());
		
		
		return ret;
	}
	
	private int yetAnotherProcess(MyTree x)
	{
		if (x==null) return 0;
		return x.getImportedElements().size() + yetAnotherProcess(x.getChild()) + yetAnotherProcess(x.getNextSibling());
	}
	
	
	
	
	
	private void processRuleOut (LocationAST what, MyTree tree, MyTree compare, Set<LocationAST> outRuler)
	{
		if (tree==null) return;
		if (tree.equals(compare)) return;
		Iterator<LocationAST> myIterator = tree.getImportedElements().iterator();
		while (myIterator.hasNext())
		{
			LocationAST u = myIterator.next();
			if (u.equals(what)) myIterator.remove();
			
			
		}
		
		processRuleOut(what, tree.getChild(),compare, outRuler);
		processRuleOut(what, tree.getNextSibling(),compare, outRuler);
		
	}
	
	private void ruleOut(MyTree y)
	{
		//myTree y = x.getChild();
		Set<LocationAST> outRuler = new HashSet<LocationAST>();
		while(y!=null)
		{
			if (y.getNode()!=null) {y = y.getNextSibling(); continue;}
			if (y.getParity()==true) {y = y.getNextSibling(); continue;}
			List<LocationAST> r = y.getImportedElements();
			Iterator<LocationAST> myIterator = r.iterator();
			while (myIterator.hasNext())
			{
				LocationAST er = myIterator.next();
				processRuleOut (er, y.getNextSibling(), y, outRuler);
				processRuleOut (er, y.getChild(), y, outRuler);
			}
			
			
			
			
			y = y.getNextSibling();
		}
		
		
	}
	
	public String getNext()
	{
		runner++;
		MyTree x = getModifiedTree();
		killConsidered(x);
		ruleOut(x.getChild());
		
		
		return makeUp(x);
	}
	
	private void killConsidered(MyTree x)
	{
		MyTree y = x.getChild();
		while (true)
		{
			if (y.getNode()==null)
			{
				//myTree s = x.getChild();
				List<LocationAST> t = y.getImportedElements();
				Iterator<LocationAST> myIterator = t.iterator();
				while (myIterator.hasNext())
				{
					LocationAST z = myIterator.next();
					if (isConsidered(z,y)) myIterator.remove();
					
				}
				
				
			}
			if (y.getNextSibling()==null) break;
			y = y.getNextSibling();
			
		}
		
		
		
		
	}
	
	private boolean isConsidered(LocationAST totest, MyTree tree)
	{
		if (tree==null) return false;
		if ((tree.getNode()!=null) 
				&&(tree.getParity()))
		{
		List<LocationAST> x = tree.getImportedElements();
		Iterator<LocationAST> myIterator = x.iterator();
		while(myIterator.hasNext())
		{
			LocationAST u = myIterator.next();
			if (u.equals(totest)) return true;
		}
		}
		if (isConsidered(totest, tree.getChild())) return true;
		if (isConsidered(totest, tree.getNextSibling())) return true;
		return false;
		
		
	}
	
	
	
	
	
	public void initialize() {
		behaviours.add(new IntWrapper(TTCN3ParserTokenTypes.TypeDef));
		behaviours.add(new IntWrapper(TTCN3ParserTokenTypes.TemplateDef));
		behaviours.add(new IntWrapper(TTCN3ParserTokenTypes.ConstDef));
		behaviours.add(new IntWrapper(TTCN3ParserTokenTypes.TestcaseDef));
		behaviours.add(new IntWrapper(TTCN3ParserTokenTypes.AltstepDef));
		behaviours.add(new IntWrapper(TTCN3ParserTokenTypes.FunctionDef));
		behaviours.add(new IntWrapper(TTCN3ParserTokenTypes.SignatureDef));
		behaviours.add(new IntWrapper(TTCN3ParserTokenTypes.ModuleParDef));
	groups = new MyTree();	
	groups.setNode(module);
	LocationAST moduleName = module;
	while (moduleName.getFirstChild()!=null) moduleName = moduleName.getFirstChild();
	groups.setName(moduleName.getText());
	allGroups = getGroups();
	// gruppen adden; altsteps, templates usw. als gruppen handhaben!!
	boolean ex = false;
	MyTree runner = groups;
	List<LocationAST> allGroupsClone = new LinkedList<LocationAST>();
	allGroupsClone = allGroups;
	for (IntWrapper i : behaviours) {
		if (ex==true) {if (fillUpBehaviour(groups, runner, i.getValue(), allGroupsClone, false)) runner = runner.getNextSibling();}
		else { if (fillUpBehaviour(groups, runner, i.getValue(), allGroupsClone, true)) {ex = true; runner= runner.getChild();}
	}
	}
	Iterator<LocationAST> myIterator = allGroups.iterator();
	while (myIterator.hasNext()) {
		LocationAST x = myIterator.next();
		if (isSubgroup(x, allGroups)) continue;
		MyTree temp = new MyTree(x);
		temp.initializeName();
		fillUpGroup(temp, allGroups);
		if (ex==true) {runner.setNextSibling(temp); runner = temp;}
		else {runner.setChild(temp); ex = true; runner = temp;}	
	}
	// now we should have a nice tree..
	
	// and it seems to work..
	
	}
	
	
	public void integrateElements (LocationAST[] imported, LocationAST[] notImported)
	{
		List<LocationAST> imp = new LinkedList<LocationAST>();
		List<LocationAST> nonImp = new LinkedList<LocationAST>();
		for (int i = 0; i<imported.length; i++) imp.add(imported[i]);
		for (int i = 0; i<notImported.length; i++) nonImp.add(notImported[i]);
		processIntegrateElements(groups.getChild(), imp, nonImp, false);
	}
	
	private void processIntegrateElements(MyTree group, List<LocationAST> imported, List<LocationAST> notImported, boolean getOneDown)
	{
		if (group==null) return;
		
		Iterator<LocationAST> myIterator = imported.iterator();
		List<LocationAST> myImported = new LinkedList<LocationAST>();
		while (myIterator.hasNext())
		{
			LocationAST element = myIterator.next();
			if (group.getNode()==null)
			{
				if (isElementOfBehaviour(group.getBehaviourNumber(), element)
					//	&&  
						//	!isGroupInBetween(group.getNode(), element, allGroups)   occurs twice or more..!!!
								) myImported.add(element);
			}
			else 
			{
				if (isElementInGroup(group.getNode(), element)
						&&  !isGroupInBetween(group.getNode(), element, allGroups)) myImported.add(element);
			}
		
		}
		
		Iterator<LocationAST> myNotIterator = notImported.iterator();
		List<LocationAST> myNotImported = new LinkedList<LocationAST>();
		
		while (myNotIterator.hasNext())
		{
			LocationAST element = myNotIterator.next();
			if (group.getNode()==null)
			{
				if (isElementOfBehaviour(group.getBehaviourNumber(), element)
						//&&  !isGroupInBetween(group.getNode(), element, allGroups)
							) myNotImported.add(element);
			}
			else 
			{
				if (isElementInGroup(group.getNode(), element)
						&&  !isGroupInBetween(group.getNode(), element, allGroups)) myNotImported.add(element);
			}
		
		}
		group.setImportedElements(myImported);
		group.setNotImportedElements(myNotImported);
		processIntegrateElements(group.getChild(), imported, notImported, false);
		if (!getOneDown) processIntegrateElements(group.getNextSibling(), imported, notImported, false);
		
	}
	
// problem hier???	
	private boolean isElementOfBehaviour(int behaviour, LocationAST element)
	{
		if (element==null) return false;
		if (element.getType()==behaviour) return true;
		if (processisElementOfBehaviour(behaviour, element.getFirstChild())) return true;
		//if (isElementOfBehaviour(behaviour, element.getNextSibling())) return true;
		return false;

		
		//LocationAST x = LocationAST.resolveParentsUntilType(element, behaviour);
		//return (x!=null);
	}
	

	
	private boolean processisElementOfBehaviour(int behaviour, LocationAST element)
	{
		if (element==null) return false;
		if (element.getType()==behaviour) return true;
		if (processisElementOfBehaviour(behaviour, element.getFirstChild())) return true;
		if (processisElementOfBehaviour(behaviour, element.getNextSibling())) return true;
		return false;	
	}
	
	
	
	
	private boolean isElementInGroup(LocationAST group, LocationAST element)
	{
		if (group==null) return false;
		if (element==null) return false;
		if (element.equals(group)) return true;
		return (isElementInGroup(group.getFirstChild(), element))||(isElementInGroup(group.getNextSibling(), element));
	}
	
	
	
	private boolean fillUpBehaviour(MyTree searchnode, MyTree group, int type, List<LocationAST> allGroups, boolean setAsChild) {
		if (!areThereAnyElements(searchnode.getNode(), type)) return false;
		MyTree first = new MyTree();
		if (setAsChild) group.setChild(first);
		else group.setNextSibling(first);
		first.setBehaviourNumber(type);
		switch (type)
		{
			case TTCN3ParserTokenTypes.TypeDef: first.setName("type"); break;
			case TTCN3ParserTokenTypes.TemplateDef: first.setName("template"); break;
			case TTCN3ParserTokenTypes.ConstDef: first.setName("const"); break;
			case TTCN3ParserTokenTypes.TestcaseDef: first.setName("testcase"); break;
			case TTCN3ParserTokenTypes.AltstepDef: first.setName("altstep"); break;
			case TTCN3ParserTokenTypes.FunctionDef: first.setName("function"); break;
			case TTCN3ParserTokenTypes.SignatureDef: first.setName("signature"); break;
			case TTCN3ParserTokenTypes.ModuleParDef: first.setName("modulepar"); break;
		}
		Iterator<LocationAST> myIterator = allGroups.iterator();
		LocationAST y;
		y = null;
		boolean reasonToStepOut = false;
		while (myIterator.hasNext())
		{
			y =myIterator.next();
			//if (isGroupInBetween(group.getNode(), y, allGroups)) continue;
			if (isSubgroupOfBehaviourClass(y, type)) {
				reasonToStepOut = true;
				Iterator<LocationAST> mySecondIterator = allGroups.iterator();
				while (mySecondIterator.hasNext()) {
					LocationAST x = mySecondIterator.next();
					if (x.equals(y)) continue;
					if ((isSubgroupOfBehaviourClass(x, type))&&(isSubgroup(x, y)))
					reasonToStepOut = false;
				}
				if (reasonToStepOut==true) break; 
			}
		}
		
		if (reasonToStepOut==false) return true;    //this is likely to happen now..
		if (y==null) return true;
		MyTree second = new MyTree(y);
		myIterator.remove();
		first.setChild(second);
		second.initializeName();
		fillUpGroup(second, allGroups);
		while (myIterator.hasNext()) {
			y = myIterator.next();
			if (!isSubgroupOfBehaviourClass(y, type)) continue;
			reasonToStepOut = true;
			Iterator<LocationAST> mySecondIterator = allGroups.iterator();
			while (mySecondIterator.hasNext()) {
				LocationAST x = mySecondIterator.next();
				if (x.equals(y)) continue;
				if ((isSubgroupOfBehaviourClass(x, type))&&(isSubgroup(x, y)))
				reasonToStepOut = false;
			}
			if (reasonToStepOut==false) continue;
			
			MyTree third = new MyTree(y);
			myIterator.remove();
			second.setNextSibling(third);
			third.initializeName();
			fillUpGroup(third, allGroups);
			second = third;
		}
		
		
		return true;
		
	}
	
	private void fillUpGroup(MyTree group, List<LocationAST> allGroups) {
		if (!hasSubgroups(group.getNode(), allGroups)) return;
		Iterator<LocationAST> myIterator = allGroups.iterator();
		LocationAST y;
		y = null;
		while (myIterator.hasNext())
		{
			y =myIterator.next();
			
			if (isSubgroup(group.getNode(), y)) {
				if (!isGroupInBetween(group.getNode(), y, allGroups)) break;
			}
		
		
		}
		if (y==null) return;    //this should not happen!
		
		MyTree first = new MyTree(y);
		myIterator.remove();
		first.initializeName();
		group.setChild(first);
		
		while (myIterator.hasNext())
		{
			LocationAST x = myIterator.next();
			if (!isSubgroup(group.getNode(), x)) continue;
			if (isGroupInBetween(group.getNode(), y, allGroups)) continue;
			MyTree temp = new MyTree(x);
			myIterator.remove();
			temp.initializeName();
			fillUpGroup(temp, allGroups);
			first.setNextSibling(temp);
			first = temp;
		}
	}
	
	private boolean isGroupInBetween(LocationAST group, LocationAST subgroup, List<LocationAST> allGroups)
	{
//		Iterator<LocationAST> myIterator = allGroups.iterator();
//		while (myIterator.hasNext()) {
//			LocationAST x = myIterator.next();
//			if (isSubgroup(group, x)&&isSubgroup(x, subgroup)) return true;
//		}
//		return false;
		if (group==null) return false;
		return processX (group.getFirstChild(), subgroup);
		
		
		
		
	}
	
	private boolean processX (LocationAST group, LocationAST subgroup) {
		if (group==null) return false;
		if ((group.getType()==TTCN3ParserTokenTypes.GroupDef)&&(processY(group.getFirstChild(),subgroup))) return true;
		if (processX(group.getFirstChild(), subgroup)) return true;
		if (processX(group.getNextSibling(), subgroup)) return true;
		return false;
	}
	
	
	private boolean processY(LocationAST group, LocationAST subgroup) {
		if (group==null) return false;
		if (group.equals(subgroup)) return true;
		if (processY(group.getFirstChild(), subgroup)) return true;
		if (processY(group.getNextSibling(), subgroup)) return true;
		return false;
	}
	
	private boolean areThereAnyElements(LocationAST callee, int type) {
		
		
		if (callee==null) return false;
		if (callee.getType() == TTCN3ParserTokenTypes.TTCN3Module) {callee = callee.getFirstChild();
			if (callee==null) return false;
		}
		
		if (callee.getType()==type) return true;
		boolean ret = false;
		if (areThereAnyElements(callee.getFirstChild(), type)) ret = true;
		if (areThereAnyElements(callee.getNextSibling(), type)) ret = true;
		return ret;
	}
	
	private boolean isSubgroupOfBehaviourClass(LocationAST group,int type) {
		boolean ret = hasElementsOfType(group, type);
		if (ret == false) return false;
		for (IntWrapper i : behaviours)
		{
			int x = i.getValue();
			if (x==type) continue;
			if (hasElementsOfType(group, x)) ret = false;
		}
		return ret;
		
	}
	
	private boolean hasElementsOfType(LocationAST group, int type) {
		if (group == null) return false;
		if (group.getType()==type) return true;
		boolean ret = false;
		if ((group.getNextSibling()!=null)&&(hasElementsOfType(group.getNextSibling(), type))) ret = true;
		if ((group.getFirstChild()!=null)&&(hasElementsOfType(group.getFirstChild(),type))) ret = true;
		return ret;
	}
	
	
	private boolean isSubgroup(LocationAST group, List<LocationAST> allGroups) {
		Iterator<LocationAST> runner = allGroups.iterator();
		while (runner.hasNext())
		{
			LocationAST x = runner.next();
			if ((!group.equals(x))&&(isSubgroup(x,group))) return true;
		}
		return false;
	}

	private boolean hasSubgroups(LocationAST group, List<LocationAST> allGroups){
		Iterator<LocationAST> runner = allGroups.iterator();
		while (runner.hasNext())
		{
			LocationAST x = runner.next();
			if ((!group.equals(x))&&(isSubgroup(group,x))) return true;
		}
		return false;
	}
	
	
	private boolean isSubgroup(LocationAST group, LocationAST subgroup){
	boolean ret = false;	
	if (group.equals(subgroup)) ret = true;
	if ((group.getFirstChild()!=null) && (isSubgroup(group.getFirstChild(),subgroup))) ret = true;
	if ((group.getNextSibling()!=null) && (isSubgroup(group.getNextSibling(),subgroup))) ret = true;	
	return ret;	
	}
	
	
	
	private List<LocationAST> getGroups()
	{
		List<LocationAST> ret = new LinkedList<LocationAST>();
		
		LocationAST x = module.getFirstChild();
		if (x==null) return null;
		processGetGroups(ret,x);
		cleanList(ret);
		return ret;
	}
	
	private void processGetGroups(List<LocationAST> ret, LocationAST x)
	{
		if (x.getType()==TTCN3ParserTokenTypes.GroupDef) ret.add(x);
		if (x.getNextSibling()!=null) processGetGroups(ret, x.getNextSibling());
		if (x.getFirstChild()!=null) processGetGroups(ret, x.getFirstChild());
	}
	
	private void cleanList(List<LocationAST> x)
	{
		Iterator<LocationAST> iterator = x.iterator();
		while (iterator.hasNext())
		{
			if (iterator.next()==null) iterator.remove();
		}
	}
	
}