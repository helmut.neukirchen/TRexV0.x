package de.ugoe.cs.swe.trex.core.refactoring.core;



import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

public class TTCN3ParameterizeTemplateRefactoring extends ProcessorBasedRefactoring{

	RefactoringProcessor processor;
	private String renameText="";
	private boolean preserveSourceTemplates = false;
		
	public TTCN3ParameterizeTemplateRefactoring(TTCN3ParameterizeTemplateProcessor processor) {
		super(processor);
		this.processor = processor;
	}
	
	// ---------------------------------------------------------------------------
	
	public RefactoringProcessor getProcessor() {
		return processor;
	}

	// ---------------------------------------------------------------------------

	public String getRenameText() {
		return renameText;
	}

	// ---------------------------------------------------------------------------

	public void setRenameText(String renameText) {
		this.renameText = renameText;
	}	
	
	// ---------------------------------------------------------------------------
	public boolean isPreserveSourceTemplates() {
		return preserveSourceTemplates;
	}

	// ---------------------------------------------------------------------------
	public void setPreserveSourceTemplates(boolean preserveTemplates) {
		this.preserveSourceTemplates = preserveTemplates;
	}

	// ---------------------------------------------------------------------------

	

}
