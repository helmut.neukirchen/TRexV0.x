package de.ugoe.cs.swe.trex.core.refactoring.core;

import org.eclipse.ltk.core.refactoring.participants.RenameProcessor;
import org.eclipse.ltk.core.refactoring.participants.RenameRefactoring;

public class TTCN3RenameRefactoring extends RenameRefactoring {

	String renameText;
	
	// ---------------------------------------------------------------------------

	public TTCN3RenameRefactoring(RenameProcessor processor) {
		super(processor);
	}

	// ---------------------------------------------------------------------------

	public String getRenameText() {
		return renameText;
	}

	// ---------------------------------------------------------------------------

	public void setRenameText(String renameText) {
		this.renameText = renameText;
	}
	
}
