package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.NullChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEditGroup;

import antlr.RecognitionException;
import antlr.collections.AST;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3FormatterTreeParser;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterParameters;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterTools;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;

	public class TTCN3DecomposeTemplateProcessor extends RefactoringProcessor{

		Symbol declarationSymbol;
		LocationAST templateDefAST ;
		LocationAST recordNode ;
		List<AST> templateFields = new ArrayList<AST>();
		List<String> newDecomposedTemplates = new ArrayList<String>();
		List<String> decomposedTemplatesNames = new ArrayList<String>();
		List<String> modifiedDecomposedTemplatesNames = new ArrayList<String>();
		Map<String, String> recordFields = new HashMap<String, String>();
		private Set<IFile> modifiedFiles = new HashSet<IFile>();
		private int numberOfChanges = 0;

		// ---------------------------------------------------------------------------
		
		public TTCN3DecomposeTemplateProcessor(TTCN3Editor editor) {
			final LocationAST editorAST = TTCN3Editor.getEditorAST(editor);
			if(editorAST != null)
				initialize(editorAST);
		}

		// ---------------------------------------------------------------------------
		
		public TTCN3DecomposeTemplateProcessor(LocationAST ast) {
			initialize(ast);
		}

		// ---------------------------------------------------------------------------
		
		private void initialize(LocationAST ast) {
			
			if (ast == null)
				return;
			
			declarationSymbol = getDeclarationSymbol(ast);
			templateDefAST = LocationAST.resolveParentsUntilType(ast,
					TTCN3ParserTokenTypes.TemplateDef);
	
			if (templateDefAST == null)
				return;
			//	It does not need to be decomposed so simple
			if( ASTUtil.getDescendantNode(templateDefAST).getType() != TTCN3ParserTokenTypes.IDENTIFIER) 
				return;
			//	only for StructuredTypeDef 		
			if(ASTUtil.getTypeSymbol(templateDefAST).getStructuredType()!= null){
						
				LocationAST templateType = ASTUtil.getDescendantNode(templateDefAST);
				Scope recordScope =	templateType.getScope();
				Symbol recordSymbol = recordScope.resolve(templateType);
				recordNode = recordSymbol.getDeclarationNode();
				templateFields = ASTUtil.getFields(templateDefAST);
				numberOfChanges = findChangeTimes();
			}
						
		}
		//	---------------------------------------------------------------------------

		private Symbol getDeclarationSymbol(LocationAST identifierNode) {
			Scope scope = identifierNode.getScope();
			if (scope == null)
				return null;
			return scope.resolve(identifierNode);
		}
		// ---------------------------------------------------------------------------
		
		public RefactoringStatus checkFinalConditions(IProgressMonitor pm, CheckConditionsContext context) throws CoreException, OperationCanceledException {
			return new RefactoringStatus();
		}

		// ---------------------------------------------------------------------------
		
		@Override
		public RefactoringStatus checkInitialConditions(IProgressMonitor pm) 
		throws CoreException, OperationCanceledException {
			
			if (templateDefAST == null || templateDefAST.getType() != TTCN3ParserTokenTypes.TemplateDef) {
				return RefactoringStatus
				.createFatalErrorStatus("You _must_ select the identifier "
						+ "of a template declaration to perform the decompose refactoring.");
			}
			if(recordNode == null ){
				return RefactoringStatus
				.createFatalErrorStatus("This template is simple can not be decomposed");
			}
			if(decomposedTemplatesNames == null ){
				return RefactoringStatus
				.createFatalErrorStatus("This type has no fields ");
			}
			if(templateFields == null ){
				return RefactoringStatus
				.createFatalErrorStatus("This template has no fields ");
			}
			if(ASTUtil.isParameterizedTemplate(templateDefAST)){
				return RefactoringStatus
				.createFatalErrorStatus("This template is parameterized template ");
			}
			
			AST child = templateDefAST.getFirstChild();
			while (child != null) {
				if (child.getType() == TTCN3LexerTokenTypes.DerivedDef) {
					return RefactoringStatus
					.createFatalErrorStatus("This template is modified template ");
				}
				child = child.getNextSibling();
			}
	 
			return new RefactoringStatus();
			
		}

		// ---------------------------------------------------------------------------
		
		@Override
		public Change createChange(IProgressMonitor pm) throws CoreException, OperationCanceledException {
			CompositeChange change = decomposeTemplateChange(pm);
			return change == null ? new NullChange() : change;
		}

		// ---------------------------------------------------------------------------

		private CompositeChange decomposeTemplateChange(IProgressMonitor pm) {
			
			if(numberOfChanges>0){
				//2*findChangeTimes() depends on changes (replace)in base template 
				//and (insert)new decomposed templates 
			pm.beginTask("Decompose Template", 2*numberOfChanges);
			CompositeChange compositeChange = new CompositeChange("Decompose Template");
			
			String declarationFileName1 = getDeclarationSymbol(recordNode).getSurroundingScope()
			.getAssociatedFilename();
			IFile declarationFile1 = getProjectFile(declarationFileName1);
			modifiedFiles.add(declarationFile1);
			
			String declarationFileName = declarationSymbol.getSurroundingScope()
			.getAssociatedFilename();
			IFile declarationFile = getProjectFile(declarationFileName);
			modifiedFiles.add(declarationFile);
			
			
			MultiTextEdit declarationMultiEdit = new MultiTextEdit();
			TextFileChange declarationFileChange = new TextFileChange(
					declarationFile.getName(), declarationFile);
			declarationFileChange.setEdit(declarationMultiEdit);
			declarationFileChange.setTextType("ttcn3");
			compositeChange.add(declarationFileChange);
			
			TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
			formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
			formatterWalker
					.setFormatterParameters(new TTCN3FormatterParameters());
			
			LocationAST templateBody ;
			String result ;
			TTCN3FormatterParameters fparam =new TTCN3FormatterParameters();
			TTCN3FormatterTools line = new TTCN3FormatterTools(fparam);
			TTCN3DecomposeTemplateRefactoring refactoring = (TTCN3DecomposeTemplateRefactoring)getRefactoring();
			modifiedDecomposedTemplatesNames = refactoring.getNamesOfDecomposedTemplates();
			
			
			String param1, param2, fieldName;
			String insertTab;
			for(int i = 0; i < decomposedTemplatesNames .size(); i++){
				param1 = decomposedTemplatesNames.get(i);
				
				for(int k = 0; k < templateFields .size(); k++){
					param2 = ASTUtil.getDescendantNode(templateFields.get(k)).getText();
					
					if(param1.equals(param2)){
						templateBody = ASTUtil.findTemplateBodyFromTemplateDef((LocationAST) templateFields.get(k));
						fieldName = "a_"+param1;
						if(modifiedDecomposedTemplatesNames.size() == decomposedTemplatesNames.size()){
							if(!modifiedDecomposedTemplatesNames.get(i).equals(decomposedTemplatesNames.get(i)))
								fieldName = modifiedDecomposedTemplatesNames.get(i);
							
						}
						result = " template "+recordFields.get(param1)+"  "+fieldName+" := ";
						
						try {
							insertTab = insertTab(formatterWalker.pr_TemplateBody(templateBody).toString());
							result += insertTab + line.getNewline()+ line.getNewline()+line.getSpacing(1);

							newDecomposedTemplates.add(result);
						} 
						catch (RecognitionException e) {
							e.printStackTrace();
						}
						addReplaceEditForField(templateBody, fieldName , "Replace field "+decomposedTemplatesNames.get(i)+" by template reference"
							, declarationMultiEdit, declarationFileChange);
						pm.worked(1);
					}
				}
				
			}
			
			for(int k = 0; k < newDecomposedTemplates.size(); k++){
				if(modifiedDecomposedTemplatesNames.size() > 0)
					addInsertEditForNewTemplate(templateDefAST, newDecomposedTemplates.get(k)
						, "Create template "+modifiedDecomposedTemplatesNames.get(k), declarationMultiEdit, declarationFileChange);
				else
					addInsertEditForNewTemplate(templateDefAST, newDecomposedTemplates.get(k)
							, "Create template ", declarationMultiEdit, declarationFileChange);
					
				pm.worked(1);
			}
			
			return compositeChange;
			}
			else 
				return null;
		}

		// ---------------------------------------------------------------------------
		
		private String insertTab(String body) {
			String newBody = "";
			TTCN3FormatterParameters fparam =new TTCN3FormatterParameters();
			TTCN3FormatterTools line = new TTCN3FormatterTools(fparam);
			List<String> bodyLines = new ArrayList<String>();
			String temp;
			int lastNewLinePosition = 0;
			
			for (int j = 0; j < body.length(); j++) {
				String aChar = body.substring(j, j + 1);
				if (aChar.matches("\n")) {
					
					if(lastNewLinePosition == 0)
						temp = body.substring(lastNewLinePosition,j+1);
					else						
						temp = line.getSpacesCountString()+line.getSpacing(0)+body.substring(lastNewLinePosition,j+1);
					bodyLines.add(temp);
					lastNewLinePosition = j+1;
					
				} 
				
			}
			if(lastNewLinePosition == 0)
				bodyLines.add(body.substring(lastNewLinePosition,body.length()));//[number] = temp;
			else
				bodyLines.add( line.getSpacesCountString()+line.getSpacing(0)+body.substring(lastNewLinePosition,body.length()));//[number] = temp;
			for (int k = 0; k < bodyLines.size(); k++) {
				newBody += bodyLines.get(k);
			}
			return newBody;
		}

		// ---------------------------------------------------------------------------
		
		private int findChangeTimes() {
	
			if(recordNode!= null ){
				//System.out.println(recordNode.getText());
				LocationAST field = recordNode.getParent().getNextSibling();
				
				while(field!= null){
					String paramName = ASTUtil.getIdentNode(field.getFirstChild().getNextSibling()).getText();
					String paramType = ASTUtil.getDescendantNode(field).getText();
					
					recordFields.put(paramName, paramType);
					decomposedTemplatesNames.add(paramName);
					field = field.getNextSibling();
				}
			}
			if(recordFields.size()>0){
				return recordFields.size();
			}
			else 
				return 0;
		}
		//	---------------------------------------------------------------------------

		private IFile getProjectFile(String filename) {
			IWorkspace ws = ResourcesPlugin.getWorkspace();
			IWorkspaceRoot root = ws.getRoot();
			IProject project = root.getProject();

			IFile file = null;
			if (project == null) {
				IPath path = ws.getRoot().findMember(filename).getFullPath();
				file = ws.getRoot().getFile(path);
			} else {
				file = project.getFile(filename);
			}

			return file;
		}

		// ---------------------------------------------------------------------------
		
		private void addInsertEditForNewTemplate(LocationAST node,String isertText,
				String textEditGroupName, MultiTextEdit multiEdit,
				TextFileChange fileChange) {
			
			InsertEdit insertEdit = new InsertEdit(node.getOffset(), isertText);
			multiEdit.addChild(insertEdit);
			TextEditGroup declarationEditGroup = new TextEditGroup(
					textEditGroupName, insertEdit);
			fileChange.addTextEditGroup(declarationEditGroup);
		}
		
		// ---------------------------------------------------------------------------
		
		private void addReplaceEditForField(LocationAST node, String replaceText,
				String textEditGroupName, MultiTextEdit multiEdit,
				TextFileChange fileChange) {
			
			ReplaceEdit replaceEdit = new ReplaceEdit(node.getOffset(), node
					.getEndOffset()
					- node.getOffset(), replaceText);
			multiEdit.addChild(replaceEdit);
			TextEditGroup declarationEditGroup = new TextEditGroup(
					textEditGroupName, replaceEdit);
			fileChange.addTextEditGroup(declarationEditGroup);
		}

		// ---------------------------------------------------------------------------

		
		@Override
		public Object[] getElements() { // NOPMD by oskar on 12.11.2014 00:51
			// TODO Auto-generated method stub
			return new String[0];
		}

		// ---------------------------------------------------------------------------
		
		@Override
		public String getIdentifier() {
			return "Decompose Template Processor Identifier";
		}

		// ---------------------------------------------------------------------------
		
		@Override
		public String getProcessorName() {
			return "Decompose Template Processor";
		}

		// ---------------------------------------------------------------------------
		
		@Override
		public boolean isApplicable() throws CoreException {
			return (templateDefAST != null && recordNode != null && templateFields != null);
		}

		// ---------------------------------------------------------------------------
		
		@Override
		public RefactoringParticipant[] loadParticipants(RefactoringStatus status, SharableParticipants sharedParticipants) throws CoreException {
			return new RefactoringParticipant[0];
		}
		
		//	---------------------------------------------------------------------------
		
		public Set<IFile> getModifiedFiles() {
			return modifiedFiles;
		}
		
		//	---------------------------------------------------------------------------
		
		public List<String> getNamesOfDecomposedTemplates(){
			String temp  ;
			List<String> decomposedNames = new ArrayList<String>();
			for(int i = 0; i < decomposedTemplatesNames.size(); i++){
				temp = decomposedTemplatesNames.get(i);
				decomposedNames.add(i,"a_"+temp ); 
			}
			return decomposedNames;
		}

		//	---------------------------------------------------------------------------
		
		public List<String> getDecomposedTemplates(){
			return newDecomposedTemplates;
		}

}
