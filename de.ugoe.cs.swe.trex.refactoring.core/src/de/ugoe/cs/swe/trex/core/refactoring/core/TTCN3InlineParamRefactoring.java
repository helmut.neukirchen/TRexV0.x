package de.ugoe.cs.swe.trex.core.refactoring.core;

import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

public class TTCN3InlineParamRefactoring extends ProcessorBasedRefactoring {

	RefactoringProcessor processor;
	private boolean prettyPrintInlinedTemplates = false;

	// ---------------------------------------------------------------------------

	public TTCN3InlineParamRefactoring(TTCN3InlineParamProcessor processor) {
		super(processor);
		this.processor = processor;
	}
	
	// ---------------------------------------------------------------------------
	@Override
	public RefactoringProcessor getProcessor() {
		return processor;
	}

	// ---------------------------------------------------------------------------

	public boolean isPrettyPrintInlinedTemplates() {
		return prettyPrintInlinedTemplates;
	}

	// ---------------------------------------------------------------------------

	public void setPrettyPrintInlinedTemplates(boolean prettyPrintInlinedTemplates) {
		this.prettyPrintInlinedTemplates = prettyPrintInlinedTemplates;
	}


}
