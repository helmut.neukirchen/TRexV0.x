package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.NullChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextChange;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.swt.widgets.Display;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEditGroup;

import antlr.RecognitionException;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3FormatterTreeParser;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.ModuleSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.TemplateSymbol;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterParameters;
import de.ugoe.cs.swe.trex.core.refactoring.core.util.SymbolHelper;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;

public class TTCN3InlineParamProcessor extends TTCN3Inline {

	IFile currfile = null;

	private boolean formalParamsRemoved = false;

	private int inCount = 0;

	IFile nextfile = null;

	private boolean templateRewritten = false;

	private Set<IFile> modifiedFiles = new HashSet<IFile>();

	// ---------------------------------------------------------------------------

	public TTCN3InlineParamProcessor(List<LocationAST> astList) {
		super(astList);
	}

	// ---------------------------------------------------------------------------

	/**
	 * Inline into a single <code>AST</code>. Used by Tests.
	 * 
	 * @param ast
	 *            a reference to the template to inline.
	 */
	public TTCN3InlineParamProcessor(LocationAST ast) {
		loadParamInlinings(ast);
	}

	// ---------------------------------------------------------------------------

	public TTCN3InlineParamProcessor(TTCN3Editor editor) {
		LocationAST ast = TTCN3Editor.getEditorAST(editor);
		loadParamInlinings(ast);
	}

	private void loadParamInlinings(LocationAST ast) {
		// if user is trying to inline based on a template itself proceed as
		// normal
		if (ast.getParent().getParent().getType() == TTCN3LexerTokenTypes.BaseTemplate) {
			loadInlinings(ast);
		} else {
			// try to look it up as a template reference and find the definition
			Symbol symb = ast.getScope().resolve(ast);
			LocationAST def = symb.getDeclarationNode();
			if (def.getParent().getParent().getType() == TTCN3LexerTokenTypes.BaseTemplate) {
				loadInlinings(def);
			} else {
				// shouldn't get here
				MessageDialog.openError(Display.getDefault().getActiveShell(),
						"TTCN-3 Inline Param...",
						"Identifier not a template!\n");
			}
		}
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm)
			throws OperationCanceledException {
		pm.beginTask("Checking initial conditions", this.inList.size() * 2);

		if (inList.size() < 1)
			return RefactoringStatus
					.createFatalErrorStatus("Nothing to inline?!");

		for (Inlining in : this.inList) {
			if (in.symb == null) {
				return RefactoringStatus
						.createFatalErrorStatus("Identifier undeclared!");
			}

			if (!(in.symb instanceof TemplateSymbol)) {
				return RefactoringStatus
						.createFatalErrorStatus("Identifier not a template!");
			}

			LocationAST templateNode = in.symb.getDeclarationNode();

			in.templateDefNode = LocationAST.resolveParentsUntilType(
					templateNode, TTCN3LexerTokenTypes.TemplateDef);

			if (ASTUtil.isParameterizedTemplateReference(in.ast)) {
				in.actualParams = ASTUtil.getActualParameters(in.ast, true);
				in.formalParams = ASTUtil
						.getFormalParametersStr(in.templateDefNode);

				if (in.actualParams.size() != in.formalParams.size()) {
					return RefactoringStatus
							.createFatalErrorStatus("Parameter count doesn't match the declaration!");
				}
			} else {
				return RefactoringStatus
						.createFatalErrorStatus("Identifier not a parameterised reference to a template!");
			}
			pm.worked(1);
		}

		List<String> params = new ArrayList<String>();

		for (LocationAST ast : inList.get(0).actualParams) {
			params.add(ASTUtil.getDescendantText(ast));
		}

		// check all actual params are the same value
		for (Inlining in : this.inList) {
			int index = 0;
			for (LocationAST ast : in.actualParams) {
				if (!ASTUtil.getDescendantText(ast).equals(params.get(index))) {
					return RefactoringStatus
							.createFatalErrorStatus("Actual params of template references are not all the same.");
				}
				index++;
				pm.worked(1);
			}
		}

		pm.done();

		return new RefactoringStatus();
	}

	// ---------------------------------------------------------------------------

	@Override
	public Change createChange(IProgressMonitor pm)
			throws OperationCanceledException, CoreException {
		Change result = null;
		try {
			result = generateInlineParamChange(pm);
		} catch (RefactoringException e) {
			// TODO Is there a better way to invalidate the refactoring in case
			// of exceptions?
			e.printStackTrace();
			// adding a fatal error results in 'no changes' being displayed to
			// the user:
			//
			// result = new NullChange();
			// result.isValid(pm).addFatalError(e.getMessage());
			//
			// But to make the problem more explicite,
			// we throw an exception to the framework:
			throw new RuntimeException(e);
		}
		return result == null ? new NullChange() : result;
	}

	// ---------------------------------------------------------------------------

	private CompositeChange generateInlineParamChange(IProgressMonitor pm)
			throws RefactoringException {
		pm.beginTask("Inline Param", this.inList.size());
		CompositeChange compositeChange = new CompositeChange("Inline Param");
		boolean lastFile = false;

		// reset template rewritten
		templateRewritten = false;

		// loop for all Inlinings
		for (int i = 0; i < this.inList.size(); i++) {
			Inlining in = this.inList.get(i);

			// get the associated file
			ModuleSymbol moduleSymbol = ASTUtil.getModuleSymbol(in.ast);
			currfile = SymbolHelper.getModuleFile(moduleSymbol);

			if (currfile == null)
				continue;

			modifiedFiles.add(currfile);

			// changes in the same file can be done in the same 'TextChange'
			// object
			TextChange textChange = new TextFileChange(currfile.getFullPath()
					.toOSString(), currfile);
			((TextFileChange) textChange)
					.setSaveMode(TextFileChange.FORCE_SAVE);
			textChange.setTextType("ttcn3");
			MultiTextEdit multiEdit = new MultiTextEdit();
			TextEditGroup updateTemplateDefinitionTextEditGroup = new TextEditGroup(
					"update template definition");
			List<TextEditGroup> updateReferenceTextEditGroups = new ArrayList<TextEditGroup>();

			// loop here for all Inlinings that are in the same file
			while (i < this.inList.size()) {
				if (i >= this.inList.size() - 1) {
					lastFile = true;
				}

				LocationAST templateBody = ASTUtil
						.findTemplateBodyFromTemplateDef(in.templateDefNode);

				if (templateBody != null) {
					ReplaceEdit replace = inlineTemplate(in, templateBody,
							compositeChange, multiEdit, lastFile,
							updateTemplateDefinitionTextEditGroup);

					if (replace != null) {
						multiEdit.addChild(replace);
						updateReferenceTextEditGroups.add(new TextEditGroup(
								"update reference", replace));

						// keep a count of how many we've done so we know when
						// to delete
						this.inCount += 1;
						pm.worked(1);
					}
				}
				i++;
				if (i < this.inList.size()) {
					nextfile = SymbolHelper.getModuleFile(ASTUtil
							.getModuleSymbol(this.inList.get(i).ast));

					if (nextfile == null || !currfile.equals(nextfile)) {
						i--;
						break;
					}
					in = this.inList.get(i);
				}
			}

			textChange.setEdit(multiEdit);
			textChange.addTextEditGroup(updateTemplateDefinitionTextEditGroup);
			for (TextEditGroup group : updateReferenceTextEditGroups) {
				textChange.addTextEditGroup(group);
			}
			compositeChange.add(textChange);
		}
		pm.done();

		return compositeChange;
	}

	// ---------------------------------------------------------------------------

	/**
	 * @return the <code>Set</code> of files modified by the refactoring
	 */
	public Set<IFile> getModifiedFiles() {
		return modifiedFiles;
	}

	@Override
	public String getType() {
		return "Inline Param";
	}

	// ---------------------------------------------------------------------------

	private ReplaceEdit inlineTemplate(Inlining in, LocationAST templateBody,
			CompositeChange compositeChange, MultiTextEdit multiEdit,
			boolean lastFile,
			TextEditGroup updateTemplateDefinitionTextEditGroup)
			throws RefactoringException {
		TemplateSymbol typeSymb = (TemplateSymbol) in.symb;

		String declFilename = typeSymb.getSurroundingScope()
				.getAssociatedFilename();
		String referenceFilename = in.ast.getScope().getAssociatedFilename();
		if (!this.templateRewritten) {
			if (declFilename.equals(referenceFilename)) {
				rewriteParameterizedTemplateBody(multiEdit, in, templateBody,
						updateTemplateDefinitionTextEditGroup);
				removeTemplateFormalParams(multiEdit, in.templateDefNode,
						in.ast.getOffset(),
						updateTemplateDefinitionTextEditGroup);
			} else if (lastFile) {
				IResource declResource = ResourcesPlugin.getWorkspace()
						.getRoot().findMember(declFilename);
				IFile declFile = (IFile) declResource;
				TextFileChange declFileChange = new TextFileChange(
						"Template Fill in Params", declFile);
				declFileChange.setTextType("ttcn3");
				MultiTextEdit declRemovalEdit = new MultiTextEdit();
				TextEditGroup declRemovalTextEditGroup = new TextEditGroup(
						"update template definition");
				rewriteParameterizedTemplateBody(declRemovalEdit, in,
						templateBody, declRemovalTextEditGroup);
				if (!this.formalParamsRemoved) {
					removeTemplateFormalParams(declRemovalEdit,
							in.templateDefNode, in.ast.getOffset(),
							declRemovalTextEditGroup);
				}
				declFileChange.setEdit(declRemovalEdit);
				declFileChange.addTextEditGroup(declRemovalTextEditGroup);
				compositeChange.add(declFileChange);
			}
		}

		// if (!this.formalParamsRemoved) {
		// if (declFilename.equals(referenceFilename)) {
		// removeTemplateFormalParams(multiEdit, in.templateDefNode,
		// in.ast.getOffset());
		// } else {
		// IResource declResource =
		// ResourcesPlugin.getWorkspace().getRoot().findMember(declFilename);
		// IFile declFile = (IFile) declResource;
		// TextFileChange declFileChange = new TextFileChange("Declaration
		// Removal", declFile);
		// declFileChange.setTextType("ttcn3");
		// MultiTextEdit declRemovalEdit = new MultiTextEdit();
		// removeTemplateFormalParams(declRemovalEdit, in.templateDefNode,
		// in.ast.getOffset());
		// declFileChange.setEdit(declRemovalEdit);
		// compositeChange.add(declFileChange);
		// }
		// }

		String inlinedTemplate = ASTUtil.getIdentNode(in.templateDefNode)
				.getText();

		LocationAST offsetAST = null;

		if (ASTUtil.isFieldSpec(in.ast)) {
			int[] types = new int[] { TTCN3LexerTokenTypes.FunctionInstance,
					TTCN3LexerTokenTypes.TemplateBody };
			offsetAST = LocationAST.resolveParentsUntilType(in.ast, types);
		} else {
			int[] types = new int[] { TTCN3LexerTokenTypes.InLineTemplate,
					TTCN3LexerTokenTypes.ArrayElementSpec };
			offsetAST = LocationAST.resolveParentsUntilType(in.ast, types);
		}

		// don't want NPE here...
		if (offsetAST == null) {
			// TODO: there was a problem with refactoring so exit
			// accordingly
			// this null workaround results in "does not change any source
			// code" - should organise a FAIL message
			return null;
		}

		return new ReplaceEdit(offsetAST.getOffset(), offsetAST.getEndOffset()
				- offsetAST.getOffset(), inlinedTemplate);
	}

	// ---------------------------------------------------------------------------

	private void removeTemplateFormalParams(MultiTextEdit multiEdit,
			LocationAST templateDefNode, int astOffset,
			TextEditGroup updateTemplateDefinitionTextEditGroup) {
		// only delete params if the template is later in the file than the
		// current reference or if this is the last reference to the template
		if ((this.inList.size() - this.inCount) <= 1
				|| templateDefNode.getOffset() > astOffset || nextfile == null
				|| !currfile.equals(nextfile)) {
			ReplaceEdit edit;

			LocationAST baseTemplate = templateDefNode.getFirstChild();

			LocationAST ident = baseTemplate.getFirstChild().getNextSibling();

			edit = new ReplaceEdit(ident.getEndOffset(), baseTemplate
					.getEndOffset()
					- ident.getEndOffset(), "");
			multiEdit.addChild(edit);
			updateTemplateDefinitionTextEditGroup.addTextEdit(edit);

			this.formalParamsRemoved = true;
		}
	}

	// ---------------------------------------------------------------------------

	/**
	 * Replaces the fields of the template with the actual values previously
	 * used to reference it.
	 * 
	 * @param multiEdit
	 *            the <code>MultiTextEdit</code> to add this change to
	 * @param in
	 *            the <code>Inlining</code> object we are working on
	 * @param templateBody
	 *            the <code>LocationAST</code> object for the body of the
	 *            template
	 * @throws RefactoringException
	 */
	private void rewriteParameterizedTemplateBody(MultiTextEdit multiEdit,
			Inlining in, LocationAST templateBody,
			TextEditGroup updateTemplateDefinitionTextEditGroup)
			throws RefactoringException {
		int astOffset = in.ast.getOffset();

		if ((this.inList.size() - this.inCount) <= 1
				|| templateBody.getOffset() > astOffset || nextfile == null
				|| !currfile.equals(nextfile)) {
			List<LocationAST> parameterReferenceNodes = ASTUtil
					.getParameterReferenceNodes(templateBody);

			Map<String, LocationAST> parameterMap = buildParameterMap(
					in.actualParams, in.formalParams);

			TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
			formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
			formatterWalker
					.setFormatterParameters(new TTCN3FormatterParameters());

			for (LocationAST refNode : parameterReferenceNodes) {
				String parameterReferenceNodeText = refNode.getText();

				LocationAST targetNode = parameterMap
						.get(parameterReferenceNodeText);
				if (targetNode == null) {
					continue;
				}

				String replaceText = null;
				if (TTCN3LexerTokenTypes.TemplateInstance == targetNode
						.getType()) {
					try {
						replaceText = formatterWalker.pr_TemplateInstance(
								targetNode).toString();
					} catch (RecognitionException e) {
						throw new RefactoringException(e);
					}
				} else if (TTCN3LexerTokenTypes.IDENTIFIER == targetNode
						.getType()) {
					// it's an identifier
					replaceText = targetNode.getText();
				} else {
					throw new RefactoringException(
							"Unexpected target node type");
				}
				ReplaceEdit edit = new ReplaceEdit(refNode.getOffset(),
						(refNode.getEndOffset() - refNode.getOffset()),
						replaceText);
				multiEdit.addChild(edit);
				updateTemplateDefinitionTextEditGroup.addTextEdit(edit);
				// System.err.println("REPLACING " +refNode.getText()+" @
				// "+refNode.getOffset()+"-"+refNode.getEndOffset()+" with
				// "+TTCN3Analyzer.getDescendantText(targetNode));
			}
			this.templateRewritten = true;
		}
	}
}
