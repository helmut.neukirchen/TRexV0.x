package de.ugoe.cs.swe.trex.core.refactoring.core;


import org.eclipse.core.runtime.Assert;
import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

public class TTCN3RemoveAssignmentsToParametersRefactoring extends ProcessorBasedRefactoring {

	private TTCN3RemoveAssignmentsToParametersProcessor fProcessor;
	public TTCN3RemoveAssignmentsToParametersRefactoring(TTCN3RemoveAssignmentsToParametersProcessor processor) {
		super(processor);
		Assert.isNotNull(processor);
		fProcessor= processor;
	}
	public RefactoringProcessor getProcessor() {
		return fProcessor;
	}
}
