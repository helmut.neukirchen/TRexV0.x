
package de.ugoe.cs.swe.trex.core.refactoring.core;

import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

public class TTCN3MergeTemplateRefactoring extends ProcessorBasedRefactoring {

	RefactoringProcessor processor;
	private String renameText;
	private boolean preserveOriginals = false;

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringProcessor getProcessor() {
		return processor;
	}

	// ---------------------------------------------------------------------------
	public TTCN3MergeTemplateRefactoring(TTCN3MergeTemplateProcessor processor) {
		super(processor);
		this.processor = processor;
	}
	
	// ---------------------------------------------------------------------------

	public String getRenameText() {
		return renameText;
	}

	// ---------------------------------------------------------------------------

	public void setRenameText(String renameText) {
		this.renameText = renameText;
	}	
	
	// ---------------------------------------------------------------------------
	public boolean isPreserveOriginals() {
		return preserveOriginals;
	}

	// ---------------------------------------------------------------------------
	public void setPreserveOriginals(boolean preserveOriginals) {
		this.preserveOriginals = preserveOriginals;
	}

	// ---------------------------------------------------------------------------

}
