package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.Vector;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.TextEditGroup;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;


public class TTCN3AutoAllT3DocProcessor extends RefactoringProcessor {

	private IFile declarationFile = null;

	private LocationAST node;
	
	private int offset = 0;
	
	Symbol decSymbol = null;
	
	private Vector<LocationAST> nodes = new Vector<LocationAST>();
	
	private String spaceString = "";
	
	private String commentText;

	public TTCN3AutoAllT3DocProcessor(LocationAST node) {
		while(node.getParent() != null){
			node = node.getParent();
		}
		node = node.getFirstChild().getFirstChild().getNextSibling().getFirstChild().getFirstChild();

		while(node != null) {
			if(node.getType() == TTCN3LexerTokenTypes.ModuleDefinition){
				if(node.getFirstChild().getType() == TTCN3LexerTokenTypes.FunctionDef ||
						node.getFirstChild().getType() == TTCN3LexerTokenTypes.AltstepDef ||
						node.getFirstChild().getType() == TTCN3LexerTokenTypes.TestcaseDef
						){
					nodes.add(node.getFirstChild().getFirstChild().getFirstChild());
					if(decSymbol == null){
						decSymbol = getDeclarationSymbol(node.getFirstChild().getFirstChild().getFirstChild());
						String declarationFileName = decSymbol.getSurroundingScope()
						.getAssociatedFilename();
						declarationFile = getProjectFile(declarationFileName);
					}
				}
				
			}
			node = node.getNextSibling();
		}
	}

	// ---------------------------------------------------------------------------

	@Override
	public Object[] getElements() {
		return new String[0];
	}

	// ---------------------------------------------------------------------------

	@Override
	public String getIdentifier() {
		return "T3Doc Processor Identifier";
	}

	// ---------------------------------------------------------------------------

	@Override
	public String getProcessorName() {
		return "T3Doc Processor";
	}

	// ---------------------------------------------------------------------------

	@Override
	public boolean isApplicable() {
		return (true);
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm)
			throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		return status;
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkFinalConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		return status;
	}

	// ---------------------------------------------------------------------------

	private void createChanges(CompositeChange composite, IProgressMonitor pm) {
		
		pm.beginTask("Auto T3Doc Refactoring", nodes.size());

			TTCN3Analyzer analyzer = TTCN3AnalyzerFlyweightFactory.getInstance().getTTCN3Analyzer(declarationFile);
			MultiTextEdit multiEdit = new MultiTextEdit();
			TextFileChange fileChange = new TextFileChange(
					declarationFile.getName(), declarationFile);
			fileChange.setEdit(multiEdit);
			fileChange.setTextType("ttcn3");
			composite.add(fileChange);
			String insertText;
			for(LocationAST currentAST : nodes){
				node = currentAST;
				offset = lastNewLine(analyzer.getCode(),node.getParent().getParent().getOffset());
				insertText = getCommentText();
				if((analyzer.getCode().charAt(offset-2) == '/' && analyzer.getCode().charAt(offset-3) == '*')
						|| (analyzer.getCode().charAt(offset-3) == '/' && analyzer.getCode().charAt(offset-4) == '*')){
					insertText = "";
				}
				
				InsertEdit insertEdit = new InsertEdit(offset, insertText);
				multiEdit.addChild(insertEdit.copy());
				spaceString = "";
			}
			TextEditGroup declarationEditGroup = new TextEditGroup(
					"T3Doc Comments", multiEdit);
			fileChange.addTextEditGroup(declarationEditGroup);
			
			pm.worked(1);

}

	
	
	
	// ---------------------------------------------------------------------------

	@Override
	public Change createChange(IProgressMonitor pm)
			throws OperationCanceledException {
		CompositeChange composite = new CompositeChange("T3Doc");

		createChanges(composite, pm);
		return composite;
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringParticipant[] loadParticipants(RefactoringStatus status,
			SharableParticipants sharedParticipants) {
		return new RefactoringParticipant[0];
	}
	////////////////////////////////////////////////////////////////////////////
	
	public String getCommentText() {
		String comment =  spaceString + "/*\n";
		comment = comment + spaceString + " * @desc\t\n";
		
		int paramCount = getParamCount();
		for(int u = 0; u < paramCount; u++){
			comment = comment + spaceString + " * @param\t" + getParameter(u+1) + "\n";
		}
		comment = comment + spaceString + " * @return\t" + getReturntype() + "\n";
		comment = comment + spaceString + " */\n";
		return comment;
	}
	
	private String getParameter(int number){
		LocationAST currentNode = node.getParent();
		int n = 1;
		while(true){
			currentNode = currentNode.getNextSibling();
			if(currentNode == null){
				return "inexistant";
			}
			if(currentNode.getType() >= TTCN3LexerTokenTypes.FormalPortPar
					&& currentNode.getType() <= TTCN3LexerTokenTypes.FormalValuePar){
				if(n == number){
					return getParameterDesc(currentNode);
				} else n++;
			}
		}
	}
	
	private String getParameterDesc(LocationAST p){

		String paramDesc = p.getFirstChild().getNextSibling().getFirstChild().getText();
		
		if(p.getType() == TTCN3LexerTokenTypes.FormalTemplatePar){
			
			//eme9: System.out.println(p.getFirstChild().getNextSibling().getNextSibling().getFirstChild().getText());
			paramDesc = p.getFirstChild().getNextSibling().getNextSibling().getFirstChild().getText();
			paramDesc = paramDesc + " template";	
			
			//System.out.println(p.toStringList());
			//eme9: The first child is the template node and it has no children
			//System.out.println(p.getFirstChild().getText());
			//eme9: That�s way we need to find it siblings and then their children, then we can navigate to the node
			//System.out.println("then "+ p.getFirstChild().getNextSibling().getFirstChild().getFirstChild().getFirstChild().getFirstChild().getText());
			
			paramDesc = paramDesc + " " + p.getFirstChild().getNextSibling().getFirstChild().getFirstChild().getFirstChild().getFirstChild().getText();

		}
		if(p.getType() == TTCN3LexerTokenTypes.FormalPortPar){
//			paramDesc = paramDesc + " template";
//			paramDesc = paramDesc + " " + p.getFirstChild().getFirstChild().getFirstChild().getFirstChild().getFirstChild().getText();
			paramDesc = "???";
		}
		if(p.getType() == TTCN3LexerTokenTypes.FormalTimerPar){
			paramDesc = paramDesc + " timer";
		}
		if(p.getType() == TTCN3LexerTokenTypes.FormalTypePar){
//			paramDesc = paramDesc + " template";
//			paramDesc = paramDesc + " " + p.getFirstChild().getFirstChild().getFirstChild().getFirstChild().getFirstChild().getText();
			paramDesc = "???";
		}
		if(p.getType() == TTCN3LexerTokenTypes.FormalValuePar){
			if(p.getFirstChild().getFirstChild().getFirstChild().getText() == "TypeReference"){
				
			paramDesc = paramDesc + " " + p.getFirstChild().getFirstChild().getFirstChild().getFirstChild().getFirstChild().getText();
			} else paramDesc = paramDesc + " " + p.getFirstChild().getFirstChild().getFirstChild().getText();
			
		}
		return paramDesc;
	}
	
	private int getParamCount(){
		LocationAST currentNode = node.getParent();
		int n = 0;
		while(true){
			currentNode = currentNode.getNextSibling();
			if(currentNode == null){
				return n;
			}
			if(currentNode.getType() >= TTCN3LexerTokenTypes.FormalPortPar
					&& currentNode.getType() <= TTCN3LexerTokenTypes.FormalValuePar){
				n++;
			}
		}
	}
	
	
	private String getReturntype(){
		LocationAST currentNode = node.getParent();
		while(true){
			currentNode = currentNode.getNextSibling();
			if(currentNode == null){
				return "-";
			}
			if(currentNode.getType() == TTCN3LexerTokenTypes.ReturnType){
				return currentNode.getFirstChild().getFirstChild().getFirstChild().getText();
			}
		}
	}

	private int lastNewLine(String code, int cOffset){ // NOPMD by oskar on 12.11.2014 00:06
		int currentOffset = cOffset;
		while(true){
			currentOffset--;
			if (currentOffset <= 0)
				return 0;
			if(code.charAt(currentOffset) == '\n' || code.charAt(currentOffset) == '\r'){
				return currentOffset + 1;
			}else spaceString = spaceString + code.charAt(currentOffset);
		}
	}
	
	public int getCommentlength(){
		return commentText.length();
	}
	
	private IFile getProjectFile(String filename) {
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		IProject project = root.getProject();

		IFile file = null;
		if (project == null) {
			IPath path = ws.getRoot().findMember(filename).getFullPath();
			file = ws.getRoot().getFile(path);
		} else {
			file = project.getFile(filename);
		}

		return file;
	}
	
	private Symbol getDeclarationSymbol(LocationAST identifierNode) {
		Scope scope = identifierNode.getScope();
		if (scope == null)
			return null;
		return scope.resolve(identifierNode);
	}
}
