package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import antlr.RecognitionException;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3FormatterTreeParser;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterParameters;

public final class TTCN3ExtractUtil {

	private TTCN3ExtractUtil () {
		// empty private constructor: this class contains only static methods!		
	}

	protected static IFile getProjectFile(String filename) {
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		IProject project = root.getProject();

		IFile file = null;
		if (project == null) {
			IPath path = ws.getRoot().findMember(filename).getFullPath();
			file = ws.getRoot().getFile(path);
		} else {
			file = project.getFile(filename);
		}

		return file;
	}
	
	protected static String addSpaceToEachNewLine(String space, String formatTemplateNode) {
		formatTemplateNode = formatTemplateNode.replaceAll("\r\n",
				"WINPLACEHOLDER");
		formatTemplateNode = formatTemplateNode.replaceAll("\r",
				"MACPLACEHOLDER");
		formatTemplateNode = formatTemplateNode.replaceAll("\n",
				"LINUXPLACEHOLDER");
		formatTemplateNode = formatTemplateNode.replaceAll("WINPLACEHOLDER",
				"\r\n" + space);
		formatTemplateNode = formatTemplateNode.replaceAll("MACPLACEHOLDER",
				"\r" + space);
		formatTemplateNode = formatTemplateNode.replaceAll("LINUXPLACEHOLDER",
				"\n" + space);
		return formatTemplateNode;
	}

	protected static String getSpaceBevorNode(IFile file, LocationAST node)
			throws CoreException {

		StringBuffer spaceBuffer = new StringBuffer();
		try {
			InputStream contents = file.getContents();
			BufferedReader stream = new BufferedReader(new InputStreamReader(
					contents));
			int offset = node.getOffset();

			char[] cbuf = new char[offset];
			stream.read(cbuf, 0, offset);

			int retLength = cbuf.length - 1;
			while (retLength > 0 && cbuf[retLength] != '\n'
					&& cbuf[retLength] != '\r') {
				spaceBuffer.insert(0, Character.toString(cbuf[retLength]));
				retLength--;
			}

		} catch (CoreException e) {
			throw e;
		} catch (IOException e) {
			throw new CoreException(
					new Status(IStatus.ERROR,
							"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK,
							null, e));
		}

		return spaceBuffer.toString();

	}
	
	protected static String getNewLineString(IFile file) throws CoreException {
		StringBuffer newLineBuffer = new StringBuffer();
		try {
			InputStream contents = file.getContents();
			BufferedReader stream = new BufferedReader(new InputStreamReader(
					contents));

			int tmp;
			do {
				tmp = stream.read();
			} while (tmp > 0 && tmp != '\n' && tmp != '\r');

			if (tmp < 0) {
				return null;
			}

			boolean readN = false;
			boolean readR = false;
			do {
				newLineBuffer.insert(0, Character.toString((char) tmp));
				tmp = stream.read();
			} while (tmp > 0 && ( (tmp == '\n' && !readN) || (tmp == '\r' && !readR)));

		} catch (CoreException e) {
			throw e;
		} catch (IOException e) {
			throw new CoreException(
					new Status(IStatus.ERROR,
							"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK,
							null, e));
		}

		return newLineBuffer.toString();
	}
	
	protected static String formatImportDefNode(LocationAST nNode) throws CoreException {
		TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
		formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
		formatterWalker.setFormatterParameters(new TTCN3FormatterParameters());
		String newTemplateDef = null;
		try {
			newTemplateDef = formatterWalker.pr_ImportDef(nNode).toString();
		} catch (RecognitionException e) {
			throw new CoreException(
					new Status(IStatus.ERROR,
							"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK,
							null, e));
		}
		return newTemplateDef;
	}
	
	protected static LocationAST getParentNode(LocationAST node, int parentNodeType) {
		LocationAST tmp = node;
		while (tmp.getParent() != null) {
			if (tmp.getType() == parentNodeType) {
				return tmp;
			}
			tmp = tmp.getParent();
		}
		return tmp;
	}
}
