package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;

import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceFinder;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;

public class TTCN3MoveModuleConstantToComponentProcessor extends
		RefactoringProcessor {

	// Result codes returned by findComponentName

	/**
	 * @author Christian Schaefer
	 * 
	 */
	public enum FindComponentNameResult {
		SUCCESS, NO_REFERENCES_ERROR, MULTIPLE_COMPONENTS_CONCERNED_ERROR, REFERENCES_OUTSIDE_COMPONENT_ERROR
	}

	private Symbol declarationSymbol;

	private LocationAST declarationNode;

	private String defString = "";

	private String code = "";

	private String componentName = "";

	private Map<String, List<ReferenceWithContext>> references;

	private IFile declarationFile = null;

	public TTCN3MoveModuleConstantToComponentProcessor(
			LocationAST identifierNode) {

		if (identifierNode == null)
			return;
		if (identifierNode.getType() != TTCN3LexerTokenTypes.IDENTIFIER)
			return;
		declarationSymbol = getDeclarationSymbol(identifierNode);
		if (declarationSymbol == null)
			return;
		declarationNode = declarationSymbol.getDeclarationNode();
		if (declarationNode == null)
			return;

		String declarationFileName = declarationSymbol.getSurroundingScope()
				.getAssociatedFilename();
		declarationFile = getProjectFile(declarationFileName);

		TTCN3Analyzer analyzer = TTCN3AnalyzerFlyweightFactory.getInstance()
				.getTTCN3Analyzer(declarationFile);
		code = analyzer.getCode();
		int endOffset = declarationNode.getParent().getParent().getParent()
				.getEndOffset();
		for (int i = declarationNode.getParent().getParent().getParent()
				.getOffset(); i < endOffset; i++) {
			defString = defString + code.charAt(i);
		}
		defString = defString + ";";

		ReferenceFinder referenceFinder = new ReferenceFinder();
		Scope scope = identifierNode.getScope();
		Symbol symb = scope.resolve(identifierNode);
		references = referenceFinder.findReferences(symb);
	}

	// ---------------------------------------------------------------------------

	private Symbol getDeclarationSymbol(LocationAST identifierNode) {
		Scope scope = identifierNode.getScope();
		if (scope == null)
			return null;
		return scope.resolve(identifierNode);
	}

	// ---------------------------------------------------------------------------

	@Override
	public Object[] getElements() {
		return new String[0];
	}

	// ---------------------------------------------------------------------------

	@Override
	public String getIdentifier() {
		return "MoveModuleConstantToComponent Processor Identifier";
	}

	// ---------------------------------------------------------------------------

	@Override
	public String getProcessorName() {
		return "MoveModuleConstantToComponent Processor";
	}

	// ---------------------------------------------------------------------------

	@Override
	public boolean isApplicable() {
		return (true);
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm)
			throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		if (declarationNode != null && declarationNode.getParent() != null
				&& declarationNode.getParent().getParent() != null
				&& declarationNode.getParent().getParent().getParent() != null) {
			if (declarationNode.getParent().getParent().getParent().getType() != TTCN3LexerTokenTypes.ConstDef) {
				status.addFatalError("Select a constant!");
				return status;
			}
			switch (findAffectedComponentName()) {
			case MULTIPLE_COMPONENTS_CONCERNED_ERROR:
				status
						.addFatalError("Constant is used in more than one component behaviour.");
				break;
			case NO_REFERENCES_ERROR:
				status
						.addFatalError("There are no component behaviours which use this constant.");
				break;
			case REFERENCES_OUTSIDE_COMPONENT_ERROR:
				status
						.addFatalError("Constant is used outside of a component context.");
				break;
			case SUCCESS:
				break;
			}
		} else
			status.addFatalError("Select a constant!");
		return status;
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkFinalConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		return status;
	}

	// ---------------------------------------------------------------------------

	private IFile getProjectFile(String filename) {
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		IProject project = root.getProject();

		IFile file = null;
		if (project == null) {
			IPath path = ws.getRoot().findMember(filename).getFullPath();
			file = ws.getRoot().getFile(path);
		} else {
			file = project.getFile(filename);
		}

		return file;
	}

	// ---------------------------------------------------------------------------

	private void createChanges(CompositeChange composite, IProgressMonitor pm) {

		pm.beginTask("MoveModuleConstantToComponent Refactoring", 2);

		String declarationFileName = declarationSymbol.getSurroundingScope()
				.getAssociatedFilename();

		IFile declarationFile = getProjectFile(declarationFileName);
		MultiTextEdit declarationMultiEdit = new MultiTextEdit();
		TextFileChange declarationFileChange = new TextFileChange(
				declarationFile.getName(), declarationFile);

		LocationAST component = findComponentNodeByName();

		if (component != null) {
			ReplaceEdit replaceEdit = new ReplaceEdit(getReplaceStart(),
					getReplaceRange(), "");
			InsertEdit insertEdit = new InsertEdit(getPlace(component),
					defString);
			declarationMultiEdit.addChild(replaceEdit);
			declarationMultiEdit.addChild(insertEdit);

		}
		declarationFileChange.setEdit(declarationMultiEdit);
		declarationFileChange.setTextType("ttcn3");
		composite.add(declarationFileChange);
		pm.worked(2);
	}

	/**
	 * Finds the name of the component which is used by all behaviours
	 * referencing the constant.
	 * 
	 * @return A result code of type FindComponentNameResult
	 */

	private FindComponentNameResult findAffectedComponentName() {
		String declarationFileName = declarationSymbol.getSurroundingScope()
				.getAssociatedFilename();
		List<ReferenceWithContext> refs = references.get(declarationFileName);

		if (refs == null) {
			return FindComponentNameResult.NO_REFERENCES_ERROR;
		}

		// Loop through each reference

		for (ReferenceWithContext ref : refs) {
			LocationAST refNode = ref.getReferenceNode();
			int behaviourType = 0;

			// Check for a valid behaviour first

			while (refNode.getParent() != null) {
				int tempType = refNode.getType();

				if (tempType == TTCN3LexerTokenTypes.TestcaseDef
						|| tempType == TTCN3LexerTokenTypes.AltstepDef
						|| tempType == TTCN3LexerTokenTypes.FunctionDef
						|| tempType == TTCN3LexerTokenTypes.ComponentDef) {
					behaviourType = tempType;
					break;
				}
				refNode = refNode.getParent();
			}

			// If no valid behaviour found, return with an error

			if (behaviourType == 0)
				return FindComponentNameResult.REFERENCES_OUTSIDE_COMPONENT_ERROR;

			refNode = refNode.getFirstChild();

			// Now, we need to find the RunsOnSpec node of the behaviour. In
			// case
			// we have a component, then we can skip this section

			if (behaviourType == TTCN3LexerTokenTypes.TestcaseDef) {
				while (refNode.getNextSibling() != null
						&& refNode.getType() != TTCN3LexerTokenTypes.ConfigSpec) {
					refNode = refNode.getNextSibling();
				}
				if (refNode != null)
					refNode = refNode.getFirstChild();
			} else if (behaviourType != TTCN3LexerTokenTypes.ComponentDef) {
				while (refNode.getNextSibling() != null
						&& refNode.getType() != TTCN3LexerTokenTypes.RunsOnSpec) {
					refNode = refNode.getNextSibling();
				}
			}

			LocationAST componentNameNode = null; // the node which holds the
													// name of the component

			if (behaviourType == TTCN3LexerTokenTypes.ComponentDef)
				componentNameNode = refNode.getFirstChild();
			else
				componentNameNode = refNode.getFirstChild().getFirstChild()
						.getFirstChild(); // RunsOnSpec
			// => ComponentType => Identifier => componentNameNode

			if (componentName.equals("")) {
				componentName = componentNameNode.getText();
			}

			// Check for multiple components affected; this is not supported
			// so set the corresponding error state and return

			if (!componentName.equals(componentNameNode.getText())) {
				return FindComponentNameResult.MULTIPLE_COMPONENTS_CONCERNED_ERROR;
			}
		}

		// If we have an empty string, then we have no
		// functions/altsteps/testcases or component
		// who reference our constant in question; so return the
		// NoReferencesError

		if (componentName.equals(""))
			return FindComponentNameResult.NO_REFERENCES_ERROR;
		else
			return FindComponentNameResult.SUCCESS;
	}

	// ---------------------------------------------------------------------------

	@Override
	public Change createChange(IProgressMonitor pm)
			throws OperationCanceledException {
		CompositeChange composite = new CompositeChange(
				"MoveModuleConstantToComponent");
		createChanges(composite, pm);
		return composite;
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringParticipant[] loadParticipants(RefactoringStatus status,
			SharableParticipants sharedParticipants) {
		return new RefactoringParticipant[0];
	}

	private int getPlace(LocationAST comp) {
		int currentOffset = comp.getParent().getParent().getParent()
				.getParent().getOffset();
		boolean foundNewLine = false;
		String spaceString = "";
		while (!foundNewLine) {
			currentOffset--;
			if (code.charAt(currentOffset) == '\n'
					|| code.charAt(currentOffset) == '\r') {
				foundNewLine = true;
			} else
				spaceString = spaceString + code.charAt(currentOffset);
		}
		defString = '\n' + spaceString + '\t' + defString;
		int offs = comp.getParent().getOffset();
		while (code.charAt(offs) != '{') {
			offs++;
		}
		return offs + 1;
	}

	private int getReplaceRange() {
		return (declarationNode.getParent().getParent().getParent().getParent()
				.getNextSibling().getEndOffset()
				- getReplaceStart() + isNewLine(declarationNode.getParent()
				.getParent().getParent().getParent().getNextSibling()
				.getEndOffset() + 1));
	}

	private int getReplaceStart() {
		int offs = declarationNode.getParent().getParent().getParent()
				.getOffset() - 1;
		while (code.charAt(offs) != '\n' && code.charAt(offs) != '\r') {
			if (code.charAt(offs) == ' ' || code.charAt(offs) == '\t') {
				offs--;
			} else
				return offs + 1;
		}
		return offs;
	}

	private int isNewLine(int offs) {
		if (code.charAt(offs) == '\r' && code.charAt(offs + 1) == '\n') {
			return 2;
		} else if (code.charAt(offs) == '\r' && code.charAt(offs + 1) != '\n') {
			return 1;
		} else if (code.charAt(offs) == '\n') {
			return 1;
		} else if (code.charAt(offs) == ' ' || code.charAt(offs) == '\t') {
			return (1 + isNewLine(offs + 1));
		} else
			return 0;
	}

	/**
	 * Finds the name of the component which is used by all behaviours and/or component
	 * referencing the constant.
	 * 
	 * @return The LocationAST of the corresponding component node
	 */

	private LocationAST findComponentNodeByName() {

		// Walk to top of hierarchy until we have the
		// TTCN3Module node

		LocationAST currentNode = declarationNode.getParent();
		while (currentNode.getParent() != null
				&& currentNode.getType() != TTCN3LexerTokenTypes.TTCN3Module) {
			currentNode = currentNode.getParent();
		}

		// Get first module definition node

		LocationAST moduleDefinitionNode = currentNode.getFirstChild()
				.getNextSibling().getFirstChild().getFirstChild();
		
		// Visit each ModuleDefinition node

		while (moduleDefinitionNode != null) {
			LocationAST typedefNode = moduleDefinitionNode.getFirstChild();
			
			// First, only select TypeDef nodes
			
			if (typedefNode != null
					&& typedefNode.getType() == TTCN3LexerTokenTypes.TypeDef) {
				LocationAST structuredTypedefNode = typedefNode.getFirstChild();
				
				// The next node must be a StructuredTypeDef node

				if (structuredTypedefNode != null
						&& structuredTypedefNode.getType() == TTCN3LexerTokenTypes.StructuredTypeDef) {
					LocationAST componentDefNode = structuredTypedefNode
							.getFirstChild();
					
					// If this is a ComponentDef node and the name of this component
					// matches the componentName returned by findComponentName,
					// then we have the desired node and return the corresponding
					// LocationAST

					if (componentDefNode != null
							&& componentDefNode.getType() == TTCN3LexerTokenTypes.ComponentDef) {
						LocationAST componentNameNode = componentDefNode
								.getFirstChild().getFirstChild();
						String foundName = componentNameNode.getText();

						if (foundName.equals(componentName)) {
							return componentNameNode;
						}
					}
				}
			}
			
			// Get pointer to next ModuleDefinition node

			moduleDefinitionNode = moduleDefinitionNode.getNextSibling();
		}

		return null;	// Nothing found; just return NULL
	}
}
