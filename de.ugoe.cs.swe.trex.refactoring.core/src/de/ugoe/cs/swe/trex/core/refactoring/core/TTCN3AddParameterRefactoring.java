package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.Vector;

import org.eclipse.core.runtime.Assert;
import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

public class TTCN3AddParameterRefactoring extends ProcessorBasedRefactoring {
	
	private int paramCount;
	private String[] paramName;
	private String[] paramType;
	private String[] paramRef;
	private TTCN3AddParameterProcessor fProcessor;
	public TTCN3AddParameterRefactoring(TTCN3AddParameterProcessor processor) {
		super(processor);
		Assert.isNotNull(processor);
		fProcessor= processor;
	}
	public RefactoringProcessor getProcessor() {
		return fProcessor;
	}
	
	public void setParamCount(int paramCount) {
		this.paramCount = paramCount;
		paramType = new String[paramCount];
		paramName = new String[paramCount];
		paramRef = new String[paramCount];
	}
	
	public void setParamType(String paramType, int number) {
		this.paramType[number] = paramType;
	}
	
	public void setParamName(String paramName, int number) {
		this.paramName[number] = paramName;
	}
	
	public void setParamRef(String paramRef, int number) {
		this.paramRef[number] = paramRef;
	}
	
	public int getParamCount() {
		return paramCount;
	}
	
	public String getParamText() {
		String paramText = "";
		for(int i=0; i<paramCount;i++){
			paramText = paramText + paramType[i] + " " + paramName[i];
			if(i < paramCount-1){
				paramText = paramText + ", ";
			}
		}
		return paramText;
	}
	
	public String getRefParamText() {
		String paramRefText = "";
		for(int i=0; i<paramCount;i++){
			paramRefText = paramRefText + paramRef[i];
			if(i < paramCount-1){
				paramRefText = paramRefText + ", ";
			}
		}
		return paramRefText;
	}
	
	public Vector<String> getPresentParamNames(){
		return fProcessor.getPresentParamNames();
	}
}
