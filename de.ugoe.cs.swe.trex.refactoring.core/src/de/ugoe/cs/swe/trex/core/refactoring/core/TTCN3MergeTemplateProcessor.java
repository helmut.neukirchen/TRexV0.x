package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jface.text.Region;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextChange;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEditGroup;

import antlr.RecognitionException;
import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceFinder;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.PairTemplateListAndFields;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3FormatterTreeParser;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.ModuleSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.SymbolTable;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.TypeSymbol;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterParameters;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterTools;
import de.ugoe.cs.swe.trex.core.refactoring.core.util.SymbolHelper;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;
import de.ugoe.cs.swe.trex.ui.preferences.adapters.TTCN3FormatterParametersAdapter;

/**
 * A <code>RefactoringProcessor</code> that will merge a template with those
 * that it is similar to and replace all references with parameterised
 * references to the new merged template.
 * 
 */
public class TTCN3MergeTemplateProcessor extends RefactoringProcessor {
	/**
	 * A small inner class to hold the information needed to perform a
	 * <code>TextEdit</code> replacement on the source file.
	 * 
	 */
	static class Replacement implements Comparable<Replacement> {
		private LocationAST templateNode;

		/** The <code>IFile</code> object in which this template resides */
		private IFile file;

		/**
		 * The name of the new parametrised template that this should now
		 * reference
		 */
		private String newRefName;

		/**
		 * The <code>LocationAST</code> object for the
		 * <code>TemplateRef</code>
		 */
		private LocationAST refNode;

		/**
		 * Default constructor. Initialises all variables.
		 * 
		 * @param ref
		 *            the <code>LocationAST</code> object for the referenced
		 *            <code>TemplateDef</code>
		 * @param map
		 *            a <code>Map</code> of fields to their values in the
		 *            template
		 * @param str
		 *            the name of the new parametrised template that this should
		 *            now reference
		 */
		public Replacement(LocationAST ref, LocationAST template, String str) {
			refNode = ref;
			templateNode = template;
			file = getFileForAST(ref);
			newRefName = str;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(Replacement other) {
			final IFile otherFile = other.getFile();
			if (file.equals(otherFile))
				// sort in reverse offset order (last first)
				return other.getRefNode().getOffset() - refNode.getOffset();
			// sort into groups by filename
			return file.getFullPath().toOSString().compareTo(
					otherFile.getFullPath().toOSString());
		}

		public LocationAST getTemplateNode() {
			return templateNode;
		}

		public IFile getFile() {
			return file;
		}

		/**
		 * Get the <code>IFile</code> that contains the given
		 * <code>LocationAST</code>
		 * 
		 * @param ast
		 *            the <code>LocationAST</code> to find the
		 *            <code>IFile</code> for
		 * @return the <code>IFile</code> object that contains the given
		 *         <code>LocationAST</code>
		 */
		private IFile getFileForAST(LocationAST ast) {
			ModuleSymbol moduleSymbol = ASTUtil.getModuleSymbol(ast);
			IFile moduleFile = SymbolHelper.getModuleFile(moduleSymbol);

			return moduleFile;
		}

		public String getNewRefName() {
			return newRefName;
		}

		public LocationAST getRefNode() {
			return refNode;
		}

	}

	/**
	 * The <code>AST</code> for the main <code>TemplateDef</code> we are
	 * operating on. i.e. the one that will be transformed into the new
	 * parametrised version.
	 */
	private LocationAST mainAST;

	private String referenceText;

	/**
	 * The <code>Set</code> of <code>AST</code> nodes for
	 * <code>TemplateDef</code> objects which can be merged with the main
	 * <code>TemplateDef</code>.
	 */
	private Set<LocationAST> similarSet;

	private List<String> differingFields;

	private Set<IFile> modifiedFiles = new HashSet<IFile>();

	/**
	 * Default constructor. Start the refactoring based on the node underneath
	 * the cursor in the editor window.
	 * 
	 * @param editor
	 *            the <code>TTCN3Editor</code> object that the refactoring was
	 *            called from
	 */
	public TTCN3MergeTemplateProcessor(TTCN3Editor editor) {
		final LocationAST editorAST = TTCN3Editor.getEditorAST(editor);
		initialize(editorAST);
	}

	public TTCN3MergeTemplateProcessor(LocationAST ast) {
		initialize(ast);
	}

	private void initialize(LocationAST ast) {
		mainAST = LocationAST.resolveParentsUntilType(ast,
				TTCN3ParserTokenTypes.TemplateDef);

		if (mainAST == null)
			return;

		PairTemplateListAndFields tlaf = TTCN3Analyzer.TEMPLATE_TO_SIMILAR
				.get(mainAST);
		similarSet = (tlaf == null) ? null : tlaf.getTemplates();
		differingFields = (tlaf == null) ? null : tlaf.getFields();
		referenceText = ASTUtil.getIdentNode(mainAST).getText() + "_merged";
	}

	@Override
	public RefactoringStatus checkFinalConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws CoreException,
			OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		TTCN3MergeTemplateRefactoring refactoring = (TTCN3MergeTemplateRefactoring) getRefactoring();

		if (refactoring.getRenameText().length() <= 0)
			status.addFatalError("Name may not be empty!");

		if (existsNameClashInScope()) {
			status
					.addFatalError("Name is in use by another definition, choose a different one!");
		}

		return status;
	}

	private boolean existsNameClashInScope() {
		final String renameText = ((TTCN3MergeTemplateRefactoring) getRefactoring())
				.getRenameText();
		final boolean preservesOriginals = ((TTCN3MergeTemplateRefactoring) getRefactoring())
				.isPreserveOriginals();
		final LocationAST mainASTIdent = ASTUtil.getIdentNode(mainAST);
		final SymbolTable st = mainASTIdent.getScope().getSymbolTable();
		final Symbol symbol = st.resolve(renameText);
		if (symbol == null) {
			// no name clash
			return false;
		}
		if (!preservesOriginals) {
			// ignore main template name
			if (renameText.equals(mainASTIdent.getText())) {
				return false;
			}
			// ignore similar (to be merged) template names
			for (LocationAST similarTemplate : similarSet) {
				LocationAST similarTemplateIdent = ASTUtil
						.getIdentNode(similarTemplate);
				if (renameText.equals(similarTemplateIdent.getText())) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm)
			throws CoreException, OperationCanceledException {
		// check the refactoring was called upon a TemplateDef and that a Set of
		// similar templates is known
		if (mainAST == null
				|| mainAST.getType() != TTCN3ParserTokenTypes.TemplateDef)
			return RefactoringStatus
					.createFatalErrorStatus("You _must_ select the identifier "
							+ "of a template declaration to perform the merge refactoring.");

		if (similarSet == null)
			return RefactoringStatus
					.createFatalErrorStatus("This template does not have a set of known similar templates.\n\n"
							+ "However, it may be in the set of similar templates for another template.\n"
							+ "Use the 'Problems' tab to guide you.");

		// if 0 differing fields then templates are duplicates so no need to
		// check types defined
		if (differingFields == null || differingFields.size() == 0)
			return new RefactoringStatus();

		List<AST> fieldNodes = ASTUtil.getFields(mainAST);

		// for each field, check that the type has been defined (we need this
		// information later)
		for (AST field : fieldNodes) {
			final LocationAST identNode = ASTUtil.getDescendantNode(field);
			final Scope scope = identNode.getScope();
			Symbol symb = scope.resolve(identNode);
			TypeSymbol typeSymb = (TypeSymbol) symb;
			if (typeSymb == null)
				return RefactoringStatus
						.createFatalErrorStatus("Unknown type '"
								+ identNode.getText()
								+ "' must be declared in order to perform merge refactoring!");
		}

		return new RefactoringStatus();
	}

	@Override
	public Change createChange(IProgressMonitor pm) throws CoreException,
			OperationCanceledException {
		List<Replacement> replacements = new ArrayList<Replacement>();
		String newRefName = ((TTCN3MergeTemplateRefactoring) getRefactoring())
				.getRenameText();

		// process main ast for references
		getReferencesForTemplate(mainAST, newRefName, replacements);

		// process all matching asts for references
		for (final LocationAST ast : similarSet) {
			getReferencesForTemplate(ast, newRefName, replacements);
		}

		// sort all Replacement objects based on file and offset
		Collections.sort(replacements);

		CompositeChange compositeChange = new CompositeChange("Merge Template");

		for (int i = 0; i < replacements.size(); i++) {
			Replacement replacement = replacements.get(i);
			IFile file, currfile;
			file = currfile = replacement.getFile();

			modifiedFiles.add(currfile);

			// changes in the same file can be done in the same 'TextChange'
			// object
			TextChange change = new TextFileChange(file.getFullPath()
					.toOSString(), file);
			((TextFileChange) change).setSaveMode(TextFileChange.FORCE_SAVE);
			change.setTextType("ttcn3");
			MultiTextEdit multiEdit = new MultiTextEdit();
			List<TextEditGroup> textEditGroups = new ArrayList<TextEditGroup>();

			while (true) {
				LocationAST ref = replacement.getRefNode();
				if (ref.getType() == TTCN3ParserTokenTypes.IDENTIFIER) {
					// if refnode in replacement object is an identifier we know
					// this is a reference to a template
					ReplaceEdit replace = modifyReference(replacement);
					if (replace != null) {
						multiEdit.addChild(replace);
						textEditGroups.add(new TextEditGroup(
								"update reference", replace));
					}
				} else if (ref.getType() == TTCN3ParserTokenTypes.TemplateDef) {
					// else refnode should be a template definition
					// is it the "main" template definition?
					if (ref.equals(mainAST)) {
						// lookup line indent
						String firstLineOfMainTemplate = change
								.getCurrentContent(new Region(ref.getOffset(),
										1), true, 0, pm);
						String indent = extractLeadingWhitespaces(firstLineOfMainTemplate);
						processMainTemplateDef(replacement, multiEdit, indent,
								textEditGroups);
					} else {
						processDependentTemplateDef(replacement, multiEdit,
								textEditGroups);
					}
				}
				i++;
				// if we've reached the end, break out of the while loop
				if (i >= replacements.size()) {
					break;
				}
				// else get the next replacement
				replacement = replacements.get(i);
				currfile = replacement.getFile();
				// if file has changed then break
				if (!currfile.equals(file)) {
					i--;
					break;
				}
			}
			// Only add change, if it is a non-empty edit
			if (multiEdit.hasChildren()) {
				change.setEdit(multiEdit);
				for (TextEditGroup group : textEditGroups) {
					change.addTextEditGroup(group);
				}
				compositeChange.add(change);
			}
		}

		return compositeChange;
	}

	@Override
	public Object[] getElements() {
		return new String[0];
	}

	@Override
	public String getIdentifier() {
		return "Merge Identifier";
	}

	/**
	 * @return the <code>Set</code> of files modified by the refactoring
	 */
	public Set<IFile> getModifiedFiles() {
		return modifiedFiles;
	}

	@Override
	public String getProcessorName() {
		return "Merge Processor";
	}

	private void getReferencesForTemplate(final LocationAST ast,
			String newRefName, List<Replacement> replacements) {
		LocationAST ident = ASTUtil.getIdentNode(ast);
		Symbol symb = ident.getScope().resolve(ident);
		// XXX: ideally would used cached references from TTCN3Analyzer to save
		// cpu time
		Map<String, List<ReferenceWithContext>> references = new ReferenceFinder()
				.findReferences(symb);

		// extract references from result
		// and cope with multiple references
		for (Map.Entry<String, List<ReferenceWithContext>> entry : references
				.entrySet()) {
			List<ReferenceWithContext> refs = entry.getValue();
			ListIterator<ReferenceWithContext> refsIt = refs.listIterator(refs
					.size());

			while (refsIt.hasPrevious()) {
				ReferenceWithContext prev = refsIt.previous();
				if (ident.equals(prev.getMatchNode())) {
					replacements.add(new Replacement(prev.getReferenceNode(),
							ast, newRefName));
				}
			}
		}

		replacements.add(new Replacement(ast, null, newRefName));
	}

	public String getReferenceText() {
		return referenceText;
	}

	@Override
	public boolean isApplicable() throws CoreException {
		// check base ast is a templatedef
		if (mainAST.getType() == TTCN3ParserTokenTypes.TemplateDef) {
			// check TTCN3Analyzer.templateToSimilar has entried for base ast
			PairTemplateListAndFields tlaf = TTCN3Analyzer.TEMPLATE_TO_SIMILAR
					.get(mainAST);
			similarSet = (tlaf == null) ? null : tlaf.getTemplates();
			if (similarSet != null && similarSet.size() > 0)
				return true;
		}
		return false;
	}

	@Override
	public RefactoringParticipant[] loadParticipants(RefactoringStatus status,
			SharableParticipants sharedParticipants) throws CoreException {
		return new RefactoringParticipant[0];
	}

	private ReplaceEdit modifyReference(Replacement replacement) {
		LocationAST ref = replacement.getRefNode();

		// if its not a reference (identifier) we do nothing
		if (ref.getType() != TTCN3ParserTokenTypes.IDENTIFIER) {
			return null;
		}
		// if "preserve original" option is selected, we still reference
		// the original template and hence do nothing
		if (((TTCN3MergeTemplateRefactoring) getRefactoring())
				.isPreserveOriginals()) {
			return null;
		}
		StringBuffer newRef = new StringBuffer(replacement.getNewRefName());

		// if we have old parameters or parameters to add then ...
		if (ASTUtil.isParameterizedTemplateReference(ref)
				|| differingFields.size() > 0) {
			newRef.append('(');

			LocationAST templateNode = replacement.getTemplateNode();

			if (ASTUtil.isParameterizedTemplate(mainAST)) {
				// first add any old parameters from the reference
				addOldRefParams(ref, newRef);

				// then add any new parameters that have been added to the
				// reference
				addNewRefParams(ref, newRef, templateNode);
			} else {
				// first add any new parameters that have been added to the
				// reference
				addNewRefParams(ref, newRef, templateNode);

				// then add any old parameters from the reference
				addOldRefParams(ref, newRef);
			}

			newRef.append(')');
		}

		// step up the tree to catch old parameters in replacement (via offsets)
		if (ref.getParent().getParent().getType() != TTCN3ParserTokenTypes.DerivedDef)
			ref = ref.getParent().getParent().getParent();

		return new ReplaceEdit(ref.getOffset(), ref.getEndOffset()
				- ref.getOffset(), newRef.toString());
	}

	private void addNewRefParams(LocationAST ref, StringBuffer newRef,
			AST templateNode) {
		final int num = differingFields.size();
		final Map<String, String> fieldToValue = ASTUtil
				.getFieldsAndValues(templateNode);
		final String comma = ", ";
		boolean second = ASTUtil.isParameterizedTemplate(mainAST);

		for (int i = 0; i < num; i++) {
			String field = differingFields.get(i);
			final String val = fieldToValue.get(field);

			if (val == null)
				continue;

			if (second && ASTUtil.isParameterizedTemplateReference(ref)) {
				newRef.append(comma);
			}

			newRef.append(val);

			if (!second
					&& (i < num - 1 || ASTUtil
							.isParameterizedTemplateReference(ref)))
				newRef.append(comma);
		}
	}

	private void addOldRefParams(LocationAST ref, StringBuffer newRef) {
		final String comma = ", ";
		TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
		formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
		formatterWalker.setFormatterParameters(new TTCN3FormatterParameters());

		if (ASTUtil.isParameterizedTemplateReference(ref)) {
			LocationAST parList = ref.getParent().getParent().getNextSibling();
			List<LocationAST> oldParams = ASTUtil.getParamsFromParList(parList,
					false);
			int num = oldParams.size();
			for (int i = 0; i < num; i++) {
				AST param = oldParams.get(i);
				String paramStr;
				try {
					paramStr = formatterWalker.pr_TemplateInstance(param)
							.toString();
					newRef.append(paramStr);
					// if last arg then skip comma
					if (i == num - 1)
						continue;
					newRef.append(comma);
				} catch (RecognitionException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// TODO: Use InsertEdit for merged template, so that the main
	// template can be handled in the same way as dependent templates.
	// Currently, the main template is modified to create the merged
	// version, and the original main template is re-added afterwards
	// (if this option is selected).
	private void processMainTemplateDef(Replacement replacement,
			MultiTextEdit multiEdit, String indent,
			List<TextEditGroup> textEditGroups) {
		TextEditGroup insertMergedTemplateEditGroup = new TextEditGroup(
				"insert merged template");
		LocationAST ref = replacement.getRefNode();
		List<AST> fieldNodes = ASTUtil.getFields(ref);
		int i = fieldNodes.size() - 1;

		ListIterator<String> fieldsIt = differingFields
				.listIterator(differingFields.size());
		List<String> paramsStr = ASTUtil.getFormalParametersStr(ref);
		StringBuffer params = new StringBuffer();
		while (fieldsIt.hasPrevious() && i >= 0) {
			String str = fieldsIt.previous();
			AST field = fieldNodes.get(i);
			while (field != null && i >= 0) {
				final LocationAST identNode = ASTUtil.getDescendantNode(field);
				String id = identNode.getText();
				final String fieldValStr = ASTUtil.getFieldValueStr(field);
				if (str.equals(id) && !paramsStr.contains(fieldValStr)) {
					// 1) look up type of field to add as parameter
					String type = "";
					Symbol symb = identNode.getScope().resolve(identNode);
					TypeSymbol typeSymb = (TypeSymbol) symb;
					// XXX: may not need all these cases
					if (typeSymb.getPredefinedType() == null) {
						if (typeSymb.getReferencedType() == null) {
							if (typeSymb.getStructuredType() != null) {
								type += typeSymb.getStructuredTypeAsString();
							}
						} else {
							if (typeSymb.getReferencedType().getModule() != null)
								type += typeSymb.getReferencedType()
										.getModule()
										+ ".";
							final Symbol refType = typeSymb.getReferencedType()
									.getIdentifier();
							// XXX: not sure when/if this is needed
							/*
							 * // prefix param with 'template' if it is not a //
							 * predefined type LocationAST decl =
							 * refType.getDeclarationNode(); TypeSymbol declSymb =
							 * (TypeSymbol)decl.getScope().resolve(decl); if
							 * ((declSymb).getPredefinedType() == null) //&&
							 * (declSymb).getStructuredType() == null) type +=
							 * "template ";
							 */
							type += refType.getName();
						}
					} else {
						type += typeSymb.getPredefinedTypeAsString();
					}

					// base param identifier on field type
					String newParam = id + "Param";

					int suffix = 2;
					Set<String> fields = ASTUtil.getFieldsAndValues(ref)
							.keySet();
					// check paramStr not already used as a param to the
					// template
					// check paramStr not already used as a field in the
					// template
					while (paramsStr.contains(newParam)
							|| fields.contains(newParam)) {
						if (paramsStr.contains(newParam + suffix)
								|| fields.contains(newParam + suffix)) {
							suffix++;
						} else {
							newParam = newParam + suffix;
						}
					}

					// 2) add to params list with type and param name
					// insert params at beginning of stringbuffer
					// (achieving reverse order)
					params.insert(0, type + " " + newParam);
					if (fieldsIt.hasPrevious()) {
						params.insert(0, ", ");
					}
					// 3) replace field declaration with param name
					final LocationAST fieldVal = (LocationAST) ASTUtil
							.getFieldValue(field);
					ReplaceEdit replace = new ReplaceEdit(fieldVal.getOffset(),
							fieldVal.getEndOffset() - fieldVal.getOffset(),
							newParam);
					multiEdit.addChild(replace);
					insertMergedTemplateEditGroup.addTextEdit(replace);
					break;
				}
				i--;
				if (i < 0) {
					break;
				}
				field = fieldNodes.get(i);
			}
		}

		LocationAST ident = ASTUtil.getIdentNode(ref);

		// if we have some params to add
		if (params.length() > 0) {
			ReplaceEdit replace;
			if (ASTUtil.isParameterizedTemplate(ref)) {
				// 4) template already parameterised so append to list
				LocationAST formalValuePar = ident.getParent().getNextSibling();
				params.insert(0, ", ");
				replace = new ReplaceEdit(formalValuePar.getEndOffset(), 0,
						params.toString());
			} else {
				// 4) non-paremeterised so add parameters
				params.insert(0, " (");
				params.append(')');
				replace = new ReplaceEdit(ident.getEndOffset(), 0, params
						.toString());
			}
			multiEdit.addChild(replace);
			insertMergedTemplateEditGroup.addTextEdit(replace);
		}
		// 5) rename template to newRefName
		ReplaceEdit replace = new ReplaceEdit(ident.getOffset(), ident
				.getEndOffset()
				- ident.getOffset(), replacement.getNewRefName());
		multiEdit.addChild(replace);
		insertMergedTemplateEditGroup.addTextEdit(replace);
		textEditGroups.add(insertMergedTemplateEditGroup);

		// if "preserve original" option is selected
		if (((TTCN3MergeTemplateRefactoring) getRefactoring())
				.isPreserveOriginals()) {
			// insert old template definition after merged template
			// and change template body to reference on merged template
			String result = createAssignDelegateToMainTemplateDef(
					replacement.newRefName, ref, indent);
			multiEdit.addChild(new InsertEdit(ref.getEndOffset(), result));
			textEditGroups
					.add(new TextEditGroup("delegate to merged template"));
		}
	}

	private String createAssignDelegateToMainTemplateDef(String newRefName,
			LocationAST ref, String indentToBeAdded) {
		TTCN3FormatterTools tools = new TTCN3FormatterTools(
				new TTCN3FormatterParametersAdapter());
		StringBuffer templateDefinition = new StringBuffer(tools.getNewlines(2));
		ref.getOffset();
		templateDefinition.append(indentToBeAdded);
		// TODO: replace with constant
		templateDefinition.append("template ");
		templateDefinition.append(extractBaseTemplate(ref));
		templateDefinition.append(" := ");
		templateDefinition.append(createDelegation(newRefName, ref));
		return templateDefinition.toString();
	}

	private String extractBaseTemplate(LocationAST templateDef) {
		try {
			TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
			formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
			formatterWalker
					.setFormatterParameters(new TTCN3FormatterParameters());
			return formatterWalker.pr_BaseTemplate(templateDef.getFirstChild())
					.toString();
		} catch (RecognitionException e) {
			// TODO: Proper exception handling!!!
			e.printStackTrace();
			return null;
		}
	}

	private void processDependentTemplateDef(Replacement replacement,
			MultiTextEdit multiEdit, List<TextEditGroup> textEditGroups) {
		LocationAST ref = replacement.getRefNode();
		ReplaceEdit replace;
		String textEditGroupName;
		// if "preserve original" option is selected
		if (((TTCN3MergeTemplateRefactoring) getRefactoring())
				.isPreserveOriginals()) {
			String newRef = createDelegation(replacement.getNewRefName(), ref);
			LocationAST templateBody = ASTUtil
					.findTemplateBodyFromTemplateDef(ref);
			replace = new ReplaceEdit(templateBody.getOffset(), templateBody
					.getEndOffset()
					- templateBody.getOffset() + checkForSemicolon(ref), newRef);
			textEditGroupName = "delegate to merged template";
		} else {
			// simply remove template definition
			replace = new ReplaceEdit(ref.getOffset(), ref.getEndOffset()
					- ref.getOffset() + checkForSemicolon(ref), "");
			textEditGroupName = "remove template definition";
		}
		multiEdit.addChild(replace);
		textEditGroups.add(new TextEditGroup(textEditGroupName, replace));
	}

	private String createDelegation(String name, LocationAST ref) {
		StringBuffer newRef = new StringBuffer(name);

		// if we have old parameters or parameters to add then ...
		if (ASTUtil.isParameterizedTemplate(ref) || differingFields.size() > 0) {
			newRef.append('(');

			if (ASTUtil.isParameterizedTemplate(mainAST)) {
				// first add any old parameters from the reference
				addOldParamsFromDef(ref, newRef);

				// then add any new parameters that have been added to the
				// reference
				addNewParamsFromDef(ref, newRef);
			} else {
				// first add any new parameters that have been added to the
				// reference
				addNewParamsFromDef(ref, newRef);

				// then add any old parameters from the reference
				addOldParamsFromDef(ref, newRef);
			}

			newRef.append(')');
		}
		return newRef.toString();
	}

	private void addNewParamsFromDef(LocationAST ref, StringBuffer newRef) {
		final int num = differingFields.size();
		final Map<String, String> fieldToValue = ASTUtil
				.getFieldsAndValues(ref);
		final String comma1 = ", ";
		boolean second = ASTUtil.isParameterizedTemplate(mainAST);

		for (int i = 0; i < num; i++) {
			String field = differingFields.get(i);
			final String val = fieldToValue.get(field);

			if (val == null)
				continue;

			if (second && ASTUtil.isParameterizedTemplate(ref)) {
				newRef.append(comma1);
			}

			newRef.append(val);

			if (!second
					&& (i < num - 1 || ASTUtil.isParameterizedTemplate(ref)))
				newRef.append(comma1);
		}
	}

	private void addOldParamsFromDef(LocationAST ref, StringBuffer newRef) {
		final String comma = ", ";
		TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
		formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
		formatterWalker.setFormatterParameters(new TTCN3FormatterParameters());

		if (ASTUtil.isParameterizedTemplate(ref)) {
			List<String> oldParamIdentifiers = ASTUtil
					.getFormalParametersStr(ref);
			for (int i = 0; i < oldParamIdentifiers.size(); i++) {
				String param = oldParamIdentifiers.get(i);
				newRef.append(param);
				// if last arg then skip comma
				if (i == oldParamIdentifiers.size() - 1)
					continue;
				newRef.append(comma);
			}
		}
	}

	/**
	 * Checks if the following sibling of a LocationAST is a semicolon.
	 * 
	 * @param ast
	 * @return 1 if ast is followed by a semicolon, 0 otherwise
	 */
	private int checkForSemicolon(LocationAST ast) {
		int endOffsetShift = 0;
		LocationAST sibling = ast.getParent().getNextSibling();
		// check for an optional semicolon and remove that as well if found
		if (sibling != null
				&& sibling.getType() == TTCN3LexerTokenTypes.SemiColon) {
			endOffsetShift += sibling.getEndOffset() - sibling.getOffset();
		}
		return endOffsetShift;
	}

	/**
	 * extracts all whitespace characers from the beginning of a line of text
	 * 
	 * @param line
	 * @return all leading whitespaces
	 */
	private String extractLeadingWhitespaces(String line) {
		String indent = "";
		for (int j = 0; j < line.length(); j++) {
			String aChar = line.substring(j, j + 1);
			if (aChar.matches("\\s")) {
				indent += aChar;
			} else {
				break;
			}
		}
		return indent;
	}
}
