package de.ugoe.cs.swe.trex.core.refactoring.core;

import org.eclipse.core.runtime.Assert;
import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

public class TTCN3AutoAllT3DocRefactoring extends ProcessorBasedRefactoring {
	
	private TTCN3AutoAllT3DocProcessor fProcessor;
	public TTCN3AutoAllT3DocRefactoring(TTCN3AutoAllT3DocProcessor processor) {
		super(processor);
		Assert.isNotNull(processor);
		fProcessor= processor;
	}
	public RefactoringProcessor getProcessor() {
		return fProcessor;
	}
	public int getCommentlength(){
		return fProcessor.getCommentlength();
	}
}
