package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RenameProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEditGroup;

import antlr.RecognitionException;
import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3FormatterTreeParser;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.TemplateSymbol;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterParameters;

public class TTCN3ReplaceMagicNumberProcessor extends RenameProcessor {

	private LocationAST currentSingleExpressionNode;

	private LocationAST currentSelectedNode = null;

	private String newLine;

	private IFile currentNodeFile;

	private Scope currentModuleScope;

	private LocationAST preDefValNode;

	private String newConstantNumberName;

	private Set<IFile> modifiedFiles = new HashSet<IFile>();

	private LocationAST allInNode;

	public TTCN3ReplaceMagicNumberProcessor(LocationAST newSelectedNode) {
		if (newSelectedNode == null) {
			return;
		}

		currentSelectedNode = newSelectedNode;

		currentModuleScope = null;

		preDefValNode = LocationAST.resolveParentsUntilType(newSelectedNode,
				new int[] { TTCN3LexerTokenTypes.SingleExpression,
						TTCN3LexerTokenTypes.SingleValueOrAttrib });

		if (preDefValNode == null) {
			return;
		}

		if (preDefValNode.getType() == TTCN3LexerTokenTypes.SingleExpression
				|| preDefValNode.getType() == TTCN3LexerTokenTypes.SingleValueOrAttrib) {
			LocationAST tmp = TTCN3ExtractUtil.getParentNode(preDefValNode,
					TTCN3LexerTokenTypes.ModuleDefinitionsPart);
			currentModuleScope = tmp.getScope().getModuleScope();

		} else {
			return;
		}

	}

	@Override
	public RefactoringStatus checkFinalConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();

		return status;
	}

	@Override
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm)
			throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		if (currentSelectedNode == null) {
			status.addFatalError("Please select a Node!");
		}

		if (currentSelectedNode.getType() != TTCN3LexerTokenTypes.IntegerValue
				&& currentSelectedNode.getParent() != null
				&& currentSelectedNode.getParent().getType() != TTCN3LexerTokenTypes.CharStringValue
				&& currentSelectedNode.getParent().getType() != TTCN3LexerTokenTypes.BooleanValue
				&& currentSelectedNode.getParent().getType() != TTCN3LexerTokenTypes.FloatValue
				&& currentSelectedNode.getType() != TTCN3LexerTokenTypes.HexStringMatch
				&& currentSelectedNode.getType() != TTCN3LexerTokenTypes.BitStringMatch
				&& currentSelectedNode.getType() != TTCN3LexerTokenTypes.OctetStringMatch) {
			status.addFatalError("Please select a Node!");
		}

		if (currentModuleScope == null) {
			status.addFatalError("Please select a valid identifier!");
		}

		if (preDefValNode != null
				&& preDefValNode.getType() != TTCN3LexerTokenTypes.SingleExpression
				&& preDefValNode.getType() != TTCN3LexerTokenTypes.SingleValueOrAttrib) {
			status.addFatalError("Please select an Magic Number identifier!");
		}

		return status;
	}

	@Override
	public Change createChange(IProgressMonitor pm) throws CoreException,
			OperationCanceledException {
		CompositeChange composite = new CompositeChange("Replace Magic Number");

		createChanges(composite, pm);

		return composite;
	}

	private void createChanges(CompositeChange composite, IProgressMonitor pm)
			throws CoreException {

		// CIN := current selected identifier node
		// getting the new values from the refactoring instance
		TTCN3ReplaceMagicNumberRefactoring refactoring = (TTCN3ReplaceMagicNumberRefactoring) getRefactoring();
		boolean proccessEachOccurrence = refactoring.isProcessEachOccurrence();
		newConstantNumberName = refactoring.getNewConstantNumberName();

		// filename of the CIN
		String currentNodeFilename;
		if (currentModuleScope != null) {
			currentNodeFilename = currentModuleScope.getAssociatedFilename();
		} else {
			throw new CoreException(new Status(IStatus.ERROR,
					"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK, null,
					null));
		}

		// iFILE of the CIN
		currentNodeFile = TTCN3ExtractUtil.getProjectFile(currentNodeFilename);
		modifiedFiles.add(currentNodeFile);

		// MultiTextEdit für CIN
		MultiTextEdit currentNodeMultiEdit = new MultiTextEdit();

		// TextFileChange für CIN
		TextFileChange currentNodeFileChange = new TextFileChange(
				currentNodeFile.getName(), currentNodeFile);
		currentNodeFileChange.setEdit(currentNodeMultiEdit);
		currentNodeFileChange.setTextType("ttcn3");

		// adding the TextFileChange to the composite change
		composite.add(currentNodeFileChange);

		newLine = TTCN3ExtractUtil.getNewLineString(currentNodeFileChange
				.getFile());

		// creates new Node
		LocationAST nNode = null;
		if (preDefValNode.getType() == TTCN3LexerTokenTypes.SingleExpression) {
			currentSingleExpressionNode = preDefValNode;

		} else if (preDefValNode.getType() == TTCN3LexerTokenTypes.SingleValueOrAttrib) {
			currentSingleExpressionNode = buildStringTree(currentSelectedNode
					.getType(), getLastChild(preDefValNode).getText());
		}

		nNode = createNewConstantDef();

		if (nNode == null) {
			throw new CoreException(new Status(IStatus.ERROR,
					"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK, null,
					null));
		}

		// manually insert a new (temporary and incomplete) template definition
		// into symbol table.
		// the entry will be completed on re-analysis after finishing the
		// refactoring.
		LocationAST behaviorDefinition = LocationAST.resolveParentsUntilType(
				preDefValNode, new int[] { TTCN3ParserTokenTypes.FunctionDef,
						TTCN3ParserTokenTypes.TestcaseDef,
						TTCN3ParserTokenTypes.AltstepDef,
						TTCN3ParserTokenTypes.ModuleDefinitionsPart });
		Scope moduleDefinitionScope = behaviorDefinition.getScope()
				.getModuleScope();
		TemplateSymbol replaceMagicNumberDefSymbol = new TemplateSymbol();
		replaceMagicNumberDefSymbol.setName(newConstantNumberName);
		replaceMagicNumberDefSymbol.setSurroundingScope(moduleDefinitionScope);
		replaceMagicNumberDefSymbol.setScope(new Scope());
		moduleDefinitionScope.getSymbolTable().put(newConstantNumberName,
				replaceMagicNumberDefSymbol);

		// allInNode is the node where every change occures in.
		// in this case behaviourDefinition is the selected node, which is to
		// change at the end
		allInNode = behaviorDefinition;

		int countNonDeclarationFileChanges = 0;
		
		// process all Reference Nodes		
		if (proccessEachOccurrence) {

			// get all analyzer and process each of them
			Set<String> keys = TTCN3AnalyzerFlyweightFactory.getInstance()
					.getModuleAnalyzerMap().keySet();

		
			HashMap<TTCN3Analyzer, ArrayList<LocationAST>> analyzerWithSameSingleExpressionNodes = null;
			analyzerWithSameSingleExpressionNodes = new HashMap<TTCN3Analyzer, ArrayList<LocationAST>>();
			for (String key : keys) {
				TTCN3Analyzer analyzer = TTCN3AnalyzerFlyweightFactory
						.getInstance().getModuleAnalyzerMap().get(key);
				LocationAST rootNode = analyzer.getRootNode();
				ArrayList<LocationAST> sameSingleExpressionNodes = getSameSingleExpressionNodes(rootNode);
				if (sameSingleExpressionNodes.size() > 0) {
					analyzerWithSameSingleExpressionNodes.put(analyzer,
							sameSingleExpressionNodes);
				}

			}

			for (TTCN3Analyzer analyzer : analyzerWithSameSingleExpressionNodes
					.keySet()) {
				IFile file = analyzer.getFile();

				ArrayList<LocationAST> sameSingleExpressionNodes;
				sameSingleExpressionNodes = analyzerWithSameSingleExpressionNodes
						.get(analyzer);
				
				if (file.getFullPath().equals(currentNodeFile.getFullPath())) {
					// processes the File of the Declaration Node
					processFile(currentNodeMultiEdit, currentNodeFileChange,
							sameSingleExpressionNodes);
				} else {
					// processes none Declaration Node files
					// MultiTextEdit für CIN
					MultiTextEdit thisMultiEdit = new MultiTextEdit();

					// TextFileChange für CIN
					TextFileChange thisFileChange = new TextFileChange(file
							.getName(), file);
					thisFileChange.setEdit(thisMultiEdit);
					thisFileChange.setTextType("ttcn3");
					
					// adding the TextFileChange to the composite change
					composite.add(thisFileChange);

					processFile(thisMultiEdit, thisFileChange,
							sameSingleExpressionNodes);
					
					countNonDeclarationFileChanges += sameSingleExpressionNodes.size();

				}

			}
			
			// getting the higher level for the constant def. 
			// if there are more then one change and there is a node 
			// outside of the changed occurence before 
			if (countNonDeclarationFileChanges > 0 
					|| !(behaviorDefinition.getOffset() >= allInNode.getOffset() && behaviorDefinition
							.getEndOffset() <= allInNode.getEndOffset())) {
				allInNode = LocationAST.resolveParentsUntilType(allInNode,
						new int[] { TTCN3ParserTokenTypes.ModuleDefinitionsPart });
			}
			
		}

		// getting the parent node, where the constant defintion should be
		// putted
		LocationAST parentNode = allInNode;

		if (allInNode.getType() == TTCN3ParserTokenTypes.FunctionDef
				|| allInNode.getType() == TTCN3ParserTokenTypes.TestcaseDef
				|| allInNode.getType() == TTCN3ParserTokenTypes.AltstepDef) {
			// getting the beginning of the statementBlock, 
			// the place where to put the constant definition 
			parentNode = allInNode.getFirstChild();
			while (parentNode != null
					&& parentNode.getType() != TTCN3ParserTokenTypes.StatementBlock) {
				parentNode = parentNode.getNextSibling();
			}
			if (parentNode == null) {
				throw new CoreException(new Status(IStatus.ERROR,
						"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK,
						null, null));
			}
			parentNode = parentNode.getFirstChild();
		}

		// process Declaration Node File
		processDeclarationNode(currentNodeMultiEdit, currentNodeFileChange,
				parentNode, nNode);

		// remove temporary symbols from symbol table
		moduleDefinitionScope.getSymbolTable().remove(newConstantNumberName);
		pm.worked(1);

	}

	private void processDeclarationNode(MultiTextEdit currentNodeMultiEdit,
			TextFileChange currentNodeFileChange, LocationAST parentNode,
			LocationAST nNode) throws CoreException {

		String space = TTCN3ExtractUtil.getSpaceBevorNode(currentNodeFileChange
				.getFile(), parentNode);

		// converts nNode to a String

		String formatTemplateNode = formatConstNode(nNode);
		formatTemplateNode = TTCN3ExtractUtil.addSpaceToEachNewLine(space,
				formatTemplateNode);

		StringBuffer newTemplateDef = new StringBuffer();
		newTemplateDef.append(formatTemplateNode);

		newTemplateDef.append(newLine + newLine + space);

		InsertEdit insertEdit = new InsertEdit(parentNode.getOffset(),
				newTemplateDef.toString());
		currentNodeMultiEdit.addChild(insertEdit);

		// description for the wizard
		TextEditGroup declarationEditGroup = new TextEditGroup(
				"add new Constant", insertEdit);
		currentNodeFileChange.addTextEditGroup(declarationEditGroup);

		ReplaceEdit replaceEdit = new ReplaceEdit(preDefValNode.getOffset(),
				preDefValNode.getEndOffset() - preDefValNode.getOffset(),
				newConstantNumberName);
		currentNodeMultiEdit.addChild(replaceEdit);
		TextEditGroup tEditGroupCIN = new TextEditGroup(
				"replace the selected Magic Number", replaceEdit);
		currentNodeFileChange.addTextEditGroup(tEditGroupCIN);

	}

	private void processFile(MultiTextEdit currentIdentifierNodeMultiEdit,
			TextFileChange currentIdentifierNodeFileChange,
			ArrayList<LocationAST> sameSingleExpressionNodes)
			throws CoreException {
		ReplaceEdit replaceEdit;

		// modificationsOnlyInCurrentFile;

		boolean insertedImport = false;
		for (LocationAST tmp : sameSingleExpressionNodes) {
			if (currentNodeFile.getFullPath().equals(
					currentIdentifierNodeFileChange.getFile().getFullPath())
					&& preDefValNode.getOffset() == tmp.getOffset()) {
				continue;
			}

			replaceEdit = new ReplaceEdit(tmp.getOffset(), tmp.getEndOffset()
					- tmp.getOffset(), newConstantNumberName);
			currentIdentifierNodeMultiEdit.addChild(replaceEdit);
			TextEditGroup declarationEditGroup2 = new TextEditGroup(
					"replace Magic Number", replaceEdit);
			currentIdentifierNodeFileChange
					.addTextEditGroup(declarationEditGroup2);

			boolean addImportStatement = isImportMissing(tmp);
			if (addImportStatement && !insertedImport) {
				insertedImport = true;
				handleImport(currentIdentifierNodeMultiEdit,
						currentIdentifierNodeFileChange, tmp);
			}
		}

	}

	private void handleImport(MultiTextEdit currentIdentifierNodeMultiEdit,
			TextFileChange currentIdentifierNodeFileChange, LocationAST tmp)
			throws CoreException {

		LocationAST moduleDefinitionListNode = TTCN3ExtractUtil.getParentNode(
				tmp, TTCN3LexerTokenTypes.ModuleDefinitionList);
		String space = TTCN3ExtractUtil.getSpaceBevorNode(
				currentIdentifierNodeFileChange.getFile(),
				moduleDefinitionListNode.getFirstChild());

		LocationAST newImportNode;
		newImportNode = createNewImportNode(currentModuleScope.getScopeSymbol()
				.getName());
		String formatedNewImportNode = TTCN3ExtractUtil
				.formatImportDefNode(newImportNode);
		formatedNewImportNode = TTCN3ExtractUtil.addSpaceToEachNewLine(space,
				formatedNewImportNode);
		newImportNode.getText();
		String importStatement = new String(formatedNewImportNode + newLine
				+ newLine + space);
		InsertEdit insertEdit = new InsertEdit(moduleDefinitionListNode
				.getOffset(), importStatement);
		currentIdentifierNodeMultiEdit.addChild(insertEdit);
		TextEditGroup declarationEditGroup3 = new TextEditGroup(
				"create new import statement", insertEdit);
		currentIdentifierNodeFileChange.addTextEditGroup(declarationEditGroup3);

	}

	private ArrayList<LocationAST> getSameSingleExpressionNodes(
			LocationAST fileRootNode) {
		// search all Magic Numbers
		ArrayList<LocationAST> singleExpressionNodeList = new ArrayList<LocationAST>();
		getAllSingleExpressionNodes(fileRootNode, singleExpressionNodeList);
		LocationAST[] singleExpressionNodeArray = singleExpressionNodeList
				.toArray(new LocationAST[singleExpressionNodeList.size()]);

		// compare Magic Numbers with the current selected Magic Number
		// after this sameMagicNumber holds the Structure of all similar
		// Magic Numbers
		ArrayList<LocationAST> sameSingleExpressionNodes = new ArrayList<LocationAST>();
		for (LocationAST tmp : singleExpressionNodeArray) {
			if (singleExpressionEqual(tmp, preDefValNode)) {
				sameSingleExpressionNodes.add(tmp);
			}
		}
		return sameSingleExpressionNodes;
	}

	private LocationAST buildStringTree(int stringType, String text) {

		LocationAST endNode = null;
		if (stringType == TTCN3LexerTokenTypes.OctetStringMatch) {
			LocationAST nodeOCTETSTRING = createLocationAST(
					TTCN3LexerTokenTypes.OSTRING, text, null);
			LocationAST nodeOctetStringValue = createLocationAST(
					TTCN3LexerTokenTypes.OctetStringValue, null,
					nodeOCTETSTRING);
			endNode = nodeOctetStringValue;
		} else if (stringType == TTCN3LexerTokenTypes.HexStringMatch) {
			LocationAST nodeHEXSTRING = createLocationAST(
					TTCN3LexerTokenTypes.HSTRING, text, null);
			LocationAST nodeHexStringValue = createLocationAST(
					TTCN3LexerTokenTypes.HexStringValue, null, nodeHEXSTRING);
			endNode = nodeHexStringValue;
		} else if (stringType == TTCN3LexerTokenTypes.BitStringMatch) {
			LocationAST nodeBITSTRING = createLocationAST(
					TTCN3LexerTokenTypes.BSTRING, text, null);
			LocationAST nodeBitStringValue = createLocationAST(
					TTCN3LexerTokenTypes.BitStringValue, null, nodeBITSTRING);
			endNode = nodeBitStringValue;
		}

		int[] typeArray = new int[] { TTCN3LexerTokenTypes.SingleExpression,
				TTCN3LexerTokenTypes.XorExpression,
				TTCN3LexerTokenTypes.AndExpression,
				TTCN3LexerTokenTypes.NotExpression,
				TTCN3LexerTokenTypes.EqualExpression,
				TTCN3LexerTokenTypes.RelExpression,
				TTCN3LexerTokenTypes.ShiftExpression,
				TTCN3LexerTokenTypes.BitOrExpression,
				TTCN3LexerTokenTypes.BitXorExpression,
				TTCN3LexerTokenTypes.BitAndExpression,
				TTCN3LexerTokenTypes.BitNotExpression,
				TTCN3LexerTokenTypes.AddExpression,
				TTCN3LexerTokenTypes.MulExpression,
				TTCN3LexerTokenTypes.UnaryExpression,
				TTCN3LexerTokenTypes.Primary, TTCN3LexerTokenTypes.Value,
				TTCN3LexerTokenTypes.PredefinedValue };
		return buildTree(typeArray, endNode);
	}

	private LocationAST buildTree(int[] typeArray, LocationAST endNode) {

		LocationAST returnAST = createLocationAST(
				typeArray[typeArray.length - 1], null, endNode);

		for (int i = typeArray.length - 2; i >= 0; i--) {
			LocationAST tmp = createLocationAST(typeArray[i], null, returnAST);
			returnAST = tmp;
		}
		return returnAST;
	}

	private LocationAST createLocationAST(int type, String name,
			LocationAST child) {
		LocationAST returnAST = new LocationAST();
		returnAST.setType(type);
		returnAST.setText(name);
		returnAST.setFirstChild(child);
		return returnAST;
	}

	private LocationAST createNewImportNode(String moduleName) {

		LocationAST IDENTIFIERNode;
		LocationAST identifierNode;
		LocationAST globalModuleIdNode;
		LocationAST moduleIdNode;
		LocationAST importFromSpecNode;
		LocationAST isIDENTIFIERNode;
		LocationAST isIdentifierNode;
		LocationAST isConstRefListNode;
		LocationAST isImportConstSpecNode;
		LocationAST isImportElementNode;
		LocationAST isImportSpecNode;
		LocationAST newImportDefNode;

		IDENTIFIERNode = createLocationAST(TTCN3LexerTokenTypes.IDENTIFIER,
				moduleName, null);

		identifierNode = createLocationAST(TTCN3LexerTokenTypes.Identifier,
				null, IDENTIFIERNode);

		globalModuleIdNode = createLocationAST(
				TTCN3LexerTokenTypes.GlobalModuleId, null, identifierNode);

		moduleIdNode = createLocationAST(TTCN3LexerTokenTypes.ModuleId, null,
				globalModuleIdNode);

		importFromSpecNode = createLocationAST(
				TTCN3LexerTokenTypes.ImportFromSpec, null, moduleIdNode);

		// importSpec

		isIDENTIFIERNode = createLocationAST(TTCN3LexerTokenTypes.IDENTIFIER,
				newConstantNumberName, null);

		isIdentifierNode = createLocationAST(TTCN3LexerTokenTypes.Identifier,
				null, isIDENTIFIERNode);

		isConstRefListNode = createLocationAST(
				TTCN3LexerTokenTypes.ConstRefList, null, isIdentifierNode);

		isImportConstSpecNode = createLocationAST(
				TTCN3LexerTokenTypes.ImportConstSpec, null, isConstRefListNode);

		isImportElementNode = createLocationAST(
				TTCN3LexerTokenTypes.ImportElement, null, isImportConstSpecNode);

		isImportSpecNode = createLocationAST(TTCN3LexerTokenTypes.ImportSpec,
				null, isImportElementNode);

		// combine both trees to ImportDef

		importFromSpecNode.setNextSibling(isImportSpecNode);

		newImportDefNode = createLocationAST(TTCN3LexerTokenTypes.ImportDef,
				null, importFromSpecNode);

		return newImportDefNode;
	}

	private boolean isImportMissing(LocationAST inlineNode) {

		LocationAST behaviorDefinition = LocationAST.resolveParentsUntilType(
				inlineNode, new int[] { TTCN3ParserTokenTypes.FunctionDef,
						TTCN3ParserTokenTypes.TestcaseDef,
						TTCN3ParserTokenTypes.AltstepDef,
						TTCN3ParserTokenTypes.ModuleDefinitionsPart });

		// resolving by string is sufficient in this case!
		Symbol resolvedTemplateSymbol = behaviorDefinition.getScope()
				.getModuleScope().resolve(newConstantNumberName);
		if (resolvedTemplateSymbol == null)
			return true;

		return false;
	}

	private String formatConstNode(LocationAST nNode) throws CoreException {
		TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
		formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
		formatterWalker.setFormatterParameters(new TTCN3FormatterParameters());
		String newConstDef = null;
		try {
			newConstDef = formatterWalker.pr_ConstDef(nNode).toString();
		} catch (RecognitionException e) {
			throw new CoreException(
					new Status(IStatus.ERROR,
							"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK,
							null, e));
		}
		return newConstDef;
	}

	private boolean singleExpressionEqual(LocationAST inlineTemplate1,
			LocationAST inlineTemplate2) {

		if ((inlineTemplate1.getType() != TTCN3LexerTokenTypes.SingleExpression || inlineTemplate2
				.getType() != TTCN3LexerTokenTypes.SingleExpression)
				&& (inlineTemplate1.getType() != TTCN3LexerTokenTypes.SingleValueOrAttrib || inlineTemplate2
						.getType() != TTCN3LexerTokenTypes.SingleValueOrAttrib)) {
			return false;
		}

		return rekursivCompareNodes(inlineTemplate1, inlineTemplate2);
	}

	private boolean rekursivCompareNodes(LocationAST node1, LocationAST node2) {
		if (node1 == null && node2 == null) {
			return true;
		}
		boolean toReturn = false;
		if (node1.getType() == node2.getType()) {
			if (node1.getType() == TTCN3LexerTokenTypes.CSTRING
					&& node1.getText().compareTo(node2.getText()) != 0 ) {
				return false;
			} else if (node1.getType() == TTCN3LexerTokenTypes.NUMBER
					&& Integer.parseInt(node1.getText()) != Integer
					.parseInt(node2.getText())) {
		return false;
	}
			
			
			
			toReturn = rekursivCompareNodes(node1.getFirstChild(), node2
					.getFirstChild());
			if (toReturn) {
				toReturn = rekursivCompareNodes(node1.getNextSibling(), node2
						.getNextSibling());
			}

		}
		return toReturn;
	}

	private LocationAST createNewConstantDef() throws CoreException {

		LocationAST constantDefNode = null;

		LocationAST typeNode = null;
		LocationAST predefinedTypeNode = null;
		LocationAST integerNode = null;

		LocationAST singleConstantDefintionNode = null;

		LocationAST identifierNode = null;
		LocationAST IDENTIFIERNode = null;

		LocationAST constantExpressionNode = null;
		LocationAST singleExpressionNode = null;

		// getting single expression node
		try {
			singleExpressionNode = ASTUtil
					.deepCloneAST(currentSingleExpressionNode);
		} catch (Exception e) {
			throw new CoreException(
					new Status(IStatus.ERROR,
							"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK,
							null, e));
		}

		// create and set constant expression node
		constantExpressionNode = createLocationAST(
				TTCN3LexerTokenTypes.ConstantExpression, null,
				singleExpressionNode);

		// create and set IDENTIFIER node
		IDENTIFIERNode = createLocationAST(TTCN3LexerTokenTypes.IDENTIFIER,
				newConstantNumberName, null);

		identifierNode = createLocationAST(TTCN3LexerTokenTypes.Identifier,
				null, IDENTIFIERNode);
		identifierNode.setNextSibling(constantExpressionNode);

		singleConstantDefintionNode = createLocationAST(
				TTCN3LexerTokenTypes.SingleConstDef, null, identifierNode);

		int typeKeyWord = 0;
		if (currentSelectedNode.getType() == TTCN3LexerTokenTypes.IntegerValue) {
			typeKeyWord = TTCN3LexerTokenTypes.IntegerKeyword;
		} else if (currentSelectedNode.getParent().getType() == TTCN3LexerTokenTypes.CharStringValue) {
			typeKeyWord = TTCN3LexerTokenTypes.CharStringKeyword;
		} else if (currentSelectedNode.getParent().getType() == TTCN3LexerTokenTypes.BooleanValue) {
			typeKeyWord = TTCN3LexerTokenTypes.BooleanKeyword;
		} else if (currentSelectedNode.getParent().getType() == TTCN3LexerTokenTypes.FloatValue) {
			typeKeyWord = TTCN3LexerTokenTypes.FloatKeyword;
		} else if (currentSelectedNode.getType() == TTCN3LexerTokenTypes.HexStringMatch) {
			typeKeyWord = TTCN3LexerTokenTypes.HexStringKeyword;
		} else if (currentSelectedNode.getType() == TTCN3LexerTokenTypes.BitStringMatch) {
			typeKeyWord = TTCN3LexerTokenTypes.BitStringKeyword;
		} else if (currentSelectedNode.getType() == TTCN3LexerTokenTypes.OctetStringMatch) {
			typeKeyWord = TTCN3LexerTokenTypes.OctetStringKeyword;
		}
		integerNode = createLocationAST(typeKeyWord, null, null);

		predefinedTypeNode = createLocationAST(
				TTCN3LexerTokenTypes.PredefinedType, null, integerNode);

		typeNode = createLocationAST(TTCN3LexerTokenTypes.Type, null,
				predefinedTypeNode);
		typeNode.setNextSibling(singleConstantDefintionNode);

		constantDefNode = createLocationAST(TTCN3LexerTokenTypes.ConstDef,
				null, typeNode);

		return constantDefNode;
	}

	public String getNewTemplateNodeName() {
		AST newConstNumber = null;

		if (preDefValNode != null) {
			newConstNumber = getLastChild(preDefValNode);
		}

		String newConstandName;

		if (newConstNumber != null) {
			newConstandName = "newConstant_"
					+ newConstNumber.getText().replaceAll("\\W", "_");
		} else {
			newConstandName = "newConstant";
		}

		return newConstandName;
	}

	private LocationAST getLastChild(LocationAST node) {
		if (node.getFirstChild() != null) {
			return getLastChild(node.getFirstChild());
		}
		return node;
	}

	private void getAllSingleExpressionNodes(LocationAST node,
			ArrayList<LocationAST> singleExpressionNodes) {
		// todo rename singelExpressionNode + singelvalueorattrib
		if (node != null) {
			if (node.getType() == TTCN3LexerTokenTypes.SingleExpression
					|| node.getType() == TTCN3LexerTokenTypes.SingleValueOrAttrib) {
				singleExpressionNodes.add(node);
			}
			getAllSingleExpressionNodes(node.getFirstChild(),
					singleExpressionNodes);
			getAllSingleExpressionNodes(node.getNextSibling(),
					singleExpressionNodes);
		}

	}

	@Override
	public Object[] getElements() {
		return new String[0];
	}

	@Override
	public String getIdentifier() {
		return "Extract Template Processor Identifier";
	}

	@Override
	public String getProcessorName() {
		return "Extract Template Processor";
	}

	@Override
	public boolean isApplicable() throws CoreException {
		return (currentModuleScope != null && currentSingleExpressionNode != null);
	}

	@Override
	public RefactoringParticipant[] loadParticipants(RefactoringStatus status,
			SharableParticipants sharedParticipants) throws CoreException {
		return new RefactoringParticipant[0];
	}

	public Set<IFile> getModifiedFiles() {
		return modifiedFiles;
	}

}