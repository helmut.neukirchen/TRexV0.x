package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.NullChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextChange;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEditGroup;
import antlr.RecognitionException;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceFinder;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3FormatterTreeParser;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.ModuleSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.TemplateSymbol;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterParameters;
import de.ugoe.cs.swe.trex.core.refactoring.core.util.SymbolHelper;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;

public class TTCN3InlineTemplateProcessor extends TTCN3Inline {

	IFile currfile = null;

	private boolean declarationDeleted = false;

	private int inCount = 0;

	IFile nextfile = null;

	Set<IFile> modifiedFiles = new HashSet<IFile>();

	// ---------------------------------------------------------------------------

	public TTCN3InlineTemplateProcessor(List<LocationAST> astList) {
		super(astList);
	}

	// ---------------------------------------------------------------------------

	/**
	 * Inline into a single <code>AST</code>. Used by Tests.
	 * 
	 * @param ast
	 *            a reference to the template to inline.
	 */
	public TTCN3InlineTemplateProcessor(LocationAST ast) {
		super(ast);
	}

	// ---------------------------------------------------------------------------

	public TTCN3InlineTemplateProcessor(TTCN3Editor editor) {
		super(editor);
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm)
			throws OperationCanceledException {
		pm.beginTask("Checking initial conditions", inList.size());

		for (Inlining in : inList) {

			if (in.symb == null)
				return RefactoringStatus
						.createFatalErrorStatus("Identifier undeclared!");

			if (!(in.symb instanceof TemplateSymbol))
				return RefactoringStatus
						.createFatalErrorStatus("Identifier not a template reference!");

			LocationAST templateIdentifierNode = in.symb.getDeclarationNode();

			LocationAST templateBodyNode = LocationAST.resolveParentsUntilType(
					in.ast, TTCN3LexerTokenTypes.TemplateBody);

			LocationAST functionRefNode = LocationAST.resolveParentsUntilType(
					in.ast, TTCN3LexerTokenTypes.FunctionRef);
			
			// The follwing block of code is very messy but it is currently the
			// most "elegant" way to check for a template which itself contains an
			// inlined template.

			if (functionRefNode != null) {
				LocationAST functionActualParList = functionRefNode
						.getNextSibling();

				if (functionActualParList != null) {
					LocationAST functionActualPar = functionActualParList
							.getFirstChild();
					while (functionActualPar != null) {
						LocationAST inlineTemplate = (LocationAST) ASTUtil
								.findChild(functionActualPar,
										TTCN3LexerTokenTypes.InLineTemplate);
						LocationAST thisTemplateBody = null;
						if (inlineTemplate != null) {
							LocationAST typeNode = (LocationAST) ASTUtil
									.findChild(inlineTemplate,
											TTCN3LexerTokenTypes.Type);
							if (typeNode == null) {
								thisTemplateBody = (LocationAST) ASTUtil
										.findChild(
												inlineTemplate,
												TTCN3LexerTokenTypes.TemplateBody);
							} else {
								thisTemplateBody = (LocationAST) ASTUtil
										.findSibling(
												typeNode,
												TTCN3LexerTokenTypes.TemplateBody);
							}
						}
						if (thisTemplateBody != null) {
							LocationAST valueType = thisTemplateBody
									.getFirstChild();
							if (valueType != null
									&& (valueType.getType() == TTCN3LexerTokenTypes.ArrayValueOrAttrib || valueType
											.getType() == TTCN3LexerTokenTypes.FieldSpecList)) {
								return RefactoringStatus
										.createFatalErrorStatus("Cannot inline a template with inlined template parameters!");
							}
						}
						functionActualPar = functionActualPar.getNextSibling();
					}
				}
			}

			// Test for a nested template and exit with error if one is found
			
			LocationAST templateDefNode = LocationAST.resolveParentsUntilType(
					in.ast, TTCN3LexerTokenTypes.TemplateDef);

			if (templateBodyNode != null) {
				LocationAST oneParentAbove = templateBodyNode.getParent();
				LocationAST twoParentsAbove = oneParentAbove.getParent();

				if (oneParentAbove != null
						&& twoParentsAbove != null
						&& (twoParentsAbove.getType() == TTCN3LexerTokenTypes.ArrayValueOrAttrib || twoParentsAbove
								.getType() == TTCN3LexerTokenTypes.FieldSpecList)) {
					LocationAST templateBodyNode2 = LocationAST
							.resolveParentsUntilType(oneParentAbove,
									TTCN3LexerTokenTypes.TemplateBody);
					if (templateBodyNode2 != null && templateDefNode == null) {
						return RefactoringStatus
								.createFatalErrorStatus("Cannot inline a nested template!");
					}
				}
			}

			in.templateDefNode = LocationAST.resolveParentsUntilType(
					templateIdentifierNode, TTCN3LexerTokenTypes.TemplateDef);

			if (ASTUtil.isParameterizedTemplateReference(in.ast)) {
				in.actualParams = ASTUtil.getActualParameters(in.ast, true);
				in.formalParams = ASTUtil
						.getFormalParametersStr(in.templateDefNode);

				if (in.actualParams.size() != in.formalParams.size())
					return RefactoringStatus
							.createFatalErrorStatus("Parameter count doesn't match the declaration!");

			}
			pm.worked(1);
		}
		pm.done();

		return new RefactoringStatus();
	}

	// ---------------------------------------------------------------------------

	@Override
	public Change createChange(IProgressMonitor pm)
			throws OperationCanceledException {
		CompositeChange change = generateInlineTemplateChange(pm);
		return change == null ? new NullChange() : change;
	}

	// ---------------------------------------------------------------------------

	private CompositeChange generateInlineTemplateChange(IProgressMonitor pm) {
		pm.beginTask("Inline Template", inList.size());
		CompositeChange compositeChange = new CompositeChange("Inline Template");
		int size = inList.size();

		// reset declaration deleted
		declarationDeleted = false;

		// loop for all Inlinings
		for (int i = 0; i < size; i++) {
			Inlining in = inList.get(i);

			// get the associated file
			ModuleSymbol moduleSymbol = ASTUtil.getModuleSymbol(in.ast);
			currfile = SymbolHelper.getModuleFile(moduleSymbol);

			if (currfile == null)
				continue;

			modifiedFiles.add(currfile);

			// changes in the same file can be done in the same 'TextChange'
			// object
			TextChange change = new TextFileChange(currfile.getFullPath()
					.toOSString(), currfile);
			((TextFileChange) change).setSaveMode(TextFileChange.FORCE_SAVE);
			change.setTextType("ttcn3");
			MultiTextEdit multiEdit = new MultiTextEdit();
			change.setEdit(multiEdit);

			// loop here for all Inlinings that are in the same file
			while (i < size) {
				LocationAST templateBody = ASTUtil
						.findTemplateBodyFromTemplateDef(in.templateDefNode);

				if (templateBody == null) {
					continue;
				}

				ReplaceEdit replace = inlineTemplate(in, templateBody,
						compositeChange, multiEdit);

				if (replace == null) {
					continue;
				}

				multiEdit.addChild(replace);
				TextEditGroup declarationEditGroup = new TextEditGroup(
						"inline template", replace);
				change.addTextEditGroup(declarationEditGroup);

				// keep a count of how many we've done so we know when to delete
				inCount += 1;
				pm.worked(1);

				i++;
				if (i < size) {
					nextfile = SymbolHelper.getModuleFile(ASTUtil
							.getModuleSymbol(inList.get(i).ast));

					if (nextfile == null || !currfile.equals(nextfile)) {
						i--;
						break;
					}
					in = inList.get(i);
				}
			}

			compositeChange.add(change);
		}
		pm.done();

		return compositeChange;
	}

	// ---------------------------------------------------------------------------

	/**
	 * @return the <code>Set</code> of files modified by the refactoring
	 */
	public Set<IFile> getModifiedFiles() {
		return modifiedFiles;
	}

	// ---------------------------------------------------------------------------

	@Override
	public String getType() {
		return "Inline Template";
	}

	// ---------------------------------------------------------------------------

	private ReplaceEdit inlineTemplate(Inlining in, LocationAST pTemplateBody,
			CompositeChange compositeChange, MultiTextEdit multiEdit) {
		TemplateSymbol typeSymb = (TemplateSymbol) in.symb;

		try {
			LocationAST templateBody = ASTUtil.deepCloneAST(pTemplateBody);

			if (ASTUtil.isParameterizedTemplateReference(in.ast)) {
				rewriteParameterizedTemplateBody(in, templateBody);
			}

			if (((TTCN3InlineTemplateRefactoring) getRefactoring())
					.isDeleteDeclaration()
					&& !declarationDeleted) {
				// if only one inlining was asked for then check references
				// don't still exist to this template before removing
				boolean referencesRemain = false;
				if (inList.size() == 1) {
					Symbol symb = in.ast.getScope().resolve(in.ast);
					Map<String, List<ReferenceWithContext>> references = new ReferenceFinder()
							.findReferences(symb);
					if (references.size() > inList.size()) {
						referencesRemain = true;
					}
				}

				if (!referencesRemain) {
					String declFilename = typeSymb.getSurroundingScope()
							.getAssociatedFilename();
					String referenceFilename = in.ast.getScope()
							.getAssociatedFilename();
					if (declFilename.equals(referenceFilename)) {
						removeDeclarationForSingleReferences(
								in.templateDefNode, in.ast.getOffset(),
								multiEdit);
					} else {
						// only delete in 'other' file if we are sure no
						// references
						// were found in it, otherwise wait until these are
						// inlined
						if ((inList.size() - inCount) <= 1) {
							IResource declResource = ResourcesPlugin
									.getWorkspace().getRoot().findMember(
											declFilename);
							IFile declFile = (IFile) declResource;
							TextFileChange declFileChange = new TextFileChange(
									"Declaration Removal", declFile);
							declFileChange.setTextType("ttcn3");
							MultiTextEdit declRemovalEdit = new MultiTextEdit();
							removeDeclarationForSingleReferences(
									in.templateDefNode, in.ast.getOffset(),
									declRemovalEdit);
							declFileChange.setEdit(declRemovalEdit);
							TextEditGroup declarationEditGroup = new TextEditGroup(
									"remove declaration", declRemovalEdit);
							declFileChange
									.addTextEditGroup(declarationEditGroup);
							compositeChange.add(declFileChange);
						}
					}
				}
			}

			TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
			formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
			formatterWalker
					.setFormatterParameters(new TTCN3FormatterParameters());
			String inlinedTemplate = "";

			LocationAST derivedDef = ASTUtil.findDerivedDef(in.templateDefNode);

			if (derivedDef == null) {
				if (!ASTUtil.isFieldSpec(in.ast)) {
					String referencedType;
					if (typeSymb.getReferencedType() == null)
						referencedType = typeSymb.getPredefinedTypeAsString();
					else
						referencedType = typeSymb.getReferencedType()
								.getIdentifier().getName();

					inlinedTemplate = referencedType + ":";
				}
			} else {
				inlinedTemplate = "modifies "
						+ derivedDef.getFirstChild().getFirstChild().getText()
						+ " := ";
			}

			String result = formatterWalker.pr_TemplateBody(templateBody)
					.toString();

			// FIXME: this isn't actually pretty printing the inlining yet
			// then...
			if (((TTCN3InlineTemplateRefactoring) getRefactoring())
					.isPrettyPrintInlinedTemplates()) {
				inlinedTemplate += result;
			} else {
				inlinedTemplate += result.replaceAll("[\r\n ]", "");
			}

			LocationAST offsetAST = null;

			if (ASTUtil.isFieldSpec(in.ast)) {
				int[] types = new int[] {
						TTCN3LexerTokenTypes.FunctionInstance,
						TTCN3LexerTokenTypes.TemplateBody };
				offsetAST = LocationAST.resolveParentsUntilType(in.ast, types);
			} else {
				int[] types = new int[] { TTCN3LexerTokenTypes.InLineTemplate,
						TTCN3LexerTokenTypes.ArrayElementSpec };
				offsetAST = LocationAST.resolveParentsUntilType(in.ast, types);
			}

			// don't want NPE here...
			if (offsetAST == null)
				// TODO: there was a problem with refactoring so exit
				// accordingly
				// this null workaround results in "does not change any source
				// code" - should organise a FAIL message
				return null;

			return new ReplaceEdit(offsetAST.getOffset(), offsetAST
					.getEndOffset()
					- offsetAST.getOffset(), inlinedTemplate);
		} catch (RecognitionException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// ---------------------------------------------------------------------------

	private void removeDeclarationForSingleReferences(
			LocationAST templateDefNode, int astOffset, MultiTextEdit multiEdit) {
		// only delete if this is the last reference to the template or if the
		// template is later in the file than the current reference
		if (inList.size() - inCount <= 1
				|| templateDefNode.getOffset() > astOffset) {
			ReplaceEdit edit2;
			int startOffsetShift = 0;
			int endOffsetShift = 0;

			// check for an optional semicolon and remove that as well if found
			LocationAST parent = templateDefNode.getParent();
			LocationAST sibling = parent == null ? null : (LocationAST) parent
					.getNextSibling();

			if (sibling != null
					&& sibling.getType() == TTCN3LexerTokenTypes.SemiColon) {
				endOffsetShift += sibling.getEndOffset() - sibling.getOffset();
			}

			edit2 = new ReplaceEdit(templateDefNode.getOffset()
					+ startOffsetShift, templateDefNode.getEndOffset()
					- templateDefNode.getOffset() + endOffsetShift, "");

			multiEdit.addChild(edit2);

			declarationDeleted = true;
		}
	}

	// ---------------------------------------------------------------------------

	private void rewriteParameterizedTemplateBody(Inlining in,
			LocationAST templateBody) {
		List<LocationAST> parameterReferenceNodes = ASTUtil
				.getParameterReferenceNodes(templateBody);

		Map<String, LocationAST> parameterMap = buildParameterMap(
				in.actualParams, in.formalParams);

		for (LocationAST refNode : parameterReferenceNodes) {
			String parameterReferenceNodeText = refNode.getText();

			LocationAST targetNode = parameterMap
					.get(parameterReferenceNodeText);
			if (targetNode == null) {
				continue;
			}

			if (targetNode.getType() == TTCN3LexerTokenTypes.TemplateInstance) {

				LocationAST replaceBody = LocationAST.resolveParentsUntilType(
						refNode, TTCN3LexerTokenTypes.TemplateBody);

				replaceBody.setFirstChild(targetNode.getFirstChild()
						.getFirstChild().getFirstChild());
			} else if (targetNode.getType() == TTCN3LexerTokenTypes.Expression) {
				LocationAST replaceBody = LocationAST.resolveParentsUntilType(
						refNode, TTCN3LexerTokenTypes.SingleValueOrAttrib);
				replaceBody.setFirstChild(targetNode.getFirstChild());
			}
		}
	}
}
