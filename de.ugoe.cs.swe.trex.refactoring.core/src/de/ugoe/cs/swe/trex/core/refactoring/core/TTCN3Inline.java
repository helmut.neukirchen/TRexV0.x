package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;

import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceFinder;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;

public abstract class TTCN3Inline extends RefactoringProcessor {

	/**
	 * Builds <code>Map</code> of formal parameters to the value given to them
	 * by the actual parameter.
	 * 
	 * @param parList a list of the actual parameters for the reference
	 * @param parRefNameList a list of the formal parameters of the template
	 * @return a <code>Map</code> of formal parameters to the value given to them by the actual parameters
	 */
	protected static Map<String, LocationAST> buildParameterMap(List<LocationAST> parList, List<String> parRefNameList) {
		Map<String, LocationAST> result = new HashMap<String, LocationAST>();
		for (int i=0; i < parList.size(); i++) {
			result.put(parRefNameList.get(i), parList.get(i));
		}
		
		return result;
	}


	// ---------------------------------------------------------------------------

	protected List<Inlining> inList = new ArrayList<Inlining>();

	// ---------------------------------------------------------------------------

	public TTCN3Inline() {
		super();
		// TODO Auto-generated constructor stub
	}	
	
	// ---------------------------------------------------------------------------	
	
	public TTCN3Inline(List<LocationAST> astList) {
		for(LocationAST ast : astList) {
			this.inList.add(new Inlining(ast));
		}
	}
	
	// ---------------------------------------------------------------------------	
	
	/**
	 * Inline into a single <code>AST</code>. Used by Tests.
	 * @param ast a reference to the template to inline.
	 */
	public TTCN3Inline(LocationAST ast) {
		loadInlinings(ast);
	}
	
	// ---------------------------------------------------------------------------	
	
	public TTCN3Inline(TTCN3Editor editor) {
		LocationAST ast = TTCN3Editor.getEditorAST(editor);
		loadInlinings(ast);
	}
	
	// ---------------------------------------------------------------------------	

	protected void loadInlinings(LocationAST ast) {
		Symbol symb = ast.getScope().resolve(ast);
		
		// if user is trying to inline based on a template itself
		if (ast.getParent().getParent().getType() == TTCN3LexerTokenTypes.BaseTemplate) {
			// get all references to this and then make inlining objects for all of those.
			Map<String, List<ReferenceWithContext>> references = new ReferenceFinder().findReferences(symb);			
			
			// extract references from result
			// and cope with multiple references
			for(Map.Entry<String, List<ReferenceWithContext>> entry : references.entrySet()) {
				List<ReferenceWithContext> refs = entry.getValue();
				ListIterator<ReferenceWithContext> refsIt = refs.listIterator( refs.size() );
				
				while(refsIt.hasPrevious()) {
					ReferenceWithContext prev = refsIt.previous();
					if(ast.equals(prev.getMatchNode())) {
						this.inList.add(new Inlining(prev.getReferenceNode()));
					}
				}
			}
			return;
		}			
		
		this.inList.add(new Inlining(ast));
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkFinalConditions(IProgressMonitor pm, CheckConditionsContext context) throws OperationCanceledException {
		return new RefactoringStatus();
	}	
	
	// ---------------------------------------------------------------------------	
	
	@Override
	public Object[] getElements() {
		return new String[0];
	}
	
	// ---------------------------------------------------------------------------	
	
	@Override
	public String getIdentifier() {
		return getType()+" Identifier";
	}
	
	// ---------------------------------------------------------------------------	
	
	@Override
	public String getProcessorName() {
		return getType()+" Processor";
	}
	
	// ---------------------------------------------------------------------------	

	public String getType() {
		return "Default";
	}
	
	// ---------------------------------------------------------------------------	
	
	@Override
	public boolean isApplicable() {
		for(Inlining in : this.inList) {
			if (in.symb == null) {
				return false;
			}
		}
		
		return true;
	}	
	
	// ---------------------------------------------------------------------------	
	
	@Override
	public RefactoringParticipant[] loadParticipants(RefactoringStatus status, SharableParticipants sharedParticipants) {
		return new RefactoringParticipant[0];
	}
}
