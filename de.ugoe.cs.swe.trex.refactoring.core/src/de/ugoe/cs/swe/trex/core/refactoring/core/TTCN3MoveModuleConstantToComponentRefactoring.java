package de.ugoe.cs.swe.trex.core.refactoring.core;

import org.eclipse.core.runtime.Assert;
import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

public class TTCN3MoveModuleConstantToComponentRefactoring extends ProcessorBasedRefactoring {
	
	private RefactoringProcessor fProcessor;
	public TTCN3MoveModuleConstantToComponentRefactoring(RefactoringProcessor processor) {
		super(processor);
		Assert.isNotNull(processor);
		fProcessor= processor;
	}
	public RefactoringProcessor getProcessor() {
		return fProcessor;
	}
}
