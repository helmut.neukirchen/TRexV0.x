
package de.ugoe.cs.swe.trex.core.refactoring.core;

import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

public class TTCN3ReplaceTemplateWithModifiedRefactoring extends ProcessorBasedRefactoring {

	RefactoringProcessor processor;
	private int limit;
	private boolean restrictToThisModule, locateInAllModules, locateInThisModule;
	
	// ---------------------------------------------------------------------------

	@Override
	public RefactoringProcessor getProcessor() {
		return processor;
	}

	// ---------------------------------------------------------------------------
	public TTCN3ReplaceTemplateWithModifiedRefactoring(TTCN3ReplaceTemplateWithModifiedProcessor processor) {
		super(processor);
		this.processor = processor;
	}
	
	// ---------------------------------------------------------------------------

	public boolean getRestrictToThisModule()
	{
		return restrictToThisModule;
	}
	public boolean getLocateInAllModules()
	{
		return locateInAllModules;
	}
	public boolean getLocateInThisModule()
	{
		return locateInThisModule;
	}

	
	
	public void setRestrictToThisModule(boolean a)
	{
		this.restrictToThisModule = a;
	}
	
	public void setLocateInAllModules(boolean a)
	{
		this.locateInAllModules = a;
	}
	public void setLocateInThisModule(boolean a)
	{
		this.locateInThisModule = a;
	}

	
	public int getLimit() {
		return limit;
	}

	// ---------------------------------------------------------------------------

	public void setLimit(int limit) {
		this.limit = limit;
	}	
}
