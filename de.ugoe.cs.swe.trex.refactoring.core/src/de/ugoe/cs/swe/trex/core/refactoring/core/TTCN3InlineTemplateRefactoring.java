package de.ugoe.cs.swe.trex.core.refactoring.core;

import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

public class TTCN3InlineTemplateRefactoring extends ProcessorBasedRefactoring {

	RefactoringProcessor processor;
	private boolean prettyPrintInlinedTemplates = false;
	private boolean deleteDeclaration = false;
	

	// ---------------------------------------------------------------------------

	public TTCN3InlineTemplateRefactoring(TTCN3InlineTemplateProcessor processor) {
		super(processor);
		this.processor = processor;
	}
	
	// ---------------------------------------------------------------------------
	@Override
	public RefactoringProcessor getProcessor() {
		return processor;
	}

	// ---------------------------------------------------------------------------

	public boolean isDeleteDeclaration() {
		return deleteDeclaration;
	}

	// ---------------------------------------------------------------------------

	public void setDeleteDeclaration(boolean deleteDeclaration) {
		this.deleteDeclaration = deleteDeclaration;
	}

	// ---------------------------------------------------------------------------

	public boolean isPrettyPrintInlinedTemplates() {
		return prettyPrintInlinedTemplates;
	}

	// ---------------------------------------------------------------------------

	public void setPrettyPrintInlinedTemplates(boolean prettyPrintInlinedTemplates) {
		this.prettyPrintInlinedTemplates = prettyPrintInlinedTemplates;
	}


}
