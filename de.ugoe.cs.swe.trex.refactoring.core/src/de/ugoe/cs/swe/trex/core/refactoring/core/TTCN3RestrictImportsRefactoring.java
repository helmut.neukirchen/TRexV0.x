package de.ugoe.cs.swe.trex.core.refactoring.core;

import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

public class TTCN3RestrictImportsRefactoring extends ProcessorBasedRefactoring {

	RefactoringProcessor processor;
	
	// ---------------------------------------------------------------------------

	@Override
	public RefactoringProcessor getProcessor() {
		return processor;
	}

	// ---------------------------------------------------------------------------
	public TTCN3RestrictImportsRefactoring(TTCN3RestrictImportsProcessor processor) {
		super(processor);
		this.processor = processor;
	}
	

}
