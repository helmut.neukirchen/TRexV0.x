package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEditGroup;

import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceFinder;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.refactoring.core.util.ConfigurationIterator;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;

final class IntWrapper {
	int value;

	public IntWrapper(int value) {
		super();
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}

final class ImportConfiguration {
	private String configurationName;
	private boolean group;
	private boolean useConfiguration;
	private boolean parity;

	public ImportConfiguration() {
		useConfiguration = false;
		parity = false;
	}

	public boolean isParity() {
		return parity;
	}

	public void setParity(boolean parity) {
		this.parity = parity;
	}

	public boolean isUseConfiguration() {
		return useConfiguration;
	}

	public void setUseConfiguration(boolean useConfiguration) {
		this.useConfiguration = useConfiguration;
	}

	public void setByNumber(int n) {
		if (n == 1) {
			useConfiguration = false;
			parity = false;
		}
		if (n == 2) {
			useConfiguration = false;
			parity = true;
		}
		if (n == 3) {
			useConfiguration = true;
			parity = false;
		}
		if (n == 4) {
			useConfiguration = true;
			parity = true;
		}
	}

	public String getConfigurationName() {
		return configurationName;
	}

	public void setConfigurationName(String configurationName) {
		this.configurationName = configurationName;
	}

	public boolean isGroup() {
		return group;
	}

	public void setGroup(boolean group) {
		this.group = group;
	}
}

public class TTCN3RestrictImportsProcessor extends RefactoringProcessor {

	private LocationAST calledModule;
	private Set<IFile> modifiedFiles = new HashSet<IFile>();

	// private ImportConfiguration a;

	public TTCN3RestrictImportsProcessor(LocationAST calledNode) {
		calledModule = null;
		if (calledNode == null)
			return;
		calledModule = LocationAST.resolveParentsUntilType(calledNode,
				TTCN3ParserTokenTypes.TTCN3Module);
	}

	public TTCN3RestrictImportsProcessor(LocationAST calledNode,
			TTCN3Editor myEditor) {
		calledModule = null;
		if (calledNode == null) {
			int i = 0;
			int o = myEditor.getCaretOffset();
			while ((i > o)
					&& (getIdentifierNodeFromOffset(myEditor, o + i) == null))
				i--;
			calledNode = getIdentifierNodeFromOffset(myEditor, o + i);
			if (calledNode == null)
				return;

		}
		calledModule = LocationAST.resolveParentsUntilType(calledNode,
				TTCN3ParserTokenTypes.TTCN3Module);
		if (calledModule == null) {
			// int myOffset = calledModule.getEndOffset();
			// TTCN3Editor myEditor = new TTCN3Editor(new
			// TTCN3MultiPageEditor());
			// myEditor.
			LocationAST x = calledNode;
			int i = 1;
			int z = 0;
			while ((z <= 100)
					&& (LocationAST.resolveParentsUntilType(x,
							TTCN3ParserTokenTypes.TTCN3Module) == null)) {
				x = getIdentifierNodeFromEditorOffset(myEditor, i);
				i++;
				if (x == null)
					z++;
			}
			// if (x==null) return;
			x = LocationAST.resolveParentsUntilType(x,
					TTCN3ParserTokenTypes.TTCN3Module);
			if ((x != null) && (x.getOffset() <= calledNode.getOffset())
					&& (x.getEndOffset() >= calledNode.getEndOffset())) {
				calledModule = LocationAST.resolveParentsUntilType(x,
						TTCN3ParserTokenTypes.TTCN3Module);
				return;
			}

			i = -1;
			// int z = 0;
			while ((i >= myEditor.getCaretOffset())
					&& (LocationAST.resolveParentsUntilType(x,
							TTCN3ParserTokenTypes.TTCN3Module) == null)) {
				x = getIdentifierNodeFromEditorOffset(myEditor, i);
				i--;
			}
			x = LocationAST.resolveParentsUntilType(x,
					TTCN3ParserTokenTypes.TTCN3Module);
			if ((x != null) && (x.getOffset() <= calledNode.getOffset())
					&& (x.getEndOffset() >= calledNode.getEndOffset())) {
				calledModule = LocationAST.resolveParentsUntilType(x,
						TTCN3ParserTokenTypes.TTCN3Module);
			}

		}

	}

	private LocationAST getIdentifierNodeFromEditorOffset(TTCN3Editor editor,
			int plusOffset) {
		Integer offset = editor.getCaretOffset() + plusOffset;
//		if (offset == null)
//			return null;

		final IdentifierCache identifierCache = editor.getReconcilingStrategy()
				.getIdentifierCache();
		return (identifierCache == null) ? null : identifierCache.get(offset);
	}

	private LocationAST getIdentifierNodeFromOffset(TTCN3Editor editor,
			int o) {
		Integer offset = o;
		if (offset == null)
			return null;

		final IdentifierCache identifierCache = editor.getReconcilingStrategy()
				.getIdentifierCache();
		return (identifierCache == null) ? null : identifierCache.get(offset);
	}

	private void processDefinitions(List<LocationAST> x, LocationAST node) {
		if (node == null)
			return;
		// System.out.println("uuoooossh");
		if (node.getType() == TTCN3ParserTokenTypes.ImportDef) {
			while (node.getFirstChild() != null)
				node = node.getFirstChild();
			// System.out.println("blingblingfirst");

			// Scope scope = node.getScope();
			// Symbol symb = scope.resolve(node);

			Symbol symb = node.getSymbol();

			// if (symb==null) symb = scope.getScopeSymbol();
			if (symb == null)
				return;
			LocationAST g = symb.getDeclarationNode();
			x.add(LocationAST.resolveParentsUntilType(g,
					TTCN3ParserTokenTypes.TTCN3Module));
			// System.out.println("blingbling");
			return;
		}
		processDefinitions(x, node.getFirstChild());
		processDefinitions(x, node.getNextSibling());
	}

	private void cleanList(List<LocationAST> x) {
		Iterator<LocationAST> iterator = x.iterator();
		while (iterator.hasNext()) {
			if (iterator.next() == null)
				iterator.remove();
		}
	}

	private List<LocationAST> removeMultiples(List<LocationAST> x) {
		cleanList(x);
		List<LocationAST> ret;
		ret = new LinkedList<LocationAST>();
		Iterator<LocationAST> iterator = x.iterator();
		while (iterator.hasNext()) {
			LocationAST z = iterator.next();
			Iterator<LocationAST> pratt = ret.iterator();
			while (pratt.hasNext()) {
				LocationAST eins = pratt.next();
				if (eins == z)
					pratt.remove(); // change: eins.equals(z)
			}
			ret.add(z);

		}
		cleanList(ret);

		return ret;
	}

	private List<LocationAST> allDefinitionModules() {
		List<LocationAST> ret;
		ret = new LinkedList<LocationAST>();
		processDefinitions(ret, calledModule);

		return removeMultiples(ret);
	}

	/*
	 * private void killSemicolons(List<LocationAST> ret) {
	 * Iterator<LocationAST> iterator = ret.iterator(); while
	 * (iterator.hasNext()) { if
	 * (iterator.next().getType()==TTCN3ParserTokenTypes.SemiColon)
	 * iterator.remove(); } }
	 */

	private List<LocationAST> newNotImportedElementsInModule(LocationAST xmodule) {
		List<LocationAST> ret;
		ret = new LinkedList<LocationAST>();
		xmodule = xmodule.getFirstChild();
		processnewNotImportedElementsInModule(xmodule, ret);
		return ret;
	}

	private void processnewNotImportedElementsInModule(LocationAST xmodule,
			List<LocationAST> ret) {
		if (xmodule == null)
			return;

		if ((xmodule.getType() == TTCN3ParserTokenTypes.ModuleDefinition)
				&& (!assureIsNoGroup(xmodule))
				&& (!isUsed(calledModule, xmodule)))
			ret.add(xmodule); // aendern!!
		processnewNotImportedElementsInModule(xmodule.getFirstChild(), ret);
		processnewNotImportedElementsInModule(xmodule.getNextSibling(), ret);
	}

	private void toList(List<LocationAST> ret, LocationAST x) {
		if (x == null)
			return;
		ret.add(x);
		toList(ret, x.getFirstChild());
		toList(ret, x.getNextSibling());
	}

	private boolean isReallyNeeded(LocationAST x) {
		List<LocationAST> myList = new LinkedList<LocationAST>();
		toList(myList, x.getFirstChild());
		Iterator<LocationAST> myIterator = myList.iterator();
		String id = "";
		while (myIterator.hasNext()) {
			LocationAST z = myIterator.next();
			if (z.getType() == TTCN3ParserTokenTypes.Identifier)
				id = z.getFirstChild().getText();
		}
		myList = new LinkedList<LocationAST>();
		toList(myList, calledModule.getFirstChild());
		myIterator = myList.iterator();
		while (myIterator.hasNext()) {
			LocationAST z = myIterator.next();
			if ((z.getType() == TTCN3ParserTokenTypes.Identifier)
					&& (z.getFirstChild().getText().equals(id))
					&& (LocationAST.resolveParentsUntilType(z,
							TTCN3ParserTokenTypes.ImportDef) == null))
				return true;

		}

		return false;
	}

	private List<LocationAST> newImportedElementsInModule(LocationAST xmodule) {
		List<LocationAST> ret;
		ret = new LinkedList<LocationAST>();
		xmodule = xmodule.getFirstChild();
		processnewImportedElementsInModule(xmodule, ret);
		Iterator<LocationAST> myIterator = ret.iterator();
		while (myIterator.hasNext()) {
			LocationAST x = myIterator.next();
			if (!isReallyNeeded(x))
				myIterator.remove();
		}
		return ret;
	}

	private void processnewImportedElementsInModule(LocationAST xmodule,
			List<LocationAST> ret) {
		if (xmodule == null)
			return;

		if ((xmodule.getType() == TTCN3ParserTokenTypes.ModuleDefinition)
				&& (!assureIsNoGroup(xmodule))
				&& (isUsed(calledModule, xmodule)))
			ret.add(xmodule); // aendern!!
		processnewImportedElementsInModule(xmodule.getFirstChild(), ret);
		processnewImportedElementsInModule(xmodule.getNextSibling(), ret);
	}

	private boolean assureIsNoGroup(LocationAST x) {
		if (x == null)
			return false;
		if (x.getType() == TTCN3ParserTokenTypes.GroupDef)
			return true;
		if (assureIsNoGroup(x.getFirstChild()))
			return true;
		if (assureIsNoGroup(x.getNextSibling()))
			return true;
		return false;
	}

	private List<LocationAST> importedElementsInModule(LocationAST xmodule) {
		LocationAST module = LocationAST.resolveParentsUntilType(xmodule,
				TTCN3ParserTokenTypes.TTCN3Module);
		module = module.getFirstChild().getNextSibling().getFirstChild()
				.getFirstChild();
		List<LocationAST> ret;
		ret = new LinkedList<LocationAST>();
		while (module != null) {
			LocationAST x = module;

			if (x.getType() != TTCN3ParserTokenTypes.ModuleDefinition) {
				module = module.getNextSibling();
				continue;
			}

			if (isUsed(calledModule, x))
				ret.add(module);
			module = module.getNextSibling();
		}
		// killSemicolons(ret);
		return removeMultiples(ret);

	}

	private List<LocationAST> nonImportedElementsInModule(LocationAST xmodule) {
		LocationAST module = LocationAST.resolveParentsUntilType(xmodule,
				TTCN3ParserTokenTypes.TTCN3Module);
		module = module.getFirstChild().getNextSibling().getFirstChild()
				.getFirstChild();
		List<LocationAST> ret;
		ret = new LinkedList<LocationAST>();
		while (module != null) {
			LocationAST x = module;

			if (x.getType() != TTCN3ParserTokenTypes.ModuleDefinition) {
				module = module.getNextSibling();
				continue;
			}

			if (!isUsed(calledModule, x))
				ret.add(module);
			module = module.getNextSibling();
		}
		// killSemicolons(ret);
		return removeMultiples(ret);

	}

	private String makeNicerLooking(String s) {
		// String ret ="";
		// int i = 0;
		if (s.length() == 0)
			return s;
		s = s.replace("\n", ""); // kill the new lines
		s = s.replace("; ", ";");
		s = s.replace(", ", ",");
		s = s.replace("except ", "except,");
		s = s.replace("except,{", "except {");
		int i = 0;
		int braces = 1;
		while (i < s.length()) {
			if (s.charAt(i) == "{".charAt(0)) {
				braces++;
				String rez = "";
				int go = 1;
				int u = braces;
				while (u > 0) {
					rez = rez + "\t";
					go++;
					u--;
				}
				String raz = "";
				int v = braces - 1;
				while (v > 0) {
					raz = raz + "\t";
					go++;
					v--;
				}
				String ges = s;
				s = ges.substring(0, i) + "\n" + raz + "{\n" + rez
						+ ges.substring(i + 1, s.length());
				i = i + go;
				continue;
			}

			if (s.charAt(i) == ";".charAt(0)) {
				String ges = s;
				String rez = "";
				int go = 1;
				int u = braces;
				while (u > 0) {
					rez = rez + "\t";
					go++;
					u--;
				}
				s = ges.substring(0, i) + ";\n" + rez
						+ ges.substring(i + 1, s.length());
				i = i + go;
				continue;
			}

			if (s.charAt(i) == ",".charAt(0)) {
				String ges = s;
				String rez = "";
				int go = 1;
				int u = braces;
				while (u > 0) {
					rez = rez + "\t";
					go++;
					u--;
				}
				s = ges.substring(0, i) + ",\n" + rez
						+ ges.substring(i + 1, s.length());
				i = i + go;
				continue;
			}

			if (s.charAt(i) == "}".charAt(0)) {
				braces--;
				String rez = "";
				int go = 1;
				int u = braces;
				while (u > 0) {
					rez = rez + "\t";
					go++;
					u--;
				}
				if (i == s.length() - 1) {
					s = s.substring(0, i - 1) + "}";
					break;
				}
				s = s.substring(0, i - 1) + "}\n" + rez + s.substring(i + 1);
				i = i + go;
				continue;
			}

			i++;
		}

		s = s.replace("except,", "except ");

		return s;
	}

	private void fillUpImportedList(LocationAST x, List<LocationAST> list) {
		if (x == null)
			return;
		if (x.getType() == TTCN3ParserTokenTypes.ImportDef)
			list.add(x);
		fillUpImportedList(x.getFirstChild(), list);
		fillUpImportedList(x.getNextSibling(), list);
	}

	public Change createChange(IProgressMonitor pm) throws CoreException,
			OperationCanceledException {
		List<LocationAST> myModuleList = allDefinitionModules();
		LocationAST[] myModules = (LocationAST[]) myModuleList
				.toArray(new LocationAST[myModuleList.size()]);
		LocationAST[][] myImportedElements;
		LocationAST[][] myNonImportedElements;
		myImportedElements = new LocationAST[myModules.length][];
		myNonImportedElements = new LocationAST[myModules.length][];
		// set up the arrays
		for (int i = 0; i < myModules.length; i++) {
			myImportedElements[i] = (LocationAST[]) newImportedElementsInModule(
					myModules[i]).toArray(
					new LocationAST[importedElementsInModule(myModules[i])
							.size()]);
			myNonImportedElements[i] = (LocationAST[]) newNotImportedElementsInModule(
					myModules[i]).toArray(
					new LocationAST[nonImportedElementsInModule(myModules[i])
							.size()]);
		}
		// LocationAST[][] myConfigurations;
		// myConfigurations = new LocationAST[myModules.length][];

		LocationAST x = LocationAST.resolveParentsUntilType(calledModule,
				TTCN3ParserTokenTypes.TTCN3Module);
		List<LocationAST> myImportModuleList = new LinkedList<LocationAST>();
		fillUpImportedList(x, myImportModuleList);
		Iterator<LocationAST> myIterator = myImportModuleList.iterator();

		CompositeChange compositeChange = new CompositeChange(
				"Replace Import Statements");
		LocationAST n = calledModule;
		while (n.getFirstChild() != null)
			n = n.getFirstChild();

		Symbol mySymbol = getDeclarationSymbol(n);
		if (mySymbol == null)
			return compositeChange;
		String myFileName = mySymbol.getSurroundingScope()
				.getAssociatedFilename();
		IFile declarationFile = getProjectFile(myFileName);
		modifiedFiles.add(declarationFile);

		MultiTextEdit multiEdit = new MultiTextEdit();
		TextFileChange fileChange = new TextFileChange(declarationFile
				.getName(), declarationFile);

		fileChange.setEdit(multiEdit);
		fileChange.setTextType("ttcn3");

		// Todo: Remove non-relevant import statements
		// in myimportmodulelist: TTCN3Modules

		// Exceptional handling for modules from that nothing is imported:
		// List<LocationAST> myImportStatements:

		while (myIterator.hasNext()) {
			int i = 0;
			LocationAST runner = myIterator.next();
			while (i < myModules.length) {
				LocationAST alpha = myModules[i];
				while (alpha.getFirstChild() != null)
					alpha = alpha.getFirstChild();
				LocationAST beta = runner;
				while (beta.getFirstChild() != null)
					beta = beta.getFirstChild();
				if (beta.getText().equals(alpha.getText()))
					break;
				i++;
			}
			String noder = "";
			if (i >= myModules.length) // this is exception handling for void
			// imports
			{

				noder = "";
			} else {
				ConfigurationIterator myConfigurationIterator = new ConfigurationIterator(
						myModules[i]);
				myConfigurationIterator.initialize();
				myConfigurationIterator.integrateElements(
						myImportedElements[i], myNonImportedElements[i]);
				myConfigurationIterator.setUpIterator();
				noder = "This is not a legal import Statement!!";
				while (myConfigurationIterator.hasNext()) {
					// String test =
					// makeNicerLooking(myConfigurationIterator.getNext());
					String test1 = myConfigurationIterator.getNext();
					String test = makeNicerLooking(test1);

					myConfigurationIterator.integrateElements(
							myImportedElements[i], myNonImportedElements[i]);
//					System.out.println(test1 + "\n");
//					System.out.println(test + "\n");

					if ((noder.equals("This is not a legal import Statement!!"))
							|| (isBetter(test, noder)))
						noder = test;
				}
				LocationAST u = myModules[i];
				while (u.getFirstChild() != null)
					u = u.getFirstChild();
				if (myNonImportedElements[i].length == 0)
					noder = "import from " + u.getText() + " all;";
				if (myImportedElements[i].length == 0)
					noder = "";
			}

			LocationAST beta = runner;
			while (beta.getFirstChild() != null)
				beta = beta.getFirstChild();

			int a = runner.getOffset();
			int b = runner.getParent().getNextSibling().getEndOffset(); // No
			// import
			// -
			// definition
			// without
			if ((runner.getParent().getNextSibling() == null)
					|| (runner.getParent().getNextSibling().getType() != TTCN3ParserTokenTypes.SemiColon))
				b = runner.getEndOffset();// a semicolon!

			ReplaceEdit replaceEdit = new ReplaceEdit(a, b - a, noder);
			multiEdit.addChild(replaceEdit);
			TextEditGroup master = new TextEditGroup(
					"Replace import statement concerning module "
							+ beta.getText());
			master.addTextEdit(replaceEdit);
			fileChange.addTextEditGroup(master);

		}

		return fileChange;
	}

	private boolean isBetter(String challenge, String reference) {
		int newlines = 0;
		for (int i = 0; i < challenge.length(); i++) {
			if (challenge.charAt(i) == "\n".charAt(0))
				newlines++;
		}
		for (int i = 0; i < reference.length(); i++) {
			if (reference.charAt(i) == "\n".charAt(0))
				newlines--;
		}
		if (newlines > 0)
			return false;
		if (newlines < 0)
			return true;
		if (challenge.length() < reference.length())
			return true;
		return false;

	}

	private Symbol getDeclarationSymbol(LocationAST identifierNode) {
		Scope scope = identifierNode.getScope();
		if (scope == null)
			return null;
		return scope.resolve(identifierNode);
	}

	private IFile getProjectFile(String filename) {
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		IProject project = root.getProject();

		IFile file = null;
		if (project == null) {
			IPath path = ws.getRoot().findMember(filename).getFullPath();
			file = ws.getRoot().getFile(path);
		} else {
			file = project.getFile(filename);
		}

		return file;
	}

	public RefactoringStatus checkInitialConditions(IProgressMonitor pm)
			throws CoreException, OperationCanceledException {
		if (calledModule == null || !hasImports(calledModule))
			return RefactoringStatus
					.createFatalErrorStatus("The selected Module does not contain any import statements.");
		return new RefactoringStatus();
	}

	public RefactoringStatus checkFinalConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws CoreException,
			OperationCanceledException {
		return new RefactoringStatus();
	}

	public String getProcessorName() {
		return "Restrict Imports Processor";
	}

	public String getIdentifier() {
		return "Restrict Imports Identifier";
	}

	public Object[] getElements() {
		return new String[0];
	}

	public boolean isApplicable() throws CoreException {
		if (calledModule == null)
			return false;
		return true;
	}

	private boolean hasImports(LocationAST x) {
		if (x == null)
			return false;
		if (x.getType() == TTCN3ParserTokenTypes.ImportDef)
			return true;
		if (hasImports(x.getFirstChild()))
			return true;
		if (hasImports(x.getNextSibling()))
			return true;
		return false;
	}

	public RefactoringParticipant[] loadParticipants(RefactoringStatus status,
			SharableParticipants sharedParticipants) throws CoreException {
		return new RefactoringParticipant[0];
	}

	// checks if bc is used in the module in that quest lives
	// quest.type = ttcn3module; isimported.type = moduledefinition
	private boolean isUsed(LocationAST quest, LocationAST isImported) {
		if (quest == null)
			return false;
		if (isImported == null)
			return false;

		// while (quest.getFirstChild()!= null) quest = quest.getFirstChild();
		quest = LocationAST.resolveParentsUntilType(quest,
				TTCN3ParserTokenTypes.TTCN3Module);

		while (isImported.getFirstChild() != null) {
			if (isImported.getType() == TTCN3ParserTokenTypes.Type) {
				if (isImported.getNextSibling() == null)
					return false;
				isImported = isImported.getNextSibling();
				continue;
			}
			isImported = isImported.getFirstChild();

		}

		LocationAST ident = ASTUtil.getIdentNode(isImported);
		Symbol symb = ident.getScope().resolve(ident);
		Map<String, List<ReferenceWithContext>> references = new ReferenceFinder()
				.findReferences(symb);
		// System.out.println(isImported.toString());
		// for (String y : references.keySet()) {System.out.println(y);}
		// return references.containsKey(quest.getText());
		for (String y : references.keySet()) {
			Iterator<ReferenceWithContext> z = references.get(y).iterator();
			while (z.hasNext())
				if (LocationAST.resolveParentsUntilType(
						z.next().getReferenceNode(),
						TTCN3ParserTokenTypes.TTCN3Module).equals(quest))
					return true;
		}
		return false;

		// return false;
		// Scope scope = quest.getScope();
		// Symbol symb = scope.resolve(quest);
		// if (symb==null) return false;
		// Map<String, Symbol> a = scope.getSymbols();
		// if(isImported.getScope()==null) return false;
		// return a.containsValue(isImported.getSymbol());

	}

}
