package de.ugoe.cs.swe.trex.core.refactoring.core;

public class RefactoringException extends Exception {

	private static final long serialVersionUID = 1683967402644703273L;

	public RefactoringException() {
		super();
	}

	public RefactoringException(String message, Throwable cause) {
		super(message, cause);
	}

	public RefactoringException(String message) {
		super(message);
	}

	public RefactoringException(Throwable cause) {
		super(cause);
	}
}
