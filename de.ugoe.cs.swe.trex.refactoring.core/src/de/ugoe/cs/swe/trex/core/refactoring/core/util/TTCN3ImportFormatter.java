package de.ugoe.cs.swe.trex.core.refactoring.core.util;

import java.io.StringReader;

import antlr.RecognitionException;
import antlr.TokenStreamException;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ASTFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Lexer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Parser;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TokenStreamTracker;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3Formatter;

public class TTCN3ImportFormatter extends TTCN3Formatter {

	
	
	
	
	protected TokenStreamTracker parseTTCN3File(String text)
	throws RecognitionException, TokenStreamException {
TTCN3Lexer lexer = new TTCN3Lexer(new StringReader(text));

lexer.setTokenObjectClass(de.ugoe.cs.swe.trex.core.analyzer.rfparser.TokenWithIndex.class);
TokenStreamTracker track = new TokenStreamTracker(lexer);
configureTokenStreamTracker(track);

parser = new TTCN3Parser(track);
parser.setASTFactory(new TTCN3ASTFactory());
parser.getASTFactory().setASTNodeClass(LocationAST.class);

parser.pr_ImportDef();

if (parser.getExceptions().size() > 0)
	throw parser.getExceptions().get(0);

return track;
}

}
