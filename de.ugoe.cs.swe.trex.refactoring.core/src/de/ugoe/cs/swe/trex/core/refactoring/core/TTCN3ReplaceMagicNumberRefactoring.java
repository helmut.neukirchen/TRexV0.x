package de.ugoe.cs.swe.trex.core.refactoring.core;

import org.eclipse.ltk.core.refactoring.participants.RenameProcessor;
import org.eclipse.ltk.core.refactoring.participants.RenameRefactoring;

public class TTCN3ReplaceMagicNumberRefactoring extends RenameRefactoring {

	private boolean processEachOccurrence;
	private String newTemplateNodeName;

		
	public TTCN3ReplaceMagicNumberRefactoring(RenameProcessor processor) {
		super(processor);
		processEachOccurrence = false;
		if(processor instanceof TTCN3ReplaceMagicNumberProcessor) {
			TTCN3ReplaceMagicNumberProcessor replaceMagicNumberProcessor = (TTCN3ReplaceMagicNumberProcessor) processor;
			setNewTemplateNodeName(replaceMagicNumberProcessor.getNewTemplateNodeName());
		}
	}

	public boolean isProcessEachOccurrence() {
		return processEachOccurrence;
	}

	public void setProcessEachOccurrence(boolean processEachOccurrence) {
		this.processEachOccurrence = processEachOccurrence;
	}

	public String getNewConstantNumberName() {
		return newTemplateNodeName;
	}

	public void setNewTemplateNodeName(String newTemplateNodeName) {
		this.newTemplateNodeName = newTemplateNodeName;
	}



}