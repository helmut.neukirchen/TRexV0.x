package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RenameProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEditGroup;

import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceFinder;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;

public class TTCN3RenameProcessor extends RenameProcessor {

	private Symbol declarationSymbol;

	private LocationAST declarationNode;

	private Map<String, List<ReferenceWithContext>> references;

	private String currentName;

	private Set<IFile> modifiedFiles = new HashSet<IFile>();

	public TTCN3RenameProcessor(LocationAST identifierNode) {
		if (identifierNode == null)
			return;
		if (identifierNode.getType() != TTCN3LexerTokenTypes.IDENTIFIER)
			return;
		declarationSymbol = getDeclarationSymbol(identifierNode);
		if (declarationSymbol == null)
			return;
		references = new ReferenceFinder().findReferences(declarationSymbol);
		declarationNode = declarationSymbol.getDeclarationNode();
		if (declarationNode == null)
			return;
		currentName = declarationNode.getText();
	}

	// ---------------------------------------------------------------------------

	private Symbol getDeclarationSymbol(LocationAST identifierNode) {
		Scope scope = identifierNode.getScope();
		if (scope == null)
			return null;
		return scope.resolve(identifierNode);
	}

	// ---------------------------------------------------------------------------

	@Override
	public Object[] getElements() {
		return new String[0];
	}

	// ---------------------------------------------------------------------------

	@Override
	public String getIdentifier() {
		return "Rename Processor Identifier";
	}

	// ---------------------------------------------------------------------------

	/**
	 * @return the <code>Set</code> of files modified by the refactoring
	 */
	public Set<IFile> getModifiedFiles() {
		return modifiedFiles;
	}

	// ---------------------------------------------------------------------------

	@Override
	public String getProcessorName() {
		return "Rename Processor";
	}

	// ---------------------------------------------------------------------------

	@Override
	public boolean isApplicable() {
		return (declarationSymbol != null && references != null
				&& declarationNode != null && currentName != null);
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm)
			throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		if (declarationSymbol == null) {
			status.addFatalError("Please select a valid identifier!");
		}
		if (references == null) {
			status.addFatalError("Could not obtain references!");
		}

		return status;
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkFinalConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		TTCN3RenameRefactoring refactoring = (TTCN3RenameRefactoring) getRefactoring();
		if (currentName.equals(refactoring.getRenameText())) {
			status.addFatalError("Name unchanged!");
		} else if (refactoring.getRenameText().length() <= 0) {
			status.addFatalError("Name may not be empty!");
		} else if (existsNameClashInScope()) {
			status.addFatalError("Name is in use by another declaration!");
		}
		return status;
	}

	// ---------------------------------------------------------------------------

	private boolean existsNameClashInScope() {
		final String replaceText = ((TTCN3RenameRefactoring) getRefactoring())
				.getRenameText();
		final Symbol symbol = declarationSymbol.getSurroundingScope().resolve(
				replaceText);
		if (symbol == null) {
			// no name clash
			return false;
		}
		return true;
	}

	// ---------------------------------------------------------------------------

	private IFile getProjectFile(String filename) {
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		IProject project = root.getProject();

		IFile file = null;
		if (project == null) {
			IPath path = ws.getRoot().findMember(filename).getFullPath();
			file = ws.getRoot().getFile(path);
		} else {
			file = project.getFile(filename);
		}

		return file;
	}

	// ---------------------------------------------------------------------------

	private void createChanges(CompositeChange composite, IProgressMonitor pm) {
		final Set<String> refsKeySet = references.keySet();
		pm.beginTask("Rename Refactoring", refsKeySet.size());

		String declarationFileName = declarationSymbol.getSurroundingScope()
				.getAssociatedFilename();
		IFile declarationFile = getProjectFile(declarationFileName);
		modifiedFiles.add(declarationFile);
		MultiTextEdit declarationMultiEdit = new MultiTextEdit();
		TextFileChange declarationFileChange = new TextFileChange(
				declarationFile.getName(), declarationFile);
		declarationFileChange.setEdit(declarationMultiEdit);
		declarationFileChange.setTextType("ttcn3");
		composite.add(declarationFileChange);

		// update declaration
		addReplaceEditForNode(declarationNode, "update declaration",
				declarationMultiEdit, declarationFileChange);
		pm.worked(1);

		for (String filename : refsKeySet) {
			List<ReferenceWithContext> refs = references.get(filename);
			IFile file = getProjectFile(filename);
			modifiedFiles.add(file);
			MultiTextEdit multiEdit;
			TextFileChange fileChange;
			if (filename.equals(declarationFileName)) {
				multiEdit = declarationMultiEdit;
				fileChange = declarationFileChange;
			} else {
				multiEdit = new MultiTextEdit();
				fileChange = new TextFileChange(file.getName(), file);
				fileChange.setEdit(multiEdit);
				fileChange.setTextType("ttcn3");
				composite.add(fileChange);
			}
			for (ReferenceWithContext ref : refs) {
				// update references
				LocationAST node = ref.getReferenceNode();
				addReplaceEditForNode(node, "update reference", multiEdit,
						fileChange);
				pm.worked(1);
			}
		}
	}

	private void addReplaceEditForNode(LocationAST node,
			String textEditGroupName, MultiTextEdit multiEdit,
			TextFileChange fileChange) {
		final String replaceText = ((TTCN3RenameRefactoring) getRefactoring())
				.getRenameText();
		ReplaceEdit replaceEdit = new ReplaceEdit(node.getOffset(), node
				.getEndOffset()
				- node.getOffset(), replaceText);
		multiEdit.addChild(replaceEdit);
		TextEditGroup declarationEditGroup = new TextEditGroup(
				textEditGroupName, replaceEdit);
		fileChange.addTextEditGroup(declarationEditGroup);
	}

	// ---------------------------------------------------------------------------

	@Override
	public Change createChange(IProgressMonitor pm)
			throws OperationCanceledException {
		CompositeChange composite = new CompositeChange("Rename");

		createChanges(composite, pm);

		return composite;
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringParticipant[] loadParticipants(RefactoringStatus status,
			SharableParticipants sharedParticipants) {
		return new RefactoringParticipant[0];
	}

	// ---------------------------------------------------------------------------

	public String getCurrentName() {
		return currentName;
	}
}
