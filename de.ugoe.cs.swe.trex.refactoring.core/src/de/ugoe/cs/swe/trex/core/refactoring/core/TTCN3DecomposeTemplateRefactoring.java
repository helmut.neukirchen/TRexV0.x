package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

public class TTCN3DecomposeTemplateRefactoring extends ProcessorBasedRefactoring {

	RefactoringProcessor processor;
	private List<String> namesOfDecomposedTemplates = new ArrayList<String>();
	
	public TTCN3DecomposeTemplateRefactoring(TTCN3DecomposeTemplateProcessor processor) {
		super(processor);
		this.processor = processor;
	}
	
	// ---------------------------------------------------------------------------
	
	public RefactoringProcessor getProcessor() {
		return processor;
	}
	
	// ---------------------------------------------------------------------------
	
	/*public void setRenameText(String renameText) {
		// TODO Auto-generated method stub
		
	}*/

	public void setNamesOfDecomposedTemplates(List<String> namesOfDecomposedTemplates) {
		this.namesOfDecomposedTemplates = namesOfDecomposedTemplates;
		
	}
	
	public List<String> getNamesOfDecomposedTemplates(){
		return  namesOfDecomposedTemplates;
	}

}
