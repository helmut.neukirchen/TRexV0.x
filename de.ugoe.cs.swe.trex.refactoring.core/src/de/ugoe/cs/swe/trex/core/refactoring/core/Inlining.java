package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.List;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;

class Inlining {
	public List<LocationAST> actualParams;
	public LocationAST ast;
	public List<String> formalParams;
	public Symbol symb;
	public LocationAST templateDefNode;
	
	public Inlining(LocationAST ast) {
		this.ast = ast;
		this.symb = ast.getScope().resolve(ast);
	}
	
	public Inlining(LocationAST ast, Symbol symb) {
		this.ast = ast;
		this.symb = symb;
	}
}
