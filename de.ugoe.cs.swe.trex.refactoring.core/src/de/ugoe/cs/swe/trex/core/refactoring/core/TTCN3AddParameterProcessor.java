package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.TextEditGroup;

import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceFinder;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;


public class TTCN3AddParameterProcessor extends RefactoringProcessor {

	private Map<String, List<ReferenceWithContext>> references;
	
	private Symbol declarationSymbol;
	
	private String paramText;
	
	private String refParamText;
	
	private LocationAST declarationNode;
	
	private Vector<String> presentParamNames = new Vector<String>();
	
	private TTCN3Analyzer analyzer;

	public TTCN3AddParameterProcessor(LocationAST identifierNode) {
		if (identifierNode == null)
			return;
		if (identifierNode.getType() != TTCN3LexerTokenTypes.IDENTIFIER)
			return;
		declarationSymbol = getDeclarationSymbol(identifierNode);
		if (declarationSymbol == null)
			return;
		references = new ReferenceFinder().findReferences(declarationSymbol);
		declarationNode = declarationSymbol.getDeclarationNode();
		if (declarationNode == null)
			return;
	}

	// ---------------------------------------------------------------------------

	@Override
	public Object[] getElements() { // NOPMD by oskar on 11.11.2014 23:56
		return new String[0];
	}

	// ---------------------------------------------------------------------------

	@Override
	public String getIdentifier() {
		return "AddParameter Processor Identifier";
	}

	// ---------------------------------------------------------------------------

	@Override
	public String getProcessorName() {
		return "AddParameter Processor";
	}

	// ---------------------------------------------------------------------------

	@Override
	public boolean isApplicable() {
		return (true);
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm)
			throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		if (	declarationNode != null  &&
				declarationNode.getType() != TTCN3LexerTokenTypes.Identifier  &&
				declarationNode.getParent() != null &&	
				declarationNode.getParent().getParent() != null &&
				declarationNode.getParent().getParent().getType() != TTCN3LexerTokenTypes.FunctionDef &&
				declarationNode.getParent().getParent().getType() != TTCN3LexerTokenTypes.TestcaseDef &&
				declarationNode.getParent().getParent().getType() != TTCN3LexerTokenTypes.AltstepDef){
			status.addFatalError("Select a function!");
		}else{
			LocationAST currAST = declarationNode.getParent();
			while((currAST.getNextSibling() != null) &&
					(currAST.getNextSibling().getType() == TTCN3LexerTokenTypes.FormalPortPar
					|| currAST.getNextSibling().getType() == TTCN3LexerTokenTypes.FormalTemplatePar
					|| currAST.getNextSibling().getType() == TTCN3LexerTokenTypes.FormalTimerPar
					|| currAST.getNextSibling().getType() == TTCN3LexerTokenTypes.FormalTypePar
					|| currAST.getNextSibling().getType() == TTCN3LexerTokenTypes.FormalValuePar)){
				currAST = currAST.getNextSibling();
				presentParamNames.add(currAST.getFirstChild().getNextSibling().getFirstChild().getText());
			}
		}
		return status;
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkFinalConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		if(((TTCN3AddParameterRefactoring) getRefactoring()).getParamCount() <= 0){
			status.addFatalError("No Parameters added!");
		}
		return status;
	}

	// ---------------------------------------------------------------------------

	private void createChanges(CompositeChange composite, IProgressMonitor pm) {
		

		final Set<String> refsKeySet = references.keySet();
		pm.beginTask("Add Parameter Refactoring", refsKeySet.size());
		paramText = ((TTCN3AddParameterRefactoring) getRefactoring()).getParamText();
		refParamText = ((TTCN3AddParameterRefactoring) getRefactoring()).getRefParamText();
		
		String declarationFileName = declarationSymbol.getSurroundingScope()
				.getAssociatedFilename();
		IFile declarationFile = getProjectFile(declarationFileName);
		MultiTextEdit declarationMultiEdit = new MultiTextEdit();
		TextFileChange declarationFileChange = new TextFileChange(
				declarationFile.getName(), declarationFile);
		declarationFileChange.setEdit(declarationMultiEdit);
		declarationFileChange.setTextType("ttcn3");
		composite.add(declarationFileChange);

		// update declaration
		analyzer = TTCN3AnalyzerFlyweightFactory.getInstance().getTTCN3Analyzer(declarationFile);
		InsertEdit insertEdit = new InsertEdit(getInsertOffset(declarationNode, analyzer.getCode()), paramText);
		declarationMultiEdit.addChild(insertEdit);
		TextEditGroup declarationEditGroup = new TextEditGroup(
				"update declaration", insertEdit);
		declarationFileChange.addTextEditGroup(declarationEditGroup);
		pm.worked(1);

		
		for (String filename : refsKeySet) {
			List<ReferenceWithContext> refs = references.get(filename);
			IFile file = getProjectFile(filename);
			MultiTextEdit multiEdit;
			TextFileChange fileChange;
			if (filename.equals(declarationFileName)) {
				multiEdit = declarationMultiEdit;
				fileChange = declarationFileChange;
			} else {
				multiEdit = new MultiTextEdit();
				fileChange = new TextFileChange(file.getName(), file);
				fileChange.setEdit(multiEdit);
				fileChange.setTextType("ttcn3");
				composite.add(fileChange);
			}
			for (ReferenceWithContext ref : refs) {
				// update references
				LocationAST node = ref.getReferenceNode();
				addEditForNode(node, "update reference", multiEdit,
						fileChange);
				pm.worked(1);
			}
		}
	}
	
	private void addEditForNode(LocationAST node, String textEditGroupName, MultiTextEdit multiEdit, TextFileChange fileChange) {
		InsertEdit insertEdit = new InsertEdit(getInsertOffset(node, analyzer.getCode()), refParamText);
		multiEdit.addChild(insertEdit);
		TextEditGroup declarationEditGroup = new TextEditGroup(textEditGroupName, insertEdit);
		fileChange.addTextEditGroup(declarationEditGroup);
	}
	
	// ---------------------------------------------------------------------------

	@Override
	public Change createChange(IProgressMonitor pm)
			throws OperationCanceledException {
		CompositeChange composite = new CompositeChange("T3Doc");

		createChanges(composite, pm);
		return composite;
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringParticipant[] loadParticipants(RefactoringStatus status,
			SharableParticipants sharedParticipants) {
		return new RefactoringParticipant[0];
	}

	
	private IFile getProjectFile(String filename) {
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		IProject project = root.getProject();

		IFile file = null;
		if (project == null) {
			IPath path = ws.getRoot().findMember(filename).getFullPath();
			file = ws.getRoot().getFile(path);
		} else {
			file = project.getFile(filename);
		}

		return file;
	}
	
	private Symbol getDeclarationSymbol(LocationAST identifierNode) {
		Scope scope = identifierNode.getScope();
		if (scope == null)
			return null;
		return scope.resolve(identifierNode);
	}
	
	private int getInsertOffset(LocationAST node, String code){
		boolean comma = false;
		int currentOffset = node.getParent().getEndOffset();
		LocationAST currentNode = node.getParent();
		if(currentNode.getParent().getType() == TTCN3LexerTokenTypes.FunctionDef 
				|| currentNode.getParent().getType() == TTCN3LexerTokenTypes.AltstepDef
				|| currentNode.getParent().getType() == TTCN3LexerTokenTypes.TestcaseDef){
			while((currentNode.getNextSibling() != null) && 
					(currentNode.getNextSibling().getType() == TTCN3LexerTokenTypes.FormalPortPar
					|| currentNode.getNextSibling().getType() == TTCN3LexerTokenTypes.FormalTemplatePar
					|| currentNode.getNextSibling().getType() == TTCN3LexerTokenTypes.FormalTimerPar
					|| currentNode.getNextSibling().getType() == TTCN3LexerTokenTypes.FormalTypePar
					|| currentNode.getNextSibling().getType() == TTCN3LexerTokenTypes.FormalValuePar)){
				comma = true;
				currentNode = currentNode.getNextSibling();				
			}
			if(comma){
				paramText = ", " + paramText;
				return currentNode.getEndOffset();
			} else{
				while(code.charAt(currentOffset) != '('){
					if(code.charAt(currentOffset) == '/' && code.charAt(currentOffset+1) == '/'){
						while(code.charAt(currentOffset) != '\n' && code.charAt(currentOffset) != '\r'){
							currentOffset++;
						}
					}
					
					if(code.charAt(currentOffset) == '/' && code.charAt(currentOffset+1) == '*'){
						while(code.charAt(currentOffset) != '*' || code.charAt(currentOffset+1) != '/'){
							currentOffset++;
						}
					}					
					currentOffset++;
				}
			}
		}else{
			currentNode = currentNode.getParent();
			while(currentNode.getNextSibling() != null && currentNode.getType() != TTCN3LexerTokenTypes.FunctionActualParList){
					currentNode = currentNode.getNextSibling();
			}
			if(currentNode.getType() == TTCN3LexerTokenTypes.FunctionActualParList){
				currentOffset = currentNode.getEndOffset();
				refParamText = ", " + refParamText;
				return currentNode.getEndOffset();
			} else{
				while(code.charAt(currentOffset) != '('){
					if(code.charAt(currentOffset) == '/' && code.charAt(currentOffset+1) == '/'){
						while(code.charAt(currentOffset) != '\n' && code.charAt(currentOffset) != '\r'){
							currentOffset++;
						}
					}
					
					if(code.charAt(currentOffset) == '/' && code.charAt(currentOffset+1) == '*'){
						while(code.charAt(currentOffset) != '*' || code.charAt(currentOffset+1) != '/'){
							currentOffset++;
						}
					}					
					currentOffset++;
				}
			}
		}
		return currentOffset+1;
	}
	
	public Vector<String> getPresentParamNames(){
		return presentParamNames;
	}
}
