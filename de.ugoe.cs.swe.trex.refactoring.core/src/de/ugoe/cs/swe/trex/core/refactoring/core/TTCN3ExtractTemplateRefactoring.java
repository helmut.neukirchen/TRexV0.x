package de.ugoe.cs.swe.trex.core.refactoring.core;

import org.eclipse.ltk.core.refactoring.participants.RenameProcessor;
import org.eclipse.ltk.core.refactoring.participants.RenameRefactoring;

public class TTCN3ExtractTemplateRefactoring extends RenameRefactoring {

	private boolean processEachOccurrence;
	private String newTemplateNodeName;

	
//	TTCN3ExtractTemplateProcessor processor;
	
	public TTCN3ExtractTemplateRefactoring(RenameProcessor processor) {
		super(processor);
		processEachOccurrence = false;
		if(processor instanceof TTCN3ExtractTemplateProcessor) {
			TTCN3ExtractTemplateProcessor extractTemplateProcessor = (TTCN3ExtractTemplateProcessor) processor;
			setNewTemplateNodeName(extractTemplateProcessor.getNewTemplateNodeName());
		}
		
		
		
	}

	public boolean isProcessEachOccurrence() {
		return processEachOccurrence;
	}

	public void setProcessEachOccurrence(boolean processEachOccurrence) {
		this.processEachOccurrence = processEachOccurrence;
	}

	public String getNewTemplateNodeName() {
		return newTemplateNodeName;
	}

	public void setNewTemplateNodeName(String newTemplateNodeName) {
		this.newTemplateNodeName = newTemplateNodeName;
	}



}