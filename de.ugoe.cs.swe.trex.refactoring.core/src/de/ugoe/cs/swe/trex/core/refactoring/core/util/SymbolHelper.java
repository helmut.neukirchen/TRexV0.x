package de.ugoe.cs.swe.trex.core.refactoring.core.util;

import java.security.InvalidParameterException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.ModuleSymbol;

public class SymbolHelper {
	public static IFile getModuleFile(ModuleSymbol moduleSymbol) {
		TTCN3Analyzer analyzer = TTCN3AnalyzerFlyweightFactory.getInstance()
				.getModuleAnalyzerMap().get(moduleSymbol.getName());

		String filename = analyzer.getFilename();

		if (filename == null)
			throw new InvalidParameterException(
					"Can't get filename for modulesymbol '"
							+ moduleSymbol.getSymbolDescriptionPlain() + "'");

		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		IResource member = root.findMember(filename);
		IFile file = (IFile) member;

		if (file == null)
			throw new InvalidParameterException(
					"Can't get IFile for filename '" + filename + "'");

		return file;
	}
}
