package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RenameProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEditGroup;

import antlr.RecognitionException;
import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3FormatterTreeParser;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.TemplateSymbol;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterParameters;

public class TTCN3ExtractTemplateProcessor extends RenameProcessor {

	private LocationAST currentInlineTemplateNode;

	private String newLine;

	private IFile currentIdentifierNodeFile;

	private Scope currentModuleScope;	

	private String newTemplateName;

	private Set<IFile> modifiedFiles = new HashSet<IFile>();

	public TTCN3ExtractTemplateProcessor(LocationAST newSelectedNode) {
		if (newSelectedNode == null) {
			return;
		}

		currentModuleScope = null;
		if (newSelectedNode.getType() == TTCN3LexerTokenTypes.IDENTIFIER) {
			currentModuleScope = newSelectedNode.getScope().getModuleScope();

		} else if (newSelectedNode.getParent().getType() == TTCN3LexerTokenTypes.PredefinedType) {
			LocationAST tmp = TTCN3ExtractUtil.getParentNode(newSelectedNode,
					TTCN3LexerTokenTypes.ModuleDefinitionsPart);
			currentModuleScope = tmp.getScope().getModuleScope();
		} else {
			return;
		}

		currentInlineTemplateNode = TTCN3ExtractUtil.getParentNode(newSelectedNode,
				TTCN3LexerTokenTypes.InLineTemplate);

	}


	@Override
	public RefactoringStatus checkFinalConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();

		return status;
	}

	@Override
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm)
			throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		if (currentInlineTemplateNode == null) {
			status.addFatalError("Please select a valid identifier!");
		}
		if (currentModuleScope == null) {
			status.addFatalError("Please select a valid identifier!");
		}
		if (currentInlineTemplateNode.getType() != TTCN3LexerTokenTypes.InLineTemplate) {
			status.addFatalError("Please select an Inline Template identifier!");
		}
		return status;
	}

	@Override
	public Change createChange(IProgressMonitor pm) throws CoreException,
			OperationCanceledException {
		CompositeChange composite = new CompositeChange("Extract Template");

		createChanges(composite, pm);

		return composite;
	}


	private void createChanges(CompositeChange composite, IProgressMonitor pm)
			throws CoreException {

		// CIN := current selected identifier node
		// getting the new values from the refactoring instance
		TTCN3ExtractTemplateRefactoring refactoring = (TTCN3ExtractTemplateRefactoring) getRefactoring();
		boolean proccessEachOccurrence = refactoring.isProcessEachOccurrence();
		newTemplateName = refactoring.getNewTemplateNodeName();

		// filename of the CIN
		String currentIdentifierNodeFilename;
		if (currentModuleScope != null) {
			currentIdentifierNodeFilename = currentModuleScope
					.getAssociatedFilename();
		} else {
			throw new CoreException(new Status(IStatus.ERROR,
					"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK, null,
					null));
		}

		// iFILE of the CIN
		currentIdentifierNodeFile = TTCN3ExtractUtil.getProjectFile(currentIdentifierNodeFilename);
		modifiedFiles.add(currentIdentifierNodeFile);

		// MultiTextEdit für CIN
		MultiTextEdit currentIdentifierNodeMultiEdit = new MultiTextEdit();

		// TextFileChange für CIN
		TextFileChange currentIdentifierNodeFileChange = new TextFileChange(
				currentIdentifierNodeFile.getName(), currentIdentifierNodeFile);
		currentIdentifierNodeFileChange.setEdit(currentIdentifierNodeMultiEdit);
		currentIdentifierNodeFileChange.setTextType("ttcn3");

		newLine = TTCN3ExtractUtil.getNewLineString(currentIdentifierNodeFileChange.getFile());
		
		// adding the TextFileChange to the composite change
		composite.add(currentIdentifierNodeFileChange);

		LocationAST parentNode = TTCN3ExtractUtil.getParentNode(currentInlineTemplateNode,
				TTCN3LexerTokenTypes.ModuleDefinition);

		// creates new Node
		LocationAST nNode = createNewTemplateDef();

		if (nNode == null) {
			throw new CoreException(new Status(IStatus.ERROR,
					"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK, null,
					null));
		}

		// manually insert a new (temporary and incomplete) template definition
		// into symbol table.
		// the entry will be completed on re-analysis after finishing the
		// refactoring.
		LocationAST behaviorDefinition = LocationAST.resolveParentsUntilType(
				currentInlineTemplateNode, new int[] {
						TTCN3ParserTokenTypes.FunctionDef,
						TTCN3ParserTokenTypes.TestcaseDef,
						TTCN3ParserTokenTypes.AltstepDef });
		Scope moduleDefinitionScope = behaviorDefinition.getScope()
				.getModuleScope();
		TemplateSymbol extractedTemplateDefSymbol = new TemplateSymbol();
		extractedTemplateDefSymbol.setName(newTemplateName);
		extractedTemplateDefSymbol.setSurroundingScope(moduleDefinitionScope);
		extractedTemplateDefSymbol.setScope(new Scope());
		moduleDefinitionScope.getSymbolTable().put(newTemplateName,
				extractedTemplateDefSymbol);

		// process Declaration Node File
		processDeclarationNode(currentIdentifierNodeMultiEdit,
				currentIdentifierNodeFileChange, parentNode, nNode);

		// process all Reference Nodes
		if (proccessEachOccurrence) {

			// get all analyzer and process each of them
			Set<String> keys = TTCN3AnalyzerFlyweightFactory.getInstance()
					.getModuleAnalyzerMap().keySet();
			for (String key : keys) {
				TTCN3Analyzer analyzer = TTCN3AnalyzerFlyweightFactory
						.getInstance().getModuleAnalyzerMap().get(key);
				LocationAST rootNode = analyzer.getRootNode();
				IFile file = analyzer.getFile();

				if(file != null){
					
				
					if (file.getFullPath().equals(
							currentIdentifierNodeFile.getFullPath())) {
						// processes the File of the Declaration Node
						processFile(currentIdentifierNodeMultiEdit,
								currentIdentifierNodeFileChange, rootNode);
					} else {
						// processes none Declaration Node files
						// MultiTextEdit für CIN
						MultiTextEdit thisMultiEdit = new MultiTextEdit();
	
						// TextFileChange für CIN
						TextFileChange thisFileChange = new TextFileChange(file
								.getName(), file);
						thisFileChange.setEdit(thisMultiEdit);
						thisFileChange.setTextType("ttcn3");
	
						// adding the TextFileChange to the composite change
						composite.add(thisFileChange);
	
						processFile(thisMultiEdit, thisFileChange, rootNode);
	
					}
	
				}
			}

		}

		// remove temporary symbols from symbol table
		moduleDefinitionScope.getSymbolTable().remove(newTemplateName);
		pm.worked(1);

	}

	private void processDeclarationNode(
			MultiTextEdit currentIdentifierNodeMultiEdit,
			TextFileChange currentIdentifierNodeFileChange,
			LocationAST parentNode, LocationAST nNode) throws CoreException {
		String space = TTCN3ExtractUtil.getSpaceBevorNode(currentIdentifierNodeFileChange
				.getFile(), parentNode);

		// converts nNode to a String
		StringBuffer newTemplateDef = new StringBuffer();
		String formatTemplateNode = formatTemplateNode(nNode);
		formatTemplateNode = TTCN3ExtractUtil.addSpaceToEachNewLine(space, formatTemplateNode);
		newTemplateDef.append(formatTemplateNode);

		newTemplateDef.append(newLine + newLine + space);

		InsertEdit insertEdit = new InsertEdit(parentNode.getOffset(),
				newTemplateDef.toString());
		currentIdentifierNodeMultiEdit.addChild(insertEdit);

		// description for the wizard
		TextEditGroup declarationEditGroup = new TextEditGroup(
				"add new Template Definition", insertEdit);
		currentIdentifierNodeFileChange.addTextEditGroup(declarationEditGroup);

		ReplaceEdit replaceEdit = new ReplaceEdit(currentInlineTemplateNode
				.getOffset(), currentInlineTemplateNode.getEndOffset()
				- currentInlineTemplateNode.getOffset(), newTemplateName);
		currentIdentifierNodeMultiEdit.addChild(replaceEdit);
		TextEditGroup tEditGroupCIN = new TextEditGroup(
				"update the selected Inline Template", replaceEdit);
		currentIdentifierNodeFileChange.addTextEditGroup(tEditGroupCIN);
	}


	private void processFile(MultiTextEdit currentIdentifierNodeMultiEdit,
			TextFileChange currentIdentifierNodeFileChange, LocationAST rootNode)
			throws CoreException {
		ReplaceEdit replaceEdit;

		// search all Inline Templates
		ArrayList<LocationAST> inlineTemplateNodeList = new ArrayList<LocationAST>();
		getAllInlineTemplates(rootNode, inlineTemplateNodeList);
		LocationAST[] inlineTemplateNodeArray = inlineTemplateNodeList
				.toArray(new LocationAST[inlineTemplateNodeList.size()]);

		// compare Inline Templates with the current selected Inline Template
		// after this sameInlineTemplates holds the Structure of all similar
		// Inline Templates
		ArrayList<LocationAST> sameInlineTemplates = new ArrayList<LocationAST>();
		for (LocationAST tmp : inlineTemplateNodeArray) {
			if (inlineTemplatesEqual(tmp, currentInlineTemplateNode)) {
				sameInlineTemplates.add(tmp);
			}
		}
		boolean insertedImport = false;
		for (LocationAST tmp : sameInlineTemplates) {
			if (currentIdentifierNodeFile.getFullPath().equals(
					currentIdentifierNodeFileChange.getFile().getFullPath())
					&& currentInlineTemplateNode.getOffset() == tmp.getOffset()) {
				continue;
			}

			replaceEdit = new ReplaceEdit(tmp.getOffset(), tmp.getEndOffset()
					- tmp.getOffset(), newTemplateName);
			currentIdentifierNodeMultiEdit.addChild(replaceEdit);
			TextEditGroup declarationEditGroup2 = new TextEditGroup(
					"update declaration", replaceEdit);
			currentIdentifierNodeFileChange
					.addTextEditGroup(declarationEditGroup2);

			boolean addImportStatement = isImportMissing(tmp);
			handleImport(currentIdentifierNodeMultiEdit, currentIdentifierNodeFileChange, insertedImport, tmp, addImportStatement);
		}

	}


	private void handleImport(MultiTextEdit currentIdentifierNodeMultiEdit, TextFileChange currentIdentifierNodeFileChange, boolean insertedImport, LocationAST tmp, boolean addImportStatement) throws CoreException {
		if (addImportStatement && !insertedImport) {
			insertedImport = true;
			LocationAST moduleDefinitionListNode = TTCN3ExtractUtil.getParentNode(tmp,
					TTCN3LexerTokenTypes.ModuleDefinitionList);
			String space = TTCN3ExtractUtil.getSpaceBevorNode(
					currentIdentifierNodeFileChange.getFile(),
					moduleDefinitionListNode.getFirstChild());

			LocationAST newImportNode;
			newImportNode = createNewImportNode(currentModuleScope
					.getScopeSymbol().getName(), newLine, space);
			String formatedNewImportNode = TTCN3ExtractUtil.formatImportDefNode(newImportNode);
			formatedNewImportNode = TTCN3ExtractUtil.addSpaceToEachNewLine(space,
					formatedNewImportNode);
			newImportNode.getText();
			String importStatement = new String(formatedNewImportNode
					+ newLine + newLine + space);
			InsertEdit insertEdit = new InsertEdit(moduleDefinitionListNode
					.getOffset(), importStatement);
			currentIdentifierNodeMultiEdit.addChild(insertEdit);
			TextEditGroup declarationEditGroup3 = new TextEditGroup(
					"create new import statement", insertEdit);
			currentIdentifierNodeFileChange
					.addTextEditGroup(declarationEditGroup3);

		}
	}

	private LocationAST createNewImportNode(String moduleName, String newLine,
			String space) {

		// importFromSpec
		LocationAST identifierFirstNode; // NOPMD by oskar on 12.11.2014 00:49
		identifierFirstNode = new LocationAST();
		identifierFirstNode.setType(TTCN3LexerTokenTypes.IDENTIFIER);
		identifierFirstNode.setText(moduleName);

		LocationAST identifierNode;
		identifierNode = new LocationAST();
		identifierNode.setType(TTCN3LexerTokenTypes.Identifier);
		identifierNode.setFirstChild(identifierFirstNode);

		LocationAST globalModuleIdNode;
		globalModuleIdNode = new LocationAST();
		globalModuleIdNode.setType(TTCN3LexerTokenTypes.GlobalModuleId);
		globalModuleIdNode.setFirstChild(identifierNode);

		LocationAST moduleIdNode;
		moduleIdNode = new LocationAST();
		moduleIdNode.setType(TTCN3LexerTokenTypes.ModuleId);
		moduleIdNode.setFirstChild(globalModuleIdNode);

		LocationAST importFromSpecNode;
		importFromSpecNode = new LocationAST();
		importFromSpecNode.setType(TTCN3LexerTokenTypes.ImportFromSpec);
		importFromSpecNode.setFirstChild(moduleIdNode);

		// importSpec

		LocationAST isIDENTIFIERNode;
		isIDENTIFIERNode = new LocationAST();
		isIDENTIFIERNode.setType(TTCN3LexerTokenTypes.IDENTIFIER);
		isIDENTIFIERNode.setText(newTemplateName);

		LocationAST isIdentifierNode;
		isIdentifierNode = new LocationAST();
		isIdentifierNode.setType(TTCN3LexerTokenTypes.Identifier);
		isIdentifierNode.setFirstChild(isIDENTIFIERNode);

		LocationAST isTemplateRefListNode;
		isTemplateRefListNode = new LocationAST();
		isTemplateRefListNode.setType(TTCN3LexerTokenTypes.TemplateRefList);
		isTemplateRefListNode.setFirstChild(isIdentifierNode);

		LocationAST isImportTemplateSpecNode;
		isImportTemplateSpecNode = new LocationAST();
		isImportTemplateSpecNode.setType(TTCN3LexerTokenTypes.ImportTemplateSpec);
		isImportTemplateSpecNode.setFirstChild(isTemplateRefListNode);

		LocationAST isImportElementNode;
		isImportElementNode = new LocationAST();
		isImportElementNode.setType(TTCN3LexerTokenTypes.ImportElement);
		isImportElementNode.setFirstChild(isImportTemplateSpecNode);

		LocationAST isImportSpecNode;
		isImportSpecNode = new LocationAST();
		isImportSpecNode.setType(TTCN3LexerTokenTypes.ImportSpec);
		isImportSpecNode.setFirstChild(isImportElementNode);

		// combine both trees to ImportDef

		importFromSpecNode.setNextSibling(isImportSpecNode);

		LocationAST newImportDefNode;
		newImportDefNode = new LocationAST();
		newImportDefNode.setType(TTCN3LexerTokenTypes.ImportDef);
		newImportDefNode.setFirstChild(importFromSpecNode);

		return newImportDefNode;
	}

	private boolean isImportMissing(LocationAST inlineNode) {
		LocationAST identifierNode = (LocationAST) ASTUtil.findChild(
				inlineNode, TTCN3ParserTokenTypes.IDENTIFIER);
		// resolving by string is sufficient in this case!
		Symbol resolvedTemplateSymbol = identifierNode.getScope().resolve(
				newTemplateName);
		if (resolvedTemplateSymbol == null)
			return true;

		return false;
	}

	private String formatTemplateNode(LocationAST nNode) throws CoreException {
		TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
		formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
		formatterWalker.setFormatterParameters(new TTCN3FormatterParameters());
		String newTemplateDef = null;
		try {
			newTemplateDef = formatterWalker.pr_TemplateDef(nNode).toString();
		} catch (RecognitionException e) {
			throw new CoreException(
					new Status(IStatus.ERROR,
							"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK,
							null, e));
		}
		return newTemplateDef;
	}



	private boolean inlineTemplatesEqual(LocationAST inlineTemplate1,
			LocationAST inlineTemplate2) {

		if (inlineTemplate1.getType() != TTCN3LexerTokenTypes.InLineTemplate
				|| inlineTemplate2.getType() != TTCN3LexerTokenTypes.InLineTemplate) {
			return false;
		}

		return rekursivCompareNodes(inlineTemplate1, inlineTemplate2);
	}

	private boolean rekursivCompareNodes(LocationAST node1, LocationAST node2) {
		if (node1 == null && node2 == null) {
			return true;
		}
		boolean toReturn = false;
		if (node1.getType() == node2.getType()) {
			if (node1.getType() == TTCN3LexerTokenTypes.IDENTIFIER
					&& !node1.getText().equals(node2.getText())) {
				return false;
			}
			toReturn = rekursivCompareNodes(node1.getFirstChild(), node2
					.getFirstChild());
			if (toReturn) {
				toReturn = rekursivCompareNodes(node1.getNextSibling(), node2
						.getNextSibling());
			}

		}
		return toReturn;
	}

	private LocationAST createNewTemplateDef() throws CoreException {
		LocationAST templNode = null;
		LocationAST templTypeNode = null;
		LocationAST templDerivedNode = null;
		LocationAST templBodyNode = null;
		LocationAST tmp;
		try {
			templNode = ASTUtil.deepCloneAST(currentInlineTemplateNode);
		} catch (Exception e) {
			throw new CoreException(
					new Status(IStatus.ERROR,
							"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK,
							null, e));
		}
		tmp = templNode.getFirstChild();
		while (tmp != null) {
			if (tmp.getType() == TTCN3LexerTokenTypes.DerivedRefWithParList) {
				templDerivedNode = tmp;
			} else if (tmp.getType() == TTCN3LexerTokenTypes.TemplateBody) {
				templBodyNode = tmp;
			} else if (tmp.getType() == TTCN3LexerTokenTypes.Type) {
				templTypeNode = tmp;
			} else {
				throw new CoreException(
						new Status(IStatus.ERROR,
								"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK,
								null, null));
			}
			tmp = tmp.getNextSibling();
		}


		// create DerivedDef Node
		LocationAST nDerivedDefNode = null;
		LocationAST derivedIDENTIFIERNode = null;
		if (templDerivedNode != null) {

			
			derivedIDENTIFIERNode = (LocationAST) ASTUtil.findChild(templDerivedNode, TTCN3LexerTokenTypes.IDENTIFIER);
			String text = derivedIDENTIFIERNode.getText();
			
			LocationAST nDerDefIDENTIFIERNode = new LocationAST();
			nDerDefIDENTIFIERNode.setType(TTCN3LexerTokenTypes.IDENTIFIER);			
			nDerDefIDENTIFIERNode.setText(text);

			LocationAST nDerDefIdentifierNode = new LocationAST();
			nDerDefIdentifierNode.setType(TTCN3LexerTokenTypes.Identifier);
			nDerDefIdentifierNode.setFirstChild(nDerDefIDENTIFIERNode);

			nDerivedDefNode = new LocationAST();
			nDerivedDefNode.setType(TTCN3LexerTokenTypes.DerivedDef);
			nDerivedDefNode.setFirstChild(nDerDefIdentifierNode);
		}
		
		// create BaseTemplate Node

		LocationAST nIDENTIFIERNode = new LocationAST();
		nIDENTIFIERNode.setType(TTCN3LexerTokenTypes.IDENTIFIER);
		nIDENTIFIERNode.setText(newTemplateName);

		LocationAST nIdentifierNode = new LocationAST();
		nIdentifierNode.setType(TTCN3LexerTokenTypes.Identifier);
		nIdentifierNode.setFirstChild(nIDENTIFIERNode);

		if (templTypeNode == null) {
			
			LocationAST derivedBaseTemplateNode = null;
			if( derivedIDENTIFIERNode != null) {
				Symbol derivedSymbol = derivedIDENTIFIERNode.getScope().resolve(derivedIDENTIFIERNode);
				// TODO this resolve should not use string, but there is no scope 
				if(derivedSymbol == null) {
					throw new CoreException(
							new Status(IStatus.ERROR,
									"de.ugoe.cs.swe.trex.refactoring.core", IStatus.OK,
									null, null));
				}
				LocationAST derivedDeclarationNode = derivedSymbol.getDeclarationNode();
				derivedBaseTemplateNode = TTCN3ExtractUtil.getParentNode(derivedDeclarationNode,
						TTCN3LexerTokenTypes.BaseTemplate);

				
			}
			templTypeNode = derivedBaseTemplateNode.getFirstChild();
			
		}
		
		
		templTypeNode.setNextSibling(nIdentifierNode);

		LocationAST nBaseTemplateNode = new LocationAST();
		nBaseTemplateNode.setType(TTCN3LexerTokenTypes.BaseTemplate);
		nBaseTemplateNode.setFirstChild(templTypeNode);


		// create TemplateBody Node

		// create new TemplateDef Node
		LocationAST nNode = new LocationAST();
		nNode.setType(TTCN3LexerTokenTypes.TemplateDef);

		// combine created Nodes
		LocationAST tmpSiblingNode;
		nNode.setFirstChild(nBaseTemplateNode);
		tmpSiblingNode = nBaseTemplateNode;

		if (nDerivedDefNode != null) {
			tmpSiblingNode.setNextSibling(nDerivedDefNode);
			tmpSiblingNode = nDerivedDefNode;
		}
		
		tmpSiblingNode.setNextSibling(templBodyNode);

		return nNode;
	}

	public String getNewTemplateNodeName() {

		AST inlineTemplateIdentifier = ASTUtil.findChild(
				currentInlineTemplateNode, TTCN3LexerTokenTypes.IDENTIFIER);

		if (inlineTemplateIdentifier != null) {
			return "n_" + inlineTemplateIdentifier.getText() + "Template";
		} else {
			return "n_Template";
		}
	}

	private void getAllInlineTemplates(LocationAST node,
			ArrayList<LocationAST> inlineTemplateNodes) {

		if (node != null) {
			if (node.getType() == TTCN3LexerTokenTypes.InLineTemplate) {
				inlineTemplateNodes.add(node);
			}
			getAllInlineTemplates(node.getFirstChild(), inlineTemplateNodes);
			getAllInlineTemplates(node.getNextSibling(), inlineTemplateNodes);
		}

	}



	@Override
	public Object[] getElements() { // NOPMD by oskar on 12.11.2014 00:51
		return new String[0];
	}

	@Override
	public String getIdentifier() {
		return "Extract Template Processor Identifier";
	}

	@Override
	public String getProcessorName() {
		return "Extract Template Processor";
	}

	@Override
	public boolean isApplicable() throws CoreException {
		return (currentModuleScope != null && currentInlineTemplateNode != null);
	}

	@Override
	public RefactoringParticipant[] loadParticipants(RefactoringStatus status,
			SharableParticipants sharedParticipants) throws CoreException {
		return new RefactoringParticipant[0];
	}

	public Set<IFile> getModifiedFiles() {
		return modifiedFiles;
	}

}