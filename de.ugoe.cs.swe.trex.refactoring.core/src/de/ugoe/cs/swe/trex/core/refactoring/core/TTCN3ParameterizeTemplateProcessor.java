package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.NullChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.text.edits.DeleteEdit;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEditGroup;

import antlr.RecognitionException;
import antlr.collections.AST;

import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceFinder;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3FormatterTreeParser;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.ModuleSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterParameters;
import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterTools;
import de.ugoe.cs.swe.trex.core.refactoring.core.util.SymbolHelper;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;

public class TTCN3ParameterizeTemplateProcessor extends RefactoringProcessor{

	
	 class TemplateAndParameters{
	 	private LocationAST templateNode;
		private List<String> parameters = new ArrayList<String>();
		

		public TemplateAndParameters( LocationAST template, List<String> paramList) {
			templateNode = template;
			parameters = paramList;
			
		}
		
		public LocationAST getTemplateNode() {
			return templateNode;
		}
		
		public List<String> getParametersList() {
			return parameters;
		}
		
		public void addParameter(String param){
			parameters.add(param);
		}

	 }
	  	 
	Symbol declarationSymbol;
	LocationAST templateDefAST ;
	LocationAST recordNode ;
	List<AST> templateFields = new ArrayList<AST>();
	List<TemplateAndParameters> templatesAndParameters = new ArrayList<TemplateAndParameters>();
	Map<String, String> recordFields = new HashMap<String, String>();
	private Set<IFile> modifiedFiles = new HashSet<IFile>();
		
	private String parameterizeTemplateText;
	private List<LocationAST> similarTemplate;
	private List<String> differingFields;
	
	
	// ---------------------------------------------------------------------------
	
	public TTCN3ParameterizeTemplateProcessor(TTCN3Editor editor) {
		final LocationAST editorAST = TTCN3Editor.getEditorAST(editor);
		if(editorAST != null)
			initialize(editorAST);
	}

	// ---------------------------------------------------------------------------
	
	public TTCN3ParameterizeTemplateProcessor(LocationAST ast) {
		initialize(ast);
	}

	// ---------------------------------------------------------------------------
	
	private void initialize(LocationAST ast) {
		if (ast == null)
			return;
		
		declarationSymbol = getDeclarationSymbol(ast);
		templateDefAST = LocationAST.resolveParentsUntilType(ast,
				TTCN3ParserTokenTypes.TemplateDef);
		similarTemplate = new ArrayList<LocationAST>();
		differingFields = new ArrayList<String>();
		

		if (templateDefAST == null)
			return;
		if( ASTUtil.getDescendantNode(templateDefAST).getType() != TTCN3ParserTokenTypes.IDENTIFIER) 
			return;
		if(ASTUtil.getTypeSymbol(templateDefAST).getStructuredType()!= null){
					
			if(isReferencedByAnotherTemplateUsingModifies(templateDefAST))
				return;
			
			parameterizeTemplateText = "parameterized_" + ASTUtil.getIdentNode( templateDefAST).getText();
			templateFields = ASTUtil.getFields(templateDefAST);
			
			LocationAST templateType = ASTUtil.getDescendantNode(templateDefAST);
			Scope recordScope =	templateType.getScope();
			Symbol recordSymbol = recordScope.resolve(templateType);
			recordNode = recordSymbol.getDeclarationNode();
			getRecordFields(recordNode);
			findAnotherTemplatesInTheSameScope(recordNode);
			getDifferingFields(templateDefAST);
			
			
		}
		
	}

	//	---------------------------------------------------------------------------

	private boolean isReferencedByAnotherTemplateUsingModifies(LocationAST templateDefAST) {
		Map<String, List<ReferenceWithContext>> references;
		LocationAST identifierNode = ASTUtil.getIdentNode(templateDefAST);
		Symbol symbol = getDeclarationSymbol(identifierNode);
				
		references = new ReferenceFinder().findReferences(symbol);
		final Set<String> refsKeySet = references.keySet();
		
		for (String filename : refsKeySet) {
			List<ReferenceWithContext> refs = references.get(filename);
							
			for(ReferenceWithContext ref : refs) {
				LocationAST nod = ref.getReferenceNode();
				LocationAST node = LocationAST.resolveParentsUntilType(nod,
						TTCN3LexerTokenTypes.DerivedDef);
				if(node != null){
					return true;
				}
				
			}
		}
		
		return false;
	}
	
	//	---------------------------------------------------------------------------

	private void getRecordFields(LocationAST recordNode2) {
		if(recordNode!= null ){
			LocationAST field = recordNode.getParent().getNextSibling();
			
			while(field!= null){
				String paramName = ASTUtil.getIdentNode(field.getFirstChild().getNextSibling()).getText();
				String paramType = ASTUtil.getDescendantNode(field).getText();
				
				recordFields.put(paramName, paramType);
				field = field.getNextSibling();
			}
		}
		
	}

	//	---------------------------------------------------------------------------

	private void findAnotherTemplatesInTheSameScope(LocationAST recordNodeIdent) {
		Map<String, List<ReferenceWithContext>> tempReferences;
		LocationAST identifierNode = ASTUtil.getIdentNode(recordNodeIdent);
		Symbol symbol = getDeclarationSymbol(identifierNode);
		
		String declarationFileName = declarationSymbol.getSurroundingScope()
		.getAssociatedFilename();
		
		tempReferences = new ReferenceFinder().findReferences(symbol);
		final Set<String> refsKeySet = tempReferences.keySet();
		
		for (String filename : refsKeySet) {
			List<ReferenceWithContext> refs = tempReferences.get(filename);
			if (filename.equals(declarationFileName)){ 
			
				for (ReferenceWithContext ref : refs) {
					// update references
					LocationAST nod = ref.getReferenceNode();
					LocationAST node = LocationAST.resolveParentsUntilType(nod,
							TTCN3ParserTokenTypes.TemplateDef);
					
					if(node != null)
						if(node != templateDefAST){
							Symbol s = getDeclarationSymbol(ASTUtil.getIdentNode(node));
							if(declarationSymbol.getSurroundingScope().equals(s.getSurroundingScope())){
								if(!isReferencedByAnotherTemplateUsingModifies(node)){
									similarTemplate.add(node);
//									
								}
							}
						}
				}
			}
		}
	}
	
	//	---------------------------------------------------------------------------

	private void getDifferingFields(LocationAST templateDefAST2) {
		String param1, param2;
		for(int i=0; i<similarTemplate.size() ; i++){
			List<AST> tempDifferingFields =  ASTUtil.getDifferingFields( templateDefAST2, similarTemplate.get(i));
			for(int j=0;j<tempDifferingFields.size();j++){
				
				param2 = tempDifferingFields.get(j).getText();
				param1 = recordFields.get(param2);
				if(param1!= null){
					if(!differingFields.contains((LocationAST) tempDifferingFields.get(j)))
						differingFields.add(tempDifferingFields.get(j).getText());
					
				}
				
			}
		}
		

		
	}


	// ---------------------------------------------------------------------------

	private IFile getProjectFile(String filename) {
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		IProject project = root.getProject();

		IFile file = null;
		if (project == null) {
			IPath path = ws.getRoot().findMember(filename).getFullPath();
			file = ws.getRoot().getFile(path);
		} else {
			file = project.getFile(filename);
		}

		return file;
	}

	//	---------------------------------------------------------------------------

		private Symbol getDeclarationSymbol(LocationAST identifierNode) {
			Scope scope = identifierNode.getScope();
			if (scope == null)
				return null;
			return scope.resolve(identifierNode);
		}

	// ---------------------------------------------------------------------------
	
	@Override
	public RefactoringStatus checkFinalConditions(IProgressMonitor pm, CheckConditionsContext context) throws CoreException, OperationCanceledException {
		
		RefactoringStatus status = new RefactoringStatus();
		TTCN3ParameterizeTemplateRefactoring refactoring = (TTCN3ParameterizeTemplateRefactoring) getRefactoring();

		if (refactoring.getRenameText().length() <= 0)
			status.addFatalError("The template name may not be empty!");

		return status;
		
	}

	// ---------------------------------------------------------------------------
	
	@Override
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm) throws CoreException, OperationCanceledException {
		if (templateDefAST == null || templateDefAST.getType() != TTCN3ParserTokenTypes.TemplateDef) {
			return RefactoringStatus
			.createFatalErrorStatus("You _must_ select the identifier "
					+ "of a template declaration to perform the parameterize refactoring.");
		}
		if(isReferencedByAnotherTemplateUsingModifies(templateDefAST)){
			return RefactoringStatus
			.createFatalErrorStatus("This template is referenced by another template using"+'"'+"modifies"+'"');
		}
		
		if(recordNode == null ){
			return RefactoringStatus
			.createFatalErrorStatus("This template is simple can not be parameterized");
		}
		
		if(ASTUtil.isParameterizedTemplate(templateDefAST)){
			return RefactoringStatus
			.createFatalErrorStatus("This template is parameterized template ");
		}
		
		if(differingFields.size() == 0 && similarTemplate.size() != 0){
			return RefactoringStatus
			.createFatalErrorStatus("This template is duplicated ");
		}
		
		if(differingFields.size() == 0 && similarTemplate.size() == 0){
			return RefactoringStatus
			.createFatalErrorStatus("No similar templates are found ");
		}
		
		AST child = templateDefAST.getFirstChild();
		while (child != null) {
			if (child.getType() == TTCN3LexerTokenTypes.DerivedDef) {
				return RefactoringStatus
				.createFatalErrorStatus("This template is modified template ");
			}
			child = child.getNextSibling();
		}
 
		return new RefactoringStatus();
		
	}

	// ---------------------------------------------------------------------------
	
	@Override
	public Change createChange(IProgressMonitor pm) throws CoreException, OperationCanceledException {
		CompositeChange change = parameterizeTemplateChange(pm);
		return change == null ? new NullChange() : change;
	}

	// ---------------------------------------------------------------------------
	
	private CompositeChange parameterizeTemplateChange(IProgressMonitor pm) {
		CompositeChange compositeChange = new CompositeChange("Parameterize Template");
		
		String declarationFileName1 = getDeclarationSymbol(recordNode).getSurroundingScope()
		.getAssociatedFilename();
		IFile declarationFile1 = getProjectFile(declarationFileName1);
		modifiedFiles.add(declarationFile1);
		
		for (int i = 0; i < similarTemplate.size(); i++) {
			LocationAST a = similarTemplate.get(i);
			IFile file = getFileForAST(a);
			modifiedFiles.add(file);
		}
		
		String declarationFileName = declarationSymbol.getSurroundingScope()
		.getAssociatedFilename();
		IFile declarationFile = getProjectFile(declarationFileName);
		modifiedFiles.add(declarationFile);
		
		MultiTextEdit declarationMultiEdit = new MultiTextEdit();
		TextFileChange declarationFileChange = new TextFileChange(
				declarationFile.getName(), declarationFile);
		declarationFileChange.setEdit(declarationMultiEdit);
		declarationFileChange.setTextType("ttcn3");
		compositeChange.add(declarationFileChange);
		
		TTCN3FormatterTreeParser formatterWalker = new TTCN3FormatterTreeParser();
		formatterWalker.getASTFactory().setASTNodeClass(LocationAST.class);
		formatterWalker
				.setFormatterParameters(new TTCN3FormatterParameters());
		
		TTCN3FormatterParameters fparam =new TTCN3FormatterParameters();
		TTCN3FormatterTools line = new TTCN3FormatterTools(fparam);
		
		
		parameterizeTemplateText = ((TTCN3ParameterizeTemplateRefactoring) getRefactoring())
											.getRenameText();
		
		String parametrisedTemplateFirstline = "template " + recordNode.getText()
					+ " " + parameterizeTemplateText+" (" ;
		for(int i = 0; i< differingFields.size() ;i++){
			if(i > 0)
				parametrisedTemplateFirstline += ", ";
			parametrisedTemplateFirstline += recordFields.get(differingFields.get(i))
						+ " p_" +differingFields.get(i);
		}
		parametrisedTemplateFirstline +=" ) := {";
		
		String parametrisedTemplateBody = "";
		String  param2;
		LocationAST fieldBody;
		int count=0;
		ArrayList<String> sourceTempParam = new ArrayList<String>();
		
		for(int k = 0; k < templateFields .size(); k++){
			param2 = ASTUtil.getDescendantNode(templateFields.get(k)).getText();
			if(recordFields.get(param2)!= null){
				// field equal parameter
				if(count > 0)
					parametrisedTemplateBody += ",";
				
				if(differingFields.contains(param2)){
					fieldBody = ASTUtil.findTemplateBodyFromTemplateDef((LocationAST) templateFields.get(k));
					try {
						sourceTempParam.add(formatterWalker.pr_TemplateBody(fieldBody).toString());
						
					} catch (RecognitionException e) {
						e.printStackTrace();
					}
					parametrisedTemplateBody += line.getNewline()+ "			"+param2 + " := p_"+param2;
					count++;
				
				}
				 //copy fieldBody
				else{
					fieldBody = ASTUtil.findTemplateBodyFromTemplateDef((LocationAST) templateFields.get(k));
					try {
						parametrisedTemplateBody +=  line.getNewline()+"			"+param2 + " := "+ 
												formatterWalker.pr_TemplateBody(fieldBody).toString();
					} catch (RecognitionException e) {
						e.printStackTrace();
					}
					count++;
				}
			}
		}
		templatesAndParameters.add(new TemplateAndParameters(templateDefAST,sourceTempParam));
		
		parametrisedTemplateBody +=  line.getNewline()+"	}"+line.getNewline();
			
		//	end of creation ParametrisedTargetTemplate	
		
		LocationAST tempBody;
		String temp;
		ArrayList<String> tempParam = new ArrayList<String>();
		 for(int i= 0; i < similarTemplate.size(); i++){
			List<AST> tempFields = ASTUtil.getFields( similarTemplate.get(i));
			for(int j= 0; j < tempFields.size();j++){
				temp= ASTUtil.getDescendantNode(tempFields.get(j)).getText();
				if(differingFields.contains(temp)){
					tempBody = ASTUtil.findTemplateBodyFromTemplateDef((LocationAST) tempFields.get(j));
					try {
						tempParam.add(formatterWalker.pr_TemplateBody(tempBody).toString());
						
					} catch (RecognitionException e) {
						e.printStackTrace();
					}
				}
			}
			templatesAndParameters.add(new TemplateAndParameters(similarTemplate.get(i),tempParam));
		 }
			
		 
			
		 for(int m = 0; m < templatesAndParameters.size() ; m++){
			 
			 TemplateAndParameters x = templatesAndParameters.get(m);
			 LocationAST tempNode = x.getTemplateNode();
			 String parameters = "( ";
			 List <String> params = x.getParametersList();
			 for(int n= 0; n < params.size(); n++){
				 if(n > 0)
					 parameters +=" , ";
				 parameters += params.get(n);
			 }
			 parameters +=" )";
			 
			 if(!isPreserveSourceTemplates()){
				//	modify references and delete source Templates 
				
				LocationAST identifierNode = ASTUtil.getIdentNode(tempNode);
				Symbol symbol = getDeclarationSymbol(identifierNode);
				
				Map<String, List<ReferenceWithContext>> tempReferences 
									= new ReferenceFinder().findReferences(symbol);
				final Set<String> refsKeySet = tempReferences.keySet();
				
				for (String filename : refsKeySet) {
					List<ReferenceWithContext> refs = tempReferences.get(filename);
					for (ReferenceWithContext ref : refs) {
						// modify reference
						LocationAST node = ref.getReferenceNode();
						//System.out.println("reference modification: "+node.getText());
						addReplaceEditForField(node, parameterizeTemplateText+parameters , 
								"Modify reference of "+ASTUtil.getIdentNode(tempNode).getText()+" template with parametrised target template", 
								declarationMultiEdit, declarationFileChange);
						
						
					}
				}
			 
			}
			else{
				
				//	modify Source Templates
				LocationAST templateBody = ASTUtil.findTemplateBodyFromTemplateDef(tempNode);
				addReplaceEditForField(templateBody, parameterizeTemplateText+parameters , 
						"Modify source template with parametrised target template", 
						declarationMultiEdit, declarationFileChange);
				
			}
		 }
		//	delete source Template
		 if(!isPreserveSourceTemplates()){
			 for(int l= 0; l < templatesAndParameters.size() ; l++){
				 TemplateAndParameters z = templatesAndParameters.get(l);
				 LocationAST nod = z.getTemplateNode();
				 deleteEditForTemplate(nod, "Delete (source Template) " 
							+ ASTUtil.getIdentNode(nod).getText(), declarationMultiEdit, declarationFileChange);
			 }
				 
		 }
		 addInsertEditForNewTemplate(templateDefAST, 
					parametrisedTemplateFirstline + parametrisedTemplateBody, 
					"Insert parametrised target template", declarationMultiEdit, declarationFileChange);
		
		return compositeChange;
	}

	// ---------------------------------------------------------------------------
		
	private boolean isPreserveSourceTemplates() {
		
		return ((TTCN3ParameterizeTemplateRefactoring) getRefactoring())
					.isPreserveSourceTemplates();
		
	}

	// ---------------------------------------------------------------------------
	
	private IFile getFileForAST(LocationAST ast) {
		ModuleSymbol moduleSymbol = ASTUtil.getModuleSymbol(ast);
		IFile moduleFile = SymbolHelper.getModuleFile(moduleSymbol);

		return moduleFile;
	}

	// ---------------------------------------------------------------------------
	
	private void addInsertEditForNewTemplate(LocationAST node,String isertText,
			String textEditGroupName, MultiTextEdit multiEdit,
			TextFileChange fileChange) {
		InsertEdit insertEdit = new InsertEdit(node.getOffset(), isertText);
		multiEdit.addChild(insertEdit);
		TextEditGroup declarationEditGroup = new TextEditGroup(
				textEditGroupName, insertEdit);
		fileChange.addTextEditGroup(declarationEditGroup);
	}
	
	// ---------------------------------------------------------------------------
	
	private void addReplaceEditForField(LocationAST node, String replaceText,
			String textEditGroupName, MultiTextEdit multiEdit,
			TextFileChange fileChange) {
		ReplaceEdit replaceEdit = new ReplaceEdit(node.getOffset(), node
				.getEndOffset()
				- node.getOffset(), replaceText);
		multiEdit.addChild(replaceEdit);
		TextEditGroup declarationEditGroup = new TextEditGroup(
				textEditGroupName, replaceEdit);
		fileChange.addTextEditGroup(declarationEditGroup);
	}

	// ---------------------------------------------------------------------------
	
	private void deleteEditForTemplate(LocationAST node,String textEditGroupName, 
			MultiTextEdit multiEdit, TextFileChange fileChange) {
		DeleteEdit deleteEdit = new DeleteEdit(node.getOffset(), node.getEndOffset()-node.getOffset());
		multiEdit.addChild(deleteEdit);
		TextEditGroup declarationEditGroup = new TextEditGroup(
				textEditGroupName, deleteEdit);
		fileChange.addTextEditGroup(declarationEditGroup);
	}
	
	
	// ---------------------------------------------------------------------------
	
	@Override
	public Object[] getElements() {
		// TODO Auto-generated method stub
		return new String[0];
	}

	// ---------------------------------------------------------------------------
	
	@Override
	public String getIdentifier() {
		return "Parameterize Template Processor Identifier";
	}

	// ---------------------------------------------------------------------------
	
	@Override
	public String getProcessorName() {
		return "Parameterize Template Processor";
	}

	//	---------------------------------------------------------------------------
	
	public Set<IFile> getModifiedFiles() {
		return modifiedFiles;
	}
	
	// ---------------------------------------------------------------------------
	
	@Override
	public boolean isApplicable() throws CoreException {
		return (templateDefAST != null && recordNode != null && differingFields.size() != 0);
	}

	// ---------------------------------------------------------------------------
	
	@Override
	public RefactoringParticipant[] loadParticipants(RefactoringStatus status, SharableParticipants sharedParticipants) throws CoreException {
		return new RefactoringParticipant[0];
	}

}
