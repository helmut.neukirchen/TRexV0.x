package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEditGroup;

import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceFinder;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;

public class TTCN3ReplaceTemplateWithModifiedProcessor extends RefactoringProcessor {
	
	private LocationAST calledNode;
	
	private LocationAST bestChoice;
	
	private String nameOfDef;
	
	private LocationAST calledNodeIdentifier;
	
	private List<LocationAST> candidates = new LinkedList<LocationAST>();
	
	private Set<IFile> modifiedFiles = new HashSet<IFile>();	
	
	private Set<LocationAST> similarSet = new HashSet<LocationAST>();

	
	public TTCN3ReplaceTemplateWithModifiedProcessor(LocationAST identifierNode) {
		if (identifierNode == null)
			return;
		if (identifierNode.getType() != TTCN3LexerTokenTypes.IDENTIFIER)
			return;
		calledNodeIdentifier = identifierNode;
		calledNode = LocationAST.resolveParentsUntilType(identifierNode, TTCN3ParserTokenTypes.TemplateDef);
		setUpSimilarSet(identifierNode);
	}
	
	private void setUpSimilarSet(LocationAST ast) {
		LocationAST mainAST = LocationAST.resolveParentsUntilType(ast,
				TTCN3ParserTokenTypes.TemplateDef);
		if (mainAST == null)
			return;

		LocationAST x =mainAST.getFirstChild().getFirstChild().getFirstChild().getFirstChild().getFirstChild().getFirstChild();
		ReferenceFinder referenceFinder = new ReferenceFinder();
		
		Scope scope = x.getScope();
		Symbol symb = scope.resolve(x);
		Map<String, List<ReferenceWithContext>> result = referenceFinder.findReferences(symb);
		Collection<List<ReferenceWithContext>> y = result.values();
		Iterator<List<ReferenceWithContext>> z = y.iterator();
		while (z.hasNext())
		{
			List<ReferenceWithContext> here = z.next();
			Iterator<ReferenceWithContext> bla = here.iterator();
			while (bla.hasNext())
			{
				similarSet.add(bla.next().getReferenceNode()); 
			}
			
		}
		
		

	}
	
	public boolean isApplicable() throws CoreException {
		if (calledNode == null) return false;
		if (calledNode.getFirstChild().getFirstChild().getNextSibling().getFirstChild() != calledNodeIdentifier) return false;
		return true;
	}

//	find all templates of the same record-type:
	public void findCandidates()
	{
		candidates.clear();
		LocationAST y = calledNode;
		while (y.getFirstChild()!=null) y = y.getFirstChild();
		String s = y.getText();
		
		
		
		
		getReferencesForRecord(calledNode, s, candidates);

		// process all matching asts for references
		for (final LocationAST ast : similarSet) {
			getReferencesForRecord(ast, s, candidates);
		}
		
		Collections.sort(candidates);
		LocationAST comp = calledNode.getFirstChild()
		.getFirstChild()
		.getFirstChild()
		.getFirstChild()
		.getFirstChild()
		.getFirstChild();
		// jetzt liegen da aber die Records!!! Die, die nicht auf Templates zeigen: entfernen!
		Iterator<LocationAST> runner = candidates.iterator();
		LocationAST x;
		int i = -1;
		while (runner.hasNext())
		{
			
			x = runner.next();
			if (x==null) {runner.remove(); continue;}
			if (!x.getText().equals(comp.getText())) {
				runner.remove(); continue;}
			i++;
			candidates.set(i,
					LocationAST.resolveParentsUntilType(x,
							TTCN3ParserTokenTypes.TemplateDef)
					);        //Ersetzen durch die Template-Def!!
			
			
		}
		
		// We do want the callee to appear as first entry.
		// (This is usefull for findbestchoice)
		
		LocationAST ex = LocationAST.resolveParentsUntilType(calledNodeIdentifier, TTCN3ParserTokenTypes.TemplateDef);
		candidates.add(0, ex);
		
		Iterator<LocationAST> srunner = candidates.iterator();
		LocationAST foo = srunner.next();
		if (foo.getParent()!=null) foo = foo.getParent();   // nonsense, just to remove the warning
		while (srunner.hasNext())
		{
			LocationAST b = srunner.next();
			
			if (b==null) {srunner.remove(); continue;}
			if (b.
					equals(ex)) srunner.remove();
		}
		
		
	}
	
	
//	find bestChoice. This must not be done before running findCandidates().
	public void findBestChoice()
	{
		TTCN3ReplaceTemplateWithModifiedRefactoring refactoring = (TTCN3ReplaceTemplateWithModifiedRefactoring) getRefactoring();
		Iterator<LocationAST> tester = candidates.iterator();
		Iterator<LocationAST> runner = candidates.iterator();
		LocationAST u;
		int ref = 0;
		int i = 0;
		LocationAST refee = tester.next();
		boolean[] commons;
		while (runner.hasNext())
		{
			u = runner.next();
			while ((u == null)&&runner.hasNext()) u = runner.next();
			if (u==null) continue;
			commons = compareTemplates(refee, u);
			i = 0;
			while (i < commons.length)
			{
				if (commons[i]==true) ref++;
				i++;
			}
		}
		bestChoice = refee;
		int mom = 0;
		while (tester.hasNext())
		{
			refee = tester.next();
			while ((refee==null)&&(tester.hasNext())) refee = tester.next();
			if (refee==null) continue;
			mom = 0;
			runner = candidates.iterator();
			while (runner.hasNext())
			{
				u = runner.next();
				while ((u == null)&&runner.hasNext()) u = runner.next();
				if (u==null) continue;
				commons = compareTemplates(refee, u);
				i = 0;
				while (i < commons.length)
				{
					if (commons[i]==true) mom++;
					i++;
				}
			}
			if (mom < ref)
			{
				LocationAST m = refee.getFirstChild();
				while (m.getFirstChild()!=null) m = m.getFirstChild();
				LocationAST n = calledNode.getFirstChild();
				while (n.getFirstChild()!=null) n = n.getFirstChild();
				
				boolean isInMod = (m.getScope().getModuleScope().equals(n.getScope().getModuleScope()));
				if (refactoring.getLocateInAllModules())
				{ref = mom;
				bestChoice = refee;}
				if (isInMod)
				{ref = mom;
				bestChoice = refee;
				}
			}
		}
		// throw away bestChoice
		tester = candidates.iterator();
		while (tester.hasNext())
		{
			u = tester.next();
			if (u==null) {tester.remove(); continue;}
			if (bestChoice.equals(u)) tester.remove();
		}
	}
	
// returns true if myTemplate differs from bestChoice only within the limit
	public boolean isRelevant(LocationAST myTemplate)
	{
		TTCN3ReplaceTemplateWithModifiedRefactoring refactoring = (TTCN3ReplaceTemplateWithModifiedRefactoring) getRefactoring();
		if (myTemplate==null) return false;
		if (myTemplate.getType() != TTCN3ParserTokenTypes.TemplateDef) return false;
		while (myTemplate.getType()!= TTCN3ParserTokenTypes.IDENTIFIER)
		{
//			if (myTemplate== null) return false;
			myTemplate = myTemplate.getFirstChild();
		}
		if (myTemplate.getText().equals(nameOfDef)) 
			{
			myTemplate = LocationAST.resolveParentsUntilType(myTemplate, TTCN3ParserTokenTypes.TemplateDef);
			int i = 0;
			int j = 0;
			boolean[] s = compareTemplates(bestChoice,myTemplate);
			while (i < s.length)
			{
				if (s[i] == true) j++;
				i++;
			}
			int p = (int) (Math.floor((((double)(100-refactoring.getLimit()))/100)*((double)s.length)));
			if (j <= p) return true;
			}
		return false;
	}

	

// looks if two LocationASTs are the same (up to position in the code)
	public boolean areDeeplyIdentical(LocationAST first, LocationAST second)
	{
		
		if (first==null) { if (second==null) return true; else return false;}
		if (second == null) return false;
		if (!first.getText().equals(second.getText())) return false;
		if (!areDeeplyIdentical(first.getNextSibling(),second.getNextSibling())) return false;
		if (!areDeeplyIdentical(first.getFirstChild(), second.getFirstChild())) return false;
		return true;
	}
	

	
// find the differences.
	public boolean[] compareTemplates(LocationAST first,LocationAST second)
	{
		if ((first.getType()!=TTCN3ParserTokenTypes.TemplateDef) ||
				(second.getType()!=TTCN3ParserTokenTypes.TemplateDef)) return new boolean[] {} ;
		LocationAST firstWorker=first, secondWorker=second;
		while (firstWorker.getType()!=TTCN3ParserTokenTypes.IDENTIFIER) 
		{
			firstWorker = firstWorker.getFirstChild();
			secondWorker = secondWorker.getFirstChild();
		}
		if (secondWorker== null) return new boolean[] {};
		if (!firstWorker.getText().equals(secondWorker.getText())) return new boolean[] {};
		firstWorker = first.getFirstChild().getNextSibling().getFirstChild();  //steht dann auf dem argumentenzweig
		secondWorker = second.getFirstChild().getNextSibling().getFirstChild();
		int i = 0;
		LocationAST runner = firstWorker.getFirstChild();
		while (runner.getNextSibling()!= null)
		{
			i++;
			runner = runner.getNextSibling();			
		}
		boolean[] ret = new boolean[i+1];        
		
		int j = 0;
		runner = firstWorker.getFirstChild();
		LocationAST climber;
		LocationAST secondClimber;
		LocationAST ams;
		String a;
		while (runner!= null)
		{
			climber = runner.getFirstChild();
			while (climber.getFirstChild()!= null) climber = climber.getFirstChild();
			a = climber.getText();         
			secondClimber = secondWorker.getFirstChild();
			ams = secondClimber;
			while (ams.getFirstChild()!= null) ams = ams.getFirstChild();
			while ((!ams.getText().equals(a))&&(secondClimber.getNextSibling()!=null))
			{
				secondClimber = secondClimber.getNextSibling();
				ams = secondClimber;
				while (ams.getFirstChild()!= null) ams = ams.getFirstChild();
			}

			secondClimber = secondClimber.getFirstChild().getNextSibling();
			
			climber = runner.getFirstChild().getNextSibling();
			ret[j] = !areDeeplyIdentical(climber, secondClimber);
			
			
			j++;
			runner = runner.getNextSibling();
		}
		return ret;
	}
	
// checks if bc is visible for quest (if bc is imported into the module in that quest lives)
	private boolean isVisible(LocationAST quest, LocationAST bc)
	{
		quest = quest.getFirstChild().getFirstChild().getNextSibling();
		while (quest.getFirstChild()!= null) quest = quest.getFirstChild();
		
		bc = bc.getFirstChild().getFirstChild().getNextSibling();
		while (bc.getFirstChild()!= null) bc = bc.getFirstChild();
		
		
		Scope scope = quest.getScope();
		Symbol symb = scope.resolve(quest);
		if (symb==null) return false;
		Map<String, Symbol> a = scope.getSymbols(); 
		return a.containsValue(bc.getSymbol());
		
	}
	
	public Change createChange(IProgressMonitor pm) throws CoreException,
	OperationCanceledException {
		CompositeChange compositeChange = new CompositeChange("Merge Template");
		TTCN3ReplaceTemplateWithModifiedRefactoring refactoring = (TTCN3ReplaceTemplateWithModifiedRefactoring) getRefactoring();
		
		findCandidates();
		
		
		// now: if the user restricted the refactoring to the actual module, remove the others..
		
		if (refactoring.getRestrictToThisModule())
		{
			Iterator<LocationAST> temRunner = candidates.iterator();
			
			LocationAST m = calledNodeIdentifier;
			m = LocationAST.resolveParentsUntilType(m, TTCN3ParserTokenTypes.TemplateDef);
			m = m.getFirstChild().getFirstChild().getNextSibling();
			while (m.getFirstChild()!=null) m = m.getFirstChild();
		
			Symbol mSymbol = getDeclarationSymbol(m);
			if (mSymbol == null)
				return compositeChange;       
			
			while (temRunner.hasNext())
			{
				LocationAST n = temRunner.next();
				n = LocationAST.resolveParentsUntilType(n, TTCN3ParserTokenTypes.TemplateDef);
				n = n.getFirstChild().getFirstChild().getNextSibling();
				while (n.getFirstChild()!=null) n = n.getFirstChild();
			
				Symbol yourSymbol = getDeclarationSymbol(n);
				if (yourSymbol == null)
					return compositeChange;       
				if (!mSymbol.getSurroundingScope().getModuleScope().equals(yourSymbol.getSurroundingScope().getModuleScope()))
							temRunner.remove();
			}
			
		}
		
		
		if (refactoring.getLocateInAllModules()||refactoring.getLocateInThisModule()) {
			findBestChoice();
		}
		else
		{
			bestChoice = LocationAST.resolveParentsUntilType(calledNodeIdentifier, TTCN3ParserTokenTypes.TemplateDef);
			
			Iterator<LocationAST> runner = candidates.iterator();
			
			while (runner.hasNext())
			{
				LocationAST q = runner.next();
				if (q==null) {runner.remove(); continue;}
				if (q.equals(bestChoice)){runner.remove(); continue;}
			}
			
		}
		
		
		Iterator<LocationAST> runner = candidates.iterator();
		while (bestChoice.getType()!= TTCN3ParserTokenTypes.IDENTIFIER)
		{
			bestChoice = bestChoice.getFirstChild();
		}
		nameOfDef = bestChoice.getText();
		bestChoice = LocationAST.resolveParentsUntilType(bestChoice, TTCN3ParserTokenTypes.TemplateDef);
		
		
		// remove the non-relevant nodes..
		while (runner.hasNext())
		{
			if (!isRelevant(LocationAST.resolveParentsUntilType(runner.next(), TTCN3ParserTokenTypes.TemplateDef))) runner.remove();
		}
	
		//remove the non-visible nodes.. (i. e. the nodes that are not imported to the module in that bestchoice lives)
		runner = candidates.iterator();
		while (runner.hasNext())
		{
			if (!isVisible(LocationAST.resolveParentsUntilType(runner.next(), TTCN3ParserTokenTypes.TemplateDef),
					LocationAST.resolveParentsUntilType(bestChoice, TTCN3ParserTokenTypes.TemplateDef))) runner.remove();
		}
		

		List<LocationAST> x = candidates;
		Iterator<LocationAST> it = x.iterator();
		while (it.hasNext())
		{
			LocationAST n = it.next();
			it.remove();
			
			if (n==null) continue;
			n = LocationAST.resolveParentsUntilType(n, TTCN3ParserTokenTypes.TemplateDef);
			n = n.getFirstChild().getFirstChild().getNextSibling();
			while (n.getFirstChild()!=null) n = n.getFirstChild();
			
			
			Symbol mySymbol = getDeclarationSymbol(n);
			if (mySymbol == null)
				return compositeChange;
			String myFileName = mySymbol.getSurroundingScope()
			.getAssociatedFilename();
			IFile declarationFile = getProjectFile(myFileName);
			modifiedFiles.add(declarationFile);
			n = LocationAST.resolveParentsUntilType(n, TTCN3ParserTokenTypes.TemplateDef);
			
			MultiTextEdit multiEdit = new MultiTextEdit();
			TextFileChange fileChange = new TextFileChange(declarationFile.getName(), declarationFile);
			
			fileChange.setEdit(multiEdit);
			fileChange.setTextType("ttcn3");
			compositeChange.add(fileChange);
			modify(bestChoice, n, multiEdit, fileChange );
			while (it.hasNext())
			{
				n = it.next();
				if (n==null) {it.remove();continue;}
				
				n = LocationAST.resolveParentsUntilType(n, TTCN3ParserTokenTypes.TemplateDef);
				n = n.getFirstChild().getFirstChild().getNextSibling();
				while (n.getFirstChild()!=null) n = n.getFirstChild();
				
				
				Symbol yourSymbol = getDeclarationSymbol(n);
				if (yourSymbol == null)
					return compositeChange;       //fehlermeldung!!!
				String yourFileName = yourSymbol.getSurroundingScope()
				.getAssociatedFilename();
				
				
				n = LocationAST.resolveParentsUntilType(n, TTCN3ParserTokenTypes.TemplateDef);
				if (myFileName.equals(yourFileName))
				{
					it.remove();
					modify(bestChoice, n, multiEdit, fileChange );
				
				}
				
			}
			it = x.iterator();
			
		}
		
		return compositeChange;
	}
	
// replaces obsolet by an modification of callee	
	private void modify (LocationAST callee, LocationAST obsolet, MultiTextEdit multiEdit, TextFileChange fileChange)
	{
		LocationAST v = obsolet;
		v = LocationAST.resolveParentsUntilType(v,TTCN3ParserTokenTypes.TemplateDef);
		v = v.getFirstChild().getFirstChild().getNextSibling().getFirstChild();
		LocationAST w = callee;
		w = LocationAST.resolveParentsUntilType(w,TTCN3ParserTokenTypes.TemplateDef);
		w = w.getFirstChild().getFirstChild().getNextSibling().getFirstChild();
		TextEditGroup master = new TextEditGroup("Replace "+v.getText()+" with a Modification of "+w.getText()+".");
		LocationAST x = obsolet;
		x = LocationAST.resolveParentsUntilType(x, TTCN3ParserTokenTypes.TemplateDef);
		LocationAST y = callee;
		y = LocationAST.resolveParentsUntilType(y, TTCN3ParserTokenTypes.TemplateDef);
		boolean[] s = compareTemplates(x, y);
		boolean addModifies = false;
		int i = 0;
		int begOff, endOff;
		
		y = y.getFirstChild().getNextSibling().getFirstChild().getFirstChild();
		x = x.getFirstChild().getNextSibling().getFirstChild().getFirstChild(); //Steht auf FieldSpec
		
		do
			{
			
			if (s[i]==false)
				{
				
				
				
				if (i==0) begOff = x.getOffset();
				else {
					LocationAST q = x;
					q = LocationAST.resolveParentsUntilType(q, TTCN3ParserTokenTypes.FieldSpecList);
					q = q.getFirstChild();
					while (!q.getNextSibling().equals(x)) q = q.getNextSibling();
					begOff = q.getEndOffset();
					
					int k = i;
					while (k < s.length)
					{	
					if (s[k]==true)begOff = x.getOffset();
					k++;
					}
				}
				
				if (x.getNextSibling()== null) endOff = x.getEndOffset();
				else {
					if (s[i+1]==true) endOff = x.getNextSibling().getOffset();
					else 
					{
						
						while (x.getNextSibling()!=null)
						{
							x = x.getNextSibling();
							i++;
							if (((i+1)>=s.length)||(s[i+1]==true)) break;
							
						}
						if (x.getNextSibling()== null) endOff = x.getEndOffset();
						else endOff = x.getNextSibling().getOffset();
						
					}
					
				}
				
				ReplaceEdit replaceEdit = new ReplaceEdit(begOff,
						endOff - begOff
						, "");
						multiEdit.addChild(replaceEdit);
						master.addTextEdit(replaceEdit);
						addModifies = true;
						
				}
			
			
			i++;
			x = x.getNextSibling();
			}
		while (x!=null);
		
		x = obsolet;
		y = callee;

		if (addModifies)
		{

			y=y.getFirstChild().getFirstChild().getNextSibling();
			while (y.getFirstChild()!=null) y = y.getFirstChild();

			
			x = LocationAST.resolveParentsUntilType(x, TTCN3ParserTokenTypes.TemplateDef);
			x = x.getFirstChild();
			
			
			ReplaceEdit replaceEdit = new ReplaceEdit(x.getEndOffset(),
					0
					, " modifies "+ y.getText());
					multiEdit.addChild(replaceEdit);
					master.addTextEdit(replaceEdit);
					fileChange.addTextEditGroup(master);
		}
	}
	
	private Symbol getDeclarationSymbol(LocationAST identifierNode) {
		Scope scope = identifierNode.getScope();
		if (scope == null)
			return null;
		return scope.resolve(identifierNode);
	}

	private void getReferencesForRecord(final LocationAST ast,       
			String recordName, List<LocationAST> replacements) {
		LocationAST ident = ASTUtil.getIdentNode(ast);
		Symbol symb = ident.getScope().resolve(ident);
		Map<String, List<ReferenceWithContext>> references = new ReferenceFinder()
				.findReferences(symb);

		// extract references from result
		// and cope with multiple references
		for (Map.Entry<String, List<ReferenceWithContext>> entry : references
				.entrySet()) {
			List<ReferenceWithContext> refs = entry.getValue();
			ListIterator<ReferenceWithContext> refsIt = refs.listIterator(refs
					.size());

			while (refsIt.hasPrevious()) {
				ReferenceWithContext prev = refsIt.previous();
				if (ident.equals(prev.getMatchNode())) {
					replacements.add(prev.getReferenceNode()
							);
				}
			}
		}

		replacements.add(ast); 
	}
	
	private IFile getProjectFile(String filename) {
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		IProject project = root.getProject();

		IFile file = null;
		if (project == null) {
			IPath path = ws.getRoot().findMember(filename).getFullPath();
			file = ws.getRoot().getFile(path);
		} else {
			file = project.getFile(filename);
		}

		return file;
	}
	
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm)
	throws CoreException, OperationCanceledException {
// check the refactoring was called upon a TemplateDef and that a Set of
// similar templates is known
if (calledNode == null
		|| calledNode.getType() != TTCN3ParserTokenTypes.TemplateDef)
	return RefactoringStatus
			.createFatalErrorStatus("You _must_ select the identifier "
					+ "of a template declaration to perform the replace refactoring.");

if (similarSet == null)
	return RefactoringStatus
			.createFatalErrorStatus("This template does not have a set of known similar templates.\n\n"
					+ "However, it may be in the set of similar templates for another template.\n"
					+ "Use the 'Problems' tab to guide you.");


return new RefactoringStatus();
}

	@Override
	public RefactoringStatus checkFinalConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws CoreException,
			OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		findCandidates();
		if ((candidates == null)||(candidates.size()<=1))
		return RefactoringStatus
				.createFatalErrorStatus("No Template lies within your specified limit.");

		
		return status;
	}

	public Object[] getElements() {
		return new String[0];
	}

	public String getProcessorName() {
		return "Replace Template With Modified Template Processor";
	}
	
	public String getIdentifier() {
		return "Replace Template With Modified Template Identifier";
	}

	@Override
	public RefactoringParticipant[] loadParticipants(RefactoringStatus status,
			SharableParticipants sharedParticipants) throws CoreException {
		return new RefactoringParticipant[0];
	}

	public Set<IFile> getModifiedFiles() {
		return modifiedFiles;
	}
	
}
