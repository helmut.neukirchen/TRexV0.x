package de.ugoe.cs.swe.trex.core.refactoring.core;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEditGroup;

import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceFinder;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;


public class TTCN3RemoveAssignmentsToParametersProcessor extends RefactoringProcessor {

	private Map<String, List<ReferenceWithContext>> references;
	
	private Symbol declarationSymbol;
	
	private String currentName;
	
	private Set<String> refsKeySet;
	
	private int temp = 0;
	
	private int refCount = 0;
	
	private String spaceString = "";
	
	private LocationAST declarationNode;
	
	private TTCN3Analyzer analyzer;
	
	private String parType = "";
	
	private String code;

	public TTCN3RemoveAssignmentsToParametersProcessor(LocationAST identifierNode) {
		if (identifierNode == null)
			return;
		if (identifierNode.getType() != TTCN3LexerTokenTypes.IDENTIFIER)
			return;
		declarationSymbol = getDeclarationSymbol(identifierNode);
		if (declarationSymbol == null)
			return;
		references = new ReferenceFinder().findReferences(declarationSymbol);
		declarationNode = declarationSymbol.getDeclarationNode();
		if (declarationNode == null)
			return;

		refsKeySet = references.keySet();
		refCount = refsKeySet.size();
		
	}

	// ---------------------------------------------------------------------------

	@Override
	public Object[] getElements() {
		return new String[0];
	}

	// ---------------------------------------------------------------------------

	@Override
	public String getIdentifier() {
		return "RemoveAssignmentsToParameters Processor Identifier";
	}

	// ---------------------------------------------------------------------------

	@Override
	public String getProcessorName() {
		return "RemoveAssignmentsToParameters Processor";
	}

	// ---------------------------------------------------------------------------

	@Override
	public boolean isApplicable() {
		return (true);
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkInitialConditions(IProgressMonitor pm)
			throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		if (	declarationNode != null  &&
				declarationNode.getType() != TTCN3LexerTokenTypes.Identifier  &&
				declarationNode.getParent() != null &&	
				declarationNode.getParent().getParent() != null &&
				declarationNode.getParent().getParent().getType() != TTCN3LexerTokenTypes.FormalPortPar &&
				declarationNode.getParent().getParent().getType() != TTCN3LexerTokenTypes.FormalTemplatePar &&
				declarationNode.getParent().getParent().getType() != TTCN3LexerTokenTypes.FormalTimerPar &&
				declarationNode.getParent().getParent().getType() != TTCN3LexerTokenTypes.FormalTypePar &&
				declarationNode.getParent().getParent().getType() != TTCN3LexerTokenTypes.FormalValuePar){
			status.addFatalError("Select a parameter!");
		}
		if(refCount == 0){
			status.addFatalError("No changes to be made!");
		}
		return status;
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringStatus checkFinalConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws OperationCanceledException {
		RefactoringStatus status = new RefactoringStatus();
		return status;
	}

	// ---------------------------------------------------------------------------

	private void createChanges(CompositeChange composite, IProgressMonitor pm) {
		
		pm.beginTask("Remove Assignments To Parameters Refactoring", refsKeySet.size());

		String declarationFileName = declarationSymbol.getSurroundingScope()
				.getAssociatedFilename();
		IFile declarationFile = getProjectFile(declarationFileName);
		MultiTextEdit declarationMultiEdit = new MultiTextEdit();
		TextFileChange declarationFileChange = new TextFileChange(
				declarationFile.getName(), declarationFile);
		declarationFileChange.setEdit(declarationMultiEdit);
		declarationFileChange.setTextType("ttcn3");
		composite.add(declarationFileChange);

		analyzer = TTCN3AnalyzerFlyweightFactory.getInstance().getTTCN3Analyzer(declarationFile);
		code = analyzer.getCode();
		
		for(int u = declarationNode.getParent().getParent().getOffset(); u < declarationNode.getParent().getOffset();u++){
			parType = parType + code.charAt(u);
		}
		if(!parType.startsWith("template")){
			parType = "var " + parType;
		}
		
		for (String filename : refsKeySet) {
			List<ReferenceWithContext> refs = references.get(filename);
			IFile file = getProjectFile(filename);
			MultiTextEdit multiEdit;
			TextFileChange fileChange;
			if (filename.equals(declarationFileName)) {
				multiEdit = declarationMultiEdit;
				fileChange = declarationFileChange;
			} else {
				multiEdit = new MultiTextEdit();
				fileChange = new TextFileChange(file.getName(), file);
				fileChange.setEdit(multiEdit);
				fileChange.setTextType("ttcn3");
				composite.add(fileChange);
			}
			for (ReferenceWithContext ref : refs) {
				LocationAST node = ref.getReferenceNode();
				addEditForNode(node, "update reference", multiEdit,
						fileChange);
				pm.worked(1);
			}
		}
	}
	
	private void addEditForNode(LocationAST node, String textEditGroupName, MultiTextEdit multiEdit, TextFileChange fileChange) {
		InsertEdit insertEdit = new InsertEdit(0,"");
		ReplaceEdit replaceEdit = new ReplaceEdit(0,0,"");
		if(node.getParent().getParent().getType() == TTCN3LexerTokenTypes.VariableRef || currentName == null){
			currentName = getNextName();
			if(currentName != null){
				replaceEdit = new ReplaceEdit(node.getParent().getOffset(), (node.getParent().getEndOffset() - node.getParent().getOffset()), currentName);
				multiEdit.addChild(replaceEdit);
			}
			insertEdit = new InsertEdit(getLastNewLine(node.getParent().getOffset()), "\n" + spaceString + parType + currentName + " := " + declarationNode.getText() + ";");
			spaceString = "";
		}else {
			replaceEdit = new ReplaceEdit(node.getParent().getOffset(), (node.getParent().getEndOffset() - node.getParent().getOffset()), currentName);
			multiEdit.addChild(replaceEdit);
		}
		multiEdit.addChild(insertEdit);
		TextEditGroup declarationEditGroup = new TextEditGroup(textEditGroupName, insertEdit);
		fileChange.addTextEditGroup(declarationEditGroup);
	}
	
	// ---------------------------------------------------------------------------

	@Override
	public Change createChange(IProgressMonitor pm)
			throws OperationCanceledException {
		CompositeChange composite = new CompositeChange("RemoveAssignmentsToParameters");

		createChanges(composite, pm);
		return composite;
	}

	// ---------------------------------------------------------------------------

	@Override
	public RefactoringParticipant[] loadParticipants(RefactoringStatus status,
			SharableParticipants sharedParticipants) {
		return new RefactoringParticipant[0];
	}

	
	private IFile getProjectFile(String filename) {
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		IProject project = root.getProject();

		IFile file = null;
		if (project == null) {
			IPath path = ws.getRoot().findMember(filename).getFullPath();
			file = ws.getRoot().getFile(path);
		} else {
			file = project.getFile(filename);
		}

		return file;
	}
	
	private Symbol getDeclarationSymbol(LocationAST identifierNode) {
		Scope scope = identifierNode.getScope();
		if (scope == null)
			return null;
		return scope.resolve(identifierNode);
	}
	
	private String getNextName(){
		return "" + declarationNode.getText() + "Temp" + temp++;
	}
	
	private int getLastNewLine(int offset){
		int currentOffset = offset - 1;
		while(code.charAt(currentOffset) != '\n' && code.charAt(currentOffset) != '\r'){
			if(code.charAt(currentOffset) == '\t'){
				spaceString = '\t' + spaceString;
			}else spaceString = ' ' + spaceString;
			currentOffset--;
		}
		if(code.charAt(currentOffset) == '\n' && code.charAt(currentOffset-1) == '\r'){
			currentOffset--;
		}
		return currentOffset;
	}
		
}
