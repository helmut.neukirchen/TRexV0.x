package de.ugoe.cs.swe.trex.refactoring.ui.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Display;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3ReplaceMagicNumberProcessor;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3ReplaceMagicNumberRefactoring;
import de.ugoe.cs.swe.trex.refactoring.ui.TTCN3ReplaceMagicNumberWizard;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;

public class ReplaceMagicNumberHandler extends RefactoringHandler {

	/**
	 * calling super constructor
	 */
	public ReplaceMagicNumberHandler() {
		super();
	}

	public void run(IAction action) {
		if (workbenchHasUnsavedEditors()) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Extract Template Refactoring",
					"All modified files must be saved before proceeding...");
			return;
		}

		
		final TTCN3ReplaceMagicNumberProcessor processor = new TTCN3ReplaceMagicNumberProcessor(
				getIdentifierNodeFromEditorOffset(editor));
		final TTCN3ReplaceMagicNumberRefactoring refactoring = new TTCN3ReplaceMagicNumberRefactoring(
				processor);
		final TTCN3ReplaceMagicNumberWizard wizard = new TTCN3ReplaceMagicNumberWizard(
				refactoring, RefactoringWizard.WIZARD_BASED_USER_INTERFACE);
		final RefactoringWizardOpenOperation openOperation = new RefactoringWizardOpenOperation(
				wizard);

		try {
			final int ran = openOperation.run(Display.getCurrent().getActiveShell(),
					"Refactoring not possible!");
			if (ran == IDialogConstants.OK_ID) {
				final IFile ffile = (IFile) editor.getEditorInput().getAdapter(
						IFile.class);
				// only re-analyze modified files
				editor.getReconcilingStrategy().analyzeAll(
						processor.getModifiedFiles(), ffile);
			}
		} catch (InterruptedException e) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Extract Template Refactoring",
					"Error while applying refactoring to workbench/wizard: "
							+ e.getMessage());
			e.printStackTrace();
		}

	}

	private LocationAST getIdentifierNodeFromEditorOffset(final TTCN3Editor editor) {
		final Integer offset = editor.getCaretOffset();
		
		LocationAST res = null;
		
		if (offset != null){
			final IdentifierCache identifierCache = editor.getReconcilingStrategy().getIdentifierCache();
			if (identifierCache != null){
				res = identifierCache.get(offset);
			}
		}
		return res;
	}

}
