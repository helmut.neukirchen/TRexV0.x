package de.ugoe.cs.swe.trex.refactoring.ui.quickfix;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.views.markers.WorkbenchMarkerResolution;

import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3InlineTemplateProcessor;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3InlineTemplateRefactoring;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPluginImages;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3MultiPageEditor;

/**
 * using WorkbenchMarkerResolution to
 * allow many matching problems to be selected and fixed at the same time
 */
public class TTCN3QuickFix extends WorkbenchMarkerResolution {
//	private IMarker marker; // Temporarily disabled: see TTC-195 and TTC-192.
	private IFile file;
//	private String message; // Temporarily disabled: see TTC-195 and TTC-192.
	final private boolean matchingMarkers;
	final private String solution;
	
	// ---------------------------------------------------------------------------
	
	/**
	 * Construct a <code>TTCN3QuickFix</code> object to represent a potential
	 * resolution for an <code>IMarker</code> problem.
	 * 
	 * @param label A description of the resolution.
	 * @param solution A code representing the solution.
	 */
	TTCN3QuickFix(final IMarker marker, final String message, final String solution) {
		super();
//		this.message = message; // Temporarily disabled: see TTC-195 and TTC-192.
		this.solution = solution;
		this.matchingMarkers = false;
		if(marker != null) {
//			this.marker = marker; // Temporarily disabled: see TTC-195 and TTC-192.
			this.file = (IFile) marker.getResource();
		}
	}
	
	// ---------------------------------------------------------------------------
	
	/** (non-Javadoc)
	 * @see org.eclipse.ui.IMarkerResolution#getLabel()
	 */
	public String getLabel() {
		return this.solution;
	}
	
	// ---------------------------------------------------------------------------	

	/**
	 * This will return the description of the test as plain text
	 * when it has been implemented.
	 */
	public String getDescription() {
		// TODO Auto-generated method stub
		return "TEST DESCRIPTION";
	}
	
	// ---------------------------------------------------------------------------	

	public Image getImage() {
		return TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.FIX_DELETE);
	}	
	
	// ---------------------------------------------------------------------------

	public static TTCN3Editor getEditor(final IFile file) {
		// See if there is an open editor on the file containing the marker
		final IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (workbenchWindow == null) {
			System.err.println("w == null");
			return null;
		}
		
		final IWorkbenchPage page = workbenchWindow.getActivePage();
		if (page == null) {
			System.err.println("page == null");
			return null;
		}
		
		final IFileEditorInput input = new FileEditorInput(file);
		IEditorPart editorPart = page.findEditor(input);
		
		if (editorPart == null) {
			// open an editor
			try {
				System.err.println("Doing IDE.openEditor");
				editorPart = IDE.openEditor(page, file, true);
			} catch (PartInitException e) {
				MessageDialog.openError(workbenchWindow.getShell(), "Resolution_Error",
				"Unable_to_open_file_editor");
			}
		}
		
		if (editorPart == null) {
			System.err.println("editorPart == null");
			return null;
		}

		TTCN3Editor editor;
		TTCN3MultiPageEditor multipageEditor;
		
		if (editorPart instanceof TTCN3Editor) {
			editor = (TTCN3Editor) editorPart;
		} else if (editorPart instanceof TTCN3MultiPageEditor) {
			multipageEditor = (TTCN3MultiPageEditor) editorPart;
			editor = multipageEditor.getFEditor();
		} else {
			editor = null;
			System.err
			.println("!(editorPart instanceof TTCN3Editor || TTCN3MultiPageEditor)");
		}
		
		return editor;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IMarkerResolution#run(org.eclipse.core.resources.IMarker)
	 */
	public void run(final IMarker mark) {
		final TTCN3Editor editor = getEditor((IFile)mark.getResource());
		
		if (editor == null) {
			System.err.println("editor == null");
		} else {
			
			try {
				if ("remove template".equals(solution) || solution.startsWith("inline template")) {
					
					final int ident = mark.getAttribute("IdentifierOffset", -1);
					final LocationAST identNode = editor.getReconcilingStrategy().getIdentifierCache().get(ident);
		
					if ("remove template".equals(solution)) {
						
						// check for an optional semicolon and schedule that for removal if found
						final LocationAST templateDefNode = LocationAST.resolveParentsUntilType(identNode, TTCN3LexerTokenTypes.TemplateDef);
						final LocationAST parent = templateDefNode == null ? null : templateDefNode.getParent();
						final LocationAST sibling = parent == null ? null : (LocationAST)parent.getNextSibling();
						
						int endOffsetShift = 0;
						
						// remove optional semicolon
						if(sibling != null && sibling.getType() == TTCN3LexerTokenTypes.SemiColon) {
							endOffsetShift += sibling.getEndOffset() - sibling.getOffset();				
						}
	
						final IDocument doc = editor.getDocumentProvider().getDocument(
								editor.getEditorInput());
						final int start = mark.getAttribute(IMarker.CHAR_START, -1);
						final int end = mark.getAttribute(IMarker.CHAR_END, -1);
						// delete the template and the warning marker
						doc.replace(start, end - start + endOffsetShift, "");
						
						// delete marker from resource
						mark.delete();
					} else {
						// inline
						inlineTemplate(identNode);
					}
				}
			} catch (BadLocationException e) {
				// ignore
				return;
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	// ---------------------------------------------------------------------------
	
	/**
	 * Find the reference to the given template and perform the 'inline
	 * template' refactoring on that reference. 
	 * TODO: extend to cope with inlining into multiple references.
	 * 
	 * @param identNode the identifier node for the template that should be inlined.
	 */
	private void inlineTemplate(final LocationAST identNode) {
		if(identNode == null) {
			return;
		}
		
		final Map<String, List<ReferenceWithContext>> references = TTCN3Analyzer.FILE_TO_REFERENCES.get(this.file);
		
		// extract references from result and cope with multiple references
		final List<LocationAST> refNodes = new ArrayList<LocationAST>();
		for(Map.Entry<String, List<ReferenceWithContext>> entry : references.entrySet()) {
			final List<ReferenceWithContext> refs = entry.getValue();

			final ListIterator<ReferenceWithContext> refsIt = refs.listIterator( refs.size() );
			
			while(refsIt.hasPrevious()) {
				final ReferenceWithContext prev = refsIt.previous();
				if(identNode.equals(prev.getMatchNode())) {
					refNodes.add(prev.getReferenceNode());
				}
			}

			inlineRefactoring(refNodes, true);
			refNodes.clear();
		}
	}

	// ---------------------------------------------------------------------------	

	private void inlineRefactoring(final List<LocationAST> refs, final boolean delete) {
		final TTCN3InlineTemplateProcessor processor = new TTCN3InlineTemplateProcessor(refs);
		final TTCN3InlineTemplateRefactoring refactoring = new TTCN3InlineTemplateRefactoring(processor);
		if(delete) {
			refactoring.setDeleteDeclaration(true);
		}
		refactoring.setPrettyPrintInlinedTemplates(false);
		try {
			performRefactoring(refactoring);
			
			if(this.matchingMarkers) {
				// re-parse and analzye the file to update the AST & references
				final TTCN3Analyzer analyzer = TTCN3AnalyzerFlyweightFactory.getInstance().getTTCN3Analyzer(this.file, false);
				if (analyzer == null) {
					return;
				}
				analyzer.analyze();
			}
		} catch (Exception e) {
			System.err.println("inlineTemplate exception -->\n");
			e.printStackTrace();
		    System.err.println("\n<-- inlineTemplate exception");
		}
	}
	
	// ---------------------------------------------------------------------------
	
	public void performRefactoring(final Refactoring ref) throws Exception {		
//		RefactoringStatus status = ref.checkAllConditions(new NullProgressMonitor());
		ref.checkAllConditions(new NullProgressMonitor());
//		Assert.assertFalse("Refactoring produced fatal error",
//						   status.hasFatalError());
		final Change change = ref.createChange(new NullProgressMonitor());
		change.initializeValidationData(new NullProgressMonitor());
//		RefactoringStatus valid = change.isValid(new NullProgressMonitor());
//		Assert.assertFalse("Refactoring has a fatal error!", valid.hasFatalError());
		change.setEnabled(true);
		change.perform(new NullProgressMonitor());
		change.dispose();
//		loadModifiedFiles();
	}

	// ---------------------------------------------------------------------------	
	
	/**
	 * Simply <code>Comparator</code> class to compare <code>IMarker</code>
	 * objects and sort them in ascending filename but descending offset order.
	 * i.e. so that the markers later on in each file come before those that are
	 * earlier on in a file.
	 * 
	 */
	static class SortMarker implements Comparator<IMarker>, Serializable {
		/**
		 * This is the UID (Unique Identification Number) of the serial version.
		 */
		private static final long serialVersionUID = 8969062452110775159L;

		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(final IMarker mark0, final IMarker mark1) {
			final IFile file0 = (IFile) mark0.getResource();
			final IFile file1 = (IFile) mark1.getResource();
			int ret;
			if(file0.equals(file1)) {
				final int offset0 = mark0.getAttribute(IMarker.CHAR_START, -1);
				final int offset1 = mark1.getAttribute(IMarker.CHAR_START, -1);
				ret = offset1 - offset0;
			} else {
				ret = file0.getName().compareTo(file1.getName());
			}
			return ret;
		}
	}

	// ---------------------------------------------------------------------------	
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.markers.WorkbenchMarkerResolution#findOtherMarkers(org.eclipse.core.resources.IMarker[])
	 */
	@Override
	public IMarker[] findOtherMarkers(final IMarker[] markers) {
// Temporarily disabled: see TTC-195 and TTC-192.
//		// we can only do multiple inlinings if they appear before 'this' marker,
//		// for deletions this doesn't matter
//
//		ArrayList<IMarker> result = new ArrayList<IMarker>(markers.length);
//		// the maximum offset we can modify is that of the marker we are working from
//		int thisoffset = this.marker.getAttribute(IMarker.CHAR_START, -1);
//		
//		// loop through all markers and add those with the same problem message
//		// that either appear in the same file but BEFORE
//		// the marker we are working from.
//		for(IMarker mk : markers) {
//			if(mk != this.marker) {
//				IFile file = (IFile) mk.getResource();
//				int offset = mk.getAttribute(IMarker.CHAR_START, -1);
//				if(file.equals(this.file) && offset < thisoffset) {
//					String msg = mk.getAttribute(IMarker.MESSAGE, "");
//					if(msg.equals(this.message)) {
//						result.add(mk);
//					}
//				}
//			}
//		}
//		
//		if(result.size() > 0) {
//			this.matchingMarkers = true;
//		}
//		
//		// sort the result and return it as an Array
//		Collections.sort(result, new SortMarker());
//
//		return result.toArray(new IMarker[result.size()]);
		return new IMarker[0];
	}
}
