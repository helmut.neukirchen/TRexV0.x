package de.ugoe.cs.swe.trex.refactoring.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class InlineParamPageComposite extends Composite {

	/**
	 * This is the code formatter button.
	 */
	final private Button CodeFormatterButton;

	/**
	 * Composes the page, adding the code formatter button.
	 */
	public InlineParamPageComposite(final Composite parent, final int style) {
		super(parent, style);
		setLayout(new GridLayout());

		CodeFormatterButton = new Button(this, SWT.CHECK);
		CodeFormatterButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, true, false));
		CodeFormatterButton.setText("Use code formatter on inlined template");
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * getter for the code formatter button
	 */
	public Button getCodeFormatterButton() {
		return CodeFormatterButton;
	}

}
