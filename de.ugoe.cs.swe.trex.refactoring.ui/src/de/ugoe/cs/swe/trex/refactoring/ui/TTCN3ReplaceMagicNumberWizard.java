package de.ugoe.cs.swe.trex.refactoring.ui;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3ReplaceMagicNumberRefactoring;

public class TTCN3ReplaceMagicNumberWizard extends RefactoringWizard {

	private TTCN3ReplaceMagicNumberRefactoring refactoring;


	public TTCN3ReplaceMagicNumberWizard(final Refactoring newRefactoring,
			final int flags) {
		super(newRefactoring, flags);
		if (newRefactoring instanceof TTCN3ReplaceMagicNumberRefactoring) {
			refactoring = (TTCN3ReplaceMagicNumberRefactoring) newRefactoring;
		}
	}



	// ---------------------------------------------------------------------------

	@Override
	protected void addUserInputPages() {
		final ReplaceMagicNumberChoiceWizardPage wizardPage = new ReplaceMagicNumberChoiceWizardPage("Refactoring");
		if(refactoring!=null) {
			wizardPage.setNewTemplateNodeName(refactoring.getNewConstantNumberName());
		}
		addPage(wizardPage);
	}

	
	static class ReplaceMagicNumberChoiceWizardPage extends UserInputWizardPage {
		private Button checkButton;
		private Text templateNodeName;
		
		private boolean processEachOccurrence;
		private String newTemplateNodeName;
		public ReplaceMagicNumberChoiceWizardPage(final String name) {
			super(name);
			newTemplateNodeName = "";
			setTitle("Extract Template Refactoring");
		}

		public void createControl(final Composite parent) {
			final Composite comp = new Composite(parent, SWT.NONE);
			final GridLayout layout = new GridLayout(2,false);
			comp.setLayout(layout);
			final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true,false);
			comp.setData(gridData);
						
			final Label label2 = new Label(comp, SWT.NONE);
			label2.setText("New Template Name: ");

			templateNodeName = new Text(comp, SWT.NONE);
			templateNodeName.setText(getNewTemplateNodeName());

			final Label label = new Label(comp, SWT.NONE);
			label.setText("Perform extraction for each occurrence?");

			checkButton = new Button(comp, SWT.CHECK);

					
			setControl(comp);
		}
		
		
		@Override
		public IWizardPage getNextPage() {
			storePropertiesToRefectoring();
			return super.getNextPage();
		}

		private void storePropertiesToRefectoring() {
			final TTCN3ReplaceMagicNumberRefactoring refactoring = (TTCN3ReplaceMagicNumberRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setProcessEachOccurrence(checkButton.getSelection());
				refactoring.setNewTemplateNodeName(templateNodeName.getText());
			}
		}
		
		@Override
		protected boolean performFinish() {
			storePropertiesToRefectoring();
			return super.performFinish();
		}

		public boolean isProcessEachOccurrence() {
			return processEachOccurrence;
		}

		public void setProcessEachOccurrence(final boolean newProcessEachOccurrence) {
			this.processEachOccurrence = newProcessEachOccurrence;
		}

		public String getNewTemplateNodeName() {
			return newTemplateNodeName;
		}

		public void setNewTemplateNodeName(final String newTemplateNodeName) {
			this.newTemplateNodeName = newTemplateNodeName;
		}

	}

}
