package de.ugoe.cs.swe.trex.refactoring.ui.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Display;

//import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
//import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3DecomposeTemplateProcessor;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3DecomposeTemplateRefactoring;
import de.ugoe.cs.swe.trex.refactoring.ui.TTCN3DecomposeTemplateRefactoringWizard;
//import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;



public class DecomposeTemplateHandler extends RefactoringHandler{

	public void run(IAction action) {
		if (workbenchHasUnsavedEditors()) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Decompose Template Refactoring",
					"All modified files must be saved before proceeding...");
			return;
		
		}
		final TTCN3DecomposeTemplateProcessor processor = new TTCN3DecomposeTemplateProcessor(editor);
		final TTCN3DecomposeTemplateRefactoring refactoring = new TTCN3DecomposeTemplateRefactoring(processor);
		final TTCN3DecomposeTemplateRefactoringWizard wizard = new TTCN3DecomposeTemplateRefactoringWizard(
				refactoring, RefactoringWizard.WIZARD_BASED_USER_INTERFACE);
		wizard.setNamesOfDecomposedTemplates(processor.getNamesOfDecomposedTemplates());
		final RefactoringWizardOpenOperation openOperation = new RefactoringWizardOpenOperation(
				wizard);
		
		final String titleForFailedChecks = "";
		try {
			final int ran = openOperation.run(Display.getCurrent().getActiveShell(),
					titleForFailedChecks);
			// don't re-analyze if it was cancelled, only if 'OK'
			if (ran == IDialogConstants.OK_ID) {
				final IFile ffile = (IFile) editor.getEditorInput().getAdapter(
						IFile.class);
				// only re-analyze modified files
				editor.getReconcilingStrategy().analyzeAll(
						processor.getModifiedFiles(), ffile);
			}
		} catch (InterruptedException e) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Decompose Template Refactoring",
					"Error while applying refactoring to workbench/wizard: "
							+ e.getMessage());
			e.printStackTrace();
		}
		 

		

	}
	

}
