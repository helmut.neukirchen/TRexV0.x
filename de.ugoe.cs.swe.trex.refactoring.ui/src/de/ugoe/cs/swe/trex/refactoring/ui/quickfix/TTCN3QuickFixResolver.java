package de.ugoe.cs.swe.trex.refactoring.ui.quickfix;

import org.eclipse.core.resources.IMarker;
import org.eclipse.ui.IMarkerResolution;
import org.eclipse.ui.IMarkerResolutionGenerator;

public class TTCN3QuickFixResolver implements IMarkerResolutionGenerator {
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IMarkerResolutionGenerator#getResolutions(org.eclipse.core.resources.IMarker)
	 */
	public IMarkerResolution[] getResolutions(final IMarker marker) {
		final String message = marker.getAttribute(IMarker.MESSAGE, "");
		final String solution = marker.getAttribute("TTCN3Solution", "");
		IMarkerResolution[] ret;
		if(message.length() == 0 || solution.length() == 0) {
			ret = new IMarkerResolution[0];
		} else {
			ret = new IMarkerResolution[] { new TTCN3QuickFix(marker, message, solution) };
		}
		return ret;
	}
}
