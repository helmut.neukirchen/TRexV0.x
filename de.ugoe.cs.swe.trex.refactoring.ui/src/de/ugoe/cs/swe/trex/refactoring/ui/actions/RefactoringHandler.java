package de.ugoe.cs.swe.trex.refactoring.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;

import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3MultiPageEditor;

/**
 * Abstract base class for Refactoring handlers
 */
public abstract class RefactoringHandler implements IEditorActionDelegate, IWorkbenchWindowActionDelegate {

	protected TTCN3Editor editor;

	protected TTCN3MultiPageEditor multipageEditor;

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		if (targetEditor instanceof TTCN3Editor) {
			editor = (TTCN3Editor) targetEditor;
		} else if (targetEditor instanceof TTCN3MultiPageEditor) {
			multipageEditor = (TTCN3MultiPageEditor) targetEditor;
			editor = multipageEditor.getFEditor();
		}
	}

	public void selectionChanged(final IAction action, final ISelection selection) {
		// do nothing
	};

	protected boolean workbenchHasUnsavedEditors() {
		// TODO: exception handling
		final IEditorPart[] dirtyEditors = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getDirtyEditors();
		return dirtyEditors.length > 0;
	}
	
	public void dispose() {
		// do nothing
	}

	public void init(IWorkbenchWindow window) {
		final IEditorPart targetEditor = window.getActivePage().getActiveEditor();
		if (targetEditor instanceof TTCN3Editor) {
			editor = (TTCN3Editor) targetEditor;
		} else if (targetEditor instanceof TTCN3MultiPageEditor) {
			multipageEditor = (TTCN3MultiPageEditor) targetEditor;
			editor = multipageEditor.getFEditor();
		}
		
	}
}