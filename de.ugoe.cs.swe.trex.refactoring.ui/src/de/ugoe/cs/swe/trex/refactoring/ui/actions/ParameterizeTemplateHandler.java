package de.ugoe.cs.swe.trex.refactoring.ui.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Display;

import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3ParameterizeTemplateProcessor;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3ParameterizeTemplateRefactoring;
import de.ugoe.cs.swe.trex.refactoring.ui.TTCN3ParameterizeTemplateRefactoringWizard;


public class ParameterizeTemplateHandler extends RefactoringHandler{

	public void run(final IAction action) {
		if (workbenchHasUnsavedEditors()) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Decompose Template Refactoring",
					"All modified files must be saved before proceeding...");
			return;
		
		}
		final TTCN3ParameterizeTemplateProcessor processor = new TTCN3ParameterizeTemplateProcessor(editor);
		final TTCN3ParameterizeTemplateRefactoring refactoring = new TTCN3ParameterizeTemplateRefactoring(processor);
		final TTCN3ParameterizeTemplateRefactoringWizard wizard = new TTCN3ParameterizeTemplateRefactoringWizard(
				refactoring, RefactoringWizard.WIZARD_BASED_USER_INTERFACE);
		final RefactoringWizardOpenOperation openOperation = new RefactoringWizardOpenOperation(
				wizard);
		
		final String titleFailedChecks = "";
		try {
			final int ran = openOperation.run(Display.getCurrent().getActiveShell(),
					titleFailedChecks);
			// don't re-analyze if it was cancelled, only if 'OK'
			if (ran == IDialogConstants.OK_ID) {
				final IFile ffile = (IFile) editor.getEditorInput().getAdapter(
						IFile.class);
				// only re-analyze modified files
				editor.getReconcilingStrategy().analyzeAll(
						processor.getModifiedFiles(), ffile);
			}
		} catch (InterruptedException e) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Parameterize Template Refactoring",
					"Error while applying refactoring to workbench/wizard: "
							+ e.getMessage());
			e.printStackTrace();
		}
		
	}

}
