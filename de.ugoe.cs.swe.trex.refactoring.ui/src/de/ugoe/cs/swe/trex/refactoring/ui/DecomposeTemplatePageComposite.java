package de.ugoe.cs.swe.trex.refactoring.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class DecomposeTemplatePageComposite extends Composite{

	private Text renameText;
	private int numberOfTexts;
	//private List<String> namesOfDecomposedTemplates = new ArrayList<String>();
	//private List<Text> names= new ArrayList<Text>();
	final private Text[] nameText;
	
	public DecomposeTemplatePageComposite(final Composite parent, final int style, final int namesSize) {
		super(parent, style);
		final GridLayout gridLayout = new GridLayout();
		setLayout(gridLayout);

		final Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 2;
		composite.setLayout(gridLayout_1);
		
		numberOfTexts = namesSize;
		nameText = new Text[numberOfTexts] ;
		Label [] nameLabel = new Label[numberOfTexts] ;
		int templateNr;
		for(int i = 0; i < numberOfTexts; i++){
			nameLabel [i]= new Label(composite, SWT.NONE);
			nameLabel[i].setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, true));
			templateNr = i+1;
			nameLabel[i].setText(" The name of Decomposed Template  "+templateNr);
			nameText[i] = new Text(composite, SWT.BORDER);
			//names.add(new Text(composite, SWT.BORDER)) ;
			nameText[i].setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, true));
		}
		
	}
	
	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * getter for renameText
	 */
	public Text getRenameText() {
		return renameText;
	}

	public List<String> getNamesOfDecomposedTemplates(){
		final List<String> namesOfTemplates = new ArrayList<String>();

		System.out.println("nameText.length = "+nameText.length);
		if(nameText.length > 0) {
			for(int i = 0; i < numberOfTexts; i++){
				namesOfTemplates.add(i, nameText[i].getText());
			}
		}
		return namesOfTemplates;
	}
	
	/**
	 * setter for numberOfTexts
	 */
	public void setNumberOfTexts(final int setValue) {
		numberOfTexts = setValue;		
	}

	public void setNamesOfDecomposedTemplates(final List<String> namesOfTemplates) {
//		List<Text> temp = new ArrayList<Text>();
		for(int i = 0; i < namesOfTemplates.size(); i++){
			nameText[i].setText(namesOfTemplates.get(i));
		}	
	}

	/**
	 * getter for nameText
	 */
	public Text[] getTexts() {
		// TODO Auto-generated method stub
		return nameText;
	}

}
