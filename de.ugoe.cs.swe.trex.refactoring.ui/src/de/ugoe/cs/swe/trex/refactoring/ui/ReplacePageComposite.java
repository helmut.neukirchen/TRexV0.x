package de.ugoe.cs.swe.trex.refactoring.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

public class ReplacePageComposite extends Composite {

	final private Spinner spinner;
	final private Button button1, button2, button3;

	
	public ReplacePageComposite(final Composite parent, final int style) {
		super(parent, style);
		final GridLayout gridLayout = new GridLayout();
		setLayout(gridLayout);

		final Composite composite = new Composite(this, SWT.NONE);
		
		
		final Label sep = new Label(composite, SWT.SEPARATOR|SWT.HORIZONTAL);
		sep.setLayoutData(new GridData (GridData.BEGINNING,GridData.CENTER, false,false));

		sep.setBounds(10, 50, 350, 10);
		
		
		
		final Label limitLabel = new Label(composite, SWT.NONE);
		limitLabel.setLayoutData(new GridData (GridData.BEGINNING, GridData.CENTER, false, true));
		limitLabel.setText("Limit :");
		limitLabel.setBounds(10, 10, 34, 17);

		
		spinner = new Spinner (composite, SWT.BORDER);
		spinner.setMinimum(0);
		spinner.setMaximum(99);
		spinner.setIncrement(5);
		spinner.setPageIncrement(20);
		spinner.pack();
		spinner.setBounds(44, 7, 44, 25);
		
		
		final Label percentLabel = new Label(composite, SWT.NONE);
		percentLabel.setLayoutData(new GridData (GridData.BEGINNING, GridData.CENTER, false, true));
		percentLabel.setText("%");
		percentLabel.setBounds(91, 10, 34, 17);
		
		
		final Label exp = new Label(composite, SWT.NONE);
		exp.setText(" of template fields must have identical values.");
		exp.setLayoutData(new GridData (GridData.BEGINNING,GridData.CENTER, false, true));
		exp.setBounds(130, 10, 550, 46);
		
		
		button2 = new Button(composite, SWT.CHECK);
		button2.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false,false));
		button2.setText("Locate best base template candidate in all modules");
		button2.setBounds(10,70, 490, 27);
		
		button3 = new Button(composite, SWT.CHECK);
		button3.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false,false));
		button3.setText("Locate best base template candidate in current module");
		button3.setBounds(10,90, 290, 27);
		
		button1 = new Button(composite, SWT.CHECK);
		button1.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false,false));
		button1.setText("Restrict changes to the current module");
		button1.setBounds(10,110, 290, 27);
		
		final Listener listener = new Listener() {
			public void handleEvent(final Event evnt) {
				button2.setEnabled(!button2.isEnabled());
			}
		};
		button3.addListener(SWT.Selection, listener);
		
		final Listener listener2 = new Listener() {
			public void handleEvent(final Event evnt) {
				button3.setEnabled(!button3.isEnabled());
			}
		};
		button2.addListener(SWT.Selection, listener2);
		
		
		
	}
	

	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * getter for the spinner
	 */
	public Spinner getSpinner(){
		return spinner;
	}

	/**
	 * getter for the first button (restrict to current module)
	 */
	public Button getRestrictToCurrentModule() {
		return button1;
	}

	/**
	 * getter for the second button (locate best in all modules)
	 */
	public Button getLocateBestInAllModules() {
		return button2;
	}

	/**
	 * getter for the third button (locate best in current module)
	 */
	public Button getLocateBestInCurrentModule() {
		return button3;
	}
}
