package de.ugoe.cs.swe.trex.refactoring.ui;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.swt.widgets.Composite;

// import de.ugoe.cs.swe.trex.core.refactoring.core.
// TTCN3RemoveAssignmentsToParametersRefactoring;


public class TTCN3RemoveAssignmentsToParametersRefactoringWizard extends RefactoringWizard {
//	private ParameterInputWizardPage wizardPage;
	
	// ---------------------------------------------------------------------------

	public TTCN3RemoveAssignmentsToParametersRefactoringWizard(final Refactoring refactoring, final int flags) {
		super(refactoring, flags);
		
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void addUserInputPages() {
//		wizardPage = new ParameterInputWizardPage("Refactoring");
//		addPage(wizardPage);

	}

	// ---------------------------------------------------------------------------

	static class ParameterInputWizardPage extends UserInputWizardPage {

//		private RemoveAssignmentsToParametersPageComposite composite;
		
		public ParameterInputWizardPage(final String name) {
			super(name);
			setTitle("Add Parameter Refactoring");
			setDescription("Automated Refactoring to add parameters to testcases, functions and altsteps.");
		}

		public void createControl(final Composite parent) {
//				composite = new RemoveAssignmentsToParametersPageComposite(parent, 0);
//				setControl(composite);
		}
		
//		public String getParamName(int number) {
//			return composite.getParamNames(number);
//		}


		@Override
		public IWizardPage getNextPage() {
			/*
			TTCN3RemoveAssignmentsToParametersRefactoring refactoring =
				(TTCN3RemoveAssignmentsToParametersRefactoring) getRefactoring();
			if (refactoring != null) {

			}
			*/
			
			return super.getNextPage();
		}
		
		@Override
		protected boolean performFinish() {
			/*
			TTCN3RemoveAssignmentsToParametersRefactoring refactoring =
				(TTCN3RemoveAssignmentsToParametersRefactoring) getRefactoring();
			if (refactoring != null) {

			}
			*/

			return super.performFinish();
		}
	}
}
