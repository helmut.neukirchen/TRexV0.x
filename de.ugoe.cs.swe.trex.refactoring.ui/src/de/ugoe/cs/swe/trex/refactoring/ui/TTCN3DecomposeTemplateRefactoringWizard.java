package de.ugoe.cs.swe.trex.refactoring.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3DecomposeTemplateRefactoring;

public class TTCN3DecomposeTemplateRefactoringWizard extends RefactoringWizard{

	private DecomposeTemplateWizardPage wizardPage;
	private List<String> namesOfDecomposedTemplates = new ArrayList<String>();
	
	public TTCN3DecomposeTemplateRefactoringWizard(final Refactoring refactoring, final int flags) {
		super(refactoring, flags);
	}

	@Override
	protected void addUserInputPages() {
		wizardPage = new DecomposeTemplateWizardPage("Refactoring");
		wizardPage.setNamesOfDecomposedTemplates(namesOfDecomposedTemplates);
		addPage(wizardPage);
		
	}
	
	public void setNamesOfDecomposedTemplates(final List<String> namesOfDecomposedTemplates) {
		this.namesOfDecomposedTemplates = namesOfDecomposedTemplates;
		if (wizardPage != null) {
			wizardPage.setNamesOfDecomposedTemplates(namesOfDecomposedTemplates);
		}
	}
	
	// ---------------------------------------------------------------------------

	static class DecomposeTemplateWizardPage extends UserInputWizardPage {

		private DecomposeTemplatePageComposite composite;
		private List<String> namesOfDecomposedTemplates = new ArrayList<String>();
		private TTCN3DecomposeTemplateRefactoring refactoring;

		public DecomposeTemplateWizardPage(final String name) {
			super(name);
			setTitle("Decompose Template Refactoring");
			setDescription("Automated Refactoring to decompose template .");
		}		
		
		public void createControl(final Composite parent) {
			composite = new DecomposeTemplatePageComposite(parent, 0, namesOfDecomposedTemplates.size());
			//composite.setNumberOfTexts(namesOfDecomposedTemplates.size());
			composite.setNamesOfDecomposedTemplates(namesOfDecomposedTemplates);
			//names= new ArrayList<Text>();
			final Text[] nameText = composite.getTexts();
			Text temp;
			for(int i = 0; i < nameText.length; i++){
				temp = nameText[i];
				temp.selectAll();
			}
			
			setControl(composite);
		}
		
		/*
		public String getRenameText() {
			return composite.getRenameText().getText();
		}
		*/
		
		/*
		public void setRenameText(String s) {
			renameText = s;
			if (composite != null)
				composite.getRenameText().setText(renameText);
		}
		*/
		
		public void setNamesOfDecomposedTemplates(final List<String> namesOfDecomposedTemplates) {
			this.namesOfDecomposedTemplates = namesOfDecomposedTemplates;
			if (composite != null){
				composite.setNamesOfDecomposedTemplates(namesOfDecomposedTemplates);
				
			}
		}
		
		public List<String> getNamesOfDecomposedTemplates(){
			return  composite.getNamesOfDecomposedTemplates();
		}
		
		@Override
		public IWizardPage getNextPage() {
			if (refactoring == null) {
				refactoring = (TTCN3DecomposeTemplateRefactoring) getRefactoring();
			}
			if (refactoring != null) {
				final List<String> names = getNamesOfDecomposedTemplates();
				refactoring.setNamesOfDecomposedTemplates(names);
			}
			return super.getNextPage();
		}
		
		@Override
		protected boolean performFinish() {
			final TTCN3DecomposeTemplateRefactoring refactoring = (TTCN3DecomposeTemplateRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setNamesOfDecomposedTemplates(getNamesOfDecomposedTemplates());
				
			}
			return super.performFinish();
		}

		

	}

	

}
