package de.ugoe.cs.swe.trex.refactoring.ui;


import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;


public class TTCN3MoveModuleConstantToComponentRefactoringWizard extends RefactoringWizard {

	// ---------------------------------------------------------------------------

	public TTCN3MoveModuleConstantToComponentRefactoringWizard(final Refactoring refactoring, final int flags) {
		super(refactoring, flags);
	}

	// ---------------------------------------------------------------------------

	/**
	 * Overriding addUserInputPages with this empty method
	 * will prevent the execution of the super() method.
	 */
	@Override
	protected void addUserInputPages() {
		// do precisely nothing
	}
	
	// ---------------------------------------------------------------------------
	
	@Override
	public boolean performFinish() {
		return super.performFinish();
	}

	
}
