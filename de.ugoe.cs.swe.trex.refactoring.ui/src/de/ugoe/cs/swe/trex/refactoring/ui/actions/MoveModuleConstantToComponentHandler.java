package de.ugoe.cs.swe.trex.refactoring.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Display;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3MoveModuleConstantToComponentProcessor;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3MoveModuleConstantToComponentRefactoring;
import de.ugoe.cs.swe.trex.refactoring.ui.TTCN3MoveModuleConstantToComponentRefactoringWizard;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;

public class MoveModuleConstantToComponentHandler extends RefactoringHandler {

	public void run(final IAction action) {
		if (workbenchHasUnsavedEditors()) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"MoveModuleConstantToComponent Refactoring",
					"All modified files must be saved before proceeding...");
			return;
		}
			
		final TTCN3MoveModuleConstantToComponentProcessor processor = new TTCN3MoveModuleConstantToComponentProcessor(getIdentifierNodeFromEditorOffset(editor));
		final TTCN3MoveModuleConstantToComponentRefactoring refactoring = new TTCN3MoveModuleConstantToComponentRefactoring(processor);
		final TTCN3MoveModuleConstantToComponentRefactoringWizard wizard = new TTCN3MoveModuleConstantToComponentRefactoringWizard(
				refactoring, RefactoringWizard.WIZARD_BASED_USER_INTERFACE);
		final RefactoringWizardOpenOperation openOperation = new RefactoringWizardOpenOperation(
				wizard);
		try {
			openOperation.run(Display.getCurrent().getActiveShell(),
					"Refactoring not possible!");
		} catch (InterruptedException e) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Move Module Constant To Component Refactoring",
					"Error while applying refactoring to workbench/wizard: "
							+ e.getMessage());
			e.printStackTrace();
		}
		
	}

	// ---------------------------------------------------------------------------

	private LocationAST getIdentifierNodeFromEditorOffset(final TTCN3Editor editor) {
		final Integer offset = editor.getCaretOffset();
		if (offset == null){
			return null;
		}

		final IdentifierCache identifierCache = editor.getReconcilingStrategy()
				.getIdentifierCache();
		if (identifierCache != null){
			return identifierCache.get(offset);
		}
		return null;
	}
}
