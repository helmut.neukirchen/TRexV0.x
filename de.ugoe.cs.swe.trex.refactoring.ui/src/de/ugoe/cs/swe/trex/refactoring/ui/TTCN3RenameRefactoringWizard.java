package de.ugoe.cs.swe.trex.refactoring.ui;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.swt.widgets.Composite;

import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3RenameRefactoring;



public class TTCN3RenameRefactoringWizard extends RefactoringWizard {
	private RenameInputWizardPage wizardPage;
	private String renameText = "";
	
	// ---------------------------------------------------------------------------

	public TTCN3RenameRefactoringWizard(final Refactoring refactoring, final int flags) {
		super(refactoring, flags);
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void addUserInputPages() {
		wizardPage = new RenameInputWizardPage("Refactoring");
		wizardPage.setRenameText(renameText);
		addPage(wizardPage);
	}

	// ---------------------------------------------------------------------------

	public void setRenameText(final String newtext) {
		renameText = newtext;
		if (wizardPage != null) {
			wizardPage.setRenameText(newtext);
		}
	}

	// ---------------------------------------------------------------------------

	static class RenameInputWizardPage extends UserInputWizardPage {

		private RenamePageComposite composite;
		private String renameText;
		
		public RenameInputWizardPage(final String name) {
			super(name);
			setTitle("Rename Refactoring");
			setDescription("Automated Refactoring to rename variables, methods, type declarations etc."
					+ " with the possibility to automatically update references");
		}

		public void createControl(final Composite parent) {
			composite = new RenamePageComposite(parent, 0);
			composite.getRenameText().setText(renameText);
			composite.getRenameText().selectAll();
			setControl(composite);
		}
		
		public String getRenameText() {
			return composite.getRenameText().getText();
		}
		
		public void setRenameText(final String newtext) {
			renameText = newtext;
			if (composite != null) {
				composite.getRenameText().setText(renameText);
			}
		}
		
		@Override
		public IWizardPage getNextPage() {
			final TTCN3RenameRefactoring refactoring = (TTCN3RenameRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setRenameText(getRenameText());
			}
			return super.getNextPage();
		}
		
		@Override
		protected boolean performFinish() {
			final TTCN3RenameRefactoring refactoring = (TTCN3RenameRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setRenameText(getRenameText());
			}

			return super.performFinish();
		}
	}

}
