package de.ugoe.cs.swe.trex.refactoring.ui;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.swt.widgets.Composite;

import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3InlineParamRefactoring;


public class TTCN3InlineParamRefactoringWizard extends RefactoringWizard {

	// ---------------------------------------------------------------------------

	public TTCN3InlineParamRefactoringWizard(final Refactoring refactoring, final int flags) {
		super(refactoring, flags);
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void addUserInputPages() {
		final InlineParamWizardPage wizardPage = new InlineParamWizardPage("Refactoring");
		addPage(wizardPage);
	}
	
	// ---------------------------------------------------------------------------
	
	@Override
	public boolean performFinish() {
		return super.performFinish();
	}

	// ---------------------------------------------------------------------------

	static class InlineParamWizardPage extends UserInputWizardPage {

		private InlineParamPageComposite composite;

		public InlineParamWizardPage(final String name) {
			super(name);
			setTitle("Inline Parameter Refactoring");
			setDescription("Automated Refactoring to inline parameter(s)");
		}
		
		public void createControl(final Composite parent) {
			composite = new InlineParamPageComposite(parent, 0);
			setControl(composite);
		}
		
		@Override
		public IWizardPage getNextPage() {
			final TTCN3InlineParamRefactoring refactoring = (TTCN3InlineParamRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setPrettyPrintInlinedTemplates(composite.getCodeFormatterButton().getSelection());
			}
			return super.getNextPage();
		}
		
		@Override
		protected boolean performFinish() {
			final TTCN3InlineParamRefactoring refactoring = (TTCN3InlineParamRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setPrettyPrintInlinedTemplates(composite.getCodeFormatterButton().getSelection());
			}
			return super.performFinish();
		}

	}
	
}
