package de.ugoe.cs.swe.trex.refactoring.ui;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class AddParameterPageComposite extends Composite {

	private int parametersCount;

	private String paramTypes[];
	private String paramNames[];
	private String paramRefs[];

	final private Vector<String> pNames = new Vector<String>();
	final private Vector<String> pTypes = new Vector<String>();
	final private Vector<String> pRefs = new Vector<String>();
	final private Vector<String> presentPNames;

	public AddParameterPageComposite(final Composite parent, final int style,
			final Vector<String> pPnames) {
		super(parent, style);
		presentPNames = pPnames;
		
		final GridLayout gridLayout = new GridLayout();
		setLayout(gridLayout);
		final Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, true));
		final GridLayout gridLayout_1 = new GridLayout(2, false);
		composite.setLayout(gridLayout_1);
		
		final Label typeLabel = new Label(composite, SWT.NONE);
		typeLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, true));
		typeLabel.setText("Parameter Type:");
		final Text paramType = new Text(composite, SWT.BORDER);
		paramType.setLayoutData(new GridData(GridData.FILL, GridData.CENTER,
				true, true));

		final Label nameLabel = new Label(composite, SWT.NONE);
		nameLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, true));
		nameLabel.setText("Parameter Name:");
		final Text paramName = new Text(composite, SWT.BORDER);
		paramName.setLayoutData(new GridData(GridData.FILL, GridData.CENTER,
				true, true));
		
		final Label refLabel = new Label(composite, SWT.NONE);
		refLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, true));
		refLabel.setText("Default Value:");
		final Text paramRef = new Text(composite, SWT.BORDER);
		paramRef.setLayoutData(new GridData(GridData.FILL, GridData.CENTER,
				true, true));
		

		final Button templateButton = new Button(composite, SWT.CHECK);
		templateButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		templateButton.setText("Template");
		templateButton.setSelection(false);

		final Button paramAddButton = new Button(composite, SWT.PUSH);
		paramAddButton.setBounds(10, 10, 50, 50);
		paramAddButton.setText("Add Parameter");

		final Table paramTable = new Table(composite, SWT.FILL);
		paramTable.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		paramTable.setHeaderVisible(true);
		paramTable.setItemCount(5);

		final TableColumn pTemplate = new TableColumn(paramTable, SWT.LEFT);
		pTemplate.setText("T");
		pTemplate.setWidth(20);
		final TableColumn pType = new TableColumn(paramTable, SWT.LEFT);
		pType.setText("Type");
		pType.setWidth(120);
		final TableColumn pName = new TableColumn(paramTable, SWT.LEFT);
		pName.setText("Name");
		pName.setWidth(120);
		final TableColumn pRef = new TableColumn(paramTable, SWT.LEFT);
		pRef.setText("Def. Value");
		pRef.setWidth(120);
		
		
		final Button paramDeleteButton = new Button(composite, SWT.PUSH);
		paramDeleteButton.setBounds(10, 10, 50, 50);
		paramDeleteButton.setText("Remove Parameter");
		
		final Label errorLabel = new Label(composite, SWT.NONE);
		errorLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, true));
		errorLabel.setText("                                                                                        ");

		addAddBtnListener(paramAddButton, paramTable, errorLabel,
						  templateButton, paramType.getText(), paramName.getText(),
						  paramRef.getText());
		addDeleteBtnListener(paramDeleteButton, paramTable);
	}
	
	void addAddBtnListener(final Button paramAddButton, final Table paramTable, final Label errorLabel,
						   final Button templateButton, final String paramType, final String paramName,
						   final String paramRef) {
		paramAddButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent selEvent) {
				errorLabel.setText("");
				if(paramTable.getItemCount() != 0 &&  paramTable.getItems()[0].getText(0).equals("")){
					paramTable.removeAll();
				}
				
				if ((paramType.length()==0) || (paramName.length() == 0)) {
					return;
				}

				String errStr = checkName(paramName);
					
				if(!" ".equals(errStr)){
					errorLabel.setText(errStr);
					return;
				}

				errStr = checkType(paramType);

				if(!" ".equals(errStr)){
					errorLabel.setText(errStr);
					return;
				}
				
				if(paramNameAlreadyExists(paramName)) {
					errorLabel.setText("A parameter with this name already exists.");
					return;
				}
				
				final TableItem parameter = new TableItem(paramTable, SWT.NONE);
				if (templateButton.getSelection()) {
					parameter.setText(0, "x");
				} else {
					parameter.setText(0, "-");
				}
								
				parameter.setText(1, paramType);
				parameter.setText(2, paramName);
				parameter.setText(3, paramRef);
				pNames.add(paramName.trim());
				pRefs.add(paramRef.trim());
				if(templateButton.getSelection()){
					pTypes.add("template " + paramType);
				} else {
					pTypes.add(paramType);
				}
				errorLabel.setText("");
			}
		});
	}
	
	void addDeleteBtnListener(final Button paramDeleteButton, final Table paramTable) {
		paramDeleteButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(final SelectionEvent selEvent) {
				final int[] itemNumber = paramTable.getSelectionIndices();
				paramTable.remove(itemNumber[0]);
				pNames.remove(itemNumber[0]);
				pTypes.remove(itemNumber[0]);
				pRefs.remove(itemNumber[0]);
			}
		});
	}

	@Override
	public void dispose() {
		super.dispose();
	}
	
	private boolean paramNameAlreadyExists(final String pName){
		for (final String o : pNames){ 
			if(o.equals(pName)){
				return true;
			}
		}
		for (final String o : presentPNames){ 
			if(o.equals(pName)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Creates the parameters based on pNames, pTypes and pRefs,
	 * assuming that the length of both pTypes and pRefs does not
	 * individually exceed the length of pNames.
	 */
	public void createParameters() {
		parametersCount = pNames.size();
		paramTypes = new String[parametersCount];
		paramNames = new String[parametersCount];
		paramRefs = new String[parametersCount];
		int paramNo = 0;
		for (final String o : pTypes){ 
			paramTypes[paramNo] = o;
			paramNo++;
		}
		paramNo = 0;
		for (final String o : pNames){ 
			paramNames[paramNo] = o;
			paramNo++;
		}
		paramNo = 0;
		for (final String o : pRefs){ 
			paramRefs[paramNo] = o;
			paramNo++;
		}
	}

	/**
	 * Gives back the type with the indicated number.
	 */
	public String getParamTypes(final int number) {
		return paramTypes[number];
	}

	/**
	 * Gives back the name with the indicated number.
	 */
	public String getParamNames(final int number) {
		return paramNames[number];
	}

	/**
	 * Gives back the reference with the indicated number.
	 */
	public String getParamRefs(final int number) {
		return paramRefs[number];
	}

	/**
	 * Gives back the overall maximum amount of parameters of each type.
	 */
	public int getParamCount() {
		return parametersCount;
	}

	/**
	 * Checks if the given name is valid, that is:
	 * It needs to have at least one character.
	 * The first character needs to be a letter or '_'.
	 * The other characters need to be letters, '_' or digits.
	 * 
	 * The return value is a string indicating what is wrong
	 * or just a space if everything is alright.
	 */
	private String checkName(final String name){
		String ret;
		if (name.length() <= 0) {
			 ret = "Enter a name!";
		}else {
			if (java.lang.Character.isDigit(name.charAt(0))) {
				ret = "Enter a valid Name!";
			} else {
				ret = " ";
				for(int i = 0; i < name.length();i++){
					if(!(java.lang.Character.isLetter(name.charAt(i)) || java.lang.Character.isDigit(name.charAt(i)) || name.charAt(i) == '_')){
						ret = "Enter a valid Name!";
						break;
					}
				}
			}
		}
		return ret;
	}

	/**
	 * Checks if the given type is valid, that is:
	 * It needs to have at least one character.
	 * The first character needs to be a letter, '<' or '>'.
	 * The other characters need to be letters, '<', '>' or digits.
	 * 
	 * The return value is a string indicating what is wrong
	 * or just a space if everything is alright.
	 */
	private String checkType(final String type){
		String ret;
		if (type.length() <= 0) {
			ret = "Enter a Type!";
		}else {
			if (java.lang.Character.isDigit(type.charAt(0))) {
				ret = "Enter a valid Type!";
			} else {
				ret = " ";
				for(int i = 0; i < type.length();i++){
					if(!(java.lang.Character.isLetter(type.charAt(i)) || java.lang.Character.isDigit(type.charAt(i)) || type.charAt(i) == '<' || type.charAt(i) == '>'/* || type.charAt(i) == '_'*/)){
						ret = "Enter a valid Type!";
						break;
					}
				}
			}
		}
		return ret;
	}
	
// 	private String checkRef(String name){
//		if (name.length() <= 1) {
//			 return "Enter a default value!";
//		}else {
//			for(int i = 0; i < name.length();i++){
//				if(java.lang.Character.isDigit(name.charAt(0)) ||
//				   !(java.lang.Character.isLetter(name.charAt(i)) ||
//				   java.lang.Character.isDigit(name.charAt(i)) ||
//				   name.charAt(i) == '_')){
//					return "Enter a valid default value!";
//				}
//			}
//		}
//		return " ";
//	}
}
