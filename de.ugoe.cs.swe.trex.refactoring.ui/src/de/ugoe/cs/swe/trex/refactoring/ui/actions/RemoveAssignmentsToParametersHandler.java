package de.ugoe.cs.swe.trex.refactoring.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Display;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3RemoveAssignmentsToParametersProcessor;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3RemoveAssignmentsToParametersRefactoring;
import de.ugoe.cs.swe.trex.refactoring.ui.TTCN3RemoveAssignmentsToParametersRefactoringWizard;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;

public class RemoveAssignmentsToParametersHandler extends RefactoringHandler {

	public void run(final IAction action) {
		if (workbenchHasUnsavedEditors()) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"RemoveAssignmentsToParameters Refactoring",
					"All modified files must be saved before proceeding...");
			return;
		}
			
		final TTCN3RemoveAssignmentsToParametersProcessor processor = new TTCN3RemoveAssignmentsToParametersProcessor(getIdentifierNodeFromEditorOffset(editor));
		final TTCN3RemoveAssignmentsToParametersRefactoring refactoring = new TTCN3RemoveAssignmentsToParametersRefactoring(processor);
		final TTCN3RemoveAssignmentsToParametersRefactoringWizard wizard = new TTCN3RemoveAssignmentsToParametersRefactoringWizard(
				refactoring, RefactoringWizard.WIZARD_BASED_USER_INTERFACE);
		final RefactoringWizardOpenOperation openOperation = new RefactoringWizardOpenOperation(
				wizard);
		try {
			openOperation.run(Display.getCurrent().getActiveShell(),
					"Refactoring not possible!");
		} catch (InterruptedException e) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Move Module Constant To Component Refactoring",
					"Error while applying refactoring to workbench/wizard: "
							+ e.getMessage());
			e.printStackTrace();
		}
		
	}

	// ---------------------------------------------------------------------------

	private LocationAST getIdentifierNodeFromEditorOffset(final TTCN3Editor editor) {
		final Integer offset = editor.getCaretOffset();
		if (offset == null){
			return null;
		}

		final IdentifierCache identifierCache = editor.getReconcilingStrategy()
				.getIdentifierCache();
		return (identifierCache == null) ? null : identifierCache.get(offset);
	}
}
