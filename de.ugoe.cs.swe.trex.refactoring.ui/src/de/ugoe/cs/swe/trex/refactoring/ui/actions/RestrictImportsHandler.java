package de.ugoe.cs.swe.trex.refactoring.ui.actions;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.PerformChangeOperation;
import org.eclipse.ltk.core.refactoring.RefactoringCore;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.swt.widgets.Display;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3RestrictImportsProcessor;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3RestrictImportsRefactoring;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;

public class RestrictImportsHandler extends RefactoringHandler {
	

	private TTCN3RestrictImportsProcessor processor;

	public void run(final IAction action) {
		if (workbenchHasUnsavedEditors()) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Restrict Imports Refactoring",
					"All modified files must be saved before proceeding...");
			return;
		}
		final LocationAST node = getIdentifierNodeFromEditorOffset(editor);
		

			processor = new TTCN3RestrictImportsProcessor(
								node, editor);
			final TTCN3RestrictImportsRefactoring refactoring = new TTCN3RestrictImportsRefactoring(
					processor);
			try {
				performRefactoring(refactoring);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	// ---------------------------------------------------------------------------

	private LocationAST getIdentifierNodeFromEditorOffset(final TTCN3Editor editor) {
		final Integer offset = editor.getCaretOffset();
		if (offset == null){
			return null;
		}
		
		final IdentifierCache identifierCache = editor.getReconcilingStrategy()
				.getIdentifierCache();
		if (identifierCache != null){
			return identifierCache.get(offset);
		}
		return null;
	}
	
	public void performRefactoring(final TTCN3RestrictImportsRefactoring ref) throws Exception {
		final NullProgressMonitor nullProgressMon = new NullProgressMonitor();
		final RefactoringStatus status = ref.checkAllConditions(nullProgressMon);
		if (status.isOK()) {
			final Change change = ref.createChange(nullProgressMon);
			change.initializeValidationData(nullProgressMon);
			change.setEnabled(true);
			final PerformChangeOperation operation = new PerformChangeOperation(change);
			operation.setUndoManager(RefactoringCore.getUndoManager(), processor.getProcessorName());
			operation.run(nullProgressMon);
			change.dispose();
		} else {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Restrict Imports Refactoring",
					status.getMessageMatchingSeverity(status.getSeverity()));
		}
	}
}
