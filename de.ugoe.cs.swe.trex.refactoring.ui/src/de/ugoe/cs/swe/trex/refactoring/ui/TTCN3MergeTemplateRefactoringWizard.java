package de.ugoe.cs.swe.trex.refactoring.ui;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.swt.widgets.Composite;

import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3MergeTemplateRefactoring;


public class TTCN3MergeTemplateRefactoringWizard extends RefactoringWizard {

	private MergeTemplateWizardPage wizardPage;
	private String renameText = "";

	// ---------------------------------------------------------------------------
	
	public TTCN3MergeTemplateRefactoringWizard(final Refactoring refactoring, final int flags) {
		super(refactoring, flags);
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void addUserInputPages() {
		wizardPage = new MergeTemplateWizardPage("Refactoring");
		wizardPage.setRenameText(renameText);
		addPage(wizardPage);
	}
	
	// ---------------------------------------------------------------------------	
	
	public void setRenameText(final String newtext) {
		renameText = newtext;
		if (wizardPage != null) {
			wizardPage.setRenameText(newtext);
		}
	}
	
	// ---------------------------------------------------------------------------
	
	@Override
	public boolean performFinish() {
		return super.performFinish();
	}

	// ---------------------------------------------------------------------------

	static class MergeTemplateWizardPage extends UserInputWizardPage {

		private MergeTemplatePageComposite composite;
		private String renameText;

		public MergeTemplateWizardPage(final String name) {
			super(name);
			setTitle("Merge Template Refactoring");
			setDescription("Automated Refactoring to merge template(s) into parameterised versions.");
		}		
		
		public void createControl(final Composite parent) {
			composite = new MergeTemplatePageComposite(parent, 0);
			composite.getRenameText().setText(renameText);
			composite.getRenameText().selectAll();
			setControl(composite);
		}
		
		public String getRenameText() {
			return composite.getRenameText().getText();
		}
		
		public void setRenameText(final String newtext) {
			renameText = newtext;
			if (composite != null) {
				composite.getRenameText().setText(renameText);
			}
		}		
		
		@Override
		public IWizardPage getNextPage() {
			final TTCN3MergeTemplateRefactoring refactoring = (TTCN3MergeTemplateRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setRenameText(getRenameText());
				refactoring.setPreserveOriginals(composite.getPreserveOriginalsButton().getSelection());
			}
			return super.getNextPage();
		}
		
		@Override
		protected boolean performFinish() {
			final TTCN3MergeTemplateRefactoring refactoring = (TTCN3MergeTemplateRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setRenameText(getRenameText());
				refactoring.setPreserveOriginals(composite.getPreserveOriginalsButton().getSelection());
			}
			return super.performFinish();
		}

	}
	
}
