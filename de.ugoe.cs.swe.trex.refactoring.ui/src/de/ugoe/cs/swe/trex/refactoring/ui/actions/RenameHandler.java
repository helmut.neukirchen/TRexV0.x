package de.ugoe.cs.swe.trex.refactoring.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Display;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3RenameProcessor;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3RenameRefactoring;
import de.ugoe.cs.swe.trex.refactoring.ui.TTCN3RenameRefactoringWizard;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;


public class RenameHandler extends RefactoringHandler {
	
	public void run(final IAction action) {
		if (workbenchHasUnsavedEditors()) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Rename Refactoring",
					"All modified files must be saved before proceeding...");
			return;
		}

		final TTCN3RenameProcessor processor = new TTCN3RenameProcessor(
				getIdentifierNodeFromEditorOffset(editor));
		final TTCN3RenameRefactoring refactoring = new TTCN3RenameRefactoring(
				processor);
		final TTCN3RenameRefactoringWizard wizard = new TTCN3RenameRefactoringWizard(
				refactoring, RefactoringWizard.WIZARD_BASED_USER_INTERFACE);
		wizard.setRenameText(processor.getCurrentName());
		final RefactoringWizardOpenOperation openOperation = new RefactoringWizardOpenOperation(
				wizard);
		try {
			openOperation.run(Display.getCurrent().getActiveShell(),
					"Refactoring not possible!");
			// IFile ff = (IFile)
			// editor.getEditorInput().getAdapter(IFile.class);
			// editor.getReconcilingStrategy().analyzeAll(
			// processor.getModifiedFiles(), ff);
		} catch (InterruptedException e) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Rename Refactoring",
					"Error while applying refactoring to workbench/wizard: "
							+ e.getMessage());
			//e.printStackTrace();
		}

	}

	// ---------------------------------------------------------------------------

	final private LocationAST getIdentifierNodeFromEditorOffset(final TTCN3Editor editor) {
		final Integer offset = editor.getCaretOffset();
		
		LocationAST result = null;
		
		if (offset != null){
			final IdentifierCache identifierCache = editor.getReconcilingStrategy().getIdentifierCache();
			if(identifierCache != null){
				result = identifierCache.get(offset);
			}
		}		
		return result;


		
		//return (identifierCache == null) ? null : identifierCache.get(offset);
	}

}
