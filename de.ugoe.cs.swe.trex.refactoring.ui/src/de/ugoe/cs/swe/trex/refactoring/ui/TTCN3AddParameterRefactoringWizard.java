package de.ugoe.cs.swe.trex.refactoring.ui;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.swt.widgets.Composite;

import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3AddParameterRefactoring;


public class TTCN3AddParameterRefactoringWizard extends RefactoringWizard {

	public TTCN3AddParameterRefactoringWizard(final Refactoring refactoring, final int flags) {
		super(refactoring, flags);
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void addUserInputPages() {
		final ParameterInputWizardPage wizardPage = new ParameterInputWizardPage("Refactoring");
		addPage(wizardPage);
	}

	// ---------------------------------------------------------------------------

	static class ParameterInputWizardPage extends UserInputWizardPage {

		private AddParameterPageComposite composite;
		
		public ParameterInputWizardPage(final String name) {
			super(name);
			setTitle("Add Parameter Refactoring");
			setDescription("Automated Refactoring to add parameters to testcases, functions and altsteps.");
		}

		public void createControl(final Composite parent) {
			final TTCN3AddParameterRefactoring refactoring = (TTCN3AddParameterRefactoring) getRefactoring();
				composite = new AddParameterPageComposite(parent, 0, refactoring.getPresentParamNames());
				setControl(composite);
		}
		
		public String getParamName(final int number) {
			return composite.getParamNames(number);
		}
		
		public String getParamType(final int number) {
			return composite.getParamTypes(number);
		}
		
		public String getParamRef(final int number) {
			return composite.getParamRefs(number);
		}
		
		public int getParamCount() {
			return composite.getParamCount();
		}

		@Override
		public IWizardPage getNextPage() {
			final TTCN3AddParameterRefactoring refactoring = (TTCN3AddParameterRefactoring) getRefactoring();
			if (refactoring != null) {
				composite.createParameters();
				refactoring.setParamCount(getParamCount());
				for(int i=0;i<refactoring.getParamCount();i++){
					refactoring.setParamType(getParamType(i), i);
					refactoring.setParamName(getParamName(i), i);
					refactoring.setParamRef(getParamRef(i), i);
				}
			}
			return super.getNextPage();
		}
		
		@Override
		protected boolean performFinish() {
			final TTCN3AddParameterRefactoring refactoring = (TTCN3AddParameterRefactoring) getRefactoring();
			if (refactoring != null) {
				composite.createParameters();
				refactoring.setParamCount(getParamCount());
				for(int i=0;i<refactoring.getParamCount();i++){
					refactoring.setParamType(getParamType(i), i);
					refactoring.setParamName(getParamName(i), i);
				}
			}

			return super.performFinish();
		}
	}
}
