package de.ugoe.cs.swe.trex.refactoring.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class InlineTemplatePageComposite extends Composite {

	/**
	 * This is the code formatter button.
	 */
	final private Button codeFormatterButton;

	/**
	 * This is the remove declaration if it becomes unreferenced button.
	 */
	final private Button removeDeclarationButton;

	public InlineTemplatePageComposite(final Composite parent, final int style) {
		super(parent, style);
		setLayout(new GridLayout());

		removeDeclarationButton = new Button(this, SWT.CHECK);
		removeDeclarationButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, true, false));
		removeDeclarationButton.setText("Remove declaration if it becomes unreferenced");
		removeDeclarationButton.setSelection(true);

		codeFormatterButton = new Button(this, SWT.CHECK);
		codeFormatterButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, true, false));
		codeFormatterButton.setText("Use code formatter on inlined template");
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * getter for the remove declaration button
	 */
	public Button getRemoveDeclarationButton() {
		return removeDeclarationButton;
	}

	/**
	 * getter for the code formatter button
	 */
	public Button getCodeFormatterButton() {
		return codeFormatterButton;
	}

}
