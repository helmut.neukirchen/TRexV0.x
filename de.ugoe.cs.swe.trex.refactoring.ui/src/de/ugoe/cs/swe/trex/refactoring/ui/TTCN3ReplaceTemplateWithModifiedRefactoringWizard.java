package de.ugoe.cs.swe.trex.refactoring.ui;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.swt.widgets.Composite;

//import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3ReplaceTemplateWithModifiedRefactoring;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;



public class TTCN3ReplaceTemplateWithModifiedRefactoringWizard extends RefactoringWizard {
	private RenameInputWizardPage wizardPage;
	private int limit;
	private boolean a,b,c;
	// ---------------------------------------------------------------------------

	public TTCN3ReplaceTemplateWithModifiedRefactoringWizard(final Refactoring refactoring, final int flags, final TTCN3Editor test) {
		super(refactoring, flags);
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void addUserInputPages() {
		wizardPage = new RenameInputWizardPage("Refactoring");
		wizardPage.setLimit(limit);                                                                                                    // !
		wizardPage.setRestrictToCurrentModule(a);
		wizardPage.setLocateBestInAllModules(b);
		wizardPage.setLocateBestInCurrentModule(c);
		addPage(wizardPage);
	}

	// ---------------------------------------------------------------------------


	
	public void setLimit(final int s)
	{
		limit = s;
		if (wizardPage != null) {
			wizardPage.setLimit(s);
		}
	}
	

	public void setRestrictToCurrentModule(final boolean resttomod)
	{
		this.a = resttomod;
		if (wizardPage != null) {
			wizardPage.setRestrictToCurrentModule(resttomod);
		}
	}
	public void setLocateBestInAllModules(final boolean locatebest)
	{
		this.b = locatebest;
		if (wizardPage != null) {
			wizardPage.setLocateBestInAllModules(locatebest);
		}
	}
	public void setLocateBestInCurrentModule(final boolean locatebest)
	{
		this.c = locatebest;
		if (wizardPage != null) {
			wizardPage.setLocateBestInCurrentModule(locatebest);
		}
	}

	// ---------------------------------------------------------------------------

	static class RenameInputWizardPage extends UserInputWizardPage {

		private ReplacePageComposite composite;
		private int limit;
		private boolean restrictToCurrentModule, locateBestInAllModules, locateBestInCurrentModule;
		
		public RenameInputWizardPage(final String name) {
			super(name);
			setTitle("Replace Templates With Modified Template Refactoring");
			setDescription("Automated Refactoring to replace templates of the same record-type\n"
					+ "with modifications of a template having the most in common with the others.");              //  +++++++++++++++++++update!!!!!
		}

		public void createControl(final Composite parent) {
			composite = new ReplacePageComposite(parent, 0);
			setLimit(limit);
			setRestrictToCurrentModule(restrictToCurrentModule);
			setLocateBestInAllModules(locateBestInAllModules);
			setLocateBestInCurrentModule(locateBestInCurrentModule);
			setControl(composite);
		}
	
		
		
		public int getLimit(){
			return composite.getSpinner().getSelection();
		}
		

		public void setLimit(final int s) {
			limit = s;
			if (composite != null) {
				composite.getSpinner().setSelection(s);
			}
			
			
		}
		
		public void setRestrictToCurrentModule(final boolean restmod)
		{
			this.restrictToCurrentModule = restmod;
			if (composite != null) {
				composite.getRestrictToCurrentModule().setSelection(restmod);
			}
		}
		public void setLocateBestInAllModules(final boolean locatebest)
		{
			this.locateBestInAllModules = locatebest;
			if (composite != null) {
				composite.getLocateBestInAllModules().setSelection(locatebest);
			}
		}
		public void setLocateBestInCurrentModule(final boolean locatebest)
		{
			this.locateBestInCurrentModule = locatebest;
			if (composite != null) {
				composite.getLocateBestInCurrentModule().setSelection(locatebest);
			}
		}
		@Override
		public IWizardPage getNextPage() {
			final TTCN3ReplaceTemplateWithModifiedRefactoring refactoring = (TTCN3ReplaceTemplateWithModifiedRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setLimit(getLimit());
				refactoring.setRestrictToThisModule(composite.getRestrictToCurrentModule().getSelection());
				refactoring.setLocateInAllModules(composite.getLocateBestInAllModules().getSelection());
				refactoring.setLocateInThisModule(composite.getLocateBestInCurrentModule().getSelection());
			}
			return super.getNextPage();
		}
		
		
		
		@Override
		protected boolean performFinish() {
			final TTCN3ReplaceTemplateWithModifiedRefactoring refactoring = (TTCN3ReplaceTemplateWithModifiedRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setLimit(getLimit());
				refactoring.setRestrictToThisModule(composite.getRestrictToCurrentModule().getSelection());
				refactoring.setLocateInAllModules(composite.getLocateBestInAllModules().getSelection());
				refactoring.setLocateInThisModule(composite.getLocateBestInCurrentModule().getSelection());
			}

			return super.performFinish();
		}
	}

}
