package de.ugoe.cs.swe.trex.refactoring.ui.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Display;

import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3InlineTemplateProcessor;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3InlineTemplateRefactoring;
import de.ugoe.cs.swe.trex.refactoring.ui.TTCN3InlineTemplateRefactoringWizard;

public class InlineTemplateHandler extends RefactoringHandler {

	public void run(final IAction action) {
		if (workbenchHasUnsavedEditors()) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Inline Template Refactoring",
					"All modified files must be saved before proceeding...");
			return;
		}

		final TTCN3InlineTemplateProcessor processor = new TTCN3InlineTemplateProcessor(
				editor);
		final TTCN3InlineTemplateRefactoring refactoring = new TTCN3InlineTemplateRefactoring(
				processor);
		final TTCN3InlineTemplateRefactoringWizard wizard = new TTCN3InlineTemplateRefactoringWizard(
				refactoring, RefactoringWizard.WIZARD_BASED_USER_INTERFACE);

		final RefactoringWizardOpenOperation openOperation = new RefactoringWizardOpenOperation(
				wizard);
		String titleFailedChecks = "";
		try {
			final int ran = openOperation.run(Display.getCurrent().getActiveShell(),
					titleFailedChecks);
			// don't re-analyze if it was cancelled, only if 'OK'
			if (ran == IDialogConstants.OK_ID) {
				final IFile ffile = (IFile) editor.getEditorInput().getAdapter(
						IFile.class);
				// only re-analyze modified files
				editor.getReconcilingStrategy().analyzeAll(
						processor.getModifiedFiles(), ffile);
			}
		} catch (InterruptedException e) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Inline Template Refactoring",
					"Error while applying refactoring to workbench/wizard: "
							+ e.getMessage());
			e.printStackTrace();
		}

	}
}
