package de.ugoe.cs.swe.trex.refactoring.ui;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.swt.widgets.Composite;

import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3InlineTemplateRefactoring;


public class TTCN3InlineTemplateRefactoringWizard extends RefactoringWizard {

	// ---------------------------------------------------------------------------

	public TTCN3InlineTemplateRefactoringWizard(final Refactoring refactoring, final int flags) {
		super(refactoring, flags);
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void addUserInputPages() {
		final InlineTemplateWizardPage wizardPage = new InlineTemplateWizardPage("Refactoring");
		addPage(wizardPage);
	}
	
	// ---------------------------------------------------------------------------
	
	@Override
	public boolean performFinish() {
		return super.performFinish();
	}

	// ---------------------------------------------------------------------------

	static class InlineTemplateWizardPage extends UserInputWizardPage {

		private InlineTemplatePageComposite composite;

		public InlineTemplateWizardPage(final String name) {
			super(name);
			setTitle("Inline Template Refactoring");
			setDescription("Automated Refactoring to inline templates");
		}
		
		public void createControl(final Composite parent) {
			composite = new InlineTemplatePageComposite(parent, 0);
			setControl(composite);
		}
		
		@Override
		public IWizardPage getNextPage() {
			final TTCN3InlineTemplateRefactoring refactoring = (TTCN3InlineTemplateRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setDeleteDeclaration(composite.getRemoveDeclarationButton().getSelection());
				refactoring.setPrettyPrintInlinedTemplates(composite.getCodeFormatterButton().getSelection());
			}
			return super.getNextPage();
		}
		
		@Override
		protected boolean performFinish() {
			final TTCN3InlineTemplateRefactoring refactoring = (TTCN3InlineTemplateRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setDeleteDeclaration(composite.getRemoveDeclarationButton().getSelection());
				refactoring.setPrettyPrintInlinedTemplates(composite.getCodeFormatterButton().getSelection());
			}
			return super.performFinish();
		}

	}
	
}
