package de.ugoe.cs.swe.trex.refactoring.ui;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.swt.widgets.Composite;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3ParameterizeTemplateRefactoring;


public class TTCN3ParameterizeTemplateRefactoringWizard extends RefactoringWizard{

	final private String renameText = "";
	
	public TTCN3ParameterizeTemplateRefactoringWizard(final Refactoring refactoring, final int flags) {
		super(refactoring, flags);
	}
	
	protected void addUserInputPages() {
		final ParameterizeTemplateWizardPage wizardPage = new ParameterizeTemplateWizardPage("Refactoring");
		wizardPage.setRenameText(renameText);
		addPage(wizardPage);
		
	}
	
	// ---------------------------------------------------------------------------

	static class ParameterizeTemplateWizardPage extends UserInputWizardPage {

		private ParameterizeTemplatePageComposite composite;
		private String renameText;

		public ParameterizeTemplateWizardPage(final String name) {
			super(name);
			setTitle("Parameterize Template Refactoring");
			setDescription("Automated Refactoring to parameterize template .");
		}
				
		public void createControl(final Composite parent) {
			composite = new ParameterizeTemplatePageComposite(parent, 0);
			composite.getRenameText().setText(renameText);
			composite.getRenameText().selectAll();
			setControl(composite);
		}
		
		public String getRenameText() {
			return composite.getRenameText().getText();
		}
		
		public void setRenameText(final String newtext) {
			renameText = newtext;
			if (composite != null) {
				composite.getRenameText().setText(renameText);
			}
		}		
		
		@Override
		public IWizardPage getNextPage() {
			TTCN3ParameterizeTemplateRefactoring refactoring = (TTCN3ParameterizeTemplateRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setRenameText(getRenameText());
				refactoring.setPreserveSourceTemplates(composite.getPreserveOriginalsButton().getSelection());
			}
			return super.getNextPage();
		}
		
		@Override
		protected boolean performFinish() {
			TTCN3ParameterizeTemplateRefactoring refactoring = (TTCN3ParameterizeTemplateRefactoring) getRefactoring();
			if (refactoring != null) {
				refactoring.setRenameText(getRenameText());
				refactoring.setPreserveSourceTemplates(composite.getPreserveOriginalsButton().getSelection());
			}
			return super.performFinish();
		} 
		 

		

	}

}
