package de.ugoe.cs.swe.trex.refactoring.ui.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Display;

import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3ReplaceTemplateWithModifiedProcessor;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3ReplaceTemplateWithModifiedRefactoring;
import de.ugoe.cs.swe.trex.refactoring.ui.TTCN3ReplaceTemplateWithModifiedRefactoringWizard;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;

public class ReplaceTemplateWithModifiedHandler extends RefactoringHandler {

	public void run(final IAction action) {
		if (workbenchHasUnsavedEditors()) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Replace Template With Modified Template Refactoring",
					"All modified files must be saved before proceeding...");
			return;
		}

		final TTCN3ReplaceTemplateWithModifiedProcessor processor = new TTCN3ReplaceTemplateWithModifiedProcessor(
				TTCN3Editor.getEditorAST(editor));
		final TTCN3ReplaceTemplateWithModifiedRefactoring refactoring = new TTCN3ReplaceTemplateWithModifiedRefactoring(
				processor);
		final TTCN3ReplaceTemplateWithModifiedRefactoringWizard wizard = new TTCN3ReplaceTemplateWithModifiedRefactoringWizard(
				refactoring, RefactoringWizard.WIZARD_BASED_USER_INTERFACE, editor);
		
		
		wizard.setLimit(75);
		wizard.setRestrictToCurrentModule(false);
		wizard.setLocateBestInAllModules(false);
		wizard.setLocateBestInCurrentModule(false);
		
		final RefactoringWizardOpenOperation openOperation = new RefactoringWizardOpenOperation(
				wizard);
		
		
		
		final String titleFailedChecks = "";
		try {
			final int ran = openOperation.run(Display.getCurrent().getActiveShell(),
					titleFailedChecks);
			// don't re-analyze if it was cancelled, only if 'OK'
			if (ran == IDialogConstants.OK_ID) {
				final IFile ffile = (IFile) editor.getEditorInput().getAdapter(
						IFile.class);
				// only re-analyze modified files
				editor.getReconcilingStrategy().analyzeAll(
						processor.getModifiedFiles(), ffile);
			}
		} catch (InterruptedException e) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Replace Templates With Modified Template Refactoring",
					"Error while applying refactoring to workbench/wizard: "
							+ e.getMessage());
			e.printStackTrace();
		}
	}
}
