package de.ugoe.cs.swe.trex.refactoring.ui.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Display;

import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3InlineParamProcessor;
import de.ugoe.cs.swe.trex.core.refactoring.core.TTCN3InlineParamRefactoring;
import de.ugoe.cs.swe.trex.refactoring.ui.TTCN3InlineParamRefactoringWizard;

public class InlineParamHandler extends RefactoringHandler {

	public void run(final IAction action) {
		if (workbenchHasUnsavedEditors()) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Inline Parameters Refactoring",
					"All modified files must be saved before proceeding...");
			return;
		}

		final TTCN3InlineParamProcessor processor = new TTCN3InlineParamProcessor(
				editor);
		final TTCN3InlineParamRefactoring refactoring = new TTCN3InlineParamRefactoring(
				processor);
		final TTCN3InlineParamRefactoringWizard wizard = new TTCN3InlineParamRefactoringWizard(
				refactoring, RefactoringWizard.WIZARD_BASED_USER_INTERFACE);

		final RefactoringWizardOpenOperation openOperation = new RefactoringWizardOpenOperation(
				wizard);
		String titleFailedChecks = "";
		try {
			final int ran = openOperation.run(Display.getCurrent().getActiveShell(),
					titleFailedChecks);
			// don't re-analyze if it was cancelled, only if 'OK'
			if (ran == IDialogConstants.OK_ID) {
				final IFile ffile = (IFile) editor.getEditorInput().getAdapter(
						IFile.class);
				// only re-analyze modified files
				editor.getReconcilingStrategy().analyzeAll(
						processor.getModifiedFiles(), ffile);
			}
		} catch (InterruptedException e) {
			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(),
					"Inline Template Refactoring",
					"Error while applying refactoring to workbench/wizard: "
							+ e.getMessage());
			e.printStackTrace();
		}
	}
}
