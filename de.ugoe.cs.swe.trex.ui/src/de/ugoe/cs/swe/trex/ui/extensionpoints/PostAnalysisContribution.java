package de.ugoe.cs.swe.trex.ui.extensionpoints;

import java.util.Observable;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.ui.TTCN3ResourceHelper;

public abstract class PostAnalysisContribution extends Observable {

	public abstract void runPostAnalysisContribution();

	protected void runEachAnalyzer() {
		final TTCN3AnalyzerFlyweightFactory analyzerFactory = TTCN3AnalyzerFlyweightFactory
				.getInstance();

		IProject project = analyzerFactory.getLastProjectAnalyzed();

		IResourceVisitor visitor = new IResourceVisitor() {
			public boolean visit(IResource resource) {
				if (TTCN3ResourceHelper.isTTCN3File(resource)) {
					IFile file = (IFile) resource;
					TTCN3Analyzer analyzer = analyzerFactory.getTTCN3Analyzer(
							file, false);
					if ((analyzer != null) && analyzer.isAnalyzed())
						processAnalyzer(analyzer);
				}
				return true;
			}
		};

		try {
			project.accept(visitor);
		} catch (CoreException e) {
			e.printStackTrace();
		}

	}

	protected abstract void processAnalyzer(TTCN3Analyzer analyzer);

}
