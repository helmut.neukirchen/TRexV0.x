package de.ugoe.cs.swe.trex.ui.views.findreferences;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.text.AbstractTextSearchResult;
import org.eclipse.search.ui.text.IEditorMatchAdapter;
import org.eclipse.search.ui.text.IFileMatchAdapter;
import org.eclipse.search.ui.text.Match;
import org.eclipse.ui.IEditorPart;

import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;

/**
 * The search result class stores the final references found.
 * An instance of this class is automatically passed to the 
 * SearchResultPage view. This requires an extension point entry 
 * of type searchResultPage in the plugin.xml file
 */
public class TTCN3FindReferencesSearchResult extends AbstractTextSearchResult implements
		IEditorMatchAdapter, IFileMatchAdapter {
	
	
	// Instance to the SearchQuery which passes the data to our SearchResult
	private TTCN3FindReferencesSearchQuery query;
	
	// The resulting references map 
	private Map<String, List<ReferenceWithContext>> fReferences;

	/**
	 * Constructs a SearchResult object. 
	 * 
	 * @param query The SearchQuery instance used for this job
	 */
	public TTCN3FindReferencesSearchResult(TTCN3FindReferencesSearchQuery query) {
		this.query = query;
	}

	/**
	 * Returns the total number of references in all files. 
	 */
	private int getReferenceCount() {
		Map<String, List<ReferenceWithContext>> references = getReferences();

		// Make sure we have references
		if (references == null) {
			return 0;
		}

		Set<String> refsKeySet = references.keySet();
		int matchCount = 0;

		// Count references for each file
		for (String filename : refsKeySet) {
			List<ReferenceWithContext> refs = references.get(filename);
			matchCount += refs.size();
		}

		return matchCount;
	}

	/**
	 * Get the label text. This text is displayed in the header of
	 * the SearchResultPage view and informs the user about the number
	 * of occurrences found.
	 */
	public String getLabel() {
		LocationAST queryNode = query.getQueryNode();
		String queryName = queryNode.getText();
		Map<String, List<ReferenceWithContext>> references = getReferences();

		if (references != null)
			return "'" + queryName + "' - " + getReferenceCount()
					+ " references found in project";
		else
			return "Searching project for references of '" + queryName + "'";
	}

	public ISearchQuery getQuery() {
		return this.query;
	}

	@Override
	public IEditorMatchAdapter getEditorMatchAdapter() {
		return null;
	}

	@Override
	public IFileMatchAdapter getFileMatchAdapter() {
		return null;
	}

	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	public String getTooltip() {
		return null;
	}

	public Match[] computeContainedMatches(AbstractTextSearchResult result,
			IEditorPart editor) {
		return null;
	}

	public boolean isShownInEditor(Match match, IEditorPart editor) {
		return false;
	}

	public Match[] computeContainedMatches(AbstractTextSearchResult result,
			IFile file) {
		return null;
	}

	public IFile getFile(Object element) {
		return null;
	}

	public void setReferences(Map<String, List<ReferenceWithContext>> references) {
		fReferences = references;
	}

	public Map<String, List<ReferenceWithContext>> getReferences() {
		return fReferences;
	}
}
