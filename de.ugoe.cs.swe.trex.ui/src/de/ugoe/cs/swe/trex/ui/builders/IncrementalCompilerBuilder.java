package de.ugoe.cs.swe.trex.ui.builders;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.MessageConsole;

import de.ugoe.cs.swe.trex.ui.CompilerConstants;
import de.ugoe.cs.swe.trex.ui.ConsoleTools;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;
import de.ugoe.cs.swe.trex.ui.actions.ActionHelper;
import de.ugoe.cs.swe.trex.ui.handlers.CompileFileHandler;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;
import de.ugoe.cs.swe.trex.ui.extensionpoints.AbstractCompilerDelegate;

public class IncrementalCompilerBuilder extends IncrementalProjectBuilder {

	public static final String BUILDER_ID = "de.ugoe.cs.swe.trex.ui.compilerbuilder";

	// ---------------------------------------------------------------------------

	public IncrementalCompilerBuilder() {
	}

	// ---------------------------------------------------------------------------

	@Override
	protected IProject[] build(int kind, @SuppressWarnings("rawtypes") Map args, IProgressMonitor monitor)
			throws CoreException {

		if (!BuilderHelper.isBuilderEnabled(getProject(), BUILDER_ID)) {
			return null;
		}

		String activeCompiler = TTCN3UIPlugin.getDefault().getPreferenceStore()
				.getString(CompilerConstants.SELECTED_COMPILER);
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IExtensionPoint ep = reg
				.getExtensionPoint("de.ugoe.cs.swe.trex.ui.compiler");
		IExtension[] extensions = ep.getExtensions();
		boolean chosen = false;
		
		List<IFile> selectedFiles = getSelectedFiles();
		
		final MessageConsole myConsole = ConsoleTools.findConsole(IConsoleConstants.ID_CONSOLE_VIEW);
		ConsoleTools.showConsole(myConsole);

		for (int i = 0; i < extensions.length; i++) {
			IExtension ext = extensions[i];
			IConfigurationElement[] ce = ext.getConfigurationElements();

			for (int j = 0; j < ce.length; j++) {
				try {
					Object obj;
					String name = ce[j].getAttribute("name");
					obj = ce[j].createExecutableExtension("class");
					/* old code:
					 * 
					 * int compilerModePreference = TTCN3UIPlugin.getDefault().getPluginPreferences().getInt("compilerMode");
					 */
					//FIXME what is the default compilerMode??
					int compilerModePreference = Platform.getPreferencesService().getInt("de.ugoe.cs.swe.trex.ui","compilerMode",0,null);
					if ((obj instanceof AbstractCompilerDelegate)
							&& (name.equals(activeCompiler))) {
						AbstractCompilerDelegate delegate = (AbstractCompilerDelegate) obj;						
						delegate.compile(selectedFiles, compilerModePreference, monitor);
						chosen = true;
					} /*else if (((obj instanceof AbstractCompilerDelegate) && activeCompiler == null) && (!chosen)) {
						AbstractCompilerDelegate delegate = (AbstractCompilerDelegate) obj;
						delegate.compile(selectedFiles, compilerModePreference, monitor);
						chosen = true;
					}*/
				} catch (CoreException e) {
					return null;
				}
			}

		}
		if (!chosen) {
			ConsoleTools.getErrorMessageConsoleStream().println("External Compiler: no compiler selected!");
		}
		return null;
	}

	// ---------------------------------------------------------------------------

	private List<IFile> getSelectedFiles() {
		CompilationCandidatesFinder finder = new CompilationCandidatesFinder();
		if (getProject().isOpen()) {
			try {
				getProject().accept(finder);
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
		
		return finder.getSelectedFiles();
	}

	// ---------------------------------------------------------------------------

	public class CompilationCandidatesFinder implements IResourceVisitor {
		List<IFile> selectedFiles = new ArrayList<IFile>();
		boolean isCompileAllProjects = TTCN3UIPlugin.getDefault().getPreferenceStore().getBoolean(CompilerConstants.COMPILE_ALL_PROJECTS);

		public boolean visit(IResource resource) throws CoreException {
			if (resource instanceof IFile) {
				final IFile file = (IFile) resource;
				String property = file.getPersistentProperty(CompileFileHandler.QUALIFIED_NAME);
				if (property != null) {
					PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
						public void run() {
							TTCN3Editor editor = ActionHelper.getActiveEditor();
							IFile editorFile = null;
							if (editor != null)
							{
								editorFile = (IFile) editor.getEditorInput().getAdapter(IFile.class);
							}
							if ((isCompileAllProjects) || ((editor != null) && (file.getProject().equals(editorFile.getProject()) )))
							{
								selectedFiles.add(file);
							}
						}
					});
				}
			}
			return true;
		}

		List<IFile> getSelectedFiles() {
			return selectedFiles;
		}
	}
	
	// ---------------------------------------------------------------------------

	protected void clean(IProgressMonitor monitor) throws CoreException {
		super.clean(monitor);
		cleanGenPath(monitor);
	}

	// ---------------------------------------------------------------------------

	private void cleanGenPath(IProgressMonitor monitor) {
		String activeCompiler = TTCN3UIPlugin.getDefault().getPreferenceStore()
		.getString(CompilerConstants.SELECTED_COMPILER);
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IExtensionPoint ep = reg
				.getExtensionPoint("de.ugoe.cs.swe.trex.ui.compiler");
		IExtension[] extensions = ep.getExtensions();
		boolean chosen = false;
		
		for (int i = 0; i < extensions.length; i++) {
			IExtension ext = extensions[i];
			IConfigurationElement[] ce = ext.getConfigurationElements();

			for (int j = 0; j < ce.length; j++) {
				try {
					Object obj;
					String name = ce[j].getAttribute("name");
					obj = ce[j].createExecutableExtension("class");
					if ((obj instanceof AbstractCompilerDelegate)
							&& (name.equals(activeCompiler)) && (!chosen)) {
						AbstractCompilerDelegate delegate = (AbstractCompilerDelegate) obj;
						delegate.clean(monitor, getProject());
						chosen = true;
					} 
				} catch (CoreException e) {
				}
			}
		}
		
	}
	
}
