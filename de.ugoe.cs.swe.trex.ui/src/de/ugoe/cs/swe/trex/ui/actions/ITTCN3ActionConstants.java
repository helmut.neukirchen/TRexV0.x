package de.ugoe.cs.swe.trex.ui.actions;

import org.eclipse.ui.texteditor.ITextEditorActionConstants;

/**
 * Action IDs for standard actions, for groups in the menu bar, and for actions
 * in context menus of TTCN3 views.
 */
public interface ITTCN3ActionConstants extends ITextEditorActionConstants {

	/**
	 * Source menu: name of standard Comment global action (value "Comment").
	 */
	public static final String COMMENT = "Comment";

	/**
	 * Source menu: name of standard Uncomment global action (value
	 * "Uncomment").
	 */
	public static final String UNCOMMENT = "Uncomment";
	
	public static final String FOLDING_TOGGLE = "Enable Folding";
	
	public static final String FOLDING_EXPAND_ALL = "Expand All";
	
	public static final String FOLDING_COLLAPSE_ALL = "Collapse All";
}
