package de.ugoe.cs.swe.trex.ui.widgets;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class WorkspaceDirectorySelectionDialog extends Dialog {

	private IProject project;
	private TTCN3WorkspaceDirectorySelector selector;
	private String pathSelection;

	public WorkspaceDirectorySelectionDialog(Shell parentShell, IProject project, String pathSelection) {
		super(parentShell);
		this.project = project;
		this.pathSelection = pathSelection;
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Select a workspace directory...");
	}
	
	protected Control createDialogArea(Composite parent) {
		//Composite composite = (Composite) super.createDialogArea(parent);
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		composite.setLayout(layout);
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.minimumWidth = 500;
		gridData.minimumHeight = 300;
		composite.setLayoutData(gridData);
		
		selector = new TTCN3WorkspaceDirectorySelector(composite, SWT.NONE, project, pathSelection);
		selector.setLayoutData(new GridData(GridData.FILL_BOTH));

		return composite;
	}

	public String getSelection() {
		return selector.getSelection();
	}
	
}
