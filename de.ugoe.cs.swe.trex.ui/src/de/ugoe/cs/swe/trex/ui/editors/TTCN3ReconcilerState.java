package de.ugoe.cs.swe.trex.ui.editors;

import java.util.Observable;

public class TTCN3ReconcilerState extends Observable {

	private static TTCN3ReconcilerState instance = null;

	private TTCN3ReconcilingStrategy reconciler = null;

	public TTCN3ReconcilerState() {
	}

	public static TTCN3ReconcilerState getInstance() {
		if (instance == null)
			instance = new TTCN3ReconcilerState();

		return instance;
	}

	public void setReconcilerUpdated(TTCN3ReconcilingStrategy reconciler) {
		this.reconciler = reconciler;
		setChanged();
	}

	public TTCN3ReconcilingStrategy getReconciler() {
		return reconciler;
	}

	public void setReconciler(TTCN3ReconcilingStrategy reconciler) {
		this.reconciler = reconciler;
	}
	
	public void setChanged() {
		super.setChanged();
	}

}
