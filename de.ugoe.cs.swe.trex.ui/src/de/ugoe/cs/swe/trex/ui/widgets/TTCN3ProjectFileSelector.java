package de.ugoe.cs.swe.trex.ui.widgets;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import de.ugoe.cs.swe.trex.ui.TTCN3ResourceHelper;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPluginImages;

public class TTCN3ProjectFileSelector extends Composite {

	private IProject project;
	private CheckboxTreeViewer tv;

	public class ProjectResourceSelectorContentProvider implements
			ITreeContentProvider {

		public void dispose() {
		}

		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

		public Object[] getChildren(Object arg0) {
			ArrayList<IResource> result = new ArrayList<IResource>();
			if (arg0 instanceof IProject) {
				IProject currentProject = (IProject)arg0;
				try {
					IResource[] members = currentProject.members();
					for (int i=0; i < members.length; i++) {						
						if (TTCN3ResourceHelper.isTTCN3File(members[i]) || (members[i] instanceof IFolder)) {
							result.add(members[i]);
						}
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
			} else if (arg0 instanceof IFolder) {
				IFolder currentFolder = (IFolder)arg0;
				try {
					IResource[] members = currentFolder.members();
					for (int i=0; i < members.length; i++) {						
						if (TTCN3ResourceHelper.isTTCN3File(members[i]) || (members[i] instanceof IFolder)) {
							result.add(members[i]);
						}
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
				
			}
			return result.toArray(new IResource[0]);
		}

		public boolean hasChildren(Object arg0) {
			if (arg0 instanceof IProject) {
				try {
					return ((IProject)arg0).members().length > 0;
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
				
			if (arg0 instanceof IFolder) {
				try {
					return ((IFolder)arg0).members().length > 0;
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
			
			return false;
		}

		public Object[] getElements(Object arg0) {
			return getChildren(arg0);
		}

		public Object getParent(Object element) {
			if (element instanceof IResource)
				return ((IResource)element).getParent();
			return null;
		}

	}

	// ------------------------------------------------------------------------------------

	public class ProjectResourceSelectorLabelProvider implements ILabelProvider {

		public Image getImage(Object element) {
			if ((element instanceof IResource) && (TTCN3ResourceHelper.isTTCN3File((IResource) element))) {
				return TTCN3UIPluginImages.get(TTCN3UIPluginImages.IMG_TTCN3);
			} else if (element instanceof IFolder) {
				return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER);
			}
			return null;
		}

		public String getText(Object element) {
			if (element instanceof IResource)
				return ((IResource)element).getName();
			
			return "Unknown Element";
		}

		public void addListener(ILabelProviderListener listener) {
		}

		public void dispose() {
		}

		public boolean isLabelProperty(Object element, String property) {
			return false;
		}

		public void removeListener(ILabelProviderListener listener) {
		}
	}

	// ------------------------------------------------------------------------------------

	public TTCN3ProjectFileSelector(Composite parent, int style, IProject project) {
		super(parent, style);
		this.project = project;
		setLayout(new FillLayout());
		tv = new CheckboxTreeViewer(this);
		tv.setAutoExpandLevel(2);
		tv.setUseHashlookup(true);
		tv.getTree().setLayout(new FillLayout());
		tv.setContentProvider(new ProjectResourceSelectorContentProvider());
		tv.setLabelProvider(new ProjectResourceSelectorLabelProvider());
		tv.setInput(this.project);
		tv.addCheckStateListener(new ICheckStateListener() {
			public void checkStateChanged(CheckStateChangedEvent event) {
				if (event.getElement() instanceof IFolder) {
					tv.setSubtreeChecked((IFolder)event.getElement(), event.getChecked());
				}
			}
		});
		
	}

	public void uncheckAllResources() {
		for (int i=0; i < tv.getCheckedElements().length; i++) {
			IResource resource = (IResource)tv.getCheckedElements()[i]; 
			tv.setChecked(resource, false);
		}
	}
	
	public List<String> getSelectedResources() {
		List<String> result = new ArrayList<String>();
		for (int i=0; i < tv.getCheckedElements().length; i++) {
			IResource resource = (IResource)tv.getCheckedElements()[i]; 
			result.add(resource.getFullPath().toPortableString());
		}
		
		return result;
	}
	
	public void selectFiles(List<String> resources, IProject project) {
		for (int i=0; i < resources.size(); i++) {
			IResource resource = project.getWorkspace().getRoot().findMember(new Path(resources.get(i)));
			if (resource != null) {
				tv.setChecked(resource, true);
			}
		}
	}
	
}
