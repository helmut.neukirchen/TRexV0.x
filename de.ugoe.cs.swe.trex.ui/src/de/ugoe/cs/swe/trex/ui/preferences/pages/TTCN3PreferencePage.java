package de.ugoe.cs.swe.trex.ui.preferences.pages;

import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;

public class TTCN3PreferencePage
	extends PreferencePage
	implements IWorkbenchPreferencePage {

	public TTCN3PreferencePage() {
		super("TRex TTCN-3");
		setPreferenceStore(TTCN3UIPlugin.getDefault().getPreferenceStore());
        setDescription("TTCN-3 Refactoring and Metric Tools settings can be made on the sub-pages of the TRex TTCN-3 preferences.");
	}

	@Override
	protected Control createContents(Composite parent) {
		noDefaultAndApplyButton();
        Composite composite = new Composite(parent, SWT.FILL); 
		return composite;
	}

	public void init(IWorkbench workbench) {
		// do nothing
	}

}