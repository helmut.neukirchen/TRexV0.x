package de.ugoe.cs.swe.trex.ui.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.ITreeViewerListener;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3MultiPageEditor;


public class TTCN3ASTView extends ViewPart {
	TreeViewer viewer;

	LocationAST root = null;

	TTCN3MultiPageEditor editor = null;

	private ViewContentProvider ttcn3ASTViewContentProvider;

	private Display display;

	protected void updateTree(TreeItem[] children, List<LocationAST> nodes) {
		if (children != null) {
			for (TreeItem element : children) {
				updateColor(element, false, nodes);
			}
		}
	}

	private List<LocationAST> getNodesFromOffset(LocationAST t, int startOffset, int endOffset) {
		if (t == null)
			return null;
		
		List<LocationAST> result = new ArrayList<LocationAST>();
		
		if (t.getFirstChild() != null) {
			LocationAST node = t.getFirstChild();
			
			boolean lookedIntoSubtree = false;
			while ((node != null) && (node.getOffset() != -1) && (node.getEndOffset() != -1) ){
				if ((node.getOffset() == startOffset) && (endOffset == node.getEndOffset())) {
					result.add(node);
				} else if ((node.getOffset() <= startOffset) && (endOffset <= node.getEndOffset())) {
					result.addAll(getNodesFromOffset(node, startOffset, endOffset));
					if ((startOffset != node.getOffset()) && (endOffset != node.getEndOffset()))
						lookedIntoSubtree = true;
				} else if ((startOffset <= node.getOffset()) && (node.getEndOffset() <= endOffset)) {
					result.addAll(getNodeChildrenList(node));
				} 
				
				node = node.getNextSibling();
			}
			
			if (lookedIntoSubtree == false) {
				node = t.getFirstChild();
				while ((node != null) && (node.getOffset() != -1) && (node.getEndOffset() != -1)){
					if ((node.getOffset() <= startOffset)
							&& (endOffset <= node.getEndOffset())) {
						// ignore
					} else if ((startOffset <= node.getOffset())
							&& (node.getEndOffset() <= endOffset)) {
						// ignore
					} else {
						result.addAll(getNodesFromOffset(node, startOffset,
								endOffset));
					} 
					node = node.getNextSibling();
				}
			}
			
		} 
		
		return result;
	}
	
	private List<LocationAST> getNodeChildrenList(LocationAST t) {
		if (t == null)
			return null;
		List<LocationAST> result = new ArrayList<LocationAST>();
		
		result.add(t);
		if (t.getFirstChild() != null) {
			LocationAST node = t.getFirstChild();
			
			while (node != null) {
				result.addAll(getNodeChildrenList(node));
				result.add(node);
				node = node.getNextSibling();
			}
		}
		return result;
	}
	
	List<LocationAST> getMatchingNodes() {
		ITextSelection selection = (ITextSelection) editor.getFEditor()
				.getSelectionProvider().getSelection();

		if ((selection.getLength() <= 0) || (selection.getLength() > 1500))
			return null;
		
		String selectedText = selection.getText();
		if (selectedText.length() <= 0)
			return null;
		
		int startOffset = selection.getOffset();
		int endOffset = startOffset + selection.getLength();
		
		LocationAST rootNode = editor.getFEditor().getReconcilingStrategy().getRootNode();
		List<LocationAST> result = getNodesFromOffset(rootNode, startOffset, endOffset); 

		return result;
	}

	protected void updateColor(TreeItem item, boolean colorize,
			List<LocationAST> nodes) {
		boolean enableColor = colorize;

		if ((item.getData() instanceof LocationAST) && (enableColor == false)) {
			
			if (nodes != null) {
				LocationAST curItem = (LocationAST) item.getData();
				if (nodes.contains(curItem)) {
					enableColor = true;
				}
			}
		}

		if (enableColor) {
			Color red = display.getSystemColor(SWT.COLOR_BLUE);
			item.setForeground(red);
			item.setExpanded(true);
		} else {
			Color black = display.getSystemColor(SWT.COLOR_BLACK);
			item.setForeground(black);
		}

		TreeItem[] children = item.getItems();

		if (children == null)
			return;

		for (TreeItem element : children) {
			updateColor(element, enableColor, nodes);
		}
	}

	ISelectionListener listener = new ISelectionListener() {

		public void selectionChanged(IWorkbenchPart part, ISelection selection) {
			if (part instanceof TTCN3MultiPageEditor) {
				editor = (TTCN3MultiPageEditor) part;
				root = (LocationAST) editor.getFEditor()
						.getReconcilingStrategy().getRoot();
				viewer.getContentProvider().inputChanged(null, null, null);

				TreeItem[] children = viewer.getTree().getItems();
				List<LocationAST> nodes = getMatchingNodes();

				if (nodes != null) {
					viewer.collapseAll();
					viewer.refresh();
					
					for (int i=0; i < nodes.size(); i++) {
						LocationAST curNode = nodes.get(i);
						if (curNode.getParent().getParent() != null) {
							viewer.expandToLevel(curNode, AbstractTreeViewer.ALL_LEVELS);
						}
					}
					
					updateTree(children, nodes);
					viewer.refresh();
				}
			}
		}

	};

	class ViewContentProvider implements IStructuredContentProvider,
			ITreeContentProvider {
		private LocationAST treeRoot = null;

		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
			treeRoot = root;
		}

		public void dispose() {
			// do nothing
		}

		public Object[] getElements(Object parent) {
			if (parent == null || parent.equals(getViewSite())) {
				if (treeRoot == null)
					treeRoot = root;
				return getChildren(treeRoot);
			}
			return getChildren(parent);
		}

		public Object getParent(Object child) {
			if (!(child instanceof LocationAST))
				return null;
			
			LocationAST node = (LocationAST) child;
		
			return node.getParent();
		}

		public Object[] getChildren(Object parent) {
			if (parent == null)
				return new Object[0];

			if (!(parent instanceof LocationAST))
				return null;

			LocationAST node = (LocationAST) parent;

			final int numChildren = node.getNumberOfChildren();
			Object[] resultNodes = new Object[numChildren];

			if (numChildren > 0) {
				LocationAST currentNode = node.getFirstChild();

				for (int i = 0; i < numChildren; i++) {
					resultNodes[i] = currentNode;
					currentNode = currentNode.getNextSibling();
				}
			}

			return resultNodes;
		}

		public boolean hasChildren(Object parent) {
			if (parent != null && parent instanceof LocationAST) {
				LocationAST node = (LocationAST) parent;

				if (node.getFirstChild() != null)
					return true;
			}
			
			return false;
		}

	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		display = parent.getDisplay();
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		ttcn3ASTViewContentProvider = new ViewContentProvider();
		viewer.setContentProvider(ttcn3ASTViewContentProvider);
		viewer.setUseHashlookup(true);
		viewer.setInput(getViewSite());
		viewer.addTreeListener(new ITreeViewerListener() {
			public void treeCollapsed(TreeExpansionEvent event) {
				updateTree(viewer.getTree().getItems(), null);
			}

			public void treeExpanded(TreeExpansionEvent event) {
				updateTree(viewer.getTree().getItems(), null);
			}
		});

		getSite().getPage().addSelectionListener(listener);

		IEditorPart editorPart = getSite().getPage().getActiveEditor();
		if (editorPart instanceof TTCN3MultiPageEditor) {
			editor = (TTCN3MultiPageEditor) editorPart;
			root = (LocationAST) editor.getFEditor().getReconcilingStrategy()
					.getRoot();
			viewer.refresh();
		}
		
		
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	@Override
	public void dispose() {
		getSite().getPage().removeSelectionListener(listener);
	}
}