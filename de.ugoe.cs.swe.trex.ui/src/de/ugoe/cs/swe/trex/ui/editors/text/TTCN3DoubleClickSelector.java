package de.ugoe.cs.swe.trex.ui.editors.text;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.ITextViewer;

/**
 * Double click strategy aware of TTCN3 identifier syntax rules.
 * @author sukiyuki
 */
public class TTCN3DoubleClickSelector implements ITextDoubleClickStrategy {

	protected static final char[] BRACKETS= {'{', '}', '(', ')', '[', ']', '<', '>' };
	private final TTCN3PairMatcher fPairMatcher= new TTCN3PairMatcher(BRACKETS);

	public TTCN3DoubleClickSelector() {
		super();
	}

	/*
	 * @see org.eclipse.jface.text.ITextDoubleClickStrategy#doubleClicked(org.eclipse.jface.text.ITextViewer)
	 */
//	@Override
	public void doubleClicked(ITextViewer textViewer) {
		int offset= textViewer.getSelectedRange().x;

		if (offset < 0) {
			return;
		}

		IDocument document= textViewer.getDocument();

		IRegion region= fPairMatcher.match(document, offset);
		if (region != null && region.getLength() >= 2) {
			textViewer.setSelectedRange(region.getOffset() + 1, region.getLength() - 2);
		} else {
			region= selectWord(document, offset);
			if (region != null && region.getLength() > 0) {
				textViewer.setSelectedRange(region.getOffset(), region.getLength());
			}
		}
	}

	protected IRegion selectWord(IDocument document, int offset) {
		return TTCN3WordFinder.findWord(document, offset);
	}
}
