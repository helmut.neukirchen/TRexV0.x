package de.ugoe.cs.swe.trex.ui.views.findreferences;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.search.ui.text.Match;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;

/**
 * This class serves as the content provider for our SearchResultPage view
 * It implements the ITreeContentProvider class 
 * 
 */
public class TTCN3FindReferencesContentProvider implements ITreeContentProvider {

	public void dispose() {
	}

	/**
	 * Returns the elements from our SearchResult object which is an
	 * array of FileElements
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof TTCN3FindReferencesSearchResult)
			return ((TTCN3FindReferencesSearchResult)inputElement).getElements();
		return null;
	}

	/**
	 * Returns the child elements as an array. The kind of elements
	 * returned depends on the parentElement object´s class.
	 * Possible element types are FileElements, ModuleElements,
	 * CompoundElements and AtomicElements. In case we get a parent
	 * with AtomicElement as  input, we have no more child elements
	 * to return.
	 * 
	 * @param parentElement the parent element to return child objects for
	 * @returns an object array of elements or null if the parent element
	 * has no childs
	 */
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof FileElement) {
			List<Match> matches = new ArrayList<Match>();
			List<ModuleElement> modules = ((FileElement) parentElement)
					.getModules();
			for (ModuleElement module : modules) {
				matches.add(new Match(module, 0, 0));
			}
			return matches.toArray();
		} else if (parentElement instanceof Match) {
			Match match = (Match) parentElement;

			if (match.getElement() instanceof ModuleElement) {
				List<Match> matches = new ArrayList<Match>();
				List<CompoundElement> compoundsList = ((ModuleElement) match
						.getElement()).getCompounds();
				for (CompoundElement thisCompound : compoundsList) {
					matches.add(new Match(thisCompound, 0, 0));
				}
				return matches.toArray();
			} else if (match.getElement() instanceof CompoundElement) {
				List<Match> matches = new ArrayList<Match>();
				List<AtomicElement> atomicList = ((CompoundElement) match
						.getElement()).getAtomics();
				for (AtomicElement thisAtomic : atomicList) {
					LocationAST refNode = thisAtomic.getReference()
							.getReferenceNode();
					matches.add(new Match(thisAtomic, refNode.getOffset(),
							refNode.getEndOffset() - refNode.getOffset()));
				}
				return matches.toArray();
			}
			return null;
		}
		return null;
	}

	public Object getParent(Object element) {
		return null;
	}

	public boolean hasChildren(Object element) {
		if (element instanceof FileElement) {
			return true;
		}
		if (element instanceof Match) {
			Match match = (Match) element;
			if (match.getElement() instanceof ModuleElement) {
				return true;
			} else if (match.getElement() instanceof CompoundElement) {
				return true;
			}
			return false;
		}
		return false;
	}
}
