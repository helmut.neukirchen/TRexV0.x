package de.ugoe.cs.swe.trex.ui.builders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;

public class BuilderCommand {
	private ArrayList<String> command;

	private IProject project;

	private IFile file;

	private Map<String, String> environment = new HashMap<String, String>();
	
	public BuilderCommand(ArrayList<String> command) {
		this.command = command;
	}
	
	public BuilderCommand(ArrayList<String> command, IFile file, IProject project) {
		this.command = command;
		this.file = file;
		this.project = project;
	}
	
	public ArrayList<String> getCommand() {
		return command;
	}

	public void setCommand(ArrayList<String> command) {
		this.command = command;
	}

	public IFile getFile() {
		return file;
	}

	public void setFile(IFile file) {
		this.file = file;
	}

	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	public Map<String, String> getEnvironment() {
		return environment;
	}

	public void setEnvironment(Map<String, String> environment) {
		this.environment = environment;
	}
	
}
