package de.ugoe.cs.swe.trex.ui;

import java.util.ArrayList;

import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class ConsoleTools {
	private static MessageConsoleStream outStream;
	private static MessageConsoleStream externalStream;
	private static MessageConsoleStream errorStream;
	private static MessageConsoleStream successStream;

	// --------------------------------------------------------------------------

	public static MessageConsoleStream getMessageConsoleStream() {
		if (outStream == null) {
			MessageConsole myConsole = ConsoleTools.findConsole(IConsoleConstants.ID_CONSOLE_VIEW);
			outStream = myConsole.newMessageStream();
			setConsoleColor(outStream, SWT.COLOR_BLACK);
		}

		return outStream;
	}
	
	// --------------------------------------------------------------------------

	public static MessageConsoleStream getExternalMessageConsoleStream() {
		if (externalStream == null) {
			MessageConsole myConsole = ConsoleTools.findConsole(IConsoleConstants.ID_CONSOLE_VIEW);
			externalStream = myConsole.newMessageStream();
			setConsoleColor(externalStream, SWT.COLOR_BLUE);
		}

		return externalStream;
	}
	// --------------------------------------------------------------------------

	public static MessageConsoleStream getErrorMessageConsoleStream() {
		if (errorStream == null) {
			MessageConsole myConsole = ConsoleTools.findConsole(IConsoleConstants.ID_CONSOLE_VIEW);
			errorStream = myConsole.newMessageStream();
			setConsoleColor(errorStream, SWT.COLOR_RED);
		}
		
		return errorStream;
	}

	// --------------------------------------------------------------------------
	
	public static MessageConsoleStream getSuccessMessageConsoleStream() {
		if (successStream == null) {
			MessageConsole myConsole = ConsoleTools.findConsole(IConsoleConstants.ID_CONSOLE_VIEW);
			successStream = myConsole.newMessageStream();
			setConsoleColor(successStream, SWT.COLOR_GREEN);
		}
		
		return successStream;
	}
	
	// --------------------------------------------------------------------------

	private static void setConsoleColor(final MessageConsoleStream stream, final int col) {
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
			public void run() {
				stream.setColor(PlatformUI.getWorkbench().getDisplay().getSystemColor(col));
			}
		});
	}
	
	// ---------------------------------------------------------------------------

	public static void showConsole(final MessageConsole myConsole) {
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
			public void run() {
				try {
					IWorkbenchPage page = TTCN3UIPlugin.getActiveWorkbenchPage();
					String id = IConsoleConstants.ID_CONSOLE_VIEW;
					IConsoleView view;
					view = (IConsoleView) page.showView(id);
					view.display(myConsole);
				} catch (PartInitException e) {
					e.printStackTrace();
				}
			}			
		});
	}
	
	// ---------------------------------------------------------------------------

	public static MessageConsole findConsole(final String name) {
		final ArrayList<MessageConsole> consoles = new ArrayList<MessageConsole>();
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
			public void run() {
				ConsolePlugin plugin = ConsolePlugin.getDefault();
				IConsoleManager conMan = plugin.getConsoleManager();
				IConsole[] existing = conMan.getConsoles();
				for (int i = 0; i < existing.length; i++) {
					if (name.equals(existing[i].getName()))
						consoles.add((MessageConsole) existing[i]);
				}
				if (consoles.size() == 0) {
					MessageConsole myConsole = new MessageConsole(name, null);
					consoles.add(myConsole);
					conMan.addConsoles(new IConsole[] { myConsole });
				}
			}
		});
		if (consoles.size() == 1)
			return consoles.get(0);
		else
			return null;
	}
	
	// ---------------------------------------------------------------------------

	public static void verbosePrintln(String str) {
		if (Platform.getPreferencesService().getBoolean("de.ugoe.cs.swe.trex.ui",CompilerConstants.VERBOSE,false,null)){
			ConsoleTools.getMessageConsoleStream().println(str);
		}
		/* old code:
		 * 
		 * if (TTCN3UIPlugin.getDefault().getPluginPreferences().getBoolean(CompilerConstants.VERBOSE)) {
		 * 		ConsoleTools.getMessageConsoleStream().println(str);
		 * }
		*/
	}
	

}
