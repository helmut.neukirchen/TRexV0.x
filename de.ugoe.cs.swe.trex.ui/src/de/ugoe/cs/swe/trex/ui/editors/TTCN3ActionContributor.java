package de.ugoe.cs.swe.trex.ui.editors;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.SubStatusLineManager;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.ide.IDEActionFactory;
import org.eclipse.ui.part.MultiPageEditorActionBarContributor;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.StatusLineContributionItem;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;

public class TTCN3ActionContributor extends MultiPageEditorActionBarContributor {

	private StatusLineContributionItem statusLineLineNumberItem;

	public TTCN3ActionContributor() {
		super();
		createActions();
	}

	// ---------------------------------------------------------------------------

	private void createActions() {
		statusLineLineNumberItem = new StatusLineContributionItem(
				TTCN3UIPlugin.PLUGIN_ID + ".statusline.linenumber", true, 20);
		statusLineLineNumberItem.setText("");
	}

	// ---------------------------------------------------------------------------

	@Override
	public void setActivePage(IEditorPart activeEditor) {
		TTCN3Editor editor = null;
		if (activeEditor instanceof TTCN3Editor) {
			editor = (TTCN3Editor) activeEditor;
		}

		IActionBars actionBars = getActionBars();

		if ((actionBars == null) || (editor == null))
			return;

		actionBars.setGlobalActionHandler(ActionFactory.DELETE.getId(),
				getAction(editor, ActionFactory.DELETE.getId()));
		actionBars.setGlobalActionHandler(ActionFactory.UNDO.getId(),
				getAction(editor, ActionFactory.UNDO.getId()));
		actionBars.setGlobalActionHandler(ActionFactory.REDO.getId(),
				getAction(editor, ActionFactory.REDO.getId()));
		actionBars.setGlobalActionHandler(ActionFactory.CUT.getId(), getAction(
				editor, ActionFactory.CUT.getId()));
		actionBars.setGlobalActionHandler(ActionFactory.COPY.getId(),
				getAction(editor, ActionFactory.COPY.getId()));
		actionBars.setGlobalActionHandler(ActionFactory.PASTE.getId(),
				getAction(editor, ActionFactory.PASTE.getId()));
		actionBars.setGlobalActionHandler(ActionFactory.SELECT_ALL.getId(),
				getAction(editor, ActionFactory.SELECT_ALL.getId()));
		actionBars.setGlobalActionHandler(ActionFactory.FIND.getId(),
				getAction(editor, ActionFactory.FIND.getId()));
		actionBars.setGlobalActionHandler(IDEActionFactory.BOOKMARK.getId(),
				getAction(editor, IDEActionFactory.BOOKMARK.getId()));
		actionBars.setGlobalActionHandler(IDEActionFactory.ADD_TASK.getId(),
				getAction(editor, IDEActionFactory.ADD_TASK.getId()));

		actionBars.updateActionBars();
	}

	// //
	// ---------------------------------------------------------------------------
	//
	// public void contributeToMenu(IMenuManager menuManager) {
	// SubMenuManager subMenuManager = new SubMenuManager(menuManager);
	// }
	//
	// //
	// ---------------------------------------------------------------------------
	//
	// public void contributeToToolBar(IToolBarManager toolBarManager) {
	// SubToolBarManager subToolbarManager = new
	// SubToolBarManager(toolBarManager);
	// }
	//	
	// //
	// ---------------------------------------------------------------------------
	//
	@Override
	public void contributeToStatusLine(IStatusLineManager statusLineManager) {
		SubStatusLineManager subStatusLineManager = new SubStatusLineManager(
				statusLineManager);
		subStatusLineManager.add(statusLineLineNumberItem);
		subStatusLineManager.setVisible(true);
	}

	// ---------------------------------------------------------------------------

	protected IAction getAction(ITextEditor anEditor, String anActionID) {
		return (anEditor == null ? null : anEditor.getAction(anActionID));
	}
}
