package de.ugoe.cs.swe.trex.ui.editors.outline;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPluginImages;

/**
 * Renderer for the outline view
 */
public class TTCN3OutlineLabelProvider extends LabelProvider {

	/** {@inheritDoc} */
	@Override
	public Image getImage(Object anElement) {
		ImageDescriptor descriptor = null;
		if (!(anElement instanceof AST))
			return null;

		final AST ast = (AST) anElement;
		if (ast.getType() == TTCN3ParserTokenTypes.ModuleDefinition) {
			switch (ast.getFirstChild().getType()) {

			case TTCN3ParserTokenTypes.AltstepDef:
				descriptor = TTCN3UIPluginImages.DESC_ALTSTEP;
				break;

			case TTCN3ParserTokenTypes.ComponentDef:
				descriptor = TTCN3UIPluginImages.DESC_COMPONENT;
				break;

			case TTCN3ParserTokenTypes.FunctionDef:
				descriptor = TTCN3UIPluginImages.DESC_FUNCTION;
				break;

			case TTCN3ParserTokenTypes.GroupDef:
				descriptor = TTCN3UIPluginImages.DESC_GROUP;
				break;

			case TTCN3ParserTokenTypes.PortDef:
				descriptor = TTCN3UIPluginImages.DESC_PORT;
				break;

			case TTCN3ParserTokenTypes.TemplateDef:
				descriptor = TTCN3UIPluginImages.DESC_TEMPLATE;
				break;

			case TTCN3ParserTokenTypes.TestcaseDef:
				descriptor = TTCN3UIPluginImages.DESC_TESTCASE;
				break;

			case TTCN3ParserTokenTypes.TypeDef:
				descriptor = TTCN3UIPluginImages.DESC_TYPE;
				break;

			default:
				descriptor = TTCN3UIPluginImages.DESC_MISC_DEFAULT;
				break;
			}
		} else {
			descriptor = TTCN3UIPluginImages.DESC_MISC_DEFAULT;
		}

		// return the cached image corresponding to the descriptor
		return TTCN3UIPluginImages.imageCacheGet(descriptor);
	}

	/** {@inheritDoc} */
	@Override
	public String getText(Object anElement) {
		if (!(anElement instanceof AST))
			return "description unavailable";

		final AST element = (AST) anElement;

		if (element.getType() == TTCN3ParserTokenTypes.TTCN3Module) {
			return ASTUtil.getDescendantText(element);
		}

		if (element.getType() == TTCN3ParserTokenTypes.ModuleDefinitionList)
			return "Module Declarations";

		if (element.getType() == TTCN3ParserTokenTypes.ModuleControlBody)
			return "Control Part";

		if (element.getFirstChild() != null) {
			return ASTUtil.getDescendantText(element.getFirstChild());
		}

		return "description unavailable";
	}

	// /** {@inheritDoc} */
	// @Override
	// public void finalize() {
	// for (Iterator<Image> images =
	// TTCN3UIPluginImages.imageCache.values().iterator(); images
	// .hasNext();) {
	// images.next().dispose();
	// }
	// fImageCache.clear();
	// super.finalize();
	// }
}
