package de.ugoe.cs.swe.trex.ui.editors.text;

import org.eclipse.jface.text.rules.IWordDetector;

/**
 * A TTCN3 aware word detector.
 * 
 * @author Jochen Kemnade
 */
public class TTCN3WordDetector implements IWordDetector {

	/**
	 * Determines if the specified character is permissible as the first
	 * character in a TTCN3 identifier.
	 * 
	 * @param aChar
	 *            the character to be tested.
	 * @return true if the character may start a TTCN3 identifier; false
	 *         otherwise.
	 * @see java.lang.Character#isLetter(char)
	 * @see org.eclipse.jface.text.rules.IWordDetector#isWordStart
	 */
	public boolean isWordStart(char aChar) {
		return Character.isLetter(aChar);
	}

	/**
	 * Determines if the specified character may be part of a TTCN3 identifier
	 * as other than the first character.
	 * 
	 * @param aChar
	 *            the character to be tested.
	 * @return true if the character may be part of a TTCN3 identifier; false
	 *         otherwise.
	 * @see java.lang.Character#isLetterOrDigit(char)
	 * @see org.eclipse.jface.text.rules.IWordDetector#isWordPart
	 */
	public boolean isWordPart(char aChar) {
		return Character.isJavaIdentifierStart(aChar) || Character.isDigit(aChar);
	}
}
