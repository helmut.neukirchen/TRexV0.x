package de.ugoe.cs.swe.trex.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;


public class CompileFileHandler extends AbstractHandler {

	private static final String ERROR_HEADLINE = "Error Toggling File for Compilation"; //$NON-NLS-1$

	private static final String QUALIFIER = TTCN3UIPlugin.PLUGIN_ID;

	private static final String LOCAL_NAME = "compileFileFlag";

	public static final String PROPERTY_SET = "true";

	public static final QualifiedName QUALIFIED_NAME = new QualifiedName(
			QUALIFIER, LOCAL_NAME);

	private IStructuredSelection selection;

	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selectedItem = HandlerUtil.getActiveWorkbenchWindow(event)
		        .getActivePage().getSelection();

	    if (selectedItem != null & selectedItem instanceof IStructuredSelection)
	      selection = (IStructuredSelection) selectedItem;

		if (selection.size() == 1) {
			Object firstElement = selection.getFirstElement();
			try {
				IFile file = (IFile) firstElement;
				String currentDisabledPropertyValue = file
						.getPersistentProperty(QUALIFIED_NAME);

				if (currentDisabledPropertyValue == null) {
					setDisabledProperty(file, PROPERTY_SET);
				} else {
					setDisabledProperty(file, null);
				}
			} catch (CoreException e) {
				MessageDialog.openError(Display.getDefault()
						.getActiveShell(), ERROR_HEADLINE,
						"The file could not be toggled for compilation."); //$NON-NLS-1$
			}
			PlatformUI.getWorkbench().getDecoratorManager().update(
					"de.ugoe.cs.swe.trex.ui.compilationDecorator");
		}
		return null;
	}

	private void setDisabledProperty(IFile file, String value)
			throws CoreException {
		file.setPersistentProperty(QUALIFIED_NAME, value);
		PlatformUI.getWorkbench().getDecoratorManager().setEnabled("de.ugoe.cs.swe.trex.ui.compilationDecorator", true);
	}

	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			this.selection = (IStructuredSelection) selection;
		} else {
			this.selection = StructuredSelection.EMPTY;
		}
	}
}
