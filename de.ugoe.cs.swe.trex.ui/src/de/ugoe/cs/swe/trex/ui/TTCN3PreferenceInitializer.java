package de.ugoe.cs.swe.trex.ui;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.internal.editors.text.EditorsPlugin;

import de.ugoe.cs.swe.trex.ui.preferences.PreferenceConstants;


/**
 * Initialize default preferences for the TTCN3 UI plugin.
 */
@SuppressWarnings("restriction")
public class TTCN3PreferenceInitializer extends AbstractPreferenceInitializer {

	/** {@inheritDoc} */
	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = TTCN3UIPlugin.getDefault()
				.getPreferenceStore();
		store.setDefault(TTCN3UIPlugin.PLUGIN_ID + ".editor.showSegments",
				false);
		PreferenceConverter.setDefault(store, "color.default", PreferenceConstants.RGB_DEFAULT);
		PreferenceConverter.setDefault(store, "color.keyword", PreferenceConstants.RGB_KEYWORD);
		PreferenceConverter.setDefault(store, "color.string", PreferenceConstants.RGB_STRING);
		PreferenceConverter.setDefault(store, "color.comment", PreferenceConstants.RGB_COMMENT);
		
		// formatter defaults
		store.setDefault(PreferenceConstants.P_TTCN3_FORMATTER_TABS, false);
		store.setDefault(PreferenceConstants.P_TTCN3_FORMATTER_UNIX_NEWLINE, false);
		store.setDefault(PreferenceConstants.P_TTCN3_FORMATTER_SPACES_BETWEEN_ASSIGNMENT, true);
		store.setDefault(PreferenceConstants.P_TTCN3_FORMATTER_SPACE_AFTER_COMMA, true);
		store.setDefault(PreferenceConstants.P_TTCN3_FORMATTER_KR_STYLE, true);
		store.setDefault(PreferenceConstants.P_TTCN3_FORMATTER_SPACES_COUNT, 4);
		store.setDefault(PreferenceConstants.P_TTCN3_FORMATTER_LINES_BETWEEN_MODULES, 2);
		store.setDefault(PreferenceConstants.P_TTCN3_FORMATTER_LINES_AFTER_CONTROL_PART, 1);
		store.setDefault(PreferenceConstants.P_TTCN3_FORMATTER_LINES_AFTER_MODULE_DEFINITION, 1);
		
		// folding defaults: turn all foldings on by default
		store.setDefault(PreferenceConstants.P_TTCN3_FOLDING_ENABLEMENT, true);
		store.setDefault(PreferenceConstants.P_TTCN3_FOLDING_TYPE, true);
		store.setDefault(PreferenceConstants.P_TTCN3_FOLDING_TEMPLATE, true);
		store.setDefault(PreferenceConstants.P_TTCN3_FOLDING_COMPONENT, true);
		store.setDefault(PreferenceConstants.P_TTCN3_FOLDING_GROUP, true);
		store.setDefault(PreferenceConstants.P_TTCN3_FOLDING_ALTSTEP, true);
		store.setDefault(PreferenceConstants.P_TTCN3_FOLDING_FUNCTION, true);
		store.setDefault(PreferenceConstants.P_TTCN3_FOLDING_TESTCASE, true);
		store.setDefault(PreferenceConstants.P_TTCN3_FOLDING_MODULE, true);
		
		// Outline defaults: Added by Einar And Niels (TTC-312)
		store.setDefault(PreferenceConstants.P_TTCN3_OUTLINE_TREE_VIEW_EXPANDED_BY_DEFAULT, false);
		//START modified by sukiyuki 2011-2-9
		//match bracket
		IPreferenceStore defaultStore = EditorsPlugin.getDefault().getPreferenceStore();
		defaultStore.setDefault(PreferenceConstants.EDITOR_MATCHING_BRACKETS, true);
		PreferenceConverter.setDefault(defaultStore, PreferenceConstants.EDITOR_MATCHING_BRACKETS_COLOR, new RGB(163,183,232));
		//END   modified by sukiyuki 2011-2-9
	}
}
