package de.ugoe.cs.swe.trex.ui.editors.text;

import org.eclipse.jface.text.rules.IWhitespaceDetector;
/**
 * @author Jochen Kemnade
 *
 */
public class TTCN3WhitespaceDetector implements IWhitespaceDetector {

	public boolean isWhitespace(char aChar) {
		return (aChar == ' ' || aChar == '\t' || aChar == '\n' || aChar == '\r');
	}
}
