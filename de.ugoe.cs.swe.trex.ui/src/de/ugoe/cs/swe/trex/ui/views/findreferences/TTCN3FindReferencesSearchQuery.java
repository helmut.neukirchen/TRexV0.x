package de.ugoe.cs.swe.trex.ui.views.findreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.ISearchResult;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.search.ui.text.Match;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceFinder;
import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;

/**
 * The search query class does the actual work. The core of
 * functionality is the run method, which collects all references 
 * and passes them to the SearchResult object which serves as
 * input for the SearchResultPage view.
 */
public class TTCN3FindReferencesSearchQuery implements ISearchQuery {

	// This value maps all references to source files
	private Map<String, List<ReferenceWithContext>> references;
	
	// The SearchResult object which holds a list of matches for the references
	private TTCN3FindReferencesSearchResult result = null;
	
	// The LocationAST object which serves as input for this search query
	private LocationAST queryNode = null;

	/**
	 * Constructs a SearchQuery object. 
	 * 
	 * @param query The actual input node
	 */
	public TTCN3FindReferencesSearchQuery(LocationAST query) {
		result = new TTCN3FindReferencesSearchResult(this);
		queryNode = query;
		
		// Make sure we have an identifier node
		if (queryNode.getType() != TTCN3LexerTokenTypes.IDENTIFIER) {
			MessageDialog.openError(null, "Error", "The operation is unavailable at the current selection. Please select a valid TTCN-3 identifier.");
			queryNode = null;
		}
	}
	
	/**
	 * Uses TTCN3 stuff to get a map of references. Does some precondition
	 * checking to make sure that we have a valid input. Used in 
	 * {@link #SearchQuery.run(IProgressMonitor)}
	 */
	private boolean collectReferences(String filename)
	{
		Scope scope = queryNode.getScope();
		Symbol declarationSymbol = null;
		if (scope != null)
			declarationSymbol = scope.resolve(queryNode);

		if (declarationSymbol == null)
			return false;

		LocationAST declarationNode = declarationSymbol.getDeclarationNode();

		if (declarationNode == null)
			return false;
		
		if (filename == "*")
			references = new ReferenceFinder().findReferences(declarationSymbol);
		else
			references = new ReferenceFinder().findReferences(declarationSymbol, filename);
		
		result.setReferences(references);
		
		return true;
	}
	
	/**
	 * Add a position marker to a file. Used in 
	 * {@link #SearchQuery.run(IProgressMonitor)}
	 * 
	 * @param file The file to which the marker should be added
	 * @param startOffset The start offset caret position for the marker
	 * @param endOffset The end offset caret position for the marker
	 */
	private void addMarkerToFile(IFile file, int startOffset, int endOffset) {
		IMarker marker = null;

		try {
			marker = file.createMarker(NewSearchUI.SEARCH_MARKER);
			Map<String, Object> attributes = new HashMap<String, Object>();
			attributes.put(IMarker.CHAR_START, new Integer(startOffset));
			attributes.put(IMarker.CHAR_END, new Integer(endOffset));
			marker.setAttributes(attributes);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the project file for this source file. Used in 
	 * {@link #SearchQuery.run(IProgressMonitor)}
	 * 
	 * @param filename The name of the source file
	 * @returns an IFile object holding the project file requested
	 */
	private IFile getProjectFile(String filename) {
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		IProject project = root.getProject();

		IFile file = null;
		if (project == null) {
			IPath path = ws.getRoot().findMember(filename).getFullPath();
			file = ws.getRoot().getFile(path);
		} else {
			file = project.getFile(filename);
		}
		return file;
	}
	
	//
	// Changed by Ingvi & �mar 29.11.2010
	//

	/**
	 * Runs this search query
	 * 
	 * @param The Progress Monitor
	 * @returns an IStatus instance containing the result of the operation
	 */
	public IStatus run(IProgressMonitor monitor)
			throws OperationCanceledException {	
		return runForOneOrManyFiles(monitor, "*");
	}
	
	//
	// End of changes
	//
	
	//
	// Added by Ingvi & �mar 29.11.2010
	//
	
	/** Runs the search query
	 * 
	 * @param The Progress Monitor
	 * @param The file to search in or "*" to search in all files
	 * @returns an IStatus instance containing the result of the operation
	 */
	public IStatus run(IProgressMonitor monitor, String filterFilename) 
			throws OperationCanceledException{
		return runForOneOrManyFiles(monitor, filterFilename);
	}
	//
	// End of add
	//
	
	
	//
	// Renamed by Ingvi & �mar 29.11.2010
	// 
	/**
	 * Runs this search query
	 * 
	 * @param The Progress Monitor
	 * @param The file to search in
	 * @returns an IStatus instance containing the result of the operation
	 */
	protected IStatus runForOneOrManyFiles(IProgressMonitor monitor, String filterFilename)
			throws OperationCanceledException {	
		monitor.beginTask("Finding references...", 1000);
		
		// First, collect all references using the ReferenceFinder.
		// If an error occurs, return CANCEL_STATUS.
		if (!collectReferences(filterFilename))
		{
			return Status.CANCEL_STATUS;
		}
		
		// Next step is to prepare building the data structures for our
		// matches so we can add them to the search result tree
		final Set<String> refsKeySet = references.keySet();
		
		// Loop through each affected file
		for (String filename : refsKeySet) {
			
			if (filterFilename != "*" && !filename.equals(filterFilename))
				continue;
			
			List<ReferenceWithContext> refs = references.get(filename);
			
			// Create a new file element
			IFile file = getProjectFile(filename);
			FileElement fileElement = new FileElement(file);

			Map<LocationAST, ModuleElement> modulesMap = new HashMap<LocationAST, ModuleElement>();
			Map<LocationAST, CompoundElement> compoundsMap = new HashMap<LocationAST, CompoundElement>();
			List<AtomicElement> atomics = new ArrayList<AtomicElement>();

			List<ModuleElement> modulesList = new ArrayList<ModuleElement>();
			List<CompoundElement> compoundsList = new ArrayList<CompoundElement>();
			
			try {
				file.deleteMarkers(NewSearchUI.SEARCH_MARKER, false, IResource.DEPTH_INFINITE);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// Loop through each reference
			for (ReferenceWithContext ref : refs) {
				LocationAST refNode = ref.getReferenceNode();
				
				// Get module node for this reference
				LocationAST moduleNode = LocationAST.resolveParentsUntilType(
						refNode, TTCN3LexerTokenTypes.TTCN3Module);

				// Create a new mapping with this module node if it
				// is not already in the map
				ModuleElement moduleElem = modulesMap.get(moduleNode);
				if (moduleElem == null) {
					moduleElem = new ModuleElement(fileElement, moduleNode);
					modulesList.add(moduleElem);
				}
				modulesMap.put(moduleNode, moduleElem);

				// Next step is to get the context node for this reference
				
				int types[] = { 
						TTCN3LexerTokenTypes.ImportDef,
						TTCN3LexerTokenTypes.TemplateDef
				};
				LocationAST compoundNode = LocationAST.resolveParentsUntilType(
						refNode, types);
				
				if (compoundNode == null)
					compoundNode = ref.getContextNode();

				// Create a new mapping with this context node if it
				// is not already in the map
				CompoundElement compElem = compoundsMap.get(compoundNode);
				if (compoundNode != null) {
					if (compElem == null) {
						compElem = new CompoundElement(moduleElem, compoundNode);
						compoundsList.add(compElem);
					}
					compoundsMap.put(compoundNode, compElem);
				}

				// Create the atomic element which holds the actual reference
				AtomicElement atomic = new AtomicElement(compElem, ref);
				atomics.add(atomic);

				// For the editor, we create a marker for each reference in
				// each file
				addMarkerToFile(file, refNode.getOffset(), refNode.getEndOffset());
			}
			
			// Add a new match containing this file element to our SearchResult 
			result.addMatch(new Match(fileElement, 0, 0));
		}

		monitor.done();
		return Status.OK_STATUS;
	}

	
	// some methods required by the interface ISearchQuery
	public boolean canRerun() {
		return false;
	}

	public boolean canRunInBackground() {
		return true;
	}

	public String getLabel() {
		return "FindReferences SearchQuery";
	}

	public ISearchResult getSearchResult() {
		return result;
	}

	public LocationAST getQueryNode() {
		return queryNode;
	}
}
