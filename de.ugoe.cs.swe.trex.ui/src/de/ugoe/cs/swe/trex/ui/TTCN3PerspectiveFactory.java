package de.ugoe.cs.swe.trex.ui;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class TTCN3PerspectiveFactory implements IPerspectiveFactory {

	public void createInitialLayout(IPageLayout layout) {
		IFolderLayout folderLayout1 = layout.createFolder("folder0", IPageLayout.LEFT, 0.22f, layout.getEditorArea());
		folderLayout1.addView("de.ugoe.cs.swe.trex.ui.views.navigator");
		IFolderLayout folderLayout = layout.createFolder("folder", IPageLayout.BOTTOM, 0.86f, layout.getEditorArea());
		folderLayout.addView(IPageLayout.ID_PROBLEM_VIEW);
		folderLayout.addView("de.ugoe.cs.swe.trex.ui.views.TTCN3MetricsView");
		folderLayout.addView(IPageLayout.ID_BOOKMARKS);
		layout.addView(IPageLayout.ID_OUTLINE, IPageLayout.RIGHT, 0.6f, layout.getEditorArea());
	}
}
