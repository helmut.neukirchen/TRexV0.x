package de.ugoe.cs.swe.trex.ui;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

/**
 * Bundle of images used by TTCN3 UI plugin.
 */
public class TTCN3UIPluginImages {

	private static final String NAME_PREFIX = TTCN3UIPlugin.PLUGIN_ID;

	private static final int NAME_PREFIX_LENGTH = TTCN3UIPluginImages.NAME_PREFIX.length();

	private static URL fgIconBaseURL = null;

	private static HashMap<ImageDescriptor, Image> imageCache = new HashMap<ImageDescriptor, Image>();

	public static Image imageCacheGet(ImageDescriptor id) {
		Image image = TTCN3UIPluginImages.imageCache.get(id);
		if ((image == null) && (id != null)){
			image = id.createImage();
			TTCN3UIPluginImages.imageCache.put(id, image);
		}
		return image;
	}
	
	// The plugin registry
	private static final ImageRegistry IMAGE_REGISTRY;
	
	static{
		// determine display depth to decide whether to use high or low color images
		// and getting a plugin iamge registry:
		final Display display = Display.getDefault(); // get the default display of the UI thread
		// the following AtomicReference is needed to be able to reach the iconDepth value
		// after the execution of the runnable
		final AtomicReference<Integer> iconDepth = new AtomicReference<Integer>();
		// the following AtomicReference is needed to be able to reach the ImageRegistry
		// after the execution of the runnable
		final AtomicReference<ImageRegistry> ir_reference = new AtomicReference<ImageRegistry>();
		// the following code has to be executed by display's syncExec(), since this thread is not the UI thread
		display.syncExec(new Runnable(){
			public void run(){
				iconDepth.set(display.getIconDepth());
				ir_reference.set(new ImageRegistry());
			}
		});
		String pathSuffix = (iconDepth.get() > 4) ? "icons/full/" : "icons/basic/";
		try { fgIconBaseURL = new URL(TTCN3UIPlugin.getInstallURL(), pathSuffix); }
		catch (MalformedURLException e){ TTCN3UIPlugin.log(e); }
		IMAGE_REGISTRY = ir_reference.get();
	}


	// Available cached Images in the TTCN3 UI plugin image registry.

	public static final String IMG_FIX_DELETE = TTCN3UIPluginImages.NAME_PREFIX + "fix_delete.gif"; 
	public static final String IMG_TTCN3 = TTCN3UIPluginImages.NAME_PREFIX + "ttcn3.gif";
	public static final String IMG_VALUE = TTCN3UIPluginImages.NAME_PREFIX + "value.gif";

	
	/** altstep */
	public static final String IMG_ALTSTEP = TTCN3UIPluginImages.NAME_PREFIX + "altstep.gif";

	/** function */
	public static final String IMG_FUNCTION = TTCN3UIPluginImages.NAME_PREFIX + "function.gif";

	/** group */
	public static final String IMG_GROUP = TTCN3UIPluginImages.NAME_PREFIX + "group.gif";	
	
	/** testcase */
	public static final String IMG_TESTCASE = TTCN3UIPluginImages.NAME_PREFIX + "testcase.gif";
	
	/** template */
	public static final String IMG_TEMPLATE = TTCN3UIPluginImages.NAME_PREFIX + "template.gif";	
	
	/** port */
	public static final String IMG_PORT = TTCN3UIPluginImages.NAME_PREFIX + "port.gif";
	
	/** component */
	public static final String IMG_COMPONENT = TTCN3UIPluginImages.NAME_PREFIX + "component.gif";
	
	/** other */
	public static final String IMG_OTHER = TTCN3UIPluginImages.NAME_PREFIX + "other.gif";


	public static final String IMG_VARIABLE = TTCN3UIPluginImages.NAME_PREFIX + "variable.gif";

	public static final String IMG_MODULE = TTCN3UIPluginImages.NAME_PREFIX + "module.gif";
	
	public static final String IMG_TYPE = TTCN3UIPluginImages.NAME_PREFIX + "type.gif";

	public static final String IMG_KEYWORD = TTCN3UIPluginImages.NAME_PREFIX + "keyword.gif";
	/** default */
	public static final String IMG_MISC_DEFAULT = TTCN3UIPluginImages.NAME_PREFIX
			+ "icon_default.gif";

	// Set of predefined Image Descriptors.
	private static final String T_OBJ = "obj16";

	// private static final String T_CLCL = "clcl16";
	private static final String T_CTOOL = "ctool16";

	// /** class */
	// public static final ImageDescriptor DESC_OBJS_CLASS =
	// createManaged(T_OBJ,
	// IMG_OBJS_CLASS);

	public static final ImageDescriptor DESC_TTCN3 = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_TTCN3);
	public static final ImageDescriptor DESC_VALUE = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_VALUE);
	
	/** altstep */
	public static final ImageDescriptor DESC_ALTSTEP = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_ALTSTEP);

	/** function */
	public static final ImageDescriptor DESC_FUNCTION = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_FUNCTION);
	
	/** group */
	public static final ImageDescriptor DESC_GROUP = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_GROUP);	

	/** testcase */
	public static final ImageDescriptor DESC_TESTCASE = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_TESTCASE);
	
	/** template */
	public static final ImageDescriptor DESC_TEMPLATE = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_TEMPLATE);
	/** port */
	public static final ImageDescriptor DESC_PORT = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_PORT);
	/** component */
	public static final ImageDescriptor DESC_COMPONENT = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_COMPONENT);
	/** other */
	public static final ImageDescriptor DESC_OTHER = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_OTHER);
	
	public static final ImageDescriptor DESC_VARIABLE = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_VARIABLE);

	public static final ImageDescriptor DESC_MODULE = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_MODULE);

	public static final ImageDescriptor DESC_TYPE = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_TYPE);

	public static final ImageDescriptor DESC_KEYWORD = createManaged(TTCN3UIPluginImages.T_OBJ,
			TTCN3UIPluginImages.IMG_KEYWORD);

	
	/** default */
	public static final ImageDescriptor DESC_MISC_DEFAULT = createManaged(
			TTCN3UIPluginImages.T_OBJ, TTCN3UIPluginImages.IMG_MISC_DEFAULT);

	/** show segments */
	public static final ImageDescriptor DESC_TOOL_SHOW_SEGMENTS = create(
			TTCN3UIPluginImages.T_CTOOL, "segment_edit.gif");

	/** go to next error */
	public static final ImageDescriptor DESC_TOOL_GOTO_NEXT_ERROR = create(
			TTCN3UIPluginImages.T_CTOOL, "next_error_nav.gif"); //$NON-NLS-1$

	/** go to previous error */
	public static final ImageDescriptor DESC_TOOL_GOTO_PREV_ERROR = create(
			TTCN3UIPluginImages.T_CTOOL, "prev_error_nav.gif"); //$NON-NLS-1$

	public static final ImageDescriptor FIX_DELETE = createManaged(TTCN3UIPluginImages.T_OBJ, TTCN3UIPluginImages.IMG_FIX_DELETE); 
	
	/**
	 * Returns the image managed under the given key in this registry.
	 * 
	 * @param aKey
	 *            the image's key
	 * @return the image managed under the given key
	 */
	public static Image get(String aKey) {
		return TTCN3UIPluginImages.IMAGE_REGISTRY.get(aKey);
	}

	/**
	 * Sets the three image descriptors for enabled, disabled, and hovered to an
	 * action. The actions are retrieved from the *tool16 folders.
	 * 
	 * @param anAction
	 *            The action for the image
	 * @param anIconName
	 *            the icon name
	 */
	public static void setToolImageDescriptors(IAction anAction,
			String anIconName) {
		setImageDescriptors(anAction, "tool16", anIconName);
	}

	/**
	 * Sets the three image descriptors for enabled, disabled, and hovered to an
	 * action. The actions are retrieved from the *lcl16 folders.
	 * 
	 * @param anAction
	 *            The action for the image
	 * @param anIconName
	 *            the icon name
	 */
	public static void setLocalImageDescriptors(IAction anAction,
			String anIconName) {
		setImageDescriptors(anAction, "lcl16", anIconName);
	}

	// ---- Helper methods to access icons on the file system -----------------

	private static void setImageDescriptors(IAction anAction, String aType,
			String aRelPath) {
		try {
			ImageDescriptor id = ImageDescriptor.createFromURL(makeIconFileURL(
					"d" + aType, aRelPath));
			if (id != null) {
				anAction.setDisabledImageDescriptor(id);
			}
		} catch (MalformedURLException e) {
			TTCN3UIPlugin.log(e);
		}

		try {
			ImageDescriptor id = ImageDescriptor.createFromURL(makeIconFileURL(
					"c" + aType, aRelPath));
			if (id != null) {
				anAction.setHoverImageDescriptor(id);
			}
		} catch (MalformedURLException e) {
			TTCN3UIPlugin.log(e);
		}

		anAction.setImageDescriptor(create("e" + aType, aRelPath));
	}

	private static ImageDescriptor createManaged(String aPrefix, String aName) {
		ImageDescriptor result;
		
		try {
			result = ImageDescriptor.createFromURL(makeIconFileURL(aPrefix,
					aName.substring(TTCN3UIPluginImages.NAME_PREFIX_LENGTH)));
			TTCN3UIPluginImages.IMAGE_REGISTRY.put(aName, result);
		} catch (MalformedURLException e) {
			result = ImageDescriptor.getMissingImageDescriptor();
		}
		return result;
	}

	private static ImageDescriptor create(String aPrefix, String aName) {
		ImageDescriptor result;
		try {
			result = ImageDescriptor.createFromURL(makeIconFileURL(aPrefix,
					aName));
		} catch (MalformedURLException e) {
			result = ImageDescriptor.getMissingImageDescriptor();
		}
		return result;
	}

	private static URL makeIconFileURL(String aPrefix, String aName)
			throws MalformedURLException {
		if (TTCN3UIPluginImages.fgIconBaseURL == null) {
			throw new MalformedURLException();
		}

		StringBuffer buffer = new StringBuffer(aPrefix);
		buffer.append('/');
		buffer.append(aName);
		return new URL(TTCN3UIPluginImages.fgIconBaseURL, buffer.toString());
	}
}
