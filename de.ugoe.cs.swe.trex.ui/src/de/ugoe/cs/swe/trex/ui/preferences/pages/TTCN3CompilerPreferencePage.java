package de.ugoe.cs.swe.trex.ui.preferences.pages;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.osgi.service.prefs.BackingStoreException;

import de.ugoe.cs.swe.trex.ui.CompilerConstants;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;

public class TTCN3CompilerPreferencePage extends PreferencePage implements
		IWorkbenchPreferencePage {

	private Combo compilerCombo;
	private Label compilerLabel;
	private Button projectWideCompilationButton;
	private Button verboseButton;
	
	// --------------------------------------------------------------------------

	public TTCN3CompilerPreferencePage() {
		super("TRex TTCN-3");
		setPreferenceStore(TTCN3UIPlugin.getDefault().getPreferenceStore());
		setDescription("TTCN-3 Compiler Settings");
	}

	// --------------------------------------------------------------------------

	@Override
	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.FILL);
		GridLayout layout = new GridLayout(2, false);
		composite.setLayout(layout);
		GridData data = new GridData(GridData.FILL);
		data.grabExcessHorizontalSpace = true;
		composite.setLayoutData(data);
		compilerLabel = new Label(composite, SWT.NONE);
		compilerLabel.setText("Please select your compiler: ");
		compilerLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		compilerCombo = new Combo(composite, SWT.READ_ONLY);
		compilerCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IExtensionPoint ep = reg.getExtensionPoint("de.ugoe.cs.swe.trex.ui.compiler");
		IExtension[] extensions = ep.getExtensions();
		for (int i = 0; i < extensions.length; i++) {
			IExtension ext = extensions[i];
			IConfigurationElement[] ce = ext.getConfigurationElements();
			for (int j = 0; j < ce.length; j++) {
				String name= ce[j].getAttribute("name");
				compilerCombo.add(name);
			}
		}
		String activeCompiler = getPreferenceStore().getString(CompilerConstants.SELECTED_COMPILER);
		boolean selected = false;
		for (int i=0; i < compilerCombo.getItemCount(); i++) {
			if (compilerCombo.getItem(i).equals(activeCompiler)) {
				compilerCombo.select(i);
				selected = true;
			}
		}
		if ((!selected) && (compilerCombo.getItemCount() > 0)) {
			compilerCombo.select(0);
//			if (compilerCombo.getText() != null)
//				getPreferenceStore().setValue("de.ugoe.cs.swe.trex.ui.compiler.selectedCompiler", compilerCombo.getText());
//			getPreferenceStore().setValue("de.ugoe.cs.swe.trex.ui.compiler.compileAllProjects", projectWideCompilationButton.getSelection());
		}
		
		projectWideCompilationButton = new Button(composite, SWT.CHECK);
		projectWideCompilationButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		projectWideCompilationButton.setText("Compile All Projects");
		boolean isCompileAllProjects = getPreferenceStore().getBoolean(CompilerConstants.COMPILE_ALL_PROJECTS);
		projectWideCompilationButton.setSelection(isCompileAllProjects);

		verboseButton = new Button(composite, SWT.CHECK);
		verboseButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		verboseButton.setText("Verbose Console Output");
		boolean isVerbose = getPreferenceStore().getBoolean(CompilerConstants.VERBOSE);
		verboseButton.setSelection(isVerbose);
		
		return composite;
	}

	// --------------------------------------------------------------------------

	public void init(IWorkbench workbench) {
		// do nothing
	}

	// --------------------------------------------------------------------------

	public boolean performOk() {
		super.performOk();
		if (compilerCombo.getText() != null)
			getPreferenceStore().setValue(CompilerConstants.SELECTED_COMPILER, compilerCombo.getText());
		getPreferenceStore().setValue(CompilerConstants.COMPILE_ALL_PROJECTS, projectWideCompilationButton.getSelection());
		getPreferenceStore().setValue(CompilerConstants.VERBOSE, verboseButton.getSelection());
		try {
			InstanceScope.INSTANCE.getNode(TTCN3UIPlugin.getUniqueIdentifier()).flush();
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block. What would be a good catch ?
			e.printStackTrace();
		}
		return true;
    }
}