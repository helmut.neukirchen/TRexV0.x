package de.ugoe.cs.swe.trex.ui.editors;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.formatter.IFormattingStrategy;
import org.eclipse.swt.widgets.Display;

import de.ugoe.cs.swe.trex.core.formatter.TTCN3Formatter;
import de.ugoe.cs.swe.trex.core.formatter.TokenStreamMismatchException;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;
import de.ugoe.cs.swe.trex.ui.preferences.adapters.TTCN3FormatterParametersAdapter;


public class TTCN3FormattingStrategy implements IFormattingStrategy {

	private TTCN3Editor editor;
	private String formattedContent = null;

	public TTCN3FormattingStrategy(TTCN3Editor editor) {
		 this.editor = editor;
	}

	public void formatterStarts(String initialIndentation) {
		// TODO
	}

	public String format(String content, boolean isLineStart,
			String indentation, int[] positions) {
		String result = content;
		formattedContent = null;
		TTCN3Formatter formatter = new TTCN3Formatter();
		try {
			String text = editor.getDocument().get();
			formattedContent = formatter.formatTTCN3Source(text,
					new TTCN3FormatterParametersAdapter());

			//return formattedContent;
			result = formattedContent;
		} catch (TokenStreamMismatchException e1) {
			MessageDialog
			.openInformation(Display.getDefault().getActiveShell(), "Formatter Exception",
					e1.getMessage() + "\nPlease Report! Formatting aborted!");
			e1.printStackTrace();
			ILog log = TTCN3UIPlugin.getInstance().getLog();
			StringBuffer logMessage = new StringBuffer();
			logMessage.append("Token Mismatch while Formatting!\n");
			logMessage.append("=[source]============================================\n");
			logMessage.append(e1.getSource().getTracker().toDebugString()).append("\n");
			logMessage.append("=[target]============================================\n");
			logMessage.append(e1.getTarget().getTracker().toDebugString()).append("\n");
			logMessage.append("=====================================================\n");
			log.log(new Status(IStatus.ERROR, TTCN3UIPlugin.PLUGIN_ID, IStatus.OK, logMessage.toString(), e1));
		} catch (Exception e2) {
			MessageDialog
					.openInformation(Display.getDefault().getActiveShell(), "Formatter Exception",
							"Formatter can only be applied to syntactically correct files!");
		}
		return result;
	}

	public void formatterStops() {
		if (formattedContent != null)
			editor.getDocument().set(formattedContent);
	}

}
