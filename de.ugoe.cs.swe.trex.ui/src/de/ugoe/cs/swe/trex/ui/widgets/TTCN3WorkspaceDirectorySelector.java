package de.ugoe.cs.swe.trex.ui.widgets;

import java.util.ArrayList;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

public class TTCN3WorkspaceDirectorySelector extends Composite {

	private IProject project;
	private TreeViewer tv;
	private TreeSelection lastSelection = null;
	
	public class ProjectResourceSelectorContentProvider implements
			ITreeContentProvider {

		public void dispose() {
		}

		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

		public Object[] getChildren(Object arg0) {
			ArrayList<IResource> result = new ArrayList<IResource>();
			if (arg0 instanceof IProject) {
				IProject currentProject = (IProject)arg0;
				try {
					IResource[] members = currentProject.members();
					for (int i=0; i < members.length; i++) {						
						if (members[i] instanceof IFolder) {
							result.add(members[i]);
						}
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
			} else if (arg0 instanceof IFolder) {
				IFolder currentFolder = (IFolder)arg0;
				try {
					IResource[] members = currentFolder.members();
					for (int i=0; i < members.length; i++) {						
						if (members[i] instanceof IFolder) {
							result.add(members[i]);
						}
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
				
			} else if (arg0 instanceof IWorkspaceRoot) {
				IProject[] projects = ((IWorkspaceRoot)arg0).getProjects();
				for (int i=0; i < projects.length; i++)
					result.add(projects[i]);
			}
			return result.toArray(new IResource[0]);
		}

		public boolean hasChildren(Object arg0) {
			if (arg0 instanceof IProject) {
				try {
					return ((IProject)arg0).members().length > 0;
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
				
			if (arg0 instanceof IFolder) {
				try {
					return ((IFolder)arg0).members().length > 0;
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}			
			if (arg0 instanceof IWorkspaceRoot) {
				return ((IWorkspaceRoot)arg0).getProjects().length > 0;
			}
			return false;
		}

		public Object[] getElements(Object arg0) {
			return getChildren(arg0);
		}

		public Object getParent(Object element) {
			if (element instanceof IResource)
				return ((IResource)element).getParent();
			return null;
		}

	}

	// ------------------------------------------------------------------------------------

	public class ProjectResourceSelectorLabelProvider implements ILabelProvider {

		public Image getImage(Object element) {
			if ((element instanceof IFolder) || (element instanceof IProject) ) {
				return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER);
			}
			return null;
		}

		public String getText(Object element) {
			if (element instanceof IResource)
				return ((IResource)element).getName();
			
			return "Unknown Element";
		}

		public void addListener(ILabelProviderListener listener) {
		}

		public void dispose() {
		}

		public boolean isLabelProperty(Object element, String property) {
			return false;
		}

		public void removeListener(ILabelProviderListener listener) {
		}
	}

	// ------------------------------------------------------------------------------------

	public TTCN3WorkspaceDirectorySelector(Composite parent, int style, IProject project, String selection) {
		super(parent, style);
		this.project = project;
		setLayout(new FillLayout());
		tv = new TreeViewer(this, SWT.SINGLE | SWT.BORDER);
		tv.setAutoExpandLevel(2);
		tv.setUseHashlookup(true);
		tv.getTree().setLayout(new FillLayout());
		tv.setContentProvider(new ProjectResourceSelectorContentProvider());
		tv.setLabelProvider(new ProjectResourceSelectorLabelProvider());
		lastSelection = (TreeSelection) tv.getSelection();
		tv.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				lastSelection = (TreeSelection) event.getSelection();
			}
			
		});
		tv.setInput(this.project.getWorkspace().getRoot());
	}

	// ------------------------------------------------------------------------------------
	
	public String getSelection() {
		if (lastSelection.getFirstElement() instanceof IResource) {
			IResource resource = (IResource) lastSelection.getFirstElement();
			return resource.getFullPath().toOSString();
		}
		return null;
	}
	
}
