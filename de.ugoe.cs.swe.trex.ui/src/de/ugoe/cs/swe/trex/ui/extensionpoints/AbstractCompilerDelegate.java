package de.ugoe.cs.swe.trex.ui.extensionpoints;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.PlatformUI;

import de.ugoe.cs.swe.trex.ui.CompilerConstants;
import de.ugoe.cs.swe.trex.ui.ConsoleTools;
import de.ugoe.cs.swe.trex.ui.TTCN3ResourceHelper;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;
import de.ugoe.cs.swe.trex.ui.builders.BuilderCommand;

public abstract class AbstractCompilerDelegate {

	private static final String EXTERNAL_COMPILER_MARKER = "de.ugoe.cs.swe.trex.ui.externalcompilermarker";

	// --------------------------------------------------------------------------

	public void compile(List<IFile> selectedFiles, int mode,
			IProgressMonitor monitor) {
		if (selectedFiles.size() > 0) {
			IWorkspace workspace = selectedFiles.get(0).getWorkspace();
			for (int i = 0; i < workspace.getRoot().getProjects().length; i++) {
				IProject project = workspace.getRoot().getProjects()[i];
				if ((project != null) && (project.isOpen()))
					deleteMarkers(project);
			}
		}

		if (selectedFiles.size() == 0)
			ConsoleTools.getErrorMessageConsoleStream().println("External Compiler: No main file selected for compilation!");

		if (!isCompilationAllowed())
			return;
		
		List<BuilderCommand> commands = compileHook(selectedFiles, mode);
		monitor.beginTask("Compilation", commands.size());
		
		for (int i = 0; i < commands.size(); i++) {
			monitor.setTaskName(commands.get(i).getCommand().get(0));
			monitor.worked(i);
			if (ConsoleTools.getMessageConsoleStream() != null) {
				File localDirectory = new File(commands.get(i).getProject().getWorkspace()
						.getRoot().getLocation().toOSString());
				
				ConsoleTools.getMessageConsoleStream().println("Executing external compiler command " + i+1 + ":");
				StringBuffer line = new StringBuffer();
				for (int n=0; n < commands.get(i).getCommand().size(); n++) {
					line.append(commands.get(i).getCommand().get(n));
					line.append(" ");
				}
				ConsoleTools.verbosePrintln("  Command         : " + line);
				ConsoleTools.verbosePrintln("  Project path    : "
					+ commands.get(i).getProject().getLocation().toOSString());
				ConsoleTools.verbosePrintln("  Build directory : " + localDirectory.getAbsolutePath());
			}
			runProcess(commands.get(i));
		}
		
		monitor.done();
	}

	// --------------------------------------------------------------------------

	abstract public void clean(IProgressMonitor monitor, IProject project);
	
	// --------------------------------------------------------------------------

	abstract public boolean isCompilationAllowed();

	// --------------------------------------------------------------------------

	public List<BuilderCommand> compileHook(List<IFile> selectedFiles, int mode){
		return new ArrayList<BuilderCommand>();
	}

	// --------------------------------------------------------------------------

	abstract public Map<String, Integer> getAvailableModes();

	// --------------------------------------------------------------------------

	public void addMarker(IFile file, int lineNumber, String message,
			Integer severity) {
		try {
			Map<String, Object> attributes = new HashMap<String, Object>();
			attributes.put(IMarker.LINE_NUMBER, new Integer(lineNumber));
			attributes.put(IMarker.MESSAGE, message);
			attributes.put(IMarker.SEVERITY, severity);
			attributes.put(IMarker.TRANSIENT, true);
			IMarker marker = file.createMarker(EXTERNAL_COMPILER_MARKER);
			marker.setAttributes(attributes);
		} catch (CoreException e) {
		}
	}

	// --------------------------------------------------------------------------

	public static void deleteMarkers(IResource startResource) {
		final List<String> workspaceFiles = new ArrayList<String>();
		final Map<String, IFile> files = new HashMap<String, IFile>();

		IResourceVisitor visitor = new IResourceVisitor() {
			public boolean visit(IResource resource) {
				if (TTCN3ResourceHelper.isTTCN3File(resource)) {
					IFile file = (IFile) resource;
					final String filename = file.getLocation().toOSString();
					files.put(filename, file);
					workspaceFiles.add(file.getFullPath().toPortableString());
				}
				return true;
			}
		};
		try {
			startResource.accept(visitor);

			for (Map.Entry<String, IFile> set : files.entrySet()) {
				IFile resource = set.getValue();
				resource.deleteMarkers(EXTERNAL_COMPILER_MARKER, true,
						IResource.DEPTH_INFINITE);
			}
		} catch (CoreException e) {
			TTCN3UIPlugin.log(e);
		}
	}

	// --------------------------------------------------------------------------
	
	private void runProcess(final BuilderCommand command) {
		if (command == null)
			return;

		final ProcessBuilder pb = new ProcessBuilder(command.getCommand());
		Map<String, String> env = pb.environment();
		env.putAll(command.getEnvironment());
		
		if (TTCN3UIPlugin.getDefault().getPreferenceStore().getBoolean(CompilerConstants.VERBOSE)) {
			Iterator<String> it = env.keySet().iterator();
			while (it.hasNext()) {
				String item = it.next();
				ConsoleTools.getMessageConsoleStream().println("[Compiler Environment] " + item + "=" + env.get(item));
			}
		}

		File localDirectory = new File(command.getProject().getWorkspace()
				.getRoot().getLocation().toOSString());

		pb.directory(localDirectory);
		// redirect stderr > stdout
		pb.redirectErrorStream(true);

		Job compilerJob = new Job("Running External Compiler") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					Process proc = pb.start();
					BufferedReader stdout = new BufferedReader(
							new InputStreamReader(proc.getInputStream()));
					String line = null;
					while ((line = stdout.readLine()) != null) {
						if (line.length() > 0) {
							parseErrors(line, command.getProject(), monitor);
							ConsoleTools.getExternalMessageConsoleStream().println(line);
						}
					}
					proc.waitFor();
					monitor.done();

					return Status.OK_STATUS;
				} catch (final IOException e) {
					//e.printStackTrace();
					PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

						public void run() {
							MessageDialog dialog = new MessageDialog(
									TTCN3UIPlugin.getActiveWorkbenchWindow().getShell(), "Command not found", 
								      null, e.getLocalizedMessage(),
								      MessageDialog.ERROR,
								      new String[] {"Ok"},
								      0);
							dialog.open();
						}
						
					});
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				return Status.CANCEL_STATUS;
			}
		};

		compilerJob.setPriority(Job.DECORATE);
		compilerJob.setUser(true);
		compilerJob.schedule();
	}

	// --------------------------------------------------------------------------
	
	protected void parseErrors(String line, IProject project,
			IProgressMonitor monitor){
	}


}
