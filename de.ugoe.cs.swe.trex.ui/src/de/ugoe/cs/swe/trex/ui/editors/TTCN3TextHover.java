package de.ugoe.cs.swe.trex.ui.editors;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DefaultInformationControl;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextHoverExtension;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import org.eclipse.swt.widgets.Shell;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.ui.editors.text.HTMLTextPresenter;

public class TTCN3TextHover implements ITextHover, ITextHoverExtension {

	private TTCN3Editor editor = null;

	public TTCN3TextHover(TTCN3Editor editor) {
		this.editor = editor;
	}

	public IInformationControlCreator getHoverControlCreator() {
		return new IInformationControlCreator() {

			public IInformationControl createInformationControl(Shell parent) {
				return new DefaultInformationControl(parent, new HTMLTextPresenter(true)); 
			}

		};
	}

	public String getHoverInfo(ITextViewer textViewer, IRegion hoverRegion) {
		if (editor == null)
			return null;

		IdentifierCache iCache = editor.getReconcilingStrategy()
				.getIdentifierCache();

		if (iCache == null) {
			return null;
		}

		LocationAST ast = iCache.get(hoverRegion.getOffset());
	
		if (ast != null && ast.getScope() != null) {
			Scope scope = ast.getScope();
			if (scope == null)
				return ast.toString();

			String currentFilename = scope.getAssociatedFilename();
			Symbol symb = scope.resolve(ast);
			
			if (symb != null) {
				LocationAST node = symb.getDeclarationNode();				
				LocationAST moduleNode = ASTUtil.getModuleNode(node);
				String assocFilename = "";
				
				if (moduleNode!=null && moduleNode.getScope()!=null) {
					assocFilename = moduleNode.getScope().getAssociatedFilename();
				}

				if (node == null)
					return symb.getSymbolDescription();
				
				int line = node.getLine() - 1;
				
				try	{
					if (!currentFilename.equals(assocFilename))
						return symb.getSymbolDescription() + "<br>" +
						   "<b>Defined in:</b><br>" +
						   assocFilename + ":" + line;

					IDocument doc = textViewer.getDocument();
					
					int startOffset = doc.getLineOffset(line);
					int lineLength = doc.getLineLength(line);
					
					if (lineLength == 0)
						return symb.getSymbolDescription();

					return symb.getSymbolDescription() + "<br>" +
					   "<b>Defined in current file:</b><br>" +
					   doc.get(startOffset, lineLength);
				}
				catch (BadLocationException e)
				{
					return symb.getSymbolDescription();
				}
			}		
			
			return "Identifier not declared!";
		}

		return null;
	}

	
	public IRegion getHoverRegion(ITextViewer textViewer, int offset) {
		return new Region(offset, 0);
	}

}
