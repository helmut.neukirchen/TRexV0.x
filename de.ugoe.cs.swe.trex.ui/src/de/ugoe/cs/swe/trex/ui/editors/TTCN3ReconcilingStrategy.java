package de.ugoe.cs.swe.trex.ui.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.reconciler.DirtyRegion;
import org.eclipse.jface.text.reconciler.IReconcilingStrategy;
import org.eclipse.jface.text.reconciler.IReconcilingStrategyExtension;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import antlr.RecognitionException;
import antlr.TokenStreamException;
import antlr.collections.AST;

import com.yourkit.api.Controller;
import com.yourkit.api.ProfilingModes;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3SemanticException;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.misc.IntRangeMap;
import de.ugoe.cs.swe.trex.ui.TTCN3ResourceHelper;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;
import de.ugoe.cs.swe.trex.ui.handlers.DisableFolderHandler;
import de.ugoe.cs.swe.trex.ui.builders.BuilderHelper;
import de.ugoe.cs.swe.trex.ui.builders.IncrementalRefactoringlRuleBuilder;
import de.ugoe.cs.swe.trex.ui.extensionpoints.ExtensionPointHelper;
import de.ugoe.cs.swe.trex.ui.extensionpoints.PostAnalysisContribution;
import de.ugoe.cs.swe.trex.ui.preferences.PreferenceConstants;
import de.ugoe.cs.swe.trex.ui.views.metrics.TTCN3MetricsView;
import de.ugoe.cs.swe.trex.ui.wizards.TTCN3NewProjectWizard;

/**
 * Reconciler strategy which parses the whole editor's content (a TTCN3 file) on
 * a document change.
 */
public class TTCN3ReconcilingStrategy implements IReconcilingStrategy,
		IReconcilingStrategyExtension {

	public final static String MARKER_EXCEPTION = "de.ugoe.cs.swe.trex.ui.exceptionmarker";

	TTCN3Editor fEditor;

	TTCN3Analyzer ttcn3Analyzer = null;

	private ArrayList<Object> plugins = null;

	private IProgressMonitor monitor = null;

	private boolean analyzeAll = false;

	private Set<IFile> analyzeAllFiles = null;

	private static HashMap<IFile, Long> modificationStampMap = new HashMap<IFile, Long>();

	public static final QualifiedName QUERY_NATURE_KEY = new QualifiedName(
			TTCN3UIPlugin.getDefault().getBundle().getSymbolicName(),
			"queryTRexNatureEnabled");

	private ArrayList<Position> fPositions = new ArrayList<Position>();

	// ---------------------------------------------------------------------------

	public TTCN3ReconcilingStrategy(TTCN3Editor anEditor) {
		fEditor = anEditor;
		plugins = ExtensionPointHelper
				.getContributors("de.ugoe.cs.swe.trex.ui.postAnalysisStep");
		TTCN3UIPlugin.debugMessage("creating reconciler for editor ");
	}

	// ---------------------------------------------------------------------------

	public void setDocument(IDocument aDocument) {
		TTCN3UIPlugin.debugMessage("setDocument on reconciler...");

		fEditor.updateOutlinePage();
	}

	// ---------------------------------------------------------------------------

	public void reconcile(DirtyRegion aDirtyRegion, IRegion aRegion) {
		TTCN3UIPlugin.debugMessage("Calling reconcile(DirtyRegion,IRegion)");
		analyze();
	}

	// ---------------------------------------------------------------------------

	public void reconcile(IRegion aRegion) {
		TTCN3UIPlugin.debugMessage("Calling reconcile(IRegion)");
		analyze();
	}

	// ---------------------------------------------------------------------------

	public IdentifierCache getIdentifierCache() {
		if (ttcn3Analyzer == null)
		{
			return null;
		}

		return ttcn3Analyzer.getIdentifierCache();
	}

	// ---------------------------------------------------------------------------

	public IntRangeMap<Scope> getScopeOffsetMap() {
		// if (ttcn3Analyzer == null)
		// return null;

		return ttcn3Analyzer.getScopeOffsetMap();
	}

	// ---------------------------------------------------------------------------

	public LocationAST getRootNode() {
		if (ttcn3Analyzer == null)
		{
			return null;
		}

		return ttcn3Analyzer.getRootNode();
	}

	// ---------------------------------------------------------------------------

	public static class ResourceCountVisitor implements IResourceVisitor {

		private int resourcesCount = 0;

		public boolean visit(IResource resource) throws CoreException {
			if (TTCN3ResourceHelper.isTTCN3File(resource)) {
				resourcesCount++;
			}
			return true;
		}

		public int getResourcesCount() {
			return resourcesCount;
		}

	}

	// ---------------------------------------------------------------------------

	private class AnalysisVisitor implements IResourceVisitor {
		private IProgressMonitor progressMonitor;

		private int resourcesAnalyzed = 1;

		private int totalResources = 0;

		public AnalysisVisitor() {
		}

		public boolean visit(final IResource resource) {
			if (TTCN3ResourceHelper.isTTCN3File(resource)) {
				final IFile file = (IFile) resource;
				if (progressMonitor != null)
				{
					progressMonitor.subTask("Analyzing file ("
							+ resourcesAnalyzed + "/" + totalResources + "): "
							+ file.getFullPath());
				}
				TTCN3UIPlugin.debugMessage("analyzing file: "
						+ file.getFullPath());

				if (((modificationStampMap.get(file) == null) || (!modificationStampMap
						.get(file).equals(file.getModificationStamp())))
						&& checkFolderEnabledProperty(file)) {
					fileBasedAnalysis(file, false);
					modificationStampMap.put(file, new Long(file
							.getModificationStamp()));
				}

				resourcesAnalyzed++;
				if (progressMonitor != null)
				{
					progressMonitor.worked(resourcesAnalyzed);
				}
			}
			return true;
		}

		private boolean checkFolderEnabledProperty(final IFile file) {
			boolean folderEnabled = true;
			IContainer parent = file.getParent();
			if (parent instanceof IFolder) {
				IFolder folder = (IFolder) parent;

				String property = null;
				try {
					property = folder
							.getPersistentProperty(DisableFolderHandler.QUALIFIED_NAME);
				} catch (CoreException e) {
					e.printStackTrace();
				}

				folderEnabled = (property == null);
				if (!folderEnabled)
				{
					TTCN3UIPlugin.debugMessage("  skipping file: " + file.getFullPath());
				}
			}
			return folderEnabled;
		}

		public void setProgressMonitor(IProgressMonitor progressMonitor) {
			this.progressMonitor = progressMonitor;
		}

		public void setTotalResources(int totalResources) {
			this.totalResources = totalResources;
		}

		public int getTotalResources() {
			return totalResources;
		}

	}

	// ---------------------------------------------------------------------------

	public void analyzeAll(final IFile currentFile, IProgressMonitor monitor,
			boolean callBuilder) {
		TTCN3ReconcilerState.getInstance().setReconciler(null);
		TTCN3UIPlugin.debugMessage("Starting visiting analyzeAll(IFile) ...");

		final TTCN3AnalyzerFlyweightFactory analyzerFactory = TTCN3AnalyzerFlyweightFactory
				.getInstance();

		final IProject p = currentFile.getProject();
		try {
			ResourceCountVisitor countVisitor = new ResourceCountVisitor();
			p.accept(countVisitor);

			final AnalysisVisitor analysisVisitor = new AnalysisVisitor();
			analysisVisitor.setTotalResources(countVisitor.getResourcesCount());
			if (monitor != null) {
				analysisVisitor.setProgressMonitor(monitor);
				monitor.beginTask("Analyzing project ", analysisVisitor
						.getTotalResources());
				monitor.subTask("Parsing and Creating AST");
			}

			Controller controller = null;
			try {
				if (TTCN3UIPlugin.isCPUProfiling()
						|| TTCN3UIPlugin.isMemProfiling()) {
					controller = new Controller();
				}
				if (TTCN3UIPlugin.isCPUProfiling())
				{
					controller.startCPUProfiling(ProfilingModes.CPU_SAMPLING,
							null);
				}

				if (TTCN3UIPlugin.isMemProfiling())
				{
					controller.startAllocationRecording(false, 0, false, 0);
				}

				p.accept(analysisVisitor);
				analyzerFactory.postProcess();

				if (TTCN3UIPlugin.isCPUProfiling()) {
					controller
							.captureSnapshot(ProfilingModes.SNAPSHOT_WITH_HEAP);
					controller.stopCPUProfiling();
				}

				if (TTCN3UIPlugin.isMemProfiling()) {
					controller
							.captureSnapshot(ProfilingModes.SNAPSHOT_WITH_HEAP);
					controller.stopAllocationRecording();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					fEditor.updateOutlinePage();
				}
			});

			if (BuilderHelper.isBuilderEnabled(p,
					IncrementalRefactoringlRuleBuilder.BUILDER_ID)
					&& (callBuilder)) {
				p.build(IncrementalProjectBuilder.FULL_BUILD,
						"de.ugoe.cs.swe.trex.ui.rulebuilder", null, monitor);
				TTCN3ReconcilerState.getInstance().setReconciler(this);
				TTCN3ReconcilerState.getInstance().setChanged();
				TTCN3ReconcilerState.getInstance().notifyObservers();
			}

			executePostAnalysisExtensionPoint();

			TTCN3UIPlugin.debugMessage("...visiting analyzeAll finished.");

		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	// ---------------------------------------------------------------------------

	public void analyzeAll(final Set<IFile> files, IFile main) {
		analyzeAllFiles = files;
		analyzeAll = true;
	}

	// ---------------------------------------------------------------------------

	public void analyzeAllInternal(final Set<IFile> files, IFile main) {
		TTCN3ReconcilerState.getInstance().setReconciler(null);
		TTCN3UIPlugin
				.debugMessage("Starting visiting analyzeAll(Set<IFile>) ...");

		TTCN3UIPlugin
				.debugMessage("  starting parsing/ast creation visitor... ");
		long time = System.currentTimeMillis();

		final TTCN3AnalyzerFlyweightFactory analyzerFactory = TTCN3AnalyzerFlyweightFactory
				.getInstance();

		final AnalysisVisitor analysisVisitor = new AnalysisVisitor();
		IProject p = null;
		for (IFile file : files) {
			final ResourceCountVisitor countVisitor = new ResourceCountVisitor();
			analysisVisitor.setTotalResources(countVisitor.getResourcesCount());
			try {
				file.accept(countVisitor);
				file.accept(analysisVisitor);
				if (p == null)
				{
					p = file.getProject();
				}
			} catch (CoreException e) {
				e.printStackTrace();
			}

		}
		analyzerFactory.postProcess();

		TTCN3UIPlugin
				.debugMessage("  ...parsing/ast creation visitor finished in "
						+ new Long(System.currentTimeMillis() - time) + " ms");

		Display.getDefault().syncExec(new Runnable() {
			public void run() {
				fEditor.updateOutlinePage();
			}
		});
		TTCN3UIPlugin.debugMessage("  starting metrics visitor... ");
		time = System.currentTimeMillis();
		// IProject p = main.getProject();

		TTCN3UIPlugin.debugMessage("  ...metrics visitor finished in "
				+ new Long(System.currentTimeMillis() - time) + " ms");

		try {
			if (BuilderHelper.isBuilderEnabled(p,
					IncrementalRefactoringlRuleBuilder.BUILDER_ID)) {
				p.build(IncrementalProjectBuilder.FULL_BUILD,
						"de.ugoe.cs.swe.trex.ui.rulebuilder", null, monitor);
				TTCN3ReconcilerState.getInstance().setReconciler(this);
				TTCN3ReconcilerState.getInstance().setChanged();
				TTCN3ReconcilerState.getInstance().notifyObservers();
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}

		executePostAnalysisExtensionPoint();

		analyzeAll = false;
		analyzeAllFiles = null;

		TTCN3UIPlugin.debugMessage("...visiting analyzeAll finished.");
	}

	// ---------------------------------------------------------------------------

	public void analyze() {
		TTCN3ReconcilerState.getInstance().setReconciler(null);

		final IFile ff = (IFile) fEditor.getEditorInput().getAdapter(
				IFile.class);
		if (ff == null) {
			displayFunctionalityDisabledWarning();
			return;
		}

		analyzeAll(ff, new NullProgressMonitor(), false);

		if ((analyzeAll) && (analyzeAllFiles.contains(ff))) {
			if (BuilderHelper.isBuilderEnabled(ff.getProject(),
					IncrementalRefactoringlRuleBuilder.BUILDER_ID)) {
				TTCN3ReconcilerState.getInstance().setReconciler(this);
				TTCN3ReconcilerState.getInstance().setChanged();
				TTCN3ReconcilerState.getInstance().notifyObservers();

			}
			analyzeAll = false;
			return;
		}

		TTCN3UIPlugin
				.debugMessage("Starting non-visiting reconciler [analyze()] on file "
						+ ff.getFullPath() + "...");

		List<Exception> exceptions = fileBasedAnalysis(ff, true);

		TTCN3AnalyzerFlyweightFactory.getInstance().postProcess();

		modificationStampMap.put(ff, new Long(ff.getModificationStamp()));

		// Update outline view and display error message in status line
		Display.getDefault().syncExec(new Runnable() {
			public void run() {
				fEditor.updateOutlinePage();
			}
		});

		if (BuilderHelper.isBuilderEnabled(ff.getProject(),
				IncrementalRefactoringlRuleBuilder.BUILDER_ID)) {
			TTCN3ReconcilerState.getInstance().setReconciler(this);
			TTCN3ReconcilerState.getInstance().setChanged();
			TTCN3ReconcilerState.getInstance().notifyObservers();

		}

		if (exceptions.size() <= 0) {
			executePostAnalysisExtensionPoint();
		}

		analyzeAll = false;
		TTCN3UIPlugin.debugMessage("...non-visiting reconciler finished");
	}

	// ---------------------------------------------------------------------------

	private void displayFunctionalityDisabledWarning() {
		MessageDialog
				.openError(
						Display.getDefault().getActiveShell(),
						"TTCN-3 Open File...",
						"Much of TRex functionality will be disabled unless you place your TTCN-3 files within a project.\n");
	}

	// ---------------------------------------------------------------------------

	private void executePostAnalysisExtensionPoint() {
		// run extension points
		if (plugins != null) {
			for (Object c : plugins) {
				if (c instanceof PostAnalysisContribution) {
					((PostAnalysisContribution) c)
							.runPostAnalysisContribution();
				}
			}
		}
	}

	// ---------------------------------------------------------------------------

	private List<Exception> fileBasedAnalysis(final IFile ff,
			boolean editorBased) {
		deleteMarkers(ff, MARKER_EXCEPTION);
		deleteMarkers(ff, "de.ugoe.cs.swe.trex.ui.semanticmarker");

		List<Exception> exceptions = new ArrayList<Exception>();
		TTCN3AnalyzerFlyweightFactory analyzerFactory = TTCN3AnalyzerFlyweightFactory
				.getInstance();

		TTCN3Analyzer localTTCN3Analyzer = null;
		final IFile editorFile = (IFile) fEditor.getEditorInput().getAdapter(
				IFile.class);
		if (editorBased) {
			localTTCN3Analyzer = analyzerFactory.getTTCN3Analyzer(ff
					.getFullPath().toPortableString(), fEditor.getDocument()
					.get());
		} else {
			localTTCN3Analyzer = analyzerFactory.getTTCN3Analyzer(ff, true);
		}

		if (ff.equals(editorFile)) {
			ttcn3Analyzer = localTTCN3Analyzer;
		}

		if ((localTTCN3Analyzer != null)
				&& (localTTCN3Analyzer.getCode() != null)
				&& (localTTCN3Analyzer.getCode().length() > 0)) {
			// if ((localTTCN3Analyzer.getCode().length() > 0)) {
			try {
				long time = System.currentTimeMillis();
				localTTCN3Analyzer.analyze();
				TTCN3UIPlugin
						.debugMessage("...finished file based analysis in "
								+ new Long(System.currentTimeMillis() - time)
								+ " ms");
				analyzerFactory.postProcess();

				try {
					//TODO what is the default value?
					boolean isSemanticAnalysisEnabled = Platform.getPreferencesService().getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_SEMANTIC_ANALYSIS_ENABLEMENT,false,null);
					localTTCN3Analyzer.semanticalAnalysis(isSemanticAnalysisEnabled);
					addSemanticExceptionMarkers(localTTCN3Analyzer, ff);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (!analyzeAll) {
					updateFoldings(ttcn3Analyzer);
					TTCN3UIPlugin.debugMessage("Updating foldings...");
				}
			} catch (TokenStreamException e) {
				exceptions.add(e);
			} catch (Exception e) {
				TTCN3UIPlugin.log(e);
				e.printStackTrace();
				TTCN3UIPlugin.debugMessage("...file based analysis failed!");
			}
			if (localTTCN3Analyzer.getExceptions() != null)
			{
				exceptions.addAll(localTTCN3Analyzer.getExceptions());
			}

			if (!(exceptions.isEmpty())) {
				for (int i = 0; i < exceptions.size(); i++) {
					createMarker(ff, exceptions.get(i), IMarker.SEVERITY_ERROR,
							MARKER_EXCEPTION, null);
				}
			}
		}
		return exceptions;
	}

	// ---------------------------------------------------------------------------
	
	private void addSemanticExceptionMarkers(TTCN3Analyzer analyzer, IFile ff) {
//		IFileEditorInput input = (IFileEditorInput) fEditor.getEditorInput();
//		IFile theFile = input.getFile();
		final ArrayList<TTCN3SemanticException> semanticExceptions = analyzer
				.getSemanticExceptions();
		if (semanticExceptions == null)
		{
			return;
		}
		for (TTCN3SemanticException e : semanticExceptions) {
			if (e.getWarningType() == TTCN3SemanticException.warningEnum.WARNING) {
				if (e.hasStartEndPosition()) {
					Map<String, Object> myAttributes = new Hashtable<String, Object>();
					myAttributes.put(IMarker.CHAR_START, e.getStartChar());
				    myAttributes.put(IMarker.CHAR_END, e.getEndChar());
					createMarker(ff, e, IMarker.SEVERITY_WARNING,
					"de.ugoe.cs.swe.trex.ui.semanticmarker", myAttributes);
				}
				else {
					createMarker(ff, e, IMarker.SEVERITY_WARNING,
					"de.ugoe.cs.swe.trex.ui.semanticmarker", null);
				}
			}
			else {
				createMarker(ff, e, IMarker.SEVERITY_ERROR,
						"de.ugoe.cs.swe.trex.ui.semanticmarker", null);
			}
		}
	}

	// ---------------------------------------------------------------------------

	public static void createMarker(final IFile aFile,
			final Exception anException, final int severity,
			final String markerId, Map<String, Object> markerAttributes ) {
		try {
			if (markerAttributes == null ) {
				markerAttributes = new Hashtable<String, Object>();
			}
			markerAttributes.put(IMarker.SEVERITY, Integer.valueOf(severity));
			markerAttributes.put(IMarker.PRIORITY, Integer
					.valueOf(IMarker.PRIORITY_HIGH));
			markerAttributes.put(IMarker.MESSAGE, anException.getMessage());
			if (anException instanceof RecognitionException)
			{
				markerAttributes.put(IMarker.LINE_NUMBER,
						((RecognitionException) anException).getLine());
			}
			if (anException instanceof TokenStreamException) {
				String msg = ((TokenStreamException) anException).toString();
				Pattern p = Pattern.compile("line ([0-9]+):[0-9]+:.*");
				Matcher m = p.matcher(msg);
				if (m.matches()) {
					if (m.groupCount() > 0) {
						String lineStr = m.group(m.groupCount());
						int line = Integer.parseInt(lineStr);
						markerAttributes.put(IMarker.LINE_NUMBER, line);
					}
				}
			}
			IMarker marker = aFile.createMarker(markerId);
			marker.setAttributes(markerAttributes);
		} catch (CoreException e) {
			TTCN3UIPlugin.log(e);
		}
	}
	
	// ---------------------------------------------------------------------------

	/**
	 * Delete all <code>IMarker</code> objects of the given type from the
	 * given file.
	 * 
	 * @param file
	 *            the <code>IFile</code> to delete markers from
	 * @param markerType
	 *            the type of markers to delete
	 */
	void deleteMarkers(final IFile file, final String markerType) {
		try {
			file.deleteMarkers(markerType, true, IResource.DEPTH_INFINITE);
			// TTCN3UIPlugin.debugMessage("Deleted '" + markerType
			// + "' Markers...");
		} catch (CoreException e) {
			TTCN3UIPlugin.log(e);
		}
	}

	// ---------------------------------------------------------------------------

	public Object[] getRootElements() {
		List<AST> rootElements = new ArrayList<AST>();

		if (ttcn3Analyzer == null)
		{
			return rootElements.toArray();
		}

		if (ttcn3Analyzer.getRootNode() != null) {
			AST tmp = ttcn3Analyzer.getRootNode();
			do {
				rootElements.add(tmp);
			} while ((tmp = tmp.getNextSibling()) != null);
		}

		return rootElements.toArray();
	}

	// ---------------------------------------------------------------------------

	public AST getRoot() {
		if (ttcn3Analyzer != null) {
			return ttcn3Analyzer.getRootNode();
		}

		return null;
	}

	// ---------------------------------------------------------------------------

	public TTCN3Analyzer getTTCN3Analyzer() {
		return ttcn3Analyzer;
	}

	// ---------------------------------------------------------------------------

	public void initialReconcile() {
		TTCN3UIPlugin.debugMessage("initialReconcile() has been triggered...");

		final IFile ff = (IFile) fEditor.getEditorInput().getAdapter(
				IFile.class);

		if (ff == null) {
			fEditor.getSite().getShell().getDisplay().syncExec(new Runnable() {
				public void run() {
					displayFunctionalityDisabledWarning();
				}
			});
			return;
		}

		if (!ff.exists() || !ff.getProject().exists())
		{
			return;
		}

		final IProject p = ff.getProject();

		TTCN3UIPlugin.debugMessage("  file: " + ff.getFullPath());
		TTCN3UIPlugin.debugMessage("  project: " + p.getName());
		TTCN3UIPlugin.debugMessage("  confirming nature...");
		confirmNature(p);
		
		TTCN3AnalyzerFlyweightFactory analyzerFactory = TTCN3AnalyzerFlyweightFactory
				.getInstance();
		
		if (analyzerFactory.hasAnalyzer(ff)) {
			TTCN3UIPlugin.debugMessage("  file exists in analyzerFactory... ");
			ttcn3Analyzer = analyzerFactory.getTTCN3Analyzer(ff);

			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					fEditor.updateOutlinePage();
				}
			});
			if (BuilderHelper.isBuilderEnabled(ff.getProject(),
					IncrementalRefactoringlRuleBuilder.BUILDER_ID)) {
				TTCN3UIPlugin.debugMessage("  notiying observers... ");
				TTCN3ReconcilerState.getInstance().setReconciler(this);
				TTCN3ReconcilerState.getInstance().setChanged();
				TTCN3ReconcilerState.getInstance().notifyObservers(
						new Boolean(true));

			}
			updateMetricsView(ff);
			return;
		}

		TTCN3UIPlugin
				.debugMessage("  triggering analyzeall and switchting project... ");
		analyzerFactory.setLastProjectAnalyzed(p);
		analyzerFactory.addAnalyzedProject(p);
		analyzeAll(ff, monitor, true);
		updateMetricsView(ff);

	}

	// ---------------------------------------------------------------------------

	private void confirmNature(final IProject p) {
		PlatformUI.getWorkbench().getDisplay().syncExec(
				new Runnable() {

					public void run() {
						try {
							String property = p
									.getPersistentProperty(QUERY_NATURE_KEY);
							if ((property == null) || (property.equals("true"))) {
								p.setPersistentProperty(QUERY_NATURE_KEY,
										"true");
								if (!p
										.hasNature(TTCN3NewProjectWizard.NATURE_ID)) {
									boolean enableNature = MessageDialog
											.openConfirm(
													fEditor.getEditorSite()
															.getShell(),
													"Enable TRex Nature",
													"You are working with TTCN-3 files in a project which does not have the TRex nature enabled. "
															+ "Without the TRex nature, some "
															+ "advanced TRex features are not available. \n\nDo you want to enable "
															+ "the TRex nature? \n(You will be asked this question only once per project.)");

									if (enableNature) {
										IProjectDescription description = p
												.getDescription();
										List<String> newIds = new ArrayList<String>();
										newIds.addAll(Arrays.asList(description
												.getNatureIds()));
										int index = newIds
												.indexOf(TTCN3NewProjectWizard.NATURE_ID);
										if (index == -1)
										{
											newIds.add(TTCN3NewProjectWizard.NATURE_ID);
										}
										else
										{
											newIds.remove(index);
										}
										description
												.setNatureIds((String[]) newIds
														.toArray(new String[newIds
																.size()]));
										p.setDescription(description,
												new NullProgressMonitor());
										BuilderHelper
												.addBuilderToProject(
														p,
														IncrementalRefactoringlRuleBuilder.BUILDER_ID);
									}
									p.setPersistentProperty(QUERY_NATURE_KEY,
											"false");
								}
							}
						} catch (CoreException e) {
							e.printStackTrace();
						}
					}

				});
	}

	// ---------------------------------------------------------------------------

	private void updateMetricsView(final IFile ff) {
		// show the current file in the Metrics view if none selected
		TTCN3UIPlugin.debugMessage("updating metrics view... ");
		long time = System.currentTimeMillis();

		TTCN3MetricsView.getMetricsFiles().clear();
		if (TTCN3MetricsView.getMetricsFiles().size() < 1) {
			TTCN3MetricsView.processFile(ff);
		}
		TTCN3MetricsView.refreshViewer();
		TTCN3UIPlugin.debugMessage("...metrics view updated in "
				+ new Long(System.currentTimeMillis() - time) + " ms");
	}

	public void updateFoldings(final TTCN3Analyzer analyzer) {
		fPositions.clear();

		if (analyzer == null)
		{
			return;
		}
		
		LocationAST ast = analyzer.getRootNode();
		
		if (ast == null)
		{
			return;
		}

		ArrayList<LocationAST> nodeList = new ArrayList<LocationAST>();

		IPreferencesService prefs = Platform.getPreferencesService();

		if (!prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FOLDING_ENABLEMENT,false,null))
		{
			return;
		}

		if (prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FOLDING_TYPE,false,null))
		{
			nodeList.addAll(ASTUtil.findTypeNodes(ast,
					TTCN3LexerTokenTypes.TypeDef));
		}

		if (prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FOLDING_TEMPLATE,false,null))
		{
			nodeList.addAll(ASTUtil.findTypeNodes(ast,
					TTCN3LexerTokenTypes.TemplateDef));
		}

		if (prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FOLDING_COMPONENT,false,null))
		{
			nodeList.addAll(ASTUtil.findTypeNodes(ast,
					TTCN3LexerTokenTypes.ComponentDef));
		}

		if (prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FOLDING_GROUP,false,null))
		{
			nodeList.addAll(ASTUtil.findTypeNodes(ast,
					TTCN3LexerTokenTypes.GroupDef));
		}

		if (prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FOLDING_ALTSTEP,false,null))
		{
			nodeList.addAll(ASTUtil.findTypeNodes(ast,
					TTCN3LexerTokenTypes.AltstepDef));
		}

		if (prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FOLDING_TESTCASE,false,null))
		{
			nodeList.addAll(ASTUtil.findTypeNodes(ast,
					TTCN3LexerTokenTypes.TestcaseDef));
		}

		if (prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FOLDING_FUNCTION,false,null))
		{
			nodeList.addAll(ASTUtil.findTypeNodes(ast,
					TTCN3LexerTokenTypes.FunctionDef));
		}

		if (prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FOLDING_MODULE,false,null))
		{
			nodeList.addAll(ASTUtil.findTypeNodes(ast,
					TTCN3LexerTokenTypes.TTCN3Module));
		}

		for (LocationAST n : nodeList) {
			
			// Ignore Nodes which occupy only a single line (DOES NOT WORK)
			//if (n.getLine() == n.getEndLine()) continue;
			
			int endOffset = n.getEndOffset();
			int line = 1;

			try {
				line = fEditor.getDocument().getLineOfOffset(endOffset);
			} catch (BadLocationException e) {
				line = 1;
			}

			int startLine = line;

			while (line == startLine) {
				endOffset++;
				try {
					line = fEditor.getDocument().getLineOfOffset(endOffset);
				} catch (BadLocationException e) {
					line = 1;
					break;
				}
			}
			
			if (endOffset > fEditor.getDocument().getLength())
			{
				endOffset--;
			}
			
			if (n.getOffset() >= 0) {
				// this throws due to first param being -1
				Position pos = new Position(n.getOffset(), endOffset - n.getOffset()); 
				fPositions.add(pos);
			} else {
				LocationAST n2 = n;
				while (n2 != null) {
					TTCN3UIPlugin.debugMessage("FoldingError: Error, invalid offset for node " + n2.getText());
					TTCN3UIPlugin.debugMessage("FoldingError: Offsets: off:"+n2.getOffset()+" end:"+endOffset+" realend:"+n2.getEndOffset());
					TTCN3UIPlugin.debugMessage("FoldingError: Offsets: line:"+n2.getLine()+" col:"+n2.getColumn()+" endline:"+n2.getEndLine());
					TTCN3UIPlugin.debugMessage("FoldingError: type: "+n2.getType());
	
					n2 = n2.getParent();
					TTCN3UIPlugin.debugMessage("FoldingError: rinting parent ::");
			}
			}
		}

		fEditor.updateFoldingStructure(fPositions);
	}

	// ---------------------------------------------------------------------------

	public void setProgressMonitor(IProgressMonitor monitor) {
		this.monitor = monitor;
	}

}
