package de.ugoe.cs.swe.trex.ui.views.findreferences;

import de.ugoe.cs.swe.trex.core.analyzer.astutil.ReferenceWithContext;

/**
 * An AtomicElement holds a single reference. It is used by FindReferences to
 * store information about the results.
 * 
 */
public class AtomicElement {
	private CompoundElement compoundElement = null;
	private ReferenceWithContext reference;

	/**
	 * Constructs a new AtomicElement object.
	 */
	public AtomicElement(CompoundElement compoundElement,
			ReferenceWithContext reference) {
		compoundElement.addAtomic(this);
		this.compoundElement = compoundElement;
		this.reference = reference;
	}

	public CompoundElement getCompoundElement() {
		return compoundElement;
	}

	public ReferenceWithContext getReference() {
		return reference;
	}
}
