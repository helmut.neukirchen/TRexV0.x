package de.ugoe.cs.swe.trex.ui.editors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.ui.actions.IJavaEditorActionDefinitionIds;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.SubContributionItem;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.ITextViewerExtension5;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.hyperlink.IHyperlinkDetector;
import org.eclipse.jface.text.hyperlink.URLHyperlinkDetector;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.ICharacterPairMatcher;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.text.source.projection.ProjectionAnnotation;
import org.eclipse.jface.text.source.projection.ProjectionAnnotationModel;
import org.eclipse.jface.text.source.projection.ProjectionSupport;
import org.eclipse.jface.text.source.projection.ProjectionViewer;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.ide.IGotoMarker;
import org.eclipse.ui.part.EditorActionBarContributor;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditor;
import org.eclipse.ui.texteditor.ContentAssistAction;
import org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds;
import org.eclipse.ui.texteditor.SourceViewerDecorationSupport;
import org.eclipse.ui.texteditor.StatusLineContributionItem;
import org.eclipse.ui.texteditor.TextOperationAction;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;
import de.ugoe.cs.swe.trex.ui.actions.GotoMatchingBracketAction;
import de.ugoe.cs.swe.trex.ui.actions.ITTCN3ActionConstants;
import de.ugoe.cs.swe.trex.ui.editors.outline.TTCN3OutlinePage;
import de.ugoe.cs.swe.trex.ui.editors.text.TTCN3PairMatcher;
import de.ugoe.cs.swe.trex.ui.preferences.PreferenceConstants;
import de.ugoe.cs.swe.trex.ui.views.findreferences.TTCN3FindReferencesSearchQuery;
public class TTCN3Editor extends AbstractDecoratedTextEditor implements
IGotoMarker, IDocumentListener {

	@Override
	public void gotoMarker(IMarker marker) {
		moveCursor(marker.getAttribute(IMarker.LINE_NUMBER, 0));
	}

	public TTCN3HyperlinkDetector fHyperlinkDetector;

	public TTCN3Hyperlink fHyperlink;

	private final TTCN3MultiPageEditor fMultiPageEditor;

	private final TTCN3ReconcilingStrategy fReconcilingStrategy;

	private TTCN3OutlinePage fOutlinePage;

	private TTCN3DocumentProvider documentProvider;

	private TTCN3Configuration configuration;

	private ProjectionSupport projectionSupport;

	private Annotation[] oldAnnotations;
	private ProjectionAnnotationModel annotationModel;

	// ---------------------------------------------------------------------------

	public TTCN3Editor(TTCN3MultiPageEditor aMultiPageEditor) {
		fMultiPageEditor = aMultiPageEditor;
		fReconcilingStrategy = new TTCN3ReconcilingStrategy(this);
		setEditorContextMenuId(TTCN3UIPlugin.PLUGIN_ID + ".editors.TTCN3Editor"); //$NON-NLS-1$
		setKeyBindingScopes(new String[] { TTCN3UIPlugin.PLUGIN_ID
				+ ".editorScope" }); //$NON-NLS-1$
		if (configuration == null) {
			configuration = new TTCN3Configuration(this);
		}
	}

	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		TTCN3UIPlugin.debugMessage("Editor: createPartControl.");
		//START modified by sukiyuki 2011-5-20
		//ProjectionViewer viewer = (ProjectionViewer) getSourceViewer();
		TTCN3SourceViewer viewer = (TTCN3SourceViewer) getSourceViewer();
		//END   modified by sukiyuki 2011-5-20

		projectionSupport = new ProjectionSupport(viewer,
				getAnnotationAccess(), getSharedColors());
		projectionSupport.install();

		// turn projection mode on
		//START modified by sukiyuki 2011-5-20
		//viewer.doOperation(ProjectionViewer.TOGGLE);
		viewer.doOperation(ProjectionViewer.TOGGLE);
		//END   modified by sukiyuki 2011-5-20

		annotationModel = viewer.getProjectionAnnotationModel();

		final IFile editorFile = (IFile) getEditorInput().getAdapter(
				IFile.class);

		TTCN3Analyzer localTTCN3Analyzer = null;

		if (editorFile != null) {
			localTTCN3Analyzer = TTCN3AnalyzerFlyweightFactory.getInstance().getTTCN3Analyzer(
					editorFile.getFullPath().toPortableString());
		}

		if (localTTCN3Analyzer != null) {
			if (localTTCN3Analyzer.getRootNode() != null) {
				fReconcilingStrategy.updateFoldings(localTTCN3Analyzer);
			}
		}
	}

	public void updateFoldingStructure(ArrayList<Position> positions) {
		Annotation[] annotations = new Annotation[positions.size()];

		// this will hold the new annotations along
		// with their corresponding positions
		HashMap<ProjectionAnnotation, Position> newAnnotations = new HashMap<ProjectionAnnotation, Position>();

		for (int i = 0; i < positions.size(); i++) {
			ProjectionAnnotation annotation = new ProjectionAnnotation();

			newAnnotations.put(annotation, positions.get(i));

			annotations[i] = annotation;
		}

		annotationModel.modifyAnnotations(oldAnnotations, newAnnotations, null);

		oldAnnotations = annotations;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractTextEditor#createSourceViewer(org.eclipse.swt.widgets.Composite,
	 *      org.eclipse.jface.text.source.IVerticalRuler, int)
	 */
	@Override
	protected ISourceViewer createSourceViewer(Composite parent,
			IVerticalRuler ruler, int styles) {
		//START modified by sukiyuki 2011-5-20
		//ISourceViewer viewer = new ProjectionViewer(parent, ruler,
		ISourceViewer viewer = new TTCN3SourceViewer(parent, ruler,
				getOverviewRuler(), isOverviewRulerVisible(), styles);
		//END   modified by sukiyuki 2011-5-20

		// ensure decoration support has been created and configured.
		getSourceViewerDecorationSupport(viewer);

		return viewer;
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void initializeEditor() {
		super.initializeEditor();

		documentProvider = new TTCN3DocumentProvider();
		setDocumentProvider(documentProvider);
		if (configuration == null) {
			configuration = new TTCN3Configuration(this);
		}
		setSourceViewerConfiguration(configuration);

	}

	// ---------------------------------------------------------------------------

	@Override
	protected void configureSourceViewerDecorationSupport(
			SourceViewerDecorationSupport support) {
		//START modified by sukiyuki 2011-2-7
		//match bracket
		support.setCharacterPairMatcher(fBracketMatcher);
		support.setMatchingCharacterPainterPreferenceKeys(MATCHING_BRACKETS, MATCHING_BRACKETS_COLOR);
		//END   modified by sukiyuki 2011-2-7
		super.configureSourceViewerDecorationSupport(support);

		// TTCN3PairMatcher pairMatcher = new TTCN3PairMatcher();
		// support.setCharacterPairMatcher(pairMatcher);
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void createActions() {
		super.createActions();

		// Add comment action
		IAction action = new TextOperationAction(TTCN3UIPlugin.getDefault()
				.getResourceBundle(), "Editor.Comment.", this,
				ITextOperationTarget.PREFIX);
		action.setActionDefinitionId(IJavaEditorActionDefinitionIds.COMMENT);
		// TODO Remove jdt dependency by replacing
		// IJavaEditorActionDefinitionIds with ITTCN3ActionConstants
		// action.setActionDefinitionId(ITTCN3ActionConstants.COMMENT);
		setAction(ITTCN3ActionConstants.COMMENT, action);

		// Add uncomment action
		action = new TextOperationAction(TTCN3UIPlugin.getDefault()
				.getResourceBundle(), "Editor.Uncomment.", this,
				ITextOperationTarget.STRIP_PREFIX);
		action.setActionDefinitionId(IJavaEditorActionDefinitionIds.UNCOMMENT);
		// TODO Remove jdt dependency by replacing
		// IJavaEditorActionDefinitionIds with ITTCN3ActionConstants
		// action.setActionDefinitionId(ITTCN3ActionConstants.UNCOMMENT);
		setAction(ITTCN3ActionConstants.UNCOMMENT, action);

		Action caAction = new ContentAssistAction(TTCN3UIPlugin.getDefault()
				.getResourceBundle(), "ContentAssistProposal.", this);
		String id = ITextEditorActionDefinitionIds.CONTENT_ASSIST_PROPOSALS;
		caAction.setActionDefinitionId(id);
		setAction("ContentAssistProposal", caAction);
		markAsStateDependentAction("ContentAssistProposal", true);
		action= new GotoMatchingBracketAction(this);
		action.setActionDefinitionId("de.ugoe.cs.swe.trex.ui.editors.goto.matching.bracket");
		setAction(GotoMatchingBracketAction.GOTO_MATCHING_BRACKET, action);

		// getSourceViewer().getTextWidget().addExtendedModifyListener(
		// new ExtendedModifyListener() {
		// public void modifyText(ExtendedModifyEvent event) {
		// int start = event.start;
		// int length = event.length -
		// event.replacedText.length();
		// rs.getScopeOffsetMap().shift(start, length);
		// }
		// });

		// Add folding actions here
	}

	// ---------------------------------------------------------------------------

	public Integer getCaretOffset() {
		//START modified by sukiyuki 2011-5-20
		//return Integer.valueOf( ((ProjectionViewer)getSourceViewer()).widgetOffset2ModelOffset(getSourceViewer().getTextWidget()
		return Integer.valueOf( ((TTCN3SourceViewer)getSourceViewer()).widgetOffset2ModelOffset(getSourceViewer().getTextWidget()
				.getCaretOffset()));
		//END   modified by sukiyuki 2011-5-20
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void handleCursorPositionChanged() {
		// for (int i=0; i <
		// getEditorSite().getActionBars().getStatusLineManager().getItems().length;
		// i++)
		// System.out.println(getEditorSite().getActionBars().getStatusLineManager().getItems()[i].getId());

		IContributionItem contributionItem = getEditorSite().getActionBars()
		.getStatusLineManager().find(
				TTCN3UIPlugin.PLUGIN_ID + ".statusline.linenumber");
		SubContributionItem subContributionItem = (SubContributionItem) contributionItem;
		StatusLineContributionItem statusLineContributionItem = (StatusLineContributionItem) subContributionItem.getInnerItem();
		if (statusLineContributionItem != null) {
			statusLineContributionItem.setText(getCursorPosition() + " ("
					//START modified by sukiyuki 2011-5-20
					//+ ((ProjectionViewer)getSourceViewer()).widgetOffset2ModelOffset(getSourceViewer().getTextWidget().getCaretOffset()) + ")");
					+ ((TTCN3SourceViewer)getSourceViewer()).widgetOffset2ModelOffset(getSourceViewer().getTextWidget().getCaretOffset()) + ")");
		}
		getEditorSite().getActionBars().getStatusLineManager().update(true);
		//END   modified by sukiyuki 2011-5-20

		//
		// Added by Ingvi and �mar 13.11.2010 - 24.11.2010
		//

		// Clear old markers
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();

		IdentifierCache iCache = getReconcilingStrategy().getIdentifierCache();
		if (iCache == null) {
			return;
		}

		LocationAST currentLocation = iCache.get(getSourceViewer().getTextWidget().getCaretOffset());
		if (currentLocation == null) {
			return;
		}

		Scope scope = currentLocation.getScope();
		if (scope == null) {
			return;
		}
		String currentFilename = scope.getAssociatedFilename();
		String[] aCurrentFilename = currentFilename.split("/");
		String nameOfCurrentProject = aCurrentFilename[1];
		String nameOfCurrentFile = aCurrentFilename[2];

		IProject currentProject = root.getProject(nameOfCurrentProject);
		IFile currentFile = currentProject.getFile(nameOfCurrentFile);

		try {
			currentFile.deleteMarkers(NewSearchUI.SEARCH_MARKER, false, IResource.DEPTH_INFINITE);
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			//TODO: Why does this keep producing tons of exceptions ?
		}

		//Find declaration node
		Symbol symb = scope.resolve(currentLocation);
		if (symb == null) {
			return;
		}

		LocationAST declarationNode = symb.getDeclarationNode();
		if (declarationNode == null) {
			return;
		}

		String declarationFilename = declarationNode.getScope().getAssociatedFilename();
		String[] aDeclarationFilename = declarationFilename.split("/");
		String nameOfProject = aDeclarationFilename[1];
		String nameOfFile = aDeclarationFilename[2];

		IProject project = root.getProject(nameOfProject);
		IFile declarationFile = project.getFile(nameOfFile);

		// Check if mark occurrences is enabled
		boolean isMarkOccurrencesEnabled = Platform.getPreferencesService().getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_MARK_OCCURRENCES_ENABLEMENT,false,null);
		if (isMarkOccurrencesEnabled) {
			return;
		}

		// Find all references
		if (currentLocation.getType() != TTCN3LexerTokenTypes.IDENTIFIER) {
			return;
		}

		TTCN3FindReferencesSearchQuery query = new TTCN3FindReferencesSearchQuery(currentLocation);
		if (query.getQueryNode() == null) {
			return;
		}

		IProgressMonitor nullMonitor = new NullProgressMonitor();
		query.run(nullMonitor, currentFilename);

		IMarker marker = null;
		
		// Mark the declaration position if it is the current file
		if (currentFilename.equals(declarationFilename)) {
			try {
				marker = declarationFile.createMarker(NewSearchUI.SEARCH_MARKER);
				Map<String, Object> attributes = new HashMap<String, Object>();
				attributes.put(IMarker.CHAR_START, new Integer(declarationNode.getOffset()));
				attributes.put(IMarker.CHAR_END, new Integer(declarationNode.getEndOffset()));
				marker.setAttributes(attributes);
			} catch (CoreException e) {
//				e.printStackTrace();
				//TODO: Why does this keep producing tons of exceptions ?
			}
		}
		//
		//End of add
		//

	}

	// ---------------------------------------------------------------------------

	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class aClass) {
		Object adapter;
		if (aClass.equals(IContentOutlinePage.class)) {
			if (fOutlinePage == null) {
				fOutlinePage = new TTCN3OutlinePage(this);
			}
			// fOutlinePage.update();
			adapter = fOutlinePage;
		} else {
			adapter = super.getAdapter(aClass);
		}
		return adapter;
	}

	// ---------------------------------------------------------------------------

	@Override
	public void dispose() {
		if (fOutlinePage != null) {
			fOutlinePage.dispose();
			fOutlinePage = null;
		}
		configuration.dispose();
		super.dispose();
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void editorContextMenuAboutToShow(IMenuManager aMenu) {
		super.editorContextMenuAboutToShow(aMenu);

		addAction(aMenu, IWorkbenchActionConstants.MB_ADDITIONS,
				ITTCN3ActionConstants.COMMENT);
		addAction(aMenu, IWorkbenchActionConstants.MB_ADDITIONS,
				ITTCN3ActionConstants.UNCOMMENT);
	}

	// ---------------------------------------------------------------------------

	@Override
	public void markInNavigationHistory() {
		getEditorSite().getPage().getNavigationHistory().markLocation(
				fMultiPageEditor);
	}

	// ---------------------------------------------------------------------------

	public IDocument getDocument() {
		return getSourceViewer().getDocument();
	}

	// ---------------------------------------------------------------------------

	public int getCursorLine() {
		int line = -1;
		ISourceViewer sourceViewer = getSourceViewer();
		if (sourceViewer != null) {
			StyledText styledText = sourceViewer.getTextWidget();
			int caret = widgetOffset2ModelOffset(sourceViewer, styledText
					.getCaretOffset());
			IDocument document = sourceViewer.getDocument();
			if (document != null) {
				try {
					line = document.getLineOfOffset(caret) + 1;
				} catch (BadLocationException e) {
					TTCN3UIPlugin.log(e);
				}
			}
		}
		return line;
	}

	// ---------------------------------------------------------------------------

	public TTCN3ReconcilingStrategy getReconcilingStrategy() {
		return fReconcilingStrategy;
	}

	// ---------------------------------------------------------------------------

	public Object[] getRootElements() {
		return fReconcilingStrategy.getRootElements();
	}

	// ---------------------------------------------------------------------------

	public void updateOutlinePage() {
		TTCN3UIPlugin.debugMessage("Updating Outline Page...");
		if (fOutlinePage != null) {
			fOutlinePage.update();
		}
	}

	// ---------------------------------------------------------------------------

	public void moveCursor(int aLine) {
		ISourceViewer sourceViewer = getSourceViewer();
		try {
			int offset = getDocument().getLineOffset(aLine - 1);
			sourceViewer.setSelectedRange(offset, 0);
			sourceViewer.revealRange(offset, 0);
		} catch (BadLocationException e) {
			TTCN3UIPlugin.log(e);
		}

		//fHyperlink.open();
		//getHyperlinkDetectors(sourceViewer);
	}

	// ---------------------------------------------------------------------------

	public void moveCursorOffset(int offset) {
		ISourceViewer sourceViewer = getSourceViewer();

		sourceViewer.setSelectedRange(offset, 0);
		sourceViewer.revealRange(offset, 0);

	}

	// ---------------------------------------------------------------------------

	public void selectRange(int offset, int rangeLength) {
		ISourceViewer sourceViewer = getSourceViewer();

		sourceViewer.setSelectedRange(offset, rangeLength);
		sourceViewer.revealRange(offset, rangeLength);
	}

	// ---------------------------------------------------------------------------

	@Override
	protected IStatusLineManager getStatusLineManager() {
		IStatusLineManager manager;
		IEditorActionBarContributor contributor = fMultiPageEditor
		.getEditorSite().getActionBarContributor();
		if (contributor != null
				&& contributor instanceof EditorActionBarContributor) {
			manager = ((EditorActionBarContributor) contributor)
			.getActionBars().getStatusLineManager();
		} else {
			manager = null;
		}
		return manager;
	}

	// ---------------------------------------------------------------------------

	public void displayErrorMessage(String aMessage) {
		IStatusLineManager manager = getStatusLineManager();
		if (manager != null) {
			manager.setErrorMessage(aMessage);
		}
	}

	// ---------------------------------------------------------------------------

	public SourceViewer getTextViewer() {
		return (SourceViewer) getSourceViewer();
	}

	// ---------------------------------------------------------------------------

	public TTCN3ReconcilingStrategy getTTCN3ReconcilingStrategy() {
		return fReconcilingStrategy;
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void initializeKeyBindingScopes() {
		setKeyBindingScopes(new String[] { "de.ugoe.cs.swe.trex.editorScope" }); //$NON-NLS-1$
	}

	// ---------------------------------------------------------------------------

	public static LocationAST getEditorAST(TTCN3Editor editor) {
		if (editor == null) {
			return null;
		}

		Integer offset = editor.getCaretOffset();

		if (offset == null) {
			return null;
		}

		return editor.getReconcilingStrategy().getIdentifierCache().get(offset);
	}

	// ---------------------------------------------------------------------------

	@Override
	public TTCN3DocumentProvider getDocumentProvider() {
		return documentProvider;
	}

	public TTCN3Configuration getConfiguration() {
		return configuration;
	}

	// ---------------------------------------------------------------------------

	@Override
	public boolean isSaveAsAllowed() {
		return true;
	}

	/**
	 * The implementation of this <code>AbstractTextEditor</code> method asks
	 * the user for the workspace path of a file resource and saves the document
	 * there.
	 * <p>
	 * XXX: This method will be removed in 3.3: for now tell the subclass to
	 * handle it.
	 * </p>
	 * 
	 * @param progressMonitor
	 *            the progress monitor to be used
	 */
	@Override
	protected void performSaveAs(IProgressMonitor progressMonitor) {
		IPreferenceStore store = EditorsUI.getPreferenceStore();
		String key = getEditorSite().getId() + ".internal.delegateSaveAs"; //$NON-NLS-1$
		store.setValue(key, true);
		super.performSaveAs(progressMonitor);
	}

	public IHyperlinkDetector[] getHyperlinkDetectors(ITextViewer sourceViewer) {
		return new IHyperlinkDetector[] { new TTCN3HyperlinkDetector(),
				new URLHyperlinkDetector() };
	}

//	@Override
	public void documentAboutToBeChanged(DocumentEvent event) {
	}

//	@Override
	public void documentChanged(DocumentEvent event) {
	}
	/**
	 * Jumps to the matching bracket.
	 */
	public void gotoMatchingBracket() {

		ISourceViewer sourceViewer = getSourceViewer();
		IDocument document = sourceViewer.getDocument();
		if (document == null) {
			return;
		}

		IRegion selection = getSignedSelection(sourceViewer);

		int selectionLength = Math.abs(selection.getLength());
		if (selectionLength > 1) {
			setStatusLineErrorMessage("No bracket selected");
			sourceViewer.getTextWidget().getDisplay().beep();
			return;
		}

		// #26314
		int sourceCaretOffset = selection.getOffset() + selection.getLength();
		if (isSurroundedByBrackets(document, sourceCaretOffset)) {
			sourceCaretOffset -= selection.getLength();
		}

		IRegion region = fBracketMatcher.match(document, sourceCaretOffset);
		if (region == null) {
			setStatusLineErrorMessage("No matching bracket found");
			sourceViewer.getTextWidget().getDisplay().beep();
			return;
		}

		int offset = region.getOffset();
		int length = region.getLength();

		if (length < 1) {
			return;
		}

		int anchor = fBracketMatcher.getAnchor();
		int targetOffset = (ICharacterPairMatcher.RIGHT == anchor) ? offset + 1: offset + length;

		boolean visible = false;
		if (sourceViewer instanceof ITextViewerExtension5) {
			ITextViewerExtension5 extension = (ITextViewerExtension5) sourceViewer;
			visible = (extension.modelOffset2WidgetOffset(targetOffset) > -1);
		} else {
			IRegion visibleRegion = sourceViewer.getVisibleRegion();
			visible = (targetOffset >= visibleRegion.getOffset() && targetOffset <= visibleRegion.getOffset() + visibleRegion.getLength());
		}

		if (!visible) {
			setStatusLineErrorMessage("Bracket is outside selected element");
			sourceViewer.getTextWidget().getDisplay().beep();
			return;
		}

		if (selection.getLength() > 0) {
			targetOffset -= selection.getLength();
		}

		sourceViewer.setSelectedRange(targetOffset, selection.getLength());
		sourceViewer.revealRange(targetOffset, selection.getLength());
	}

	protected IRegion getSignedSelection(ISourceViewer sourceViewer) {
		StyledText text = sourceViewer.getTextWidget();
		Point selection = text.getSelectionRange();

		if (text.getCaretOffset() == selection.x) {
			selection.x = selection.x + selection.y;
			selection.y = -selection.y;
		}

		selection.x = widgetOffset2ModelOffset(sourceViewer, selection.x);

		return new Region(selection.x, selection.y);
	}
	private static boolean isSurroundedByBrackets(IDocument document, int offset) {
		if (offset == 0 || offset == document.getLength()) {
			return false;
		}

		try {
			return isBracket(document.getChar(offset - 1)) &&
			isBracket(document.getChar(offset));
		} catch (BadLocationException e) {
			return false;
		}
	}
	private static boolean isBracket(char character) {
		for (int i = 0; i != BRACKETS.length; ++i) {
			if (character == BRACKETS[i]) {
				return true;
			}
		}
		return false;
	}
	protected final static char[] BRACKETS = { '{', '}', '(', ')', '[', ']', '<', '>' };
	protected TTCN3PairMatcher fBracketMatcher = new TTCN3PairMatcher(BRACKETS);
	/** Preference key for matching brackets */
	protected final static String MATCHING_BRACKETS=  PreferenceConstants.EDITOR_MATCHING_BRACKETS;
	/** Preference key for matching brackets color */
	protected final static String MATCHING_BRACKETS_COLOR=  PreferenceConstants.EDITOR_MATCHING_BRACKETS_COLOR;
}
