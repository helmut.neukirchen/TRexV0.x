package de.ugoe.cs.swe.trex.ui;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

/**
 * Color provider to syntax highlight TTCN3 files
 */
public class TTCN3ColorProvider {
	
	protected Map<String, Color> fColorTable = new HashMap<String, Color>();

	/**
	 * Returns specified color that is stored in the color table. If color not
	 * found in color table then a new instance is created from according
	 * preferences value and stored in color table.
	 * 
	 * @param aName
	 *            the name of the color
	 * @return the color instance
	 */
	public Color getColor(String aName) {
		Color color = fColorTable.get(aName);
		if (color == null) {
			IPreferenceStore store = TTCN3UIPlugin.getDefault()
					.getPreferenceStore();
			RGB rgb = PreferenceConverter.getColor(store, "color." + aName);
			if (rgb == null) {
				color = Display.getCurrent().getSystemColor(
						SWT.COLOR_LIST_FOREGROUND);
				TTCN3UIPlugin
						.logErrorMessage("Undefined color '" + aName + "'");
			} else {
				color = new Color(Display.getCurrent(), rgb);
			}
			fColorTable.put(aName, color);
		}
		return color;
	}

	/**
	 * Release all of the color resources held onto by the color provider.
	 */
	public void dispose() {
		Iterator<Color> colors = fColorTable.values().iterator();
		while (colors.hasNext()) {
			colors.next().dispose();
		}
	}
}
