package de.ugoe.cs.swe.trex.ui.properties;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.dialogs.PropertyPage;

public class TRexPropertyPage extends PropertyPage {

	public TRexPropertyPage() {
		super();		
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.FILL);
		GridLayout layout = new GridLayout();
		composite.setLayout(layout);
		layout.numColumns = 1;
		Label info = new Label(composite, SWT.NONE);
		info.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, true));
		info.setText("The property pages in the category will help you to customize your TRex compiler \n" +
				"and builder settings. The properties can be invoked on projects and files.");
		return composite;
	}

}