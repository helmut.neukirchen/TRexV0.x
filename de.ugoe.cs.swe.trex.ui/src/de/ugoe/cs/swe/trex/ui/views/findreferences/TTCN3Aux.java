package de.ugoe.cs.swe.trex.ui.views.findreferences;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;

/**
 * Class for handy functions dealing with the TTCN3 AST
 * 
 */
public class TTCN3Aux {

	/**
	 * Returns the identifier node from this node. Applicable for all
	 * definitions which require an identifier (i.e. variables, testcases,
	 * functions)
	 * 
	 * @param node
	 *            the input node of interest
	 * @returns the corresponding identifier node
	 */
	static LocationAST getIdentifierNodeFromNode(LocationAST node) {
		LocationAST tempNode = node;
		while (tempNode != null) {
			if (tempNode.getType() == TTCN3LexerTokenTypes.IDENTIFIER)
				break;
			tempNode = tempNode.getFirstChild();
		}
		return tempNode;
	}

	/**
	 * Returns the identifier name from this node. Applicable for all
	 * definitions which require an identifier (i.e. variables, testcases,
	 * functions)
	 * 
	 * @param node
	 *            the input node of interest
	 * @returns the corresponding identifier name
	 */
	static String getIdentifierNameFromNode(LocationAST node) {
		LocationAST idNode = getIdentifierNodeFromNode(node);

		if (idNode != null)
			return idNode.getText();

		return "";
	}
}
