package de.ugoe.cs.swe.trex.ui.properties;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.dialogs.PropertyPage;

import de.ugoe.cs.swe.trex.ui.builders.BuilderHelper;
import de.ugoe.cs.swe.trex.ui.builders.IncrementalCompilerBuilder;
import de.ugoe.cs.swe.trex.ui.builders.IncrementalRefactoringlRuleBuilder;

public class BuilderPropertyPage extends PropertyPage {

	private IProject project;
	private Button enableMetricsCalculationButton;
	private Button enableCompilerButton;
	public final static String RULEBUILDER_ENABLED = "de.ugoe.cs.swe.trex.ui.rulebuilderenabled";
	
	
	public BuilderPropertyPage() {
		super();
		
	}

	protected Control createContents(Composite parent) {
		IResource resource = (IResource) getElement().getAdapter(IResource.class);
		project = resource.getProject();

		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout());
		GridData data = new GridData(GridData.FILL);
		data.grabExcessHorizontalSpace = true;
		composite.setLayoutData(data);

		final Label trexSettingsLabel = new Label(composite, SWT.NONE);
		trexSettingsLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		trexSettingsLabel.setText("TRex Settings (" + project.getFullPath() + ")");

		final Label label = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

		enableMetricsCalculationButton = new Button(composite, SWT.CHECK);
		enableMetricsCalculationButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		enableMetricsCalculationButton.setText("Enable Metrics Calculation and Refactoring Suggestions");
		enableMetricsCalculationButton.setSelection(BuilderHelper.isBuilderEnabled(project, IncrementalRefactoringlRuleBuilder.BUILDER_ID));

		final Label metricsCalculationExplanation = new Label(composite, SWT.NONE);
		metricsCalculationExplanation.setText("(Updating metrics and refactoring suggestions requires rebuild, which is usually triggered by saving a file.)");

		enableCompilerButton = new Button(composite, SWT.CHECK);
		enableCompilerButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		enableCompilerButton.setText("Enable Compiler Integration");
		enableCompilerButton.setSelection(BuilderHelper.isBuilderEnabled(project, IncrementalCompilerBuilder.BUILDER_ID));
		
		return composite;
	}

	protected void performDefaults() {
		enableMetricsCalculationButton.setSelection(true);
		setRuleBuilderState(true, project);
		setCompilerBuilderState(true, project);
	}
	
	private void setRuleBuilderState(boolean enabled, IProject project) {
		if (enabled) {
			BuilderHelper.addBuilderToProject(project, IncrementalRefactoringlRuleBuilder.BUILDER_ID);
		} else {
			BuilderHelper.removeBuilderFromProject(project, IncrementalRefactoringlRuleBuilder.BUILDER_ID);
			try {
				project.accept(new IncrementalRefactoringlRuleBuilder.MarkerRemovalVisitor());
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void setCompilerBuilderState(boolean enabled, IProject project) {
		if (enabled) {
			BuilderHelper.addBuilderToProject(project, IncrementalCompilerBuilder.BUILDER_ID);
		} else {
			BuilderHelper.removeBuilderFromProject(project, IncrementalCompilerBuilder.BUILDER_ID);
			try {
				project.accept(new IncrementalRefactoringlRuleBuilder.MarkerRemovalVisitor());
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean performOk() {
		setRuleBuilderState(enableMetricsCalculationButton.getSelection(), project);
		setCompilerBuilderState(enableCompilerButton.getSelection(), project);
		return true;
	}

}