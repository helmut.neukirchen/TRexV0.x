package de.ugoe.cs.swe.trex.ui.editors;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.formatter.ContentFormatter;
import org.eclipse.jface.text.formatter.IContentFormatter;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.reconciler.MonoReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.source.IAnnotationHover;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.ui.editors.text.TextFileDocumentProvider;
import org.eclipse.ui.editors.text.TextSourceViewerConfiguration;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditor;

import de.ugoe.cs.swe.trex.ui.TTCN3ColorProvider;
import de.ugoe.cs.swe.trex.ui.editors.text.NonRuleBasedDamagerRepairer;
import de.ugoe.cs.swe.trex.ui.editors.text.PartitionDoubleClickSelector;
import de.ugoe.cs.swe.trex.ui.editors.text.TTCN3CodeScanner;
import de.ugoe.cs.swe.trex.ui.editors.text.TTCN3DoubleClickSelector;
import de.ugoe.cs.swe.trex.ui.editors.text.TTCN3PartitionScanner;

/**
 * Configure an editor for TTCN3 files.
 * 
 * Note: Call dispose() when this TTCN3Configuration and its TTCN3ColorProvider
 * is no longer needed.
 */
public class TTCN3Configuration extends TextSourceViewerConfiguration {

	private AbstractDecoratedTextEditor fEditor;

	private TTCN3ColorProvider colorProvider = new TTCN3ColorProvider();

	public TTCN3Configuration() {
	}

	/**
	 * Create the instance
	 * 
	 * @param anEditor
	 *            the target editor
	 */
	public TTCN3Configuration(AbstractDecoratedTextEditor anEditor) {
		fEditor = anEditor;
	}

	/** {@inheritDoc} */
	@Override
	public ITextDoubleClickStrategy getDoubleClickStrategy(
			ISourceViewer sourceViewer, String contentType) {
		//START modified by sukiyuki 2011-5-20
		//return new DoubleClickStrategy();
		if (TTCN3PartitionScanner.SINGLE_LINE_COMMENT.equals(contentType)) {
			return new PartitionDoubleClickSelector(getConfiguredDocumentPartitioning(sourceViewer), 0, 0);
		} else if (TTCN3PartitionScanner.MULTI_LINE_COMMENT.equals(contentType)) {
			return new PartitionDoubleClickSelector(getConfiguredDocumentPartitioning(sourceViewer), 0, 0);
		} else if (TTCN3PartitionScanner.STRING.equals(contentType)) {
			return new PartitionDoubleClickSelector(getConfiguredDocumentPartitioning(sourceViewer), 1, 1);
		}
		return new TTCN3DoubleClickSelector();
		//END   modified by sukiyuki 2011-5-20
	}

	/** {@inheritDoc} */
	@Override
	public String[] getConfiguredContentTypes(ISourceViewer aSourceViewer) {
		return TTCN3PartitionScanner.PARTITION_TYPES;
	}

	/** {@inheritDoc} */
	@Override
	public IPresentationReconciler getPresentationReconciler(
			ISourceViewer aViewer) {
		PresentationReconciler reconciler = new PresentationReconciler();

		DefaultDamagerRepairer dr = new DefaultDamagerRepairer(
				new TTCN3CodeScanner(colorProvider));

		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);

		NonRuleBasedDamagerRepairer ndr = new NonRuleBasedDamagerRepairer(
				new TextAttribute(colorProvider.getColor("string")));
		reconciler.setDamager(ndr, TTCN3PartitionScanner.STRING);
		reconciler.setRepairer(ndr, TTCN3PartitionScanner.STRING);

		ndr = new NonRuleBasedDamagerRepairer(new TextAttribute(colorProvider
				.getColor("comment")));
		reconciler.setDamager(ndr, TTCN3PartitionScanner.SINGLE_LINE_COMMENT);
		reconciler.setRepairer(ndr, TTCN3PartitionScanner.SINGLE_LINE_COMMENT);

		ndr = new NonRuleBasedDamagerRepairer(new TextAttribute(colorProvider
				.getColor("comment")));
		reconciler.setDamager(ndr, TTCN3PartitionScanner.MULTI_LINE_COMMENT);
		reconciler.setRepairer(ndr, TTCN3PartitionScanner.MULTI_LINE_COMMENT);

		return reconciler;
	}

	/** {@inheritDoc} */
	@Override
	public IReconciler getReconciler(ISourceViewer aSourceViewer) {
		MonoReconciler reconciler;
		if (fEditor != null && fEditor instanceof TTCN3Editor) {
			reconciler = new MonoReconciler(((TTCN3Editor) fEditor)
					.getReconcilingStrategy(), false);
			reconciler.setProgressMonitor(new NullProgressMonitor());
			if (fEditor.getDocumentProvider() instanceof TextFileDocumentProvider) {
				TextFileDocumentProvider docProvider = (TextFileDocumentProvider) fEditor
						.getDocumentProvider();
				if (docProvider.getProgressMonitor() != null)
					reconciler.setProgressMonitor(docProvider.getProgressMonitor());
			}
		} else {
			reconciler = null;
		}
		return reconciler;
	}

	/** {@inheritDoc} */
	@Override
	public IContentAssistant getContentAssistant(ISourceViewer aSourceViewer) {
		ContentAssistant assistant;
		if (fEditor != null && fEditor instanceof TTCN3Editor) {
			assistant = new ContentAssistant();
			IContentAssistProcessor pr = new TTCN3ContentAssistProcessor(
					(TTCN3Editor) fEditor);
			assistant.setContentAssistProcessor(pr,
					IDocument.DEFAULT_CONTENT_TYPE);
			assistant
					.setInformationControlCreator(getInformationControlCreator(aSourceViewer));
			assistant.enableAutoInsert(true);
		} else {
			assistant = null;
		}
		return assistant;
	}

	/** {@inheritDoc} */
	@Override
	public IAnnotationHover getAnnotationHover(ISourceViewer aSourceViewer) {
		return new TTCN3AnnotationHover();
	}

	/** {@inheritDoc} */
	@Override
	public String[] getDefaultPrefixes(ISourceViewer aSourceViewer,
			String aContentType) {
		return new String[] { "//", "" };
	}

	@Override
	public IContentFormatter getContentFormatter(ISourceViewer sourceViewer) {
		ContentFormatter formatter = new ContentFormatter();
		formatter.setFormattingStrategy(new TTCN3FormattingStrategy(
				(TTCN3Editor) fEditor), IDocument.DEFAULT_CONTENT_TYPE);
		formatter.enablePartitionAwareFormatting(false);

		return formatter;
	}

	@Override
	public ITextHover getTextHover(ISourceViewer sv, String contentType) {
		return new TTCN3TextHover((TTCN3Editor) fEditor);
	}

	public void dispose() {
		if (colorProvider != null) {
			colorProvider.dispose();
		}
	}
}