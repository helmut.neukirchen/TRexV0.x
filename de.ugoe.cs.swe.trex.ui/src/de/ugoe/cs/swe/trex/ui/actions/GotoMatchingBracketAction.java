package de.ugoe.cs.swe.trex.ui.actions;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.action.Action;

import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;

public class GotoMatchingBracketAction extends Action {

	public final static String GOTO_MATCHING_BRACKET= "GotoMatchingBracket";
	
	private final TTCN3Editor fEditor;
	
	public GotoMatchingBracketAction(TTCN3Editor editor) {
		super("Go to Matching Bracket"); 
		Assert.isNotNull(editor);
		fEditor= editor;
		setEnabled(true);
	}
	
	@Override
	public void run() {
		fEditor.gotoMatchingBracket();
	}
}
