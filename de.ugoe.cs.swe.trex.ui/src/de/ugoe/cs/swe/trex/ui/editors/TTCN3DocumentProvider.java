package de.ugoe.cs.swe.trex.ui.editors;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.rules.FastPartitioner;
import org.eclipse.ui.editors.text.TextFileDocumentProvider;

import de.ugoe.cs.swe.trex.ui.editors.text.TTCN3PartitionScanner;

/**
 * This class provides the IDocuments used by TTCN3 editors. These IDocuments
 * have an TTCN3-aware partition scanner (multi-line comments) attached.
 */
public class TTCN3DocumentProvider extends TextFileDocumentProvider {

	@Override
	public IDocument getDocument(Object anElement) {
		IDocument document = super.getDocument(anElement);
		if (document != null) {
			IDocumentPartitioner partitioner = new FastPartitioner(
					new TTCN3PartitionScanner(),
					TTCN3PartitionScanner.PARTITION_TYPES);
			partitioner.connect(document);
			document.setDocumentPartitioner(partitioner);
		}

		return document;
	}

}
