package de.ugoe.cs.swe.trex.ui.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;

import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;

public class TRexPreferenceInitializer extends AbstractPreferenceInitializer {

	public TRexPreferenceInitializer() {
	}

	@Override
	public void initializeDefaultPreferences() {
		IEclipsePreferences node = DefaultScope.INSTANCE
				.getNode("org.eclipse.update.scheduler"); //$NON-NLS-1$

		node.putBoolean("enabled", true);
		node.put("schedule", "on-startup");
		node.putBoolean("download", false);
		TTCN3UIPlugin.getDefault().getPreferenceStore().setDefault("de.ugoe.cs.swe.trex.ui.compiler.compileAllProjects", true);
	}

}
