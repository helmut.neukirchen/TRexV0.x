package de.ugoe.cs.swe.trex.ui.editors.text;

import org.eclipse.jface.text.rules.IWordDetector;

/**
 * Detector for empty TTCN3 multi-line comments.
 */
public class TTCN3EmptyCommentDetector implements IWordDetector {

	/**
	 * @see IWordDetector#isWordStart(char)
	 */
	public boolean isWordStart(char c) {
		return (c == '/');
	}

	/**
	 * @see IWordDetector#isWordPart(char)
	 */
	public boolean isWordPart(char c) {
		return (c == '*' || c == '/');
	}
}
