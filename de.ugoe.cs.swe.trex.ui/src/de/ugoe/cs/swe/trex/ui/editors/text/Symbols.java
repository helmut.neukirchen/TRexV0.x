package de.ugoe.cs.swe.trex.ui.editors.text;

/**
 * Symbols for the heuristic TTCN3 scanner.
 * @author sukiyuki
 */
public interface Symbols {
	int TokenEOF = -1;
	int TokenLBRACE = 1;
	int TokenRBRACE = 2;
	int TokenLBRACKET = 3;
	int TokenRBRACKET = 4;
	int TokenLPAREN = 5;
	int TokenRPAREN = 6;
	int TokenSEMICOLON = 7;
	int TokenOTHER = 8;
	int TokenCOLON = 9;
	int TokenQUESTIONMARK = 10;
	int TokenCOMMA = 11;
	int TokenEQUAL = 12;
	int TokenLESSTHAN = 13;
	int TokenGREATERTHAN = 14;
	int TokenDOT = 15;
	int TokenMINUS = 16;
	int TokenTILDE = 17;
	int TokenSHIFTRIGHT = 18;
	int TokenARROW = 19;
	int TokenDOUBLECOLON = 20;
	int TokenSHIFTLEFT = 21;
	int TokenIF = 109;
	int TokenDO = 1010;
	int TokenFOR = 1011;
	int TokenTRY = 1012;
	int TokenCASE = 1013;
	int TokenELSE = 1014;
	int TokenBREAK = 1015;
	int TokenCATCH = 1016;
	int TokenWHILE = 1017;
	int TokenRETURN = 1018;
	int TokenSTATIC = 1019;
	int TokenSWITCH = 1020;
	int TokenGOTO = 1021;
	int TokenDEFAULT = 1022;
	int TokenPUBLIC = 1025;
	int TokenCLASS = 1028;
	int TokenUNION = 1030;
	int TokenOPERATOR = 1034;
	int TokenTHROW = 1035;
	int TokenCONST = 1036;
	int TokenIDENT = 2000;
}
