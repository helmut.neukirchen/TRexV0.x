package de.ugoe.cs.swe.trex.ui.preferences.adapters;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;

import de.ugoe.cs.swe.trex.core.formatter.TTCN3FormatterParameters;
import de.ugoe.cs.swe.trex.ui.preferences.PreferenceConstants;


public class TTCN3FormatterParametersAdapter extends TTCN3FormatterParameters {

	private IPreferencesService prefs;

	public TTCN3FormatterParametersAdapter() {
		prefs = Platform.getPreferencesService();

		//TODO default values of preferences?
	}

	@Override
	public boolean isTabs() {
		return prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_TABS,false,null);
	}

	@Override
	public boolean isUnixNewline() {
		return prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_UNIX_NEWLINE,false,null);
	}

	@Override
	public boolean isSpacesBetweenAssignment() {
		return prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_SPACES_BETWEEN_ASSIGNMENT,false,null);
	}

	@Override
	public boolean isSpaceAfterComma() {
		return prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_SPACE_AFTER_COMMA,false,null);
	}

	@Override
	public boolean isKRstyle() {
		return prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_KR_STYLE,false,null);
	}
	
	@Override
	public boolean isNewlineBeforeRunsOn() {
		return prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_NEWLINE_BEFORE_RUNS_ON,false,null);
	}
	
	@Override
	public boolean isNewlineBeforeSystem() {
		return prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_NEWLINE_BEFORE_SYSTEM,false,null);
	}
	
	public boolean isNewlineBeforeReturn() {
		return prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_NEWLINE_BEFORE_RETURN,false,null);
	}

	@Override
	public boolean isNewlineBeforeExceptionSpec() {
		return prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_NEWLINE_BEFORE_EXCEPTION_SPEC,false,null);
	}
	
	@Override
	public boolean isNewlineBeforeFormalPar() {
		return prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_NEWLINE_BEFORE_FORMAL_PAR,false,null);
	}
	
	@Override
	public boolean isNewlineAfterFormalParList() {
		return prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_NEWLINE_AFTER_FORMAL_PAR_LIST,false,null);
	}
	
	@Override
	public boolean isSpacesAroundParentheses() {
		return prefs.getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_SPACES_AROUND_PARENTHESES,false,null);
	}
	
	@Override
	public int getSpacesCount() {
		return prefs.getInt("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_SPACES_COUNT,0,null);
	}

	@Override
	public int getLinesBetweenModules() {
		return prefs.getInt("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_LINES_BETWEEN_MODULES,0,null);
	}

	@Override
	public int getLinesAfterControlPart() {
		return prefs.getInt("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_LINES_AFTER_CONTROL_PART,0,null);
	}

	@Override
	public int getLinesAfterModuleDefinition() {
		return prefs.getInt("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_FORMATTER_LINES_AFTER_MODULE_DEFINITION,0,null);
	}

}
