package de.ugoe.cs.swe.trex.ui.views.metrics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ContentViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.ViewPart;

import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.ui.TTCN3ResourceHelper;

/**
 * <code>ViewPart</code> for displaying TTCN-3 Metrics.
 */
public class TTCN3MetricsView extends ViewPart implements ISelectionListener,
		IPartListener2 {
	/**
	 * Simple <code>ViewerSorter</code> to allow the Metrics to be sorted by
	 * column.
	 * 
	 */
	static class MetricSorter extends ViewerSorter {
		private int column;

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ViewerSorter#compare(org.eclipse.jface.viewers.Viewer,
		 *      java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(Viewer view, Object e1, Object e2) {
			switch (column) {
			case 0:
				break;
			case 1:
				if (e1 instanceof String && e2 instanceof String) {
					break;
				}

				if (view != null && view instanceof ContentViewer) {
					if (e1 instanceof IFile && e2 instanceof IFile
							|| e1 instanceof AST && e2 instanceof AST) {
						IBaseLabelProvider prov = ((ContentViewer) view)
								.getLabelProvider();
						if (prov instanceof TTCN3MetricsLabelProvider) {
							TTCN3MetricsLabelProvider lprov = (TTCN3MetricsLabelProvider) prov;
							return (int) Math.round(lprov.getMetricValue(e2)
									- lprov.getMetricValue(e1));
						}
					}
				}

				break;
			case 2:
				if (e1 instanceof LocationAST && e2 instanceof LocationAST) {
					return TTCN3MetricsLabelProvider.getReferences(e2)
							- TTCN3MetricsLabelProvider.getReferences(e1);
				}
				break;
			default:
				break;
			}

			return super.compare(view, e1, e2);
		}

		/**
		 * Set the column that the <code>Tree</code> should be sorted upon.
		 * 
		 * @param col
		 *            the index of the column to sort upon.
		 */
		public void setColumn(int col) {
			this.column = col;
		}
	}

	// ---------------------------------------------------------------------------

	/**
	 * <code>Map</code> from <code>AST</code> nodes to the
	 * <code>IFile</code> object they are associated with.
	 */
	static Map<AST, IFile> astFileMap = new HashMap<AST, IFile>();

	/**
	 * <code>Map</code> from a particular <code>IFile</code> reference to
	 * the parent Metric name (<code>String</code>) in this context.
	 */
	static Map<IFile, String> fileToMetricName = new HashMap<IFile, String>();

	/**
	 * A <code>List</code> of the <code>IFile</code> objects whose Metrics
	 * should be displayed.
	 */
	static List<IFile> metricsFiles = new ArrayList<IFile>();

	TreeColumn column0;

	TreeColumn column1;

	TreeColumn column2;

	Action doubleClickAction;

	private boolean hidden = false;

	static TreeViewer viewer;

	Composite parent;

	// ---------------------------------------------------------------------------

	/**
	 * Creates a new view.
	 */
	public TTCN3MetricsView() {
		super();
	}

	// ---------------------------------------------------------------------------

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(final Composite parent) {
		this.parent = parent;

		setupViewer();

		IWorkbenchPage page = getViewSite().getPage();
		page.addSelectionListener(this);
		page.addPartListener(this);

		final Tree tree = viewer.getTree();
		tree.setHeaderVisible(true);
		tree.setLinesVisible(true);

		setupColumns(tree);

		setupTreeListener(tree);

		makeActions();
		hookDoubleClickAction();

		refreshViewer();
	}

	// ---------------------------------------------------------------------------

	/**
	 * @return the metricsFiles
	 */
	public static List<IFile> getMetricsFiles() {
		return metricsFiles;
	}

	/**
	 * Add a <code>DoubleClickEvent</code> to allow navigation in the Metrics
	 * view.
	 */
	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	// ---------------------------------------------------------------------------

	/**
	 * Add the click <code>Action</code> to allow navigation when
	 * <code>IFile</code> or <code>LocationAST</code> objects are clicked in
	 * the Metrics view.
	 */
	private void makeActions() {
		doubleClickAction = new Action() {
			@Override
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection) selection)
						.getFirstElement();
				IWorkbenchPage p = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage();
				if (p == null)
					return;

				// for working with clicks on IFile names
				if (obj instanceof IFile) {
					IFile file = (IFile) obj;
					try {
						IDE.openEditor(p, file, true);
					} catch (PartInitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (obj instanceof LocationAST) {
					LocationAST ast = (LocationAST) obj;
					IFile file = TTCN3MetricsView.astFileMap.get(ast);
					if (file != null) {
						IMarker marker;
						try {
							// jump to selected AST In editor window
							marker = file.createMarker(IMarker.TEXT);
							marker.setAttribute(IMarker.CHAR_START, ast
									.getOffset());
							marker.setAttribute(IMarker.CHAR_END, ast
									.getOffset());
							IDE.openEditor(p, marker);

						} catch (CoreException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		};
	}

	// ---------------------------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener2#partActivated(org.eclipse.ui.IWorkbenchPartReference)
	 */
	public void partActivated(IWorkbenchPartReference partRef) {
		// do nothing
	}

	// ---------------------------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener2#partBroughtToTop(org.eclipse.ui.IWorkbenchPartReference)
	 */
	public void partBroughtToTop(IWorkbenchPartReference partRef) {
		// do nothing
	}

	// ---------------------------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener2#partClosed(org.eclipse.ui.IWorkbenchPartReference)
	 */
	public void partClosed(IWorkbenchPartReference partRef) {
		// do nothing
	}

	// ---------------------------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener2#partDeactivated(org.eclipse.ui.IWorkbenchPartReference)
	 */
	public void partDeactivated(IWorkbenchPartReference partRef) {
		// do nothing
	}

	// ---------------------------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener2#partHidden(org.eclipse.ui.IWorkbenchPartReference)
	 */
	public void partHidden(IWorkbenchPartReference partRef) {
		if (this.getPartName().equals(partRef.getPartName())) {
			this.hidden = true;
		}
	}

	// ---------------------------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener2#partInputChanged(org.eclipse.ui.IWorkbenchPartReference)
	 */
	public void partInputChanged(IWorkbenchPartReference partRef) {
		// do nothing
	}

	// ---------------------------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener2#partOpened(org.eclipse.ui.IWorkbenchPartReference)
	 */
	public void partOpened(IWorkbenchPartReference partRef) {
		// do nothing
	}

	// ---------------------------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPartListener2#partVisible(org.eclipse.ui.IWorkbenchPartReference)
	 */
	public void partVisible(IWorkbenchPartReference partRef) {
		if (this.getPartName().equals(partRef.getPartName())) {
			this.hidden = false;
		}
	}

	// ---------------------------------------------------------------------------

	/**
	 * If the given <code>IFile</code> has the extension .ttcn or .ttcn3 then
	 * add it to the list of files to display Metrics for.
	 * 
	 * @param file
	 *            the <code>IFile</code> to display Metrics for.
	 */
	public static void processFile(IFile file) {
		if (TTCN3ResourceHelper.isTTCN3File(file)) {
			TTCN3MetricsView.metricsFiles.add(file);
		}
	}

	// ---------------------------------------------------------------------------

	/**
	 * Recursively process the given <code>IFolder</code> and its subfolders,
	 * adding any TTCN-3 <code>IFile</code> objects to the list of files to
	 * display Metrics for.
	 * 
	 * @param folder
	 *            the <code>IFolder</code> to begin from.
	 */
	private void processFolder(IFolder folder) {
		try {
			IResource[] members = folder.members();
			for (IResource element : members) {
				if (element instanceof IFolder) {
					processFolder((IFolder) element);
				} else if (element instanceof IFile) {
					processFile((IFile) element);
				}
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// ---------------------------------------------------------------------------

	/**
	 * Called by selectionListener thread to tell the Metrics view to update.
	 */
	public static void refreshViewer() {
		TTCN3MetricsView.fileToMetricName.clear();
		TTCN3MetricsView.astFileMap.clear();

		if (viewer == null)
			return;

		final Control control = viewer.getControl();
		if (control != null && !control.isDisposed()) {
			final Display display = control.getDisplay();

			if (display != null && !display.isDisposed()) {
				display.asyncExec(new Runnable() {
					public void run() {
						if (viewer != null && control.isDisposed())
							return;

						viewer.collapseAll();
						viewer.refresh();
					}
				});
			}
		}
	}

	// ---------------------------------------------------------------------------

	/**
	 * Update the files displayed in the Metrics <code>ViewPart</code>
	 * whenever the Navigator selection has changed.
	 * 
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection.isEmpty() || hidden)
			return;

		// if new selection and metrics view isn't hidden
		final String partTitle = part.getTitle();
		if (("Package Explorer".equals(partTitle) || "Navigator"
				.equals(partTitle))) {
			if (selection instanceof IStructuredSelection) {
				IStructuredSelection structSelection = (IStructuredSelection) selection;

				// single selection
				if (structSelection.size() == 1) {
					Object first = structSelection.getFirstElement();

					// cope with IFolder and get all files from within it
					if (first instanceof IFolder) {
						TTCN3MetricsView.metricsFiles.clear();
						processFolder((IFolder) first);
						TTCN3MetricsView.refreshViewer();
					} else if (first instanceof IFile) {
						TTCN3MetricsView.metricsFiles.clear();
						processFile((IFile) first);
						TTCN3MetricsView.refreshViewer();
					}
				}
			}
		}
	}

	// ---------------------------------------------------------------------------

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	// ---------------------------------------------------------------------------

	/**
	 * Assign the given parameters as properties for the <code>TreeColumn</code>
	 * object and setup the <code>SelectionListener</code> to sort on this
	 * column when clicked.
	 * 
	 * @param col
	 *            the <code>TreeColumn</code> object to operate on.
	 * @param colText
	 *            the <code>String</code> to assign as the column title.
	 * @param colNum
	 *            the index of the <code>TreeColumn</code> in its parent.
	 */
	private void setupColumn(TreeColumn col, String colText, final int colNum) {
		col.setText(colText);
		col.setMoveable(false);
		col.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				((MetricSorter) viewer.getSorter()).setColumn(colNum);
				viewer.refresh();
			}
		});
	}

	// ---------------------------------------------------------------------------

	/**
	 * Initialise suitable columns for the <code>Tree</code>.
	 * 
	 * @param tree
	 *            the <code>Tree</code> to add <code>TreeColumn</code>
	 *            objects to.
	 */
	private void setupColumns(final Tree tree) {
		column0 = new TreeColumn(tree, SWT.LEFT);
		setupColumn(column0, "Metric", 0);

		column1 = new TreeColumn(tree, SWT.RIGHT);
		setupColumn(column1, "Total", 1);

		column2 = new TreeColumn(tree, SWT.RIGHT);
		setupColumn(column2, "References", 2);
	}

	// ---------------------------------------------------------------------------

	/**
	 * Add the <code>ControlListener</code> to oversee the widths of each
	 * <code>TreeColumn</code> in the <code>View</code>.
	 * 
	 * @param tree
	 *            the <code>Tree</code> to add the
	 *            <code>ControlListener</code> to.
	 */
	private void setupTreeListener(final Tree tree) {
		parent.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				final Rectangle area = parent.getClientArea();
				int width = area.width - 2 * tree.getBorderWidth();
				ScrollBar verticalBar = tree.getVerticalBar();
				if (verticalBar != null) {
					// Subtract the scrollbar width from the total column width
					// if a vertical scrollbar will be required
					Point vBarSize = verticalBar.getSize();
					width -= vBarSize.x;
				}
				column2.pack();
				final int colwidth = column2.getWidth();
				column1.setWidth(colwidth);
				column0.setWidth(width - 2 * colwidth - 4);
				tree.setSize(area.width, area.height);
			}
		});
	}

	// ---------------------------------------------------------------------------

	/**
	 * Initialise the <code>TreeViewer</code> and add it to the
	 * <code>Composite</code>.
	 */
	private void setupViewer() {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.setContentProvider(new TTCN3MetricsContentProvider());
		viewer.setLabelProvider(new TTCN3MetricsLabelProvider());
		viewer.setSorter(new MetricSorter());
		viewer.setInput(getViewSite());
	}
}
