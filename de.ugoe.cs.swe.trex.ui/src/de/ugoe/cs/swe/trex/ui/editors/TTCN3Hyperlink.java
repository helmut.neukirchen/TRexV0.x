package de.ugoe.cs.swe.trex.ui.editors;

import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.hyperlink.IHyperlink;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;

public class TTCN3Hyperlink implements IHyperlink {
	private String location;
    private IRegion region;
    
    TTCN3Editor activeEditor;
	TTCN3MultiPageEditor multipageEditor;

    public TTCN3Hyperlink(IRegion region, String text) {
        this.region= region;
        this.location = text;
    }

    public IRegion getHyperlinkRegion() {
        return region;
    }

    public void open() {
    	int offset=0;
        if(location!=null)
        {
        	TTCN3Editor editor= getActiveEditor();
        	final IdentifierCache identifierCache = editor.getReconcilingStrategy().getIdentifierCache();
        	LocationAST ast = (identifierCache == null) ? null : identifierCache.get(offset);

            offset=ast.getOffset();
           
            editor.selectAndReveal(offset,0);
            editor.setFocus();
        }
    }    
	
	public String getTypeLabel() {
        return null;
    }

    public String getHyperlinkText() {
        return null;
    }
    
    private TTCN3Editor getActiveEditor() {
		IWorkbenchWindow window= PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window != null) {
			IWorkbenchPage page= window.getActivePage();
			if (page != null) {
				IEditorPart editor= page.getActiveEditor();
				if (editor instanceof TTCN3MultiPageEditor) {
					multipageEditor = (TTCN3MultiPageEditor)editor;
					return multipageEditor.getFEditor();
				}
			}
		}
		return null;
	}
}
