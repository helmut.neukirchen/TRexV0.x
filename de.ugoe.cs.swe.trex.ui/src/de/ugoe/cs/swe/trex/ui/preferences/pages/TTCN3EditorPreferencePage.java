package de.ugoe.cs.swe.trex.ui.preferences.pages;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.osgi.service.prefs.BackingStoreException;

import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;
import de.ugoe.cs.swe.trex.ui.preferences.PreferenceConstants;

/**
 * General settings for TTCN-3 editor.
 */
public class TTCN3EditorPreferencePage extends FieldEditorPreferencePage
        implements IWorkbenchPreferencePage {

    /**
     * Create the pref page
     */
    public TTCN3EditorPreferencePage() {
        super(FieldEditorPreferencePage.GRID);
        setPreferenceStore(TTCN3UIPlugin.getDefault().getPreferenceStore());
        setDescription("TTCN-3 editor preferences page\n\n" + "Enabling any of the preferences below will decrease speed:");
//      TODO: A long term solution is to register at PreferenceStore as IPropertyChangeListener using addPropertyChangeListener()
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
     */
    @Override
    protected void createFieldEditors() {
    	 
		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_SEMANTIC_ANALYSIS_ENABLEMENT,
				"Enable &Semantic Analysis", getFieldEditorParent()));
		
		// Added by Ingvi and Omar 23.11.2010
		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_MARK_OCCURRENCES_ENABLEMENT,
				"Enable &Mark Occurrences", getFieldEditorParent()));
		
		// Added by Einar and Niels (TTC-312)
		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_OUTLINE_TREE_VIEW_EXPANDED_BY_DEFAULT,
				"Expand Tree In &Outline View by Default", getFieldEditorParent()));			
    }

    /** {@inheritDoc} */
    public void init(IWorkbench aWorkbench) {
        // nothing to do here
    }

    /** {@inheritDoc} */
	@Override
    public boolean performOk() {
        boolean value = super.performOk();
		try {
			InstanceScope.INSTANCE.getNode(TTCN3UIPlugin.getUniqueIdentifier()).flush();
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block. What would be a good catch ?
			e.printStackTrace();
		}
        return value;
    }
}
