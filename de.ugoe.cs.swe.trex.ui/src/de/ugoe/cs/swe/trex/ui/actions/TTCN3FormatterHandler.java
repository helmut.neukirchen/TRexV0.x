package de.ugoe.cs.swe.trex.ui.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3MultiPageEditor;


public class TTCN3FormatterHandler extends AbstractHandler implements IEditorActionDelegate {

	TTCN3Editor activeEditor;

	// ---------------------------------------------------------------------------
	
	public Object execute(ExecutionEvent event) {
		final TTCN3Editor editor = ActionHelper.getActiveEditor();
		
		if (editor == null)
			return null;

		if (editor.getReconcilingStrategy().getTTCN3Analyzer().isAnalyzed()) {
			editor.getTextViewer().doOperation(ISourceViewer.FORMAT);
		} else {
			displayCannotFormatErrorDialog(editor.getSite().getShell());
		}
		// note: does this really need to analyze all files again? surely only
		// current file needs re-analyzing...
		// zeiss: commented out for that reason

		//final IFile ff = (IFile) editor.getEditorInput().getAdapter(IFile.class);		
		//editor.getReconcilingStrategy().analyzeAll(ff, null, true);
		
		return null;
	}

	// ---------------------------------------------------------------------------

	public void run(IAction action) {
		if (activeEditor == null) {
			MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Formatter Error",
					"No Editor initialized. Something is wrong...");
			return;
		}
		if (activeEditor.getReconcilingStrategy().getTTCN3Analyzer().isAnalyzed()) {
			activeEditor.getTextViewer().doOperation(ISourceViewer.FORMAT);
		} else {
			displayCannotFormatErrorDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		}
	}

	private void displayCannotFormatErrorDialog(Shell shell) {
		MessageDialog.openInformation(shell, "Formatter Error",
		"Cannot format file, because it contains syntax errors or is not yet analyzed!");
	}

	// ---------------------------------------------------------------------------

	public void selectionChanged(IAction action, ISelection selection) {
		// do nothing
	}

	// ---------------------------------------------------------------------------

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		if (targetEditor instanceof TTCN3Editor) {
			activeEditor = (TTCN3Editor) targetEditor;
		} else if (targetEditor instanceof TTCN3MultiPageEditor) {
			TTCN3MultiPageEditor multiPageEditor = (TTCN3MultiPageEditor) targetEditor;
			activeEditor = multiPageEditor.getFEditor();
		}
	}
	
}
