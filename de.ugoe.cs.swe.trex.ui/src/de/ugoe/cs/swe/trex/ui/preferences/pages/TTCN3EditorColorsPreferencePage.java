package de.ugoe.cs.swe.trex.ui.preferences.pages;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.osgi.service.prefs.BackingStoreException;

import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;

/**
 * Color settings for editor syntax highliting.
 */
public class TTCN3EditorColorsPreferencePage extends FieldEditorPreferencePage
        implements IWorkbenchPreferencePage {

    /**
     * Create the pref page
     */
    public TTCN3EditorColorsPreferencePage() {
        super(FieldEditorPreferencePage.GRID);
        setPreferenceStore(TTCN3UIPlugin.getDefault().getPreferenceStore());
        setDescription("Syntax Highlighting Colors\n\n"
                + "(Note: These preferences will have no effect on already opened TTCN-3 editors.)");
//      TODO: A long term solution is to register at PreferenceStore as IPropertyChangeListener using addPropertyChangeListener()
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
     */
    @Override
    protected void createFieldEditors() {
        addField(new ColorFieldEditor("color.default", "&Default Text",
                getFieldEditorParent()));
        addField(new ColorFieldEditor("color.keyword", "&Keywords",
                getFieldEditorParent()));
        addField(new ColorFieldEditor("color.string", "&Strings",
                getFieldEditorParent()));
        addField(new ColorFieldEditor("color.comment", "&Comments",
                getFieldEditorParent()));
    }

    /** {@inheritDoc} */
    public void init(IWorkbench aWorkbench) {
        // nothing to do here
    }

    /** {@inheritDoc} */
	@Override
    public boolean performOk() {
        boolean value = super.performOk();
		try {
			InstanceScope.INSTANCE.getNode(TTCN3UIPlugin.getUniqueIdentifier()).flush();
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block. What would be a good catch ?
			e.printStackTrace();
		}
        return value;
    }
}
