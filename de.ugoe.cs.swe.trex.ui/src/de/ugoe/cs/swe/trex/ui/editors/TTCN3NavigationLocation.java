package de.ugoe.cs.swe.trex.ui.editors;

import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.TextSelectionNavigationLocation;

/**
 * @author Jochen Kemnade
 * 
 */
public class TTCN3NavigationLocation extends TextSelectionNavigationLocation {

	/**
	 * @param anEditor
	 * @param anInitialize
	 */
	public TTCN3NavigationLocation(ITextEditor anEditor, boolean anInitialize) {
		super(anEditor, anInitialize);
	}

	/**
	 * Redirect from MultiPageEditor to associated ITextEditor.
	 * 
	 * @see org.eclipse.ui.NavigationLocation#getEditorPart()
	 */
	@Override
	protected IEditorPart getEditorPart() {
		IEditorPart editor = super.getEditorPart();
		if (editor != null) {
			editor = (ITextEditor) editor.getAdapter(ITextEditor.class);
		}
		return editor;
	}
}
