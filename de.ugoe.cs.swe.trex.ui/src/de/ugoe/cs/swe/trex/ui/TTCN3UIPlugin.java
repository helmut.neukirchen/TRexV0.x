package de.ugoe.cs.swe.trex.ui;

import java.net.URL;
import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.ugoe.cs.swe.trex.core.TTCN3CorePlugin;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.misc.HTMLPrinter;
import de.ugoe.cs.swe.trex.ui.builders.IncrementalRefactoringlRuleBuilder;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3MultiPageEditor;

/**
 * Central access point for the TTCN3 UI plug-in (id "ttcn3").
 * 
 * @author Jochen Kemnade
 */
public class TTCN3UIPlugin extends AbstractUIPlugin {

	public static final String PLUGIN_ID = "de.ugoe.cs.swe.trex.ui";

	/** Singleton instance of this plugin */
	private static TTCN3UIPlugin plugin;

	private static final String RESOURCE_NAME = TTCN3UIPlugin.PLUGIN_ID
			+ ".messages";

	private ResourceBundle resourceBundle;

	/**
	 * Create the plugin instance
	 */
	public TTCN3UIPlugin() {
		TTCN3UIPlugin.plugin = this;

		try {
			resourceBundle = ResourceBundle
					.getBundle(TTCN3UIPlugin.RESOURCE_NAME);
		} catch (MissingResourceException e) {
			log(e);
			resourceBundle = null;
		}

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		workspace.addResourceChangeListener(new TTCN3ResourceChangeListener(),
				IResourceChangeEvent.POST_CHANGE);
	}

	public static synchronized TTCN3UIPlugin getInstance() {
		if (TTCN3UIPlugin.plugin == null) {
			TTCN3UIPlugin.plugin = new TTCN3UIPlugin();
		}

		return TTCN3UIPlugin.plugin;
	}

	public static void debugMessage(String message) {
		String traceValue = Platform.getDebugOption(TTCN3UIPlugin.PLUGIN_ID
				+ "/debug");
		if ((traceValue != null) && (traceValue.equalsIgnoreCase("true")))
			System.out.println(message);
	}

	public static void debugMessage2(String message) {
		String traceValue = Platform.getDebugOption(TTCN3UIPlugin.PLUGIN_ID
				+ "/debug");
		if ((traceValue != null) && (traceValue.equalsIgnoreCase("true")))
			System.out.print(message);
	}

	public static void debugMessageTimer(String message) {
		String traceValue = Platform.getDebugOption(TTCN3UIPlugin.PLUGIN_ID
				+ "/timer");
		if ((traceValue != null) && (traceValue.equalsIgnoreCase("true")))
		{
			System.out.println(message);
		}
	}

	public static void debugMessageSymbolResolver(String message) {
		String traceValue = Platform.getDebugOption(TTCN3UIPlugin.PLUGIN_ID
				+ "/symbolresolver");
		if ((traceValue != null) && (traceValue.equalsIgnoreCase("true")))
		{
			System.out.println(message);
		}
	}

	public static void debugMessageSymbolResolverTimer(String message) {
		String traceValue = Platform.getDebugOption(TTCN3UIPlugin.PLUGIN_ID
				+ "/timer");
		String traceValue2 = Platform.getDebugOption(TTCN3UIPlugin.PLUGIN_ID
				+ "/symbolresolver");
		if ((traceValue != null) && (traceValue.equalsIgnoreCase("true"))
				&& traceValue2 != null && traceValue2.equalsIgnoreCase("true"))
			System.out.println(message);
	}

	public static boolean isCPUProfiling() {
		String traceValue = Platform.getDebugOption(TTCN3UIPlugin.PLUGIN_ID
				+ "/cpuprofiling");
		if (TTCN3CorePlugin.isProfilingAgent() && (traceValue != null) && (traceValue.equalsIgnoreCase("true")))
		{
			return true;
		}
		
		return false;
	}

	public static boolean isMemProfiling() {
		String traceValue = Platform.getDebugOption(TTCN3UIPlugin.PLUGIN_ID
				+ "/memprofiling");
		if ( TTCN3CorePlugin.isProfilingAgent() && (traceValue != null) && (traceValue.equalsIgnoreCase("true")))
			return true;

		return false;
	}

	/**
	 * Returns the shared instance.
	 * 
	 * @return the shared instance
	 */
	public static TTCN3UIPlugin getDefault() {
		return TTCN3UIPlugin.plugin;
	}

	/**
	 * Get the resource bundle
	 * 
	 * @return the resource bundle
	 */
	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}

	/**
	 * Get the workspace
	 * 
	 * @return the workspace
	 */
	public static IWorkspace getWorkspace() {
		return ResourcesPlugin.getWorkspace();
	}

	/**
	 * Get the active workbench shell
	 * 
	 * @return the shell
	 */
	public static Shell getActiveWorkbenchShell() {
		IWorkbenchWindow window = getActiveWorkbenchWindow();
		return (window == null ? null : window.getShell());
	}

	/**
	 * Get the active window
	 * 
	 * @return the active window
	 */
	public static IWorkbenchWindow getActiveWorkbenchWindow() {
		return getDefault().getWorkbench().getActiveWorkbenchWindow();
	}

	/**
	 * Get the active page
	 * 
	 * @return the active page
	 */
	public static IWorkbenchPage getActiveWorkbenchPage() {
		return getDefault().getWorkbench().getActiveWorkbenchWindow()
				.getActivePage();
	}

	/**
	 * Get my installation URL
	 * 
	 * @return my installation URL
	 */
	public static URL getInstallURL() {
		return TTCN3UIPlugin.getDefault().getBundle().getEntry("/");
		// return Platform.getBundle((PLUGIN_ID)).getEntry("/");
	}

	/**
	 * Get my plugin id
	 * 
	 * @return my plugin id
	 */
	public static String getUniqueIdentifier() {
		return TTCN3UIPlugin.getDefault().getBundle().getSymbolicName();
	}

	/**
	 * Log a status
	 * 
	 * @param aStatus
	 *            the status to log
	 */
	public static void log(IStatus aStatus) {
		getDefault().getLog().log(aStatus);
	}

	/**
	 * log an exception
	 * 
	 * @param aThrowable
	 *            the exception to log
	 */
	public static void log(Throwable aThrowable) {
		log(new Status(IStatus.ERROR, TTCN3UIPlugin.PLUGIN_ID, IStatus.OK,
				getMessage("Plugin.internal_error"), aThrowable));
	}

	/**
	 * log an error message
	 * 
	 * @param aMessage
	 *            the message to log
	 */
	public static void logErrorMessage(String aMessage) {
		log(new Status(IStatus.ERROR, TTCN3UIPlugin.PLUGIN_ID, IStatus.OK,
				aMessage, null));
	}

	/**
	 * log an error status
	 * 
	 * @param aMessage
	 *            the message
	 * @param aStatus
	 *            the status
	 */
	public static void logErrorStatus(String aMessage, IStatus aStatus) {
		if (aStatus == null) {
			logErrorMessage(aMessage);
		} else {
			MultiStatus multi = new MultiStatus(TTCN3UIPlugin.PLUGIN_ID,
					IStatus.OK, aMessage, null);
			multi.add(aStatus);
			log(multi);
		}
	}

	/**
	 * Are we in debug mode?
	 * 
	 * @return true for debug mode; false otherwise
	 */
	public static boolean isDebug() {
		return getDefault().isDebugging();
	}

	/**
	 * Translate a key to a bundled message
	 * 
	 * @param aKey
	 *            the key
	 * @return the message
	 */
	public static String getMessage(String aKey) {
		String bundleString;
		ResourceBundle bundle = getDefault().getResourceBundle();
		if (bundle == null) {
			bundleString = "!" + aKey + "!";
		} else {
			try {
				bundleString = bundle.getString(aKey);
			} catch (MissingResourceException e) {
				log(e);
				bundleString = "!" + aKey + "!";
			}
		}
		return bundleString;
	}

	/**
	 * Translate a message with an argument
	 * 
	 * @param aKey
	 *            the key
	 * @param anArg
	 *            the argument
	 * @return the formatted messsage
	 */
	public static String getFormattedMessage(String aKey, String anArg) {
		return getFormattedMessage(aKey, new String[] { anArg });
	}

	/**
	 * Translate a message with arguments
	 * 
	 * @param aKey
	 *            the key
	 * @param anArgs
	 *            the arguments
	 * @return the formatted messsage
	 */
	public static String getFormattedMessage(String aKey, String[] anArgs) {
		return MessageFormat.format(getMessage(aKey), (Object[]) anArgs);
	}

	/**
	 * This method is called upon plug-in activation
	 * 
	 * @param context
	 *            the bundle description
	 * @throws Exception
	 *             something evil happened
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);

		HTMLPrinter.getInstance().setHTMLPrinterDelegate(
				new EclipseHTMLPrinterDelegate());
		Scope.debugVal = Platform.getDebugOption(TTCN3CorePlugin.PLUGIN_ID
				+ "/symbolresolver");
		Scope.debug = "true".equalsIgnoreCase(Scope.debugVal);

		// set type based sorting as the default sorting type
		@SuppressWarnings(value = { "all" })
		IDialogSettings settings = org.eclipse.ui.internal.ide.IDEWorkbenchPlugin
				.getDefault().getDialogSettings();
		IDialogSettings problemSection = settings
				.getSection("org.eclipse.ui.views.problem");
		if (problemSection == null)
			problemSection = settings
					.addNewSection("org.eclipse.ui.views.problem");

		IDialogSettings sorterSection = problemSection.getSection("sorter");
		if (sorterSection == null)
			sorterSection = problemSection.addNewSection("sorter");

		sorterSection.put("categoryField", "TRex Problem Type");

		// catch partactivated event
		if ((PlatformUI.getWorkbench() == null)
				|| (PlatformUI.getWorkbench().getActiveWorkbenchWindow() == null)
				|| (PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getPartService() == null))
		{
			return;
		}

		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService()
				.addPartListener(new IPartListener() {
					public void partActivated(IWorkbenchPart part) {
						TTCN3Editor activeEditor = null;
						if (part instanceof TTCN3Editor) {
							activeEditor = (TTCN3Editor) part;
						} else if (part instanceof TTCN3MultiPageEditor) {
							TTCN3MultiPageEditor multipageEditor = (TTCN3MultiPageEditor) part;
							activeEditor = multipageEditor.getFEditor();
						}
						if (activeEditor == null)
						{
							return;
						}
						
						TTCN3AnalyzerFlyweightFactory analyzerFactory = TTCN3AnalyzerFlyweightFactory
								.getInstance();

						IFile file = null;
						file = (IFile) activeEditor.getEditorInput()
								.getAdapter(IFile.class);
						if (file == null)
						{
							return;
						}
						if (!file.exists())
						{
							return;
						}
						if (!file.getProject().isOpen())
						{
							return;
						}
						if (!analyzerFactory.hasAnalyzer(file))
						{
							return;
						}

						if ((analyzerFactory.getLastProjectAnalyzed() != null)
								&& (!file.getProject().equals(
										analyzerFactory
												.getLastProjectAnalyzed()))) {
							try {
								file
										.getProject()
										.accept(
												new IncrementalRefactoringlRuleBuilder.MarkerRemovalVisitor());
								if (analyzerFactory.getLastProjectAnalyzed().exists())
								{
									analyzerFactory.getLastProjectAnalyzed().accept(
													new IncrementalRefactoringlRuleBuilder.MarkerRemovalVisitor());
								}
							} catch (CoreException e) {
								e.printStackTrace();
							}

							if (!analyzerFactory.hasAnalyzedProject(file.getProject()))
							{
								return;
							}
								
							analyzerFactory.setLastProjectAnalyzed(file.getProject());
							try {
								IncrementalRefactoringlRuleBuilder
										.setOverrideStatus(true);
								file
										.getProject()
										.build(
												IncrementalRefactoringlRuleBuilder.FULL_BUILD,
												new NullProgressMonitor());
								IncrementalRefactoringlRuleBuilder
										.setOverrideStatus(false);
							} catch (CoreException e) {
								e.printStackTrace();
							}
							// activeEditor.getDocumentProvider().changed(null);
						}
					}

					public void partBroughtToTop(IWorkbenchPart part) {
					}

					public void partClosed(IWorkbenchPart part) {
					}

					public void partDeactivated(IWorkbenchPart part) {
					}

					public void partOpened(IWorkbenchPart part) {
					}
				});
	}

	/**
	 * This method is called when the plug-in is stopped
	 * 
	 * @param context
	 *            the bundle description
	 * @throws Exception
	 *             something evil happened
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
		TTCN3UIPlugin.plugin = null;
		resourceBundle = null;
	}

}
