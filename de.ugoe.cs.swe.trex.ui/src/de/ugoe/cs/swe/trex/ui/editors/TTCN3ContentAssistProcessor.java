package de.ugoe.cs.swe.trex.ui.editors;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.eclipse.swt.graphics.Image;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.AltstepSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.ConstantSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.FunctionSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.ModuleSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.SubtypeSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.TestcaseSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.TypeSymbol;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.VariableInstanceSymbol;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPluginImages;
import de.ugoe.cs.swe.trex.ui.editors.text.TTCN3CodeScanner;


public class TTCN3ContentAssistProcessor implements IContentAssistProcessor {

	private TTCN3Editor editor = null;

	public TTCN3ContentAssistProcessor(TTCN3Editor editor) {
		this.editor = editor;
	}

	private int findLast(int offset, ITextViewer viewer) {
		int ofs = offset;
		try {
			Character currentChar = new Character(viewer.getDocument().getChar(
					ofs));
			if (currentChar.toString().matches("[ \t\n\f\r\\[(.;{}]+")) {
				ofs--;
				currentChar = new Character(viewer.getDocument().getChar(ofs));
			}
			while (!(currentChar.toString().matches("[ \t\n\f\r\\[(.;{}]+"))) {
				ofs--;
				currentChar = new Character(viewer.getDocument().getChar(ofs));
			}
		} catch (BadLocationException e) {
			// ignore
		}
		return ofs;
	}

	private String incrementString(String s) {
		if (s.length() <= 0)
		{
			return "";
		}
		char ch = s.charAt(s.length()-1);
		char newChar = (char)(ch+1);
		return s.substring(0, s.length()-1) + newChar;
	}
	
	public ICompletionProposal[] computeCompletionProposals(ITextViewer viewer,
			int offset) {

		try {
			IDocument doc = viewer.getDocument();

			int ofs = findLast(offset, viewer) + 1;

			String incompleteString = doc.get(ofs, offset - ofs);

			List<CompletionProposal> proposalList = new ArrayList<CompletionProposal>();

			if (editor.getReconcilingStrategy().getScopeOffsetMap() == null)
			{
				return null;
			}
			
			Scope moduleScope = editor.getReconcilingStrategy().getScopeOffsetMap()
					.getWithSmallestRange(offset);

			if (moduleScope == null)
			{
				return new ICompletionProposal[0];
			}
			
			Scope scope = Scope.findDeepestScope(moduleScope, offset);
			
			Map<String, Symbol> symbols = scope.getSymbols();

			// add symbols: new efficient O(log(n)) lookup
			String toIncompleteString = incrementString(incompleteString);
			Set<Entry<String, Symbol>> matchingEntries = null;

			if (incompleteString.length() > 0 && symbols instanceof TreeMap<?, ?>) {
				SortedMap<String, Symbol> matchingSymbols = ((TreeMap<String, Symbol>)symbols).subMap(incompleteString.toLowerCase(), toIncompleteString.toLowerCase());
				matchingEntries = matchingSymbols.entrySet();
			} else {
				matchingEntries = symbols.entrySet();
			}
			
			Iterator<Entry<String, Symbol>> it = matchingEntries.iterator();
			while (it.hasNext()) {
				Entry<String, Symbol> entry = it.next();
				String symbolName = entry.getKey();
				String symbolAssistDescription = entry.getValue().getSymbolDescriptionContentAssist();
				Image typeImage = null;
				if (entry.getValue() instanceof AltstepSymbol) {
					typeImage = TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_ALTSTEP);
				} else if (entry.getValue() instanceof TestcaseSymbol) {
					typeImage = TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_TESTCASE);
				} else if (entry.getValue() instanceof FunctionSymbol) {
					typeImage = TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_FUNCTION);
				} else if (entry.getValue() instanceof VariableInstanceSymbol) {
					typeImage = TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_VARIABLE);
				} else if (entry.getValue() instanceof ModuleSymbol) {
					typeImage = TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_MODULE);
				} else if (entry.getValue() instanceof ConstantSymbol) {
					typeImage = TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_VARIABLE);
				} else if (entry.getValue() instanceof TypeSymbol) {
					typeImage = TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_TYPE);
				} else if (entry.getValue() instanceof SubtypeSymbol) {
					typeImage = TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_TYPE);
				}
				
				CompletionProposal proposal = new CompletionProposal(symbolName, ofs, incompleteString.length(), symbolName.length(), typeImage, 
						symbolAssistDescription, null, "");
				proposalList.add(proposal);
			}

			
			// add ttcn3 keywords
			Image keywordImage = TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_KEYWORD);
			for (String element : TTCN3CodeScanner.TTCN3_KEYWORDS) {
				CompletionProposal proposal = new CompletionProposal(element, ofs, incompleteString.length(), element.length(), keywordImage, 
						element, null, "");
				if (element.toLowerCase().startsWith(incompleteString.toLowerCase()))
				{
					proposalList.add(proposal);
				}
				
				if (incompleteString.length() <= 0)
				{
					proposalList.add(proposal);
				}
			}
			
			return proposalList.toArray(new ICompletionProposal[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public IContextInformation[] computeContextInformation(ITextViewer viewer,
			int offset) {
		return null;
	}

	public char[] getCompletionProposalAutoActivationCharacters() {
		return null;
	}

	public char[] getContextInformationAutoActivationCharacters() {
		return null;
	}

	public String getErrorMessage() {
		return null;
	}

	public IContextInformationValidator getContextInformationValidator() {
		return null;
	}

}
