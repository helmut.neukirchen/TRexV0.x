package de.ugoe.cs.swe.trex.ui;

public class CompilerConstants {
	public final static String SELECTED_COMPILER = "de.ugoe.cs.swe.trex.ui.compiler.selectedCompiler";
	public final static String COMPILE_ALL_PROJECTS = "de.ugoe.cs.swe.trex.ui.compiler.compileAllProjects";
	public final static String VERBOSE = "de.ugoe.cs.swe.trex.ui.compiler.verbose";
}
