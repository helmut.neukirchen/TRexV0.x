package de.ugoe.cs.swe.trex.ui.compare;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.compare.contentmergeviewer.TextMergeViewer;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.TextViewer;
import org.eclipse.jface.text.rules.FastPartitioner;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.widgets.Composite;

import de.ugoe.cs.swe.trex.ui.editors.TTCN3Configuration;
import de.ugoe.cs.swe.trex.ui.editors.text.TTCN3PartitionScanner;

public class TTCN3ContentMergeViewer extends TextMergeViewer {

	private TTCN3Configuration ttcn3Configuration;

	public TTCN3ContentMergeViewer(Composite parent,
			CompareConfiguration configuration) {
		super(parent, configuration);
		if (ttcn3Configuration == null) {
			ttcn3Configuration = new TTCN3Configuration();
		}
	}

	@Override
	protected void configureTextViewer(TextViewer textViewer) {
		if (textViewer instanceof SourceViewer) {
			if (ttcn3Configuration == null) {
				ttcn3Configuration = new TTCN3Configuration();
			}			
			((SourceViewer) textViewer).configure(ttcn3Configuration);
		}
	}

	@Override
	protected IDocumentPartitioner getDocumentPartitioner() {
		IDocumentPartitioner partitioner = new FastPartitioner(
				new TTCN3PartitionScanner(),
				TTCN3PartitionScanner.PARTITION_TYPES);
		return partitioner;
	}

	@Override
	protected void handleDispose(DisposeEvent event) {
		ttcn3Configuration.dispose();
		super.handleDispose(event);
	}

}
