package de.ugoe.cs.swe.trex.ui.preferences;


import org.eclipse.swt.graphics.RGB;

import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;

public class PreferenceConstants {

	public static final String P_TTCN3_FORMATTER_TABS = TTCN3UIPlugin.PLUGIN_ID + ".formatter.tabs";

	public static final String P_TTCN3_FORMATTER_UNIX_NEWLINE = TTCN3UIPlugin.PLUGIN_ID + ".formatter.unixNewline";

	public static final String P_TTCN3_FORMATTER_NEWLINE_BEFORE_RUNS_ON = TTCN3UIPlugin.PLUGIN_ID + ".formatter.newlineBeforeRunsOn";

	public static final String P_TTCN3_FORMATTER_NEWLINE_BEFORE_SYSTEM = TTCN3UIPlugin.PLUGIN_ID + ".formatter.newlineBeforeSystem";

	public static final String P_TTCN3_FORMATTER_NEWLINE_BEFORE_RETURN = TTCN3UIPlugin.PLUGIN_ID + ".formatter.newlineBeforeReturn";

	public static final String P_TTCN3_FORMATTER_NEWLINE_BEFORE_EXCEPTION_SPEC = TTCN3UIPlugin.PLUGIN_ID + ".formatter.newlineBeforeExceptionSpec";

	public static final String P_TTCN3_FORMATTER_NEWLINE_BEFORE_FORMAL_PAR = TTCN3UIPlugin.PLUGIN_ID + ".formatter.newlineBeforeFormalPar";

	public static final String P_TTCN3_FORMATTER_NEWLINE_AFTER_FORMAL_PAR_LIST = TTCN3UIPlugin.PLUGIN_ID + ".formatter.newlineAfterFormalParList";

	public static final String P_TTCN3_FORMATTER_SPACES_BETWEEN_ASSIGNMENT = TTCN3UIPlugin.PLUGIN_ID + ".formatter.spacesBetweenAssignment";
	
	public static final String P_TTCN3_FORMATTER_SPACE_AFTER_COMMA = TTCN3UIPlugin.PLUGIN_ID + ".formatter.spaceAfterComma";

	public static final String P_TTCN3_FORMATTER_SPACES_AROUND_PARENTHESES = TTCN3UIPlugin.PLUGIN_ID + ".formatter.spacesAroundParentheses";
	
	public static final String P_TTCN3_FORMATTER_KR_STYLE = TTCN3UIPlugin.PLUGIN_ID + ".formatter.KRstyle";
	
	public static final String P_TTCN3_FORMATTER_SPACES_COUNT = TTCN3UIPlugin.PLUGIN_ID + ".formatter.spacesCount";

	public static final String P_TTCN3_FORMATTER_LINES_BETWEEN_MODULES = TTCN3UIPlugin.PLUGIN_ID + ".formatter.linesBetweenModules";

	public static final String P_TTCN3_FORMATTER_LINES_AFTER_CONTROL_PART = TTCN3UIPlugin.PLUGIN_ID + ".formatter.linesAfterControlPart";
	
	public static final String P_TTCN3_FORMATTER_LINES_AFTER_MODULE_DEFINITION = TTCN3UIPlugin.PLUGIN_ID + ".formatter.linesAfterModuleDefinition";

	public static final String P_BOOLEAN = "booleanPreference";

	public static final String P_CHOICE = "choicePreference";

	public static final String P_STRING = "stringPreference";

	// Default colors
	public static final RGB RGB_DEFAULT = new RGB(0, 0, 0);

	public static final RGB RGB_KEYWORD = new RGB(127, 0, 85);

	public static final RGB RGB_STRING = new RGB(42, 0, 255);

	public static final RGB RGB_COMMENT = new RGB(63, 127, 95);	
	
	// Folding labels
	public static final String P_TTCN3_FOLDING_ENABLEMENT = TTCN3UIPlugin.PLUGIN_ID + ".folding.enablement";
	
	public static final String P_TTCN3_FOLDING_TYPE = TTCN3UIPlugin.PLUGIN_ID + ".folding.type";
	
	public static final String P_TTCN3_FOLDING_TEMPLATE = TTCN3UIPlugin.PLUGIN_ID + ".folding.template";
	
	public static final String P_TTCN3_FOLDING_COMPONENT = TTCN3UIPlugin.PLUGIN_ID + ".folding.component";
	
	public static final String P_TTCN3_FOLDING_GROUP = TTCN3UIPlugin.PLUGIN_ID + ".folding.group";
	
	public static final String P_TTCN3_FOLDING_ALTSTEP = TTCN3UIPlugin.PLUGIN_ID + ".folding.altstep";
	
	public static final String P_TTCN3_FOLDING_FUNCTION = TTCN3UIPlugin.PLUGIN_ID + ".folding.function";
	
	public static final String P_TTCN3_FOLDING_TESTCASE = TTCN3UIPlugin.PLUGIN_ID + ".folding.testcase";
	
	public static final String P_TTCN3_FOLDING_MODULE = TTCN3UIPlugin.PLUGIN_ID + ".folding.module";
	
	public static final String P_TTCN3_SEMANTIC_ANALYSIS_ENABLEMENT = TTCN3UIPlugin.PLUGIN_ID + ".semanticanalysis.enabled";

	// Add by Ingvi and Omar 23.11.2010
	public static final String P_TTCN3_MARK_OCCURRENCES_ENABLEMENT = TTCN3UIPlugin.PLUGIN_ID + ".markoccurrences.enabled";

	// Add by Einar and Niels (TTC-312)
	public static final String P_TTCN3_OUTLINE_TREE_VIEW_EXPANDED_BY_DEFAULT = TTCN3UIPlugin.PLUGIN_ID + ".treeoutlineview.enabled";

	//START modified by sukiyuki 2011-2-7
	//match bracket
	public static final String EDITOR_MATCHING_BRACKETS = "matchingBrackets";
	
	public static final String EDITOR_MATCHING_BRACKETS_COLOR = "matchingBracketsColor";	
	//END   modified by sukiyuki 2011-2-7
}
