package de.ugoe.cs.swe.trex.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.dialogs.MessageDialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3ReconcilingStrategy;
import de.ugoe.cs.swe.trex.ui.preferences.PreferenceConstants;

/**
 * Toggles "folderDisabled" property.
 */

public class DisableFolderHandler extends AbstractHandler {

	private static final String ERROR_HEADLINE = "Error Enabling/Disabling Folder"; //$NON-NLS-1$

	private IStructuredSelection selection;

	private static final String QUALIFIER = TTCN3UIPlugin.PLUGIN_ID;

	private static final String LOCAL_NAME = "folderDisabled";

	public static final String PROPERTY_SET = "true";

	public static final QualifiedName QUALIFIED_NAME = new QualifiedName(
			QUALIFIER, LOCAL_NAME);

	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selectedItem = HandlerUtil.getActiveWorkbenchWindow(event)
		        .getActivePage().getSelection();

	    if (selectedItem != null & selectedItem instanceof IStructuredSelection)
	      selection = (IStructuredSelection) selectedItem;

		if (selection.size() == 1) {

			Object firstElement = selection.getFirstElement();
			if (firstElement instanceof IFolder) {
				try {
					IFolder folder = (IFolder) firstElement;
					String currentDisabledPropertyValue = folder
							.getPersistentProperty(QUALIFIED_NAME);

					if (currentDisabledPropertyValue == null) {
						setDisabledProperty(folder, PROPERTY_SET);
					} else {
						setDisabledProperty(folder, null);
					}
				} catch (CoreException e) {
					MessageDialog.openError(Display.getDefault()
							.getActiveShell(), ERROR_HEADLINE,
							"The folder could not be enabled/disabled."); //$NON-NLS-1$
				}
				PlatformUI.getWorkbench().getDecoratorManager().update(
						"de.ugoe.cs.swe.trex.ui.folderDecorator");
			} else
				// shouldn't happen, but handle error condition
				MessageDialog
						.openError(Display.getDefault().getActiveShell(),
								ERROR_HEADLINE,
								"The element that was selected was not of the right type."); //$NON-NLS-1$
		} else
			// shouldn't happen, but handle error condition
			MessageDialog.openError(Display.getDefault().getActiveShell(),
					ERROR_HEADLINE,
					"An invalid number of resources were selected."); //$NON-NLS-1$

		return null;
	}

	/**
	 * Sets folderDisabled property for this IFolder and all subfolders
	 */
	private void setDisabledProperty(IFolder folder, String value)
			throws CoreException {
		folder.setPersistentProperty(QUALIFIED_NAME, value);
		PlatformUI.getWorkbench().getDecoratorManager().setEnabled("de.ugoe.cs.swe.trex.ui.folderDecorator", true);
		IResource[] members = folder.members(false);
		for (int i = 0; i < members.length; i++) {
			IResource member = members[i];
			if (member instanceof IFolder) {
				setDisabledProperty((IFolder) member, value);
			} else if (member instanceof IFile) {
				IFile file = (IFile) member;
				if (value != null) {
					TTCN3UIPlugin.debugMessage("removing " + member.getFullPath() + " from analyzerfactory");
					TTCN3AnalyzerFlyweightFactory.getInstance().removeTTCN3Analyzer(file.getFullPath().toPortableString());
					TTCN3AnalyzerFlyweightFactory.getInstance().postProcess();
				} else {
					TTCN3UIPlugin.debugMessage("adding " + member.getFullPath() + " to analyzerfactory");
					TTCN3Analyzer analyzer = TTCN3AnalyzerFlyweightFactory.getInstance().getTTCN3Analyzer(file, true);
					List<Exception> exceptions = new ArrayList<Exception>();
					if (analyzer.getCode().length() > 0) {
						try {
							analyzer.analyze();
						} catch (Exception e) {
						}
						if (analyzer.getExceptions() != null)
							exceptions.addAll(analyzer.getExceptions());

						analyzer.postProcess();
						try {
							boolean isSemanticAnalysisEnabled = Platform.getPreferencesService().getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_SEMANTIC_ANALYSIS_ENABLEMENT,false,null);
							analyzer.semanticalAnalysis(isSemanticAnalysisEnabled);
						} catch (Exception e) {
						}
						TTCN3AnalyzerFlyweightFactory.getInstance().postProcess();

						if (!(exceptions.isEmpty())) {
							for (int n = 0; n < exceptions.size(); n++) {
								TTCN3ReconcilingStrategy.createMarker(file, (Exception) exceptions.get(n), IMarker.SEVERITY_ERROR,
										TTCN3ReconcilingStrategy.MARKER_EXCEPTION,null);
							}
						}
					}
				}
			}
		}
	}

	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			this.selection = (IStructuredSelection) selection;
		} else {
			this.selection = StructuredSelection.EMPTY;
		}
	}
}
