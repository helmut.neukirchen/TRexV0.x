package de.ugoe.cs.swe.trex.ui.editors.text;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.text.TextPresentation;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;

public class HTML2TextReader extends SubstitutionTextReader {
	private static final String EMPTY_STRING= ""; //$NON-NLS-1$
	private static final Map<String, String> FG_ENTITY_LOOKUP;
	private static final Set<String> FG_TAGS;

	static {

		FG_TAGS= new HashSet<String>();
		HTML2TextReader.FG_TAGS.add("b"); //$NON-NLS-1$
		HTML2TextReader.FG_TAGS.add("br"); //$NON-NLS-1$
		HTML2TextReader.FG_TAGS.add("h5"); //$NON-NLS-1$
		HTML2TextReader.FG_TAGS.add("p"); //$NON-NLS-1$
		HTML2TextReader.FG_TAGS.add("dl"); //$NON-NLS-1$
		HTML2TextReader.FG_TAGS.add("dt"); //$NON-NLS-1$
		HTML2TextReader.FG_TAGS.add("dd"); //$NON-NLS-1$
		HTML2TextReader.FG_TAGS.add("li"); //$NON-NLS-1$
		HTML2TextReader.FG_TAGS.add("ul"); //$NON-NLS-1$
		HTML2TextReader.FG_TAGS.add("pre"); //$NON-NLS-1$

		FG_ENTITY_LOOKUP= new HashMap<String, String>(7);
		HTML2TextReader.FG_ENTITY_LOOKUP.put("lt", "<"); //$NON-NLS-1$ //$NON-NLS-2$
		HTML2TextReader.FG_ENTITY_LOOKUP.put("gt", ">"); //$NON-NLS-1$ //$NON-NLS-2$
		HTML2TextReader.FG_ENTITY_LOOKUP.put("nbsp", " "); //$NON-NLS-1$ //$NON-NLS-2$
		HTML2TextReader.FG_ENTITY_LOOKUP.put("amp", "&"); //$NON-NLS-1$ //$NON-NLS-2$
		HTML2TextReader.FG_ENTITY_LOOKUP.put("circ", "^"); //$NON-NLS-1$ //$NON-NLS-2$
		HTML2TextReader.FG_ENTITY_LOOKUP.put("tilde", "~"); //$NON-NLS-2$ //$NON-NLS-1$
		HTML2TextReader.FG_ENTITY_LOOKUP.put("quot", "\"");		 //$NON-NLS-1$ //$NON-NLS-2$
	}

	private int fCounter= 0;
	private TextPresentation fTextPresentation;
	private int fBold= 0;
	private int fStartOffset= -1;
	private boolean fInParagraph= false;
	private boolean fIsPreformattedText= false;

	/**
	 * Transforms the html text from the reader to formatted text.
	 * @param presentation If not <code>null</code>, formattings will be applied to
	 * the presentation.
	*/
	public HTML2TextReader(Reader reader, TextPresentation presentation) {
		super(new PushbackReader(reader));
		fTextPresentation= presentation;
	}

	@Override
	public int read() throws IOException {
		int c= super.read();
		if (c != -1)
		{
			++ fCounter;
		}
		return c;
	}

	protected void startBold() {
		if (fBold == 0)
		{
			fStartOffset= fCounter;
		}
		++ fBold;
	}

	protected void startPreformattedText() {
		fIsPreformattedText= true;
		setSkipWhitespace(false);
	}

	protected void stopPreformattedText() {
		fIsPreformattedText= false;
		setSkipWhitespace(true);
	}

	protected void stopBold() {
		-- fBold;
		if (fBold == 0) {
			if (fTextPresentation != null) {
				fTextPresentation.addStyleRange(new StyleRange(fStartOffset, fCounter - fStartOffset, null, null, SWT.BOLD));
			}
			fStartOffset= -1;
		}
	}

	/*
	 * @see org.eclipse.jdt.internal.ui.text.SubstitutionTextReader#computeSubstitution(int)
	 */
	@Override
	protected String computeSubstitution(int c) throws IOException {

		if (c == '<')
		{
			return  processHTMLTag();
		}
		else if (c == '&')
		{
			return processEntity();
		}
		else if (fIsPreformattedText)
		{
			return processPreformattedText(c);
		}

		return null;
	}

	private String html2Text(String html) {

		if (html == null || html.length() == 0)
		{
			return HTML2TextReader.EMPTY_STRING;
		}

		String tag= html;
		if ('/' == tag.charAt(0))
		{
			tag= tag.substring(1);
		}

		if (!HTML2TextReader.FG_TAGS.contains(tag))
		{
			return HTML2TextReader.EMPTY_STRING;
		}


		if ("pre".equals(html)) { //$NON-NLS-1$
			startPreformattedText();
			return HTML2TextReader.EMPTY_STRING;
		}

		if ("/pre".equals(html)) { //$NON-NLS-1$
			stopPreformattedText();
			return HTML2TextReader.EMPTY_STRING;
		}

		if (fIsPreformattedText)
		{
			return HTML2TextReader.EMPTY_STRING;
		}

		if ("b".equals(html)) { //$NON-NLS-1$
			startBold();
			return HTML2TextReader.EMPTY_STRING;
		}

		if ("h5".equals(html) || "dt".equals(html)) { //$NON-NLS-1$ //$NON-NLS-2$
			startBold();
			return HTML2TextReader.EMPTY_STRING;
		}

		if ("dl".equals(html)) //$NON-NLS-1$
		{
			return SubstitutionTextReader.LINE_DELIM;
		}

		if ("dd".equals(html)) //$NON-NLS-1$
		{
			return "\t"; //$NON-NLS-1$
		}

		if ("li".equals(html)) //$NON-NLS-1$
		{
			return SubstitutionTextReader.LINE_DELIM; /*+ JavaUIMessages.HTML2TextReader_listItemPrefix;*/
		}

		if ("/b".equals(html)) { //$NON-NLS-1$
			stopBold();
			return HTML2TextReader.EMPTY_STRING;
		}

		if ("p".equals(html))  { //$NON-NLS-1$
			fInParagraph= true;
			return SubstitutionTextReader.LINE_DELIM;
		}

		if ("br".equals(html)) //$NON-NLS-1$
		{
			return SubstitutionTextReader.LINE_DELIM;
		}

		if ("/p".equals(html))  { //$NON-NLS-1$
			boolean inParagraph= fInParagraph;
			fInParagraph= false;
			return inParagraph ? HTML2TextReader.EMPTY_STRING : SubstitutionTextReader.LINE_DELIM;
		}

		if ("/h5".equals(html) || "/dt".equals(html)) { //$NON-NLS-1$ //$NON-NLS-2$
			stopBold();
			return SubstitutionTextReader.LINE_DELIM;
		}

		if ("/dd".equals(html)) //$NON-NLS-1$
		{
			return SubstitutionTextReader.LINE_DELIM;
		}

		return HTML2TextReader.EMPTY_STRING;
	}

	/*
	 * A '<' has been read. Process a html tag
	 */
	private String processHTMLTag() throws IOException {

		StringBuffer buf= new StringBuffer();
		int ch;
		do {

			ch= nextChar();

			while (ch != -1 && ch != '>') {
				buf.append(Character.toLowerCase((char) ch));
				ch= nextChar();
				if (ch == '"'){
					buf.append(Character.toLowerCase((char) ch));
					ch= nextChar();
					while (ch != -1 && ch != '"'){
						buf.append(Character.toLowerCase((char) ch));
						ch= nextChar();
					}
				}
				if (ch == '<'){
					unread(ch);
					return '<' + buf.toString();
				}
			}

			if (ch == -1)
			{
				return null;
			}

			int tagLen= buf.length();
			// needs special treatment for comments
			if ((tagLen >= 3 && "!--".equals(buf.substring(0, 3))) //$NON-NLS-1$
				&& !(tagLen >= 5 && "--".equals(buf.substring(tagLen - 2)))) { //$NON-NLS-1$
				// unfinished comment
				buf.append(ch);
			} else {
				break;
			}
		} while (true);

		return html2Text(buf.toString());
	}

	private String processPreformattedText(int c) {
		if  (c == '\r' || c == '\n')
		{
			fCounter++;
		}
		return null;
	}


	private void unread(int ch) throws IOException {
		((PushbackReader) getReader()).unread(ch);
	}

	protected String entity2Text(String symbol) {
		if (symbol.length() > 1 && symbol.charAt(0) == '#') {
			int ch;
			try {
				if (symbol.charAt(1) == 'x') {
					ch= Integer.parseInt(symbol.substring(2), 16);
				} else {
					ch= Integer.parseInt(symbol.substring(1), 10);
				}
				return HTML2TextReader.EMPTY_STRING + (char)ch;
			} catch (NumberFormatException e) {
				// ignore
			}
		} else {
			String str= HTML2TextReader.FG_ENTITY_LOOKUP.get(symbol);
			if (str != null) {
				return str;
			}
		}
		return "&" + symbol; // not found //$NON-NLS-1$
	}

	/*
	 * A '&' has been read. Process a entity
	 */
	private String processEntity() throws IOException {
		StringBuffer buf= new StringBuffer();
		int ch= nextChar();
		while (Character.isLetterOrDigit((char)ch) || ch == '#') {
			buf.append((char) ch);
			ch= nextChar();
		}

		if (ch == ';')
		{
			return entity2Text(buf.toString());
		}

		buf.insert(0, '&');
		if (ch != -1)
		{
			buf.append((char) ch);
		}
		return buf.toString();
	}

}
