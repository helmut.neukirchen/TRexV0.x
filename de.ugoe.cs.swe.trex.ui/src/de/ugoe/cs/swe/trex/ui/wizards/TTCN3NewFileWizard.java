package de.ugoe.cs.swe.trex.ui.wizards;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3MultiPageEditor;

/**
 * This wizard's role is to create a new file resource in the provided
 * container. If the container resource (a folder or a project) is selected in
 * the workspace when the wizard is opened, it will accept it as the target
 * container. The wizard creates one file with the extension "ttcn3".
 */

public class TTCN3NewFileWizard extends Wizard implements INewWizard {
	private TTCN3NewFileWizardPage page;

	private ISelection selection;

	/**
	 * Constructor for TTCN3NewFileWizard.
	 */
	public TTCN3NewFileWizard() {
		super();
		setNeedsProgressMonitor(true);
	}

	/**
	 * Adding the page to the wizard.
	 */

	@Override
	public void addPages() {
		page = new TTCN3NewFileWizardPage(selection);
		addPage(page);
	}

	/**
	 * This method is called when 'Finish' button is pressed in the wizard. We
	 * will create an operation and run it using wizard as execution context.
	 */
	@Override
	public boolean performFinish() {
		final String projectName = page.getProjectName();
		final String fileName = page.getFileName();
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor)
					throws InvocationTargetException {
				try {
					doFinish(projectName, fileName, monitor);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException
					.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * The worker method. It will find the container, create the file if missing
	 * or just replace its contents, and open the editor on the newly created
	 * file.
	 */

	void doFinish(String containerName, String fileName,
			IProgressMonitor monitor) throws CoreException {
		// create a sample file
		monitor.beginTask("Creating " + fileName, 2);
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IResource resource = root.findMember(new Path(containerName));
		if (!resource.exists() || !(resource instanceof IContainer)) {
			throwCoreException("Container \"" + containerName
					+ "\" does not exist.");
		}
		IContainer container = (IContainer) resource;
		final IFile iFile = container.getFile(new Path(fileName));
		final File file = iFile.getLocation().toFile();
		try {
			OutputStream os = new FileOutputStream(file, false);

			os.close();
		} catch (IOException e) {
			e.printStackTrace();
			throwCoreException("Error writing to file " + file.toString());
		}
		container.refreshLocal(IResource.DEPTH_INFINITE, monitor);
		monitor.worked(1);
		monitor.setTaskName("Opening file for editing...");
		getShell().getDisplay().asyncExec(new Runnable() {
			public void run() {
				IWorkbenchPage wbpage = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage();
				try {
					TTCN3MultiPageEditor mpe = (TTCN3MultiPageEditor) IDE.openEditor(wbpage, iFile, true);
					TTCN3Editor editor = mpe.getFEditor();
					
					String newFileContent = "module " + page.getModuleName() + " {\n\t";
					
					int offset = newFileContent.indexOf("\t") + 1;
					
					if (page.getDefaultControlPartFlag()) {
						newFileContent += "\n\tcontrol {\n\t}\n";
					}
					
					newFileContent += "\n}";
					
					editor.getDocument().set(newFileContent);
					
					editor.selectAndReveal(offset, 0);
				} catch (PartInitException e) {
					// ignore
				}
			}
		});
		monitor.worked(1);
	}

	private void throwCoreException(String message) throws CoreException {
		IStatus status = new Status(IStatus.ERROR, "ttcn3", IStatus.OK,
				message, null);
		throw new CoreException(status);
	}

	/**
	 * We will accept the selection in the workbench to see if we can initialize
	 * from it.
	 * 
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection select) {
		this.selection = select;
	}
}