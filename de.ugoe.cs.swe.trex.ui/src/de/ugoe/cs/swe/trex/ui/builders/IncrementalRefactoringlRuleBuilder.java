package de.ugoe.cs.swe.trex.ui.builders;

import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3AnalyzerFlyweightFactory;
import de.ugoe.cs.swe.trex.ui.TTCN3ResourceHelper;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3ReconcilerState;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3ReconcilingStrategy.ResourceCountVisitor;
import de.ugoe.cs.swe.trex.ui.views.metrics.TTCN3MetricsView;

public class IncrementalRefactoringlRuleBuilder extends
		IncrementalProjectBuilder implements Observer {

	public static final String BUILDER_ID = "de.ugoe.cs.swe.trex.ui.rulebuilder";

	private int fKind;

	private Map<?,?> fArgs;

	private IProgressMonitor fMonitor;

	private boolean tooEarly = false;

	private static boolean overrideStatus = false;

	public IncrementalRefactoringlRuleBuilder() {
		TTCN3UIPlugin
				.debugMessage("---> creating refactoring rule builder, register as observer");

		TTCN3ReconcilerState.getInstance().deleteObservers();
		TTCN3ReconcilerState.getInstance().addObserver(this);
	}

	// ---------------------------------------------------------------------------

	public static class MarkerRemovalVisitor implements IResourceVisitor {
		public boolean visit(IResource resource) throws CoreException {
			if (TTCN3ResourceHelper.isTTCN3File(resource)) {
				final IFile file = (IFile) resource;
				deleteRuleMarkers(file);
			}
			return true;
		}
	}

	/**
	 * Delete all <code>IMarker</code> objects of the given type from the
	 * given file.
	 * 
	 * @param file
	 *            the <code>IFile</code> to delete markers from
	 * @param markerType
	 *            the type of markers to delete
	 */
	public static void deleteRuleMarkers(IFile file) {
		final String unreferencedRule = "de.ugoe.cs.swe.trex.ui.unreferencedrulemarker";
		final String referencesRule = "de.ugoe.cs.swe.trex.ui.referencesrulemarker";
		final String paramsRule = "de.ugoe.cs.swe.trex.ui.paramsrulemarker";
		final String mergeRule = "de.ugoe.cs.swe.trex.ui.mergerulemarker";
		final String[] markerTypes = { unreferencedRule, referencesRule,
				paramsRule, mergeRule };

		for (String markerType : markerTypes) {
			try {
				file.deleteMarkers(markerType, true, IResource.DEPTH_INFINITE);
				// TTCN3UIPlugin.debugMessage("--> Deleted '" + markerType + "'
				// Markers on file: " + file.getName());
			} catch (CoreException e) {
				TTCN3UIPlugin.log(e);
			}
		}
	}

	// ---------------------------------------------------------------------------

	private class MetricsVisitor implements IResourceVisitor {
		TTCN3AnalyzerFlyweightFactory analyzerFactory;

		private IProgressMonitor progressMonitor;

		private int resourcesAnalyzed = 1;

		private int totalResources = 0;

		public MetricsVisitor() {
			analyzerFactory = TTCN3AnalyzerFlyweightFactory.getInstance();
		}

		public boolean visit(final IResource resource) {
			if (TTCN3ResourceHelper.isTTCN3File(resource)) {
				IFile file = (IFile) resource;
				if (progressMonitor != null) {
					progressMonitor.subTask("Calculating metrics for file ("
							+ resourcesAnalyzed + "/" + totalResources + "): "
							+ file.getFullPath());
				}
				TTCN3UIPlugin.debugMessage("metric analysis on file: "
						+ file.getFullPath());

				if (!analyzerFactory.hasAnalyzer(file))
				{
					return true;
				}

				TTCN3Analyzer analyzer = analyzerFactory.getTTCN3Analyzer(file,
						false);

				try {
					long time = System.currentTimeMillis();
					analyzer.runMetricAnalysis();
					TTCN3UIPlugin
							.debugMessage("...finished metric analysis in "
									+ new Long(System.currentTimeMillis()
											- time) + " ms");
				} catch (Exception e) {
					TTCN3UIPlugin.debugMessage("...failed metrics!");
				}
				resourcesAnalyzed++;
				if (progressMonitor != null)
				{
					progressMonitor.worked(resourcesAnalyzed);
				}
			}
			return true;
		}

		public void setProgressMonitor(IProgressMonitor progressMonitor) {
			this.progressMonitor = progressMonitor;
		}

		public void setTotalResources(int totalResources) {
			this.totalResources = totalResources;
		}

//		public int getTotalResources() {
//			return totalResources;
//		}

	}

	// ---------------------------------------------------------------------------

	private IProject[] internalBuild(int kind, @SuppressWarnings("rawtypes") Map args,
			IProgressMonitor monitor) throws CoreException {

		if (!BuilderHelper.isBuilderEnabled(getProject(), BUILDER_ID))
		{
			return null;
		}

		TTCN3Analyzer.TEMPLATE_TO_SIMILAR.clear();

		ResourceCountVisitor resourceCountVisitor = new ResourceCountVisitor();
		getProject().accept(resourceCountVisitor);
		getProject().accept(new MarkerRemovalVisitor());

		MetricsVisitor metricsVisitor = new MetricsVisitor();
		metricsVisitor.setTotalResources(resourceCountVisitor
				.getResourcesCount());
		metricsVisitor.setProgressMonitor(monitor);
		if (kind == IncrementalProjectBuilder.FULL_BUILD) {
			TTCN3UIPlugin
					.debugMessage("---> full refactoring rule builder call");
			getProject().accept(metricsVisitor);
		} else {
			TTCN3UIPlugin
					.debugMessage("---> delta refactoring rule builder call");
			getProject().accept(metricsVisitor);
		}
		TTCN3MetricsView.refreshViewer();
		TTCN3ReconcilerState.getInstance().setReconciler(null);
		return null;
	}

	// ---------------------------------------------------------------------------

	@Override
	protected IProject[] build(int kind, @SuppressWarnings("rawtypes") Map args, IProgressMonitor monitor)
			throws CoreException {

		this.fKind = kind;
		this.fArgs = args;
		this.fMonitor = monitor;

		boolean isReconilerFinished = (TTCN3ReconcilerState.getInstance()
				.getReconciler() != null);

		if (isReconilerFinished || overrideStatus) {
			internalBuild(kind, args, monitor);
		} else {
			tooEarly = true;
		}
		return null;
	}

	// ---------------------------------------------------------------------------

	public void update(Observable arg0, Object arg1) {
		TTCN3UIPlugin.debugMessage("---> observer update");
		if (tooEarly
				|| ((arg1 instanceof Boolean) && ((Boolean) arg1)
						.booleanValue())) {
			try {
				internalBuild(fKind, fArgs, fMonitor);
			} catch (CoreException e) {
				e.printStackTrace();
			}
			tooEarly = false;
		}
	}

	// ---------------------------------------------------------------------------

	public static boolean isOverrideStatus() {
		return overrideStatus;
	}

	// ---------------------------------------------------------------------------

	public static void setOverrideStatus(boolean overrideStatus) {
		IncrementalRefactoringlRuleBuilder.overrideStatus = overrideStatus;
	}

}
