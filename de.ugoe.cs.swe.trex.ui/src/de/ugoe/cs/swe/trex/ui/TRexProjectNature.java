package de.ugoe.cs.swe.trex.ui;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;

public class TRexProjectNature implements IProjectNature {

	private IProject project;

	public void configure() throws CoreException {
		//IncrementalRefactoringlRuleBuilder.addBuilderToProject(project);
	}

	public void deconfigure() throws CoreException {
	}

	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

}
