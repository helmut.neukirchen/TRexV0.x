package de.ugoe.cs.swe.trex.ui.views.findreferences;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.search.ui.text.AbstractTextSearchViewPage;
import org.eclipse.search.ui.text.Match;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3LexerTokenTypes;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPluginImages;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3MultiPageEditor;

/**
 * The search result page class is responsible for displaying the
 * elements. We use a subclass of the standard eclipse implementation
 * which uses a treeviewer to display its elements.
 */
public class TTCN3FindReferencesSearchResultPage extends AbstractTextSearchViewPage {

	private TreeViewer viewer;

	/**
	 * Constructor. Does nothing special here, just call superclass
	 */
	public TTCN3FindReferencesSearchResultPage() {
		super(AbstractTextSearchViewPage.FLAG_LAYOUT_TREE);
	};

	@Override
	protected void clear() {
		viewer.refresh();
	}

	@Override
	protected void elementsChanged(Object[] objects) {
		viewer.refresh();
	}

	@Override
	protected void configureTableViewer(TableViewer viewer) {
	}

	/**
	 * Configures our tree viewer. Sets the DoubeClickListener,
	 * the content provider and the label provider
	 */
	@Override
	protected void configureTreeViewer(final TreeViewer viewer) {
		this.viewer = viewer;

		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				Object o = viewer.getTree().getSelection()[0].getData();
				if (o instanceof Match) {
					Match m = (Match) o;
					try {
						showMatch(m, 0, 0, true);
					} catch (PartInitException e) {
					}
				}
			}
		});

		this.viewer
				.setContentProvider(new TTCN3FindReferencesContentProvider());

		// Set the LabelProvider as an anonymous class
		viewer.setLabelProvider(new LabelProvider() {
			
			/**
			 * Returns the image for the given element
			 */
			public Image getImage(Object element) {
				ImageDescriptor descriptor = TTCN3UIPluginImages.DESC_MISC_DEFAULT;
				
				if (element instanceof FileElement) {
					descriptor = TTCN3UIPluginImages.DESC_TTCN3;
				} else if (element instanceof Match) {
					Match match = (Match) element;
					if (match.getElement() instanceof ModuleElement) {
						descriptor = TTCN3UIPluginImages.DESC_MISC_DEFAULT;
					} else if (match.getElement() instanceof CompoundElement) {
						CompoundElement compElem = (CompoundElement) match
								.getElement();
						switch (compElem.getNode().getType()) {
						case TTCN3LexerTokenTypes.GroupDef:
							descriptor = TTCN3UIPluginImages.DESC_GROUP;
							break;
						case TTCN3LexerTokenTypes.TestcaseDef:
							descriptor = TTCN3UIPluginImages.DESC_TESTCASE;
							break;
						case TTCN3LexerTokenTypes.TemplateDef:
							descriptor = TTCN3UIPluginImages.DESC_TESTCASE;
							break;
						case TTCN3LexerTokenTypes.FunctionDef:
							descriptor = TTCN3UIPluginImages.DESC_FUNCTION;
							break;
						case TTCN3LexerTokenTypes.ComponentDef:
							descriptor = TTCN3UIPluginImages.DESC_COMPONENT;
							break;
						case TTCN3LexerTokenTypes.AltstepDef:
							descriptor = TTCN3UIPluginImages.DESC_ALTSTEP;
							break;
						default:
							descriptor = TTCN3UIPluginImages.DESC_MISC_DEFAULT;
							break;
						}
					} else if (match.getElement() instanceof AtomicElement) {
						descriptor = TTCN3UIPluginImages.DESC_VARIABLE;
					}
				}

				Image image = TTCN3UIPluginImages.imageCacheGet(descriptor);
				return image;
			}

			/**
			 * Returns the text label for the given element
			 */
			public String getText(Object element) {

				if (element instanceof FileElement) {
					// Return the file name if we have a FileElement
					return ((FileElement) element).getFile().getName();
				}
				if (element instanceof Match) {
					Match match = (Match) element;
					if (match.getElement() instanceof ModuleElement) {
						return TTCN3Aux
								.getIdentifierNameFromNode(((ModuleElement) match
										.getElement()).getNode())
								+ " (Module)";
					} else if (match.getElement() instanceof CompoundElement) {
						
						// If it´s an compound element, return the label of this
						// element along with the element type
						String label = "Unknown";

						CompoundElement compElem = (CompoundElement) match
								.getElement();
						LocationAST compNode = compElem.getNode();
						switch (compNode.getType()) {
						case TTCN3LexerTokenTypes.ComponentDef:
							label = TTCN3Aux
									.getIdentifierNameFromNode(compNode)
									+ " (Component)";
							break;
						case TTCN3LexerTokenTypes.TestcaseDef:
							label = TTCN3Aux
									.getIdentifierNameFromNode(compNode)
									+ " (Testcase)";
							break;
						case TTCN3LexerTokenTypes.TemplateDef:
							label = TTCN3Aux
									.getIdentifierNameFromNode(compNode)
									+ " (Template)";
							break;
						case TTCN3LexerTokenTypes.AltstepDef:
							label = TTCN3Aux
									.getIdentifierNameFromNode(compNode)
									+ " (Altstep)";
							break;
						case TTCN3LexerTokenTypes.FunctionDef:
							label = TTCN3Aux
									.getIdentifierNameFromNode(compNode)
									+ " (Function)";
							break;
						case TTCN3LexerTokenTypes.GroupDef:
							label = TTCN3Aux
									.getIdentifierNameFromNode(compNode)
									+ " (Group)";
							break;
						case TTCN3LexerTokenTypes.ImportDef:
							label = "Import";
							break;
						}
						return label;
					} else if (match.getElement() instanceof AtomicElement) {
						
						// If it´s an atomic element, return name of this element 
						// along with sourc file line
						AtomicElement atomic = (AtomicElement) match
								.getElement();
						LocationAST refNode = atomic.getReference()
								.getReferenceNode();
						return (refNode.getText() + " (Line "
								+ refNode.getLine() + ")");
					}
					return "Unknown: " + element.toString();
				}
				return "Unknown: " + element.toString();
			}
		});
	}

	/**
	 * Shows the match in the editor.
	 * 
	 * @param match The match to show
	 * @param currentOffset The match offset
	 * @param currentLength The match length
	 */
	@Override
	protected void showMatch(Match match, int currentOffset, int currentLength,
			boolean activate) throws PartInitException {
		if (match.getElement() instanceof AtomicElement) {
			AtomicElement atomic = (AtomicElement) match.getElement();
			FileElement elem = atomic.getCompoundElement().getModuleElement()
					.getFileElement();
			IFile file = elem.getFile();

			TTCN3MultiPageEditor mpe = (TTCN3MultiPageEditor) IDE.openEditor(
					PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage(), file, true);
			TTCN3Editor editor = mpe.getFEditor();

			LocationAST refNode = atomic.getReference().getReferenceNode();

			editor.selectRange(refNode.getOffset(), refNode.getEndOffset()
					- refNode.getOffset());
		} else if (match.getElement() instanceof CompoundElement) {
			CompoundElement comp = (CompoundElement) match.getElement();
			FileElement elem = comp.getModuleElement().getFileElement();
			IFile file = elem.getFile();

			TTCN3MultiPageEditor mpe = (TTCN3MultiPageEditor) IDE.openEditor(
					PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage(), file, true);
			TTCN3Editor editor = mpe.getFEditor();

			LocationAST compNode = comp.getNode();

			LocationAST idNode = TTCN3Aux.getIdentifierNodeFromNode(compNode);

			editor.selectRange(idNode.getOffset(), idNode.getEndOffset()
					- idNode.getOffset());
		} else if (match.getElement() instanceof ModuleElement) {
			ModuleElement module = (ModuleElement) match.getElement();
			FileElement elem = module.getFileElement();
			IFile file = elem.getFile();

			TTCN3MultiPageEditor mpe = (TTCN3MultiPageEditor) IDE.openEditor(
					PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage(), file, true);
			TTCN3Editor editor = mpe.getFEditor();

			LocationAST moduleNode = module.getNode();

			LocationAST idNode = TTCN3Aux.getIdentifierNodeFromNode(moduleNode);

			editor.selectRange(idNode.getOffset(), idNode.getEndOffset()
					- idNode.getOffset());
		} else if (match.getElement() instanceof FileElement) {
			FileElement fileElem = (FileElement) match.getElement();

			IFile file = fileElem.getFile();

			IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage(), file, true);
		}
	}
}
