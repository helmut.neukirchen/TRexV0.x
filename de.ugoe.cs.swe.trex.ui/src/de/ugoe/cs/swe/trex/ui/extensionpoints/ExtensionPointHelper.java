package de.ugoe.cs.swe.trex.ui.extensionpoints;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

public class ExtensionPointHelper {

	public static ArrayList<Object> getContributors(String extensionPoint) {
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IExtensionPoint ep = reg.getExtensionPoint(extensionPoint);
		IExtension[] extensions = ep.getExtensions();
		ArrayList<Object> result = new ArrayList<Object>();
		TreeMap<Integer, Object> contributors = new TreeMap<Integer, Object>();
		for (int i = 0; i < extensions.length; i++) {
			IExtension ext = extensions[i];
			IConfigurationElement[] ce = ext.getConfigurationElements();

			for (int j = 0; j < ce.length; j++) {
				try {
					Object obj;
					obj = ce[j].createExecutableExtension("class");
					int priority = Integer.parseInt(ce[j]
							.getAttribute("priority"));
					contributors.put(new Integer(priority), obj);
				} catch (CoreException e) {
					return null;
				}
			}
		}
		Iterator<Integer> it = contributors.keySet().iterator();
		while (it.hasNext()) {
			Integer item = it.next();
			result.add(contributors.get(item));
		}

		return result;
	}

}
