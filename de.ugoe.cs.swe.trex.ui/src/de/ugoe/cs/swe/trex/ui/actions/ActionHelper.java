package de.ugoe.cs.swe.trex.ui.actions;

import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3MultiPageEditor;

public class ActionHelper {
	
	public static TTCN3Editor getActiveEditor() {
		IWorkbenchWindow window= PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window != null) {
			IWorkbenchPage page= window.getActivePage();
			if (page != null) {
				IEditorPart editor= page.getActiveEditor();
				if (editor instanceof TTCN3MultiPageEditor)
					return ((TTCN3MultiPageEditor)editor).getFEditor();
			}
		}
		return null;
	}

}
