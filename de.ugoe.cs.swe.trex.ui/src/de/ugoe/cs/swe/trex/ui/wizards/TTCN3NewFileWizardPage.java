package de.ugoe.cs.swe.trex.ui.wizards;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.IDialogPage;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;

/**
 * The "New" wizard page allows setting the container for the new file as well
 * as the file name. The page will only accept file name with an extension that
 * matches the expected one.
 */

public class TTCN3NewFileWizardPage extends WizardPage {
	private Text projectNameTextField;

	private Text moduleNameTextField;

	private ISelection selection;
	
	private Button createDefaultControlPartCheckBox;

	/**
	 * Constructor for SampleNewWizardPage.
	 * 
	 * @param selection
	 */
	public TTCN3NewFileWizardPage(ISelection selection) {
		super("wizardPage");
		setTitle("TTCN-3 File");
		setDescription("Create a new TTCN-3 file that can be opened by the TRex TTCN-3 editor.");
		this.selection = selection;
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;
	
		Label label = new Label(container, SWT.NULL);
		label.setText("&Project:");

		projectNameTextField = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		
		projectNameTextField.setLayoutData(gd);
		projectNameTextField.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		Button button = new Button(container, SWT.PUSH);
		button.setText("Browse...");
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleBrowse();
			}
		});
		label = new Label(container, SWT.NULL);
		label.setText("&Module name:");

		moduleNameTextField = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		moduleNameTextField.setLayoutData(gd);
		moduleNameTextField.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});
		
		createDefaultControlPartCheckBox = new Button(container, SWT.CHECK);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 3;
		createDefaultControlPartCheckBox.setLayoutData(gd);
		createDefaultControlPartCheckBox.setText("Create default control part");
		
		Composite container2 = new Composite(parent, SWT.NULL);
		GridLayout gl = new GridLayout();
		gl.numColumns = 2;
		gl.verticalSpacing = 9;
		container2.setLayout(gl);
		
		initialize();
		dialogChanged();
		setControl(container);
	}

	/**
	 * Tests if the current workbench selection is a suitable container to use.
	 */

	private void initialize() {
		if (selection != null && selection.isEmpty() == false
				&& selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			if (ssel.size() > 1)
				return;
			Object obj = ssel.getFirstElement();
			if (obj instanceof IResource) {
				IContainer container;
				if (obj instanceof IContainer)
					container = (IContainer) obj;
				else
					container = ((IResource) obj).getParent();
				projectNameTextField.setText(container.getFullPath().toString());
			}
		}
		moduleNameTextField.setFocus();
		moduleNameTextField.selectAll();
	}

	/**
	 * Uses the standard container selection dialog to choose the new value for
	 * the container field.
	 */

	void handleBrowse() {
		ContainerSelectionDialog dialog = new ContainerSelectionDialog(
				getShell(), ResourcesPlugin.getWorkspace().getRoot(), false,
				"Select new file container");
		if (dialog.open() == Window.OK) {
			Object[] result = dialog.getResult();
			if (result.length == 1) {
				projectNameTextField.setText(((Path) result[0]).toString());
			}
		}
	}

	/**
	 * Ensures that both text fields are set.
	 */

	void dialogChanged() {
		IResource project = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(new Path(getProjectName()));
		String moduleName = getModuleName();

		if (getProjectName().length() == 0) {
			updateStatus("Project must be specified");
			return;
		}
		if (project == null
				|| (project.getType() & (IResource.PROJECT | IResource.FOLDER)) == 0) {
			updateStatus("Project does not exist");
			return;
		}
		if (!project.isAccessible()) {
			updateStatus("Project must be writable");
			return;
		}
		if (moduleName.length() == 0) {
			updateStatus("Module name must be specified");
			return;
		}
		if (moduleName.replace('\\', '/').indexOf('/', 1) > 0) {
			updateStatus("Module name must be valid");
			return;
		}
	
		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

	/**
	 * @return the containing project's name
	 */
	public String getProjectName() {
		return projectNameTextField.getText();
	}

	/**
	 * @return the given module name
	 */
	public String getModuleName() {
		return moduleNameTextField.getText();
	}
	
	/**
	 * @return the given file name 
	 */
	public String getFileName() {
		return getModuleName() + ".ttcn3";
	}
	
	public boolean getDefaultControlPartFlag() {
		return createDefaultControlPartCheckBox.getSelection();
	}
}