package de.ugoe.cs.swe.trex.ui.views.metrics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.metrics.TTCN3Metrics;

public class TTCN3MetricsContentProvider implements ITreeContentProvider {
	
	final static String[] METRICS = { "Number of test cases",
			"Number of functions", "Number of altsteps", "Number of ports",
			"Number of components", "Number of types",
			"Number of templates", "Number of others",
			"Template Coupling Metric"};
	
	// ---------------------------------------------------------------------------
	
	public TTCN3MetricsContentProvider() {
		super();
	}
	
	// ---------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
//		calculated = false;
	}
	
	// ---------------------------------------------------------------------------
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	public void dispose() {
		// TODO
	}
	
	// ---------------------------------------------------------------------------
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	public Object[] getElements(Object parent) {
		// only show if at least one file selected
		if(TTCN3MetricsView.metricsFiles != null && TTCN3MetricsView.metricsFiles.size() > 0) {	
			// return string array of different metric types
			return TTCN3MetricsContentProvider.METRICS;
		}
		
		return new Object[0];
	}
	
	// ---------------------------------------------------------------------------
	
	/**
	 * Get a list of all the selected ttcn3 files for the given metric
	 * @param ifiles 
	 * @param parent
	 * @return
	 */
	public Object[] getFilenames(String parent) {
		List<IFile> mfiles = new ArrayList<IFile>(TTCN3MetricsView.metricsFiles.size());
		TTCN3Metrics mi;
		
		// loop for all files
		for(IFile file : TTCN3MetricsView.metricsFiles) {
			// get the TTCN3Metrics object assosciated with thie file
			mi = TTCN3Analyzer.FILE_TO_METRIC_SCORES.get(file);
			if(mi != null && mi.has(parent)) {
				mfiles.add(file);
				// remember the parent metric type for this file object
				TTCN3MetricsView.fileToMetricName.put(file, parent);
			}
		}
		return mfiles.toArray();			
	}
	
	// ---------------------------------------------------------------------------
    
	/**
	 * Get the AST objects for those involved in building the total for the given metric in the given file.
	 * @param mi the TTCN3Metrics object from which to get the list of ASTs
	 * @param metric the metric type to look up the objects for
	 * @param file the parent file object to associate with the ASTs
	 * @return
	 */
	private Object[] getASTs(TTCN3Metrics mi, String metric, IFile file) {
		AST ast;
		List<AST> list = mi.get(metric);
		
		if(list != null) {
			Iterator<AST> iter = list.iterator();
			while(iter.hasNext()) {
				ast = iter.next();
				TTCN3MetricsView.astFileMap.put(ast, file);
				// calculate and cache references now to avoid slowdown later
				// when expanding tree and also to show warnings now
				TTCN3MetricsLabelProvider.getReferences(ast);
			}
			return list.toArray();
		}
		return null;
	}

	// ---------------------------------------------------------------------------
	
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof String) {
			String str = (String)parentElement;
			
			if(TTCN3MetricsView.metricsFiles != null) {
				// list of all selected ttcn3 files below metric
				// FIXME: don't show if 0 metric information
				return getFilenames(str);
			}
		} else if (parentElement instanceof IFile) {
			// list of all findings of this metric type in this file
			String metric = TTCN3MetricsView.fileToMetricName.get(parentElement);
			TTCN3Metrics mi = TTCN3Analyzer.FILE_TO_METRIC_SCORES.get(parentElement);
			return getASTs(mi, metric, (IFile)parentElement);
		}
		
		return null;
    }
	
	// ---------------------------------------------------------------------------
	
    public Object getParent(Object element) {
    	if(element instanceof IFile) {
    		return TTCN3MetricsView.fileToMetricName.get(element);
    	} else if (element instanceof AST) {
    		// TODO: 
    	}
		return null;
    }
    
	// ---------------------------------------------------------------------------
    
    public boolean hasChildren(Object element) {
    	// FIXME: optimise with own method?
    	if (element instanceof String) {
    		final Object[] children = getChildren(element);
			if(children != null) {
    			return children.length > 0;
    		}
    	}
    	
    	if (element instanceof IFile) {
    		String metric = TTCN3MetricsView.fileToMetricName.get(element);
			TTCN3Metrics mi = TTCN3Analyzer.FILE_TO_METRIC_SCORES.get(element);
    		if(mi.get(metric) != null) {
    			return true;
    		}
    	}
    	
		return false;
    }
}
