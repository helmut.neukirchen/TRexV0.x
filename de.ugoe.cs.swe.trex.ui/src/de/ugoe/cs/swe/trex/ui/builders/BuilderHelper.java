package de.ugoe.cs.swe.trex.ui.builders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;

public class BuilderHelper {

	public static void addBuilderToProject(IProject project, String builderID) {
		if (!project.isOpen())
		{
			return;
		}

		IProjectDescription description;
		try {
			description = project.getDescription();
		} catch (CoreException e) {
			return;
		}
		ICommand[] cmds = description.getBuildSpec();
		for (int i = 0; i < cmds.length; i++) {
			if (cmds[i].getBuilderName().equals(builderID))
			{
				return;
			}
		}

		ICommand newCmd = description.newCommand();
		newCmd.setBuilderName(builderID);
		List<ICommand> newCmds = new ArrayList<ICommand>();

		newCmds.addAll(Arrays.asList(cmds));
		newCmds.add(newCmd);
		description.setBuildSpec(newCmds.toArray(new ICommand[newCmds.size()]));
		try {
			project.setDescription(description, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	// ---------------------------------------------------------------------------

	public static void removeBuilderFromProject(IProject project, String builderID) {
		if (!project.isOpen())
		{
			return;
		}

		IProjectDescription description;
		try {
			description = project.getDescription();
		} catch (CoreException e) {
			return;
		}

		int index = -1;
		ICommand[] cmds = description.getBuildSpec();
		for (int i = 0; i < cmds.length; i++) {
			if (cmds[i].getBuilderName().equals(builderID)) {
				index = i;
				break;
			}
		}

		if (index == -1)
		{
			return;
		}

		List<ICommand> newCmds = new ArrayList<ICommand>();
		newCmds.addAll(Arrays.asList(cmds));
		newCmds.remove(index);
		description.setBuildSpec(newCmds.toArray(new ICommand[newCmds.size()]));
		try {
			project.setDescription(description, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	// ---------------------------------------------------------------------------

	public static boolean isBuilderEnabled(IProject project, String builerID) {
		if (!project.isOpen())
		{
			return false;
		}

		IProjectDescription description;
		try {
			description = project.getDescription();
		} catch (CoreException e) {
			return false;
		}

		ICommand[] cmds = description.getBuildSpec();
		for (int i = 0; i < cmds.length; i++) {
			if (cmds[i].getBuilderName().equals(builerID)) {
				return true;
			}
		}

		return false;
	}
	


}
