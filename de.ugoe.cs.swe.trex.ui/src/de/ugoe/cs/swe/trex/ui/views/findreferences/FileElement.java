package de.ugoe.cs.swe.trex.ui.views.findreferences;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;

/**
 * A FileElement holds a IFile object and a list of TTCN3 Modules It is used by
 * FindReferences to store information about the results.
 * 
 */
public class FileElement {
	private IFile file = null;
	private List<ModuleElement> modules = new ArrayList<ModuleElement>();

	/**
	 * Constructs a new FileElement object.
	 */
	public FileElement(IFile file) {
		this.file = file;
	}

	public IFile getFile() {
		return file;
	}

	/**
	 * Adds a module to this object
	 * 
	 * @param module
	 *            the module element
	 */
	public void addModule(ModuleElement module) {
		modules.add(module);
	}

	public List<ModuleElement> getModules() {
		return modules;
	}
}
