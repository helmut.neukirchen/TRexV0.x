package de.ugoe.cs.swe.trex.ui.editors.text;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.swt.SWT;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.KeywordExtractor;
import de.ugoe.cs.swe.trex.ui.TTCN3ColorProvider;

/**
 * An TTCN-3 aware code scanner.
 */
public class TTCN3CodeScanner extends RuleBasedScanner {
	
	/** TTCN3 keywords we care about */
	public static final String[] TTCN3_KEYWORDS = KeywordExtractor.getInstance().getLiterals().toArray(new String[0]);
/*	public static final String[] TTCN3_KEYWORDS = new String[] { "action",
			"activate", "address", "alive", "all", "alt", "altstep", "and",
			"and4b", "any", "anytype", "bitstring", "boolean", "break", "case",
			"call", "catch", "char", "charstring", "check", "clear",
			"complement", "component", "connect", "const", "continue",
			"control", "create", "deactivate", "default", "disconnect",
			"display", "do", "done", "else", "encode", "enumerated", "error",
			"except", "exception", "execute", "extends", "extension",
			"external", "fail", "false", "float", "for", "friend", "from",
			"function", "getverdict", "getcall", "getreply", "goto", "group",
			"halt", "hexstring", "if", "ifpresent", "import", "in", "inconc",
			"infinity", "inout", "integer", "interleave", "kill", "killed",
			"label", "language", "length", "log", "map", "match", "message",
			"mixed", "mod", "modifies", "module", "modulepar", "mtc",
			"noblock", "none", "not", "not4b", "nowait", "null", "octetstring",
			"of", "omit", "on", "optional", "or", "or4b", "out", "override",
			"param", "pass", "pattern", "permutation", "port", "present",
			"private", "procedure", "public", "raise", "read", "receive",
			"record", "rem", "repeat", "reply", "recursive", "return",
			"running", "runs", "select", "self", "send", "sender", "set",
			"setverdict", "signature", "start", "stop", "subset", "superset",
			"system", "template", "testcase", "timeout", "timer", "to",
			"trigger", "true", "type", "union", "universal", "unmap", "value",
			"valueof", "var", "variant", "verdicttype", "while", "with", "xor",
			"xor4b", "objid" };
*/
	/**
	 * Create an instance of a code scanner for syntax highlighting
	 * 
	 * @param aColorProvider
	 *            The color mapping
	 */
	public TTCN3CodeScanner(TTCN3ColorProvider aColorProvider) {
		IToken keyword = new Token(new TextAttribute(aColorProvider
				.getColor("keyword"), null, SWT.BOLD));
		IToken other = new Token(new TextAttribute(aColorProvider
				.getColor("default")));
		List<IRule> rules = new ArrayList<IRule>();

		// Add generic whitespace rule
		rules.add(new WhitespaceRule(new TTCN3WhitespaceDetector()));

		// Add word rule for TTCN3 keywords

		WordRule wordRule = new WordRule(new TTCN3WordDetector(), other);
		for (String element : TTCN3CodeScanner.TTCN3_KEYWORDS) {
			wordRule.addWord(element, keyword);
		}

		rules.add(wordRule);

		IRule[] result = new IRule[rules.size()];
		rules.toArray(result);
		setRules(result);
	}
}
