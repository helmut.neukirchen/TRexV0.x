package de.ugoe.cs.swe.trex.ui.preferences.pages;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.osgi.service.prefs.BackingStoreException;

import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;
import de.ugoe.cs.swe.trex.ui.preferences.PreferenceConstants;

public class TTCN3FormatterPreferencePage extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {

	public TTCN3FormatterPreferencePage() {
		super(FieldEditorPreferencePage.GRID);
		setPreferenceStore(TTCN3UIPlugin.getDefault().getPreferenceStore());
		setDescription("TTCN-3 Formatter Preferences");
	}

	@Override
	public void createFieldEditors() {
		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_TABS,
				"Use &tabs instead of spaces", getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_UNIX_NEWLINE,
				"Use &Unix-style newlines", getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_SPACES_BETWEEN_ASSIGNMENT,
				"Space before and after &assignments", getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_SPACE_AFTER_COMMA,
				"Space after &comma", getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_KR_STYLE,
				"&K&&R line breaks", getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_NEWLINE_BEFORE_RUNS_ON,
				"New line before &runs on clauses", getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_NEWLINE_BEFORE_SYSTEM,
				"New line before &system clauses", getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_NEWLINE_BEFORE_RETURN,
				"New line before r&eturn clauses", getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_NEWLINE_BEFORE_EXCEPTION_SPEC,
				"New line before e&xception clauses ", getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_NEWLINE_BEFORE_FORMAL_PAR,
				"New line before &formal parameters, unless single", getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_NEWLINE_AFTER_FORMAL_PAR_LIST,
				"New line after formal par &list", getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_SPACES_AROUND_PARENTHESES,
				"Spaces around &parentheses", getFieldEditorParent()));

		
		addField(new IntegerFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_SPACES_COUNT,
				"Tab &width", getFieldEditorParent()));

		addField(new IntegerFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_LINES_AFTER_MODULE_DEFINITION,
				"Lines after top-level &declarations", getFieldEditorParent()));

		addField(new IntegerFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_LINES_BETWEEN_MODULES,
				"Lines between multiple &modules in one file",
				getFieldEditorParent()));

		addField(new IntegerFieldEditor(
				PreferenceConstants.P_TTCN3_FORMATTER_LINES_AFTER_CONTROL_PART,
				"Lines after c&ontrol part", getFieldEditorParent()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
		// do nothing
	}

	@Override
	public boolean performOk() {
		boolean value = super.performOk();
		try {
			 InstanceScope.INSTANCE.getNode(TTCN3UIPlugin.getUniqueIdentifier()).flush();
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block. What would be a good catch ?
			e.printStackTrace();
		}
		return value;
	}

}