package de.ugoe.cs.swe.trex.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public class TTCN3ResourceChangeListener implements IResourceChangeListener {

	public void resourceChanged(IResourceChangeEvent event) {
		IResourceDelta delta = event.getDelta();
		try {
			delta.accept(new IResourceDeltaVisitor() {

				public boolean visit(IResourceDelta delta) {
					if (delta.getKind() == IResourceDelta.REMOVED) {
					
						String lastSegment = delta.getFullPath().segment(delta.getFullPath().segmentCount() - 1);
						String pathNoLastSegment = delta.getFullPath().toPortableString().substring(0, delta.getFullPath().toPortableString().length() - lastSegment.length());

						if (lastSegment.equals(".project")) {
							final List<IEditorPart> editors = findEditorStartsWith(pathNoLastSegment, PlatformUI.getWorkbench());
							for (int i=0; i < editors.size(); i++) {
								final int index = i;
								PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
									public void run() {
										IEditorSite editorSite = editors.get(index).getEditorSite();
										editorSite.getPage().closeEditor(editors.get(index), false);
									}
									
								});
								
							}
						} else {
							final IEditorPart editor = findEditorFor(delta.getFullPath().toPortableString(), PlatformUI.getWorkbench());
							if (editor != null) {
								PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
									public void run() {
										editor.getEditorSite().getPage().closeEditor(editor, false);
									}
								});
							}
						}
					}

					return true;
				}

			});
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	// ---------------------------------------------------------------------------------------------------------
	
	public IEditorPart findEditorFor(String portablePath, IWorkbench workbench) {

		IWorkbenchWindow[] windows = workbench.getWorkbenchWindows();

		for (IWorkbenchWindow element : windows) {
			IWorkbenchPage[] pages = element.getPages();
			for (IWorkbenchPage element0 : pages) {

				IEditorReference[] editors = element0.getEditorReferences();

				for (IEditorReference ref : editors) {

					if (ref == null)
						continue;
					
					IEditorPart editor = ref.getEditor(true);

					if (editor == null) {
						continue;
					}

					IEditorInput input = editor.getEditorInput();
					IFile editorFile = (IFile) input.getAdapter(IFile.class);

					if (editorFile != null && editorFile.getFullPath().toPortableString().equals(portablePath)) {
						return editor;
					}

				}
			}
		}
		return null;
	}

	// ---------------------------------------------------------------------------------------------------------
	
	public List<IEditorPart> findEditorStartsWith(String portablePath, IWorkbench workbench) {
		List<IEditorPart> result = new ArrayList<IEditorPart>();
				
		IWorkbenchWindow[] windows = workbench.getWorkbenchWindows();

		for (IWorkbenchWindow element : windows) {
			IWorkbenchPage[] pages = element.getPages();
			for (IWorkbenchPage element0 : pages) {

				IEditorReference[] editors = element0.getEditorReferences();

				for (IEditorReference ref : editors) {

					IEditorPart editor = ref.getEditor(true);

					if (editor == null) {
						continue;
					}

					IEditorInput input = editor.getEditorInput();
					IFile editorFile = (IFile) input.getAdapter(IFile.class);

					if (editorFile != null && editorFile.getFullPath().toPortableString().startsWith(portablePath)) {
						result.add(editor);
					}

				}
			}
		}
		
		return result;
	}
}
