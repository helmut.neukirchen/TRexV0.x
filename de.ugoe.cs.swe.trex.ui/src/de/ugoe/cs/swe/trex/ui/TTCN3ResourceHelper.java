package de.ugoe.cs.swe.trex.ui;

import org.eclipse.core.resources.IResource;

/**
 * A class with helper methods for TTCN3 files and resources
 * 
 * @author neukirchen
 */
public class TTCN3ResourceHelper {

	/**
	 * Returns whether the resource is a file with a valid TTCN-3 file
	 * extension.
	 * 
	 * @param resource
	 *            An IResource
	 * @return true if ressource extension indicates a TTCN-3 file
	 */
	public static boolean isTTCN3File(IResource resource) {
		if (resource != null && (resource.getType() == IResource.FILE)) {
			return isTTCN3Extension(resource.getFileExtension());
		} else {
			return false;
		}
	}

	/**
	 * Returns whether extension is a valid TTCN-3 file extension.
	 * 
	 * @param extension
	 * @return true if extension is a valid TTCN-3 extension
	 */
	public static boolean isTTCN3Extension(String extension) {
		if (extension != null
				&& (extension.equals("ttcn") || extension.equals("ttcn3") || extension
						.equals("3mp"))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get a human readable string representation of valid TTCN-3 extensions
	 * 
	 * @return List of valid TTCN-3 extensions seperated by "or"
	 */
	public static String getTTCN3ExtensionsAsString() {
		return "\"ttcn\", \"ttcn3\", or \"3mp\"";
	}
}
