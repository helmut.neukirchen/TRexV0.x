package de.ugoe.cs.swe.trex.ui.handlers;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.UIElement;
import org.osgi.service.prefs.BackingStoreException;

import de.ugoe.cs.swe.trex.ui.actionsets.CompilerModeList;

/**
 * Handler for the Compiler Mode menu
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class SelectCompilerMode extends AbstractHandler implements IElementUpdater {

	/**
	 * Click on the menu item
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// Get the compiler mode that was clicked (as a command parameter)
		String compilerMode = event.getParameter("de.ugoe.cs.swe.trex.ui.commands.SelectCompilerModeParameter");
		// Get the compiler modes
		final Map<String, Integer> modes = CompilerModeList.getModes();
		// If no modes, return
		if (modes == null) {
			return null;
		}
		// Changes the preference
		IEclipsePreferences prefs = InstanceScope.INSTANCE.getNode("de.ugoe.cs.swe.trex.core");
		prefs.putInt("compilerMode", modes.get(compilerMode));
		try{
			prefs.flush();
		} catch(BackingStoreException bse){
			prefs.putInt("compilerMode", 0); // if prefs can not be saved, revert to default value
		}
		return null;
	}
	
	/**
	 * Set the menu element checked or not
	 */
	public void updateElement(UIElement element, @SuppressWarnings("rawtypes") Map parameters) {
		// Get the menu item
		String compilerMode = (String) parameters.get("de.ugoe.cs.swe.trex.ui.commands.SelectCompilerModeParameter");
		// Get the compiler modes
		final Map<String, Integer> modes = CompilerModeList.getModes();
		// If no modes, return
		if (modes == null) {
			return;
		}
		// Get the active compiler mode
		int compilerModePreference = Platform.getPreferencesService().getInt("de.ugoe.cs.swe.trex.core",
				"compilerMode",0,null);
		// Set the checked state
		if (modes.get(compilerMode) == compilerModePreference) {
			element.setChecked(true);
		} else {
			element.setChecked(false);
		}
	}
	
}
