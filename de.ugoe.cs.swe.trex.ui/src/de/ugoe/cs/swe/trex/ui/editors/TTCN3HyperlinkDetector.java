package de.ugoe.cs.swe.trex.ui.editors;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.hyperlink.IHyperlink;
import org.eclipse.jface.text.hyperlink.IHyperlinkDetector;

public class TTCN3HyperlinkDetector implements IHyperlinkDetector {

	public IHyperlink[] detectHyperlinks(ITextViewer textViewer, IRegion region, 
		    boolean canShowMultipleHyperlinks) {

		    IRegion lineInfo;
		    String line;
		    Integer offset = 0;

		    try {
		        lineInfo= ((org.eclipse.jface.text.IDocument) region).getLineInformationOfOffset(offset);
		            line= ((org.eclipse.jface.text.IDocument) region).get(lineInfo.getOffset(), lineInfo.getLength());
		        } catch (BadLocationException ex) {
		            return null;
		        }

		        int begin= line.indexOf("<");
		        int end = line.indexOf(">");

		        if(end<0 || begin<0 || end==begin+1)
		        {
		        	return null;
		        }

		        String text = line.substring(begin+1,end+1);
		        IRegion newRegion = new Region(lineInfo.getOffset()+begin+1,text.length());

		        return new IHyperlink[] {new TTCN3Hyperlink(newRegion, text)};
		    }
}
