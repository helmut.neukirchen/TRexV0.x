package de.ugoe.cs.swe.trex.ui;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;
import org.eclipse.jface.viewers.LabelProvider;

public class DisabledFolderDecorator extends LabelProvider implements
		ILightweightLabelDecorator {

	public void decorate(Object element, IDecoration decoration) {
		if (element instanceof IFolder) {
			IFolder folder = (IFolder) element;
			try {
				String folderDisabledPropertyString = folder.getPersistentProperty(new QualifiedName("de.ugoe.cs.swe.trex.ui","folderDisabled"));
				boolean folderDisabledProperty = Boolean.valueOf(folderDisabledPropertyString);
				if (folder.getProject().hasNature("de.ugoe.cs.swe.trex.ui.trexnature")) {
					folder.setPersistentProperty(new QualifiedName("de.ugoe.cs.swe.trex.ui","trexnature"), "true");
					
					if (!folderDisabledProperty)
						decoration.addOverlay(TTCN3UIPluginImages.DESC_TTCN3);
				}
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
	}

}
