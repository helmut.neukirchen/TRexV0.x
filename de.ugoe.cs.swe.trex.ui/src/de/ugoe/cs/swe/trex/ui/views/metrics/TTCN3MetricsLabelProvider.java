package de.ugoe.cs.swe.trex.ui.views.metrics;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.ASTUtil;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3Analyzer;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;
import de.ugoe.cs.swe.trex.core.metrics.TTCN3Metrics;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPluginImages;

public class TTCN3MetricsLabelProvider extends LabelProvider implements ITableLabelProvider {
	
	// ---------------------------------------------------------------------------
	
	public TTCN3MetricsLabelProvider() {
		super();
	}
	
	// ---------------------------------------------------------------------------
		
	/**
	 * Retrieves an entry for the <code>Value</code> column in the Metrics
	 * view. This will either be a total of all files (for a Metric title row)
	 * or a value for a single <code>IFile</code>.
	 * 
	 * @param obj the 
	 * @return
	 */
	public double getMetricValue(Object obj) {
		TTCN3Metrics mi;
		
		if(obj instanceof String) {			
				// return total for particular metric type
				double total = 0;
				boolean isValueC = "Template Coupling Metric".equals(obj);
				
				for(IFile file : TTCN3MetricsView.metricsFiles) {
					mi = TTCN3Analyzer.FILE_TO_METRIC_SCORES.get(file);
					if (mi != null) {
						if(isValueC) {
							total += mi.getValueCoupling();	
						} else {
							total += mi.getNum(obj.toString());
						}
					}
				}
				return isValueC ? total : Math.round(total);
		} else if(obj instanceof IFile) {
			IFile file = (IFile)obj;
				// look up the value for that metric for this file (obj)
				String metricName = TTCN3MetricsView.fileToMetricName.get(file);
				mi = TTCN3Analyzer.FILE_TO_METRIC_SCORES.get(file);
				
				if(metricName.equals("Template Coupling Metric")) {
					// retrieve the pre-calculated ValueCoupling score for this file
					return +mi.getValueCoupling();
				}
				
				return +mi.getNum(metricName);
		} else if(obj instanceof AST) {
			// AST objects only have a ValueCoupling score, use the static
			// method in TTCN3Metrics to calculate it
			int score = TTCN3Metrics.getValueCouplingScore((AST)obj);
			
			return score > 0 ? score : -1;
		}
		
		return -1;
	}
	
	// ---------------------------------------------------------------------------
	
	@Override
	public String getText(Object element) {
		if(element == null) {
			return "";
		}
		
		if(element instanceof IFile) {
			return ((IFile)element).getName();
		}
		
		if(element instanceof AST) {
			return ASTUtil.getDescendantText((AST)element);
		}
		
		return element.toString();
	}
	
	// ---------------------------------------------------------------------------
	
	public String getColumnText(Object obj, int index) {
		String ret = "";
		int iTemp;
		double dTemp;
		
		switch (index) {
		case 0:
			ret = getText(obj);
			break;
		case 1:
			dTemp = getMetricValue(obj);
			
			if(dTemp > 0) {
				ret = "" + dTemp;
				// no point showing x.0
				if (ret.endsWith(".0")) {
					ret = "" + Math.round(dTemp);
				}
			}
			
			break;
		case 2:
			iTemp = getReferences(obj);
			
			// if template then return exact value to print (i.e. allow 0)
			if(iTemp > 0 || obj instanceof AST && ((AST)obj).getType() == TTCN3ParserTokenTypes.TemplateDef) {
				ret = ""+iTemp;
			}
			
			break;
		default:
			break;
		}
		return ret;
	}

	// ---------------------------------------------------------------------------
	
	/**
	 * Get the number of references to a given <code>LocationAST</code> node.
	 * @param obj the <code>LocationAST</code> node to find the number of references to.
	 * @return the number of references to the given node. 
	 */
	public static int getReferences(Object obj) {
		if(obj instanceof LocationAST) {
			IFile file = TTCN3MetricsView.astFileMap.get(obj);
			TTCN3Metrics mi = file == null ? null : TTCN3Analyzer.FILE_TO_METRIC_SCORES.get(file);
			return mi == null ? 0 : mi.getReferences((LocationAST)obj);
		}
		
		return 0;
	}
	
	// ---------------------------------------------------------------------------
	
	public Image getColumnImage(Object obj, int index) {
		return index == 0 ? getImage(obj) : null;
	}
	
	// ---------------------------------------------------------------------------
	
	public Image getImage(ImageDescriptor desc) {
		return TTCN3UIPluginImages.imageCacheGet(desc);
	}
	
	// ---------------------------------------------------------------------------
	
	@Override
	public Image getImage(Object obj) {
		Image def = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
		String txt = getText(obj);
		
		if(obj instanceof AST) {
			return null;
		} 
		
		if(obj instanceof IFile) {
			return getImage(TTCN3UIPluginImages.DESC_TTCN3);
		}
		
		if ("Template Coupling Metric".equals(txt)) {
			return getImage(TTCN3UIPluginImages.DESC_VALUE);
		} 
		
		if(txt.startsWith("Number of")) {
			String metricType = txt.substring(10);
			if ("test cases".equals(metricType)) {
				return getImage(TTCN3UIPluginImages.DESC_TESTCASE);
			} else if ("altsteps".equals(metricType)) {
				return getImage(TTCN3UIPluginImages.DESC_ALTSTEP);
			} else if ("functions".equals(metricType)) {
				return getImage(TTCN3UIPluginImages.DESC_FUNCTION);
			} else if ("templates".equals(metricType)) {
				return getImage(TTCN3UIPluginImages.DESC_TEMPLATE);
			} else if ("ports".equals(metricType)) {
				return getImage(TTCN3UIPluginImages.DESC_PORT);
			} else if ("components".equals(metricType)) {
				return getImage(TTCN3UIPluginImages.DESC_COMPONENT);
			} else if ("types".equals(metricType)) {
				return getImage(TTCN3UIPluginImages.DESC_TYPE);
			} else if ("others".equals(metricType)) {
				return getImage(TTCN3UIPluginImages.DESC_OTHER);
			}
		}	
		
		return def;
	}
	
}
