package de.ugoe.cs.swe.trex.ui.views.findreferences;

import java.util.ArrayList;
import java.util.List;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;

/**
 * A CompoundElement holds information about the compound node and its child
 * atomic elements. It is used by FindReferences to store information about the
 * results.
 * 
 */
public class CompoundElement {
	private ModuleElement moduleElement = null;
	private LocationAST node = null;
	private List<AtomicElement> atomics = new ArrayList<AtomicElement>();

	/**
	 * Constructs a new CompoundElement object.
	 */
	public CompoundElement(ModuleElement moduleElement, LocationAST node) {
		moduleElement.addCompound(this);
		this.moduleElement = moduleElement;
		this.node = node;
	}

	/**
	 * Adds an atomic to this object
	 * 
	 * @param atomic
	 *            the atomic element
	 */
	public void addAtomic(AtomicElement atomic) {
		atomics.add(atomic);
	}

	public List<AtomicElement> getAtomics() {
		return atomics;
	}

	public ModuleElement getModuleElement() {
		return moduleElement;
	}

	public LocationAST getNode() {
		return node;
	}
}
