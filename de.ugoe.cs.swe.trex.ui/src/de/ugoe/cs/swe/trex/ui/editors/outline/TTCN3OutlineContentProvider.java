package de.ugoe.cs.swe.trex.ui.editors.outline;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.TTCN3ParserTokenTypes;

/**
 * Supplies a model for an outline view
 */
public class TTCN3OutlineContentProvider implements ITreeContentProvider {

	/** {@inheritDoc} */
	public void inputChanged(Viewer aViewer, Object anOldInput, Object aNewInput) {
		// nothing to do here
	}

	/** {@inheritDoc} */
	public void dispose() {
		// nothing to do here
	}

	/** {@inheritDoc} */
	public Object[] getElements(Object inputElement) {
		final Object[] children = getChildren(inputElement);
		return children;
	}

	/** {@inheritDoc} */
	public Object[] getChildren(Object anElement) {
		final List<AST> children = getChildrenVector(anElement);
		return children.toArray();
	}

	private List<AST> getChildrenVector(Object anElement) {
		List<AST> theChildren = new ArrayList<AST>();

		if (anElement instanceof AST) {
			AST aChild = ((AST) anElement).getFirstChild();
			while (aChild != null) {
				final int type = aChild.getType();			
				final AST bChild = aChild.getFirstChild();
				
				if(bChild == null) {
					aChild = aChild.getNextSibling();
					continue;
				}
				
				if (type == TTCN3ParserTokenTypes.TTCN3File
						|| type == TTCN3ParserTokenTypes.TTCN3Module) {
					theChildren.add(aChild);
				} else if (type == TTCN3ParserTokenTypes.ModuleControlPart
						|| type == TTCN3ParserTokenTypes.ModuleDefinitionsPart) {
					theChildren.add(bChild);
				} else if (type == TTCN3ParserTokenTypes.ModuleControlBody
						|| type == TTCN3ParserTokenTypes.ModuleDefinitionList
				        || type == TTCN3ParserTokenTypes.ModuleDefinition) {
					theChildren.add(aChild);
				} else if (type == TTCN3ParserTokenTypes.GroupDef) {
			    	final AST modDefList = aChild.getFirstChild().getNextSibling().getFirstChild();
					aChild = modDefList.getFirstChild();
					continue;
				} else if (type == TTCN3ParserTokenTypes.TemplateDef) {
					// add fields of template as children
					final AST templateBody = bChild.getNextSibling();
					final AST fieldSpecList = templateBody == null ? null : templateBody.getFirstChild();
					AST fieldSpec = fieldSpecList == null ? null : fieldSpecList.getFirstChild();
					while(fieldSpec != null) {
						theChildren.add(fieldSpec);
						fieldSpec = fieldSpec.getNextSibling();
					}
				} else if (type == TTCN3ParserTokenTypes.FieldReference) {
					// check for nested fields
					AST templateBody = aChild.getNextSibling();
					AST fieldSpecList = templateBody.getFirstChild();
					if(fieldSpecList.getType() == TTCN3ParserTokenTypes.FieldSpecList) {
						AST fieldSpec = fieldSpecList.getFirstChild();
						while(fieldSpec != null) {
							theChildren.add(fieldSpec);
							fieldSpec = fieldSpec.getNextSibling();
						}
					}
				}

				aChild = aChild.getNextSibling();
			}
		}
		return theChildren;
	}

	/** {@inheritDoc} */
	public Object getParent(Object anElement) {
		if(anElement instanceof LocationAST) {
			return ((LocationAST)anElement).getParent();
		}
		
		return null;
	}

	/** {@inheritDoc} */
	public boolean hasChildren(Object anElement) {
		if (anElement instanceof LocationAST) {
			final AST theElement = (AST) anElement;
			final int type = theElement.getType();

			AST aChild = theElement.getFirstChild();
			if(aChild == null) {
				return false;
			}

			if (type == TTCN3ParserTokenTypes.TTCN3File
					|| type == TTCN3ParserTokenTypes.TTCN3Module
					|| type == TTCN3ParserTokenTypes.ModuleDefinitionsPart
					|| type == TTCN3ParserTokenTypes.ModuleControlPart
					|| type == TTCN3ParserTokenTypes.ModuleControlBody
					|| type == TTCN3ParserTokenTypes.ModuleDefinitionList) {
				return true;
			} else if (type == TTCN3ParserTokenTypes.ModuleDefinition) {
			    if (aChild.getType() == TTCN3ParserTokenTypes.GroupDef) {
			    	final AST modDefPart = aChild.getFirstChild().getNextSibling();
			    	final AST modDefList = modDefPart == null ? null : modDefPart.getFirstChild();
			    	return modDefList != null;
			    } else if (aChild.getType() == TTCN3ParserTokenTypes.TemplateDef) {
					// fields of template are children
					final AST bChild = aChild.getFirstChild();
					final AST templateBody = bChild == null ? null : bChild.getNextSibling();
					final AST fieldSpecList = templateBody == null ? null : templateBody.getFirstChild();
					final AST fieldSpec = fieldSpecList == null ? null : fieldSpecList.getFirstChild();
					return fieldSpec != null;
			    }
			} else if (type == TTCN3ParserTokenTypes.FieldSpec) {
				// check for nested fields
				AST templateBody = aChild.getNextSibling();
				AST fieldSpecList = templateBody == null ? null : templateBody.getFirstChild();
				if(fieldSpecList == null)
					return false;
					
				return (fieldSpecList.getType() == TTCN3ParserTokenTypes.FieldSpecList);
			}
		}
		return false;
	}
}
