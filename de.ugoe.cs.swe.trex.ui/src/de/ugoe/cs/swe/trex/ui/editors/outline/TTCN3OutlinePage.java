package de.ugoe.cs.swe.trex.ui.editors.outline;


import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.views.contentoutline.ContentOutlinePage;

import antlr.collections.AST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPluginImages;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;
import de.ugoe.cs.swe.trex.ui.preferences.PreferenceConstants;

/**
 * @author Jochen Kemnade
 */
public class TTCN3OutlinePage extends ContentOutlinePage {
	TreeViewer viewer;	
	
	protected TTCN3Editor fEditor;

	private TTCN3OutlineLabelProvider fLabelProvider;
	
	private int start;

	private boolean fIsDisposed;
	
	/**
	 * Creates a new TTCN3OutlinePage.
	 * 
	 * @param anEditor
	 */
	public TTCN3OutlinePage(TTCN3Editor anEditor) {
		super();
		this.fEditor = anEditor;
	}

	/**
	 * Creates the control and registers the popup menu for this page Menu id
	 * "org.eclipse.ui.examples.readmetool.outline"
	 * 
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		super.createControl(parent);
		fLabelProvider = new TTCN3OutlineLabelProvider();
		viewer = getTreeViewer();
		viewer.setContentProvider(new TTCN3OutlineContentProvider());
		viewer.setLabelProvider(fLabelProvider);
		viewer.setInput(getContentOutline());
		viewer.addSelectionChangedListener(this);
		viewer.expandToLevel(TreeViewer.ALL_LEVELS);
		createSortToggle();
		createExpandTree();
		createCollapseTree();
		start = 0;
		// drag support	--------------
		int dndOps = DND.DROP_COPY;
		Transfer[] transfers = new Transfer[] {TextTransfer.getInstance()};
		DragSourceListener dndListener = new DragSourceListener(){
			public void dragStart(DragSourceEvent dse){}
			public void dragSetData(DragSourceEvent dse){
				if (TextTransfer.getInstance().isSupportedType(dse.dataType)){
					// get items selected in outline
					TreeItem[] selection = viewer.getTree().getSelection();
					// we want to drop only the identifier (i.e. the part before the ':')
					// of the first item selected. no matter how many actually are selected
					dse.data = selection[0].getText().split(" : ")[0];
				}
			}
			public void dragFinished(DragSourceEvent dse){}
			
		};
		viewer.addDragSupport(dndOps, transfers, dndListener);	
		// ----------------------------
	}

	// Check if tree outline view is enabled (TTC-312)
	private void treeOutlineView() {
		boolean isTreeOutlineViewExpandedByDefault = Platform.getPreferencesService().getBoolean("de.ugoe.cs.swe.trex.ui",PreferenceConstants.P_TTCN3_OUTLINE_TREE_VIEW_EXPANDED_BY_DEFAULT,false,null);		
		if (isTreeOutlineViewExpandedByDefault == true)
		{
				getTreeViewer().expandToLevel(TreeViewer.ALL_LEVELS);
				//Commented out for TTC-321. This was done so the outline tree takes the chosen default starting position when updated.
				//start = 1;
		}
	}
	
	/**
	 * Expands the outline view
	 * 
	 */
	private void createExpandTree()
	{
		IActionBars bars = getSite().getActionBars();
		
		Action expandAction = new Action("Expand All") { 
			@Override
			public void run() { 
				getTreeViewer().expandToLevel(TreeViewer.ALL_LEVELS);
				}
		 };

		TTCN3UIPluginImages.setLocalImageDescriptors(expandAction, "expandall.gif");
		bars.getToolBarManager().add(expandAction);
	}
	
	/**
	 * Collapse the outline view
	 * 
	 */
	private void createCollapseTree()
	{
		IActionBars bars = getSite().getActionBars();
		
		Action collapseAction = new Action("Collapse All") { 
			@Override
			public void run() { 
				getTreeViewer().collapseAll();
				}
		 };

		TTCN3UIPluginImages.setLocalImageDescriptors(collapseAction, "collapseall2.gif");
		bars.getToolBarManager().add(collapseAction);
	}
	
	private void createSortToggle() {
		IActionBars bars = getSite().getActionBars();
		Action sortToggler = new Action("Sort") {
			@Override
			public void run() {
				ViewerSorter sort = viewer.getSorter();
				if(sort == null) {
					setChecked(true);
					viewer.setSorter(new ViewerSorter());
				} else {
					setChecked(false);
					viewer.setSorter(null);
				}
				viewer.refresh();
			}
		};
		sortToggler.setChecked(false);
		TTCN3UIPluginImages.setLocalImageDescriptors(sortToggler, "alphab_sort_co.gif");
		bars.getToolBarManager().add(sortToggler);
	}	

	/**
	 * Gets the content outline . Returns the root node or null if the outline
	 * could not be generated.
	 * 
	 * @return the root element of the document's parse tree
	 */
	private AST getContentOutline() {
		return fEditor.getReconcilingStrategy().getRoot();
	}

	/**
	 * Forces the page to update its contents.
	 * 
	 */
	public void update() {
		getControl().setRedraw(false);
		getTreeViewer().setInput(getContentOutline());
		getControl().setRedraw(true);
		if (start==0)
		{
			treeOutlineView();
		}
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		super.selectionChanged(event);

		ISelection selection = event.getSelection();

		if (selection.isEmpty())
		{
			fEditor.resetHighlightRange();
		}
		else {
			LocationAST position = (LocationAST) ((IStructuredSelection) selection)
					.getFirstElement();
			
			int start = position.getOffset();
			int length = position.getEndOffset() - position.getOffset();
			length = (length > 0) ? length : 0;

			try {
				fEditor.setHighlightRange(start, length, true);
			} catch (IllegalArgumentException x) {
				fEditor.resetHighlightRange();
			}
		}

	}
	
	/** {@inheritDoc} */
	@Override
	public void dispose() {
		if (fLabelProvider != null) {
			fLabelProvider.dispose();
			fLabelProvider = null;
		}
		fIsDisposed = true;
		super.dispose();
	}

	/**
	 * Have we disposed the outline yet?
	 * 
	 * @return true if disposed
	 */
	public boolean isDisposed() {
		return fIsDisposed;
	}
}
