package de.ugoe.cs.swe.trex.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3MultiPageEditor;
import de.ugoe.cs.swe.trex.ui.views.findreferences.TTCN3FindReferencesSearchQuery;

public class TTCN3FindReferencesHandler implements IEditorActionDelegate {

	TTCN3Editor editor;
	TTCN3MultiPageEditor multipageEditor;

	// ---------------------------------------------------------------------------

	public TTCN3FindReferencesHandler() {
		super();
	}

	// ---------------------------------------------------------------------------

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		if (targetEditor instanceof TTCN3Editor) {
			editor = (TTCN3Editor) targetEditor;
		} else if (targetEditor instanceof TTCN3MultiPageEditor) {
			multipageEditor = (TTCN3MultiPageEditor) targetEditor;
			editor = multipageEditor.getFEditor();
		}
	}

	// ---------------------------------------------------------------------------

	public void run(IAction action) 
	{
		if (editor == null) {
			Shell shell = new Shell();
			MessageDialog.openInformation(shell, "Error while executing Find References",
					"No Editor initialized. Something is wrong...");
			return;
		}

		Integer offset = editor.getCaretOffset();

		if (offset == null)
			return;
		
		LocationAST ast = editor.getReconcilingStrategy().getIdentifierCache().get(offset);
		
		//TTCN3FindReferencesQuery query = new TTCN3FindReferencesQuery(ast);
		//NewSearchUI.runQueryInForeground(null, query);
		
		TTCN3FindReferencesSearchQuery query = new TTCN3FindReferencesSearchQuery(ast);
		
		if (query.getQueryNode() != null) {
			NewSearchUI.runQueryInForeground(null, query);
		}
	}
	
	// ---------------------------------------------------------------------------

	public void selectionChanged(IAction action, ISelection selection) {
		// do nothing
	}

}
