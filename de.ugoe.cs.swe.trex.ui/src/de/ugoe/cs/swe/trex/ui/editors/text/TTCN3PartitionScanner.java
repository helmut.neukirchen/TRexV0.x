package de.ugoe.cs.swe.trex.ui.editors.text;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.PatternRule;
import org.eclipse.jface.text.rules.RuleBasedPartitionScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;

/**
 * This scanner recognizes the TTCN3 comments.
 */
public class TTCN3PartitionScanner extends RuleBasedPartitionScanner {
	//START modified by sukiyuki 2011-2-6
	//The identifier of the TTCN3 partitioning.
	public final static String TTCN3_PARTITIONING= "___ttcn3_partitioning";
	//END   modified by sukiyuki 2011-2-6
	/** string type */
	public final static String STRING = "__ttcn3_string";

	/** single-line comment type */
	public final static String SINGLE_LINE_COMMENT = "__ttcn3_single_line_comment";

	/** multi-line comment type */
	public final static String MULTI_LINE_COMMENT = "__ttcn3_multi_line_comment";

	/**
	 * types of partitions in an TTCN3 file
	 */
	public static final String[] PARTITION_TYPES = new String[] {
			IDocument.DEFAULT_CONTENT_TYPE, TTCN3PartitionScanner.STRING, TTCN3PartitionScanner.SINGLE_LINE_COMMENT,
			TTCN3PartitionScanner.MULTI_LINE_COMMENT, };


	/**
	 * Creates the partitioner and sets up the appropriate rules.
	 */
	public TTCN3PartitionScanner() {
		IToken string = new Token(TTCN3PartitionScanner.STRING);
		IToken singleLineComment = new Token(TTCN3PartitionScanner.SINGLE_LINE_COMMENT);
		IToken multiLineComment = new Token(TTCN3PartitionScanner.MULTI_LINE_COMMENT);

		List<PatternRule> rules = new ArrayList<PatternRule>();

		// Add rule for single line comments
		rules.add(new EndOfLineRule("//", singleLineComment));

		// Add rule for strings and character constants.
		rules.add(new SingleLineRule("\"", "\"", string, '\\'));
		rules.add(new SingleLineRule("'", "'", string, '\\'));

		// Add special empty comment word rule

		// Add rules for multi-line comments
		rules.add(new MultiLineRule("/*", "*/", multiLineComment, '\0', true));


		IPredicateRule[] result = new IPredicateRule[rules.size()];
		rules.toArray(result);
		setPredicateRules(result);
	}
}
