package de.ugoe.cs.swe.trex.ui.editors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.INavigationLocation;
import org.eclipse.ui.INavigationLocationProvider;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.editors.text.ILocationProvider;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.ide.IGotoMarker;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;


/**
 * @author Jochen Kemnade
 * 
 */
public class TTCN3MultiPageEditor extends MultiPageEditorPart implements
		INavigationLocationProvider, IGotoMarker {
	private static final String PREFIX = "Editor.page.";

	private static final int PAGE_EDITOR = 0;

	private TTCN3Editor fEditor;

	// ---------------------------------------------------------------------------

	public INavigationLocation createEmptyNavigationLocation() {
		return new TTCN3NavigationLocation(fEditor, false);
	}

	// ---------------------------------------------------------------------------

	public INavigationLocation createNavigationLocation() {
		return new TTCN3NavigationLocation(fEditor, true);
	}

	// ---------------------------------------------------------------------------

	public void activateEditor() {
		if (getActivePage() != TTCN3MultiPageEditor.PAGE_EDITOR) {
			setActivePage(TTCN3MultiPageEditor.PAGE_EDITOR);
		}
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void createPages() {
		try {
			fEditor = new TTCN3Editor(this);
			if (getEditorInput() != null) {
				addPage(fEditor, getEditorInput());
				setPageText(TTCN3MultiPageEditor.PAGE_EDITOR, TTCN3UIPlugin
					.getMessage(TTCN3MultiPageEditor.PREFIX + "Source"));
			}
		} catch (PartInitException e) {
			ErrorDialog.openError(getSite().getShell(), TTCN3UIPlugin
					.getMessage("ErrorCreatingNestedEditor"), null, e
					.getStatus());
		}
	}

	// ---------------------------------------------------------------------------

	@Override
	public void doSave(IProgressMonitor monitor) {
		getEditor(TTCN3MultiPageEditor.PAGE_EDITOR).doSave(monitor);
	}

	// ---------------------------------------------------------------------------

	@Override
	public void doSaveAs() {
		IEditorPart editor = getEditor(TTCN3MultiPageEditor.PAGE_EDITOR);
		editor.doSaveAs();
		setInput(editor.getEditorInput());
	}

	// ---------------------------------------------------------------------------

	public void gotoMarker(IMarker aMarker) {
		setActivePage(TTCN3MultiPageEditor.PAGE_EDITOR);
		IDE.gotoMarker(getEditor(TTCN3MultiPageEditor.PAGE_EDITOR), aMarker);
	}

	// ---------------------------------------------------------------------------

	@Override
	public boolean isSaveAsAllowed() {
		return fEditor != null && fEditor.isSaveAsAllowed();
	}

	// ---------------------------------------------------------------------------

	@Override
	public boolean isDirty() {
		return fEditor != null && fEditor.isDirty();
	}

	// ---------------------------------------------------------------------------

	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class aClass) {
		Object adapter;
		if (aClass.equals(IContentOutlinePage.class)) {
			adapter = fEditor.getAdapter(aClass);
		} else if (aClass.equals(ITextEditor.class)) {
			adapter = fEditor;
		} else {
			adapter = super.getAdapter(aClass);
		}
		return adapter;
	}

	// ---------------------------------------------------------------------------

	@Override
	protected void setInput(IEditorInput anInput) {
		super.setInput(anInput);

		if (anInput != null) {
			setPartName(anInput.getName());

			// If the fEditor is a file then use the path to the fEditor file
			// (without the filename) as content discription
			if (anInput instanceof ILocationProvider) {
				IPath path = ((ILocationProvider) anInput).getPath(anInput);
				setContentDescription(path.removeLastSegments(1).toString());
			}
		}
	}

	// ---------------------------------------------------------------------------

	public TTCN3Editor getFEditor() {
		return fEditor;
	}
	
	// ---------------------------------------------------------------------------

	public void switchActiveEditor(String filename/*, int offset*/) {
		
			IWorkspace ws = ResourcesPlugin.getWorkspace();
			IWorkspaceRoot root = ws.getRoot(); 
			IProject project = root.getProject(); 
		
			IFile file = null;
			if (project == null) {
				IPath path = ws.getRoot().findMember(filename).getFullPath(); 
				file = ws.getRoot().getFile(path);
			} else {
				file = project.getFile(filename);
			}
		
			if (file == null)
				return;
			
			IWorkbenchPage page = getEditorSite().getPage();
			
			try {
				IDE.openEditor(page, file);
			} catch (PartInitException e) {
				e.printStackTrace();
			}
	}
	
}
