package de.ugoe.cs.swe.trex.ui.preferences.pages;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.osgi.service.prefs.BackingStoreException;

import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;
import de.ugoe.cs.swe.trex.ui.preferences.PreferenceConstants;

/**
 * Color settings for editor syntax highliting.
 */
public class TTCN3EditorFoldingPreferencePage extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {

	private BooleanFieldEditor[] booleanFields = new BooleanFieldEditor[9];
	/**
	 * Create the pref page
	 */
	public TTCN3EditorFoldingPreferencePage() {
		super(FieldEditorPreferencePage.GRID);
		setPreferenceStore(TTCN3UIPlugin.getDefault().getPreferenceStore());
		setDescription("Folding Options\n\n" + "(Note: These preferences will have no effect on already opened TTCN-3 editors.)");
		// TODO: A long term solution is to register at PreferenceStore as
		// IPropertyChangeListener using addPropertyChangeListener()
	}
	
	public void propertyChange(PropertyChangeEvent event) {
		super.propertyChange(event);
		if (event.getProperty().equals(FieldEditor.VALUE)) {
			if (event.getSource() == booleanFields[0]) {
				boolean enabled = booleanFields[0].getBooleanValue();
				for (int i = 1; i < booleanFields.length; i++)
					booleanFields[i].setEnabled(enabled, getFieldEditorParent());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {		
			    
		addField(booleanFields[0] = new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FOLDING_ENABLEMENT, "E&nable Folding Functionality",
				getFieldEditorParent()));
		addField(booleanFields[1] = new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FOLDING_TYPE, "T&ype",
				getFieldEditorParent()));
		addField(booleanFields[2] = new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FOLDING_TEMPLATE, "T&emplate",
				getFieldEditorParent()));
		addField(booleanFields[3] = new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FOLDING_COMPONENT, "&Component",
				getFieldEditorParent()));
		addField(booleanFields[4] = new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FOLDING_GROUP, "&Group",
				getFieldEditorParent()));
		addField(booleanFields[5] = new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FOLDING_ALTSTEP, "&Altstep",
				getFieldEditorParent()));
		addField(booleanFields[6] = new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FOLDING_FUNCTION, "&Function",
				getFieldEditorParent()));
		addField(booleanFields[7] = new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FOLDING_TESTCASE, "&Testcase",
				getFieldEditorParent()));
		addField(booleanFields[8] = new BooleanFieldEditor(
				PreferenceConstants.P_TTCN3_FOLDING_MODULE, "&Module",
				getFieldEditorParent()));
	}

	/** {@inheritDoc} */
	public void init(IWorkbench aWorkbench) {
		// nothing to do here
	}

	/** {@inheritDoc} */
	@Override
	public boolean performOk() {
		boolean value = super.performOk();
		try {
			InstanceScope.INSTANCE.getNode(TTCN3UIPlugin.getUniqueIdentifier()).flush();
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block. What would be a good catch ?
			e.printStackTrace();
		}
		return value;
	}
}
