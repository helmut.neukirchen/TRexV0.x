package de.ugoe.cs.swe.trex.ui.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.IdentifierCache;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Scope;
import de.ugoe.cs.swe.trex.core.analyzer.rfparser.symboltable.Symbol;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3Editor;
import de.ugoe.cs.swe.trex.ui.editors.TTCN3MultiPageEditor;

public class TTCN3OpenDeclarationHandler extends AbstractHandler implements IEditorActionDelegate {

	TTCN3Editor activeEditor;
	TTCN3MultiPageEditor multipageEditor;
	
	// ---------------------------------------------------------------------------

	public TTCN3OpenDeclarationHandler() {
		super();
	}

	// ---------------------------------------------------------------------------

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		if (targetEditor instanceof TTCN3Editor) {
			activeEditor = (TTCN3Editor) targetEditor;
		} else if (targetEditor instanceof TTCN3MultiPageEditor) {
			multipageEditor = (TTCN3MultiPageEditor) targetEditor;
			activeEditor = multipageEditor.getFEditor();
		}
	}

	// ---------------------------------------------------------------------------

	public void run(IAction action) {
		if (activeEditor == null) {
			Shell shell = new Shell();
			MessageDialog.openInformation(shell, "Formatter Error",
					"No Editor initialized. Something is wrong...");
			return;
		}
		
		gotoDeclaration(activeEditor);
	}

	// ---------------------------------------------------------------------------

	public void selectionChanged(IAction action, ISelection selection) {
		// do nothing
	}

	// ---------------------------------------------------------------------------

	public Object execute(ExecutionEvent event)  {
		TTCN3Editor editor= getActiveEditor();
		
		if (editor == null)
			return null;
		
		gotoDeclaration(editor);
		
		return null;
	}
	
	// ---------------------------------------------------------------------------

	private void gotoDeclaration(TTCN3Editor editor) {
		if (editor == null)
			return;
		
		Integer offset = editor.getCaretOffset();

		if (offset == null)
			return;
		
		final IdentifierCache identifierCache = editor.getReconcilingStrategy().getIdentifierCache();
		LocationAST ast = (identifierCache == null) ? null : identifierCache.get(offset);

		if ((ast == null) || (ast.getScope() == null))
			return;

		editor.selectAndReveal(ast.getOffset(), ast.getEndOffset()-ast.getOffset());
		
		Scope scope = ast.getScope();

		Symbol symb = scope.resolve(ast);
		
		if (symb == null)
			return;
		
		LocationAST t = symb.getDeclarationNode();

		if (t == null)
			return;
		
		TTCN3Editor edit;
		
		if (multipageEditor == null) {
			edit = getActiveEditor();
		}
    		
		multipageEditor.switchActiveEditor(symb.getSurroundingScope().getAssociatedFilename());
		edit = getActiveEditor();
		edit.moveCursorOffset(t.getOffset());
		edit.selectAndReveal(t.getOffset(), t.getEndOffset()-t.getOffset());
	}
	
	// ---------------------------------------------------------------------------
	
	private TTCN3Editor getActiveEditor() {
		IWorkbenchWindow window= PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window != null) {
			IWorkbenchPage page= window.getActivePage();
			if (page != null) {
				IEditorPart editor= page.getActiveEditor();
				if (editor instanceof TTCN3MultiPageEditor) {
					multipageEditor = (TTCN3MultiPageEditor)editor;
					return multipageEditor.getFEditor();
				}
			}
		}
		return null;
	}
}
