package de.ugoe.cs.swe.trex.ui.views.findreferences;

import java.util.ArrayList;
import java.util.List;

import de.ugoe.cs.swe.trex.core.analyzer.rfparser.LocationAST;

/**
 * A ModuleElement holds information about the module node and its child context
 * elements. It is used by FindReferences to store information about the
 * results.
 * 
 */
public class ModuleElement {
	private FileElement fileElement = null;
	private LocationAST node = null;
	private List<CompoundElement> compounds = new ArrayList<CompoundElement>();

	/**
	 * Constructs a new ModuleElement object.
	 */
	public ModuleElement(FileElement fileElement, LocationAST node) {
		fileElement.addModule(this);
		this.fileElement = fileElement;
		this.node = node;
	}

	/**
	 * Adds a comnpound to this object
	 * 
	 * @param compound
	 *            the compound element
	 */
	public void addCompound(CompoundElement compound) {
		compounds.add(compound);
	}

	public List<CompoundElement> getCompounds() {
		return compounds;
	}

	public FileElement getFileElement() {
		return fileElement;
	}

	public LocationAST getNode() {
		return node;
	}
}
