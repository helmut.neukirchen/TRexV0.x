package de.ugoe.cs.swe.trex.ui.actionsets;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.CompoundContributionItem;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import de.ugoe.cs.swe.trex.ui.TTCN3UIPlugin;
import de.ugoe.cs.swe.trex.ui.extensionpoints.AbstractCompilerDelegate;

public class CompilerModeList extends CompoundContributionItem {
	
	protected IContributionItem[] getContributionItems() {
		// Get the compiler modes
		final Map<String, Integer> modes = getModes();
		// If no modes, return
		if (modes == null) {
			return new IContributionItem[0];
		}
		// Create the item list
	    IContributionItem[] list = new IContributionItem[modes.size()];
		Iterator<String> it = modes.keySet().iterator();
		IEclipsePreferences defPrefs  = DefaultScope.INSTANCE.getNode("de.ugoe.cs.swe.trex.core");
		defPrefs.putInt("compilerMode", 0);
		int i = 0;
		while (it.hasNext()) {
			final String compilerMode = it.next();
			// Create the menu item configuration
		    CommandContributionItemParameter param = new CommandContributionItemParameter(
		    		PlatformUI.getWorkbench().getActiveWorkbenchWindow(),
		    		"de.ugoe.cs.swe.trex.ui.actionsets.compiler"+i,
		    		"de.ugoe.cs.swe.trex.ui.commands.SelectCompilerMode",
		    		CommandContributionItem.STYLE_CHECK);
		    param.label = compilerMode;
		    // Command Parameter -> to be used in the command handler to identify the menu that was clicked
		    Map<String, String> parms = new HashMap<String, String>();
		    parms.put("de.ugoe.cs.swe.trex.ui.commands.SelectCompilerModeParameter", compilerMode);
		    param.parameters = parms;
		    // Create the menu item
		    list[i] = new CommandContributionItem(param);
			i++;
		}

	    return list;
	}

	/**
	 * Return the compiler modes as a Map<String, Integer>
	 * 
	 * This method is public static because used by other classes to
	 * handle the menu items
	 * 
	 * @return Map<String, Integer> or null if no mode
	 */
	static public Map<String, Integer> getModes() {
		Map<String, Integer> modes;

		String activeCompiler = TTCN3UIPlugin.getDefault().getPreferenceStore()
				.getString("de.ugoe.cs.swe.trex.ui.compiler.selectedCompiler");
		
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IExtensionPoint ep = reg
				.getExtensionPoint("de.ugoe.cs.swe.trex.ui.compiler");
		IExtension[] extensions = ep.getExtensions();

		for (int i = 0; i < extensions.length; i++) {
			IExtension ext = extensions[i];
			IConfigurationElement[] ce = ext.getConfigurationElements();

			for (int j = 0; j < ce.length; j++) {
				try {
					Object obj;
					String name = ce[j].getAttribute("name");
					obj = ce[j].createExecutableExtension("class");
					if ((activeCompiler.equals("")) && (j==0)) {
						TTCN3UIPlugin.getDefault().getPreferenceStore().setValue("de.ugoe.cs.swe.trex.ui.compiler.selectedCompiler", name);
						activeCompiler = name;
						TTCN3UIPlugin.getDefault().getPreferenceStore().setValue("de.ugoe.cs.swe.trex.ui.compiler.compileAllProjects", false);
					}
					if ((obj instanceof AbstractCompilerDelegate)
							&& (name.equals(activeCompiler))) {
						AbstractCompilerDelegate delegate = (AbstractCompilerDelegate) obj;
						modes = delegate.getAvailableModes();
						return modes;
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}
